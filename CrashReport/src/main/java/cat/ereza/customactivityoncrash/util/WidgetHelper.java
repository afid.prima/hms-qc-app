package cat.ereza.customactivityoncrash.util;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import cat.ereza.customactivityoncrash.R;


/**
 * Created by PASPC-02009 on 04/10/2016.
 */
public class WidgetHelper {
    public static AlertDialog showWaitingDialog(Context context, String message){
        View view = (View) LayoutInflater.from(context).inflate(R.layout.custom_dialog_waiting,null);
        TextView tvMessage = (TextView) view.findViewById(R.id.cust_tv_progress);
        tvMessage.setText(message);
        AlertDialog alertDialog = new AlertDialog
                .Builder(context, R.style.customactivityoncrash_MyAlertDialogStyle)
                .setView(view)
                .create();
        alertDialog.show();
        return alertDialog;
    }

    public static AlertDialog showOKDialog(Context context,
                                           String title,
                                           String message,
                                           DialogInterface.OnClickListener onClickListener){
        View view = (View) LayoutInflater.from(context).inflate(R.layout.custom_dialog_waiting,null);
        AlertDialog alertDialog = new AlertDialog
                .Builder(context, R.style.customactivityoncrash_MyAlertDialogStyle)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK",onClickListener)
                .create();
        alertDialog.show();
        return alertDialog;
    }
}
