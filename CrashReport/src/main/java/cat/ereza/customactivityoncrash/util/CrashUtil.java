package cat.ereza.customactivityoncrash.util;

import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Point;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

import com.jaredrummler.android.device.DeviceName;

import net.rehacktive.waspdb.WaspDb;
import net.rehacktive.waspdb.WaspFactory;
import net.rehacktive.waspdb.WaspHash;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import cat.ereza.customactivityoncrash.model.CrashDetailAnalyticInfo;
import cat.ereza.customactivityoncrash.model.DeviceInfo;

public class CrashUtil {
    public static DeviceInfo getDeviceInfo(Context context){
        String androidId = Settings.Secure.getString(
                context.getContentResolver(), Settings.Secure.ANDROID_ID);

        DeviceInfo deviceInfo = new DeviceInfo();
        deviceInfo.setAndroidID(androidId);
        deviceInfo.setAndroidOS(Build.VERSION.CODENAME);
        deviceInfo.setAndroidOS_code(Build.VERSION.SDK_INT+"");
        deviceInfo.setBoard(Build.BOARD);
        deviceInfo.setBrand(DeviceName.getDeviceName());
        deviceInfo.setDevice(Build.DEVICE);
        deviceInfo.setDisplay(Build.DISPLAY);
        deviceInfo.setHardware(Build.HARDWARE);
        deviceInfo.setId(Build.ID);
        deviceInfo.setManufacturer(Build.MANUFACTURER);
        deviceInfo.setProduct(Build.PRODUCT);
        deviceInfo.setType(Build.TYPE);
//        deviceInfo.setDeviceRAM(AppUtils.getTotalRAM(context));
//        deviceInfo.setDeviceStorage(AppUtils.getStorageSize());
//        deviceInfo.setDeviceResolution(getScreenResolution(context));
//        deviceInfo.setDeviceScreenSize(getDeviceSize(context));
        return deviceInfo;
    }

    public static CrashDetailAnalyticInfo getCrashDetail(Context context, int userID, String packageName, String crashDetail){
        String androidId = Settings.Secure.getString(
                context.getContentResolver(), Settings.Secure.ANDROID_ID);

        CrashDetailAnalyticInfo obj = new CrashDetailAnalyticInfo();
        obj.setAndroidID(androidId);
        obj.setPackageName(packageName);
        obj.setCrashDate(new Date());
        obj.setUserID(userID);
        obj.setBrand(DeviceName.getDeviceName());
        obj.setBoard(Build.BOARD);
        obj.setDisplay(Build.DISPLAY);
        obj.setHardware(Build.HARDWARE);
        obj.setManufacturer(Build.MANUFACTURER);
        obj.setId(Build.ID);
        obj.setProduct(Build.PRODUCT);
        obj.setType(Build.TYPE);
        obj.setAndroidOS(Build.VERSION.CODENAME);
        obj.setAndroidOS_code(Build.VERSION.SDK_INT+"");
        obj.setDeviceRAM(getTotalRAM(context));
        obj.setDeviceStorage(getStorageSize());
        obj.setDeviceScreenSize(getDeviceSize(context));
        obj.setDeviceResolution(getScreenResolution(context));
        obj.setCrashIP(""+getIPAddress(true));
        obj.setMacAddress(""+getMacAddress(context));
        obj.setCrashDetail(""+crashDetail);

        return obj;
    }

    public static String getPackageName(Context context){
        return context.getPackageName();
    }

    public static String getStringFromDate(Date value){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        return sdf.format(value);
    }

    public static long getTotalRAM(Context context){
        ActivityManager actManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo memInfo = new ActivityManager.MemoryInfo();
        actManager.getMemoryInfo(memInfo);
        long totalMemory = memInfo.totalMem;
        return totalMemory;
    }

    public static long getStorageSize(){
        StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getAbsolutePath());
        long totalMemory;
        if(Build.VERSION.SDK_INT>=18){
            totalMemory = statFs.getBlockCountLong() * statFs.getBlockSizeLong();
        }else{
            totalMemory = statFs.getBlockCount() * statFs.getBlockSize();
        }
        return totalMemory;
    }

    public static String getScreenResolution(Context context){
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        String res = "";
        Point size = new Point();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            wm.getDefaultDisplay().getRealSize(size);
            res = size.x +"x" + size.y;
        }else{
            Display display = wm.getDefaultDisplay();
            DisplayMetrics metrics = new DisplayMetrics();
            display.getMetrics(metrics);
            int width = metrics.widthPixels;
            int height = metrics.heightPixels;
            res = width +"x" + height;
        }

        return res;
    }

    public static double getDeviceSize(Context context){
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Point size = new Point();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            wm.getDefaultDisplay().getRealSize(size);
            DecimalFormat df = new DecimalFormat("#.0");
            DisplayMetrics dm = new DisplayMetrics();
            wm.getDefaultDisplay().getRealMetrics(dm);
            double wi=(double)size.x/dm.xdpi;
            double hi=(double)size.y/dm.ydpi;
            double x = Math.pow(wi,2);
            double y = Math.pow(hi,2);
            double screenInches = Math.sqrt(x+y);
            return Double.parseDouble(String.valueOf(screenInches));
        }else{
            Display display = wm.getDefaultDisplay();
            DecimalFormat df = new DecimalFormat("#.0");
            DisplayMetrics dm = new DisplayMetrics();
            double wi=(double)display.getWidth()/dm.xdpi;
            double hi=(double)display.getHeight()/dm.ydpi;
            double x = Math.pow(wi,2);
            double y = Math.pow(hi,2);
            double screenInches = Math.sqrt(x+y);
            return Double.parseDouble(String.valueOf(screenInches));
        }

//        return Double.parseDouble(df.format(screenInches));
    }

    public static String getMacAddress(Context context){
        String macAddress = "";
        try{
            WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            WifiInfo wInfo = wifiManager.getConnectionInfo();
            macAddress = wInfo.getMacAddress();
        }catch (Exception e){

        }

        return macAddress;
    }

    /**
     * Get IP address from first non-localhost interface
     * @param useIPv4   true=return ipv4, false=return ipv6
     * @return  address or empty string
     */
    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':')<0;

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim<0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception ignored) { } // for now eat exceptions
        return "";
    }

    public static void flushCrashReport(){
        WaspDb db = WaspFactory.openOrCreateDatabase(cat.ereza.customactivityoncrash.helper.GlobalHelper.getDatabasePath(),
                cat.ereza.customactivityoncrash.helper.GlobalHelper.DB_MASTER,
                cat.ereza.customactivityoncrash.helper.GlobalHelper.getDatabasePass());
        WaspHash crashTbl = db.openOrCreateHash(cat.ereza.customactivityoncrash.helper.GlobalHelper.CrashReport);
        crashTbl.flush();
    }
}
