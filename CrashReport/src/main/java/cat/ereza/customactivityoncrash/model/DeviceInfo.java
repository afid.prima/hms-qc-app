package cat.ereza.customactivityoncrash.model;

public class DeviceInfo {
    private String androidID;
    private String brand;
    private String device;
    private String board;
    private String display;
    private String hardware;
    private String manufacturer;
    private String id;
    private String product;
    private String type;
    private String androidOS;
    private String androidOS_code;
    private long deviceRAM;
    private long deviceStorage;
    private String deviceResolution;
    private double deviceScreenSize;

    public String getDeviceResolution() {
        return deviceResolution;
    }

    public void setDeviceResolution(String deviceResolution) {
        this.deviceResolution = deviceResolution;
    }

    public double getDeviceScreenSize() {
        return deviceScreenSize;
    }

    public void setDeviceScreenSize(double deviceScreenSize) {
        this.deviceScreenSize = deviceScreenSize;
    }

    public long getDeviceRAM() {
        return deviceRAM;
    }

    public void setDeviceRAM(long deviceRAM) {
        this.deviceRAM = deviceRAM;
    }

    public long getDeviceStorage() {
        return deviceStorage;
    }

    public void setDeviceStorage(long deviceStorage) {
        this.deviceStorage = deviceStorage;
    }

    public String getAndroidID() {
        return androidID;
    }

    public void setAndroidID(String androidID) {
        this.androidID = androidID;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getBoard() {
        return board;
    }

    public void setBoard(String board) {
        this.board = board;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public String getHardware() {
        return hardware;
    }

    public void setHardware(String hardware) {
        this.hardware = hardware;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAndroidOS() {
        return androidOS;
    }

    public void setAndroidOS(String androidOS) {
        this.androidOS = androidOS;
    }

    public String getAndroidOS_code() {
        return androidOS_code;
    }

    public void setAndroidOS_code(String androidOS_code) {
        this.androidOS_code = androidOS_code;
    }
}

