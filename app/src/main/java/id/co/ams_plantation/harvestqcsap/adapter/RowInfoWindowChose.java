package id.co.ams_plantation.harvestqcsap.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import id.co.ams_plantation.harvestqcsap.Fragment.TphInputFragment;
import id.co.ams_plantation.harvestqcsap.model.TPH;
import id.co.ams_plantation.harvestqcsap.R;

/**
 * Created by user on 10/9/2017.
 */

public class RowInfoWindowChose extends RecyclerView.Adapter<RowInfoWindowChose.Holder> {
    Context context;
    private ArrayList<TPH> tphArrayList;

    public RowInfoWindowChose(Context context, ArrayList<TPH> tphs) {
        this.context = context;
        this.tphArrayList = tphs;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.row_infowindow_chose, parent, false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.setValue(tphArrayList.get(position));
    }

    @Override
    public int getItemCount() {
        return tphArrayList != null ? tphArrayList.size() : 0;
    }

    class Holder extends RecyclerView.ViewHolder {
        CardView cv_riw_chose;
        TextView tv_riw_chose;
        TextView tv_riw_chose1;
        ImageView iv_status;

        public Holder(View view) {
            super(view);
            cv_riw_chose = (CardView) view.findViewById(R.id.cv_riw_chose);
            tv_riw_chose = (TextView) view.findViewById(R.id.tv_riw_chose);
            tv_riw_chose1 = (TextView) view.findViewById(R.id.tv_riw_chose1);
            iv_status = (ImageView) view.findViewById(R.id.iv_status);
        }

        public void setValue(final TPH value) {
//            TphActivity activity = ((TphActivity) context);
            SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd MMM yyyy");

            tv_riw_chose.setText(value.getNamaTph());
            tv_riw_chose1.setText(sdf.format(new Date(value.getCreateDate())));
            //tv_riw_chose2.setText(nilai);
            iv_status.setVisibility(View.GONE);
//            iv_status = activity.newRecording.setupivstatus(modelInfoWindowChose.getStatus(),iv_status);
//
//            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(100, 100);
//            iv_status.setLayoutParams(layoutParams);

            cv_riw_chose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Fragment fragment = (Fragment) activity.getSupportFragmentManager().findFragmentById(R.id.content_container);
//                    if(fragment instanceof TphInputFragment){
//                        int idx = 0;
//                        for (HashMap.Entry<Integer, TPH> entry : ((TphInputFragment) fragment).tphHelper.hashpoint.entrySet()) {
//                            if(value.getNoTph().equals(entry.getValue().getNoTph())){
//                                idx = entry.getKey();
//                            }
//                        }
//                        ((TphInputFragment) fragment).tphHelper.callout.animatedHide();
//                        ((TphInputFragment) fragment).tphHelper.showInfoWindow(idx,value);
//                    }
                }
            });
        }
    }
}