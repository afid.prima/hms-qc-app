package id.co.ams_plantation.harvestqcsap.TableView.holder;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder;

import id.co.ams_plantation.harvestqcsap.TableView.model.TableViewCell;
import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.model.SensusBjr;
import id.co.ams_plantation.harvestqcsap.model.TransaksiPanen;

/**
 * Created by user on 12/24/2018.
 */

public class SensusBjrCellViewHolder extends AbstractViewHolder {

    public final TextView cell_textview;
    public final LinearLayout cell_container;
    private TableViewCell cell;

    public SensusBjrCellViewHolder(View itemView) {
        super(itemView);
        cell_textview = (TextView) itemView.findViewById(R.id.cell_data);
        cell_container = (LinearLayout) itemView.findViewById(R.id.cell_container);
    }

    public void setCell(TableViewCell cell) {
        this.cell = cell;
        cell_textview.setText(String.valueOf(cell.getData()));
        // If your TableView should have auto resize for cells & columns.
        // Then you should consider the below lines. Otherwise, you can ignore them.

        // It is necessary to remeasure itself.
        cell_container.getLayoutParams().width = LinearLayout.LayoutParams.WRAP_CONTENT;

        String [] id = cell.getId().split("-");
        switch (Integer.parseInt(id[2])){
            case SensusBjr.SAVE:{
                cell_container.setBackgroundColor(SensusBjr.COLOR_SAVE);
                break;
            }
            case TransaksiPanen.UPLOAD:{
                cell_container.setBackgroundColor(SensusBjr.COLOR_UPLOAD);
                break;
            }
        }

        cell_textview.requestLayout();
    }
}
