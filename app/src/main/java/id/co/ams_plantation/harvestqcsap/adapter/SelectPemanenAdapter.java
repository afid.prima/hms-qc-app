package id.co.ams_plantation.harvestqcsap.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import id.co.ams_plantation.harvestqcsap.model.Pemanen;
import id.co.ams_plantation.harvestqcsap.R;

public class SelectPemanenAdapter extends ArrayAdapter<Pemanen> {

    public ArrayList<Pemanen> items;
    public ArrayList<Pemanen> itemsAll;
    public ArrayList<Pemanen> suggestions;
    private int viewResourceId;

    public SelectPemanenAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Pemanen> data) {
        super(context, resource, data);
        this.items = new ArrayList<>();
        this.items.addAll(data);
        this.itemsAll = (ArrayList<Pemanen>)this.items.clone();
        this.suggestions = new ArrayList<>();
        this.viewResourceId = resource;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(viewResourceId, null);
        }
        if(items.size()>position){
            Pemanen tph = items.get(position);
            if (tph != null) {
                RelativeLayout rlImage = (RelativeLayout) v.findViewById(R.id.rlImage);
                TextView item_ket = (TextView) v.findViewById(R.id.item_ket);
                TextView item_ket1 = (TextView) v.findViewById(R.id.item_ket1);
                TextView item_ket2 = (TextView) v.findViewById(R.id.item_ket2);
                TextView item_ket3 = (TextView) v.findViewById(R.id.item_ket3);
                rlImage.setVisibility(View.GONE);
                item_ket.setText(tph.getNama());
                item_ket2.setText(tph.getNik());
                item_ket1.setText(tph.getNoUrut() == 0 ? "-" : String.valueOf(tph.getNoUrut()));
                item_ket3.setText(tph.getEstCodeSAP() == null ? "-" : tph.getEstCodeSAP());
            }
        }
        return v;
    }

    public Pemanen getItemAt(int position){
        return items.get(position);
    }

    @Override
    public int getCount(){
        return items!=null?items.size():0;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            String str = ((Pemanen)(resultValue)).getNama();
            return str;
        }
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if(constraint != null) {
                suggestions.clear();
                for (Pemanen Pemanen : itemsAll) {
                    try {
                        if(String.valueOf(Pemanen.getNoUrut()).contains(constraint.toString())){
                            suggestions.add(Pemanen);
                        }else if (Pemanen.getNik().toLowerCase().contains(constraint.toString().toLowerCase())){
                            suggestions.add(Pemanen);
                        }else if (Pemanen.getNama().toLowerCase().contains(constraint.toString().toLowerCase())){
                            suggestions.add(Pemanen);
                        }else if (Pemanen.getEstCodeSAP().toLowerCase().contains(constraint.toString().toLowerCase())){
                            suggestions.add(Pemanen);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<Pemanen> filteredList = (ArrayList<Pemanen>) results.values;
            items.clear();
            if(results != null && results.count > 0) {
                items.addAll(filteredList);
            }else{
                items.clear();
            }
            notifyDataSetChanged();
        }
    };
}
