package id.co.ams_plantation.harvestqcsap.util;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.adapter.QrItemAdapter;
import id.co.ams_plantation.harvestqcsap.model.ModelRecyclerView;

public class ModelRecyclerViewHelper {
    Context context;
    AlertDialog listrefrence;

    public ModelRecyclerViewHelper(Context context) {
        this.context = context;
    }

    public void showSummaryTransaksi(ArrayList<ModelRecyclerView> modelRecyclerViews){
        if(listrefrence != null){
            if(listrefrence.isShowing()){
                listrefrence.dismiss();
            }
        }

        View view = LayoutInflater.from(context).inflate(R.layout.show_qr2,null);
        RelativeLayout isi = (RelativeLayout) view.findViewById(R.id.isi);
        Button shareqr = (Button) view.findViewById(R.id.btn_shareqr);
        RecyclerView rv = (RecyclerView) view.findViewById(R.id.rv);

        rv.setLayoutManager(new LinearLayoutManager(context));
        QrItemAdapter adapter = new QrItemAdapter(context,modelRecyclerViews);
        rv.setAdapter(adapter);
        shareqr.setText(context.getResources().getString(R.string.close));
        shareqr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listrefrence.dismiss();
            }
        });
        listrefrence = WidgetHelper.showListReference(listrefrence,view,context);
    }
}
