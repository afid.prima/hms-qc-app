package id.co.ams_plantation.harvestqcsap.model;

/**
 * Created on : 20,December,2021
 * Author     : Afid
 */

public class MasterApps extends Apps{
    private boolean isAvailable;
    private boolean isUpdateAvailable;

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public boolean isUpdateAvailable(){
        return isUpdateAvailable;
    }

    public void setUpdateAvailable(boolean updateAvailable) {
        isUpdateAvailable = updateAvailable;
    }
}
