package id.co.ams_plantation.harvestqcsap.connection;

import android.util.Log;

public class Logger {

    public static void request(ServiceRequest request) {
        Log.i(":", "********************* Api Request *********************");
        Log.i("Url", request.getHost() + request.getPath());
        Log.i("Method", String.valueOf(request.getMethod()));
        Log.i("Tag", String.valueOf(request.getTag()));
        Log.i("Path", request.getPath());
        if (request.getQueryParameter() != null) {
            StringBuilder query = new StringBuilder();
            for (String key : request.getQueryParameter().keySet()) {
                query.append(key).append(":").append(request.getQueryParameter().get(key)).append("\n");
            }

            Log.i("Query Parameter", query.toString());
        }

        if (request.getBodyParameter() != null) {
            StringBuilder query = new StringBuilder();
            for (String key : request.getBodyParameter().keySet()) {
                query.append(key).append(":").append(request.getBodyParameter().get(key)).append("\n");
            }

            Log.i("Body Parameter", query.toString());
        }

        if(request.getBodyRawArray() != null){
            Log.i("Body Parameter", String.valueOf(request.getBodyRawArray()));
        }

        if(request.getBodyRaw() != null){
            Log.i("Body Parameter", String.valueOf(request.getBodyRaw()));
        }

        if (request.getFiles() != null) {
            StringBuilder files = new StringBuilder();
            for (String key : request.getFiles().keySet()) {
                files.append(key).append(":").append(request.getFiles().get(key).getName()).append("\n");
            }

            Log.i("Multipart File", files.toString());
        }

        if (request.getHeaders() != null) {
            String header = "";
            for (String key : request.getHeaders().keySet()) {
                header = header + key + ":" + request.getHeaders().get(key) + "\n";
            }

            Log.i("Header", header);
        }

        Log.i(":", "*******************************************************");
    }

    public static void analytic(Tag tag, long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
        Log.w(":", "#################### API Analytics ####################");
        Log.w("Tag", String.valueOf(tag));
        Log.w("Taken Time", String.valueOf(timeTakenInMillis) + " ms");
        Log.w("Bytes Sent", bytesSent + " byte");
        Log.w("Bytes Received", bytesReceived + " byte");
        Log.w("From Cache", String.valueOf(isFromCache));
        Log.w(":", "#######################################################");
    }

    public static void response(ServiceResponse response, String content) {
        Log.i(":", "==================== Api Response ====================");
        Log.i("Tag", "" + String.valueOf(response.getTag()));
        Log.i("Http Code", "" + String.valueOf(response.getHttpCode()));
        Log.i("Error Code", "" + String.valueOf(response.getErrorCode()));
        Log.i("Message", "" + response.getMessage());
        if (content != null) Log.i("Content", "" + content);
        Log.i(":", "======================================================");
    }
}
