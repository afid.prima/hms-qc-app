package id.co.ams_plantation.harvestqcsap.util;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Hashtable;

import id.co.ams_plantation.harvestqcsap.HarvestApp;
import id.co.ams_plantation.harvestqcsap.R;

import static android.graphics.Color.BLACK;
import static android.graphics.Color.WHITE;

/**
 * Created by user on 11/30/2018.
 */

public class QrHelper {
    Context context;

    //QRcode
    public static final int QR_WIDTH = 450;
    public static final int QR_HEIGHT = 450;
    public static final int BARCODE_WIDTH = 900;
    public static final int BARCODE_HEIGHT = 220;

    public QrHelper(Context context) {
        this.context = context;
    }

    public static Bitmap encodeAsBitmapBarcode(String str) throws WriterException {
        try {
            MultiFormatWriter writer = new MultiFormatWriter();
            String finalData = Uri.encode(str);

            // Use 1 as the height of the matrix as this is a 1D Barcode.
            BitMatrix bm = writer.encode(finalData, BarcodeFormat.CODE_128, BARCODE_WIDTH, 1);
            int bmWidth = bm.getWidth();

            Bitmap imageBitmap = Bitmap.createBitmap(bmWidth, BARCODE_HEIGHT, Bitmap.Config.ARGB_8888);

            for (int i = 0; i < bmWidth; i++) {
                // Paint columns of width 1
                int[] column = new int[BARCODE_HEIGHT];
                Arrays.fill(column, bm.get(i, 0) ? Color.BLACK : Color.WHITE);
                imageBitmap.setPixels(column, 0, 1, i, 0, 1, BARCODE_HEIGHT);
            }

            return imageBitmap;
        } catch (WriterException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Bitmap encodeAsBitmap(String str) throws WriterException {
        BitMatrix result;
        try {
            Hashtable<EncodeHintType, String> hints = new Hashtable<EncodeHintType, String>();
            hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
            result = new QRCodeWriter().encode(str,
                    BarcodeFormat.QR_CODE, QR_WIDTH, QR_HEIGHT, hints);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int w = result.getWidth();
        int h = result.getHeight();
        int[] pixels = new int[w * h];
        for (int y = 0; y < h; y++) {
            int offset = y * w;
            for (int x = 0; x < w; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, QR_WIDTH, 0, 0, w, QR_HEIGHT);
        return bitmap;
    }

    public File saveBitMap(RecyclerView rv){
        File pictureFile = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 + "/" + GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_QR], "qr.png");
        Bitmap bitmap;
        bitmap = GlobalHelper.getScreenshotFromRecyclerView(rv);
        try {
            pictureFile.createNewFile();
            FileOutputStream oStream = new FileOutputStream(pictureFile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, oStream);
            oStream.flush();
            oStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return pictureFile;
    }


    public static void SaveBitmap(String namafile,Bitmap bitmap){
        File pictureFile = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 + "/" + GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_QR], namafile);
        try {
            pictureFile.createNewFile();
            FileOutputStream oStream = new FileOutputStream(pictureFile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, oStream);
            oStream.flush();
            oStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void share(FragmentActivity activity, RecyclerView rv){

        File fileQr = saveBitMap(rv);
        Uri imageUri = Uri.parse(fileQr.toString());
//        Uri test = Uri.parse("content:/"+fileQr.getAbsolutePath());
//        Log.i("ShareUriFile", "share: "+test.toString());
        if (fileQr.isFile()) {
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("image/*");
            share.putExtra(Intent.EXTRA_STREAM, imageUri); // buat image temp kalo ga di save ?
//            share.putExtra(Intent.EXTRA_STREAM, test);
            activity.startActivity(Intent.createChooser(share, HarvestApp.getContext().getResources().getString(R.string.choose_app_to_share)));
        } else {
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("image/*");
            share.putExtra(Intent.EXTRA_STREAM, imageUri); // buat image temp kalo ga di save ?
            activity.startActivity(Intent.createChooser(share, HarvestApp.getContext().getResources().getString(R.string.choose_app_to_share)));
        }
    }
}
