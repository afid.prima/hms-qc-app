package id.co.ams_plantation.harvestqcsap.model;

import org.dizitart.no2.Document;
import org.dizitart.no2.IndexType;
import org.dizitart.no2.mapper.Mappable;
import org.dizitart.no2.mapper.NitriteMapper;
import org.dizitart.no2.objects.Id;
import org.dizitart.no2.objects.Index;
import org.dizitart.no2.objects.Indices;

import java.io.Serializable;

/**
 * Created by user on 12/13/2018.
 */

@Indices({
        @Index(value = "param1", type = IndexType.NonUnique),
        @Index(value = "param2", type = IndexType.NonUnique),
        @Index(value = "param3", type = IndexType.NonUnique)
})
public class DataNitrit implements Mappable, Serializable {

    @Id
    String idDataNitrit;
    String valueDataNitrit;
    String param1;
    String param2;
    String param3;

    public DataNitrit(String idDataNitrit, String valueDataBitrit) {
        this.idDataNitrit = idDataNitrit;
        this.valueDataNitrit = valueDataBitrit;
    }

    public DataNitrit(String idDataNitrit, String valueDataNitrit, String param1) {
        this.idDataNitrit = idDataNitrit;
        this.valueDataNitrit = valueDataNitrit;
        this.param1 = param1;
    }


    public DataNitrit(String idDataNitrit, String valueDataNitrit, String param1,String param2) {
        this.idDataNitrit = idDataNitrit;
        this.valueDataNitrit = valueDataNitrit;
        this.param1 = param1;
        this.param2 = param2;
    }

    public DataNitrit(String idDataNitrit, String valueDataNitrit, String param1, String param2, String param3) {
        this.idDataNitrit = idDataNitrit;
        this.valueDataNitrit = valueDataNitrit;
        this.param1 = param1;
        this.param2 = param2;
        this.param3 = param3;
    }

    public String getIdDataNitrit() {
        return idDataNitrit;
    }

    public void setIdDataNitrit(String idDataNitrit) {
        this.idDataNitrit = idDataNitrit;
    }

    public String getValueDataNitrit() {
        return valueDataNitrit;
    }

    public void setValueDataNitrit(String valueDataNitrit) {
        this.valueDataNitrit = valueDataNitrit;
    }

    public String getParam1() {
        return param1;
    }

    public void setParam1(String param1) {
        this.param1 = param1;
    }

    public String getParam2() {
        return param2;
    }

    public void setParam2(String param2) {
        this.param2 = param2;
    }

    public String getParam3() {
        return param3;
    }

    public void setParam3(String param3) {
        this.param3 = param3;
    }

    @Override
    public Document write(NitriteMapper mapper) {
        Document document = new Document();
        document.put("idDataNitrit", idDataNitrit);
        document.put("valueDataNitrit", valueDataNitrit);
        document.put("param1", param1);
        document.put("param2", param2);
        document.put("param3", param3);
        return document;
    }

    @Override
    public void read(NitriteMapper mapper, Document document) {
        idDataNitrit = (String) document.get("idDataNitrit");
        valueDataNitrit = (String) document.get("valueDataNitrit");
        param1 = (String) document.get("param1");
        param2 = (String) document.get("param2");
        param3 = (String) document.get("param3");
    }
}
