package id.co.ams_plantation.harvestqcsap.util;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;

import id.co.ams_plantation.harvestqcsap.Fragment.TphInputFragment;
import id.co.ams_plantation.harvestqcsap.ui.MapActivity;
import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.adapter.RowInfoWindowChose;
import id.co.ams_plantation.harvestqcsap.model.TPH;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;

/**
 * Created by user on 12/11/2018.
 */

public class InfoWindowChose {
    static Context context;
    static ViewGroup retView;

    private static void setupInfoWindowsChose(final Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        retView = (ViewGroup) inflater.inflate(R.layout.info_window_chose, null);
        InfoWindowChose.context = context;
    }
    public static ViewGroup setupInfoWindowsChose(final Context context, ArrayList<TPH> windowsChoseArrayList){
        setupInfoWindowsChose(context);
        initBlockDetail(retView,windowsChoseArrayList);
        return retView;
    }

    private static void initBlockDetail(final ViewGroup retView, ArrayList<TPH> windowsChoseArrayList) {
//        TphActivity activity = (TphActivity)context;

        RecyclerView rv_objectspatila = (RecyclerView) retView.findViewById(R.id.rv_objectspatila);
        Button btnClose = (Button) retView.findViewById(R.id.btnClose_iw);

//        RowInfoWindowChose rowInfoWindowChose = new RowInfoWindowChose(activity,windowsChoseArrayList);
//        rv_objectspatila.setLayoutManager(new GridLayoutManager(activity,2));
//        ScaleInAnimationAdapter scaleInAnimationAdapter = new ScaleInAnimationAdapter(rowInfoWindowChose);
//        rv_objectspatila.setAdapter(scaleInAnimationAdapter);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Fragment fragment = (Fragment) activity.getSupportFragmentManager().findFragmentById(R.id.content_container);
//                if(fragment instanceof TphInputFragment){
//                    ((TphInputFragment) fragment).tphHelper.callout.hide();
//                }
            }
        });
    }


    private static void setupInfoWindowsChoseTphnLangsiran(final Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        retView = (ViewGroup) inflater.inflate(R.layout.info_window_chose, null);
        InfoWindowChose.context = context;
    }
//    public static ViewGroup setupInfoWindowsChoseTphnLangsiran(final Context context, ArrayList<TPHnLangsiran> windowsChoseArrayList){
//        setupInfoWindowsChoseTphnLangsiran(context);
//        initBlockDetailTphnLangsiran(retView,windowsChoseArrayList);
//        return retView;
//    }

//    private static void initBlockDetailTphnLangsiran(final ViewGroup retView, ArrayList<TPHnLangsiran> windowsChoseArrayList) {
//        MapActivity activity = (MapActivity)context;
//
//        RecyclerView rv_objectspatila = (RecyclerView) retView.findViewById(R.id.rv_objectspatila);
//        Button btnClose = (Button) retView.findViewById(R.id.btnClose_iw);
//
//        RowInfoWindowChoseTphnLangsiran rowInfoWindowChose = new RowInfoWindowChoseTphnLangsiran(activity,windowsChoseArrayList);
//        rv_objectspatila.setLayoutManager(new GridLayoutManager(activity,2));
//        ScaleInAnimationAdapter scaleInAnimationAdapter = new ScaleInAnimationAdapter(rowInfoWindowChose);
//        rv_objectspatila.setAdapter(scaleInAnimationAdapter);
//
//        btnClose.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ((MapActivity) context).callout.hide();
//            }
//        });
//    }
}
