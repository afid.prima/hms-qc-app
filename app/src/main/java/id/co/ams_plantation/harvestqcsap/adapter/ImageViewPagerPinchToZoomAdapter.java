package id.co.ams_plantation.harvestqcsap.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bogdwellers.pinchtozoom.ImageMatrixTouchHandler;
import com.bogdwellers.pinchtozoom.ImageViewerCorrector;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import id.co.ams_plantation.harvestqcsap.HarvestApp;
import id.co.ams_plantation.harvestqcsap.util.GlobalHelper;
import id.co.ams_plantation.harvestqcsap.R;


/**
 * Created by user on 12/11/2017.
 */

public class ImageViewPagerPinchToZoomAdapter extends PagerAdapter {
    private List<File> drawables;
    float angle = 0;
    public ImageViewPagerPinchToZoomAdapter(List<File> drawables) {
        this.drawables = drawables;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Context context = container.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.page_image, null);
        container.addView(view);

        ImageView imageView = (ImageView) view.findViewById(R.id.image);
        ImageView iv_iz_rotate = (ImageView) view.findViewById(R.id.iv_iz_rotate);

        iv_iz_rotate.setImageDrawable(new
                IconicsDrawable(context)
                .icon(MaterialDesignIconic.Icon.gmi_refresh).sizeDp(30)
                .colorRes(R.color.Black));

        if(drawables.get(position).toString().startsWith("http:")){
            String surl = drawables.get(position).toString();
            if(drawables.get(position).toString().startsWith("http://")){

            }else if(drawables.get(position).toString().startsWith("http:/")){
                surl =surl.replace("http:/","http://");
            }
            try {

                Picasso.with(HarvestApp.getContext()).load(GlobalHelper.setUrlFoto(surl)).into(imageView);
//                URL urlConnection = new URL(surl);
//                HttpURLConnection connection = (HttpURLConnection) urlConnection.openConnection();
//                connection.setDoInput(true);
//                connection.connect();
//                InputStream input = connection.getInputStream();
//                myBitmap = BitmapFactory.decodeStream(input);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            Bitmap myBitmap = BitmapFactory.decodeFile(drawables.get(position).getAbsolutePath());
            if(myBitmap != null) {

                Bitmap rotatedBitmap = adjustRotateBitmap(myBitmap);

                imageView.setImageBitmap(rotatedBitmap);

                ImageMatrixTouchHandler imageMatrixTouchHandler = new ImageMatrixTouchHandler(context);
                imageView.setOnTouchListener(imageMatrixTouchHandler);
            }else{
                Toast.makeText(HarvestApp.getContext(),"Gagal Load Foto",Toast.LENGTH_SHORT);
            }
        }

        iv_iz_rotate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                angle += 90;
                if(angle > 360){
                    angle = 0;
                }
                imageView.setRotation(angle);
            }
        });

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;

        ImageView imageView = (ImageView) view.findViewById(R.id.image);
        imageView.setImageResource(0);

        container.removeView(view);
    }

    @Override
    public int getCount() {
        return drawables.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public int getItemPosition (Object object) {
        return POSITION_NONE;
    }

    public Bitmap adjustRotateBitmap(Bitmap source) {
        if (source.getWidth() > source.getHeight()) {
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
        }else{
            return source;
        }
    }
}

