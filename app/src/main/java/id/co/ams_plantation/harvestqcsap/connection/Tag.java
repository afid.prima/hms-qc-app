package id.co.ams_plantation.harvestqcsap.connection;


public enum Tag {

    GetTphListByEstate,
    GetSensusBJRByEstate,
    GetQcBuahByEstate,
    GetQCMutuAncakByEstate,
    GetMasterUserByEstate,
    GetPemanenListByEstate,
    GetApplicationConfiguration,
    GetMasterSPKByEstate,
    GetEstateMapping,
    GetQCQuestionAnswer,
    GetSessionQcFotoPanen,

    PostQcBuahImages,
    PostQcBuah,
    PostSensusBjrImages,
    PostSensusBjr,
    PostQcAncakImages,
    PostQcAncak,
    PostSessionQcFotoPanen,
    InsertNewSession,

    GetQCMutuAncakHeaderCounterByUserID,
    GetQCBuahCounterByUserID,
    GetChoiceSelection,
    GetNameInputByTPanen,
    GetPemanenByTPanen,
    GetSPBInpectionFoto,

    GetAllMapFilesByEstate,

    CrashReportList
}
