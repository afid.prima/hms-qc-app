package id.co.ams_plantation.harvestqcsap.Fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.evrencoskun.tableview.TableView;
import com.evrencoskun.tableview.listener.ITableViewListener;
import com.google.gson.Gson;
import com.mikepenz.iconics.utils.Utils;
import com.roughike.swipeselector.OnSwipeItemSelectedListener;
import com.roughike.swipeselector.SwipeItem;
import com.roughike.swipeselector.SwipeSelector;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.ObjectRepository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import id.co.ams_plantation.harvestqcsap.TableView.adapter.QcMutuBuahAdapter;
import id.co.ams_plantation.harvestqcsap.TableView.holder.ColumnHeaderViewHolder;
import id.co.ams_plantation.harvestqcsap.TableView.holder.QcMutuBuahRowHeaderViewHolder;
import id.co.ams_plantation.harvestqcsap.TableView.popup.ColumnHeaderPopup;
import id.co.ams_plantation.harvestqcsap.TableView.view_model.QcMutuBuahTableViewModel;
import id.co.ams_plantation.harvestqcsap.model.DataNitrit;
import id.co.ams_plantation.harvestqcsap.model.HeaderTableTransaksi;
import id.co.ams_plantation.harvestqcsap.model.QcMutuBuah;
import id.co.ams_plantation.harvestqcsap.model.TransaksiPanen;
import id.co.ams_plantation.harvestqcsap.ui.BaseActivity;
import id.co.ams_plantation.harvestqcsap.ui.MainMenuActivity;
import id.co.ams_plantation.harvestqcsap.ui.QcMutuBuahActivity;
import id.co.ams_plantation.harvestqcsap.util.CompleteTextViewHelper;
import id.co.ams_plantation.harvestqcsap.util.GlobalHelper;
import id.co.ams_plantation.harvestqcsap.util.TransaksiPanenHelper;
import id.co.ams_plantation.harvestqcsap.util.WidgetHelper;
import id.co.ams_plantation.harvestqcsap.R;

/**
 * Created by user on 12/4/2018.
 */

public class QcMutuBuahFragment extends Fragment {

    public static final int STATUS_LONG_OPERATION_NONE = 0;
    public static final int STATUS_LONG_OPERATION_SEARCH = 1;
    public static final int STATUS_LONG_OPERATION_SELECT_ITEM = 2;

    HashMap<String, QcMutuBuah> origin;
    int totalQcJanjang;
    QcMutuBuahAdapter qcMutuBuahAdapter;
    TableView mTableView;
//    Filter mTableFilter;
    QcMutuBuahTableViewModel mTableViewModel;
    CompleteTextViewHelper etSearch;
    Button btnSearch;
    TextView tvQcTph;
    TextView tvQcJanjang;
    RelativeLayout ltableview;
    LinearLayout llNewData;

    Set<String> listSearch;
    Date dateSelected;
    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
    SimpleDateFormat timeFormat = new SimpleDateFormat("HH:ss");

    QcMutuBuah qcMutuBuah;
    TransaksiPanenHelper transaksiPanenHelper;
    ArrayList<HeaderTableTransaksi> headerTableTransaksis;
    boolean isVisibleToUser;
    MainMenuActivity mainMenuActivity;

    public static QcMutuBuahFragment getInstance(){
        QcMutuBuahFragment fragment = new QcMutuBuahFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.qc_buah_layout,null,false);
        mainMenuActivity = (MainMenuActivity) getActivity();
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        this.isVisibleToUser = isVisibleToUser;
        main();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SwipeSelector menuSwipeSelector = view.findViewById(R.id.menuSwipeSelector);
        etSearch = view.findViewById(R.id.etSearch);
        btnSearch = view.findViewById(R.id.btnSearch);
        tvQcTph = view.findViewById(R.id.tvQcTph);
        tvQcJanjang = view.findViewById(R.id.tvQcJanjang);
        ltableview = view.findViewById(R.id.ltableview);
        mTableView = view.findViewById(R.id.tableview);
        llNewData = view.findViewById(R.id.llNewData);

        dateSelected = new Date();
        origin = new HashMap<>();
        totalQcJanjang = 0;
        llNewData.setVisibility(View.GONE);
        transaksiPanenHelper = new TransaksiPanenHelper(this.mainMenuActivity);

        ArrayList<SwipeItem> swipeItems= new ArrayList<>();
        Long now = System.currentTimeMillis();
        for(int i = 0; i < GlobalHelper.MAX_LAST_DAY_DATA; i++){
            Long l = now - ( i * 24 * 60 * 60 * 1000);
            swipeItems.add(new SwipeItem(i,sdf.format(l),""));
        }
        Collections.reverse(swipeItems);

        SwipeItem[] tmpStrSwipe = new SwipeItem[swipeItems.size()];
        tmpStrSwipe = swipeItems.toArray(tmpStrSwipe);
        menuSwipeSelector.setItems(tmpStrSwipe);
        menuSwipeSelector.selectItemAt(swipeItems.size() - 1);
        menuSwipeSelector.setOnItemSelectedListener(new OnSwipeItemSelectedListener() {
            @Override
            public void onItemSelected(SwipeItem item) {
                try {
                    Date date = sdf.parse(item.title);
                    dateSelected = date;
                    startLongOperation(STATUS_LONG_OPERATION_NONE);
                    Log.d("item Date ", String.valueOf(date.getTime()) + " "+ sdf.format(date));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
        headerTableTransaksis = transaksiPanenHelper.setHeaderTableQcMutuBuah();

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etSearch.getText().toString().isEmpty()){
                    new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_NONE));
                }else{
                    new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_SEARCH));
                }
            }
        });

        llNewData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(requireActivity(), QcMutuBuahActivity.class);
                startActivityForResult(intent,GlobalHelper.RESULT_QC_MUTU_BUAH);
                if (((BaseActivity) requireActivity()).alertDialogBase != null) {
                    ((BaseActivity) requireActivity()).alertDialogBase.cancel();
                }
            }
        });

        startLongOperation(STATUS_LONG_OPERATION_NONE);
    }

    private void main(){
        if (isVisibleToUser && getActivity() != null) {
            mainMenuActivity.ivLogo.setVisibility(View.VISIBLE);
            mainMenuActivity.btnFilter.setVisibility(View.GONE);
            startLongOperation(STATUS_LONG_OPERATION_NONE);
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }

    private void updateDataRv(int status){
        origin = new HashMap<>();
        listSearch = new android.support.v4.util.ArraySet<>();
        totalQcJanjang = 0;

        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_BUAH);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = Iterable.iterator(); iterator.hasNext();) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            QcMutuBuah qcMutuBuah = gson.fromJson(dataNitrit.getValueDataNitrit(),QcMutuBuah.class);
            if(qcMutuBuah.getTransaksiPanenQc() != null){
                TransaksiPanen transaksiPanen = qcMutuBuah.getTransaksiPanenQc();
                if (sdf.format(dateSelected).equals(sdf.format(new Date(transaksiPanen.getCreateDate())))) {
                    if (status == STATUS_LONG_OPERATION_SEARCH) {
                        if (transaksiPanen.getTph().getNamaTph().toLowerCase().contains(etSearch.getText().toString().toLowerCase()) ||
                                transaksiPanen.getTph().getBlock().toLowerCase().contains(etSearch.getText().toString().toLowerCase()) ||
                                String.valueOf(transaksiPanen.getHasilPanen().getJanjangNormal()).contains(etSearch.getText().toString().toLowerCase()) ||
                                String.valueOf(transaksiPanen.getRestan()).contains(etSearch.getText().toString().toLowerCase())) {
                            addRowTable(qcMutuBuah);
                        }
                    } else if (status == STATUS_LONG_OPERATION_NONE) {
                        addRowTable(qcMutuBuah);
                    }
                    listSearch.add(transaksiPanen.getTph().getNamaTph());
                    if(transaksiPanen.getPemanen() != null) {
                        listSearch.add(transaksiPanen.getPemanen().getNama());
                    }
                    listSearch.add(String.valueOf(transaksiPanen.getHasilPanen().getJanjangNormal()));
                    listSearch.add(String.valueOf(transaksiPanen.getRestan()));
                }
            }
        }
        db.close();

    }

    private void addRowTable(QcMutuBuah qcMutuBuah){
        origin.put(qcMutuBuah.getIdQCMutuBuah(), qcMutuBuah);
        totalQcJanjang += qcMutuBuah.getTransaksiPanenQc().getHasilPanen().getTotalJanjang();
    }

    private void updateUIRV(int status){
        List<String> lSearch = new ArrayList<>();
        if(listSearch.size() > 0){
            lSearch.addAll(listSearch);
        }

        if(mainMenuActivity != null) {
            ArrayAdapter<String> adapterSearch = new ArrayAdapter<String>(mainMenuActivity,
                    android.R.layout.simple_dropdown_item_1line, lSearch);
            etSearch.setAdapter(adapterSearch);
            etSearch.setThreshold(1);
            adapterSearch.notifyDataSetChanged();
        }

        if(status == STATUS_LONG_OPERATION_NONE){
            etSearch.setText("");
        }

        if(origin.size() > 0 ) {
            ArrayList<QcMutuBuah> list = new ArrayList<>(origin.values());

            Collections.sort(list, new Comparator<QcMutuBuah>() {
                @Override
                public int compare(QcMutuBuah o1, QcMutuBuah o2) {
                    return Long.compare(o2.getTransaksiPanenQc().getCreateDate(), o1.getTransaksiPanenQc().getCreateDate());
                }
            });

            ltableview.setVisibility(View.VISIBLE);
            mTableViewModel = new QcMutuBuahTableViewModel(getContext(), list,headerTableTransaksis);
            // Create TableView Adapter
            qcMutuBuahAdapter = new QcMutuBuahAdapter(getContext(), mTableViewModel);
            mTableView.setAdapter(qcMutuBuahAdapter);
            mTableView.setRowHeaderWidth(Utils.convertDpToPx(mainMenuActivity,100));
            mTableView.setTableViewListener(new ITableViewListener() {
                @Override
                public void onCellClicked(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

                }

                @Override
                public void onCellLongPressed(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

                }

                @Override
                public void onColumnHeaderClicked(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {
                    if (columnHeaderView != null && columnHeaderView instanceof ColumnHeaderViewHolder) {
                        // Create Long Press Popup
                        ColumnHeaderPopup popup = new ColumnHeaderPopup(
                                (ColumnHeaderViewHolder) columnHeaderView, mTableView);
                        // Show
                        popup.show();
                    }
                }

                @Override
                public void onColumnHeaderLongPressed(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {

                }

                @Override
                public void onRowHeaderClicked(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {
                    if (rowHeaderView != null && rowHeaderView instanceof QcMutuBuahRowHeaderViewHolder) {

                        String [] sid = ((QcMutuBuahRowHeaderViewHolder) rowHeaderView).getCellId().split("-");
                        qcMutuBuah = origin.get(sid[1]);
                        startLongOperation(STATUS_LONG_OPERATION_SELECT_ITEM);

                    }

                }

                @Override
                public void onRowHeaderLongPressed(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {

                }
            });
            qcMutuBuahAdapter.setAllItems(mTableViewModel.getColumnHeaderList(), mTableViewModel
                    .getRowHeaderList(), mTableViewModel.getCellList());


//            mTableFilter = new Filter(mTableView);
        }else{
            ltableview.setVisibility(View.GONE);
        }


        tvQcTph.setText(String.valueOf(origin.size()));
        tvQcJanjang.setText(String.valueOf(totalQcJanjang));

        Date d = new Date(System.currentTimeMillis());
        if(sdf.format(d).equals(sdf.format(dateSelected))){
            if(GlobalHelper.enableToNewTransaksi()){
                llNewData.setVisibility(View.VISIBLE);
            }else{
                llNewData.setVisibility(View.GONE);
            }
        }else{
            llNewData.setVisibility(View.GONE);
        }
    }

    public void startLongOperation(int status){
        new LongOperation().execute(String.valueOf(status));
    }

    private class LongOperation extends AsyncTask<String, Void, String> {
//        private AlertDialog alertDialogAllpoin ;
        String dataString;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (((BaseActivity) requireActivity()).alertDialogBase != null) {
                ((BaseActivity) requireActivity()).alertDialogBase.cancel();
            }
            ((BaseActivity) requireActivity()).alertDialogBase = WidgetHelper.showWaitingDialog(requireActivity(),getResources().getString(R.string.wait));
        }

        @Override
        protected String doInBackground(String... strings) {
            switch (Integer.parseInt(strings[0])){
                case STATUS_LONG_OPERATION_NONE:{
                    updateDataRv(Integer.parseInt(strings[0]));
                    break;
                }
                case STATUS_LONG_OPERATION_SEARCH:{
                    updateDataRv(Integer.parseInt(strings[0]));
                    break;
                }
                case STATUS_LONG_OPERATION_SELECT_ITEM:{
                    Gson gson = new Gson();
                    dataString = gson.toJson(qcMutuBuah);
                    break;
                }
            }
            return strings[0];
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            switch (Integer.parseInt(result)){
                case STATUS_LONG_OPERATION_NONE:{
                    updateUIRV(Integer.parseInt(result));

                    if (((BaseActivity) Objects.requireNonNull(requireActivity())).alertDialogBase != null) {
                        ((BaseActivity) requireActivity()).alertDialogBase.cancel();
                    }
                    break;
                }
                case STATUS_LONG_OPERATION_SEARCH:{
                    updateUIRV(Integer.parseInt(result));

                    if (((BaseActivity) requireActivity()).alertDialogBase != null) {
                        ((BaseActivity) requireActivity()).alertDialogBase.cancel();
                    }
                    break;
                }
                case STATUS_LONG_OPERATION_SELECT_ITEM:{
                    Intent intent = new Intent(getActivity(), QcMutuBuahActivity.class);
                    intent.putExtra("qcMutuBuah",dataString);
                    startActivityForResult(intent,GlobalHelper.RESULT_QC_MUTU_BUAH);
                    if (((BaseActivity) requireActivity()).alertDialogBase != null) {
                        ((BaseActivity) requireActivity()).alertDialogBase.cancel();
                    }
                    break;
                }
            }

        }
    }
}
