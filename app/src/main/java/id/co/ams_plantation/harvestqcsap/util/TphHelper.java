package id.co.ams_plantation.harvestqcsap.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.util.ArraySet;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.esri.android.map.Callout;
import com.esri.android.map.GraphicsLayer;
import com.esri.android.map.LocationDisplayManager;
import com.esri.android.map.MapView;
import com.esri.android.map.ags.ArcGISLocalTiledLayer;
import com.esri.android.map.event.OnPanListener;
import com.esri.android.map.event.OnStatusChangedListener;
import com.esri.android.map.event.OnZoomListener;
import com.esri.android.map.ogc.kml.KmlLayer;
import com.esri.core.geometry.GeometryEngine;
import com.esri.core.geometry.Point;
import com.esri.core.geometry.SpatialReference;
import com.esri.core.map.Graphic;
import com.esri.core.symbol.CompositeSymbol;
import com.esri.core.symbol.SimpleLineSymbol;
import com.esri.core.symbol.SimpleMarkerSymbol;
import com.google.gson.Gson;
import com.mikepenz.iconics.utils.Utils;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import co.ceryle.segmentedbutton.SegmentedButtonGroup;
import id.co.ams_plantation.harvestqcsap.HarvestApp;
import id.co.ams_plantation.harvestqcsap.ui.MapActivity;
import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.model.DataNitrit;
import id.co.ams_plantation.harvestqcsap.model.Estate;
import id.co.ams_plantation.harvestqcsap.model.NearestBlock;
import id.co.ams_plantation.harvestqcsap.model.StampImage;
import id.co.ams_plantation.harvestqcsap.model.TPH;
import id.co.ams_plantation.harvestqcsap.model.User;
import id.co.ams_plantation.harvestqcsap.ui.BaseActivity;
import pl.aprilapps.easyphotopicker.EasyImage;

/**
 * Created by user on 12/12/2018.
 */

public class TphHelper {

    public AlertDialog listrefrence;
    FragmentActivity activity;

    SlidingUpPanelLayout mLayout;
    MapView map;
    LocationDisplayManager locationDisplayManager;
    FloatingActionButton fabZoomToLocation;
    FloatingActionButton fabZoomToEstate;
    SegmentedButtonGroup segmentedButtonGroup;
    RelativeLayout layoutBottomSheet;
    ImageView ivCrosshair;
    LinearLayout ll_latlon;
    TextView tv_lat_manual;
    TextView tv_lon_manual;
    TextView tv_dis_manual;
    TextView tv_deg_manual;

    ImageView ivPhoto;
    ImageView imagecacnel;
    ImageView imagesview;
    RelativeLayout rl_ipf_takepicture;
    SliderLayout sliderLayout;

    LinearLayout lheader;
    LinearLayout btnShowForm;
    AppCompatEditText etNamaTph;
    EditText etEst;
    AppCompatEditText etAncak;
    CompleteTextViewHelper etBlock;
    CompleteTextViewHelper etAfd;
    //    ChipView cvAncak;
    Button btnSave;
//    BottomSheetBehavior sheetBehavior;
    View langsiran_new;
    View pks_new;

    GraphicsLayer graphicsLayer;
    GraphicsLayer graphicsLayerTPH;
    SpatialReference spatialReference;
    boolean isMapLoaded=false;

    ArrayList<File> ALselectedImage;

    ArcGISLocalTiledLayer tiledLayer;
    KmlLayer kmlLayer;
    String locKmlMap;

    boolean newData;
    BaseActivity baseActivity;
    MapActivity mapActivity;
    int idgraptemp = 0;
    boolean manual;

    public double latawal = 0.0;
    public double longawal = 0.0;
    public double latitude = 0.0;
    public double longitude = 0.0;

    public HashMap<Integer,TPH> hashpoint;
    public Callout callout;

    TPH selectedTph;
    Boolean popUpForm = false;
    AlertDialog alertDialog2;
    boolean isNewData;
    LinearLayout lAncak;
//    boolean awal = true;

    public TphHelper(FragmentActivity activity) {
        this.activity = activity;
    }

    //unutk deklarias dari map activity
    public void setupFormInput(View view,TPH tph){
        mapActivity = (MapActivity) activity;
        locKmlMap = mapActivity.locKmlMap;
        ALselectedImage= new ArrayList<>();
        selectedTph = tph;
        if(ivPhoto == null) {
            ivPhoto = (ImageView) view.findViewById(R.id.ivPhoto);
            imagecacnel = (ImageView) view.findViewById(R.id.imagecacnel);
            imagesview = (ImageView) view.findViewById(R.id.imagesview);
            rl_ipf_takepicture = (RelativeLayout) view.findViewById(R.id.rl_ipf_takepicture);
            lAncak = (LinearLayout) view.findViewById(R.id.lAncak);
            sliderLayout = (SliderLayout) view.findViewById(R.id.sliderLayout);
            etNamaTph = (AppCompatEditText) view.findViewById(R.id.etNamaTph);
            etEst = (EditText) view.findViewById(R.id.etEst);
            etAfd = (CompleteTextViewHelper) view.findViewById(R.id.etAfd);
            etAncak = (AppCompatEditText) view.findViewById(R.id.etAncak);
            etBlock = (CompleteTextViewHelper) view.findViewById(R.id.etBlock);
            btnSave = (Button) view.findViewById(R.id.btnSave);
        }else{
            if(tph == null) {
                setupimageslide();
                etNamaTph.setText("");
                etAncak.setText("");
            }
        }
        lAncak.setVisibility(View.GONE);
        btnSave.setVisibility(View.VISIBLE);

        if(tph != null) {
            isNewData = false;
            showValueForm(tph);
            if(GlobalHelper.distance(((BaseActivity) activity).currentlocation.getLatitude(),latitude,
                    ((BaseActivity) activity).currentlocation.getLongitude(),longitude) > GlobalHelper.RADIUS){
                btnSave.setVisibility(View.GONE);
            }
        }else{
            isNewData = true;
            Estate estate = tph == null ? GlobalHelper.getEstate() : GlobalHelper.getEstateByEstCode(tph.getEstCode());
            etEst.setText(estate.getEstName());
        }

        etBlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etBlock.showDropDown();
            }
        });
        etBlock.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    etBlock.showDropDown();
                }
            }
        });

        etBlock.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                etAfd.requestFocus();
            }
        });

        etAfd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etAfd.showDropDown();
            }
        });
        etAfd.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    etBlock.showDropDown();
                }
            }
        });
        etAfd.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(ALselectedImage.size() == 0){
                    GlobalHelper.TAG_CAMERA = GlobalHelper.TAG_CAMERA_NEW_TPH_MAP_ACTIVITY;
                    EasyImage.openCamera(activity,1);
                }
                GlobalHelper.hideKeyboard(activity);
            }
        });

        ivPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ALselectedImage.size() > 3) {
                    Toast.makeText(HarvestApp.getContext(),activity.getResources().getString(R.string.warning_take_foto),Toast.LENGTH_LONG).show();
                }else {
                    if(etBlock.getText().toString().isEmpty()){
                        etBlock.requestFocus();
                        Toast.makeText(HarvestApp.getContext(),activity.getResources().getString(R.string.please_chose_block),Toast.LENGTH_LONG).show();
                        return;
                    }

                    StampImage stampImage = new StampImage();
                    stampImage.setBlock(etBlock.getText().toString());

                    Gson gson  = new Gson();
                    SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.STAMP_IMAGES, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString(HarvestApp.STAMP_IMAGES,gson.toJson(stampImage));
                    editor.apply();

                    GlobalHelper.TAG_CAMERA = GlobalHelper.TAG_CAMERA_NEW_TPH_MAP_ACTIVITY;
                    EasyImage.openCamera(activity,1);
                }
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.STAMP_IMAGES, Context.MODE_PRIVATE);
//                String stringIsi = preferences.getString(HarvestApp.STAMP_IMAGES,null);
//                Gson gson  = new Gson();
//                StampImage stampImage = gson.fromJson(stringIsi,StampImage.class);
//                if(!stampImage.getBlock().equals(etBlock.getText().toString())){
//                    stampImage.setBlock(etBlock.getText().toString());
//                    SharedPreferences.Editor editor = preferences.edit();
//                    editor.putString(HarvestApp.STAMP_IMAGES,gson.toJson(stampImage));
//                    editor.apply();
//
//                    GlobalHelper.TAG_CAMERA = GlobalHelper.TAG_CAMERA_NEW_TPH_MAP_ACTIVITY;
//                    EasyImage.openCamera(activity,1);
//                    Toast.makeText(HarvestApp.getContext(),activity.getResources().getString(R.string.warning_take_foto2),Toast.LENGTH_LONG).show();
//                    return;
//                }
//
//                mapActivity.mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
//                mapActivity.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        new LongOperation().execute();
//                    }
//                });
            }
        });
    }

    public ArrayList<TPH> setDataTph(String Estate, String[] Block){
        ArrayList<TPH> tphArrayList = new ArrayList<>();
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABEL_TPH);
        if(db != null) {
            for(int i = 0 ; i < Block.length; i++) {
                ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
                Cursor<DataNitrit> tphIterable = repository.find(ObjectFilters.eq("param1", Block[i]));
                for (Iterator iterator = tphIterable.iterator(); iterator.hasNext(); ) {
                    DataNitrit dataNitrit = (DataNitrit) iterator.next();
                    Gson gson = new Gson();
                    TPH tph = gson.fromJson(dataNitrit.getValueDataNitrit(), TPH.class);
                    if ((tph.getStatus() != TPH.STATUS_TPH_DELETED && tph.getStatus() != TPH.STATUS_TPH_INACTIVE_QC_MOBILE)
                            && (tph.getCreateBy() != TPH.USER_GIS_SYSTEM) &&
                            (tph.getBlock().equals(Block[i])) && (tph.getEstCode().equals(Estate))) {
                        tphArrayList.add(tph);
                    }
                }
            }
            db.close();
        }
        return tphArrayList;
    }

    public ArrayList<TPH> setDataTph(String Estate, ArrayList<String> Block){
        ArrayList<TPH> tphArrayList = new ArrayList<>();
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABEL_TPH);
        if(db != null) {
            for(int i = 0 ; i < Block.size(); i++) {
                ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
                Cursor<DataNitrit> tphIterable = repository.find(ObjectFilters.eq("param1", Block.get(i)));
                for (Iterator iterator = tphIterable.iterator(); iterator.hasNext(); ) {
                    DataNitrit dataNitrit = (DataNitrit) iterator.next();
                    Gson gson = new Gson();
                    TPH tph = gson.fromJson(dataNitrit.getValueDataNitrit(), TPH.class);
                    if ((tph.getStatus() != TPH.STATUS_TPH_DELETED && tph.getStatus() != TPH.STATUS_TPH_INACTIVE_QC_MOBILE)
                            && (tph.getCreateBy() != TPH.USER_GIS_SYSTEM) &&
                            (tph.getBlock().equals(Block.get(i))) && (tph.getEstCode().equals(Estate))) {
                        tphArrayList.add(tph);
                    }
                }
            }
            db.close();
        }
        return tphArrayList;
    }

    public ArrayList<TPH> setDataTph(String Estate,String Block){
        ArrayList<TPH> tphArrayList = new ArrayList<>();
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABEL_TPH);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Cursor<DataNitrit> tphIterable = repository.find(ObjectFilters.eq("param1", Block));
        for (Iterator iterator = tphIterable.iterator(); iterator.hasNext();) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            TPH tph = gson.fromJson(dataNitrit.getValueDataNitrit(),TPH.class);
//            double radius = GlobalHelper.distance(latawal,tph.getLatitude(),
//                    longawal,tph.getLongitude());
            if ((tph.getStatus() != TPH.STATUS_TPH_DELETED && tph.getStatus() != TPH.STATUS_TPH_INACTIVE_QC_MOBILE)
                    && (tph.getCreateBy() != TPH.USER_GIS_SYSTEM) &&
                    (tph.getBlock().equals(Block)) && (tph.getEstCode().equals(Estate))) {
                tphArrayList.add(tph);
            }
        }
        db.close();
        return tphArrayList;
    }

    public TPH getTphBayangan(String Estate,String Block){
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABEL_TPH);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Cursor<DataNitrit> tphIterable = repository.find(ObjectFilters.eq("param1", Block));
        for (Iterator iterator = tphIterable.iterator(); iterator.hasNext();) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            TPH tph = gson.fromJson(dataNitrit.getValueDataNitrit(),TPH.class);
//            double radius = GlobalHelper.distance(latawal,tph.getLatitude(),
//                    longawal,tph.getLongitude());
            if ((tph.getCreateBy() == TPH.USER_GIS_SYSTEM) &&
                    (tph.getBlock().equals(Block)) && (tph.getEstCode().equals(Estate))) {
                db.close();
                return tph;
            }
        }
        db.close();
        return null;
    }

    public TPH cariTphTerdekat(ArrayList<TPH> alltph){
        double distance = 0.0;
        TPH selectTph = null;
        baseActivity = (BaseActivity) activity;
        if(locationDisplayManager != null){
            baseActivity.currentlocation = locationDisplayManager.getLocation();
        }
        for(int i = 0; i < alltph.size(); i++){
            if(i == 0){
                selectTph = alltph.get(i);
                distance = GlobalHelper.distance(baseActivity.currentlocation.getLatitude(),selectTph.getLatitude(),
                        baseActivity.currentlocation.getLongitude(),selectTph.getLongitude());
            }else{
                TPH selectTphX = alltph.get(i);
                double disX = GlobalHelper.distance(baseActivity.currentlocation.getLatitude(),selectTphX.getLatitude(),
                        baseActivity.currentlocation.getLongitude(),selectTphX.getLongitude());
                if(disX <= distance){
                    distance = disX;
                    selectTph = selectTphX;
                }

            }
        }
        return selectTph;
    }

    public NearestBlock getNearBlock(Location location){
        File kml = new File(GlobalHelper.decryptFiles(GlobalHelper.TYPE_KMLBLOCK));
        Set<String> allBlock = GlobalHelper.getNearBlock(kml, location.getLatitude(),location.getLongitude());
        Set<String> bloks = new ArraySet<>();
        String block = "";
        for (String est : allBlock) {
            try {
                String[] split = est.split(";");
                bloks.add(split[2]);
                block = split[2];
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            String [] estAfdBlock = GlobalHelper.getAndAssignBlock(kml,location.getLatitude(),location.getLongitude()).split(";");
            block = estAfdBlock[2];
        }catch (Exception e){
            e.printStackTrace();
        }

        return new NearestBlock(location,block,bloks);
    }

    public NearestBlock getNearBlock(Double latitude,Double longitude){
        File kml = new File(GlobalHelper.decryptFiles(GlobalHelper.TYPE_KMLBLOCK));
        Set<String> allBlock = GlobalHelper.getNearBlock(kml, latitude,longitude);
        Set<String> bloks = new ArraySet<>();
        String block = "";
        for (String est : allBlock) {
            try {
                String[] split = est.split(";");
                bloks.add(split[2]);
                block = split[2];
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            String [] estAfdBlock = GlobalHelper.getAndAssignBlock(kml,latitude,longitude).split(";");
            block = estAfdBlock[2];
        }catch (Exception e){
            e.printStackTrace();
        }

        return new NearestBlock(null,block,bloks);
    }

    public boolean saveTPH(TPH tph, boolean insert){
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABEL_TPH);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Gson gson = new Gson();
        DataNitrit dataNitrit = new DataNitrit(tph.getNoTph(),
                gson.toJson(tph),
                tph.getBlock(),
                tph.getAfdeling(),
                String.valueOf(tph.getCreateBy()));
        if(insert) {
            repository.insert(dataNitrit);
        }else{
            repository.update(dataNitrit);
        }


        db.close();
        return true;
    }

    public void showValueForm(TPH tph){
        Gson gson = new Gson();
        Log.d(this.getClass()+" "+ "showValueForm", gson.toJson(tph));

        //unutk tampilanpeta sudah di handle di map activity
        if(mapActivity == null) {
            graphicsLayer.removeAll();
            switchCrossHairUI(false);
            zoomToLocation(latitude, longitude, spatialReference);
            mLayout.setPanelState(SlidingUpPanelLayout.PanelState.ANCHORED);
        }

        String est = "";
        for(Estate estate : GlobalHelper.getUser().getEstates()){
            if(estate.getEstCode().equalsIgnoreCase(tph.getEstCode())){
                est = estate.getEstName();
            }
        }

//        sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        etNamaTph.setText(tph.getNamaTph());
        etEst.setText(est);
        etAfd.setText(tph.getAfdeling());
        etAncak.setText(tph.getAncak());
        etBlock.setText(tph.getBlock());

//nanti muncul
//        etEst.setFocusable(false);
//        etAfd.setFocusable(false);
//        etBlock.setFocusable(false);

        longitude = tph.getLongitude();
        latitude = tph.getLatitude();

        ALselectedImage.clear();
        if(tph.getFoto() != null) {
            if (tph.getFoto().size() > 0) {
                for (String s : tph.getFoto()) {
                    File file = new File(s);
                    ALselectedImage.add(file);
                }
            }
        }
        setupimageslide();

        double radius = GlobalHelper.distance(latitude,latawal,longitude,longawal);
        if(radius > GlobalHelper.RADIUS){
            btnSave.setVisibility(View.GONE);
        }else{
            btnSave.setVisibility(View.VISIBLE);
        }
    }

    private void addGraphicTph(TPH tph){
        Point fromPoint = new Point(tph.getLongitude(), tph.getLatitude());
        Point toPoint = (Point) GeometryEngine.project(fromPoint, SpatialReference.create(SpatialReference.WKID_WGS84), spatialReference);

        CompositeSymbol cms = new CompositeSymbol();
        SimpleMarkerSymbol sms = new SimpleMarkerSymbol(activity.getResources().getColor(R.color.Blue), Utils.convertDpToPx(activity, GlobalHelper.MARKER_SIZE), SimpleMarkerSymbol.STYLE.CIRCLE);
        SimpleLineSymbol sls = new SimpleLineSymbol(activity.getResources().getColor(R.color.Blue), GlobalHelper.GARISTEPI_SIZE, SimpleLineSymbol.STYLE.SOLID);
        sms.setOutline(sls);
        cms.add(sms);
        Graphic pointGraphic = new Graphic(toPoint, cms);
        int idgrap = graphicsLayerTPH.addGraphic(pointGraphic);
        hashpoint.put(idgrap,tph);
    }

//    private void defineTapSetup(float x,float y){
//        List<TPH> tphList = new ArrayList<>();
//        int[] graphicIDs = graphicsLayerTPH.getGraphicIDs(x,y,Utils.convertDpToPx(activity,GlobalHelper.MARKER_TOUCH_SIZE));
//
//        if(graphicIDs.length > 0) {
//            for(int gid : graphicIDs) {
//                for (HashMap.Entry<Integer, TPH> entry : hashpoint.entrySet()) {
//                    if(gid == entry.getKey()){
//                        tphList.add(entry.getValue());
//                    }
//                }
//            }
//        }
//
//        calloutSetup();
//        if (callout.isShowing()) {
//            callout.hide();
//        } else {
//            if (graphicIDs.length == 1) {
//                //jika dalam satu tap hanya ada satu titik maka kesini
//                int idx = -1;
//                String graptype = "";
//                Graphic graphic = graphicsLayerTPH.getGraphic(graphicIDs[0]);
//                if (graphic.getGeometry() instanceof Point) {
//                    graptype = "point";
//                    idx = graphicIDs[0];
//                }
//                showInfoWindow(idx,tphList.get(0));
//            }else if (graphicIDs.length > 1){
//                showChoseInfoWindow(graphicIDs, graphicsLayerTPH, tphList);
//            }
//        }
//    }

    private void showChoseInfoWindow(int[] graphicIDs,GraphicsLayer gl,List<TPH> tphList){
        ArrayList<TPH> windowChoseArrayList = new ArrayList<>();

        for (int i = 0; i < graphicIDs.length; i++) {
            Graphic graphic = gl.getGraphic(graphicIDs[i]);
            int idx =0;
            if (graphic.getGeometry() instanceof Point) {
                idx = graphicIDs[i];
            }

            if(idx != 0){
                for (HashMap.Entry<Integer, TPH> entry : hashpoint.entrySet()) {
                    if(idx == entry.getKey()){
                        windowChoseArrayList.add(entry.getValue());
                    }
                }
            }
        }

        Collections.sort(windowChoseArrayList, new Comparator<TPH>() {
            @Override
            public int compare(TPH o1, TPH o2) {
                return o1.getNamaTph().compareTo(o2.getNamaTph());
            }
        });

        if(windowChoseArrayList.size()>1){
            ViewGroup content = InfoWindowChose.setupInfoWindowsChose(activity, windowChoseArrayList);
            callout.setContent(content);
            Graphic graphic = gl.getGraphic(graphicIDs[graphicIDs.length -1]);
            String graptype = "";
            int idx =0;
            if (graphic.getGeometry() instanceof Point) {
                graptype = "point";
                idx = graphicIDs[graphicIDs.length -1];
            }

            Point realpoin = (Point) gl.getGraphic(idx).getGeometry();

            Point wgs84poin = (Point) GeometryEngine.project(realpoin,
                    gl.getSpatialReference(),
                    SpatialReference.create(SpatialReference.WKID_WGS84));
            Point mapPoint = (Point) GeometryEngine.project(wgs84poin,
                    SpatialReference.create(SpatialReference.WKID_WGS84),
                    map.getSpatialReference());
            callout.setCoordinates(mapPoint);
            map.centerAt(mapPoint,true);
            callout.animatedShow(mapPoint,content);
        }else if (windowChoseArrayList.size() == 1){
            showInfoWindow(graphicIDs[0],windowChoseArrayList.get(0));
        }
    }


    private void calloutSetup() {
        this.callout = map.getCallout();
        this.callout.setStyle(R.xml.statisticspop);
        if (this.callout.isShowing()) {
            this.callout.hide();
        }
    }

    public void showInfoWindow(int idx,TPH tph){
        ViewGroup content = InfoWindow.setupInfoWindows(activity, tph);
        callout.setContent(content);
        Point realpoin = (Point) graphicsLayerTPH.getGraphic(idx).getGeometry();

        if(realpoin!= null) {
            Point wgs84poin = (Point) GeometryEngine.project(realpoin,
                    graphicsLayerTPH.getSpatialReference(),
                    SpatialReference.create(SpatialReference.WKID_WGS84));
            Point mapPoint = (Point) GeometryEngine.project(wgs84poin,
                    SpatialReference.create(SpatialReference.WKID_WGS84),
                    map.getSpatialReference());
            callout.setCoordinates(mapPoint);
            map.centerAt(mapPoint,true);
            callout.animatedShow(mapPoint,content);
        }
    }

    public void setImages(File images,Context context){
//        Glide.with(context)
//                .load(images)
//                .asBitmap()
//                .error(ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_camera_alt, null))
//                .centerCrop();
        ALselectedImage.add(images);
        setupimageslide();
    }

    private void setupimageslide(){
        Set<File> sf = new HashSet<>();
        sf.addAll(ALselectedImage);
        ALselectedImage.clear();
        ALselectedImage.addAll(sf);

        if(ALselectedImage.size() > 3){
            ivPhoto.setVisibility(View.GONE);
        }else{
            ivPhoto.setVisibility(View.VISIBLE);
        }
        if(ALselectedImage.size() != 0){
            imagecacnel.setVisibility(View.VISIBLE);
            imagesview.setVisibility(View.VISIBLE);
            sliderLayout.setVisibility(View.VISIBLE);
        }else{
            imagecacnel.setVisibility(View.GONE);
            imagesview.setVisibility(View.GONE);
            sliderLayout.setVisibility(View.GONE);
        }
        sliderLayout.removeAllSliders();
        for(File file : ALselectedImage){
            TextSliderView textSliderView = new TextSliderView(activity);
            // initialize a SliderLayout
            if(file.toString().startsWith("http:")){
                String surl = file.toString();
                if(file.toString().startsWith("http://")){

                }else if(file.toString().startsWith("http:/")){
                    surl =surl.replace("http:/","http://");
                }

                textSliderView
                        .image(GlobalHelper.setUrlFoto(surl))
                        .setScaleType(BaseSliderView.ScaleType.Fit);
            }else {
                textSliderView
                        .image(file)
                        .setScaleType(BaseSliderView.ScaleType.Fit);
            }
            sliderLayout.addSlider(textSliderView);
        }
        sliderLayout.stopAutoCycle();
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.FlipHorizontal);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setCustomAnimation(new DescriptionAnimation());
        imagecacnel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(GSApp.getContext(),"slider getCurrentPosition " + sliderLayout.getCurrentPosition(),Toast.LENGTH_SHORT).show();
                try {
                    if (sliderLayout.getCurrentPosition() == 0 && ALselectedImage.size() == 1) {
                        ALselectedImage.clear();
                        sliderLayout.removeAllSliders();
                        sliderLayout.setVisibility(View.GONE);
                        imagecacnel.setVisibility(View.GONE);
                        imagesview.setVisibility(View.GONE);
                    } else {
                        ALselectedImage.remove(sliderLayout.getCurrentPosition());
                        sliderLayout.removeSliderAt(sliderLayout.getCurrentPosition());
                    }
                }catch (Exception e){
                    Log.e("sliderLayout", e.toString());
                }
            }
        });
        imagesview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WidgetHelper.showImagesZoom(activity,ALselectedImage);
            }
        });
    }

    private ArrayList<String> getListEstate(User user, Estate estate){
        ArrayList<String> estateArrayList = new ArrayList<>();
        for(Estate estate1:user.getEstates()) {
            if(estate1.getCompanyShortName().equalsIgnoreCase(estate.getCompanyShortName())){
                estateArrayList.add(estate1.getEstName());
            }
        }
        return estateArrayList;
    }

    private void mapSetup(){
        //
        map.removeAll();
        graphicsLayer.removeAll();
        String locTiledMap = GlobalHelper.decryptFiles(GlobalHelper.TYPE_VKM);
        tiledLayer = new ArcGISLocalTiledLayer(locTiledMap,true);
        tiledLayer.setRenderNativeResolution(true);
        tiledLayer.setMinScale(70000d);
        tiledLayer.setMaxScale(1000d);
        map.addLayer(tiledLayer);

        locKmlMap = GlobalHelper.decryptFiles(GlobalHelper.TYPE_KMLBLOCK);

        kmlLayer = new KmlLayer(locKmlMap);
        kmlLayer.setOpacity(0.3f);
        map.addLayer(kmlLayer);

        map.setOnStatusChangedListener(new OnStatusChangedListener() {
            @Override
            public void onStatusChanged(Object o, STATUS status) {
                if(o==map && status==STATUS.INITIALIZED){
                    spatialReference = map.getSpatialReference();
                    isMapLoaded = true;
                    fabZoomToEstate.callOnClick();
                }
            }
        });
        map.setOnPanListener(new OnPanListener() {
            @Override
            public void prePointerMove(float v, float v1, float v2, float v3) {
                if(map.isLoaded()){
                    if(map.getSpatialReference()!=null){
                        Point prePoint = map.getCenter();
                        Point point = (Point) GeometryEngine.project(prePoint,map.getSpatialReference(), SpatialReference.create(SpatialReference.WKID_WGS84));
                    }
                }
            }

            @Override
            public void postPointerMove(float v, float v1, float v2, float v3) {
                if(map.isLoaded()){
                    if(map.getSpatialReference()!=null){
                        Point prePoint = map.getCenter();
                        Point point = (Point) GeometryEngine.project(prePoint,map.getSpatialReference(),SpatialReference.create(SpatialReference.WKID_WGS84));
                        double imeter = GlobalHelper.distance(latawal,point.getY(),longawal,point.getX());
                        double bearing = GlobalHelper.bearing(latawal,point.getY(),longawal,point.getX());
                        tv_lat_manual.setText(String.format("%.5f",point.getY()));
                        tv_lon_manual.setText(String.format("%.5f",point.getX()));
                        tv_dis_manual.setText(String.format("%.0f m",imeter));
                        tv_deg_manual.setText(String.format("%.0f",bearing));
                        if(point.getY() != 0.0 && point.getX() != 0.0) {
                            if (imeter <= GlobalHelper.RADIUS) {
                                latitude = point.getY();
                                longitude = point.getX();
                                ll_latlon.setBackgroundColor(Color.WHITE);
                            } else {
                                ll_latlon.setBackgroundColor(Color.RED);
                            }
                        }
                    }

                }
            }

            @Override
            public void prePointerUp(float v, float v1, float v2, float v3) {

            }

            @Override
            public void postPointerUp(float v, float v1, float v2, float v3) {

            }
        });
        map.setOnZoomListener(new OnZoomListener() {
            @Override
            public void preAction(float v, float v1, double v2) {
                if(map.isLoaded()){
                    if(map.getSpatialReference()!=null){
                        Log.e("zoom preAction",v+ " ; "+ v1+" ; "+v2);
                        Log.e("zoom scale",String.format("%.0f",map.getScale()));
                        int scale = (int) map.getScale();
                        int zoomlvl = GlobalHelper.getZoomlvl(map.getScale());
                        Log.v("zoom lvl",String.format(" 1 : %,dm Zl : %,d",scale,zoomlvl));
                        map.getSpatialReference();
                    }

                }
            }

            @Override
            public void postAction(float v, float v1, double v2) {
                if(map.isLoaded()){
                    if(map.getSpatialReference()!=null){
                        Log.e("zoom postAction",v+ " ; "+ v1+" ; "+v2);
                        int scale = (int) map.getScale();
                        int zoomlvl = GlobalHelper.getZoomlvl(map.getScale());
                        Log.v("zoom lvl",String.format(" 1 : %,dm Zl : %,d",scale,zoomlvl));
                    }

                }
            }
        });

        graphicsLayer.setMinScale(70000d);
        graphicsLayer.setMaxScale(1000d);
        graphicsLayerTPH.setMinScale(70000d);
        graphicsLayerTPH.setMaxScale(1000d);
        map.addLayer(graphicsLayer);
        map.addLayer(graphicsLayerTPH);
        map.invalidate();
    }

    public void pointTPHSetup(String Estate,String Block){
        graphicsLayerTPH.removeAll();
        hashpoint = new HashMap<>();

        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABEL_TPH);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Cursor<DataNitrit> tphIterable = repository.find(ObjectFilters.eq("param1", Block));
        for (Iterator iterator = tphIterable.iterator(); iterator.hasNext();) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            TPH tph = gson.fromJson(dataNitrit.getValueDataNitrit(),TPH.class);
//            double radius = GlobalHelper.distance(latawal,tph.getLatitude(),
//                    longawal,tph.getLongitude());
            if ((tph.getStatus() != TPH.STATUS_TPH_DELETED && tph.getStatus() != TPH.STATUS_TPH_INACTIVE_QC_MOBILE)
                    && (tph.getCreateBy() != TPH.USER_GIS_SYSTEM)) {
                addGraphicTph(tph);
            }
        }

        db.close();
    }

    public void setupBlokAfdeling(Double latitude,Double longitude){
        MapActivity.LogIntervalStep("addGrapTemp 1 start");
        Set<String> allBlockAfdeling = GlobalHelper.getNearBlock(new File(locKmlMap),latitude,longitude);
        MapActivity.LogIntervalStep("addGrapTemp 1 getNearBlock");

        Set<String> allBlock = new ArraySet<>();
        Set<String> allAfdeling = new ArraySet<>();

        MapActivity.LogIntervalStep("addGrapTemp 1 getDataBlockAfd");
        for(String s : allBlockAfdeling){
            try {
                String [] split = s.split(";");
                allAfdeling.add(split[1]);
                allBlock.add(split[2]);
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        MapActivity.LogIntervalStep("addGrapTemp 1 setUpBlockAfd");
        ArrayAdapter<String> adapterBlock = new ArrayAdapter<String>
                (activity,android.R.layout.select_dialog_item, allBlock.toArray(new String[allBlock.size()]));
        etBlock.setThreshold(1);
        etBlock.setAdapter(adapterBlock);

        ArrayAdapter<String> adapterAfdeling = new ArrayAdapter<String>
                (activity,android.R.layout.select_dialog_item, allAfdeling.toArray(new String[allAfdeling.size()]));
        adapterAfdeling.notifyDataSetChanged();
        etAfd.setThreshold(1);
        etAfd.setAdapter(adapterAfdeling);

        MapActivity.LogIntervalStep("addGrapTemp 1 setUpBlockAfdFinish");
        try {

            MapActivity.LogIntervalStep("addGrapTemp 1 setValue");
            String [] blockAfdeling = GlobalHelper.getAndAssignBlock(new File(locKmlMap),latitude,longitude).split(";");
            if(selectedTph == null){
                etBlock.setText(blockAfdeling[2]);
                etAfd.setText(blockAfdeling[1]);
            }

            MapActivity.LogIntervalStep("addGrapTemp 1 finish Value");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void switchCrossHairUI(boolean visible){
        if(visible){
            ivCrosshair.setVisibility(View.VISIBLE);
            ll_latlon.setVisibility(View.VISIBLE);

            RelativeLayout.LayoutParams params= (RelativeLayout.LayoutParams) ll_latlon.getLayoutParams();
            params.addRule(RelativeLayout.BELOW, R.id.iv_crosshair);
            //ll_latlon.setLayoutParams(params);
        }else{
            ivCrosshair.setVisibility(View.GONE);
            ll_latlon.setVisibility(View.GONE);
        }
    }

    private void zoomToLocation(double lat, double lon, SpatialReference spatialReference) {
        Point mapPoint = GeometryEngine.project(lon,lat,spatialReference);
        map.centerAt(mapPoint,true);
        map.zoomTo(mapPoint,15000f);
    }

    public static String getAfdeling(String Estate,String Block){
        String afdeling = "";
        ArrayList<TPH> tphArrayList = new ArrayList<>();
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABEL_TPH);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Cursor<DataNitrit> tphIterable = repository.find(ObjectFilters.eq("param1", Block));
        for (Iterator iterator = tphIterable.iterator(); iterator.hasNext();) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            TPH tph = gson.fromJson(dataNitrit.getValueDataNitrit(),TPH.class);
//            double radius = GlobalHelper.distance(latawal,tph.getLatitude(),
//                    longawal,tph.getLongitude());
            if ((tph.getStatus() != TPH.STATUS_TPH_DELETED && tph.getStatus() != TPH.STATUS_TPH_INACTIVE_QC_MOBILE)
                    && (tph.getCreateBy() != TPH.USER_GIS_SYSTEM) &&
                    (tph.getBlock().equals(Block)) && (tph.getEstCode().equals(Estate))) {
                if(tph.getAfdeling() != null){
                    afdeling = tph.getAfdeling();
                    break;
                }
            }
        }
        db.close();
        return afdeling;
    }
}
