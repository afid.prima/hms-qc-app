package id.co.ams_plantation.harvestqcsap.TableView.holder;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder;

import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.TableView.model.TableViewCell;
import id.co.ams_plantation.harvestqcsap.model.SessionQcFotoPanen;

/**
 * Created on : 26,October,2021
 * Author     : Afid
 */

public class SessionQcFotoPanenCellViewHolder extends AbstractViewHolder {

    public final TextView cell_textview;
    public final LinearLayout cell_container;
    private TableViewCell cell;

    public SessionQcFotoPanenCellViewHolder(View itemView) {
        super(itemView);
        cell_textview = (TextView) itemView.findViewById(R.id.cell_data);
        cell_container = (LinearLayout) itemView.findViewById(R.id.cell_container);
    }

    public void setCell(TableViewCell cell) {
        this.cell = cell;
        cell_textview.setText(String.valueOf(cell.getData()));
        // If your TableView should have auto resize for cells & columns.
        // Then you should consider the below lines. Otherwise, you can ignore them.

        // It is necessary to remeasure itself.
        cell_container.getLayoutParams().width = LinearLayout.LayoutParams.WRAP_CONTENT;

        String[] id = cell.getId().split("-");
        switch (Integer.parseInt(id[2])) {
            case SessionQcFotoPanen.Status_OutStanding_Qc:
                cell_container.setBackgroundColor(SessionQcFotoPanen.COLOR_OutStanding);
                break;
            case SessionQcFotoPanen.Status_Done_Qc:
                cell_container.setBackgroundColor(SessionQcFotoPanen.COLOR_Done);
                break;
            case SessionQcFotoPanen.Status_Upload_Qc:
                cell_container.setBackgroundColor(SessionQcFotoPanen.COLOR_Upload);
                break;
        }

        cell_textview.requestLayout();
    }
}