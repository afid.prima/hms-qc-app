package id.co.ams_plantation.harvestqcsap.util;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Locale;

import id.co.ams_plantation.harvestqcsap.R;

/**
 * Created by user on 11/22/2018.
 */

public class NfcHelper {

    public static final int NFC_TIDAK_BISA_WRITE = 999;

    public static String valueNFC ;
    public static int stat = NFC_TIDAK_BISA_WRITE;
    static AlertDialog alertDialog = null;

    public static void showNFCTap(FragmentActivity activity,int statParam){
        View view = LayoutInflater.from(activity).inflate(R.layout.nfc_tap,null);
        Button btnClose = view.findViewById(R.id.btnClose);
        stat = statParam;
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stat = NFC_TIDAK_BISA_WRITE;
                alertDialog.dismiss();
            }
        });

        alertDialog = WidgetHelper.showFormDialog(alertDialog,view,activity);
    }

    /******************************************************************************
     **********************************Write to NFC Tag****************************
     ******************************************************************************/
    public static boolean write(String text, Tag tag,boolean isDismiss) {
        NdefRecord[] records = new NdefRecord[0];
        records = new NdefRecord[]{ createRecord(text) };

        if(records == null){
            return false;
        }
        NdefMessage message = new NdefMessage(records);
        // Get an instance of Ndef for the tag.
        Ndef ndef = Ndef.get(tag);
        // Enable I/O
        try {
            ndef.connect();
            // Write the message
            ndef.writeNdefMessage(message);
            // Close the connection
            ndef.close();

            if(isDismiss) {
                if (alertDialog != null) {
                    if (alertDialog.isShowing()) {
                        alertDialog.dismiss();
                    }
                }
            }
            stat = NFC_TIDAK_BISA_WRITE;
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } catch (FormatException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e){
            e.printStackTrace();;
            return false;
        }
    }

    public static NdefRecord createRecord(String text)  {
        String lang       = "en";
        byte[] textBytes  = text.getBytes();
        byte[] langBytes  = new byte[0];
        try {
            langBytes = lang.getBytes("US-ASCII");
            int    langLength = langBytes.length;
            int    textLength = textBytes.length;
            byte[] payload    = new byte[1 + langLength + textLength];

            // set status byte (see NDEF spec for actual bits)
            payload[0] = (byte) langLength;

            // copy langbytes and textbytes into payload
            System.arraycopy(langBytes, 0, payload, 1,              langLength);
            System.arraycopy(textBytes, 0, payload, 1 + langLength, textLength);

            NdefRecord recordNFC = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,  NdefRecord.RTD_TEXT,  new byte[0], payload);

            return recordNFC;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Boolean writeNewFormat(String text, Tag tag)  {
        if (tag != null) {
            try {
                NdefMessage message = createTextMessage(text);
                Ndef ndefTag = Ndef.get(tag);

                if (ndefTag == null) {
                    // Let's try to format the Tag in NDEF
                    NdefFormatable nForm = NdefFormatable.get(tag);
                    if (nForm != null) {
                        nForm.connect();
                        nForm.format(message);
                        nForm.close();
                    }
                }
                else {
                    ndefTag.connect();
                    ndefTag.writeNdefMessage(message);
                    ndefTag.close();
                }
                return true;
            }
            catch(Exception e) {
                e.printStackTrace();
                return false;
            }
        }
        return false;
    }

    public static NdefMessage createTextMessage(String content) {
        try {
            // Get UTF-8 byte
            byte[] lang = Locale.getDefault().getLanguage().getBytes("UTF-8");
            byte[] text = content.getBytes("UTF-8"); // Content in UTF-8

            int langSize = lang.length;
            int textLength = text.length;

            ByteArrayOutputStream payload = new ByteArrayOutputStream(1 + langSize + textLength);
            payload.write((byte) (langSize & 0x1F));
            payload.write(lang, 0, langSize);
            payload.write(text, 0, textLength);
            NdefRecord record = new NdefRecord(NdefRecord.TNF_WELL_KNOWN, NdefRecord.RTD_TEXT, new byte[0], payload.toByteArray());
            return new NdefMessage(new NdefRecord[]{record});
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /******************************************************************************
     **********************************Read From NFC Tag***************************
     ******************************************************************************/
    public static String readFromIntent(Intent intent) {
        String action = intent.getAction();
        String nfcCardData = "Tidak Ada NFC";
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            NdefMessage[] msgs = null;
            if (rawMsgs != null) {
                msgs = new NdefMessage[rawMsgs.length];
                for (int i = 0; i < rawMsgs.length; i++) {
                    msgs[i] = (NdefMessage) rawMsgs[i];
                }
            }
            nfcCardData = buildTagViews(msgs);
        }
        return nfcCardData;
    }

    private static String buildTagViews(NdefMessage[] msgs) {
        if (msgs == null || msgs.length == 0) return "";

        String values = "";
        try {
            String text = "";
//        String tagId = new String(msgs[0].getRecords()[0].getType());
            byte[] payload = msgs[0].getRecords()[0].getPayload();
            String textEncoding = ((payload[0] & 128) == 0) ? "UTF-8" : "UTF-16"; // Get the Text Encoding
            int languageCodeLength = payload[0] & 0063; // Get the Language Code, e.g. "en"
            // String languageCode = new String(payload, 1, languageCodeLength, "US-ASCII");

            // Get the Text
            text = new String(payload, languageCodeLength + 1, payload.length - languageCodeLength - 1, textEncoding);
            values = GlobalHelper.decompress(text);
        } catch (UnsupportedEncodingException e) {
            Log.e("UnsupportedEncoding", e.toString());
        }catch (IndexOutOfBoundsException e){
            Log.e("UnsupportedEncoding", e.toString());
        }

        return values;
    }

    /******************************************************************************
     **********************************Enable Write********************************
     ******************************************************************************/
    public static void WriteModeOn(NfcAdapter nfcAdapter,Activity activity,PendingIntent pendingIntent,IntentFilter writeTagFilters[]){
       if(nfcAdapter != null) {
           nfcAdapter.enableForegroundDispatch(activity, pendingIntent, writeTagFilters, null);
       }
    }
    /******************************************************************************
     **********************************Disable Write*******************************
     ******************************************************************************/
    public static void WriteModeOff(NfcAdapter nfcAdapter, Activity activity){
        if(nfcAdapter != null) {nfcAdapter.disableForegroundDispatch(activity);}
    }

    public static int cekFormatNFC(String isiNFC){
        try {
//            if(isiNFC.length() == 5) {
                JSONObject object = new JSONObject(isiNFC);
                if (object.getInt("z") == GlobalHelper.TYPE_NFC_BIRU) {
                    return GlobalHelper.TYPE_NFC_BIRU;
                } else if (object.getInt("z") == GlobalHelper.TYPE_NFC_HIJAU) {
                    return GlobalHelper.TYPE_NFC_HIJAU;
                }
//            }
            return GlobalHelper.TYPE_NFC_SALAH;
        } catch (JSONException e) {
            e.printStackTrace();
            return GlobalHelper.TYPE_NFC_SALAH;
        }
    }
}
