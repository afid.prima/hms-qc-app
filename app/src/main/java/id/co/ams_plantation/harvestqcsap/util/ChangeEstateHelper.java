package id.co.ams_plantation.harvestqcsap.util;

import static id.co.ams_plantation.harvestqcsap.util.GlobalHelper.ESTATE_FULL_NAME;
import static id.co.ams_plantation.harvestqcsap.util.GlobalHelper.getDatabasePath;
import static id.co.ams_plantation.harvestqcsap.util.GlobalHelper.getFileContent;
import static id.co.ams_plantation.harvestqcsap.util.GlobalHelper.isFileContentAvailable;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;

import net.rehacktive.waspdb.WaspDb;
import net.rehacktive.waspdb.WaspFactory;
import net.rehacktive.waspdb.WaspHash;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

import id.co.ams_plantation.harvestqcsap.HarvestApp;
import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.adapter.EstateAdapter;
import id.co.ams_plantation.harvestqcsap.encryptor.Encrypts;
import id.co.ams_plantation.harvestqcsap.model.Estate;
import id.co.ams_plantation.harvestqcsap.model.EstateFullName;
import id.co.ams_plantation.harvestqcsap.ui.ActiveActivity;
import id.co.ams_plantation.harvestqcsap.ui.EntryActivity;
import id.co.ams_plantation.harvestqcsap.ui.MainMenuActivity;
import jp.wasabeef.recyclerview.adapters.SlideInLeftAnimationAdapter;

/**
 * Created on : 06,November,2021
 * Author     : Afid
 */

public class ChangeEstateHelper {
    Context context;
    AlertDialog dialog;

    public ChangeEstateHelper(Context context) {
        this.context = context;
    }

    public void changeEstate(ArrayList<Estate> estates){
        final  ArrayList<String> arrayListCompany = new ArrayList<>();
        final ArrayList<Estate> estateArrayList = new ArrayList<>();
        View view = LayoutInflater.from(context).inflate(R.layout.change_estate_list_dialog,null);
        EditText etSearch = (EditText) view.findViewById(R.id.et_search_estate);
        LinearLayout btnChangeCompany = (LinearLayout) view.findViewById(R.id.btn_select_company);
        ImageView ivChangeCompany = (ImageView) view.findViewById(R.id.iv_select_company);
        final TextView tvChangeCompany = (TextView) view.findViewById(R.id.tv_select_company);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.rv_estate);
        tvChangeCompany.setText("SEMUA PT");
        String currComp="";
        arrayListCompany.add("SEMUA PT");
        btnChangeCompany.setVisibility(View.GONE);

        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        final EstateAdapter adapter = new EstateAdapter(context,estates);
        SlideInLeftAnimationAdapter adapterLeft = new SlideInLeftAnimationAdapter(adapter);
        adapterLeft.setFirstOnly(false);
//        adapterLeft.setInterpolator(new OvershootInterpolator(1f));
        adapterLeft.setDuration(400);
        recyclerView.setAdapter(adapterLeft);
//        final PopupMenu popupMenu = new PopupMenu(context,btnChangeCompany);
//        for(String company:arrayListCompany){
//            popupMenu.getMenu().add(company);
//        }
//        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(MenuItem item) {
////                popupMenu.dismiss();
//                ArrayList<Estate> tmpEstates = new ArrayList<Estate>();
//                String companyCode = "ALL";
//                if(!item.getTitle().toString().toLowerCase().equals("semua pt")){
//                    companyCode = item.getTitle().toString().split(" - ")[0];
//                }
//                for(Estate estate:estateArrayList){
//                    if(estate.getCompanyShortName().equals(companyCode)) {
//                        tmpEstates.add(estate);
//                    }
//                }
//                if(!companyCode.equals("ALL")){
//                    adapter.setData(tmpEstates);
//                }else{
//                    adapter.setData(estateArrayList);
//                }
//
//                tvChangeCompany.setText(item.getTitle());
//                return true;
//            }
//        });
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s.toString());
            }
        });
        ivChangeCompany.setImageDrawable(new IconicsDrawable(context)
                .icon(MaterialDesignIconic.Icon.gmi_city_alt)
                .colorRes(R.color.Gray));
//        btnChangeCompany.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                popupMenu.show();
//            }
//        });
        dialog = new AlertDialog.Builder(context,R.style.MyAlertDialogStyle)
                .setTitle("Change Estate")
                .setView(view).create();
        dialog.show();
    }

    public void setEstate(Estate estate){
        final WaspDb db = WaspFactory.openOrCreateDatabase(getDatabasePath(),
                GlobalHelper.DB_MASTER,
                GlobalHelper.getDatabasePass());
        final WaspHash estateHash = db.openOrCreateHash(GlobalHelper.SELECTED_ESTATE);
        Gson gson = new Gson();
        estateHash.put(GlobalHelper.SELECTED_ESTATE,estate);
        GlobalHelper.generateUserFiles(context,GlobalHelper.SELECTED_ESTATE,Encrypts.encrypt(gson.toJson(
                estate)));

        Intent intent = new Intent(HarvestApp.getContext(), EntryActivity.class);
        int pendingIntentId = 123;
        PendingIntent pi = PendingIntent.getActivity(HarvestApp.getContext(),
                pendingIntentId,intent,PendingIntent.FLAG_CANCEL_CURRENT);
        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.O){
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            ((MainMenuActivity)context).startActivity(intent);
        }else{
            AlarmManager manager = (AlarmManager) HarvestApp.getContext().getSystemService(Context.ALARM_SERVICE);
            manager.set(AlarmManager.RTC,System.currentTimeMillis()+1000,pi);
            ((MainMenuActivity)context).finish();
        }
    }
//
//    public static ArrayList<EstateFullName> getCompanies(){
//        ArrayList<EstateFullName> estateFullNames = new ArrayList<>();
//        Gson gson = new Gson();
//        if(isFileContentAvailable(Encrypts.encrypt(ESTATE_FULL_NAME))){
//            try {
//                JSONArray array = new JSONArray(Encrypts.decrypt(getFileContent(Encrypts.encrypt(ESTATE_FULL_NAME))));
//                for(int i=0;i<array.length();i++){
//                    EstateFullName estateFullName = gson.fromJson(array.getJSONObject(i).toString(),
//                            EstateFullName.class);
//                    estateFullNames.add(estateFullName);
//                }
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
//        return estateFullNames;
//    }
}
