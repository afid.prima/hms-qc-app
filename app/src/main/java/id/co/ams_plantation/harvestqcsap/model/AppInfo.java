package id.co.ams_plantation.harvestqcsap.model;

import android.support.annotation.Nullable;

import java.io.Serializable;

/**
 * Created on : 20,December,2021
 * Author     : Afid
 */

public class AppInfo implements Serializable {

    @Nullable
    private Maps maps;
    private int id;
    private String image;
    private int progress;
    private String finishedDownload;
    private String totalDownload;
    private String downloadPerSize;
    private int status;
    private String url;
    private String filePath;
    private String fileName;


    @Nullable
    public Maps getMaps() {
        return maps;
    }
    @Nullable
    public void setMaps(Maps maps) {
        this.maps = maps;
    }

    public String getFinishedDownload() {
        return finishedDownload;
    }

    public void setFinishedDownload(String finishedDownload) {
        this.finishedDownload = finishedDownload;
    }

    public String getTotalDownload() {
        return totalDownload;
    }

    public void setTotalDownload(String totalDownload) {
        this.totalDownload = totalDownload;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public String getDownloadPerSize() {
        return downloadPerSize;
    }

    public void setDownloadPerSize(String downloadPerSize) {
        this.downloadPerSize = downloadPerSize;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}