package id.co.ams_plantation.harvestqcsap.model;

/**
 * Created on : 20,December,2021
 * Author     : Afid
 */

public class Apps {
    private String PackageName;
    private String Version;
    private String LastUpdate;
    private String FileURL;
    private String FileSize;
    private String AppsName;
    private String FileIcon;
    private String VersionCode;

    /**
     *
     * @return
     * The PackageName
     */
    public String getPackageName() {
        return PackageName;
    }

    /**
     *
     * @param PackageName
     * The PackageName
     */
    public void setPackageName(String PackageName) {
        this.PackageName = PackageName;
    }

    /**
     *
     * @return
     * The Version
     */
    public String getVersion() {
        return Version;
    }

    /**
     *
     * @param Version
     * The Version
     */
    public void setVersion(String Version) {
        this.Version = Version;
    }

    /**
     *
     * @return
     * The LastUpdate
     */
    public String getLastUpdate() {
        return LastUpdate;
    }

    /**
     *
     * @param LastUpdate
     * The LastUpdate
     */
    public void setLastUpdate(String LastUpdate) {
        this.LastUpdate = LastUpdate;
    }

    /**
     *
     * @return
     * The FileURL
     */
    public String getFileURL() {
        return FileURL;
    }

    /**
     *
     * @param FileURL
     * The FileURL
     */
    public void setFileURL(String FileURL) {
        this.FileURL = FileURL;
    }

    /**
     *
     * @return
     * The FileSize
     */
    public String getFileSize() {
        return FileSize;
    }

    /**
     *
     * @param FileSize
     * The FileSize
     */
    public void setFileSize(String FileSize) {
        this.FileSize = FileSize;
    }

    public String getAppsName() {
        return AppsName;
    }

    public void setAppsName(String appsName) {
        AppsName = appsName;
    }

    public String getFileIcon() {
        return FileIcon;
    }

    public void setFileIcon(String fileIcon) {
        FileIcon = fileIcon;
    }

    public String getVersionCode() {
        return VersionCode;
    }

    public void setVersionCode(String versionCode) {
        VersionCode = versionCode;
    }
}