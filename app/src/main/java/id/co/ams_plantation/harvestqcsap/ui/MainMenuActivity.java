package id.co.ams_plantation.harvestqcsap.ui;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import butterknife.BindView;
import cat.ereza.customactivityoncrash.util.ServerConnectionListener;
import devlight.io.library.ntb.NavigationTabBar;
import id.co.ams_plantation.harvestqcsap.Fragment.QcFotoPanenFragmet;
import id.co.ams_plantation.harvestqcsap.HarvestApp;
import id.co.ams_plantation.harvestqcsap.model.Estate;
import id.co.ams_plantation.harvestqcsap.model.FilePdf;
import id.co.ams_plantation.harvestqcsap.presenter.MainMenuPresenter;
import id.co.ams_plantation.harvestqcsap.Fragment.QcMutuAncakFragmet;
import id.co.ams_plantation.harvestqcsap.Fragment.QcMutuBuahFragment;
import id.co.ams_plantation.harvestqcsap.Fragment.QcSensusBjrFragment;
import id.co.ams_plantation.harvestqcsap.Fragment.SettingFragment;
import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.encryptor.Encrypts;
import id.co.ams_plantation.harvestqcsap.model.User;
import id.co.ams_plantation.harvestqcsap.presenter.ConnectionPresenter;
import id.co.ams_plantation.harvestqcsap.util.ChangeEstateHelper;
import id.co.ams_plantation.harvestqcsap.util.GlobalHelper;
import id.co.ams_plantation.harvestqcsap.util.MutuAncakHelper;
import id.co.ams_plantation.harvestqcsap.util.NfcHelper;
import id.co.ams_plantation.harvestqcsap.util.QrHelper;
import id.co.ams_plantation.harvestqcsap.util.SessionFotoPanenHelper;
import id.co.ams_plantation.harvestqcsap.util.WidgetHelper;
import id.co.ams_plantation.harvestqcsap.view.MainMenuView;

import static id.co.ams_plantation.harvestqcsap.util.NfcHelper.readFromIntent;

/**
 * Created by user on 11/21/2018.
 */

public class MainMenuActivity extends BaseActivity implements MainMenuView {

    @BindView(R.id.ntb_horizontal)
    NavigationTabBar navigationTabBar;
    @BindView(R.id.vp_horizontal_ntb)
    public ViewPager viewPager;
    @BindView(R.id.footerUser)
    TextView footerUser;
    @BindView(R.id.footerEstate)
    TextView footerEstate;
    public @BindView(R.id.btnFilter)
    LinearLayout btnFilter;
    public @BindView(R.id.tvFilterAfdeling)
    TextView tvFilterAfdeling;
    public @BindView(R.id.tvFilterDataInputBy)
    TextView tvFilterDataInputBy;
    public @BindView(R.id.ivLogo)
    ImageView ivLogo;
    @BindView(R.id.cvToken)
    CardView cvToken;
    @BindView(R.id.tvToken)
    TextView tvToken;
    @BindView(R.id.lChangeEstate)
    LinearLayout lChangeEstate;

    MainMenuPresenter presenter;

    Activity activity;
    QrHelper qrHelper;

    ArrayList<Fragment> arrayListFragment;

    public ConnectionPresenter connectionPresenter;
    public ChangeEstateHelper changeEstateHelper;

/****************************************** NFC ******************************************************************/
    public Tag myTag;
    NfcAdapter nfcAdapter;
    PendingIntent pendingIntent;
    IntentFilter writeTagFilters[];

    //tambahan crash
    ServerConnectionListener serverConnectionListener;
    String value;
    public MutuAncakHelper mutuAncakHelper;

    @Override
    protected void initPresenter() {
        presenter = new MainMenuPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu);
        getSupportActionBar().hide();

        qrHelper = new QrHelper(this);
        mutuAncakHelper = new MutuAncakHelper(this);
        changeEstateHelper = new ChangeEstateHelper(this);

        activity = this;
        main();
        if (qcAncakOn()){
            AlertDialog alertDialog = new AlertDialog.Builder(this, R.style.MyAlertDialogStyle)
                    .setTitle("Perhatian")
                    .setMessage("Apakah Anda Inggin Melanjutkan Proses Qc Ancak")
                    .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mutuAncakHelper.hapusFileQcAncak(true);
                            dialog.dismiss();
                        }
                    })
                    .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            Intent intent = new Intent(MainMenuActivity.this,QcMutuAncakActivity.class);
                            if(value != null) {
                                intent.putExtra("qcAncak", value);
                            }
                            startActivityForResult(intent, GlobalHelper.RESULT_QC_MUTU_ANCAK);
                        }
                    })
                    .create();
            alertDialog.show();
        }

        if(sessionFotoPanenOn()){
            Intent intent = new Intent(MainMenuActivity.this,InspectionFotoActivity.class);
            startActivityForResult(intent, GlobalHelper.RESULT_QC_FOTO_PANEN);
        }

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        try {
            Long exptDate = sdf.parse(GlobalHelper.getUser().getExpirationDate()).getTime();
            long sisaHari = exptDate - System.currentTimeMillis();
            sisaHari = sisaHari / (24 * 60 * 60 * 1000);
            if(sisaHari <= GlobalHelper.NOTIF_TOKEN_DAYS){
                tvToken.setText(sisaHari+" "+getResources().getString(R.string.days_left));
            }else{
                cvToken.setVisibility(View.GONE);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        ivLogo.setVisibility(View.GONE);
        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                filterHelper.showFilterAfdeling(arrayListFragment);
            }
        });

        lChangeEstate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HashMap<String, Estate> hEstateMaping= GlobalHelper.getEstateMapping();
                ArrayList<Estate> estates = new ArrayList<>();
                if(hEstateMaping.size() > 0 ) {
                    for (Estate estate : hEstateMaping.values()) {
                        estates.add(estate);
                    }
                }

                if(estates.size() > 0 ){
                    changeEstateHelper.changeEstate(estates);
                }
            }
        });
    }

    private boolean qcAncakOn(){
        File db = new File(GlobalHelper.getDatabasePathHMS() + "/" + Encrypts.encrypt(GlobalHelper.TABLE_QC_ANCAK_POHON) + "/" +Encrypts.encrypt(GlobalHelper.TABLE_QC_ANCAK_POHON)+ ".db");
        File fileQcAncakSelected = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECTED_QC_ANCAK] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECTED_QC_ANCAK] );
        value = null;

        //dari spb yang sudah pernah di save atau ada di db
        if(fileQcAncakSelected.exists()){
            value = GlobalHelper.readFileContent(fileQcAncakSelected.getAbsolutePath());
        }

        if(db.exists()){
            return true;
        }
        return false;
    }

    private boolean sessionFotoPanenOn(){
        File sessionCurrent = new File(SessionFotoPanenHelper.FULLPATH_SESSION_CURRENT);
        if(sessionCurrent.exists()){
            return true;
        }
        return false;
    }

    private void main(){
        try {
            JSONObject jModule = GlobalHelper.getModule();

            if(jModule == null){
                Toast.makeText(HarvestApp.getContext(),getResources().getString(R.string.cannot_acses),Toast.LENGTH_LONG).show();
                Intent intent = getPackageManager().getLaunchIntentForPackage("id.co.ams_plantation.amsadminapps");
//                if(BuildConfig.BUILD_VARIANT.equals("dev")){
//                    intent = getPackageManager().getLaunchIntentForPackage("id.co.ams_plantation.amsadminapps.dev");
//                }
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return;
            }

//            dataAllUser = GlobalHelper.getAllUser();
//            dataOperator = GlobalHelper.getAllOperator();
//            dataVehicle = GlobalHelper.getAllVehicle();

            NavigationTabBar.Model qcFoto = new NavigationTabBar.Model.Builder(
                    getResources().getDrawable(R.drawable.ic_qc_foto),
                    getResources().getColor(R.color.OrangeRed)
            ).title(getString(R.string.qc_photo_panen)).build();

            NavigationTabBar.Model qcAncak = new NavigationTabBar.Model.Builder(
                    getResources().getDrawable(R.drawable.ic_palm_bw),
                    getResources().getColor(R.color.light_green_color)
            ).title(getString(R.string.qc_mutu_ancak)).build();

            NavigationTabBar.Model qcMutuBuah = new NavigationTabBar.Model.Builder(
                    getResources().getDrawable(R.drawable.ic_qc_bw),
                    getResources().getColor(R.color.md_light_green_800)
            ).title(getString(R.string.qc_mutu_buah)).build();

            NavigationTabBar.Model qcBjr = new NavigationTabBar.Model.Builder(
                    getResources().getDrawable(R.drawable.ic_sensus_bjr_bw),
                    getResources().getColor(R.color.green_color)
            ).title(getString(R.string.sensus_bjr)).build();

            NavigationTabBar.Model setting = new NavigationTabBar.Model.Builder(
                    ContextCompat.getDrawable(this, R.drawable.ic_settings),
                    ContextCompat.getColor(this, R.color.Blue)
            ).title(getString(R.string.action_settings)).build();

            ArrayList<NavigationTabBar.Model> models = new ArrayList<>();
            arrayListFragment = new ArrayList<>();
            arrayListFragment.clear();
            if (jModule.getString("mdlAccCode").equals("QHS2") || jModule.getString("mdlAccCode").equals("QHS4") ||
                (jModule.getString("mdlAccCode").equals("QHS3") && jModule.getString("subMdlAccCode").equals("P7"))
            ){
                models.add(qcFoto);
                models.add(qcAncak);
                models.add(qcMutuBuah);
                models.add(qcBjr);
                models.add(setting);

                arrayListFragment.add(QcFotoPanenFragmet.getInstance());
                arrayListFragment.add(QcMutuAncakFragmet.getInstance());
                arrayListFragment.add(QcMutuBuahFragment.getInstance());
                arrayListFragment.add(QcSensusBjrFragment.getInstance());
                arrayListFragment.add(SettingFragment.getInstance());
            }else if (jModule.getString("mdlAccCode").equals("QHS3")) {
                if(jModule.getString("subMdlAccCode").equals("P1") ||
                        jModule.getString("subMdlAccCode").equals("P4") ||
                        jModule.getString("subMdlAccCode").equals("P5")){
                    models.add(qcFoto);
                    models.add(qcAncak);
                    arrayListFragment.add(QcFotoPanenFragmet.getInstance());
                    arrayListFragment.add(QcMutuAncakFragmet.getInstance());
                }else if(jModule.getString("subMdlAccCode").equals("P2") ||
                        jModule.getString("subMdlAccCode").equals("P4") ||
                        jModule.getString("subMdlAccCode").equals("P6") ){
                    models.add(qcFoto);
                    models.add(qcMutuBuah);
                    arrayListFragment.add(QcFotoPanenFragmet.getInstance());
                    arrayListFragment.add(QcMutuBuahFragment.getInstance());
                }else if(jModule.getString("subMdlAccCode").equals("P3") ||
                        jModule.getString("subMdlAccCode").equals("P5") ||
                        jModule.getString("subMdlAccCode").equals("P6")){
                    models.add(qcFoto);
                    models.add(qcBjr);
                    arrayListFragment.add(QcFotoPanenFragmet.getInstance());
                    arrayListFragment.add(QcSensusBjrFragment.getInstance());
                }
                models.add(setting);
                arrayListFragment.add(SettingFragment.getInstance());
            }else{
                models.add(setting);
                arrayListFragment.add(SettingFragment.getInstance());
            }

            viewPager.setAdapter(new TabPagerAdapter(getSupportFragmentManager(), arrayListFragment));

//        models.add(
//                new NavigationTabBar.Model.Builder(
//                        getResources().getDrawable(R.drawable.ic_data_source),
//                        getResources().getColor(R.color.LightBlue)
//                ).title(getString(R.string.qc)).build()
//        );

            navigationTabBar.setModels(models);
            navigationTabBar.setViewPager(viewPager);

            navigationTabBar.post(new Runnable() {
                @Override
                public void run() {
                    final View bgNavigationTabBar = (View) findViewById(R.id.bg_ntb_horizontal);
                    bgNavigationTabBar.getLayoutParams().height = (int) navigationTabBar.getBarHeight();
                    bgNavigationTabBar.requestLayout();

                    final View viewPager = findViewById(R.id.vp_horizontal_ntb);
                    ((ViewGroup.MarginLayoutParams) viewPager.getLayoutParams()).topMargin =
                            (int) -navigationTabBar.getBadgeMargin();
                    viewPager.requestLayout();
                }
            });
//            viewPager.setOffscreenPageLimit(0);
            navigationTabBar.setOnTabBarSelectedIndexListener(new NavigationTabBar.OnTabBarSelectedIndexListener() {
                @Override
                public void onStartTabSelected(final NavigationTabBar.Model model, final int index) {

                }

                @Override
                public void onEndTabSelected(final NavigationTabBar.Model model, final int index) {
                    model.hideBadge();
                }
            });
            viewPager.setOffscreenPageLimit(1);

            User user = GlobalHelper.getUser();
            footerUser.setText(user.getUserFullName());
            footerEstate.setText(GlobalHelper.getEstate().getEstCode() + " - " +GlobalHelper.getEstate().getEstName());

            nfcAdapter = NfcAdapter.getDefaultAdapter(this);
            if (nfcAdapter == null) {
                // Stop here, we definitely need NFC
                Toast.makeText(this, "This device doesn't support NFC.", Toast.LENGTH_LONG).show();
            }

            pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
            IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
            tagDetected.addCategory(Intent.CATEGORY_DEFAULT);
            writeTagFilters = new IntentFilter[]{tagDetected};

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public Fragment getFragmentCurrent(){
        return  ((TabPagerAdapter)viewPager.getAdapter()).getItem(viewPager.getCurrentItem());
    }



    public void openPDF (FilePdf filePdf){
        File file = filePdf.getFullPath();
        Log.e("MainmenuActivity","File "+file.getAbsolutePath());

        Intent intent = new Intent(Intent.ACTION_VIEW);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Uri apkUri = FileProvider.getUriForFile(this,
                    this.getApplicationContext().getPackageName() + ".provider",
                    file);

            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            Log.e("Location",apkUri.toString());
            intent.setDataAndType(apkUri, "application/pdf");
        }else{
            intent.setDataAndType(Uri.fromFile(file), "application/pdf");
        }
        //intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }

    @Override
    public void onStart() {
        super.onStart();
//        // If Bluetooth is not on, request that it be enabled.
//        // setupChat() will then be called during onActivityResult
//        if (!bluetoothHelper.mBluetoothAdapter.isEnabled()) {
//            Intent enableIntent = new Intent(
//                    BluetoothAdapter.ACTION_REQUEST_ENABLE);
//            startActivityForResult(enableIntent, GlobalHelper.REQUEST_ENABLE_BT);
//            // Otherwise, setup the session
//        } else {
//            if (bluetoothHelper.mService == null)
//                bluetoothHelper.mService = new BluetoothService(this, bluetoothHelper.mHandler);
//        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
            myTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            Log.i("tagNFC", GlobalHelper.bytesToHexString(myTag.getId()));
            Log.i("tagNFC", Arrays.toString(myTag.getTechList()));
        }
        value = readFromIntent(intent);
        if (value != null) {
            int formatNFC = NfcHelper.cekFormatNFC(value);
            if(formatNFC != GlobalHelper.TYPE_NFC_SALAH && value.length() == 5){
                Toast.makeText(this,getResources().getString(R.string.nfc_empty) +" "+ GlobalHelper.LIST_NFC[formatNFC],Toast.LENGTH_SHORT).show();
            }else {
                idNFC = GlobalHelper.bytesToHexString(myTag.getId());
                cekDataPassing(value, this);
            }
        }else {
            Toast.makeText(this,R.string.format_not_valid,Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onPause(){
        super.onPause();
//        unRegister();
        NfcHelper.WriteModeOff(nfcAdapter,this);
    }

    @Override
    public void onResume(){
        super.onResume();
//        registerCrashReceiver();
        NfcHelper.WriteModeOn(nfcAdapter,this,pendingIntent,writeTagFilters);
//        if(bluetoothHelper != null) {
//            if (bluetoothHelper.mService != null) {
//
//                if (bluetoothHelper.mService.getState() == BluetoothService.STATE_NONE) {
//                    // Start the Bluetooth services
//                    bluetoothHelper.mService.start();
//                }
//            }
//        }


        if(!GlobalHelper.isTimeAutomatic(MainMenuActivity.this)){
            AlertDialog alertDialog = WidgetHelper.showOKCancelDialog(MainMenuActivity.this,
                    "Aktifkan Tanggal & Waktu Otomatis",
                    "Mohon aktifkan konfigurasi tanggal & waktu otomatis untuk melanjutkan menggunakan aplikasi",
                    "Tutup",
                    "Buka Pengaturan",
                    false,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_DATE_SETTINGS), 0);
                            finish();
                        }
                    }, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Toast.makeText(MainMenuActivity.this, "Anda harus mengaktifkan tanggal otomatis", Toast.LENGTH_SHORT).show();
                            MainMenuActivity.this.finish();
                        }
                    });

            alertDialog.show();
            return;
        }

//        if(!GlobalVars.checkCoreFileExists()){
//            Bundle bundle = new Bundle();
//            bundle.putString("message","Anda tidak memiliki akses/akses anda habis. Silahkan aktivasi ulang melalui Admin Apps!");
//            Intent intent = new Intent(this,EntryActivity.class);
//            intent.putExtras(bundle);
//            startActivity(intent);
//        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Stop the Bluetooth services
//        if (bluetoothHelper != null) {
//            if (bluetoothHelper.mService != null)
//                bluetoothHelper.mService.stop();
//        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
            switch (resultCode){

                case GlobalHelper.RESULT_QC_MUTU_BUAH:{
                    Fragment fragment = ((TabPagerAdapter)viewPager.getAdapter()).getItem(viewPager.getCurrentItem());
                    if(fragment instanceof QcMutuBuahFragment){
                        ((QcMutuBuahFragment) fragment).startLongOperation(QcMutuBuahFragment.STATUS_LONG_OPERATION_NONE);
                    }
                    break;
                }
                case GlobalHelper.RESULT_QC_SENSUS_BJR:{
                    Fragment fragment = ((TabPagerAdapter)viewPager.getAdapter()).getItem(viewPager.getCurrentItem());
                    if(fragment instanceof QcSensusBjrFragment){
                        ((QcSensusBjrFragment) fragment).startLongOperation(QcSensusBjrFragment.STATUS_LONG_OPERATION_NONE);
                    }
                    break;
                }
                case GlobalHelper.RESULT_QC_MUTU_ANCAK:{
                    Fragment fragment = ((TabPagerAdapter)viewPager.getAdapter()).getItem(viewPager.getCurrentItem());
                    if(fragment instanceof QcMutuAncakFragmet){
                        ((QcMutuAncakFragmet) fragment).startLongOperation(QcMutuAncakFragmet.STATUS_LONG_OPERATION_NONE);
                    }
                    break;
                }
                case GlobalHelper.RESULT_QC_FOTO_PANEN:{
                    Fragment fragment = ((TabPagerAdapter)viewPager.getAdapter()).getItem(viewPager.getCurrentItem());
                    if(fragment instanceof QcFotoPanenFragmet){
                        ((QcFotoPanenFragmet) fragment).startLongOperation(QcFotoPanenFragmet.STATUS_LONG_OPERATION_NONE);
                    }
                    break;
                }
            }
    }

    @Override
    public void onBackPressed() {
        AlertDialog alertDialog = new AlertDialog.Builder(this,R.style.MyAlertDialogStyle)
                .setTitle("Perhatian")
                .setMessage("Yakin keluar aplikasi?")
                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                })
                .create();
        alertDialog.show();
    }
    public class TabPagerAdapter extends FragmentStatePagerAdapter {
        ArrayList<Fragment> arrayListFragment;
        public TabPagerAdapter(FragmentManager fm, ArrayList<Fragment> arrayListFragment) {
            super(fm);
            this.arrayListFragment = arrayListFragment;
        }
        @Override
        public Fragment getItem(int position) {
            return arrayListFragment.get(position);
        }

        @Override
        public int getCount() {
            return arrayListFragment.size();
        }

    }
    //tambahan broadcast crash
    private void registerCrashReceiver(){
        serverConnectionListener = new ServerConnectionListener();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(serverConnectionListener,intentFilter);
    }
    private void unRegister(){
        if(serverConnectionListener!=null){
            unregisterReceiver(serverConnectionListener);
        }
    }

}
