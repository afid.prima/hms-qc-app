package id.co.ams_plantation.harvestqcsap.Fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.esri.android.map.LocationDisplayManager;
import com.esri.android.map.event.OnStatusChangedListener;
import com.esri.core.geometry.GeometryEngine;
import com.esri.core.geometry.Point;
import com.esri.core.geometry.SpatialReference;
import com.esri.core.map.Graphic;
import com.esri.core.symbol.CompositeSymbol;
import com.esri.core.symbol.SimpleLineSymbol;
import com.esri.core.symbol.SimpleMarkerSymbol;
import com.esri.core.symbol.TextSymbol;
import com.google.gson.Gson;
import com.mikepenz.iconics.utils.Utils;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import id.co.ams_plantation.harvestqcsap.HarvestApp;
import id.co.ams_plantation.harvestqcsap.model.NearestBlock;
import id.co.ams_plantation.harvestqcsap.model.QcAncak;
import id.co.ams_plantation.harvestqcsap.model.TPH;
import id.co.ams_plantation.harvestqcsap.model.TransaksiPanen;
import id.co.ams_plantation.harvestqcsap.util.TphHelper;
import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.model.DataNitrit;
import id.co.ams_plantation.harvestqcsap.model.JanjangSensus;
import id.co.ams_plantation.harvestqcsap.model.SensusBjr;
import id.co.ams_plantation.harvestqcsap.ui.BaseActivity;
import id.co.ams_plantation.harvestqcsap.ui.QcSensusBjrActivity;
import id.co.ams_plantation.harvestqcsap.util.CompleteTextViewHelper;
import id.co.ams_plantation.harvestqcsap.util.GlobalHelper;
import id.co.ams_plantation.harvestqcsap.util.SensusBjrHelper;
import id.co.ams_plantation.harvestqcsap.util.WidgetHelper;
import ng.max.slideview.SlideView;
import pl.aprilapps.easyphotopicker.EasyImage;

/**
 * Created by user on 12/4/2018.
 */

public class QcSensusBjrInputFragment extends Fragment {

    View tphphoto;
    ImageView ivPhoto;
    ImageView imagecacnel;
    ImageView imagesview;
    SliderLayout sliderLayout;
    RelativeLayout rl_ipf_takepicture;

    LinearLayout lInfoTph;
    LinearLayout lGang;
    LinearLayout lPemanen;
    LinearLayout lTph;
    LinearLayout lBlock;
    LinearLayout lAncak;
    CompleteTextViewHelper etNotph;
    CompleteTextViewHelper etBlock;
//    AppCompatEditText etAncak;
    LayoutInflater vi;
    ViewGroup lSensusBjr;
    LinearLayout lsave;
    LinearLayout lldeleted;
    LinearLayout lcancel;
    LinearLayout lTglPanen;

    LocationDisplayManager locationDisplayManager;
    TransaksiPanen selectedTransaksiPanen;
    SensusBjr selectedSensusBjr;

    QcSensusBjrActivity qcSensusBjrActivity;
    BaseActivity baseActivity;
    SensusBjrHelper sensusBjrHelper;
    TphHelper tphHelper;

    ArrayList<TPH> allTph;
    ArrayList<File> ALselectedImage;
    ArrayList<View> alv;
    public HashMap<String,Integer> hmdynamicqc;
    int totalJanjang;
    boolean newData;

    private static String TAG = QcSensusBjrInputFragment.class.getSimpleName();
    public double latitude;
    public double longitude;

    final int YesNo_Simpan = 0;
    final int YesNo_Batal = 1;
    final int YesNo_Hapus = 2;
    final int YesNo_UpdateQc = 3;
    final int Setup_Point = 4;

    public static QcSensusBjrInputFragment getInstance(){
        QcSensusBjrInputFragment fragment = new QcSensusBjrInputFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.qc_sensus_bjr_input,null,false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        baseActivity = ((BaseActivity) getActivity());
        sensusBjrHelper = new SensusBjrHelper(getActivity());

        if (getActivity() instanceof QcSensusBjrActivity) {
            qcSensusBjrActivity = ((QcSensusBjrActivity) getActivity());
            if (qcSensusBjrActivity.selectedSensusBjr != null){
                newData = false;
                selectedSensusBjr = qcSensusBjrActivity.selectedSensusBjr;
                selectedTransaksiPanen = qcSensusBjrActivity.selectedSensusBjr.getTransaksiPanen();
                qcSensusBjrActivity.selectedSensusBjr = null;
                if(selectedSensusBjr.getTph().getNamaTph() == null) {
                    Toast.makeText(HarvestApp.getContext(),"Anda Tidak Punya Master TPH Ini",Toast.LENGTH_SHORT).show();
                    qcSensusBjrActivity.backProses();
//                    qcSensusBjrActivity.searchingGPS = Snackbar.make(qcSensusBjrActivity.myCoordinatorLayout,"Anda Tidak Punya Master TPH Ini",Snackbar.LENGTH_INDEFINITE);
//                    qcSensusBjrActivity.searchingGPS.show();
                    return;
                }
            }else if(qcSensusBjrActivity.selectedTransaksiPanen != null) {
                newData = true;
                selectedTransaksiPanen = qcSensusBjrActivity.selectedTransaksiPanen;
                qcSensusBjrActivity.selectedTransaksiPanen = null;
                if(selectedTransaksiPanen.getTph().getNoTph() == null){
                    qcSensusBjrActivity.backProses();
                    qcSensusBjrActivity.searchingGPS = Snackbar.make(qcSensusBjrActivity.myCoordinatorLayout,"Anda Tidak Punya Master TPH Ini",Snackbar.LENGTH_INDEFINITE);
                    qcSensusBjrActivity.searchingGPS.show();
                    return;
                }
            }else{
                //kenapa transaksi panen karena jika data transaksi panen berarti berasaldari tap kartu atau scan qr
                qcSensusBjrActivity.backProses();
                qcSensusBjrActivity.searchingGPS = Snackbar.make(qcSensusBjrActivity.myCoordinatorLayout,"Transaksi Panen Tidak Diketahui",Snackbar.LENGTH_INDEFINITE);
                qcSensusBjrActivity.searchingGPS.show();
                return;
            }

//            if(!qcSensusBjrActivity.currentLocationOK()){
//                qcSensusBjrActivity.backProses();
//                qcSensusBjrActivity.searchingGPS = Snackbar.make(qcSensusBjrActivity.myCoordinatorLayout,
//                        qcSensusBjrActivity.getResources().getString(R.string.gps_not_accurate),
//                        Snackbar.LENGTH_INDEFINITE);
//                qcSensusBjrActivity.searchingGPS.show();
//                return;
//            }
        }

        tphphoto = (View) view.findViewById(R.id.tphphoto);
        ivPhoto = (ImageView) view.findViewById(R.id.ivPhoto);
        imagecacnel = (ImageView) view.findViewById(R.id.imagecacnel);
        imagesview = (ImageView) view.findViewById(R.id.imagesview);
        rl_ipf_takepicture = (RelativeLayout) view.findViewById(R.id.rl_ipf_takepicture);
        sliderLayout = (SliderLayout) view.findViewById(R.id.sliderLayout);

        lInfoTph = (LinearLayout) view.findViewById(R.id.lInfoTph);
        lTglPanen = (LinearLayout) view.findViewById(R.id.lTglPanen);
//        lGang = (LinearLayout) view.findViewById(R.id.lGang);
//        lPemanen = (LinearLayout) view.findViewById(R.id.lPemanen);
        lTph = (LinearLayout) view.findViewById(R.id.lTph);
        lBlock = (LinearLayout) view.findViewById(R.id.lBlock);
        lAncak = (LinearLayout) view.findViewById(R.id.lAncak);
        etNotph = (CompleteTextViewHelper) view.findViewById(R.id.etNotph);
        etBlock = (CompleteTextViewHelper) view.findViewById(R.id.etBlock);
//        etAncak = (AppCompatEditText) view.findViewById(R.id.etAncak);

        lSensusBjr = (ViewGroup) view.findViewById(R.id.lSensusBjr);
        lsave = (LinearLayout) view.findViewById(R.id.lsave);
        lldeleted = (LinearLayout) view.findViewById(R.id.lldeleted);
        lcancel = (LinearLayout) view.findViewById(R.id.lcancel);

//        lGang.setVisibility(View.GONE);
        lAncak.setVisibility(View.GONE);
        lTglPanen.setVisibility(View.GONE);
        etNotph.setFocusable(false);
        etBlock.setFocusable(false);
//        etAncak.setFocusable(false);

        main();

        if(selectedSensusBjr != null){
            if (selectedSensusBjr.getStatus() == SensusBjr.UPLOAD){
                lsave.setVisibility(View.GONE);
                lldeleted.setVisibility(View.GONE);
            }
        }

        baseActivity.zoomToLocation.setVisibility(View.VISIBLE);

        baseActivity.mapView.setOnStatusChangedListener(new OnStatusChangedListener() {
            @Override
            public void onStatusChanged(Object o, STATUS status) {
                if(o==baseActivity.mapView && status==STATUS.INITIALIZED){
                    qcSensusBjrActivity.spatialReference = baseActivity.mapView.getSpatialReference();
                    qcSensusBjrActivity.isMapLoaded = true;
                    locationListenerSetup();
                    new LongOperation(Setup_Point).execute(String.valueOf(Setup_Point));
                }
            }
        });

        baseActivity.zoomToLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new LongOperation(Setup_Point).execute(String.valueOf(Setup_Point));
            }
        });

        ivPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (baseActivity.currentLocationOK()) {
                    if(ALselectedImage.size() > 3) {
                        Toast.makeText(HarvestApp.getContext(),getActivity().getResources().getString(R.string.warning_take_foto),Toast.LENGTH_LONG).show();
                    }else {
                        GlobalHelper.TAG_CAMERA = GlobalHelper.TAG_CAMERA_QC_SENSUSBJR;
                        EasyImage.openCamera(getActivity(),1);
                    }
                } else {
                    qcSensusBjrActivity.searchingGPS = Snackbar.make(qcSensusBjrActivity.myCoordinatorLayout, getResources().getString(R.string.search_gps), Snackbar.LENGTH_INDEFINITE);
                    qcSensusBjrActivity.searchingGPS.show();
                }

            }
        });

        lsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!GlobalHelper.isDoubleClick()) {
                    showYesNo("Apakah Anda Yakin Untuk Simpan Data Ini", YesNo_Simpan);
                }
            }
        });
        lcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!GlobalHelper.isDoubleClick()) {
                    showYesNo("Apakah Anda Yakin Ingin Keluar Form", YesNo_Batal);
                }
            }
        });
        lldeleted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!GlobalHelper.isDoubleClick()) {
                    showYesNo("Apakah Anda Yakin Untuk Hapus Data Ini", YesNo_Hapus);
                }
            }
        });
    }

    public void setupPoint(){
        if(qcSensusBjrActivity.currentlocation != null) {
            latitude = qcSensusBjrActivity.currentlocation.getLatitude();
            longitude = qcSensusBjrActivity.currentlocation.getLongitude();
        }

//        if(selectedTransaksiPanen != null) {
//            latitude = selectedTransaksiPanen.getLatitude();
//            longitude = selectedTransaksiPanen.getLongitude();
//        }

//        Set<String> block = new ArraySet<>();
        Gson gson = new Gson();
        allTph = new ArrayList<>();
        ArrayList<TPH> tphArrayList = tphHelper.setDataTph(GlobalHelper.getEstate().getEstCode(),selectedTransaksiPanen.getTph().getBlock());
        for(TPH tph: tphArrayList){
            addGraphicTph(tph);
            allTph.add(tph);
        }
//        if(qcMutuBuahActivity.currentlocation != null) {

        if (selectedTransaksiPanen != null) {
            addGrapTemp(selectedTransaksiPanen.getLatitude(), selectedTransaksiPanen.getLongitude());
        }

        zoomToLocation(selectedTransaksiPanen.getLatitude(), selectedTransaksiPanen.getLongitude(), qcSensusBjrActivity.spatialReference);
    }

    public void zoomToLocation(double lat, double lon, SpatialReference spatialReference) {
        Log.d(TAG, "zoomToLocation: "+lat +":"+lon);
        Point mapPoint = GeometryEngine.project(lon,lat,spatialReference);
        try {
            baseActivity.mapView.centerAt(mapPoint,true);
            baseActivity.mapView.zoomTo(mapPoint,15000f);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void addGraphicTph(TPH tph){

        Point fromPoint = new Point(tph.getLongitude(), tph.getLatitude());
        Point toPoint = (Point) GeometryEngine.project(fromPoint, SpatialReference.create(SpatialReference.WKID_WGS84), qcSensusBjrActivity.spatialReference);

        CompositeSymbol cms = new CompositeSymbol();
        int color = tph.getResurvey() == 1 ? TPH.COLOR_TPH_RESURVEY : TPH.COLOR_TPH;
        SimpleMarkerSymbol sms = new SimpleMarkerSymbol(color, Utils.convertDpToPx(getActivity(), GlobalHelper.MARKER_SIZE_TRANSKASI), SimpleMarkerSymbol.STYLE.CIRCLE);
        SimpleLineSymbol sls = new SimpleLineSymbol(TPH.COLOR_BELUMUPLOAD, GlobalHelper.GARISTEPI_SIZE, SimpleLineSymbol.STYLE.SOLID);
        if(tph.getStatus() == TPH.STATUS_TPH_MASTER || tph.getStatus() == TPH.STATUS_TPH_UPLOAD){
            sls = new SimpleLineSymbol(TPH.COLOR_UPLOAD, GlobalHelper.GARISTEPI_SIZE, SimpleLineSymbol.STYLE.SOLID);
        }
        sms.setOutline(sls);
        cms.add(sms);

        Graphic pointGraphic = new Graphic(toPoint, cms);
        qcSensusBjrActivity.graphicsLayerTPH.addGraphic(pointGraphic);

        TextSymbol.HorizontalAlignment horizontalAlignment = TextSymbol.HorizontalAlignment.CENTER;
        TextSymbol.VerticalAlignment verticalAlignment = TextSymbol.VerticalAlignment.BOTTOM;

        int Tcolor = Color.parseColor("#000000");
        TextSymbol txtSymbol = new TextSymbol(GlobalHelper.TEXT_SIZE_TRANSKASI,tph.getNamaTph(),Tcolor) ;
        txtSymbol.setHorizontalAlignment(horizontalAlignment);
        txtSymbol.setVerticalAlignment(verticalAlignment);

        pointGraphic = new Graphic(toPoint,txtSymbol);
        qcSensusBjrActivity.graphicsLayerText.addGraphic(pointGraphic);
        Log.d(TAG, "addGraphicTph: "+ tph.getNamaTph());
    }

    public void addGrapTemp(Double lat, Double lon){
        if(baseActivity.mapView!=null && baseActivity.mapView.isLoaded()){
            Point fromPoint = new Point(lon, lat);
            Point toPoint = (Point) GeometryEngine.project(fromPoint, SpatialReference.create(SpatialReference.WKID_WGS84), qcSensusBjrActivity.spatialReference);

            CompositeSymbol cms = new CompositeSymbol();
            SimpleMarkerSymbol sms = new SimpleMarkerSymbol(Color.GREEN, Utils.convertDpToPx(getActivity(), GlobalHelper.MARKER_SIZE), SimpleMarkerSymbol.STYLE.X);
            SimpleLineSymbol sls = new SimpleLineSymbol(Color.GREEN, GlobalHelper.GARISTEPI_SIZE, SimpleLineSymbol.STYLE.SOLID);
            sms.setOutline(sls);
            cms.add(sms);
            Graphic pointGraphic = new Graphic(toPoint, cms);
            qcSensusBjrActivity.graphicsLayer.addGraphic(pointGraphic);
            Log.d(TAG, "addGrapTemp: "+lat + ":"+ lon);
        }
    }

    public void locationListenerSetup(){

        if(baseActivity.mapView!=null && baseActivity.mapView.isLoaded()){

            locationDisplayManager = baseActivity.mapView.getLocationDisplayManager();
            locationDisplayManager.setAllowNetworkLocation(false);
            locationDisplayManager.setAccuracyCircleOn(true);
            locationDisplayManager.setLocationListener(new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    baseActivity.currentlocation = location;
//                    zoomToLocation(location.getLatitude(), location.getLongitude(), spatialReference);
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {

                }

                @Override
                public void onProviderEnabled(String s) {

                }

                @Override
                public void onProviderDisabled(String s) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("GPS anda tidak hidup! Apakah anda ingin menghidupkan GPS?")
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();

                                }
                            });
                    final AlertDialog alert = builder.create();
                    alert.show();

                }


            });
            locationDisplayManager.start();
        }
    }

    public void showYesNo(String text,int stat){
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.yes_no, null);
        TextView tvDialogKet = (TextView) dialogView.findViewById(R.id.ket1);
        SlideView btnCancelDialog = (SlideView) dialogView.findViewById(R.id.btnCancel);
        SlideView btnOkDialog = (SlideView) dialogView.findViewById(R.id.btnOK);

        final AlertDialog dialog = new AlertDialog.Builder(getActivity(), R.style.MyAlertDialogStyle)
                .setView(dialogView)
                .setCancelable(false)
                .create();

        final ForegroundColorSpan fcs = new ForegroundColorSpan(getActivity().getResources().getColor(R.color.Red));
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);

        tvDialogKet.setText(text);
        btnOkDialog.setText("Yes");
        btnCancelDialog.setText("No");

        btnCancelDialog.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(SlideView slideView) {
                switch (stat) {
                    case YesNo_UpdateQc:
                        qcSensusBjrActivity.backProses();
                        dialog.dismiss();
                        break;
                    default:
                        dialog.dismiss();
                        break;
                }
            }
        });
        btnOkDialog.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(SlideView slideView) {
                switch (stat){
                    case YesNo_Simpan:
                        new LongOperation(YesNo_Simpan).execute(String.valueOf(YesNo_Simpan));
                        break;
                    case YesNo_Hapus:
                        new LongOperation(YesNo_Hapus).execute(String.valueOf(YesNo_Hapus));
                        break;
                    case YesNo_Batal:
                        qcSensusBjrActivity.backProses();
                        break;
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void main(){

        tphHelper = new TphHelper(getActivity());
        allTph = new ArrayList<>();
        ALselectedImage = new ArrayList<>();
        ALselectedImage.clear();

        lSensusBjr.removeAllViews();
        vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        alv = new ArrayList<>();
        hmdynamicqc = new HashMap<>();

        if(selectedTransaksiPanen != null) {
            lldeleted.setVisibility(View.GONE);
            if(selectedTransaksiPanen.getFoto() != null) {
                for (String s : selectedTransaksiPanen.getFoto()) {
                    File file = new File(s);
                    ALselectedImage.add(file);
                }
            }

            totalJanjang = GlobalHelper.getTotalJanjang(selectedTransaksiPanen.getHasilPanen());
            etNotph.setText(selectedTransaksiPanen.getTph().getNamaTph());
            etBlock.setText(selectedTransaksiPanen.getTph().getBlock());
//            etAncak.setText(selectedTransaksiPanen.getTph().getAncak());
            if(newData) {
                Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_BJR);
                ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
                Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("param1", selectedTransaksiPanen.getIdTPanen()));
                if (cursor.size() > 0) {
                    newData = false;
                    showYesNo("Apakah Anda Akan Melakukan Update Sensus BJR pada Tph " + selectedTransaksiPanen.getTph().getNamaTph(), YesNo_UpdateQc);
                }
                db.close();
            }
        }else if (selectedSensusBjr != null){
            lldeleted.setVisibility(View.VISIBLE);
            if(selectedSensusBjr.getFoto() != null) {
                for (String s : selectedSensusBjr.getFoto()) {
                    File file = new File(s);
                    ALselectedImage.add(file);
                }
            }

            totalJanjang = selectedSensusBjr.getBeratJanjang().size();
            etNotph.setText(selectedSensusBjr.getTph().getNamaTph());
            etBlock.setText(selectedSensusBjr.getTph().getBlock());
//            etAncak.setText(selectedSensusBjr.getTph().getAncak());
        }
        setupimageslide();
        if (selectedSensusBjr != null) {
            alv.add(addBjrSensus("Brondolan", selectedSensusBjr.getBeratBrondolan() == null ? 0 : selectedSensusBjr.getBeratBrondolan()));
        }else if(selectedTransaksiPanen != null) {
            alv.add(addBjrSensus("Brondolan", 0));
        }
        for(int i = 0 ; i < totalJanjang + 1; i ++){
            int no = i + 1;
            String id = "Janjang "+ String.valueOf(no);
            if (selectedSensusBjr != null){
                if(id.contains("Janjang")){
                    alv.add(addBjrSensus(id, selectedSensusBjr.getBeratJanjang().get(no).getJanjang()));
                }
            }else if(selectedTransaksiPanen != null) {
                alv.add(addBjrSensus(id, 0));
            }
        }

        for(int i = alv.size() - 1 ; i >= 0; i -- ){
            lSensusBjr.addView(alv.get(i), 0, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT));
        }

    }

    public View addBjrSensus(String id,Integer nilai){
        View v = vi.inflate(R.layout.sensus_bjr, null);
        bjrSensus(v,id,String.valueOf(nilai));
        hmdynamicqc.put(id, nilai);
        return v;
    }

    private boolean validasiSensusBJR(){
        if(baseActivity.currentLocationOK()){
            longitude = baseActivity.currentlocation.getLongitude();
            latitude = baseActivity.currentlocation.getLatitude();
            //jika dari nfc biru ada maka cek apa kah jarak lokasi sekarang denggan tph masih sesuai toleransi
            //jika dari nfc hijau ada maka cek apa kah jarak lokasi sekarang denggan tph langsiran masih sesuai toleransi
            if(selectedTransaksiPanen != null){
                double distance = GlobalHelper.distance(selectedTransaksiPanen.getLatitude(),latitude,
                        selectedTransaksiPanen.getLongitude(),longitude);
                if(distance > GlobalHelper.RADIUS){
                    WidgetHelper.warningRadiusTph(qcSensusBjrActivity,qcSensusBjrActivity.myCoordinatorLayout,distance);
//                    qcSensusBjrActivity.searchingGPS = Snackbar.make(qcSensusBjrActivity.myCoordinatorLayout, getResources().getString(R.string.radius_to_long_with_tph), Snackbar.LENGTH_INDEFINITE);
//                    qcSensusBjrActivity.searchingGPS.show();
                    return false;
                }
            }
        }else{
            WidgetHelper.warningFindGps(qcSensusBjrActivity,qcSensusBjrActivity.myCoordinatorLayout);
//            qcSensusBjrActivity.searchingGPS = Snackbar.make(qcSensusBjrActivity.myCoordinatorLayout, getResources().getString(R.string.search_gps), Snackbar.LENGTH_INDEFINITE);
//            qcSensusBjrActivity.searchingGPS.show();
            return false;
        }

        HashMap<Integer,JanjangSensus> beratJanjang = new HashMap<>();
        boolean lanjut = false;
        int jmlKg = 0;
        int countJjgIsi = 0;
        int countJjgTimbang = 0;
        for(TreeMap.Entry<String,Integer> entry :hmdynamicqc.entrySet()){
            if(!entry.getKey().equals("Brondolan")) {
                countJjgTimbang++;
                if (entry.getValue() != 0) {
                    lanjut = true;
                }
                jmlKg += entry.getValue();
                int idx = Integer.parseInt(entry.getKey().replace("Janjang","").trim());
                JanjangSensus janjangSensus = new JanjangSensus(idx,hmdynamicqc.get("Janjang "+idx));
                if(janjangSensus.getJanjang() == 0){
                    lanjut = false;
//                    Toast.makeText(HarvestApp.getContext(),"Mohon Timbang Seluruh Janjang ",Toast.LENGTH_SHORT).show();
//                    return false;
                }else{
                    countJjgIsi++;
                }
                beratJanjang.put(idx,janjangSensus);
            }
        }

        double rataRataBjr = 0;
        int brondolan = hmdynamicqc.get("Brondolan") == null ? 0 :  hmdynamicqc.get("Brondolan");
        if(beratJanjang.size() > 0){
            jmlKg = 0;
            countJjgIsi= 0;
            for(Map.Entry<Integer,JanjangSensus> entry : beratJanjang.entrySet()) {
                if(entry.getValue().getJanjang() > 0){
                    countJjgIsi++;
                    jmlKg += entry.getValue().getJanjang();
                }
            }
            double rataRataJJG = jmlKg / countJjgIsi;
            double proporsiBrondolan = (Double.parseDouble(String.valueOf(brondolan)) *
                    Double.parseDouble(String.valueOf(countJjgIsi)) / Double.parseDouble(String.valueOf(beratJanjang.size()))
            )/Double.parseDouble(String.valueOf(countJjgIsi));
            rataRataBjr = rataRataJJG + proporsiBrondolan;
        }

        if(countJjgIsi >= GlobalHelper.MAX_JANJANG_TIMBANG && countJjgTimbang >= GlobalHelper.MAX_JANJANG_TIMBANG){
            lanjut = true;
        }

        if(!lanjut){
            qcSensusBjrActivity.searchingGPS = Snackbar.make(qcSensusBjrActivity.myCoordinatorLayout, "Mohon Isi Berat Janjang", Snackbar.LENGTH_INDEFINITE);
            qcSensusBjrActivity.searchingGPS.show();
            return false;
        }

        HashSet<String> sFoto = new HashSet<>();
        if(ALselectedImage.size() > 0){
            for(File file : ALselectedImage){
                sFoto.add(file.getAbsolutePath());
            }
        }
        String idPanen = selectedSensusBjr != null ? selectedSensusBjr.getIdTPanen() : selectedTransaksiPanen.getIdTPanen();
        String idQCSensusBJR = "QC2_"+ GlobalHelper.getUser().getUserID()+"_"+ idPanen;
        selectedSensusBjr = new SensusBjr(idQCSensusBJR,
                idPanen,
                latitude,
                longitude,
                brondolan,
                rataRataBjr,
                GlobalHelper.getUser().getUserID(),
                System.currentTimeMillis(),
                SensusBjr.SAVE,
                selectedSensusBjr != null ? selectedSensusBjr.getTph() : selectedTransaksiPanen.getTph(),
                beratJanjang,
                sFoto,
                selectedTransaksiPanen
        );
        return true;
    }

    public void bjrSensus(final View v, final String id,  String values){
        TextView tv = (TextView) v.findViewById(R.id.tvKet);
        AppCompatEditText et = (AppCompatEditText) v.findViewById(R.id.etKet);
        if(id.toLowerCase().contains("janjang")){
            tv.setText("Berat" + id);
        }else{
            tv.setText("BeratBrondolan");
        }

        if(values.equals("0")){
            et.setText("");
        }else{
            et.setText(String.valueOf(values));
        }

        et.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!et.getText().toString().isEmpty()) {
                    hmdynamicqc.put(id, Integer.parseInt(et.getText().toString()));
                }
            }
        });

//        et.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                String idLower = id.toLowerCase();
//                if (!hasFocus ){
//                    if(idLower.contains("janjang")){
//                        try{
//                            int jjgKe = Integer.parseInt(idLower.replace("janjang ",""));
//                            if(jjgKe <= 10){
//                                if(et.getText().toString().equals("") || et.getText().toString().equals("0")){
//                                    Toast.makeText(HarvestApp.getContext(),"Harap Timbang JJG "+ jjgKe + " Dahulu",Toast.LENGTH_SHORT).show();
//                                }
//                            }
//                        }catch (Exception e){
//                            e.printStackTrace();
//                        }
//                    }
//                }else{
//                    if(idLower.contains("janjang")) {
//                        try{
//                            int jjgKe = Integer.parseInt(idLower.replace("janjang ",""));
//                            if(jjgKe <= 11 && jjgKe > 1){
//                                if(et.getText().toString().equals("") || et.getText().toString().equals("0")){
//                                    Toast.makeText(HarvestApp.getContext(),"Harap Timbang JJG "+ jjgKe + " Dahulu",Toast.LENGTH_SHORT).show();
//                                }
//                            }
//                        }catch (Exception e){
//                            e.printStackTrace();
//                        }
//                    }
//                }
//            }
//        });
    }

    public void setImages(File images){
        ALselectedImage.add(images);
        setupimageslide();
    }

    private void setupimageslide(){
        Set<File> sf = new HashSet<>();
        sf.addAll(ALselectedImage);
        ALselectedImage.clear();
        ALselectedImage.addAll(sf);

        if(ALselectedImage.size() > 3){
            ivPhoto.setVisibility(View.GONE);
        }else{
            ivPhoto.setVisibility(View.VISIBLE);
        }
        if(ALselectedImage.size() != 0){
//            tphphoto.setVisibility(View.VISIBLE);
            imagecacnel.setVisibility(View.VISIBLE);
            imagesview.setVisibility(View.VISIBLE);
            sliderLayout.setVisibility(View.VISIBLE);
        }else{
//            tphphoto.setVisibility(View.GONE);
            imagecacnel.setVisibility(View.GONE);
            imagesview.setVisibility(View.GONE);
            sliderLayout.setVisibility(View.GONE);
        }
        sliderLayout.removeAllSliders();
        for(File file : ALselectedImage){
            TextSliderView textSliderView = new TextSliderView(getActivity());
            // initialize a SliderLayout
            if(file.toString().startsWith("http:")){
                String surl = file.toString();
                if(file.toString().startsWith("http://")){

                }else if(file.toString().startsWith("http:/")){
                    surl =surl.replace("http:/","http://");
                }

                textSliderView
                        .image(GlobalHelper.setUrlFoto(surl))
                        .setScaleType(BaseSliderView.ScaleType.Fit);
            }else {
                textSliderView
                        .image(file)
                        .setScaleType(BaseSliderView.ScaleType.Fit);
            }
            sliderLayout.addSlider(textSliderView);
        }
        sliderLayout.stopAutoCycle();
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.FlipHorizontal);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setCustomAnimation(new DescriptionAnimation());
        imagecacnel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(GSApp.getContext(),"slider getCurrentPosition " + sliderLayout.getCurrentPosition(),Toast.LENGTH_SHORT).show();
                try {
                    if (sliderLayout.getCurrentPosition() == 0 && ALselectedImage.size() == 1) {
                        ALselectedImage.clear();
                        sliderLayout.removeAllSliders();
                        sliderLayout.setVisibility(View.GONE);
                        imagecacnel.setVisibility(View.GONE);
                        imagesview.setVisibility(View.GONE);
                    } else {
                        ALselectedImage.remove(sliderLayout.getCurrentPosition());
                        sliderLayout.removeSliderAt(sliderLayout.getCurrentPosition());
                    }
                }catch (Exception e){
                    Log.e("sliderLayout", e.toString());
                }
            }
        });
        imagesview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WidgetHelper.showImagesZoom(getActivity(),ALselectedImage);
            }
        });
    }

    private class LongOperation extends AsyncTask<String, Void, String> {
        private AlertDialog alertDialogAllpoin;
        private Boolean validasi;
        int statLongOperation;

        public LongOperation(int statLongOperation) {
            this.statLongOperation = statLongOperation;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            alertDialogAllpoin = WidgetHelper.showWaitingDialog(getActivity(), getResources().getString(R.string.wait));
            if(statLongOperation == YesNo_Simpan) {
                validasi = validasiSensusBJR();
            }else{
                validasi = true;
            }
        }

        @Override
        protected String doInBackground(String... params) {
            if (validasi) {
                switch (Integer.parseInt(params[0])) {
                    case YesNo_Simpan:
                        validasi = sensusBjrHelper.save(selectedSensusBjr);
                    break;
                    case YesNo_Hapus:
                        validasi = sensusBjrHelper.hapus(selectedSensusBjr);
                    break;
                    case Setup_Point:
                        setupPoint();
                        break;
                }
            }
            return String.valueOf(params[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            alertDialogAllpoin.dismiss();
            switch (Integer.parseInt(result)) {
                case YesNo_Simpan:
                    if (validasi) {
                        Toast.makeText(HarvestApp.getContext(), "Data Berhasil Disimpan", Toast.LENGTH_SHORT).show();
                        qcSensusBjrActivity.backProses();
                    } else {
                        Toast.makeText(HarvestApp.getContext(), "Data Tidak Bisa Disimpan", Toast.LENGTH_SHORT).show();
                    }
                break;
                case YesNo_Hapus:
                    if (validasi) {
                        Toast.makeText(HarvestApp.getContext(), "Data Berhasil Dihapus", Toast.LENGTH_SHORT).show();
                        qcSensusBjrActivity.backProses();
                    } else {
                        Toast.makeText(HarvestApp.getContext(), "Data Tidak Bisa Dihapups", Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }
    }
}
