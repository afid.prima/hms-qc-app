package id.co.ams_plantation.harvestqcsap.ui;

import android.Manifest;
import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
//import com.github.glomadrian.roadrunner.DeterminateRoadRunner;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.mikepenz.iconics.utils.Utils;
import com.robinhood.ticker.TickerUtils;
import com.robinhood.ticker.TickerView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.TreeMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ams_plantation.harvestqcsap.HarvestApp;
import id.co.ams_plantation.harvestqcsap.BuildConfig;
import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.encryptor.Encrypts;
import id.co.ams_plantation.harvestqcsap.model.Block;
import id.co.ams_plantation.harvestqcsap.presenter.EntryPresenter;
import id.co.ams_plantation.harvestqcsap.util.GlobalHelper;
import id.co.ams_plantation.harvestqcsap.util.WidgetHelper;
import id.co.ams_plantation.harvestqcsap.view.EntryView;

import static id.co.ams_plantation.harvestqcsap.util.GlobalHelper.PLAY_SERVICES_RESOLUTION_REQUEST;

/**
 * Created by user on 11/21/2018.
 */

public class EntryActivity extends BaseActivity implements EntryView {
    @BindView(R.id.layout_splash) LinearLayout splashLayout;
    @BindView(R.id.layoutIntro) RelativeLayout introLayout;
    @BindView(R.id.keterangan_entry) TextView txtHarapTunggu;
//    @BindView(R.id.determinate1) DeterminateRoadRunner determinateRoadRunner1;
//    @BindView(R.id.determinate2) DeterminateRoadRunner determinateRoadRunner2;
    @BindView(R.id.tvNamaAplikasi) TickerView namaAplikasi;
    @BindView(R.id.ic_paslogo) ImageView ivPas;
    @BindView(R.id.ic_pas_typo) ImageView ivTypographPas;

    boolean isFinishedButPaused = false;
    protected boolean isPermissionOK = true;
    String[] originalPermissionList;
    private char[] alphabetlist;
    private ValueAnimator progressAnimator;

    EntryPresenter presenter;
    @Override
    protected void initPresenter() {
        presenter = new EntryPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.entry_layout);
        ButterKnife.bind(this);
        getSupportActionBar().hide();
        main();
    }

    private void main(){
        ArrayList<String> permissionList = new ArrayList<>();

        if(!checkPermission(Manifest.permission.INTERNET)) permissionList.add(Manifest.permission.INTERNET);
        if(!checkPermission(Manifest.permission.ACCESS_NETWORK_STATE))permissionList.add(Manifest.permission.ACCESS_NETWORK_STATE);
        if(!checkPermission(Manifest.permission.ACCESS_WIFI_STATE))permissionList.add(Manifest.permission.ACCESS_WIFI_STATE);
        if(!checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE))permissionList.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        if(!checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE))permissionList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if(!checkPermission(Manifest.permission.ACCESS_COARSE_LOCATION))permissionList.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        if(!checkPermission(Manifest.permission.ACCESS_FINE_LOCATION))permissionList.add(Manifest.permission.ACCESS_FINE_LOCATION);
        if(!checkPermission(Manifest.permission.DISABLE_KEYGUARD))permissionList.add(Manifest.permission.DISABLE_KEYGUARD);
        if(!checkPermission(Manifest.permission.VIBRATE))permissionList.add(Manifest.permission.VIBRATE);
        if(!checkPermission(Manifest.permission.CAMERA))permissionList.add(Manifest.permission.CAMERA);
        if(!checkPermission(Manifest.permission.NFC))permissionList.add(Manifest.permission.NFC);
        if(!checkPermission(Manifest.permission.BLUETOOTH_ADMIN))permissionList.add(Manifest.permission.BLUETOOTH_ADMIN);
        if(!checkPermission(Manifest.permission.BLUETOOTH))permissionList.add(Manifest.permission.BLUETOOTH);

        String[] strPermission = new String[permissionList.size()];
        if(permissionList.size()>0){
            requestPermission(permissionList.toArray(strPermission),1);
        }else{
            screeningApplication();
        }
    }

    private boolean checkPermission(String permission){
        int result = ContextCompat.checkSelfPermission(this,permission);
        if(result== PackageManager.PERMISSION_GRANTED){
            isPermissionOK &= true;
            return true;
        }else{
            isPermissionOK &=false;
            return false;
        }
    }

    public boolean checkPlayService(){
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = googleApiAvailability.isGooglePlayServicesAvailable(this);
        if(resultCode != ConnectionResult.SUCCESS){
            if(googleApiAvailability.isUserResolvableError(resultCode)){
                googleApiAvailability.getErrorDialog(this,resultCode,PLAY_SERVICES_RESOLUTION_REQUEST);
            }else{
                Toast.makeText(HarvestApp.getContext(),"Knp Nih Google Play Servicesnya",Toast.LENGTH_LONG);
                finish();
            }
            return false;
        }
        return true;
    }

    public void switchLayout(boolean isSplashVisible){

        if(isSplashVisible){
            introLayout.setVisibility(View.GONE);
            splashLayout.setVisibility(View.VISIBLE);
            YoYo.with(Techniques.FadeIn).duration(100).playOn(splashLayout);
        }else{
            splashLayout.setVisibility(View.GONE);
            introLayout.setVisibility(View.VISIBLE);
            YoYo.with(Techniques.FadeInUp).duration(100).playOn(introLayout);

        }
    }

    private void requestPermission(String[] permission,int reqcode){
        ActivityCompat.requestPermissions(this,permission,reqcode);
    }

    private void screeningApplication(){
        if(!checkPlayService()){
            WidgetHelper.showOKDialog(this,
                    "Warning",
                    "Anda Harus Instal Google Play Service Dahulu ",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            finish();
                        }
                    }
            );
        }else {

            if (GlobalHelper.getUser() != null && GlobalHelper.getEstate() != null &&
//                    BuildConfig.BUILD_VARIANT.equals("dev")?
//                    GlobalHelper.isPackageAvailable("id.co.ams_plantation.amsadminapps.dev", getPackageManager()):
                    GlobalHelper.isPackageAvailable("id.co.ams_plantation.amsadminapps", getPackageManager())
            ) {
                //masuk
//            User user = GlobalHelper.getUser();
//            String androidId = Settings.Secure.getString(
//                    this.getContentResolver(), Settings.Secure.ANDROID_ID);
//            if(user.getAndroidId().equalsIgnoreCase(androidId)){
//                //masuk
                GlobalHelper.setupHMSFolder();
                authenticated();
//            }else{
//                WidgetHelper.showOKDialog(this,
//                        getResources().getString(R.string.global_dialog_title_warning),
//                        getResources().getString(R.string.global_dialog_no_auth),
//                        new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.dismiss();
//                                finish();
//                            }
//                        }
//                );
//            }

            } else {
                WidgetHelper.showOKDialog(this,
                        "Warning",
                        "Anda Tidak Memiliki Akses Ke Aplikasi Ini",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                finish();
                            }
                        }
                );
            }
        }
    }

    private void authenticated(){
//        nanti jika module sudah ada
//        if(getIntent().getExtras()!=null){
//            if(getIntent().getExtras().containsKey("message")){
//                String msg = getIntent().getExtras().getString("message");
//                Toast.makeText(this,msg,Toast.LENGTH_LONG).show();
//            }
//        }
//
//        final WaspHash tblModule = GlobalHelper.getTableHash(GlobalHelper.SELECTED_MODULE);
//
//        String dateStr = GlobalHelper.getUser().getExpirationDate();
//        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
//        try {
//            if(System.currentTimeMillis() < sdf.parse(dateStr).getTime()){
//                Boolean bisaMasuk = false;
//                for (Object o: tblModule.getAllValues()){
//                    Gson gson = new Gson();
//                    UserModuleAccess userModuleAccess = gson.fromJson(o.toString(),UserModuleAccess.class);
//                    if(userModuleAccess.getMdlAccCode().contains("HRT")){
//                        bisaMasuk = true;
//                        break;
//                    }
//                }
//
//                if(bisaMasuk){
                    startAnimation();
//                }else{
//                    WidgetHelper.showOKDialog(this,
//                            "Warning",
//                            "Anda Tidak Memiliki Akses Ke Aplikasi Ini",
//                            new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    dialog.dismiss();
//                                    finish();
//                                }
//                            }
//                    );
//                }
//
//            }else{
//                WidgetHelper.showOKDialog(this,
//                        "Warning",
//                        "Token anda telah expired! harap memperbarui melalui Admin Apps",
//                        new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.dismiss();
//                                finish();
//                            }
//                        }
//                );
//            }
//
//        } catch (ParseException e) {
//            WidgetHelper.showOKDialog(this,
//                    "Warning",
//                    "Token anda telah expired! harap memperbarui melalui Admin Apps",
//                    new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            dialog.dismiss();
//                            finish();
//                        }
//                    }
//            );
//            e.printStackTrace();
//        }
    }

    private void startAnimation(){
//        determinateRoadRunner1 = (DeterminateRoadRunner) findViewById(R.id.determinate1);
//        determinateRoadRunner2 = (DeterminateRoadRunner) findViewById(R.id.determinate2);
        namaAplikasi = (TickerView) findViewById(R.id.tvNamaAplikasi);
        alphabetlist = new char[53];
        alphabetlist[0] = TickerUtils.EMPTY_CHAR;
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 26; j++) {
                // Add all lowercase characters first, then add the uppercase characters.
                alphabetlist[1 + i * 26 + j] = (char) ((i == 0) ? j + 97 : j + 65);
            }
        }

        ivPas = (ImageView) findViewById(R.id.ic_paslogo);
        ivTypographPas = (ImageView) findViewById(R.id.ic_pas_typo);
//        determinateRoadRunner1.setValue(0);
//        determinateRoadRunner2.setValue(0);

        namaAplikasi.setCharacterList(alphabetlist);
        namaAplikasi.setText("-");
        progressAnimator = ValueAnimator.ofInt(0,500).setDuration(500);
        progressAnimator.setStartDelay(500);
        progressAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int value = (Integer) valueAnimator.getAnimatedValue();
//                determinateRoadRunner1.setValue(value);
//                determinateRoadRunner2.setValue(value);
            }
        });
        progressAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
//                YoYo.with(Techniques.FadeOut).duration(400).playOn(determinateRoadRunner1);
//                YoYo.with(Techniques.FadeOut).duration(400).playOn(determinateRoadRunner2);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ivPas.setVisibility(View.VISIBLE);
                        YoYo.with(Techniques.FadeIn).duration(400).playOn(ivPas);

                    }
                },200);
                finishedAnimation();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        progressAnimator.start();
    }

    private void finishedAnimation(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                float tmpWidth = (float) ivPas.getWidth();
                float tmpHeight = (float) ivPas.getHeight();
                float tujuanX = ivPas.getX()-(((ivPas.getWidth()*3)/4) - Utils.convertDpToPx(EntryActivity.this,8));
                float tujuanY = ivTypographPas.getPivotY();
                ivPas.animate()
                        .setDuration(400)
                        .scaleX(0.75f)
                        .scaleY(0.75f)
                        .x(tujuanX);
                ivTypographPas.setVisibility(View.VISIBLE);
                YoYo.with(Techniques.FadeInRight).duration(400).playOn(ivTypographPas);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        namaAplikasi.setVisibility(View.VISIBLE);
                        namaAplikasi.setText(getResources().getString(R.string.app_name));
                        YoYo.with(Techniques.SlideInUp).duration(400).playOn(namaAplikasi);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                YoYo.with(Techniques.FadeOut).duration(500).playOn(introLayout);
                                new Handler().post(new Runnable() {
                                    @Override
                                    public void run() {
                                        switchLayout(true);
                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                cekAdaIsi();
                                            }
                                        },1200);
                                    }
                                });
                            }
                        },1800);
                    }
                },1200);

            }
        },800);
    }

    private void cekAdaIsi(){
        try {
            if(!GlobalHelper.isTimeAutomatic(EntryActivity.this)){
                AlertDialog alertDialog = WidgetHelper.showOKCancelDialog(EntryActivity.this,
                        "Aktifkan Tanggal & Waktu Otomatis",
                        "Mohon aktifkan konfigurasi tanggal & waktu otomatis untuk melanjutkan menggunakan aplikasi",
                        "Tutup",
                        "Buka Pengaturan",
                        false,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                startActivityForResult(new Intent(android.provider.Settings.ACTION_DATE_SETTINGS), 0);
                                finish();
                            }
                        }, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Toast.makeText(EntryActivity.this, "Anda harus mengaktifkan tanggal otomatis", Toast.LENGTH_SHORT).show();
                                EntryActivity.this.finish();
                            }
                        });

                alertDialog.show();
                return;
            }
            File cekFileLastSync = new File(GlobalHelper.getDatabasePathHMS(),Encrypts.encrypt(GlobalHelper.LAST_SYNC));
            String locKmlMap = GlobalHelper.decryptFiles(GlobalHelper.TYPE_KMLBLOCK);
            if(new File(locKmlMap).exists()) {
                TreeMap<String, ArrayList<Block>> afdelingBlocks = GlobalHelper.getAllAfdelingBlock(new File(locKmlMap));
                JSONObject object = new JSONObject(afdelingBlocks);

                SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.AFDELING_BLOCK, MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString(HarvestApp.AFDELING_BLOCK,object.toString());
                editor.apply();

                if(cekFileLastSync.exists()) {
                    JSONObject jModule = GlobalHelper.getModule();
                    if(jModule == null){
                        Toast.makeText(HarvestApp.getContext(),getResources().getString(R.string.cannot_acses),Toast.LENGTH_LONG).show();
                        Intent intent = getPackageManager().getLaunchIntentForPackage("id.co.ams_plantation.amsadminapps");
//                        if(BuildConfig.BUILD_VARIANT.equals("dev")){
//                            intent = getPackageManager().getLaunchIntentForPackage("id.co.ams_plantation.amsadminapps.dev");
//                        }
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                        return;
                    }
                    if ((jModule.getString("mdlAccCode").equals("QHS3")) && (jModule.getString("subMdlAccCode").equals("P5"))){
                        nextIntentAfterEntry("MapActivity");
                    }else {
                        if(GlobalHelper.aktifKembali(jModule)){
                            nextIntentAfterEntry("ActiveActivity");
                        }else {
                            GlobalHelper.setUpAllData();
                            nextIntentAfterEntry("MainMenuActivity");
                        }
                    }
                }else{
                    nextIntentAfterEntry("ActiveActivity");
                }
            }else{
//                WidgetHelper.showOKDialog(this,
//                        "Kesalahan",
//                        "Mohon Download KML Block Via Admin Apps",
//                        new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.dismiss();
//                                finish();
//                            }
//                        }
//                );
                nextIntentAfterEntry("DownloadActivity");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void nextIntentAfterEntry(String nextActivity){
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        try {
            Long exptDate = sdf.parse(GlobalHelper.getUser().getExpirationDate()).getTime();
            long sisaHari = exptDate - System.currentTimeMillis();
            sisaHari = sisaHari / (24 * 60 * 60 * 1000);
            if(sisaHari <= 0 ){
                Toast.makeText(HarvestApp.getContext(),getResources().getString(R.string.token_expired),Toast.LENGTH_SHORT).show();
                Intent intent = getPackageManager().getLaunchIntentForPackage("id.co.ams_plantation.amsadminapps");
//                if(BuildConfig.BUILD_VARIANT.equals("dev")){
//                    intent = getPackageManager().getLaunchIntentForPackage("id.co.ams_plantation.amsadminapps.dev");
//                }
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }else if(sisaHari <= GlobalHelper.NOTIF_TOKEN_DAYS){
                String redWord = sisaHari + " Hari Lagi";
                String pesan = String.format("Token Anda Akan Habis Dalam Waktu %,d Hari Lagi\n" +
                        "Harap Extand Token Di Admin Apps.\n"+
                        "Buka Admin Apps ?",sisaHari);
                SpannableString spanPesan = new SpannableString(pesan);
                spanPesan.setSpan(new ForegroundColorSpan(Color.RED),
                        spanPesan.toString().indexOf(redWord),
                        spanPesan.toString().indexOf(redWord) + redWord.length(),
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                AlertDialog alertDialog = new AlertDialog.Builder(this, R.style.MyAlertDialogStyle)
                        .setTitle("Perhatian")
                        .setMessage(spanPesan)
                        .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                switch (nextActivity){
                                    case "MapActivity":
                                        setIntent(EntryActivity.this, MapActivity.class);
                                        break;
                                    case "ActiveActivity":
                                        setIntent(EntryActivity.this,ActiveActivity.class);
                                        break;
                                    case "MainMenuActivity":
                                        setIntent(EntryActivity.this, MainMenuActivity.class);
                                        break;
                                    case "DownloadActivity":
                                        setIntent(EntryActivity.this, DownloadActivity.class);
                                        break;
                                }
                            }
                        })
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Intent intent = getPackageManager().getLaunchIntentForPackage("id.co.ams_plantation.amsadminapps");
//                                if(BuildConfig.BUILD_VARIANT.equals("dev")){
//                                    intent = getPackageManager().getLaunchIntentForPackage("id.co.ams_plantation.amsadminapps.dev");
//                                }
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                finish();
                            }
                        })
                        .create();
                alertDialog.show();
            }else{
                Log.d("nextActivity",nextActivity);
                switch (nextActivity){
                    case "MapActivity":
                        setIntent(EntryActivity.this, MapActivity.class);
                        break;
                    case "ActiveActivity":
                        setIntent(EntryActivity.this,ActiveActivity.class);
                        break;
                    case "MainMenuActivity":
                        setIntent(EntryActivity.this, MainMenuActivity.class);
                        break;
                    case "DownloadActivity":
                        setIntent(EntryActivity.this, DownloadActivity.class);
                        break;
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
            Toast.makeText(HarvestApp.getContext(),"Get User Gagal",Toast.LENGTH_SHORT);
            Intent intent = getPackageManager().getLaunchIntentForPackage("id.co.ams_plantation.amsadminapps");
//            if(BuildConfig.BUILD_VARIANT.equals("dev")){
//                intent = getPackageManager().getLaunchIntentForPackage("id.co.ams_plantation.amsadminapps.dev");
//            }
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.e("Result permission " + requestCode, permissions.toString() +" "+ grantResults.toString());
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        isPermissionOK = true;
        for(int codeResult:grantResults){
            if(codeResult== PackageManager.PERMISSION_GRANTED){
                isPermissionOK &= true;
            }else{
                isPermissionOK &= false;
            }
        }
        if(isPermissionOK){
            screeningApplication();
        }else{
            WidgetHelper.showOKDialog(this,
                    "Kesalahan",
                    "Mohon berikan izin!",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            finish();
                        }
                    }
            );
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        if(isFinishedButPaused){
            switchLayout(true);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    cekAdaIsi();
                }
            },1200);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        ArrayList<String> permissionList = new ArrayList<>();

        if(!checkPermission(Manifest.permission.INTERNET)) permissionList.add(Manifest.permission.INTERNET);
        if(!checkPermission(Manifest.permission.ACCESS_NETWORK_STATE))permissionList.add(Manifest.permission.ACCESS_NETWORK_STATE);
        if(!checkPermission(Manifest.permission.ACCESS_WIFI_STATE))permissionList.add(Manifest.permission.ACCESS_WIFI_STATE);
        if(!checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE))permissionList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if(!checkPermission(Manifest.permission.CAMERA))permissionList.add(Manifest.permission.CAMERA);
        if(permissionList.size()==0) isFinishedButPaused = true;
    }
}
