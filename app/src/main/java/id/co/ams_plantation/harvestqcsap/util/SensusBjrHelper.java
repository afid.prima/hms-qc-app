package id.co.ams_plantation.harvestqcsap.util;

import android.support.v4.app.FragmentActivity;

import com.google.gson.Gson;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;

import id.co.ams_plantation.harvestqcsap.model.DataNitrit;
import id.co.ams_plantation.harvestqcsap.model.SensusBjr;

public class SensusBjrHelper {

    FragmentActivity activity;

    public SensusBjrHelper(FragmentActivity activity) {
        this.activity = activity;
    }

    public boolean save(SensusBjr sensusBjr){
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_BJR);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Gson gson = new Gson();
        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", sensusBjr.getIdQCSensusBJR()));
        DataNitrit dataNitrit = new DataNitrit(sensusBjr.getIdQCSensusBJR(),
                gson.toJson(sensusBjr),
                sensusBjr.getIdTPanen());
        if(cursor.size() > 0){
            repository.update(dataNitrit);
        }else{
            repository.insert(dataNitrit);
        }
        db.close();
        return true;
    }

    public boolean hapus(SensusBjr sensusBjr){
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_BJR);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Gson gson = new Gson();
        DataNitrit dataNitrit = new DataNitrit(sensusBjr.getIdQCSensusBJR(),
                gson.toJson(sensusBjr),
                sensusBjr.getIdTPanen());
        repository.remove(dataNitrit);
        db.close();
        return true;
    }
}
