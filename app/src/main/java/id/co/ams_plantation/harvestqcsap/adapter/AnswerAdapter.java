package id.co.ams_plantation.harvestqcsap.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.model.Answer;

/**
 * Created on : 08,October,2021
 * Author     : Afid
 */

public class AnswerAdapter extends RecyclerView.Adapter<AnswerAdapter.Holder>{
    Context context;
    ArrayList<Answer> answers;

    public AnswerAdapter(Context context, ArrayList<Answer> answers) {
        this.context = context;
        this.answers = answers;
    }

    public AnswerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.list_popup_item,viewGroup,false);
        return new AnswerAdapter.Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AnswerAdapter.Holder holder, int i) {
        holder.setValue(answers.get(i));
    }

    @Override
    public int getItemCount() {
        return answers.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView item_blockId;
        TextView item_lastUpdate;
        public Holder(View itemView) {
            super(itemView);
            item_blockId = itemView.findViewById(R.id.item_blockId);
            item_lastUpdate = itemView.findViewById(R.id.item_lastUpdate);

            item_lastUpdate.setVisibility(View.GONE);
        }

        public void setValue(Answer answer) {
            item_blockId.setText(answer.getQcAnsware());
        }
    }
}
