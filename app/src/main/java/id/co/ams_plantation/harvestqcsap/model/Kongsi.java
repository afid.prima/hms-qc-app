package id.co.ams_plantation.harvestqcsap.model;

import java.util.ArrayList;

public class Kongsi {
    public static final int idNone = 0;
    public static final int idKelompok = 1;
    public static final int idMekanis = 2;

    public static final String [] arrayTipeKongsi = {
            "Tidak Kongsi",
            "Kelompok",
            "Mekanis"
    };

    int tipeKongsi;
    ArrayList<SubPemanen> subPemanens;

    public Kongsi(int tipeKongsi, ArrayList<SubPemanen> subPemanens) {
        this.tipeKongsi = tipeKongsi;
        this.subPemanens = subPemanens;
    }

    public int getTipeKongsi() {
        return tipeKongsi;
    }

    public void setTipeKongsi(int tipeKongsi) {
        this.tipeKongsi = tipeKongsi;
    }

    public ArrayList<SubPemanen> getSubPemanens() {
        return subPemanens;
    }

    public void setSubPemanens(ArrayList<SubPemanen> subPemanens) {
        this.subPemanens = subPemanens;
    }
}
