package id.co.ams_plantation.harvestqcsap.model;

import android.location.Location;

import java.util.Set;

public class NearestBlock {
    Location location;
    String block;
    Set<String> blocks;

    public NearestBlock(Location location, String block, Set<String> blocks) {
        this.location = location;
        this.block = block;
        this.blocks = blocks;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public Set<String> getBlocks() {
        return blocks;
    }

    public void setBlocks(Set<String> blocks) {
        this.blocks = blocks;
    }
}
