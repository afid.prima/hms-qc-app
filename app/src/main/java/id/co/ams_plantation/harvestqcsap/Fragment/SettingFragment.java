package id.co.ams_plantation.harvestqcsap.Fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import cat.ereza.customactivityoncrash.util.CrashUtil;
import co.ceryle.segmentedbutton.SegmentedButtonGroup;
import id.co.ams_plantation.harvestqcsap.HarvestApp;
import id.co.ams_plantation.harvestqcsap.connection.ServiceResponse;
import id.co.ams_plantation.harvestqcsap.connection.Tag;
import id.co.ams_plantation.harvestqcsap.model.Estate;
import id.co.ams_plantation.harvestqcsap.presenter.ConnectionPresenter;
import id.co.ams_plantation.harvestqcsap.ui.MainMenuActivity;
import id.co.ams_plantation.harvestqcsap.ui.MapActivity;
import id.co.ams_plantation.harvestqcsap.util.GlobalHelper;
import id.co.ams_plantation.harvestqcsap.util.RestoreHelper;
import id.co.ams_plantation.harvestqcsap.util.RestoreMasterHelper;
import id.co.ams_plantation.harvestqcsap.util.SyncHistoryHelper;
import id.co.ams_plantation.harvestqcsap.util.UploadHelper;
import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.model.GangList;
import id.co.ams_plantation.harvestqcsap.model.LastSyncTime;
import id.co.ams_plantation.harvestqcsap.model.ModelRecyclerView;
import id.co.ams_plantation.harvestqcsap.model.SupervisionList;
import id.co.ams_plantation.harvestqcsap.model.SyncTime;
import id.co.ams_plantation.harvestqcsap.ui.BaseActivity;
import id.co.ams_plantation.harvestqcsap.util.SetUpDataSyncHelper;
import id.co.ams_plantation.harvestqcsap.util.SyncTimeHelper;
import id.co.ams_plantation.harvestqcsap.util.WidgetHelper;
import id.co.ams_plantation.harvestqcsap.view.ApiView;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;

/**
 * Created by user on 12/4/2018.
 */

public class SettingFragment extends Fragment implements ApiView {
    int LongOperation;
    public static final int LongOperation_Download_MasterUser = 5;
    public static final int LongOperation_Download_Pemanen = 6;
    public static final int LongOperation_Download_TPH = 7;
    public static final int LongOperation_Download_QCQuestionAnswer = 8;
    public static final int LongOperation_BackUp_HMS = 13;
    public static final int LongOperation_Get_TPH = 14;
    public static final int LongOperation_Get_QCQuestionAnswer = 15;
    public static final int LongOperation_Get_EstateMapping = 18;
    public static final int LongOperation_Get_MasterUser = 19;
    public static final int LongOperation_Get_Pemanen = 20;
    public static final int LongOperation_Restore_HMS = 27;
    public static final int LongOperation_Map = 28;
    public static final int LongOperation_My_Pemanen = 29;
    public static final int LongOperation_Download_EstateMapping = 36;
    public static final int LongOperation_Download_ApplicationConfiguration = 37;
    public static final int LongOperation_Get_ApplicationConfiguration  = 38;
    public static final int LongOperation_Format_NFC = 48;
    public static final int LongOperation_Upload_QcBuah = 49;
    public static final int LongOperation_Upload_SensusBjr = 50;
    public static final int LongOperation_Upload_QcAncak = 51;
    public static final int LongOperation_Get_QcBuah = 52;
    public static final int LongOperation_Get_SensusBjr = 53;
    public static final int LongOperation_Get_QcAncak = 54;
    public static final int LongOperation_Download_QcBuah = 55;
    public static final int LongOperation_Download_SensusBjr = 56;
    public static final int LongOperation_Download_QcAncak = 57;
    public static final int LongOperation_Count_QcAncak = 58;
    public static final int LongOperation_Upload_CrashReport = 61;
    public static final int LongOperation_Cek_DataUpload = 62;
    public static final int LongOperation_Get_SPK = 63;
    public static final int LongOperation_Download_SPK = 64;
    public static final int LongOperation_RestoreMaster = 68;
    public static final int LongOperation_Upload_SessionQcFotoPanen = 69;
    public static final int LongOperation_Get_SessionQcFotoPanen = 70;
    public static final int LongOperation_Download_SessionQcFotoPanen = 71;

    public static final int ButtonGroup_Lokal = 0;
    public static final int ButtonGroup_Public = 1;

    int flagCekData;
    public ConnectionPresenter presenter;
    MainMenuActivity mainMenuActivity;
    public SetUpDataSyncHelper setUpDataSyncHelper;
    RestoreHelper restoreHelper;
    RestoreMasterHelper restoreMasterHelper;
    AlertDialog progressDialog;
    JSONObject objSetUp;
    ServiceResponse responseApi;
    UploadHelper uploadHelper;
    SyncHistoryHelper syncHistoryHelper;
    public CoordinatorLayout myCoordinatorLayout;
    RecyclerView rv;
    public SegmentedButtonGroup segmentedButtonGroup;
    boolean isVisibleToUser;

    public boolean adaDataTransaksi;
    public ArrayList<GangList> gangLists;
    public ArrayList<SupervisionList> supervisionListsAll;
//    public SuperVision superVision;

    public static SettingFragment getInstance() {
        SettingFragment fragment = new SettingFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.setting_layout, null, false);
        mainMenuActivity = (MainMenuActivity) getActivity();
        mainMenuActivity.connectionPresenter = new ConnectionPresenter(this);
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        this.isVisibleToUser = isVisibleToUser;
        main();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        segmentedButtonGroup = view.findViewById(R.id.segmentedButtonGroup);
        rv = view.findViewById(R.id.rv);
        myCoordinatorLayout = view.findViewById(R.id.myCoordinatorLayout);
        setUpDataSyncHelper = new SetUpDataSyncHelper(getActivity());
        restoreHelper = new RestoreHelper(getActivity());
        restoreMasterHelper = new RestoreMasterHelper(getActivity());
        uploadHelper = new UploadHelper(getActivity());
        syncHistoryHelper = new SyncHistoryHelper(getActivity());
        main();
    }

    private void main(){
        if (isVisibleToUser && getActivity() != null) {
            SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
            if(preferences.getBoolean(HarvestApp.CONNECTION_PREF,false)){
                segmentedButtonGroup.setPosition(ButtonGroup_Public);
            }else{
                segmentedButtonGroup.setPosition(ButtonGroup_Lokal);
            }

            segmentedButtonGroup.setOnClickedButtonListener(new SegmentedButtonGroup.OnClickedButtonListener() {
                @Override
                public void onClickedButton(int position) {
                    switch (position){
                        case 0: {
                            SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putBoolean(HarvestApp.CONNECTION_PREF, false);
                            editor.apply();
                            break;
                        }
                        case 1: {
                            SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putBoolean(HarvestApp.CONNECTION_PREF, true);
                            editor.apply();
                            break;
                        }
                    }
                }
            });

            mainMenuActivity.ivLogo.setVisibility(View.VISIBLE);
            mainMenuActivity.btnFilter.setVisibility(View.GONE);

            GlobalHelper.hideKeyboard(getActivity());
            ArrayList<ModelRecyclerView> arrayList = new ArrayList<>();
            arrayList.add(new ModelRecyclerView(String.valueOf(R.drawable.ic_sync),
                    getString(R.string.sync), getString(R.string.sync_info), String.valueOf(GlobalHelper.SETTING_SYNC)));
            arrayList.add(new ModelRecyclerView(String.valueOf(R.drawable.ic_history),
                    getString(R.string.sync_history), getString(R.string.sync_history_info), String.valueOf(GlobalHelper.SETTING_HISTORY_SYNC)));
            arrayList.add(new ModelRecyclerView(String.valueOf(R.drawable.ic_map),
                    getString(R.string.map), getString(R.string.map_info), String.valueOf(GlobalHelper.SETTING_MAP)));
            arrayList.add(new ModelRecyclerView(String.valueOf(R.drawable.ic_backup),
                    getString(R.string.restore), getString(R.string.restore_info), String.valueOf(GlobalHelper.SETTING_BACKUP)));
            arrayList.add(new ModelRecyclerView(String.valueOf(R.drawable.ic_master_data_bw),
                    getString(R.string.restoremaster), getString(R.string.restoremaster_info), String.valueOf(GlobalHelper.SETTING_RESTOREMASTER)));
            arrayList.add(new ModelRecyclerView(String.valueOf(R.drawable.ic_about_application),
                    getString(R.string.about_application), getString(R.string.about_application_info), String.valueOf(GlobalHelper.SETTING_ABOUT_APPLICATION)));

            SettingItemAdapter adapter = new SettingItemAdapter(getActivity(), arrayList);
            rv.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
            ScaleInAnimationAdapter scaleInAnimationAdapter = new ScaleInAnimationAdapter(adapter);
            rv.setAdapter(scaleInAnimationAdapter);

            getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }

    public void questionLanjutDownload(){

        SharedPreferences prefer = HarvestApp.getContext().getSharedPreferences(HarvestApp.SYNC_TIME, Context.MODE_PRIVATE);
        String sync_time = prefer.getString(HarvestApp.SYNC_TIME,null);
        Gson gson = new Gson();
        LastSyncTime lastSyncTime = gson.fromJson(sync_time,LastSyncTime.class);
        HashMap<String,SyncTime> syncTimeHashMap = lastSyncTime.getSyncTimeHashMap();
        SyncTime syncTimeTransaksi = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
        SyncTime syncTimeMaster = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);

        if(!syncTimeTransaksi.isSelected() && !syncTimeMaster.isSelected()){
            Toast.makeText(getActivity(), "Upload Berhasil", Toast.LENGTH_LONG).show();
            return;
        }

        String message = "Upload Berhasil \nApakah Anda Ingin Memulai Download Data ?";
        AlertDialog alertDialog = new AlertDialog.Builder(mainMenuActivity, R.style.MyAlertDialogStyle)
                .setTitle("Perhatian")
                .setMessage(message)
                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        LongOperation = LongOperation_BackUp_HMS;
                        startLongOperation();
                        dialog.dismiss();
                    }
                })
                .create();
        alertDialog.show();
    }

    class SettingItemAdapter extends RecyclerView.Adapter<SettingItemAdapter.Holder>{
        Context context;
        ArrayList<ModelRecyclerView> originLists;
        public SettingItemAdapter(Context context, ArrayList<ModelRecyclerView> modelRecyclerViews){
            this.context = context;
            originLists = modelRecyclerViews;
        }

        @Override
        public SettingItemAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(context).inflate(R.layout.row_setting,parent,false);
            return new Holder(v);
        }

        @Override
        public void onBindViewHolder(SettingItemAdapter.Holder holder, int position) {
            holder.setValue(originLists.get(position));
        }

        @Override
        public int getItemCount() {
            return originLists!=null ? originLists.size() : 0;
        }

        public class Holder extends RecyclerView.ViewHolder {

            CardView cv;
            ImageView iv;
            TextView tv;
            TextView tv1;

            public Holder(View itemView) {
                super(itemView);
                cv = (CardView) itemView.findViewById(R.id.cv);
                iv = (ImageView) itemView.findViewById(R.id.iv);
                tv = (TextView) itemView.findViewById(R.id.tv);
                tv1 = (TextView) itemView.findViewById(R.id.tv1);
            }

            public void setValue(ModelRecyclerView value) {
                iv.setImageResource(Integer.parseInt(value.getLayout()));
                tv.setText(value.getNama());
                tv1.setText(value.getNama1());
                cv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        switch (Integer.parseInt(value.getNama2())){
                            case GlobalHelper.SETTING_SCANQR:
//                                Intent intent = new Intent(getActivity(), QRScan.class);
//                                startActivityForResult(intent,GlobalHelper.RESULT_SCAN_QR);
                                break;
                            case GlobalHelper.SETTING_SYNC:
                                LongOperation = LongOperation_Cek_DataUpload;
                                flagCekData = GlobalHelper.SETTING_SYNC;
                                startLongOperation();
                                break;
                            case GlobalHelper.SETTING_BACKUP:
                                LongOperation = LongOperation_Cek_DataUpload;
                                flagCekData = GlobalHelper.SETTING_BACKUP;
                                startLongOperation();
                                break;
                            case GlobalHelper.SETTING_RESTOREMASTER:
                                LongOperation = LongOperation_Cek_DataUpload;
                                flagCekData = GlobalHelper.SETTING_RESTOREMASTER;
                                startLongOperation();
                                break;
                            case GlobalHelper.SETTING_MAP:
                                LongOperation = LongOperation_Map;
                                startLongOperation();
                                break;
                            case GlobalHelper.SETTING_PEMANEN:
                                LongOperation = LongOperation_My_Pemanen;
                                startLongOperation();
                                break;
                            case GlobalHelper.SETTING_FORMATNFC:
                                LongOperation = LongOperation_Format_NFC;
                                startLongOperation();
                                break;
                            case GlobalHelper.SETTING_HISTORY_SYNC:
                                syncHistoryHelper.showAllSyncHistroy();
                                break;
                            case GlobalHelper.SETTING_REPORT:
//                                Toast.makeText(getActivity(), "Coming soon", Toast.LENGTH_SHORT).show();
//                                Intent intentReport = new Intent(getActivity(), ReportActivity.class);
//                                startActivity(intentReport);
                                break;
                            case GlobalHelper.SETTING_ABOUT_APPLICATION:
                                GlobalHelper.menuAboutApplication(getActivity());
                                break;
                        }
                    }
                });

            }
        }
    }

    private void startLongOperation(){
        new LongOperation().execute(String.valueOf(LongOperation));
    }

    public void startLongOperation(int longOperation){
        LongOperation = longOperation;
        new LongOperation().execute(String.valueOf(LongOperation));
    }

    public class LongOperation extends AsyncTask<String, String, String> {
        boolean skip = false;
        boolean error = false;
        Snackbar snackbar;
        int statLongOperation;

        public LongOperation(){}

        public LongOperation(int statLongOperation) {
            this.statLongOperation = statLongOperation;
            LongOperation = statLongOperation;
        }

        @Override
        protected void onPreExecute() {

            mainMenuActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(progressDialog != null){
                        progressDialog.dismiss();
                    }

                    String text ="";
                    switch (LongOperation){
                        case LongOperation_Download_TPH:
                            text = getActivity().getResources().getString(R.string.update_data_tph);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Get_TPH:
                            text = getActivity().getResources().getString(R.string.get_data_tph);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Download_QCQuestionAnswer:
                            text = getActivity().getResources().getString(R.string.update_data_qc_qa);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Get_QCQuestionAnswer:
                            text = getActivity().getResources().getString(R.string.get_data_qc_qa);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Get_MasterUser:
                            text = getActivity().getResources().getString(R.string.get_master_user);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Download_MasterUser:
                            text = getActivity().getResources().getString(R.string.download_master_user);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Get_Pemanen:
                            text = getActivity().getResources().getString(R.string.get_master_pemanen);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Download_Pemanen:
                            text = getActivity().getResources().getString(R.string.download_master_pemanen);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Upload_QcBuah:
                            text = getActivity().getResources().getString(R.string.setup_data_Qc_Buah);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Get_QcBuah:
                            text = getActivity().getResources().getString(R.string.get_qc_buah_list);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Download_QcBuah:
                            text = getActivity().getResources().getString(R.string.download_qc_buah_list);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Upload_SessionQcFotoPanen:
                            text = getActivity().getResources().getString(R.string.setup_data_Qc_Session_Foto_Panen);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Get_SessionQcFotoPanen:
                            text = getActivity().getResources().getString(R.string.get_qc_Session_Foto_Panen);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Download_SessionQcFotoPanen:
                            text = getActivity().getResources().getString(R.string.download_qc_Session_Foto_Panen);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Upload_QcAncak:
                            text = getActivity().getResources().getString(R.string.setup_data_Qc_Buah);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Get_QcAncak:
                            text = getActivity().getResources().getString(R.string.get_qc_ancak_list);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Download_QcAncak:
                            text = getActivity().getResources().getString(R.string.download_qc_ancak_list);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Count_QcAncak:
                            text = getActivity().getResources().getString(R.string.setup_data_Counter_mutu_ancak);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Upload_SensusBjr:
                            text = getActivity().getResources().getString(R.string.setup_data_Sensus_Bjr);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Get_SensusBjr:
                            text = getActivity().getResources().getString(R.string.get_sensus_bjr_list);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Download_SensusBjr:
                            text = getActivity().getResources().getString(R.string.download_sensus_bjr_list);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Get_ApplicationConfiguration:
                            text = getActivity().getResources().getString(R.string.get_master_app_config);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Download_ApplicationConfiguration:
                            text = getActivity().getResources().getString(R.string.download_master_app_config);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Get_SPK:
                            text = getActivity().getResources().getString(R.string.wait);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Download_SPK:
                            text = getActivity().getResources().getString(R.string.wait);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Upload_CrashReport:
                            text = getActivity().getResources().getString(R.string.wait);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_BackUp_HMS:
                            text = getActivity().getResources().getString(R.string.backup_data_hms);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Restore_HMS:
                            text = getActivity().getResources().getString(R.string.restore);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_RestoreMaster:
                            text = getActivity().getResources().getString(R.string.wait);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Map:
                            text = getActivity().getResources().getString(R.string.map);
                            ((BaseActivity) getActivity()).alertDialogBase = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_My_Pemanen:
                            text = getActivity().getResources().getString(R.string.wait);
                            ((BaseActivity) getActivity()).alertDialogBase = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Format_NFC:
                            text = getActivity().getResources().getString(R.string.wait);
                            ((BaseActivity) getActivity()).alertDialogBase = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Cek_DataUpload:
                            text = getActivity().getResources().getString(R.string.wait);
                            ((BaseActivity) getActivity()).alertDialogBase = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Get_EstateMapping:
                            text = getActivity().getResources().getString(R.string.wait);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Download_EstateMapping:
                            text = getActivity().getResources().getString(R.string.wait);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                    }

                    Log.d("SettingFragment","onPreExecute "+text);
                }
            });
            objSetUp = new JSONObject();
            skip = false;
            error = false;

        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                switch (Integer.parseInt(strings[0])) {
                    case LongOperation_Cek_DataUpload:

                        HashMap<String, Estate> hEstateMaping= GlobalHelper.getEstateMapping();
                        ArrayList<Estate> estates = new ArrayList<>();
                        if(hEstateMaping.size() > 0 ) {
                            for (Estate estate : hEstateMaping.values()) {
                                estates.add(estate);
                            }
                        }

                        mainMenuActivity.estatesMapping = estates;
                        mainMenuActivity.idxEstate = 0;

                        adaDataTransaksi = false;

                        if(!adaDataTransaksi) {
                            for(int i = 0 ; i < mainMenuActivity.estatesMapping.size();i++) {
                                mainMenuActivity.idxEstate = i;
                                objSetUp = setUpDataSyncHelper.UploadSessionQcFotoPanen(true, this);
                                if (objSetUp.getJSONArray("data").length() > 0) {
                                    adaDataTransaksi = true;
                                    break;
                                }
                            }
                        }
                        if(!adaDataTransaksi) {
                            for(int i = 0 ; i < mainMenuActivity.estatesMapping.size();i++) {
                                mainMenuActivity.idxEstate = i;
                                objSetUp = setUpDataSyncHelper.UploadQcAncak(true, this);
                                if (objSetUp.getJSONArray("data").length() > 0) {
                                    adaDataTransaksi = true;
                                    break;
                                }
                            }
                        }
                        if(!adaDataTransaksi) {
                            for(int i = 0 ; i < mainMenuActivity.estatesMapping.size();i++) {
                                mainMenuActivity.idxEstate = i;
                                objSetUp = setUpDataSyncHelper.UploadQcBuah(true, this);
                                if (objSetUp.getJSONArray("data").length() > 0) {
                                    adaDataTransaksi = true;
                                    break;
                                }
                            }
                        }
                        if(!adaDataTransaksi) {
                            for(int i = 0 ; i < mainMenuActivity.estatesMapping.size();i++) {
                                mainMenuActivity.idxEstate = i;
                                objSetUp = setUpDataSyncHelper.UploadSensusBjr(true, this);
                                if (objSetUp.getJSONArray("data").length() > 0) {
                                    adaDataTransaksi = true;
                                    break;
                                }
                            }
                        }
                        break;

                    case LongOperation_Get_TPH:
                        //akses api doang TPH
                        mainMenuActivity.connectionPresenter.GetTphListByEstate(setUpDataSyncHelper.estCodeSyncNow());
                        break;
                    case LongOperation_Download_TPH:
                        //update db tph
                        skip = !setUpDataSyncHelper.downLoadTph(responseApi,this);
                        break;


                    case LongOperation_Get_QCQuestionAnswer:
                        //akses api doang TPH
                        mainMenuActivity.connectionPresenter.GetQCQuestionAnswer();
                        break;
                    case LongOperation_Download_QCQuestionAnswer:
                        //update db tph
                        skip = !setUpDataSyncHelper.downLoadQCQuestionAnswer(responseApi,this);
                        break;

                    // qc foto panen
                    case LongOperation_Upload_SessionQcFotoPanen:
                        objSetUp = setUpDataSyncHelper.UploadSessionQcFotoPanen(true, this);
                        if(objSetUp != null) {
                            if (objSetUp.getJSONArray("data").length() > 0) {
                                uploadHelper.uploadData(this, Tag.PostSessionQcFotoPanen, objSetUp.getJSONArray("data"), 0);
                            } else {
                                skip = true;
                            }
                        }else{
                            error = true;
                        }
                        break;
                    case LongOperation_Get_SessionQcFotoPanen:
                        JSONArray jsonArray= new JSONArray();
                        objSetUp = setUpDataSyncHelper.GetSessionQcFotoPanen(this);
                        if(objSetUp.has("data")){
                            jsonArray =objSetUp.getJSONArray("data");
                        }
                        mainMenuActivity.connectionPresenter.GetSessionQcFotoPanen(setUpDataSyncHelper.estCodeSyncNow(),jsonArray);
                        break;
                    case LongOperation_Download_SessionQcFotoPanen:
                        //update db Qc Buah
                        skip = !setUpDataSyncHelper.downLoadSessionQcFotoPanen(responseApi,this);
                        break;

                    // qc buah
                    case LongOperation_Upload_QcBuah:
                        objSetUp = setUpDataSyncHelper.UploadQcBuah(true, this);
                        if(objSetUp != null) {
                            if (objSetUp.getJSONArray("data").length() > 0) {
                                uploadHelper.uploadImages(this,Tag.PostQcBuahImages,objSetUp.getJSONArray("data"),0);
                            } else {
                                skip = true;
                            }
                        }else{
                            error = true;
                        }
                        break;
                    case LongOperation_Get_QcBuah:
                        //aksesapi doang Qc Buah
                        mainMenuActivity.connectionPresenter.GetQcBuahByEstate(setUpDataSyncHelper.estCodeSyncNow());
                        break;
                    case LongOperation_Download_QcBuah:
                        //update db Qc Buah
                        skip = !setUpDataSyncHelper.downLoadQcBuah(responseApi,this);
                        break;

                    //Sensus BJR
                    case LongOperation_Upload_SensusBjr:
                        objSetUp = setUpDataSyncHelper.UploadSensusBjr(true, this);
                        if(objSetUp != null) {
                            if (objSetUp.getJSONArray("data").length() > 0) {
                                uploadHelper.uploadImages(this,Tag.PostSensusBjrImages,objSetUp.getJSONArray("data"),0);
                            } else {
                                skip = true;
                            }
                        }else{
                            error = true;
                        }
                        break;
                    case LongOperation_Get_SensusBjr:
                        //aksesapi doang Sensus BJR
                        mainMenuActivity.connectionPresenter.GetSensusBJRByEstate(setUpDataSyncHelper.estCodeSyncNow());
                        break;
                    case LongOperation_Download_SensusBjr:
                        //update db Sensus
                        skip = !setUpDataSyncHelper.downLoadSensusBJR(responseApi,this);
                        break;

                    //Qc Ancak
                    case LongOperation_Upload_QcAncak:
                        objSetUp = setUpDataSyncHelper.UploadQcAncak(true, this);
                        if(objSetUp != null) {
                            if (objSetUp.getJSONArray("data").length() > 0) {
                                uploadHelper.uploadImages(this,Tag.PostQcAncakImages,objSetUp.getJSONArray("data"),0);
                                //qc ancak tidak pakai foto
                                //uploadHelper.uploadData(this,Tag.PostQcAncak,objSetUp.getJSONArray("data"),0);
                            } else {
                                skip = true;
                            }
                        }else{
                            error = true;
                        }
                        break;
                    case LongOperation_Get_QcAncak:
                        mainMenuActivity.connectionPresenter.GetQCMutuAncakByEstate(setUpDataSyncHelper.estCodeSyncNow());
                        break;
                    case LongOperation_Download_QcAncak:
                        //update db Qc Ancak
                        skip = !setUpDataSyncHelper.downLoadQcAncak(responseApi,this);
                        break;
                    case LongOperation_Count_QcAncak:
                        mainMenuActivity.connectionPresenter.GetQCMutuAncakHeaderCounterByUserID();
                        break;

                    case LongOperation_Get_Pemanen:
                        //akses api doang Langsiran
                        mainMenuActivity.connectionPresenter.GetPemanenListByEstate(setUpDataSyncHelper.estCodeSyncNow());
                        break;
                    case LongOperation_Download_Pemanen:
                        //update db Langsiran
                        skip = !setUpDataSyncHelper.SetPemanenListByEstate(responseApi,this);
                        break;

                    case LongOperation_Upload_CrashReport:
                        objSetUp = setUpDataSyncHelper.UploadCrashReport(true, this);
                        if(objSetUp != null) {
                            if (objSetUp.getJSONArray("data").length() > 0) {
                                mainMenuActivity.connectionPresenter.UploadCrashReport(objSetUp.getJSONArray("data"));
                            } else {
                                skip = true;
                            }
                        }else{
                            error = true;
                        }
                        break;

                    //Backup
                    case LongOperation_BackUp_HMS:
//                        if(!GlobalHelper.harusBackup()){
                            setUpDataSyncHelper.backupDataHMS(mainMenuActivity.estatesMapping,this);
//                        }
                        break;
                    //Restore
                    case LongOperation_Restore_HMS:
                        skip =!restoreHelper.choseRestore(this);
                        break;
                    case LongOperation_RestoreMaster:
                        skip =!restoreMasterHelper.choseRestoreMaster();
                        break;
                    //Master User
                    case LongOperation_Get_MasterUser:
                        //akses api doang Langsiran
                        mainMenuActivity.connectionPresenter.GetMasterUserByEstate(setUpDataSyncHelper.estCodeSyncNow());
                        break;
                    case LongOperation_Download_MasterUser:
                        //update db Langsiran
                        skip = !setUpDataSyncHelper.SetMasterUserByEstate(responseApi,this);
                        break;

                    //Master AfdelingAssistantByEstate
                    case LongOperation_Get_ApplicationConfiguration:
                        //akses api doang
                        mainMenuActivity.connectionPresenter.GetApplicationConfiguration(setUpDataSyncHelper.estCodeSyncNow());
                        break;
                    case LongOperation_Download_ApplicationConfiguration:
                        //update db
                        skip = !setUpDataSyncHelper.downLoadApplicationConfiguration(responseApi,this);
                        break;

                    //SPK
                    case LongOperation_Get_SPK:
                        mainMenuActivity.connectionPresenter.GetMasterSPKByEstate(setUpDataSyncHelper.estCodeSyncNow());
                        break;
                    case LongOperation_Download_SPK:
                        skip=!setUpDataSyncHelper.downLoadMasterSpk(responseApi,this);
                        break;

                    case LongOperation_Get_EstateMapping:
                        mainMenuActivity.connectionPresenter.GetEstateMapping(setUpDataSyncHelper.estCodeSyncNow());
                        break;
                    case LongOperation_Download_EstateMapping:
                        skip = !setUpDataSyncHelper.getEstateMapping(responseApi,this);
                        break;
                }

                Log.d("SettingFragment ","doInBackground "+strings[0]);
                return strings[0];
            } catch (JSONException e) {
                e.printStackTrace();
                return "JSONException";
            }
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            mainMenuActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject objProgres = new JSONObject(String.valueOf(values[0]));

                        if(snackbar== null){
                            snackbar = Snackbar.make(myCoordinatorLayout,objProgres.getString("ket"),Snackbar.LENGTH_INDEFINITE);
                        }
                        snackbar.setText(String.format(objProgres.getString("ket") + " %d %s [%d/%d] ",
                                objProgres.getInt("persen"), "%", objProgres.getInt("count"), objProgres.getInt("total")));
                        snackbar.show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            Log.d("SettingFragment","onPostExecute "+result);
            if(snackbar != null){
                snackbar.dismiss();
            }
            if(result.equals("JSONException")){
                Toast.makeText(HarvestApp.getContext(),"Gagal Prepare Data Long Operation",Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
            }else {
                switch (Integer.parseInt(result)) {
                    case LongOperation_Cek_DataUpload:
                        ((BaseActivity) getActivity()).alertDialogBase.dismiss();
                        if(flagCekData == GlobalHelper.SETTING_SYNC){
                            uploadHelper.SelectUploadSync();
                        }else if(flagCekData == GlobalHelper.SETTING_BACKUP){
                            if(adaDataTransaksi){
                                Toast.makeText(HarvestApp.getContext(),"Mohon Upload Data Dahulu",Toast.LENGTH_LONG).show();
                            }else {
                                restoreHelper.showAllRestore();
                            }
                        }else if (flagCekData == GlobalHelper.SETTING_RESTOREMASTER){
                            restoreMasterHelper.showRestoreMaster();
                        }
                        break;

                    case LongOperation_Download_TPH:
                        if (skip) {
                            progressDialog.dismiss();
                        } else {
                            Toast.makeText(HarvestApp.getContext(), "Berhasil Update TPH", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                            if (mainMenuActivity.estatesMapping.size() -1 == mainMenuActivity.idxEstate) {
                                mainMenuActivity.idxEstate = 0;
                                new LongOperation(LongOperation_Get_QCQuestionAnswer).execute(String.valueOf(LongOperation_Get_QCQuestionAnswer));
                            } else {
                                mainMenuActivity.idxEstate++;
                                new LongOperation(LongOperation_Get_TPH).execute(String.valueOf(LongOperation_Get_TPH));
                            }
                        }
                        break;

                    case LongOperation_Download_QCQuestionAnswer:
                        if (skip) {
                            progressDialog.dismiss();
                        } else {
                            Toast.makeText(HarvestApp.getContext(), "Berhasil Update QC Question Answer", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                            if (mainMenuActivity.estatesMapping.size() -1 == mainMenuActivity.idxEstate) {
                                mainMenuActivity.idxEstate = 0;
                                new LongOperation(LongOperation_Get_SPK).execute(String.valueOf(LongOperation_Get_SPK));
                            } else {
                                mainMenuActivity.idxEstate++;
                                new LongOperation(LongOperation_Get_QCQuestionAnswer).execute(String.valueOf(LongOperation_Get_QCQuestionAnswer));
                            }
                        }
                        break;

                    //Qc MutuBuah
                    case LongOperation_Upload_QcBuah:
                        if (skip) {
                            progressDialog.dismiss();
                            if (mainMenuActivity.estatesMapping.size() -1 == mainMenuActivity.idxEstate) {
                                mainMenuActivity.idxEstate = 0;
                                new LongOperation(LongOperation_Upload_SensusBjr).execute(String.valueOf(LongOperation_Upload_SensusBjr));
                            } else {
                                mainMenuActivity.idxEstate++;
                                new LongOperation(LongOperation_Upload_QcBuah).execute(String.valueOf(LongOperation_Upload_QcBuah));
                            }
                        } else if(error){
                            progressDialog.dismiss();
                            Toast.makeText(HarvestApp.getContext(), "Data Tidak Bisa Di Upload", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case LongOperation_Download_QcBuah:
                        if (skip) {
                            progressDialog.dismiss();
                            WidgetHelper.showSnackBar(myCoordinatorLayout,"Gagal Download Qc Buah");
                        } else {
                            Toast.makeText(HarvestApp.getContext(), "Berhasil Update Qc Bauh", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                            if (mainMenuActivity.estatesMapping.size() -1 == mainMenuActivity.idxEstate) {
                                mainMenuActivity.idxEstate = 0;
                                new LongOperation(LongOperation_Get_SensusBjr).execute(String.valueOf(LongOperation_Get_SensusBjr));
                            } else {
                                mainMenuActivity.idxEstate++;
                                new LongOperation(LongOperation_Get_QcBuah).execute(String.valueOf(LongOperation_Get_QcBuah));
                            }
                        }
                        break;

                    //Qc Sensus BJR
                    case LongOperation_Upload_SensusBjr:
                        if (skip) {
                            progressDialog.dismiss();
                            if (mainMenuActivity.estatesMapping.size() -1 == mainMenuActivity.idxEstate) {
                                mainMenuActivity.idxEstate = 0;
                                new LongOperation(LongOperation_Upload_QcAncak).execute(String.valueOf(LongOperation_Upload_QcAncak));
                            } else {
                                mainMenuActivity.idxEstate++;
                                new LongOperation(LongOperation_Upload_SensusBjr).execute(String.valueOf(LongOperation_Upload_SensusBjr));
                            }
                        } else if(error){
                            progressDialog.dismiss();
                            Toast.makeText(HarvestApp.getContext(), "Data Tidak Bisa Di Upload", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case LongOperation_Download_SensusBjr:
                        if (skip) {
                            progressDialog.dismiss();
                            WidgetHelper.showSnackBar(myCoordinatorLayout,"Gagal Download Sensus BJR");
                        } else {
                            Toast.makeText(HarvestApp.getContext(), "Berhasil Update Sensus BJR", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                            if (mainMenuActivity.estatesMapping.size() -1 == mainMenuActivity.idxEstate) {
                                mainMenuActivity.idxEstate = 0;
                                new LongOperation(LongOperation_Get_QcAncak).execute(String.valueOf(LongOperation_Get_QcAncak));
                            } else {
                                mainMenuActivity.idxEstate++;
                                new LongOperation(LongOperation_Get_SensusBjr).execute(String.valueOf(LongOperation_Get_SensusBjr));
                            }
                        }
                        break;

                    //Qc Ancak
                    case LongOperation_Upload_QcAncak:
                        if (skip) {
                            progressDialog.dismiss();
                            if (mainMenuActivity.estatesMapping.size() -1 == mainMenuActivity.idxEstate) {
                                mainMenuActivity.idxEstate = 0;
                                SyncTimeHelper.updateFinishSyncTime(SyncTime.UPLOAD_TRANSAKSI);
                                new LongOperation(LongOperation_Upload_SessionQcFotoPanen).execute(String.valueOf(LongOperation_Upload_SessionQcFotoPanen));
                            } else {
                                mainMenuActivity.idxEstate++;
                                new LongOperation(LongOperation_Upload_QcAncak).execute(String.valueOf(LongOperation_Upload_QcAncak));
                            }
                        } else if(error){
                            progressDialog.dismiss();
                            Toast.makeText(HarvestApp.getContext(), "Data Tidak Bisa Di Upload", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case LongOperation_Download_QcAncak:
                        if (skip) {
                            progressDialog.dismiss();
                            WidgetHelper.showSnackBar(myCoordinatorLayout,"Gagal Download Qc Ancak");
                        } else {
                            Toast.makeText(HarvestApp.getContext(), "Berhasil Update Qc Ancak", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                            if (mainMenuActivity.estatesMapping.size() -1 == mainMenuActivity.idxEstate) {
                                mainMenuActivity.idxEstate = 0;
                                new LongOperation(LongOperation_Get_SessionQcFotoPanen).execute(String.valueOf(LongOperation_Get_SessionQcFotoPanen));
                            } else {
                                mainMenuActivity.idxEstate++;
                                new LongOperation(LongOperation_Get_QcAncak).execute(String.valueOf(LongOperation_Get_QcAncak));
                            }
                        }
                        break;

                    //Qc Ancak
                    case LongOperation_Upload_SessionQcFotoPanen:
                        if (skip) {
                            progressDialog.dismiss();
                            if (mainMenuActivity.estatesMapping.size() -1 == mainMenuActivity.idxEstate) {
                                mainMenuActivity.idxEstate = 0;
                                SyncTimeHelper.updateFinishSyncTime(SyncTime.UPLOAD_TRANSAKSI);
                                new LongOperation(LongOperation_Upload_CrashReport).execute(String.valueOf(LongOperation_Upload_CrashReport));
                            } else {
                                mainMenuActivity.idxEstate++;
                                new LongOperation(LongOperation_Upload_SessionQcFotoPanen).execute(String.valueOf(LongOperation_Upload_SessionQcFotoPanen));
                            }
                        } else if(error){
                            progressDialog.dismiss();
                            Toast.makeText(HarvestApp.getContext(), "Data Tidak Bisa Di Upload", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case LongOperation_Download_SessionQcFotoPanen:
                        if (skip) {
                            progressDialog.dismiss();
                            WidgetHelper.showSnackBar(myCoordinatorLayout,"Gagal Download Session Foto Panen");
                        } else {
                            Toast.makeText(HarvestApp.getContext(), "Berhasil Update Session Foto Panen", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                            if (mainMenuActivity.estatesMapping.size() -1 == mainMenuActivity.idxEstate) {
                                mainMenuActivity.idxEstate = 0;
                                new LongOperation(LongOperation_Count_QcAncak).execute(String.valueOf(LongOperation_Count_QcAncak));
                            } else {
                                mainMenuActivity.idxEstate++;
                                new LongOperation(LongOperation_Get_SessionQcFotoPanen).execute(String.valueOf(LongOperation_Get_SessionQcFotoPanen));
                            }
                        }
                        break;

                    case LongOperation_Download_Pemanen:
                        if(skip){
                            Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetPemanenByEstate",Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }else {
                            progressDialog.dismiss();
                            if (mainMenuActivity.estatesMapping.size() -1 == mainMenuActivity.idxEstate) {
                                mainMenuActivity.idxEstate = 0;
                                new LongOperation(LongOperation_Get_TPH).execute(String.valueOf(LongOperation_Get_TPH));
                            } else {
                                mainMenuActivity.idxEstate++;
                                new LongOperation(LongOperation_Get_Pemanen).execute(String.valueOf(LongOperation_Get_Pemanen));
                            }
                        }
                        break;

                    case LongOperation_Upload_CrashReport:
                        if (skip) {
                            progressDialog.dismiss();
                            questionLanjutDownload();
                        } else if(error){
                            progressDialog.dismiss();
                            questionLanjutDownload();
                        }
                        break;

                    //Backup
                    case LongOperation_BackUp_HMS:
                        progressDialog.dismiss();
                        LongOperation = LongOperation_Get_EstateMapping;
                        startLongOperation();
                        break;
                    //restore
                    case LongOperation_Restore_HMS:
                        restoreHelper.closeRestore();
                        progressDialog.dismiss();
                        break;
                    case LongOperation_RestoreMaster:
                        restoreMasterHelper.closeRestoreMaster();
                        progressDialog.dismiss();
                        break;

                    //
                    //master user
                    case LongOperation_Download_MasterUser:
                        if(skip){
                            Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetMasterUserByEstate",Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }else {
                            progressDialog.dismiss();
                            if (mainMenuActivity.estatesMapping.size() -1 == mainMenuActivity.idxEstate) {
                                mainMenuActivity.idxEstate = 0;
                                new LongOperation(LongOperation_Get_Pemanen).execute(String.valueOf(LongOperation_Get_Pemanen));
                            } else {
                                mainMenuActivity.idxEstate++;
                                new LongOperation(LongOperation_Get_MasterUser).execute(String.valueOf(LongOperation_Get_MasterUser));
                            }
                        }
                        break;

                    case LongOperation_Download_ApplicationConfiguration:
                        if(skip){
                            Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetApplicationConfiguration",Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }else {
                            progressDialog.dismiss();
                            if (mainMenuActivity.estatesMapping.size() -1 == mainMenuActivity.idxEstate) {
                                mainMenuActivity.idxEstate = 0;
                                new LongOperation(LongOperation_Get_MasterUser).execute(String.valueOf(LongOperation_Get_MasterUser));
                            } else {
                                mainMenuActivity.idxEstate++;
                                new LongOperation(LongOperation_Get_ApplicationConfiguration).execute(String.valueOf(LongOperation_Get_ApplicationConfiguration));
                            }
                        }
                        break;

                    case LongOperation_Download_EstateMapping:
                        if(skip){
                            Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetEstateMapping",Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }else {
                            progressDialog.dismiss();
                            if (mainMenuActivity.estatesMapping.size() -1 == mainMenuActivity.idxEstate) {
                                mainMenuActivity.idxEstate = 0;
                                new LongOperation(LongOperation_Get_ApplicationConfiguration).execute(String.valueOf(LongOperation_Get_ApplicationConfiguration));
                            } else {
                                mainMenuActivity.idxEstate++;
                                new LongOperation(LongOperation_Get_EstateMapping).execute(String.valueOf(LongOperation_Get_EstateMapping));
                            }
                        }
                        break;

                    case LongOperation_Map:
                        ((BaseActivity) getActivity()).alertDialogBase.dismiss();
                        Intent i = new Intent(getActivity(), MapActivity.class);
                        startActivityForResult(i,GlobalHelper.RESULT_MAPACTIVITY);
                        break;
                    case LongOperation_My_Pemanen:
//                        Intent intent = new Intent(getActivity(), PemanenActivity.class);
//                        startActivityForResult(intent,GlobalHelper.RESULT_PEMANENACTIVITY);
                        break;
                    case LongOperation_Format_NFC:
//                        Intent i2 = new Intent(getActivity(), FormatNfcActivity.class);
//                        startActivityForResult(i2,GlobalHelper.RESULT_FORMATNFCACTIVITY);
                        break;

                    case LongOperation_Download_SPK:
                        if(skip){
                            Toast.makeText(HarvestApp.getContext(),"Gagal Insert SPK",Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }else {
                            progressDialog.dismiss();

                            if (mainMenuActivity.estatesMapping.size() -1 == mainMenuActivity.idxEstate) {
                                mainMenuActivity.idxEstate = 0;
                                SyncTimeHelper.updateFinishSyncTime(SyncTime.SYNC_DATA_MASTER);
                                new LongOperation(LongOperation_Get_QcBuah).execute(String.valueOf(LongOperation_Get_QcBuah));
                            } else {
                                mainMenuActivity.idxEstate++;
                                new LongOperation(LongOperation_Get_SPK).execute(String.valueOf(LongOperation_Get_SPK));
                            }
                        }
                        break;
                }
            }
        }

        public void publishProgress(JSONObject objProgres){
            onProgressUpdate(String.valueOf(objProgres));
        }

        private TextView getDialogText() {
            if (progressDialog.isShowing()) {
                View v = progressDialog.getWindow().getDecorView();
                return (TextView) v.findViewById(R.id.cust_tv_progress);
            }
            return null;
        }
    }


    @Override
    public void successResponse(ServiceResponse serviceResponse) {
        try {
            switch (serviceResponse.getTag()) {
                // TPH
                case GetTphListByEstate:
//                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Selesai TPH " + setUpDataSyncHelper.estCodeSyncNow());
                    progressDialog.dismiss();
                    responseApi = serviceResponse ;
                    LongOperation = LongOperation_Download_TPH;
                    startLongOperation();
                    break;

                // QCQuestionAnswer
                case GetQCQuestionAnswer:
//                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Selesai TPH " + setUpDataSyncHelper.estCodeSyncNow());
                    progressDialog.dismiss();
                    responseApi = serviceResponse ;
                    LongOperation = LongOperation_Download_QCQuestionAnswer;
                    startLongOperation();
                    break;

                //QcBuah
                case PostQcBuahImages:
                    uploadHelper.uploadData(this,Tag.PostQcBuah,objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                    break;
                case PostQcBuah:
                    uploadHelper.listTransaksiDone(this,serviceResponse.getTag(),objSetUp.getJSONArray("data"),mainMenuActivity.idxUpload);
                    if(objSetUp.getJSONArray("data").length() -1 != mainMenuActivity.idxUpload) {
                        mainMenuActivity.idxUpload ++;
                        uploadHelper.uploadImages(this,Tag.PostQcBuahImages,objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                    }else {
//                        WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Selesai");
                        progressDialog.dismiss();
                        if (mainMenuActivity.estatesMapping.size() -1 == mainMenuActivity.idxEstate) {
                            mainMenuActivity.idxEstate = 0;
                            new LongOperation(LongOperation_Upload_SensusBjr).execute(String.valueOf(LongOperation_Upload_SensusBjr));
                        } else {
                            mainMenuActivity.idxEstate++;
                            new LongOperation(LongOperation_Upload_QcBuah).execute(String.valueOf(LongOperation_Upload_QcBuah));
                        }
                    }
                    break;

                case GetQcBuahByEstate:
//                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Selesai Qc Buah " + setUpDataSyncHelper.estCodeSyncNow());
                    progressDialog.dismiss();
                    responseApi = serviceResponse ;
                    LongOperation = LongOperation_Download_QcBuah;
                    startLongOperation();
                    break;

                //Sensus BJR
                case PostSensusBjrImages:
                    uploadHelper.uploadData(this,Tag.PostSensusBjr,objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                    break;
                case PostSensusBjr:
                    uploadHelper.listTransaksiDone(this,serviceResponse.getTag(),objSetUp.getJSONArray("data"),mainMenuActivity.idxUpload);
                    if(objSetUp.getJSONArray("data").length() -1 != mainMenuActivity.idxUpload) {
                        mainMenuActivity.idxUpload ++;
                        uploadHelper.uploadImages(this,Tag.PostSensusBjrImages,objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                    }else {
//                        WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Selesai");
                        progressDialog.dismiss();
                        if (mainMenuActivity.estatesMapping.size() -1 == mainMenuActivity.idxEstate) {
                            mainMenuActivity.idxEstate = 0;
                            new LongOperation(LongOperation_Upload_QcAncak).execute(String.valueOf(LongOperation_Upload_QcAncak));
                        } else {
                            mainMenuActivity.idxEstate++;
                            new LongOperation(LongOperation_Upload_SensusBjr).execute(String.valueOf(LongOperation_Upload_SensusBjr));
                        }
                    }
                    break;
                case GetSensusBJRByEstate:
//                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Selesai Sensus BJR " + setUpDataSyncHelper.estCodeSyncNow());
                    progressDialog.dismiss();
                    responseApi = serviceResponse ;
                    LongOperation = LongOperation_Download_SensusBjr;
                    startLongOperation();
                    break;

                // Qc Ancak
                case PostQcAncakImages:
                    if(setUpDataSyncHelper.hapusAncakImages(serviceResponse)){
                        uploadHelper.uploadData(this,Tag.PostQcAncak,objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                    }else {
                        uploadHelper.uploadImages(this, Tag.PostQcAncakImages, objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                    }
                    break;
                case PostQcAncak:
                    uploadHelper.listTransaksiDone(this,serviceResponse.getTag(),objSetUp.getJSONArray("data"),mainMenuActivity.idxUpload);
                    if(objSetUp.getJSONArray("data").length() -1 != mainMenuActivity.idxUpload) {
                        mainMenuActivity.idxUpload ++;
                        uploadHelper.uploadImages(this,Tag.PostQcAncakImages,objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                    }else {
//                        WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Selesai");
                        progressDialog.dismiss();
                        if (mainMenuActivity.estatesMapping.size() -1 == mainMenuActivity.idxEstate) {
                            mainMenuActivity.idxEstate = 0;
                            new LongOperation(LongOperation_Upload_SessionQcFotoPanen).execute(String.valueOf(LongOperation_Upload_SessionQcFotoPanen));
                        } else {
                            mainMenuActivity.idxEstate++;
                            new LongOperation(LongOperation_Upload_QcAncak).execute(String.valueOf(LongOperation_Upload_QcAncak));
                        }
                    }
                    break;
                case GetQCMutuAncakByEstate:
//                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Selesai Qc Mutu Ancak " + setUpDataSyncHelper.estCodeSyncNow());
                    progressDialog.dismiss();
                    responseApi = serviceResponse ;
                    LongOperation = LongOperation_Download_QcAncak;
                    startLongOperation();
                    break;

                // Qc Sensus BJR
                case PostSessionQcFotoPanen:
                    uploadHelper.listTransaksiDone(this,serviceResponse.getTag(),objSetUp.getJSONArray("data"),mainMenuActivity.idxUpload);
                    if(objSetUp.getJSONArray("data").length() -1 != mainMenuActivity.idxUpload) {
                        mainMenuActivity.idxUpload ++;
                        uploadHelper.uploadData(this,Tag.PostSessionQcFotoPanen,objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                    }else {
//                        WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Selesai");
                        progressDialog.dismiss();
                        if (mainMenuActivity.estatesMapping.size() -1 == mainMenuActivity.idxEstate) {
                            mainMenuActivity.idxEstate = 0;
                            new LongOperation(LongOperation_Upload_CrashReport).execute(String.valueOf(LongOperation_Upload_CrashReport));
                        } else {
                            mainMenuActivity.idxEstate++;
                            new LongOperation(LongOperation_Upload_SessionQcFotoPanen).execute(String.valueOf(LongOperation_Upload_SessionQcFotoPanen));
                        }
                    }
                    break;
                case GetSessionQcFotoPanen:
//                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Selesai Qc Mutu Ancak " + setUpDataSyncHelper.estCodeSyncNow());
                    progressDialog.dismiss();
                    responseApi = serviceResponse ;
                    LongOperation = LongOperation_Download_SessionQcFotoPanen;
                    startLongOperation();
                    break;

                case GetQCMutuAncakHeaderCounterByUserID:
                    if(setUpDataSyncHelper.UpdateQCMutuAncakCounterByUserID(serviceResponse)){
                        mainMenuActivity.connectionPresenter.GetQCBuahCounterByUserID();
                        progressDialog.dismiss();
                    }else{
                        WidgetHelper.showSnackBar(myCoordinatorLayout,"Gagal GetQCMutuAncakHeaderCounterByUserID");
                        progressDialog.dismiss();
                    }
                    break;
                case GetQCBuahCounterByUserID:
                    if(setUpDataSyncHelper.UpdateQCMutuBuahCounterByUserID(serviceResponse)){
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(HarvestApp.getContext(),"SYNC Berhasil",Toast.LENGTH_SHORT).show();
                            }
                        });
                        SyncTimeHelper.updateFinishSyncTime(SyncTime.SYNC_DATA_TRANSAKSI);
                        setUpDataSyncHelper.setUpLastSync(myCoordinatorLayout);
                        progressDialog.dismiss();
                    }else{
                        WidgetHelper.showSnackBar(myCoordinatorLayout,"Gagal GetQCMutuAncakHeaderCounterByUserID");
                        progressDialog.dismiss();
                    }
                    break;

                // Master User
                case GetMasterUserByEstate:
//                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Selesai Master User " + setUpDataSyncHelper.estCodeSyncNow());
                    progressDialog.dismiss();
                    responseApi = serviceResponse;
                    new LongOperation(LongOperation_Download_MasterUser).execute(String.valueOf(LongOperation_Download_MasterUser));
                    break;

                // Pemanen
                case GetPemanenListByEstate:
//                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Selesai Pemanen " + setUpDataSyncHelper.estCodeSyncNow());
                    progressDialog.dismiss();
                    responseApi = serviceResponse ;
                    new LongOperation(LongOperation_Download_Pemanen).execute(String.valueOf(LongOperation_Download_Pemanen));
                    break;

                //ApplicationConfiguration
                case GetApplicationConfiguration:
//                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Selesai Assistant Afd " + setUpDataSyncHelper.estCodeSyncNow());
                    progressDialog.dismiss();
                    responseApi = serviceResponse;
                    new LongOperation(LongOperation_Download_ApplicationConfiguration).execute(String.valueOf(LongOperation_Download_ApplicationConfiguration));
                    break;

                case GetEstateMapping:
//                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Mapping Estate "+ setUpDataSyncHelper.estCodeSyncNow());
                    progressDialog.dismiss();
                    responseApi = serviceResponse;
                    new LongOperation(LongOperation_Download_EstateMapping).execute(String.valueOf(LongOperation_Download_EstateMapping));
                    break;

                //SPK
                case GetMasterSPKByEstate:
//                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Selesai Master SPK " + setUpDataSyncHelper.estCodeSyncNow());
                    progressDialog.dismiss();
                    responseApi = serviceResponse ;
                    new LongOperation(LongOperation_Download_SPK).execute(String.valueOf(LongOperation_Download_SPK));
                    break;

                case CrashReportList:
//                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Selesai CrashReport");
                    progressDialog.dismiss();
                    CrashUtil.flushCrashReport();
                    questionLanjutDownload();
                    break;

            }
            Log.d("SettingFragment","successResponse " +serviceResponse.getTag());
        } catch (JSONException e) {
            e.printStackTrace();
            objSetUp = new JSONObject();
            WidgetHelper.showSnackBar(myCoordinatorLayout,"Gagal Upload JSONException successResponse");
//            Toast.makeText(HarvestApp.getContext(), "Gagal Upload JSONException successResponse", Toast.LENGTH_LONG).show();
            progressDialog.dismiss();
        }
    }

    @Override
    public void badResponse(ServiceResponse serviceResponse) {
        Log.d("SettingFragment","badResponse " +serviceResponse.getTag());
        Log.d("SettingFragment","badResponse " +serviceResponse.getMessage());
        switch (serviceResponse.getTag()){
            //TPH
            case GetTphListByEstate:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"GET Gagal TPH "+ setUpDataSyncHelper.estCodeSyncNow());
//                Toast.makeText(HarvestApp.getContext(),"GET Gagal TPH",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            //TPH
            case GetQCQuestionAnswer:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"GET Gagal GetQCQuestionAnswer "+ setUpDataSyncHelper.estCodeSyncNow());
//                Toast.makeText(HarvestApp.getContext(),"GET Gagal TPH",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;

            //QcBuah
            case PostQcBuahImages:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Images Gagal Qc Buah");
//                Toast.makeText(HarvestApp.getContext(),"Upload Images Gagal",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case PostQcBuah:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Gagal Qc Buah");
//                Toast.makeText(HarvestApp.getContext(),"Upload Gagal",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case GetQcBuahByEstate:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Gagal Qc Buah "+ setUpDataSyncHelper.estCodeSyncNow());
                progressDialog.dismiss();
                break;

            //Sensus BJR
            case PostSensusBjrImages:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Images Gagal Sensus BJR ");
//                Toast.makeText(HarvestApp.getContext(),"Upload Images Gagal",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case PostSensusBjr:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Gagal Sensus BJR ");
//                Toast.makeText(HarvestApp.getContext(),"Upload Gagal",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case GetSensusBJRByEstate:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Gagal Sensus BJR "+ setUpDataSyncHelper.estCodeSyncNow());
                progressDialog.dismiss();
                break;

            //Qc Ancak
            case PostQcAncakImages:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Images Gagal Qc Ancak");
//                Toast.makeText(HarvestApp.getContext(),"Upload Images Gagal",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case PostQcAncak:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Gagal Qc Ancak ");
//                Toast.makeText(HarvestApp.getContext(),"Upload Gagal",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case GetQCMutuAncakByEstate:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Gagal Qc Ancak " + setUpDataSyncHelper.estCodeSyncNow());
//                Toast.makeText(HarvestApp.getContext(),"Upload Gagal",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;

            //Qc Session Foto Panen
            case PostSessionQcFotoPanen:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Gagal Qc Session Foto Panen ");
//                Toast.makeText(HarvestApp.getContext(),"Upload Gagal",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case GetSessionQcFotoPanen:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Gagal Qc Session Foto Panen " + setUpDataSyncHelper.estCodeSyncNow());
//                Toast.makeText(HarvestApp.getContext(),"Upload Gagal",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;

            case GetQCMutuAncakHeaderCounterByUserID:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Update Count Mutu Ancak " + setUpDataSyncHelper.estCodeSyncNow());
//                Toast.makeText(HarvestApp.getContext(),"Update Count Transaksi SPB",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case GetQCBuahCounterByUserID:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Update Count Mutu Buah " + setUpDataSyncHelper.estCodeSyncNow());
//                Toast.makeText(HarvestApp.getContext(),"Update Count Transaksi SPB",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;

            case CrashReportList:
                progressDialog.dismiss();
                questionLanjutDownload();
                break;

            // Master User
            case GetMasterUserByEstate:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"GET Gagal Master User " + setUpDataSyncHelper.estCodeSyncNow());
//                Toast.makeText(HarvestApp.getContext(), "GET Gagal Master User", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;

            case GetPemanenListByEstate:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"GET Gagal Pemanen "+ setUpDataSyncHelper.estCodeSyncNow());
//                Toast.makeText(HarvestApp.getContext(), "GET Gagal Pemanen", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;

            case GetApplicationConfiguration:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"GET Gagal ApplicationConfiguration "+ setUpDataSyncHelper.estCodeSyncNow());
//                Toast.makeText(HarvestApp.getContext(), "GET Gagal Operator", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;

            //SPK
            case GetMasterSPKByEstate:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"GET Gagal MasterSPK ");
//                Toast.makeText(HarvestApp.getContext(), "GET Gagal Operator", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;

            case GetEstateMapping:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"GET Gagal EstateMapping "+ setUpDataSyncHelper.estCodeSyncNow());
//                Toast.makeText(HarvestApp.getContext(), "GET Gagal Operator", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
        }
    }
}
