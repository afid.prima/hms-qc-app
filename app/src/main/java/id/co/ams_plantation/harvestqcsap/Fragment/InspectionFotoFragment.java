package id.co.ams_plantation.harvestqcsap.Fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import co.ceryle.segmentedbutton.SegmentedButtonGroup;
import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.adapter.RowPemanenAdapter;
import id.co.ams_plantation.harvestqcsap.model.DataQcFotoPanen;
import id.co.ams_plantation.harvestqcsap.model.QcFotoPanen;
import id.co.ams_plantation.harvestqcsap.model.SessionQcFotoPanen;
import id.co.ams_plantation.harvestqcsap.ui.InspectionFotoActivity;
import id.co.ams_plantation.harvestqcsap.util.GlobalHelper;
import id.co.ams_plantation.harvestqcsap.util.WidgetHelper;

/**
 * Created on : 21,October,2021
 * Author     : Afid
 */

public class InspectionFotoFragment extends Fragment {

    final int LongOperation_Save_Data_Qc = 1;

//    ImageView imagecacnel;
//    ImageView imagesview;
    ImageView sliderLayout;
    LinearLayout rl_ipf_takepicture;

    TextView tvIdPanen;
    TextView tvSudahQc;
    TextView tvKrani;
    TextView tvTahunTanam;
    TextView tvWaktuPanen;
    TextView tvBlock;
    TextView tvTph;
    TextView tvTotalJanjang;
    TextView tvJanjangNormal;
    TextView tvBuahMentah;
    TextView tvTangkaiPanjang;
    TextView tvBuahLewatMatang;
    TextView tvBuahAbnormal;
    TextView tvBuahDimakanTikus;
    TextView tvBusukDanJangkos;
    TextView tvBrondola;
    TextView tvKetBrondolan;
    TextView tvKetAlasBrondolan;
    RecyclerView rvPemanen;
    RadioButton rbFotoSelisih;
    RadioGroup rgKondisiFoto;
    RadioGroup rgKondisiBrondolan;
    RadioGroup rgKondisiAlasBrondolan;
    ImageView ivBrdl;
    ImageView ivAlasBrdl;
    CheckBox cbTPHRepair;
    CheckBox cbBuahTidakDisusun;
    SegmentedButtonGroup sbgKet;
    LinearLayout lWarning;
    LinearLayout lnextPrevImg;
    LinearLayout lprevImg;
    LinearLayout lnextImg;
    LinearLayout lprev;
    LinearLayout lsave;
    LinearLayout lnext;
    View detailPanen;
    View formInpection;

    InspectionFotoActivity inspectionFotoActivity;
    ArrayList<File> ALselectedImage;
    QcFotoPanen qcFotoPanen;

    int idxFoto = 0;

    public static InspectionFotoFragment getInstance(){
        InspectionFotoFragment fragment = new InspectionFotoFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.inpection_foto_layout,null,false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getActivity() instanceof InspectionFotoActivity){
            inspectionFotoActivity = ((InspectionFotoActivity) getActivity());
        }

//        imagecacnel = view.findViewById(R.id.imagecacnel);
//        imagesview = view.findViewById(R.id.imagesview);
        rl_ipf_takepicture = view.findViewById(R.id.rl_ipf_takepicture);
        sliderLayout = view.findViewById(R.id.sliderLayout);
        sbgKet = view.findViewById(R.id.sbgKet);
        detailPanen = view.findViewById(R.id.detailPanen);
        formInpection = view.findViewById(R.id.formInpection);
        tvIdPanen = view.findViewById(R.id.tvIdPanen);
        tvSudahQc = view.findViewById(R.id.tvSudahQc);
        tvKrani = view.findViewById(R.id.tvKrani);
        tvTahunTanam = view.findViewById(R.id.tvTahunTanam);
        tvWaktuPanen = view.findViewById(R.id.tvWaktuPanen);
        tvBlock = view.findViewById(R.id.tvBlock);
        tvTph = view.findViewById(R.id.tvTph);
        tvTotalJanjang = view.findViewById(R.id.tvTotalJanjang);
        tvJanjangNormal = view.findViewById(R.id.tvJanjangNormal);
        tvBuahMentah = view.findViewById(R.id.tvBuahMentah);
        tvTangkaiPanjang = view.findViewById(R.id.tvTangkaiPanjang);
        tvBuahLewatMatang = view.findViewById(R.id.tvBuahLewatMatang);
        tvBuahAbnormal = view.findViewById(R.id.tvBuahAbnormal);
        tvBuahDimakanTikus = view.findViewById(R.id.tvBuahDimakanTikus);
        tvBusukDanJangkos = view.findViewById(R.id.tvBusukDanJangkos);
        tvBrondola = view.findViewById(R.id.tvBrondola);
        tvKetBrondolan = view.findViewById(R.id.tvKetBrondolan);
        tvKetAlasBrondolan = view.findViewById(R.id.tvKetAlasBrondolan);
        rvPemanen = view.findViewById(R.id.rvPemanen);
        rbFotoSelisih = view.findViewById(R.id.rbFotoSelisih);
        rgKondisiFoto = view.findViewById(R.id.rgKondisiFoto);
        rgKondisiBrondolan = view.findViewById(R.id.rgKondisiBrondolan);
        rgKondisiAlasBrondolan = view.findViewById(R.id.rgKondisiAlasBrondolan);
        ivBrdl = view.findViewById(R.id.ivBrdl);
        ivAlasBrdl = view.findViewById(R.id.ivAlasBrdl);
        cbTPHRepair = view.findViewById(R.id.cbTPHRepair);
        cbBuahTidakDisusun = view.findViewById(R.id.cbBuahTidakDisusun);
        lWarning  = view.findViewById(R.id.lWarning);
        lnextPrevImg  = view.findViewById(R.id.lnextPrevImg);
        lprevImg  = view.findViewById(R.id.lprevImg);
        lnextImg  = view.findViewById(R.id.lnextImg);
        lprev = view.findViewById(R.id.lprev);
        lsave = view.findViewById(R.id.lsave);
        lnext = view.findViewById(R.id.lnext);

        if(inspectionFotoActivity.sessionQcFotoPanen.getStatus() == SessionQcFotoPanen.Status_Upload_Qc){
            lsave.setVisibility(View.GONE);
        }

        ViewGroup.LayoutParams params = rl_ipf_takepicture.getLayoutParams();
        params.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 150, getResources().getDisplayMetrics());;
        rl_ipf_takepicture.setLayoutParams(params);
        sbgKet.setPosition(1);
        setFormPotition(1);

        ivBrdl.setImageDrawable(new IconicsDrawable(getActivity())
            .icon(MaterialDesignIconic.Icon.gmi_check_circle)
            .colorRes(R.color.Green)
            .sizeDp(24));

        ivAlasBrdl.setImageDrawable(new IconicsDrawable(getActivity())
                .icon(MaterialDesignIconic.Icon.gmi_check_circle)
                .colorRes(R.color.Green)
                .sizeDp(24));

        ALselectedImage = new ArrayList<>();
        inspectionFotoActivity.idxFotoQc = 0;
        setQcFotoPanen();
        setUpValueQcFotoPanen();

        sbgKet.setOnClickedButtonListener(new SegmentedButtonGroup.OnClickedButtonListener() {
            @Override
            public void onClickedButton(int position) {
                setFormPotition(position);
            }
        });

        rgKondisiBrondolan.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.rbFotoBrondolanAda:
                        if(qcFotoPanen.getHasilPanen().getBrondolan() > 0) {
                            ivBrdl.setImageDrawable(new IconicsDrawable(getActivity())
                                    .icon(MaterialDesignIconic.Icon.gmi_check_circle)
                                    .colorRes(R.color.Green)
                                    .sizeDp(24));
                        }else{
                            ivBrdl.setImageDrawable(new IconicsDrawable(getActivity())
                                    .icon(MaterialDesignIconic.Icon.gmi_close_circle)
                                    .colorRes(R.color.Red)
                                    .sizeDp(24));
                        }
                        break;
                    case R.id.rbFotoBrondolanTidak:
                        if(qcFotoPanen.getHasilPanen().getBrondolan() == 0) {
                            ivBrdl.setImageDrawable(new IconicsDrawable(getActivity())
                                    .icon(MaterialDesignIconic.Icon.gmi_check_circle)
                                    .colorRes(R.color.Green)
                                    .sizeDp(24));
                        }else{
                            ivBrdl.setImageDrawable(new IconicsDrawable(getActivity())
                                    .icon(MaterialDesignIconic.Icon.gmi_close_circle)
                                    .colorRes(R.color.Red)
                                    .sizeDp(24));
                        }
                        break;
                }
            }
        });

        rgKondisiAlasBrondolan.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.rbAlasBrondolanAda:
                        if(qcFotoPanen.getHasilPanen().isAlasanBrondolan()) {
                            ivAlasBrdl.setImageDrawable(new IconicsDrawable(getActivity())
                                    .icon(MaterialDesignIconic.Icon.gmi_check_circle)
                                    .colorRes(R.color.Green)
                                    .sizeDp(24));
                        }else{
                            ivAlasBrdl.setImageDrawable(new IconicsDrawable(getActivity())
                                    .icon(MaterialDesignIconic.Icon.gmi_close_circle)
                                    .colorRes(R.color.Red)
                                    .sizeDp(24));
                        }
                        break;
                    case R.id.rbAlasBrondolanTidak:
                        if(!qcFotoPanen.getHasilPanen().isAlasanBrondolan()) {
                            ivAlasBrdl.setImageDrawable(new IconicsDrawable(getActivity())
                                    .icon(MaterialDesignIconic.Icon.gmi_check_circle)
                                    .colorRes(R.color.Green)
                                    .sizeDp(24));
                        }else{
                            ivAlasBrdl.setImageDrawable(new IconicsDrawable(getActivity())
                                    .icon(MaterialDesignIconic.Icon.gmi_close_circle)
                                    .colorRes(R.color.Red)
                                    .sizeDp(24));
                        }
                        break;
                }
            }
        });

        lprev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!GlobalHelper.isDoubleClick()) {
                    prevImages();
                }
            }
        });

        lnext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!GlobalHelper.isDoubleClick()) {
                    nextImages();
                }
            }
        });

        lsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!GlobalHelper.isDoubleClick()) {
                    new LongOperation().execute(String.valueOf(LongOperation_Save_Data_Qc));
                }
            }
        });
    }

    public void prevImages(){
        if(inspectionFotoActivity.idxFotoQc == 0){
            inspectionFotoActivity.idxFotoQc = inspectionFotoActivity.sessionQcFotoPanen.getQcFotoPanen().size() - 1;
            setQcFotoPanen();
            setUpValueQcFotoPanen();
        }else{
            inspectionFotoActivity.idxFotoQc--;
            setQcFotoPanen();
            setUpValueQcFotoPanen();
        }
    }

    public void nextImages(){
        if(inspectionFotoActivity.idxFotoQc == inspectionFotoActivity.sessionQcFotoPanen.getQcFotoPanen().size() - 1){
            inspectionFotoActivity.idxFotoQc = 0;
            setQcFotoPanen();
            setUpValueQcFotoPanen();
        }else{
            inspectionFotoActivity.idxFotoQc++;
            setQcFotoPanen();
            setUpValueQcFotoPanen();
        }
    }

    private boolean validastionQcFotoPanen(){
        int chjedRB = rgKondisiFoto.getCheckedRadioButtonId();
        if(chjedRB < 1){
            Toast.makeText(getActivity(),"Mohon Pilih Kondisi Foto",Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void saveQcFotoPanen(){
        DataQcFotoPanen dataQcFotoPanen = new DataQcFotoPanen(
                System.currentTimeMillis(),
                GlobalHelper.getUser().getUserID(),
                qcFotoPanen.getIdTPanen(),
                GlobalHelper.getEstate().getEstCode()

        );
        switch (rgKondisiFoto.getCheckedRadioButtonId()){
            case R.id.rbFotoSesuai:
                dataQcFotoPanen.setStatus(DataQcFotoPanen.STATUS_FOTO_SESUAI);
                break;
            case R.id.rbFotoSelisih:
                dataQcFotoPanen.setStatus(DataQcFotoPanen.STATUS_FOTO_SELISIH);
                break;
            case R.id.rbFotoKurangTepat:
                dataQcFotoPanen.setStatus(DataQcFotoPanen.STATUS_FOTO_KURANG_TEPAT);
                break;
            case R.id.rbFotoKosongBenar:
                dataQcFotoPanen.setStatus(DataQcFotoPanen.STATUS_FOTO_KOSONG);
                break;
            case R.id.rbFotoPenipuan:
                dataQcFotoPanen.setStatus(DataQcFotoPanen.STATUS_FOTO_MANIPULASI);
                break;
            case R.id.rbFotoAtasFoto:
                dataQcFotoPanen.setStatus(DataQcFotoPanen.STATUS_FOTO_PENIPUAN);
                break;
        }

        switch (rgKondisiBrondolan.getCheckedRadioButtonId()){
            case R.id.rbFotoBrondolanAda:
                dataQcFotoPanen.setBrondolan(DataQcFotoPanen.STATUS_BRONDOLAN_ADA);
                break;
            case R.id.rbFotoBrondolanTidak:
                dataQcFotoPanen.setBrondolan(DataQcFotoPanen.STATUS_BRONDOLAN_TIDAK_ADA);
                break;
        }

        switch (rgKondisiAlasBrondolan.getCheckedRadioButtonId()){
            case R.id.rbAlasBrondolanAda:
                dataQcFotoPanen.setAlasbrondolan(DataQcFotoPanen.STATUS_ALASBRONDOLAN_ADA);
                break;
            case R.id.rbAlasBrondolanTidak:
                dataQcFotoPanen.setAlasbrondolan(DataQcFotoPanen.STATUS_ALASBRONDOLAN_TIDAK_ADA);
                break;
        }

        if(cbTPHRepair.isChecked()){
            dataQcFotoPanen.setTphRepair(DataQcFotoPanen.STATUS_TPH_REPAIR);
        }else{
            dataQcFotoPanen.setTphRepair(DataQcFotoPanen.STATUS_TPH_OK);
        }

        if(cbBuahTidakDisusun.isChecked()){
            dataQcFotoPanen.setBuahTidakDiSusun(DataQcFotoPanen.STATUS_BUAH_TIDAK_DISUSUN);
        }else{
            dataQcFotoPanen.setBuahTidakDiSusun(DataQcFotoPanen.STATUS_BUAH_DISUSUN);
        }

        qcFotoPanen.setDataQcFotoPanen(dataQcFotoPanen);
        HashMap<String ,QcFotoPanen> qcFotoPanenHashMap = new HashMap<String,QcFotoPanen>();
        for(int i = 0 ; i < inspectionFotoActivity.sessionQcFotoPanen.getQcFotoPanen().size(); i++){
            QcFotoPanen qcFotoPanen1 = inspectionFotoActivity.sessionQcFotoPanen.getQcFotoPanen().get(i);
            qcFotoPanenHashMap.put(qcFotoPanen1.getIdTPanen(),qcFotoPanen1);
        }
        qcFotoPanenHashMap.put(qcFotoPanen.getIdTPanen(),qcFotoPanen);
        ArrayList<QcFotoPanen> qcFotoPanens = new ArrayList<QcFotoPanen>(qcFotoPanenHashMap.values());

        Collections.sort(qcFotoPanens, new Comparator<QcFotoPanen>() {
            @Override
            public int compare(QcFotoPanen o1, QcFotoPanen o2) {
                return o2.getCreateDate() > o1.getCreateDate() ? -1 : (o2.getCreateDate() < o1.getCreateDate()) ? 1 : 0;
            }
        });

        inspectionFotoActivity.sessionQcFotoPanen.setQcFotoPanen(qcFotoPanens);
        inspectionFotoActivity.sessionFotoPanenHelper.updateCurentSession(
                inspectionFotoActivity.sessionQcFotoPanen
        );
        inspectionFotoActivity.cekHasilQc();
    }

    private void setQcFotoPanen() {
        if(inspectionFotoActivity.sessionQcFotoPanen.getQcFotoPanen() == null){
            Toast.makeText(getActivity(),"Detail Foto Panen Tidak Ada ",Toast.LENGTH_LONG).show();
            inspectionFotoActivity.backProses();
            return;
        }
        if (inspectionFotoActivity.idxFotoQc > inspectionFotoActivity.sessionQcFotoPanen.getQcFotoPanen().size() - 1) {
            inspectionFotoActivity.idxFotoQc--;
            setQcFotoPanen();
        } else {
            qcFotoPanen = inspectionFotoActivity.sessionQcFotoPanen.getQcFotoPanen().get(inspectionFotoActivity.idxFotoQc);
        }
    }

    private void setUpValueQcFotoPanen(){
        ALselectedImage.clear();
        if(qcFotoPanen.getFoto() != null) {
            for (String s : qcFotoPanen.getFoto()) {
                File file = new File(s);
                ALselectedImage.add(file);
            }
        }
        setupimageslide();

        SimpleDateFormat simpleDateFormat =new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
        String idPanen = qcFotoPanen.getIdTPanen();
        if(qcFotoPanen.getTph().getNamaTph().equals("999")){
            idPanen += "*";
            lWarning.setVisibility(View.VISIBLE);
        }else{
            lWarning.setVisibility(View.GONE);
        }
        idPanen += "\nSudah Qc :"+ inspectionFotoActivity.persen + " %";
        tvIdPanen.setText(idPanen);
        tvKrani.setText(qcFotoPanen.getKraniName());
        tvTahunTanam.setText(String.valueOf(qcFotoPanen.getTahunTanam()));
        tvWaktuPanen.setText(simpleDateFormat.format(qcFotoPanen.getCreateDate()));
        tvBlock.setText(qcFotoPanen.getTph().getBlock());
        tvTph.setText(qcFotoPanen.getTph().getNamaTph());
        tvTotalJanjang.setText(String.valueOf(qcFotoPanen.getHasilPanen().getTotalJanjang()));
        tvJanjangNormal.setText(String.valueOf(qcFotoPanen.getHasilPanen().getJanjangNormal()));
        tvBuahMentah.setText(String.valueOf(qcFotoPanen.getHasilPanen().getBuahMentah()));
        tvTangkaiPanjang.setText(String.valueOf(qcFotoPanen.getHasilPanen().getTangkaiPanjang()));
        tvBuahLewatMatang.setText(String.valueOf(qcFotoPanen.getHasilPanen().getBuahLewatMatang()));
        tvBuahAbnormal.setText(String.valueOf(qcFotoPanen.getHasilPanen().getBuahAbnormal()));
        tvBuahDimakanTikus.setText(String.valueOf(qcFotoPanen.getHasilPanen().getBuahDimakanTikus()));
        tvBusukDanJangkos.setText(String.valueOf(qcFotoPanen.getHasilPanen().getBusukNJangkos()));
        tvBrondola.setText(String.valueOf(qcFotoPanen.getHasilPanen().getBrondolan()));
        rbFotoSelisih.setText("Pencatatan Panen Janjang : "+String.valueOf(qcFotoPanen.getHasilPanen().getTotalJanjang())+"\n" +
                "Hitungan Janjang Tidak Sesuai Foto");
        tvKetBrondolan.setText("Pencatatan Panen Brondolan : "+ String.valueOf(qcFotoPanen.getHasilPanen().getBrondolan())+"Kg");
        String alasB = qcFotoPanen.getHasilPanen().isAlasanBrondolan() == true ? "Ada" : "Tidak Ada";
        tvKetAlasBrondolan.setText("Pencatatan Panen Alas Brondolan : "+ alasB);

        RowPemanenAdapter adapter = new RowPemanenAdapter(getActivity(),qcFotoPanen.getKongsi().getSubPemanens());
        rvPemanen.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvPemanen.setAdapter(adapter);

        if(qcFotoPanen.getHasilPanen().getBrondolan() > 0){
            rgKondisiBrondolan.check(R.id.rbFotoBrondolanAda);
        }else{
            rgKondisiBrondolan.check(R.id.rbFotoBrondolanTidak);
        }

        if(qcFotoPanen.getHasilPanen().isAlasanBrondolan()){
            rgKondisiAlasBrondolan.check(R.id.rbAlasBrondolanAda);
        }else{
            rgKondisiAlasBrondolan.check(R.id.rbAlasBrondolanTidak);
        }

        tvSudahQc.setText("");
        cbTPHRepair.setChecked(false);
        cbBuahTidakDisusun.setChecked(false);
        rgKondisiFoto.clearCheck();

        if(qcFotoPanen.getDataQcFotoPanen() != null){
            DataQcFotoPanen dataQcFotoPanen = qcFotoPanen.getDataQcFotoPanen();
            tvSudahQc.setText("Sudah Di Qc "+simpleDateFormat.format(dataQcFotoPanen.getCreateDate()));
            switch (dataQcFotoPanen.getStatus()){
                case DataQcFotoPanen.STATUS_FOTO_SESUAI:
                    rgKondisiFoto.check(R.id.rbFotoSesuai);
                    break;
                case DataQcFotoPanen.STATUS_FOTO_SELISIH:
                    rgKondisiFoto.check(R.id.rbFotoSelisih);
                    break;
                case DataQcFotoPanen.STATUS_FOTO_KURANG_TEPAT:
                    rgKondisiFoto.check(R.id.rbFotoKurangTepat);
                    break;
                case DataQcFotoPanen.STATUS_FOTO_MANIPULASI:
                    rgKondisiFoto.check(R.id.rbFotoPenipuan);
                    break;
                case DataQcFotoPanen.STATUS_FOTO_PENIPUAN:
                    rgKondisiFoto.check(R.id.rbFotoAtasFoto);
                    break;
                case DataQcFotoPanen.STATUS_FOTO_KOSONG:
                    rgKondisiFoto.check(R.id.rbFotoKosongBenar);
                    break;
            }

            switch (dataQcFotoPanen.getBrondolan()){
                case DataQcFotoPanen.STATUS_BRONDOLAN_ADA:
                    rgKondisiBrondolan.check(R.id.rbFotoBrondolanAda);
                    break;
                case DataQcFotoPanen.STATUS_BRONDOLAN_TIDAK_ADA:
                    rgKondisiBrondolan.check(R.id.rbFotoBrondolanTidak);
                    break;
            }

            switch (dataQcFotoPanen.getAlasbrondolan()){
                case DataQcFotoPanen.STATUS_ALASBRONDOLAN_ADA:
                    rgKondisiAlasBrondolan.check(R.id.rbAlasBrondolanAda);
                    break;
                case DataQcFotoPanen.STATUS_ALASBRONDOLAN_TIDAK_ADA:
                    rgKondisiAlasBrondolan.check(R.id.rbAlasBrondolanTidak);
                    break;
            }

            switch (dataQcFotoPanen.getTphRepair()){
                case DataQcFotoPanen.STATUS_TPH_OK:
                    cbTPHRepair.setChecked(false);
                    break;
                case DataQcFotoPanen.STATUS_TPH_REPAIR:
                    cbTPHRepair.setChecked(true);
                    break;
            }


            switch (dataQcFotoPanen.getBuahTidakDiSusun()){
                case DataQcFotoPanen.STATUS_BUAH_DISUSUN:
                    cbBuahTidakDisusun.setChecked(false);
                    break;
                case DataQcFotoPanen.STATUS_BUAH_TIDAK_DISUSUN:
                    cbBuahTidakDisusun.setChecked(true);
                    break;
            }

        }

    }


    private void setupimageslide(){
        Set<File> sf = new HashSet<>();
        sf.addAll(ALselectedImage);
        ALselectedImage.clear();
        ALselectedImage.addAll(sf);
//        imagecacnel.setVisibility(View.GONE);
//        imagesview.setVisibility(View.GONE);
        sliderLayout.setVisibility(View.VISIBLE);
        idxFoto = 0;
        setUpImage();

        if(ALselectedImage.size() > 1 ){
            lnextPrevImg.setVisibility(View.GONE);
        }

        sliderLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WidgetHelper.showImagesZoom(getActivity(),ALselectedImage);
            }
        });

        lnextImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(idxFoto == ALselectedImage.size() - 1){
                    idxFoto = 0;
                }else{
                    idxFoto++;
                }
                setUpImage();
            }
        });

        lprevImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(idxFoto == 0){
                    idxFoto = 0;
                }else{
                    idxFoto--;
                }
                setUpImage();
            }
        });

//        imagecacnel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //Toast.makeText(GSApp.getContext(),"slider getCurrentPosition " + sliderLayout.getCurrentPosition(),Toast.LENGTH_SHORT).show();
////                try {
////                    if (sliderLayout.getCurrentPosition() == 0 && ALselectedImage.size() == 1) {
////                        ALselectedImage.clear();
////                        sliderLayout.removeAllSliders();
////                        sliderLayout.setVisibility(View.GONE);
////                        imagecacnel.setVisibility(View.GONE);
////                        imagesview.setVisibility(View.GONE);
////                    } else {
////                        ALselectedImage.remove(sliderLayout.getCurrentPosition());
////                        sliderLayout.removeSliderAt(sliderLayout.getCurrentPosition());
////                    }
////                }catch (Exception e){
////                    Log.e("sliderLayout", e.toString());
////                }
//            }
//        });
//        imagesview.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                WidgetHelper.showImagesZoom(getActivity(),ALselectedImage);
//            }
//        });
    }

    private void setUpImage(){

        File file  = ALselectedImage.get(idxFoto);
        if(file.exists()){
            Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
            sliderLayout.setImageBitmap(myBitmap);
            sliderLayout.setScaleType(ImageView.ScaleType.FIT_XY);
        }
    }

    private void setFormPotition(int position){
        switch (position) {
            case 0: {
                detailPanen.setVisibility(View.VISIBLE);
                formInpection.setVisibility(View.GONE);
                break;
            }
            case 1: {
                detailPanen.setVisibility(View.GONE);
                formInpection.setVisibility(View.VISIBLE);
                break;
            }
        }
    }

    private class LongOperation extends AsyncTask<String, String, String> {
        private AlertDialog alertDialog;
        boolean validasi = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            alertDialog = WidgetHelper.showWaitingDialog(getActivity(), getResources().getString(R.string.wait));
            validasi = validastionQcFotoPanen();
        }

        @Override
        protected String doInBackground(String... params) {
            if(validasi) {
                switch (Integer.parseInt(params[0])) {
                    case LongOperation_Save_Data_Qc: {
                        saveQcFotoPanen();
                    }
                }
            }

            return params[0];
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (validasi) {
                switch (Integer.parseInt(result)) {
                    case LongOperation_Save_Data_Qc: {
                        nextImages();
                        if(inspectionFotoActivity.persen >= 99){
                            inspectionFotoActivity.backProses();
                        }
                    }
                }
            }
            alertDialog.dismiss();
        }
    }
}
