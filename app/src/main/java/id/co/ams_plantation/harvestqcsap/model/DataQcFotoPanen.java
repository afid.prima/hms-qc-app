package id.co.ams_plantation.harvestqcsap.model;

/**
 * Created on : 27,October,2021
 * Author     : Afid
 */

public class DataQcFotoPanen {
    public static final int STATUS_FOTO_SESUAI = 1;
    public static final int STATUS_FOTO_SELISIH = 2;
    public static final int STATUS_FOTO_KURANG_TEPAT = 3;
    public static final int STATUS_FOTO_MANIPULASI = 4;
    public static final int STATUS_FOTO_PENIPUAN = 5;
    public static final int STATUS_FOTO_KOSONG = 6;

    public static final int STATUS_BRONDOLAN_ADA = 1;
    public static final int STATUS_BRONDOLAN_TIDAK_ADA = 0;

    public static final int STATUS_ALASBRONDOLAN_ADA = 1;
    public static final int STATUS_ALASBRONDOLAN_TIDAK_ADA = 0;

    public static final int STATUS_TPH_REPAIR = 1;
    public static final int STATUS_TPH_OK = 0;

    public static final int STATUS_BUAH_TIDAK_DISUSUN = 1;
    public static final int STATUS_BUAH_DISUSUN = 0;

    int status;
    int brondolan;
    int alasbrondolan;
    int tphRepair;
    int buahTidakDiSusun;
    long createDate;
    String createBy;
    String idTPanen;
    String estCode;

    public DataQcFotoPanen() {
    }

    public DataQcFotoPanen(long createDate, String createBy, String idTPanen, String estCode) {
        this.createDate = createDate;
        this.createBy = createBy;
        this.idTPanen = idTPanen;
        this.estCode = estCode;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getBrondolan() {
        return brondolan;
    }

    public void setBrondolan(int brondolan) {
        this.brondolan = brondolan;
    }

    public int getAlasbrondolan() {
        return alasbrondolan;
    }

    public void setAlasbrondolan(int alasbrondolan) {
        this.alasbrondolan = alasbrondolan;
    }

    public int getTphRepair() {
        return tphRepair;
    }

    public void setTphRepair(int tphRepair) {
        this.tphRepair = tphRepair;
    }

    public int getBuahTidakDiSusun() {
        return buahTidakDiSusun;
    }

    public void setBuahTidakDiSusun(int buahTidakDiSusun) {
        this.buahTidakDiSusun = buahTidakDiSusun;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getIdTPanen() {
        return idTPanen;
    }

    public void setIdTPanen(String idTPanen) {
        this.idTPanen = idTPanen;
    }

    public String getEstCode() {
        return estCode;
    }

    public void setEstCode(String estCode) {
        this.estCode = estCode;
    }
}
