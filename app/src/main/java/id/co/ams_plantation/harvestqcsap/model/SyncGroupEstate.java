package id.co.ams_plantation.harvestqcsap.model;

import java.util.ArrayList;

/**
 * Created on : 23,April,2021
 * Author     : Afid
 */

public class SyncGroupEstate {
    ArrayList<Estate> estates;
    int idxEstate;

    public SyncGroupEstate(ArrayList<Estate> estates, int idxEstate) {
        this.estates = estates;
        this.idxEstate = idxEstate;
    }

    public ArrayList<Estate> getEstates() {
        return estates;
    }

    public void setEstates(ArrayList<Estate> estates) {
        this.estates = estates;
    }

    public int getIdxEstate() {
        return idxEstate;
    }

    public void setIdxEstate(int idxEstate) {
        this.idxEstate = idxEstate;
    }
}
