package id.co.ams_plantation.harvestqcsap.model;

import java.io.File;

/**
 * Created on : 16,December,2021
 * Author     : Afid
 */

public class FilePdf {
    String remarks;
    String url;
    String namaFile;
    File fullPath;

    public FilePdf(String remarks, String url, String namaFile, File fullPath) {
        this.remarks = remarks;
        this.url = url;
        this.namaFile = namaFile;
        this.fullPath = fullPath;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getNamaFile() {
        return namaFile;
    }

    public void setNamaFile(String namaFile) {
        this.namaFile = namaFile;
    }

    public File getFullPath() {
        return fullPath;
    }

    public void setFullPath(File fullPath) {
        this.fullPath = fullPath;
    }
}
