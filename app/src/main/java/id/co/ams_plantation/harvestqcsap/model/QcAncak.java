package id.co.ams_plantation.harvestqcsap.model;

import java.util.ArrayList;

import id.co.ams_plantation.harvestqcsap.HarvestApp;
import id.co.ams_plantation.harvestqcsap.R;

public class QcAncak {

    public static final int COLOR_ANCAK = HarvestApp.getContext().getResources().getColor(R.color.Blue);
    public static final int COLOR_TEXT_ANCAK = HarvestApp.getContext().getResources().getColor(R.color.OrangeRed);

    public static final int NotSave = -1;
    public static final int Save = 0;
    public static final int Upload = 1;

    String idQcAncak;
    TPH tph;
    int createBy;
    long startTime;
    long finishTime;
    int totalBuahMatahari;
    int totalOverPrun;
    int totalSusunanPelepah;
    int totalPelepahSengkelan;
    int totalJanjangPanen;
    int totalBuahTinggal;
    int totalBrondolan;
    int brondolanDiTph;
    double latitude;
    double longitude;
    int status;
    String penyebabBrondolan;
    String kondisiAncak;
    String kondisiAncakText;

    ArrayList<QcAncakPohon> qcAncakPohons;

    public QcAncak(){}

    public String getIdQcAncak() {
        return idQcAncak;
    }

    public void setIdQcAncak(String idQcAncak) {
        this.idQcAncak = idQcAncak;
    }

    public TPH getTph() {
        return tph;
    }

    public void setTph(TPH tph) {
        this.tph = tph;
    }

    public int getCreateBy() {
        return createBy;
    }

    public void setCreateBy(int createBy) {
        this.createBy = createBy;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(long finishTime) {
        this.finishTime = finishTime;
    }

    public int getTotalBuahMatahari() {
        return totalBuahMatahari;
    }

    public void setTotalBuahMatahari(int totalBuahMatahari) {
        this.totalBuahMatahari = totalBuahMatahari;
    }

    public int getTotalOverPrun() {
        return totalOverPrun;
    }

    public void setTotalOverPrun(int totalOverPrun) {
        this.totalOverPrun = totalOverPrun;
    }

    public int getTotalSusunanPelepah() {
        return totalSusunanPelepah;
    }

    public void setTotalSusunanPelepah(int totalSusunanPelepah) {
        this.totalSusunanPelepah = totalSusunanPelepah;
    }

    public int getTotalPelepahSengkelan() {
        return totalPelepahSengkelan;
    }

    public void setTotalPelepahSengkelan(int totalPelepahSengkelan) {
        this.totalPelepahSengkelan = totalPelepahSengkelan;
    }

    public int getTotalJanjangPanen() {
        return totalJanjangPanen;
    }

    public void setTotalJanjangPanen(int totalJanjangPanen) {
        this.totalJanjangPanen = totalJanjangPanen;
    }

    public int getTotalBuahTinggal() {
        return totalBuahTinggal;
    }

    public void setTotalBuahTinggal(int totalBuahTinggal) {
        this.totalBuahTinggal = totalBuahTinggal;
    }

    public int getTotalBrondolan() {
        return totalBrondolan;
    }

    public void setTotalBrondolan(int totalBrondolan) {
        this.totalBrondolan = totalBrondolan;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<QcAncakPohon> getQcAncakPohons() {
        return qcAncakPohons;
    }

    public void setQcAncakPohons(ArrayList<QcAncakPohon> qcAncakPohons) {
        this.qcAncakPohons = qcAncakPohons;
    }

    public int getBrondolanDiTph() {
        return brondolanDiTph;
    }

    public void setBrondolanDiTph(int brondolanDiTph) {
        this.brondolanDiTph = brondolanDiTph;
    }

    public String getPenyebabBrondolan() {
        return penyebabBrondolan;
    }

    public void setPenyebabBrondolan(String penyebabBrondolan) {
        this.penyebabBrondolan = penyebabBrondolan;
    }

    public String getKondisiAncak() {
        return kondisiAncak;
    }

    public void setKondisiAncak(String kondisiAncak) {
        this.kondisiAncak = kondisiAncak;
    }

    public String getKondisiAncakText() {
        return kondisiAncakText;
    }

    public void setKondisiAncakText(String kondisiAncakText) {
        this.kondisiAncakText = kondisiAncakText;
    }
}
