package id.co.ams_plantation.harvestqcsap.ui;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.esri.android.map.MapView;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import butterknife.ButterKnife;
import id.co.ams_plantation.harvestqcsap.Fragment.QcMutuAncakInputEntryFragment;
import id.co.ams_plantation.harvestqcsap.Fragment.QcSensusBjrInputFragment;
import id.co.ams_plantation.harvestqcsap.Fragment.TphInputFragment;
import id.co.ams_plantation.harvestqcsap.HarvestApp;
import id.co.ams_plantation.harvestqcsap.model.Estate;
import id.co.ams_plantation.harvestqcsap.model.TransaksiPanen;
import id.co.ams_plantation.harvestqcsap.util.GlobalHelper;
import id.co.ams_plantation.harvestqcsap.Fragment.QcMutuBuahFragment;
import id.co.ams_plantation.harvestqcsap.Fragment.QcSensusBjrFragment;
import id.co.ams_plantation.harvestqcsap.util.TransaksiPanenHelper;
import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.model.DataNitrit;
import id.co.ams_plantation.harvestqcsap.model.QcMutuBuah;
import id.co.ams_plantation.harvestqcsap.util.NfcHelper;
import id.co.ams_plantation.harvestqcsap.util.WidgetHelper;
import id.zelory.compressor.Compressor;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

import static id.co.ams_plantation.harvestqcsap.util.GlobalHelper.FASTESTINTERVAL;
import static id.co.ams_plantation.harvestqcsap.util.GlobalHelper.INTERVAL;


public abstract class BaseActivity extends AppCompatActivity {
//    public BluetoothHelper bluetoothHelper;
    public LocationManager mLocationManager;
    public Location currentlocation;
    public GoogleApiClient mGoogleApiClient;
    public LocationRequest mLocationRequest;

    public RelativeLayout rlMap;
    public MapView mapView;
    public ImageView zoomToLocation;

    final int LongOperation_CekDataPassing_Biru = 0;
    final int LongOperation_CekDataPassing_Hijau = 1;
    final int LongOperation_CekDataPassing_Red = 2;

    Activity activitySelected;
    public JSONObject objSeledted = new JSONObject();

    public AlertDialog alertDialogBase;
    public int idxUpload;
    public View viewIndicator;

    public int idxEstate;
    public ArrayList<Estate> estatesMapping;

    public String idNFC = null;

    protected abstract void initPresenter();

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
//        bluetoothHelper = new BluetoothHelper(this);
        initPresenter();

        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1,
                1f, mLocationListener);
    }

    public void setNavigationTitle(TextView toolbar, @StringRes int title) {
        toolbar.setTextColor(Color.parseColor("#FFFFFF"));
        toolbar.setText(title);
    }

    public void setNavigationBack(TextView toolbar, ImageView backImg, @StringRes int title) {
        backImg.setVisibility(View.VISIBLE);
        backImg.setImageDrawable(new IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_arrow_left)
                .colorRes(R.color.White)
                .sizeDp(24));
        toolbar.setTextColor(Color.parseColor("#FFFFFF"));
        toolbar.setText(title);
        toolbar.setOnClickListener(view -> finish());
    }

    public void setNavigationBackWithMenu(TextView toolbar, ImageView backImg, ImageView menuImg, @StringRes int title) {
        backImg.setVisibility(View.VISIBLE);
        menuImg.setVisibility(View.VISIBLE);
        backImg.setImageDrawable(new IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_arrow_left)
                .colorRes(R.color.White)
                .sizeDp(24));
        menuImg.setImageDrawable(new IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_menu)
                .colorRes(R.color.White)
                .sizeDp(24));
        toolbar.setTextColor(Color.parseColor("#FFFFFF"));
        toolbar.setText(title);
        toolbar.setOnClickListener(view -> finish());
    }

    public void cekDataPassing(String value, Activity activity) {
        try {
            activitySelected = activity;
            objSeledted = new JSONObject(value);
            switch (objSeledted.getInt("z")) {
                case GlobalHelper.TYPE_NFC_BIRU: {
//                    addFlagInNFC(value,activity,GlobalHelper.TYPE_NFC_BIRU);
                    new LongOperation().execute(String.valueOf(LongOperation_CekDataPassing_Biru));
                    break;
                }
                case GlobalHelper.TYPE_NFC_HIJAU: {
                    new LongOperation().execute(String.valueOf(LongOperation_CekDataPassing_Hijau));
                    break;
                }
                case GlobalHelper.TYPE_NFC_RED: {
                    new LongOperation().execute(String.valueOf(LongOperation_CekDataPassing_Red));
                    break;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(HarvestApp.getContext(), HarvestApp.getContext().getResources().getString(R.string.data_not_valid), Toast.LENGTH_SHORT).show();
        }
    }

    public void setIntent(Context activity, Class<?> aClass) {
        Intent intent = new Intent(activity, aClass);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source, int type) {
                if (currentLocationOK()) {
                    File picture = Compressor.getDefault(BaseActivity.this).compressToFile(imageFiles.get(0));
                    Location location = currentlocation;
                    if (GlobalHelper.TAG_CAMERA == GlobalHelper.TAG_CAMERA_NEW_TPH_MAP_ACTIVITY || GlobalHelper.TAG_CAMERA == GlobalHelper.TAG_CAMERA_NEW_LANGSIRAN_MAP_ACTIVITY) {
                        try {
                            SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.LATLON_PENDAFTARAN, Context.MODE_PRIVATE);
                            String objLatlon = preferences.getString(HarvestApp.LATLON_PENDAFTARAN, null);

                            JSONObject obj = new JSONObject(objLatlon);
                            location.setLongitude(obj.getDouble("lon"));
                            location.setLatitude(obj.getDouble("lat"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    picture = GlobalHelper.photoAddStampPanen(picture);

                    picture = GlobalHelper.photoAddStamp(picture, location);
                    if (picture != null)
                        if (picture.exists()) {
                            for (File file : imageFiles) file.delete();
                        }
                    String namaFile = System.currentTimeMillis() + "_" + String.format("%.5f", location.getLongitude()) + "_" + String.format("%.5f", location.getLatitude());
                    try {
                        File newdir, selectedImage;
                        switch (GlobalHelper.TAG_CAMERA) {
                            case GlobalHelper.TAG_CAMERA_QC_SENSUSBJR: {
                                newdir = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 + "/" + GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_QC_SENSUS_BJR]);
                                selectedImage = GlobalHelper.moveFile(picture, newdir, namaFile);
                                QcSensusBjrInputFragment fragment = (QcSensusBjrInputFragment) getSupportFragmentManager().findFragmentById(R.id.content_container);
                                ((QcSensusBjrInputFragment) fragment).setImages(selectedImage);
                                break;
                            }
                            case GlobalHelper.TAG_CAMERA_QC_MUTUBUAH:{
                                newdir = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 + "/" + GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_QC_MUTU_BUAH]);
                                selectedImage = GlobalHelper.moveFile(picture, newdir, namaFile);
                                TphInputFragment fragment = (TphInputFragment) getSupportFragmentManager().findFragmentById(R.id.content_container);
                                ((TphInputFragment) fragment).setImages(selectedImage);
                                break;
                            }
                            case GlobalHelper.TAG_CAMERA_QC_ANCAK:{
                                newdir = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 + "/" + GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_QC_MUTU_ANCAK]);
                                selectedImage = GlobalHelper.moveFile(picture, newdir, namaFile);
                                QcMutuAncakInputEntryFragment fragment = (QcMutuAncakInputEntryFragment) getSupportFragmentManager().findFragmentById(R.id.content_container);
                                ((QcMutuAncakInputEntryFragment) fragment).setImages(selectedImage);
                                break;
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(HarvestApp.getContext(), "GPS Tidak Akurat", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                if (source == EasyImage.ImageSource.CAMERA) {
                    File photoFile = EasyImage.lastlyTakenButCanceledPhoto(BaseActivity.this);
                    if (photoFile != null) photoFile.delete();
                }
                return;
            }
        });
        switch (requestCode) {
            case GlobalHelper.REQUEST_ENABLE_BT: {
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a session
                    Toast.makeText(this, "Bluetooth Aktif", Toast.LENGTH_SHORT).show();
                } else {
                    // User did not enable Bluetooth or an error occured
                    Toast.makeText(this, R.string.bt_not_enabled,
                            Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
    }

    public final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            currentlocation = location;
            if (currentLocationOK()) {
                if (BaseActivity.this instanceof QcMutuAncakActivity) {
                    if (((QcMutuAncakActivity) BaseActivity.this).searchingGPS != null) {
                        if (((QcMutuAncakActivity) BaseActivity.this).searchingGPS.isShown()) {
                            ((QcMutuAncakActivity) BaseActivity.this).searchingGPS.dismiss();
                        }
                    }
                } else if (BaseActivity.this instanceof QcMutuBuahActivity) {
                    if (((QcMutuBuahActivity) BaseActivity.this).searchingGPS != null) {
                        if (((QcMutuBuahActivity) BaseActivity.this).searchingGPS.isShown()) {
                            ((QcMutuBuahActivity) BaseActivity.this).searchingGPS.dismiss();
                        }
                    }
                } else if (BaseActivity.this instanceof QcSensusBjrActivity) {
                    if (((QcSensusBjrActivity) BaseActivity.this).searchingGPS != null) {
                        if (((QcSensusBjrActivity) BaseActivity.this).searchingGPS.isShown()) {
                            ((QcSensusBjrActivity) BaseActivity.this).searchingGPS.dismiss();
                        }
                    }
                }
            }
            Log.v("change Location", currentlocation.getLongitude() + "," + currentlocation.getLatitude());
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
            Log.v("onStatusChanged L", s);
        }

        @Override
        public void onProviderEnabled(String s) {
            Log.v("onProviderEnabled L", s);
        }

        @Override
        public void onProviderDisabled(String s) {
            Log.v("onProviderDisabled L", s);
        }
    };

    public void starLocationUpdate() {
        // Request location updates
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTESTINTERVAL);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        currentlocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
    }

    public void locationChangeListener(Location location){
        currentlocation = location;
        if (currentLocationOK()) {
            if (BaseActivity.this instanceof QcMutuBuahActivity) {
                if (((QcMutuBuahActivity) BaseActivity.this).searchingGPS != null) {
                    if (((QcMutuBuahActivity) BaseActivity.this).searchingGPS.isShown()) {
                        ((QcMutuBuahActivity) BaseActivity.this).searchingGPS.dismiss();
                    }
                }
            }
        }
        Log.v("change Location", currentlocation.getLongitude() + "," + currentlocation.getLatitude());
    }

    public boolean currentLocationOK(){
        if(currentlocation != null){
            if(currentlocation.getAccuracy() >= GlobalHelper.MAX_ACCURACY_CALIBRATION ){
                if(this instanceof EntryActivity){

                }else if(this instanceof ActiveActivity){

                }else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                seIndicatorAccuracy();
                                alertDialogBase = WidgetHelper.showCalibrationGpsDialog(BaseActivity.this);
                            } catch (WindowManager.BadTokenException e) {
                                Log.e("WindowManagerBad ", e.toString());
                            }
                        }
                    });
                }
                return false;
            }else if(currentlocation.getAccuracy() < GlobalHelper.MAX_ACCURACY){
                seIndicatorAccuracy();
                return true;
            }
        }
        seIndicatorAccuracy();
        return false;
    }

    private void seIndicatorAccuracy(){
        if(viewIndicator != null){
            if(currentlocation == null){
                viewIndicator.setBackgroundColor(getResources().getColor(R.color.Red));
            }else if (currentlocation.getAccuracy() >= GlobalHelper.MAX_ACCURACY + 30) {
                viewIndicator.setBackgroundColor(getResources().getColor(R.color.Red));
            }else if (currentlocation.getAccuracy() >= GlobalHelper.MAX_ACCURACY) {
                viewIndicator.setBackgroundColor(getResources().getColor(R.color.Yellow));
            }else if (currentlocation.getAccuracy() < GlobalHelper.MAX_ACCURACY) {
                viewIndicator.setBackgroundColor(getResources().getColor(R.color.Green));
            }
        }
    }

    private class LongOperation extends AsyncTask<String, Void, String> {
        private AlertDialog alertDialogAllpoin ;
        TransaksiPanen transaksiPanen;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            transaksiPanen = null;
            alertDialogAllpoin = WidgetHelper.showWaitingDialog(BaseActivity.this,getResources().getString(R.string.wait));
        }

        @Override
        protected String doInBackground(String... strings) {
            switch (Integer.parseInt(strings[0])) {
                case LongOperation_CekDataPassing_Biru: {
                    transaksiPanen = TransaksiPanenHelper.toDecompre(objSeledted);
                    if(idNFC != null){
                        transaksiPanen.setIdNfc(idNFC);
                    }
                    break;
                }
            }
            return strings[0];
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            switch (Integer.parseInt(result)) {
                case LongOperation_CekDataPassing_Biru:{
                    if(transaksiPanen != null) {
                        if (transaksiPanen.getEstCode().equals(GlobalHelper.getEstate().getEstCode())) {
                            if (activitySelected instanceof MainMenuActivity) {
                                Fragment fragment = ((MainMenuActivity.TabPagerAdapter) ((MainMenuActivity) activitySelected).viewPager.getAdapter()).getItem(((MainMenuActivity) activitySelected).viewPager.getCurrentItem());
                                if (fragment instanceof QcMutuBuahFragment) {
                                    QcMutuBuah qcMutuBuah = new QcMutuBuah();
                                    qcMutuBuah.setIdTPanen(transaksiPanen.getIdTPanen());
                                    qcMutuBuah.setTransaksiPanenKrani(transaksiPanen);

                                    Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_BUAH);
                                    ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
                                    Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("param1", transaksiPanen.getIdTPanen()));
                                    if (cursor.size() > 0) {
                                        for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                                            DataNitrit dataNitrit = (DataNitrit) iterator.next();
                                            Gson gson = new Gson();
                                            qcMutuBuah = gson.fromJson(dataNitrit.getValueDataNitrit(), QcMutuBuah.class);
                                            break;
                                        }
                                    }
                                    db.close();

                                    Gson gson = new Gson();
                                    Intent intent = new Intent(activitySelected, QcMutuBuahActivity.class);
                                    intent.putExtra("qcMutuBuah", gson.toJson(qcMutuBuah).toString());
                                    startActivityForResult(intent, GlobalHelper.RESULT_QC_MUTU_BUAH);
                                } else if (fragment instanceof QcSensusBjrFragment) {
                                    Gson gson = new Gson();
                                    Intent intent = new Intent(activitySelected, QcSensusBjrActivity.class);
                                    intent.putExtra("transaksiPanen", gson.toJson(transaksiPanen).toString());
                                    startActivityForResult(intent, GlobalHelper.RESULT_QC_SENSUS_BJR);
                                } else {
                                    Toast.makeText(HarvestApp.getContext(), "Dimenu Ini Tidak Terima Kartu Panen", Toast.LENGTH_SHORT).show();
                                }


                            } else if (activitySelected instanceof QcMutuBuahActivity) {


                            } else if (activitySelected instanceof QcSensusBjrActivity) {
                                ((QcSensusBjrActivity) activitySelected).selectedTransaksiPanen = transaksiPanen;
                                ((QcSensusBjrActivity) activitySelected).getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.content_container, QcSensusBjrInputFragment.getInstance())
                                        .commit();
                            }
                        }else {
                            Toast.makeText(HarvestApp.getContext(), HarvestApp.getContext().getResources().getString(R.string.diffrent_estate) + " Transaksi", Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Toast.makeText(HarvestApp.getContext(), HarvestApp.getContext().getResources().getString(R.string.data_not_valid) + " Transaksi", Toast.LENGTH_SHORT).show();
                    }
                    break;
                }
            }

            if (alertDialogAllpoin != null) {
                alertDialogAllpoin.cancel();
            }
        }
    }
}
