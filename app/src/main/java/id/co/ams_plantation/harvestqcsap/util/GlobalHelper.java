package id.co.ams_plantation.harvestqcsap.util;

import static id.co.ams_plantation.harvestqcsap.model.SessionQcFotoPanen.Status_Deleted;
import static id.co.ams_plantation.harvestqcsap.model.SessionQcFotoPanen.Status_Upload_Qc;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.media.ExifInterface;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.Base64;
import android.util.Log;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.esri.android.map.MapView;
import com.esri.android.map.ogc.kml.KmlLayer;
import com.esri.core.geometry.GeometryEngine;
import com.esri.core.geometry.LinearUnit;
import com.esri.core.geometry.Point;
import com.esri.core.geometry.Polygon;
import com.esri.core.geometry.SpatialReference;
import com.esri.core.geometry.Unit;
import com.esri.core.map.ogc.kml.KmlNode;
import com.google.gson.Gson;
import com.mikepenz.iconics.utils.Utils;
import com.mobapphome.mahencryptorlib.MAHEncryptor;
import com.robinhood.ticker.TickerUtils;
import com.robinhood.ticker.TickerView;

import net.rehacktive.waspdb.WaspDb;
import net.rehacktive.waspdb.WaspFactory;
import net.rehacktive.waspdb.WaspHash;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.RecordIterable;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.channels.FileChannel;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.TreeMap;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import id.co.ams_plantation.harvestqcsap.HarvestApp;
import id.co.ams_plantation.harvestqcsap.encryptor.Encrypts;
import id.co.ams_plantation.harvestqcsap.model.HasilPanen;
import id.co.ams_plantation.harvestqcsap.BuildConfig;
import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.model.Block;
import id.co.ams_plantation.harvestqcsap.model.CounterTransaksi;
import id.co.ams_plantation.harvestqcsap.model.DataNitrit;
import id.co.ams_plantation.harvestqcsap.model.DynamicParameterPenghasilan;
import id.co.ams_plantation.harvestqcsap.model.Estate;
import id.co.ams_plantation.harvestqcsap.model.Pemanen;
import id.co.ams_plantation.harvestqcsap.model.SPK;
import id.co.ams_plantation.harvestqcsap.model.SessionQcFotoPanen;
import id.co.ams_plantation.harvestqcsap.model.StampImage;
import id.co.ams_plantation.harvestqcsap.model.SupervisionList;
import id.co.ams_plantation.harvestqcsap.model.User;

/**
 * Created by PASPC02009 on 3/7/2018.
 */

public class GlobalHelper {

    public static final int TYPE_2DMAP = 1;
    public static final int TYPE_KMLBLOCK = 2;
    public static final int TYPE_TPK = 3;
    public static final int TYPE_VKM = 4;

    public static final String PASSWORD_ENCRYPT = "KopiNikmatGakBikinKembung";
    public static final String DB_MASTER = "DBMasterAMS";
    public static final String DB_PASS = "AMS12345PALM";
    public static final String SELECTED_USER = "User";
    public static final String SELECTED_ESTATE = "SelEstate";
    public static final String SELECTED_MODULE = "SelMod";
    public static final String EXTERNAL_DIR_FILES = "/AMSApp/files";
    public static final String EXTERNAL_DIR_FILES_HMS_DB = BuildConfig.BUILD_VARIANT.equals("dev") ? "/AMSApp/files/QCHMSSAPTESTING/db":"/AMSApp/files/QCHMSSAP/db" ;
    public static final String EXTERNAL_DIR_FILES_HMS_DB2 = BuildConfig.BUILD_VARIANT.equals("dev") ? "/AMSApp/files/QCHMSSAPTESTING/db2":"/AMSApp/files/QCHMSSAP/db2";
    public static final String EXTERNAL_DIR_FILES_HMS_BACKUP_DB = BuildConfig.BUILD_VARIANT.equals("dev") ? "/AMSApp/files/QCHMSSAPTESTING_BACKUP/":"/AMSApp/files/QCHMSSAP_BACKUP/";
    public static final String EXTERNAL_DIR_FILES_HMS_TEMP = BuildConfig.BUILD_VARIANT.equals("dev") ? "/AMSApp/files/QCHMSSAPTESTING_TEMP/":"/AMSApp/files/QCHMSSAP_TEMP/";
    public static final String EXTERNAL_DIR_FILES_HMS_TEMP_MASTER = BuildConfig.BUILD_VARIANT.equals("dev") ? "/AMSApp/files/QCHMSSAPTESTING_TEMP_MASTER/":"/AMSApp/files/QCHMSSAP_TEMP_MASTER/";
    public static final String REPORT_DIR_FILES = "/Report HMSQc";
    public static final String FONT_CORBERT = "CorbertCondensed-Regular.otf";
    public static final String LAST_SYNC = "LAST_SYNC";
    public static final String LAST_GETDATA = "LAST_GETDATA";
    public static final String COUNT_BAD_RESPONSE = "COUNT_BAD_RESPONSE";
    public static final String UPLOAD_HISTORY = "UPLOAD_HISTORY";
    public static final String ESTATE_FULL_NAME = "EstateFullName";
    public static final String USER_AUTHORIZED_MAPS = "UserAuthMaps";

    public static final String [] LIST_FOLDER = {
            "QR",
            "TPH",
//            "TRANSAKSI_TPH",
//            "ANGKUT",
//            "TRANSAKSI_ANGKUT",
//            "SELECT_ANGKUT",
            "QC_SENSUS_BJR",
//            "LANGSIRAN",
//            "SPB",
//            "ANGKUT_SPB",
            "SELECTED_QC_ANCAK",
            "SELECTED_QC_MUTUBUAH",
            "SELECTED_QC_ANCAK_POHON",
//            "TPanenSupervision",
            "QC_MUTU_BUAH",
            "QC_MUTU_ANCAK",
            "QC_FOTO_PANEN",
//            "TRANSAKSI_TPH_NFC",
//            "TRANSAKSI_ANGKUT_NFC",
//            "REQUEST_ASSISTENSI",
//            "COUNTNFC_SPB"
    };
    public static final int LIST_FOLDER_QR = 0;
    public static final int LIST_FOLDER_TPH = 1;
//    public static final int LIST_FOLDER_TRANSAKSI_TPH = 2;
//    public static final int LIST_FOLDER_ANGKUT = 3;
//    public static final int LIST_FOLDER_TRANSAKSI_ANGKUT = 4;
//    public static final int LIST_FOLDER_SELECT_ANGKUT = 5;
    public static final int LIST_FOLDER_QC_SENSUS_BJR = 2;
//    public static final int LIST_FOLDER_LANGSIRAN = 7;
//    public static final int LIST_FOLDER_SPB = 8;
//    public static final int LIST_FOLDER_ANGKUT_SPB = 9;
    public static final int LIST_FOLDER_SELECTED_QC_ANCAK = 3;
    public static final int LIST_FOLDER_SELECTED_QC_MUTUBUAH = 4;
    public static final int LIST_FOLDER_SELECTED_QC_ANCAK_POHON = 5;
//    public static final int LIST_FOLDER_TPanenSupervision = 13;
    public static final int LIST_FOLDER_QC_MUTU_BUAH = 6;
    public static final int LIST_FOLDER_QC_MUTU_ANCAK = 7;
    public static final int LIST_FOLDER_QC_FOTO_PANEN = 8;
//    public static final int LIST_FOLDER_TRANSAKSI_TPH_NFC = 16;
//    public static final int LIST_FOLDER_TRANSAKSI_ANGKUT_NFC = 17;
//    public static final int LIST_FOLDER_REQUEST_ASSISTENSI = 18;
//    public static final int LIST_FOLDER_COUNTNFC_SPB = 19;

    public static final int TYPE_NFC_SALAH = 999;
    public static final int TYPE_NFC_BIRU = 0;
    public static final int TYPE_NFC_HIJAU = 1;
    public static final int TYPE_NFC_RED = 2;
    public static final int TYPE_ISI_ANGKUT = 9;

    public static final String [] LIST_NFC = {"TYPE NFC PANEN","TYPE NFC ANGKUT"};

    public static final String TABEL_TPH = "TPH";
    public static final String TABEL_PEMANEN = "PEMANEN";
    public static final String TABEL_QCQuestionAnswer = "QCQuestionAnswer";
//    public static final String TABLE_TRANSAKSI_TPH = "TRANSAKSI_TPH";
//    public static final String TABLE_ANGKUT = "ANGKUT";
//    public static final String TABLE_ANGKUT_SPB = "ANGKUTSPB";
//    public static final String TABLE_TRANSAKSI_ANGKUT = "TRANSAKSI_ANGKUT";
//    public static final String TABLE_LANGSIRAN = "LANGSIRAN";
//    public static final String TABLE_TRANSAKSI_SPB = "TRANSAKSI_SPB";
    public static final String TABLE_USER_HARVEST = "TABLE_USER_HARVEST";
//    public static final String TABLE_PKS = "PKS";
    public static final String TABLE_QC_BUAH = "TABLE_QC_BUAH";
    public static final String TABLE_QC_BJR = "TABLE_QC_BJR";
    public static final String TABLE_QC_ANCAK = "TABLE_QC_ANCAK";
    public static final String TABLE_QC_ANCAK_POHON = "TABLE_QC_ANCAK_POHON";
    public static final String TABLE_QC_FOTO_PANEN = "TABLE_QC_FOTO_PANEN";
//    public static final String TABEL_OPERATOR = "OPERATOR";
//    public static final String TABLE_VEHICLE = "VEHICLE";
//    public static final String TABLE_AFDELING_ASSISTANT = "AFDELING_ASSISTANT";
    public static final String TABLE_APPLICATION_CONFIGURATION = "APPLICATION_CONFIGURATION";
//    public static final String TABLE_TPANENGANGLIST = "TPANENGANGLIST";
//    public static final String TABLE_TPANENSUPERVISIONLIST = "TPANENSUPERVISIONLIST";
//    public static final String TABLE_BJRINFORMATION = "BJRINFORMATION";
//    public static final String TABLE_ISCCINFORMATION = "ISCCINFORMATION";
//    public static final String TABLE_TRANSAKSI_TPH_NFC = "TRANSAKSI_TPH_NFC";
//    public static final String TABLE_TRANSAKSI_ANGKUT_NFC = "TRANSAKSI_ANGKUT_NFC";
//    public static final String TABLE_REQUEST_ASSISTENSI = "REQUEST_ASSISTENSI";
//    public static final String TABLE_ESTATE_AND_ASSISTENSI = "STATE_AND_ASSISTENSI";
    public static final String TABLE_SPK = "SPK";
    public static final String TABLE_COUNTNFC_SPB = "COUNTNFCSPB";
    public static final String TABLE_ESTATE_MAPPING = "ESTATE_MAPPING";
    public static final String TABLE_INFORMATION = "INFORMATION";

    public static final String [] LIST_TABEL = {
            TABEL_TPH,
            TABEL_PEMANEN,
//            TABLE_TRANSAKSI_TPH,
//            TABLE_ANGKUT,
//            TABLE_ANGKUT_SPB,
//            TABLE_TRANSAKSI_ANGKUT,
//            TABLE_LANGSIRAN,
//            TABLE_TRANSAKSI_SPB,
            TABLE_USER_HARVEST,
//            TABLE_PKS,
            TABLE_QC_BUAH,
            TABLE_QC_BJR,
            TABLE_QC_ANCAK,
            TABLE_QC_ANCAK_POHON,
            TABLE_QC_FOTO_PANEN,
//            TABEL_OPERATOR,
//            TABLE_VEHICLE,
//            TABLE_AFDELING_ASSISTANT,
            TABLE_APPLICATION_CONFIGURATION,
//            TABLE_TPANENGANGLIST,
//            TABLE_TPANENSUPERVISIONLIST,
//            TABLE_BJRINFORMATION,
//            TABLE_ISCCINFORMATION,
//            TABLE_TRANSAKSI_TPH_NFC,
//            TABLE_TRANSAKSI_ANGKUT_NFC,
//            TABLE_REQUEST_ASSISTENSI,
//            TABLE_ESTATE_AND_ASSISTENSI,
            TABLE_SPK,
            TABLE_COUNTNFC_SPB,
            TABLE_ESTATE_MAPPING,
            TABLE_INFORMATION
    };

    //setting codeID
    public static final int SETTING_SCANQR = 0;
    public static final int SETTING_SYNC = 1;
    public static final int SETTING_BACKUP =3;
    public static final int SETTING_MAP = 4;
    public static final int SETTING_PEMANEN = 5;
    public static final int SETTING_FORMATNFC = 6;
    public static final int SETTING_HISTORY_SYNC = 7;
    public static final int SETTING_REPORT = 8;
    public static final int SETTING_ABOUT_APPLICATION = 9;
    public static final int SETTING_RESTOREMASTER = 12;

    // Intent request codes
    public static final int REQUEST_CONNECT_DEVICE = 1;
    public static final int REQUEST_ENABLE_BT = 2;
    public static final int REQUEST_CHOSE_BMP = 3;
    public static final int REQUEST_CAMER = 4;
//    public static final int REQUEST_SCAN_QR = 5;

    public static int TAG_CAMERA;
    public static final int TAG_CAMERA_NEW_TPH = 0;
    public static final int TAG_CAMERA_TRANSAKSI_TPH = 1;
    public static final int TAG_CAMERA_NEW_LANGSIRAN = 2;
    public static final int TAG_CAMERA_NEW_TPH_MAP_ACTIVITY = 3;
    public static final int TAG_CAMERA_NEW_LANGSIRAN_MAP_ACTIVITY = 4;
    public static final int TAG_CAMERA_QC_SENSUSBJR = 5;
    public static final int TAG_CAMERA_TRANSAKSI_ANGKUT = 6;
    public static final int TAG_CAMERA_TRANSAKSI_SPB = 7;
    public static final int TAG_CAMERA_QC_MUTUBUAH = 8;
    public static final int TAG_CAMERA_QC_ANCAK = 9;

    public static final int RESULT_TPHACTIVITY = 10;
    public static final int RESULT_SCAN_QR  = 11;
    public static final int RESULT_ANGKUTACTIVITY = 12;
    public static final int RESULT_SPBACTIVITY = 13;
    public static final int RESULT_MAPACTIVITY = 14;
    public static final int RESULT_PEMANENACTIVITY = 15;
    public static final int RESULT_QC_MUTU_BUAH = 16;
    public static final int RESULT_QC_SENSUS_BJR = 17;
    public static final int RESULT_QC_MUTU_ANCAK = 18;
    public static final int RESULT_FORMATNFCACTIVITY = 19;
    public static final int RESULT_REKONSILIASI = 20;
    public static final int RESULT_ASSISTENSI = 21;
    public static final int RESULT_COUNTNFCACTIVITY = 22;
    public static final int RESULT_QC_FOTO_PANEN = 23;

    public static final float MAX_ACCURACY = BuildConfig.BUILD_VARIANT.equals("dev") ?
            999999999:
            55;
    public static final float MAX_ACCURACY_CALIBRATION = BuildConfig.BUILD_VARIANT.equals("dev") ?
            999999999:
            350;
    public static final int MARKER_SIZE = 7;
    public static final int MARKER_SIZE_TRANSKASI = 7;
    public static final int MARKER_TOUCH_SIZE = 16;
    public static final float GARISTEPI_SIZE = 1.5f;
    public static final int TEXT_SIZE_TRANSKASI = 9;
    public static final float RADIUS =
            BuildConfig.BUILD_VARIANT.equals("dev") ?
            999999999:
            55;
    public static final float MAX_ACCURACY_MAPMENU = BuildConfig.BUILD_VARIANT.equals("dev") ?
            999999999:
            45;
    public static final float MAX_RADIUS_MAPMENU = BuildConfig.BUILD_VARIANT.equals("dev") ?
            999999999:
            45;
    public static final int SIZE_RESTORE = 10;
    public static final int PARTITION_UPLOAD = 50;
    public static final int PARTITION_UPLOAD_ANCAK = 1;
    public static final int PARTITION_UPLOAD_IMAGE = 5;
    public static final int MAX_BAD_RESPONSE = 2;
    public static final int MAX_BARIS_TPH = 300;
    public static final int MAX_HISTORY_SYNC = 20;
    public static final int MAX_LAST_DAY_DATA = 3;
    public static final int MAX_TIME_SAME_TPH_AND_SAME_PEMANEN = 60 * 60000;
    public static final int MAX_ANGKUT_IN_TRANSAKSI = 14;
    public static final int MAX_JANJANG_TIMBANG = 3;
    public static final int MAX_PEMANEN_KELOMPOK = 6;
    public static final int MAX_PEMANEN_MEKANIS = 7;
    public static final int MAX_TANGGAL_PANEN = 9;
    public static final long MAX_FOTO_PANEN = 6 * 24 * 3600 * 1000;
    public static final int MIN_PERSEN_QC_FOTO = 70;

    // Constants for Google Location API
    public static final int REQUEST_RESOLVE_ERROR = 1001;
    public static final int INTERVAL = 2000;
    public static final int FASTESTINTERVAL = 1000;
    public static final int NUMLOOKUPS = 10;
    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static final int TIMES_OUT_NETWORK = 10 * 60;

    public static final String PATH_SELMOD_FILE =
//            BuildConfig.BUILD_VARIANT.equals("dev") ?
//            Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES +"/dev/db/"+ Encrypts.encrypt("ModFile"):
            Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES +"/db/"+ Encrypts.encrypt("ModFile");
    public static final int NOTIF_TOKEN_DAYS = 7;

    public static long lastClickTime = 0;
    public static final long DOUBLE_CLICK_TIME_DELTA = 500;


    public static HashMap<String,User> dataAllUser;
    public static HashMap<String,Pemanen> dataPemanen;

    public static HashMap<String, ArrayList<Polygon>> polygonCollection = null;

    public static final String FORMAT_NFC_PANEN = "{z:0}";
    public static final String FORMAT_NFC_ANGKUT = "{z:1}";

    public static void setUpAllData(){
        dataPemanen = getAllPemanen();
        dataAllUser = getAllUser();
    }

    public static void setupHMSFolder(){
        File extStatisticsFolder = new File( BuildConfig.BUILD_VARIANT.equals("dev")?  Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES + "/QCHMSSAPTESTING" : Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES + "/QCHMSSAP");
        if(!extStatisticsFolder.exists()) extStatisticsFolder.mkdirs();

        extStatisticsFolder = new File( Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB);
        if(!extStatisticsFolder.exists()) extStatisticsFolder.mkdirs();

        extStatisticsFolder = new File( Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB +"/"+ GlobalHelper.getEstate().getEstCode());
        if(!extStatisticsFolder.exists()) extStatisticsFolder.mkdirs();

        extStatisticsFolder = new File( Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_BACKUP_DB);
        if(!extStatisticsFolder.exists()) extStatisticsFolder.mkdirs();

        extStatisticsFolder = new File( Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 );
        if(!extStatisticsFolder.exists()) extStatisticsFolder.mkdirs();

        extStatisticsFolder = new File( Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_TEMP);
        if(!extStatisticsFolder.exists()) extStatisticsFolder.mkdirs();

        extStatisticsFolder = new File( Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_TEMP_MASTER);
        if(!extStatisticsFolder.exists()) extStatisticsFolder.mkdirs();

        extStatisticsFolder = new File( Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_TEMP_MASTER +"/"+ GlobalHelper.getEstate().getEstCode());
        if(!extStatisticsFolder.exists()) extStatisticsFolder.mkdirs();

        for (String s: LIST_FOLDER) {
            File extStatisticsFolderIn = new File( Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +'/'+ s);
            if(!extStatisticsFolderIn.exists()) extStatisticsFolderIn.mkdirs();
        }

        for(String s : LIST_TABEL){
            File extStatisticsFolderIn = new File( Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB +"/"+ GlobalHelper.getEstate().getEstCode() +'/'+ Encrypts.encrypt(s));
            if(!extStatisticsFolderIn.exists()) extStatisticsFolderIn.mkdirs();
        }

//        File dbPemanen = new File(GlobalHelper.getDatabasePathHMS() + "/" +Encrypts.encrypt(TABEL_PEMANEN) + "/" +Encrypts.encrypt(TABEL_PEMANEN)+ ".db");
//        if(!dbPemanen.exists()) {
////        setupDataDummyUntukPemanen
//            Pemanen pemanen = new Pemanen("01326_MER", "01326", "MUJI LAGIONO", "PN042", "THP1", "MER", "AFD-4");
////        WaspHash tblPemanen = getTableHash(TABEL_PEMANEN);
////        tblPemanen.flush();
//            Gson gson = new Gson();
//            Nitrite db = getTableNitrit(TABEL_PEMANEN);
//            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
//            DataNitrit dataNitrit = new DataNitrit(pemanen.getKodePemanen(), gson.toJson(pemanen));
//            repository.insert(dataNitrit);
////        tblPemanen.put(pemanen.getKodePemanen(),gson.toJson(pemanen));
//
//            pemanen = new Pemanen("01327_MER", "01327", "SUDARTO", "PN042", "THP1", "MER", "AFD-4");
//            dataNitrit = new DataNitrit(pemanen.getKodePemanen(), gson.toJson(pemanen));
//            repository.insert(dataNitrit);
////        tblPemanen.put(pemanen.getKodePemanen(),gson.toJson(pemanen));
//
//            pemanen = new Pemanen("01330_MER", "01330", "SAMSURI BOANG MANALU", "PN042", "THP1", "MER", "AFD-4");
//            dataNitrit = new DataNitrit(pemanen.getKodePemanen(), gson.toJson(pemanen));
//            repository.insert(dataNitrit);
////        tblPemanen.put(pemanen.getKodePemanen(),gson.toJson(pemanen));
//
//            pemanen = new Pemanen("01380_MER", "01380", "VIKTOR ODALIGO HAREFA", "PN042", "THP1", "MER", "AFD-4");
//            dataNitrit = new DataNitrit(pemanen.getKodePemanen(), gson.toJson(pemanen));
//            repository.insert(dataNitrit);
////        tblPemanen.put(pemanen.getKodePemanen(),gson.toJson(pemanen));
//
//            pemanen = new Pemanen("01382_MER", "01382", "ALUIZARO WARUWU", "PN042", "THP1", "MER", "AFD-4");
//            dataNitrit = new DataNitrit(pemanen.getKodePemanen(), gson.toJson(pemanen));
//            repository.insert(dataNitrit);
////        tblPemanen.put(pemanen.getKodePemanen(),gson.toJson(pemanen));
//            db.close();
//        }
//        stress test
//        wasp DB
//        Gson gson = new Gson();
//        HashMap<String,String> dataTest = new HashMap<>();
//        for(int i = 0 ; i < 5000; i++) {
//            Pemanen pemanen = new Pemanen(String.valueOf(i)+"_MER","01326","MUJI LAGIONO","PN042","THP1","MER","AFD-4");
//            dataTest.put(pemanen.getKodePemanen(),gson.toJson(pemanen));
//        }
//
//
//        Long start = System.currentTimeMillis();
//        Nitrite db = getTableNitrit(TABEL_PEMANEN);
//        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
//        for (Map.Entry<String, String> entry : dataTest.entrySet()) {
//            DataNitrit dataNitrit = new DataNitrit(entry.getKey(),entry.getValue());
//            repository.insert(dataNitrit);
//        }
//        db.close();
//        Log.d("nitrit add ", String.valueOf(System.currentTimeMillis() - start));
//
//        WaspHash tblPemanen = getTableHash(TABEL_PEMANEN);
//        tblPemanen.flush();
//         start = System.currentTimeMillis();
//        for (Map.Entry<String, String> entry : dataTest.entrySet()) {
//            tblPemanen.put(entry.getKey(),entry.getValue());
//        }
//        Log.d("WaspDB add ", String.valueOf(System.currentTimeMillis() - start));
    }


    public static void setupHMSFolder(String estCode){
        File extStatisticsFolder = new File( BuildConfig.BUILD_VARIANT.equals("dev")?  Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES + "/QCHMSSAPTESTING" : Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES + "/QCHMSSAP");
        if(!extStatisticsFolder.exists()) extStatisticsFolder.mkdirs();

        extStatisticsFolder = new File( Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB);
        if(!extStatisticsFolder.exists()) extStatisticsFolder.mkdirs();

        extStatisticsFolder = new File( Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB +"/"+ estCode);
        if(!extStatisticsFolder.exists()) extStatisticsFolder.mkdirs();

        extStatisticsFolder = new File( Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_BACKUP_DB);
        if(!extStatisticsFolder.exists()) extStatisticsFolder.mkdirs();

        extStatisticsFolder = new File( Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 );
        if(!extStatisticsFolder.exists()) extStatisticsFolder.mkdirs();

        extStatisticsFolder = new File( Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_TEMP);
        if(!extStatisticsFolder.exists()) extStatisticsFolder.mkdirs();

        extStatisticsFolder = new File( Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_TEMP_MASTER);
        if(!extStatisticsFolder.exists()) extStatisticsFolder.mkdirs();

        extStatisticsFolder = new File( Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_TEMP_MASTER +"/"+ estCode);
        if(!extStatisticsFolder.exists()) extStatisticsFolder.mkdirs();

        for (String s: LIST_FOLDER) {
            File extStatisticsFolderIn = new File( Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +'/'+ s);
            if(!extStatisticsFolderIn.exists()) extStatisticsFolderIn.mkdirs();
        }

        for(String s : LIST_TABEL){
            File extStatisticsFolderIn = new File( Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB +"/"+ estCode +'/'+ Encrypts.encrypt(s));
            if(!extStatisticsFolderIn.exists()) extStatisticsFolderIn.mkdirs();
        }

    }

    public static void menuAboutApplication(Context context){
        char[] alphabetlist = new char[53];
        alphabetlist[0] = TickerUtils.EMPTY_CHAR;
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 26; j++) {
                // Add all lowercase characters first, then add the uppercase characters.
                alphabetlist[1 + i * 26 + j] = (char) ((i == 0) ? j + 97 : j + 65);
            }
        }
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy");

        View v = LayoutInflater.from(context).inflate(R.layout.about_application_layout,null);
        TickerView tvNamaAplikasi = (TickerView) v.findViewById(R.id.tvNamaAplikasi);
        TickerView tvVersiAplikasi = (TickerView) v.findViewById(R.id.tvVersion);
        TextView tvPresent = (TextView) v.findViewById(R.id.tvPresent);
        ImageView imageView = (ImageView) v.findViewById(R.id.ivLogoAplikasi);
        try {
            PackageInfo pi = context.getPackageManager().getPackageInfo(context.getPackageName(),0);
            Glide.with(context)
                    .load(R.mipmap.ic_launcher)
                    .into(imageView);
            tvNamaAplikasi.setCharacterList(alphabetlist);
            tvVersiAplikasi.setCharacterList(TickerUtils.getDefaultListForUSCurrency());
            tvNamaAplikasi.setText("-");
            tvVersiAplikasi.setText("-");
            tvNamaAplikasi.setText(context.getString(R.string.app_name));
            tvVersiAplikasi.setText("Versi : "+ pi.versionName);
            tvPresent.setText("© 2019 - "+sdf.format(System.currentTimeMillis())+" PreciseAgri System");

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        AlertDialog alertDialog = new AlertDialog.Builder(context,R.style.MyAlertDialogStyle)
                .setView(v)
                .setPositiveButton(context.getString(R.string.dg_confirm_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create();

        alertDialog.show();
        YoYo.with(Techniques.ZoomIn).duration(500).playOn(imageView);
    }

    public static User getUser(){
        Gson gson = new Gson();
        Log.e("Encryp",Encrypts.encrypt(SELECTED_USER));
        if(isFileContentAvailable(Encrypts.encrypt(SELECTED_USER))){
            User user = gson.fromJson(
                    Encrypts.decrypt(getFileContent(Encrypts.encrypt(SELECTED_USER))),
                    User.class);
            Log.e("User",gson.toJson(user));
            return user;
        }
        return null;
    }

    public static Estate getEstate(){
        Gson gson = new Gson();
        if(isFileContentAvailable(Encrypts.encrypt(SELECTED_ESTATE))){
            Estate estate = null;

            estate = gson.fromJson(
                    Encrypts.decrypt(getFileContent(Encrypts.encrypt(SELECTED_ESTATE))),
                    Estate.class);

            return estate;
        }
        return null;
    }

    public static Estate getEstateByEstCode(String estCode){
        Estate sEstate = null;
        User user = GlobalHelper.getUser();
        for (Estate estate:user.getEstates()) {
            if(estate.getEstCode().equals(estCode)){
                sEstate = estate;
                break;
            }
        }
        return sEstate;
    }

    public static JSONObject getModule(){
        try {
            JSONObject jModule = new JSONObject();
//            WaspHash hash = GlobalHelper.getTableHash(GlobalHelper.SELECTED_MODULE);
//            if(hash.getAllValues() == null){
                String value = GlobalHelper.readFileContent(GlobalHelper.PATH_SELMOD_FILE);
                if(value == null){
                    return null;
                }
                JSONArray jsonArray = new JSONArray(Encrypts.decrypt(value));
                for (int i =0;i< jsonArray.length();i++){
                    jModule = jsonArray.getJSONObject(i);
                    if (jModule.getString("mdlCode").equals("QHS")) {
                        return jModule;
                    }
                }
//            }else {
//                for (Object module : hash.getAllValues()) {
//                    jModule = new JSONObject(String.valueOf(module));
//                    if (jModule.getString("mdlCode").equals("HMS")) {
//                        return jModule;
//                    }
//                }
//            }
        }catch (JSONException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static boolean isPackageAvailable(String packagename, PackageManager packageManager){
        try {
            packageManager.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static boolean isFileContentAvailable(String fileName){
        File file = new File(getDatabasePath(),fileName);
        return file.exists();
    }

    public static String getDatabasePath() {
//        if (BuildConfig.BUILD_VARIANT.equals("dev")) {
//            return Environment.getExternalStorageDirectory() + EXTERNAL_DIR_FILES + "/dev/db/";
//        } else {
            return Environment.getExternalStorageDirectory() + EXTERNAL_DIR_FILES + "/db/";
//        }
    }

    public static String getDatabasePathHMS(){
        return Environment.getExternalStorageDirectory() + EXTERNAL_DIR_FILES_HMS_DB +"/"+ GlobalHelper.getEstate().getEstCode();
    }

    public static String getDatabasePathHMS(String estCode){
        return Environment.getExternalStorageDirectory() + EXTERNAL_DIR_FILES_HMS_DB +"/"+ estCode;
    }

    public static String getDatabasePathHMSBackup(){
        return Environment.getExternalStorageDirectory() + EXTERNAL_DIR_FILES_HMS_BACKUP_DB;
    }

    public static String getDatabasePathHMSTemp(){
        return Environment.getExternalStorageDirectory() + EXTERNAL_DIR_FILES_HMS_TEMP;
    }

    public static String getDatabasePathHMSTempMaster(){
        return Environment.getExternalStorageDirectory() + EXTERNAL_DIR_FILES_HMS_TEMP_MASTER +"/"+ GlobalHelper.getEstate().getEstCode();
    }

    public static String getDatabasePathHMSTempMaster(String estCode){
        return Environment.getExternalStorageDirectory() + EXTERNAL_DIR_FILES_HMS_TEMP_MASTER +"/"+ estCode;
    }

    public static HashMap<Integer,File> getBackUpPathHMS(){
        HashMap<Integer,File> fileDb = new HashMap<>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyMMddHHmm");
        File f = new File(BuildConfig.BUILD_VARIANT.equals("dev")? Environment.getExternalStorageDirectory() + EXTERNAL_DIR_FILES +"/QCHMSSAPTESTING" : Environment.getExternalStorageDirectory() + EXTERNAL_DIR_FILES +"/QCHMSSAP");


        if(!f.exists()){
            f.mkdir();
        }

        File fDb = new File(f + "/db");
        if(!fDb.exists()){
            fDb.mkdir();
        }

        File fEst = new File(fDb +"/"+ GlobalHelper.getEstate().getEstCode());
        if(!fEst.exists()){
            fEst.mkdir();
        }


        fileDb.put(1,fEst);

        fDb = new File(f + "/db2");
        if(!fDb.exists()){
            fDb.mkdir();
        }

        fileDb.put(2,fDb);

        return  fileDb;
    }

    public static String getFileContent(String fileName){
        File file = new File(getDatabasePath(),fileName);
        try {
            FileInputStream fis = new FileInputStream(file);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader bufferedReader = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String line;
            while((line=bufferedReader.readLine()) !=null){
                sb.append(line);
            }
            return sb.toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getDatabasePass(){
        return Encrypts.encrypt(DB_PASS);
    }

    public static WaspHash getTableHash(String tableName){
        WaspDb db = WaspFactory.openOrCreateDatabase(GlobalHelper.getDatabasePath(),
                GlobalHelper.DB_MASTER,
                GlobalHelper.getDatabasePass());
        WaspHash returnedTable = db.openOrCreateHash(tableName);
        return returnedTable;
    }

    public static Nitrite getTableNitrit(String tableName){
        try {
            Nitrite db = Nitrite.builder()
//                    .compressed()
                    .filePath(GlobalHelper.getDatabasePathHMS() + "/" +Encrypts.encrypt(tableName) + "/" +Encrypts.encrypt(tableName)+ ".db")
                    .openOrCreate(GlobalHelper.DB_MASTER, GlobalHelper.getDatabasePass());
            return db;
        }catch (Exception e){
            e.printStackTrace();
            writeFileContentAppend(GlobalHelper.getDatabasePathHMS() + "/" +Encrypts.encrypt(tableName) +"/ErrorLog.txt",
                    System.currentTimeMillis()+"\n"+e.toString());
            return cekNitrit(GlobalHelper.getDatabasePathHMS() + "/" +Encrypts.encrypt(tableName),Encrypts.encrypt(tableName),1);
        }
    }

    public static Nitrite getTableNitrit(String estCode,String tableName){
        try {
            Nitrite db = Nitrite.builder()
//                    .compressed()
                    .filePath(GlobalHelper.getDatabasePathHMS(estCode) + "/" +Encrypts.encrypt(tableName) + "/" +Encrypts.encrypt(tableName)+ ".db")
                    .openOrCreate(GlobalHelper.DB_MASTER, GlobalHelper.getDatabasePass());
            return db;
        }catch (Exception e){
            e.printStackTrace();
            writeFileContentAppend(GlobalHelper.getDatabasePathHMS(estCode) + "/" +Encrypts.encrypt(tableName) +"/ErrorLog.txt",
                    System.currentTimeMillis()+"\n"+e.toString());
            return cekNitrit(GlobalHelper.getDatabasePathHMS(estCode) + "/" +Encrypts.encrypt(tableName),Encrypts.encrypt(tableName),1);
        }
    }

    public static ArrayList<String> getAllFileDB(String tableName,boolean db){
        ArrayList<String> allDB = new ArrayList<>();
        File dirdb = new File(GlobalHelper.getDatabasePathHMS() + "/" +Encrypts.encrypt(tableName));
        if(dirdb.isDirectory()){
            for(File file : dirdb.listFiles()){
                if(db) {
                    if (file.getName().toLowerCase().contains(".db") && file.length() > 0) {
                        allDB.add(file.getAbsolutePath());
                    }
                }else{
                    allDB.add(file.getAbsolutePath());
                }
            }
        }
        return allDB;
    }

    public static ArrayList<String> getAllFileDB(String estCode,String tableName,boolean db){
        ArrayList<String> allDB = new ArrayList<>();
        File dirdb = new File(GlobalHelper.getDatabasePathHMS(estCode) + "/" +Encrypts.encrypt(tableName));
        if(dirdb.isDirectory()){
            for(File file : dirdb.listFiles()){
                if(db) {
                    if (file.getName().toLowerCase().contains(".db") && file.length() > 0) {
                        allDB.add(file.getAbsolutePath());
                    }
                }else{
                    allDB.add(file.getAbsolutePath());
                }
            }
        }
        return allDB;
    }

    public static Nitrite cekNitrit(String path,String namaDb,int idx){
        File oldFile = new File(path + "/" +namaDb+ ".db");
        File newFile = new File(path + "/" +namaDb+"_"+String.valueOf(idx)+ ".db");

        if(newFile.exists()) {
            idx++;
            return cekNitrit(path,namaDb,idx);
        }
        try {
            oldFile.renameTo(newFile);
            if(newFile.exists()) {
                if (oldFile.exists()) oldFile.delete();
            }
            Nitrite db = Nitrite.builder()
                    .compressed()
                    .filePath(path + "/" +namaDb+ ".db")
                    .openOrCreate(GlobalHelper.DB_MASTER, GlobalHelper.getDatabasePass());
            return db;
        } catch (Exception e) {
            e.printStackTrace();
            if(idx == 5){
                return null;
            }
            idx++;
            return cekNitrit(path,namaDb,idx);
        }
    }

    public static Nitrite getTableNitritSyncHistory(){
        Nitrite db = Nitrite.builder()
//                .compressed()
                .filePath(GlobalHelper.getDatabasePathHMSTemp() + "/" +Encrypts.encrypt(getUploadHistoryName())+ ".db")
                .openOrCreate(GlobalHelper.DB_MASTER, GlobalHelper.getDatabasePass());
        return db;
//        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
//        return  repository;
    }

    public static String getLogTransaksi(String tableName,String tanggal){
        return Environment.getExternalStorageDirectory() +EXTERNAL_DIR_FILES_HMS_DB2 + "/" + tableName + "/" +"LOG_"+tanggal+".json";
    }

    public static File fileDbNitrit(String tableName){
        return new File(GlobalHelper.getDatabasePathHMS() + "/" +Encrypts.encrypt(tableName) + "/" +Encrypts.encrypt(tableName)+ ".db");
    }

    public static void overrideFont(Context context, String defaultFontNameToOverride, String customFontFileNameInAssets) {
        try {
            final Typeface customFontTypeface = Typeface.createFromAsset(context.getAssets(), customFontFileNameInAssets);

            final Field defaultFontTypefaceField = Typeface.class.getDeclaredField(defaultFontNameToOverride);
            defaultFontTypefaceField.setAccessible(true);
            Log.e("font", customFontFileNameInAssets + " instead of " + defaultFontNameToOverride);
            defaultFontTypefaceField.set(null, customFontTypeface);
        } catch (Exception e) {
            Log.e("Can not set custom font", customFontFileNameInAssets + " instead of " + defaultFontNameToOverride);
        }
    }

    public static String compress(String data) {
        try {
            Log.d("comppress", data);
            ByteArrayOutputStream bos = new ByteArrayOutputStream(data.length());
            GZIPOutputStream gzip = new GZIPOutputStream(bos);
            gzip.write(data.getBytes());
            gzip.close();
            byte[] compressed = bos.toByteArray();
            bos.close();
            String base64 = Base64.encodeToString(compressed, Base64.DEFAULT);
            Log.d("comppress", base64);
            return base64;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String decompress(String value)  {
        try {
            byte [] buf = Base64.decode(value, Base64.DEFAULT);
            ByteArrayInputStream bis = new ByteArrayInputStream(buf);
            GZIPInputStream gis = new GZIPInputStream(bis);
            BufferedReader br = new BufferedReader(new InputStreamReader(gis, "UTF-8"));
            StringBuilder sb = new StringBuilder();
            String line;
            while((line = br.readLine()) != null) {
                sb.append(line);
            }
            br.close();
            gis.close();
            bis.close();
            return sb.toString();
        }catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return value;
        } catch (IOException e) {
            e.printStackTrace();
            return value;
        }catch (Exception e){
            e.printStackTrace();
            return value;
        }
    }

    public static String getEncoded64ImageStringFromBitmap(String fullpathimages) {
        File imagefile = new File(fullpathimages );
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(imagefile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Bitmap bm = BitmapFactory.decodeStream(fis);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        byte[] byteFormat = stream.toByteArray();
        // get the base 64 string
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);

        return imgString;
    }

    public static boolean decode64ImageStringToBitMap(String base64,String fullPathImages){
        byte[] data = Base64.decode(base64, Base64.NO_WRAP);
        File file = new File(fullPathImages);
        if(file.exists()){
            file.delete();
        }
        try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file))) {
            outputStream.write(data);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static JSONArray getMetaDataFromImages(String fullPathImages){

        JSONArray jsonArray = new JSONArray();
        try {
            String [] allTagExif = ExsifAllTag.getAllExsifTag();

            ExifInterface exif = new ExifInterface(fullPathImages);
            for(String s:allTagExif){
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(s,exif.getAttribute(s));
                if(jsonObject.length()> 0) {
                    jsonArray.put(jsonObject);
                }
            }


        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return jsonArray;
    }

    public static Bitmap getBitmapFromView(View view) {

        RecyclerView rv = (RecyclerView) ((RelativeLayout) view).getChildAt(2);
        RecyclerView.Adapter adapter = rv.getAdapter();
        int size = 0;
        int height = 0;
        Paint paint = new Paint();
        int iHeight = 0;
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int cacheSize = maxMemory / 8;
        LruCache<String, Bitmap> bitmaCache = new LruCache<>(cacheSize);
        if (adapter != null) {
            size = adapter.getItemCount();
            height = 0;
            iHeight = 0;

            // Use 1/8th of the available memory for this memory cache.
            for (int i = 0; i < size; i++) {
                RecyclerView.ViewHolder holder = adapter.createViewHolder(rv, adapter.getItemViewType(i));
                adapter.onBindViewHolder(holder, i);
                holder.itemView.measure(View.MeasureSpec.makeMeasureSpec(view.getWidth(), View.MeasureSpec.EXACTLY),
                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
                holder.itemView.layout(0, 0, holder.itemView.getMeasuredWidth(), holder.itemView.getMeasuredHeight());
                holder.itemView.setDrawingCacheEnabled(true);
                holder.itemView.buildDrawingCache();
                Bitmap drawingCache = holder.itemView.getDrawingCache();
                if (drawingCache != null) {
                    bitmaCache.put(String.valueOf(i), drawingCache);
                }
                height += holder.itemView.getMeasuredHeight();
            }
        }
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), height,Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable =view.getBackground();
        if (bgDrawable!=null) {
            bgDrawable.draw(canvas);
        }   else{
            canvas.drawColor(Color.WHITE);
        }

        for (int i = 0; i < size; i++) {
            Bitmap bitmap = bitmaCache.get(String.valueOf(i));
            canvas.drawBitmap(bitmap, 0f, iHeight, paint);
            iHeight += bitmap.getHeight();
            bitmap.recycle();
        }

        // draw the view on the canvas
        view.draw(canvas);
        //return the bitmap
        return returnedBitmap;
    }

    public static Bitmap getScreenshotFromRecyclerView(RecyclerView view) {
        RecyclerView.Adapter adapter = view.getAdapter();
        Bitmap bigBitmap = null;
        if (adapter != null) {
            int size = adapter.getItemCount();
            int height = 0;
            Paint paint = new Paint();
            int iHeight = 0;
            final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

            // Use 1/8th of the available memory for this memory cache.
            final int cacheSize = maxMemory / 8;
            LruCache<String, Bitmap> bitmaCache = new LruCache<>(cacheSize);
            for (int i = 0; i < size; i++) {
                RecyclerView.ViewHolder holder = adapter.createViewHolder(view, adapter.getItemViewType(i));
                adapter.onBindViewHolder(holder, i);
                holder.itemView.measure(View.MeasureSpec.makeMeasureSpec(view.getWidth(), View.MeasureSpec.EXACTLY),
                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
                holder.itemView.layout(0, 0, holder.itemView.getMeasuredWidth(), holder.itemView.getMeasuredHeight());
                holder.itemView.setDrawingCacheEnabled(true);
                holder.itemView.buildDrawingCache();
                Bitmap drawingCache = holder.itemView.getDrawingCache();
                if (drawingCache != null) {

                    bitmaCache.put(String.valueOf(i), drawingCache);
                }
                height += holder.itemView.getMeasuredHeight();
            }

            bigBitmap = Bitmap.createBitmap(view.getMeasuredWidth(), height, Bitmap.Config.ARGB_8888);
            Canvas bigCanvas = new Canvas(bigBitmap);
            bigCanvas.drawColor(Color.WHITE);

            for (int i = 0; i < size; i++) {
                Bitmap bitmap = bitmaCache.get(String.valueOf(i));
                bigCanvas.drawBitmap(bitmap, 0f, iHeight, paint);
                iHeight += bitmap.getHeight();
                bitmap.recycle();
            }

        }
        return bigBitmap;
    }

    public static String getFilePath(int type, Estate estate){
        String typePath="";
        String typePrefix="";
        String typeExtension="";
        String companyStr="";
        switch (type){
            case TYPE_VKM:
                typePath = "VECTOR_KEBUN_MAP";
                typePrefix = "VKM";
                typeExtension = ".tpk";
                break;
            case TYPE_2DMAP:
                typePath = "2D_MAP";
                typePrefix = "2DM";
                typeExtension = ".kmz";
                break;
            case TYPE_KMLBLOCK:
                typePath = "KML_BLOCK";
                typePrefix = "BLOCK";
                typeExtension = ".kmz";
                break;
            case TYPE_TPK:
                typePath = "FOTO_UDARA_MAP";
                typePrefix = "FUM";
                typeExtension = ".tpk";
        }
        if (estate.getCompanyShortName().toLowerCase().contains("thip")){
            companyStr = "PT.THIP";
        }else if (estate.getCompanyShortName().toLowerCase().contains("panpl") || estate.getCompanyShortName().toLowerCase().contains("panp-l")){
            companyStr = "PT.PANPL";
        }else if (estate.getCompanyShortName().toLowerCase().contains("panps") || estate.getCompanyShortName().toLowerCase().contains("panp-s")){
            companyStr = "PT.PANPS";
        }else if (estate.getCompanyShortName().toLowerCase().contains("panpp") || estate.getCompanyShortName().toLowerCase().contains("panp-p")){
            companyStr = "PT.PANPP";
        }else if (estate.getCompanyShortName().toLowerCase().contains("panp")){
            companyStr = "PT.PNP";
        }else{
            companyStr = "PT."+estate.getCompanyShortName();
        }
        String filePath = null;
        if(type==TYPE_TPK){
            File folderTPK = new File(Environment.getExternalStorageDirectory()
                    + EXTERNAL_DIR_FILES
                    + "/" + encryptString(typePath) + "/"
                    + encryptString(companyStr) + "/"
                    + encryptString(estate.getEstCode()) + "/");
            ArrayList<File> arrayListFile = new ArrayList<>();
            if(folderTPK.exists()){
                for(File file:folderTPK.listFiles()){
                    arrayListFile.add(file);
                }
            }

            Collections.sort(arrayListFile, new Comparator<File>() {
                @Override
                public int compare(File o1, File o2) {
                    return o2.getName().compareToIgnoreCase(o1.getName());
                }
            });

            if(arrayListFile.size()>0){
                filePath = arrayListFile.get(0).getAbsolutePath();
                if(arrayListFile.get(0).getName().contains("FUM_"+GlobalHelper.getEstate().getEstCode())){
                    File file = new File(arrayListFile.get(0).getParentFile().getAbsolutePath(),
                            encryptString(arrayListFile.get(0).getName()));
                    arrayListFile.get(0).renameTo(file);
                    filePath = file.getAbsolutePath();
                }
            }else{
                filePath = Environment.getExternalStorageDirectory()
                        + EXTERNAL_DIR_FILES
                        + "/" + encryptString(typePath) + "/"
                        + encryptString(companyStr) + "/"
                        + encryptString(estate.getEstCode()) + "/"
                        + encryptString(typePrefix + "_" +estate.getEstCode() + typeExtension);
            }
        }else{
            filePath = Environment.getExternalStorageDirectory()
                    + EXTERNAL_DIR_FILES
                    + "/" + encryptString(typePath) + "/"
                    + encryptString(companyStr) + "/"
                    + encryptString(estate.getEstCode()) + "/"
                    + encryptString(typePrefix + "_" +estate.getEstCode() + typeExtension);
        }

        return filePath;
    }

    public static String encryptString(String encString){
        try {

            MAHEncryptor encryptor = MAHEncryptor.newInstance(PASSWORD_ENCRYPT);
            String encrypted = encryptor.encode(encString);
            String urlSafe = URLEncoder.encode(encrypted,"UTF-8");
            Log.e("URL ENCODE",urlSafe);
            return urlSafe;
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String decryptString(String decString){
        try {
            String urlSafe = URLDecoder.decode(decString,"UTF-8");
            MAHEncryptor encryptor = MAHEncryptor.newInstance(PASSWORD_ENCRYPT);
            String decrypt = encryptor.decode(urlSafe);
            return decrypt;
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        return decString;
    }

    public static String decryptFiles(int type){
        String encryptedFilePath ="";
        switch (type){
            case TYPE_TPK:
                encryptedFilePath = GlobalHelper.getFilePath(GlobalHelper.TYPE_TPK,GlobalHelper.getEstate());
                break;
            case TYPE_VKM:
                encryptedFilePath = GlobalHelper.getFilePath(TYPE_VKM,GlobalHelper.getEstate());
                break;
            case TYPE_KMLBLOCK:
                encryptedFilePath = GlobalHelper.getFilePath(TYPE_KMLBLOCK,GlobalHelper.getEstate());
                break;
        }
        File initialPath = new File(encryptedFilePath);
        Log.e("Initial Path",initialPath.getAbsolutePath());
        if(initialPath.exists()){
            File returnedPath = new File(initialPath.getParentFile().getAbsolutePath(),decryptString(initialPath.getName()));
            initialPath.renameTo(returnedPath);
            return returnedPath.getAbsolutePath();
        }else{
            File returnedPath = new File(initialPath.getParentFile().getAbsolutePath(),decryptString(initialPath.getName()));
            return returnedPath.getAbsolutePath();
        }
    }

    public static double distance(double lat1, double lat2, double lon1,double lon2) {

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters


        distance = Math.pow(distance, 2);

        return Math.sqrt(distance);
    }

    public static double bearing(double startLat, double endLat, double startLng,  double endLng){
        double longitude1 = startLng;
        double longitude2 = endLng;
        double latitude1 = Math.toRadians(startLat);
        double latitude2 = Math.toRadians(endLat);
        double longDiff= Math.toRadians(longitude2-longitude1);
        double y= Math.sin(longDiff)*Math.cos(latitude2);
        double x=Math.cos(latitude1)*Math.sin(latitude2)-Math.sin(latitude1)*Math.cos(latitude2)*Math.cos(longDiff);
        return (Math.toDegrees(Math.atan2(y, x))+360)%360;
    }

    public static void showKeyboard(FragmentActivity context){
        View view = context.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
        }
    }

    public static void hideKeyboard(FragmentActivity context){
        View view = context.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)
                    context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static String getAndAssignBlock(File kmlLoc, double lat, double lon){
        String block = "";
        if(kmlLoc.exists()) {
            HashMap<String, ArrayList<Polygon>> polygonCollection = KmlHelper.getLineOfPlacemarks(kmlLoc);
            Point point = new Point(lon, lat);
            for (String str : polygonCollection.keySet()) {
                ArrayList<Polygon> polCollection = polygonCollection.get(str);
                for (Polygon pol : polCollection) {
                    if (GeometryEngine.within(point, pol, SpatialReference.create(SpatialReference.WKID_WGS84))) {
                        block = str;
                        break;
                    }
                }
            }
        }else{
            Toast.makeText(HarvestApp.getContext(),HarvestApp.getContext().getResources().getString(R.string.kml_notfound),Toast.LENGTH_LONG).show();
        }
        return block;
    }

    public static String getAndAssignBlock(MapView map, KmlLayer kmlLayer, double lat, double lon){
        Point source = GeometryEngine.project(lon,lat, SpatialReference.create(3857));
        Log.e("Source",source.getX()+" | "+source.getY());
        Point mapPoint = map.toScreenPoint(source);
        Log.e("Map point",mapPoint.getX()+" | "+mapPoint.getY());
        KmlNode[] kmlNodes = kmlLayer.getKmlNodes(mapPoint.getX(),mapPoint.getY(),0,false);

        Log.e("kml note MAP",String.valueOf(kmlNodes.length));

        if(kmlNodes.length>0){
            try {
                return GlobalHelper.tagCleanerKmlNode(kmlNodes[0]);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e){

            }
        }
        return null;
    }

    public static String tagCleanerKmlNode(KmlNode node) throws JSONException {
        Gson gson = new Gson();
        JSONObject object = new JSONObject(gson.toJson(node));
        String strData = object.getString("b") + ";"+"" ;
        String[] ssp = object.getString("d").split(";");
        if(ssp.length > 2){
            strData = strData + ssp[1];
        }
        Log.e("strData",strData);
        return strData;
    }

    public static TreeMap<String,ArrayList<Block>> getAllAfdelingBlock(File kmlLoc){
        HashMap<String, ArrayList<Polygon>> polygonCollection = KmlHelper.getLineOfPlacemarks(kmlLoc);
        TreeMap<String,ArrayList<Block>> afdelingBlocks = new TreeMap<>();
        ArrayList<Block> blocks = null;
        for ( HashMap.Entry<String, ArrayList<Polygon>> entry : polygonCollection.entrySet()){
            try {
                String s = entry.getKey();
                String [] split = s.split(";");
                if(!split[2].isEmpty()) {
                    if(afdelingBlocks.size()> 0 ){
                        blocks = afdelingBlocks.get(split[1]);
                    }
                    if(blocks == null){
                        blocks = new ArrayList<>();
                    }
                    Block block = new Block(split[2],false,entry.getValue());
                    blocks.add(block);
                    afdelingBlocks.put(split[1],blocks);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

//        TreeMap<String, ArrayList<Block>> sorted = new TreeMap<>(afdelingBlocks);
//        sorted.putAll(afdelingBlocks);
//        for (Map.Entry<String, ArrayList<Block>> e : sorted.entrySet()) {
//            afdelingBlocksOk.put(e.getKey(), e.getValue());
//        }
//
        return afdelingBlocks;
    }

    public static Set<String> getAllBlock(File kmlLoc){
        HashMap<String, ArrayList<Polygon>> polygonCollection = KmlHelper.getLineOfPlacemarks(kmlLoc);
        Set<String> allBlock = new android.support.v4.util.ArraySet<>();
        for ( String s : polygonCollection.keySet()){
            try {
                String [] split = s.split(";");
                allBlock.add(split[2]);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return allBlock;
    }

    public static Set<String> getNearBlock(File kmlLoc, double lat, double lon){
        Set<String> block = new android.support.v4.util.ArraySet<>();
        if(kmlLoc.exists()) {
            if(polygonCollection == null) {
                polygonCollection = KmlHelper.getLineOfPlacemarks(kmlLoc);
            }

            Point point = new Point(lon, lat);
            for (String str : polygonCollection.keySet()) {
                ArrayList<Polygon> polCollection = polygonCollection.get(str);
                for (Polygon pol : polCollection) {
                    Polygon pol1 = GeometryEngine.buffer(pol,SpatialReference.create(3857),0.15, Unit.create(LinearUnit.Code.CENTIMETER));
                    if (GeometryEngine.within(point, pol1, SpatialReference.create(3857))) {
                        block.add(str);
                        break;
                    }
                }
            }
        }else{
            Toast.makeText(HarvestApp.getContext(),HarvestApp.getContext().getResources().getString(R.string.kml_notfound),Toast.LENGTH_LONG).show();
        }
        return block;
    }

    public static HashMap<String ,Pemanen> getAllPemanen(){
        HashMap<String ,Pemanen> sPemanen = new HashMap<>();
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABEL_PEMANEN);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = iterable.iterator(); iterator.hasNext();) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            Pemanen pemanen = gson.fromJson(dataNitrit.getValueDataNitrit(),Pemanen.class);
            sPemanen.put(pemanen.getKodePemanen(),pemanen);
        }
        db.close();
        return sPemanen;
    }

    public static HashMap<String ,User> getAllUser(){
        HashMap<String ,User> sUser = new HashMap<>();
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_USER_HARVEST);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = iterable.iterator(); iterator.hasNext();) {
            Gson gson = new Gson();
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            User user = gson.fromJson(dataNitrit.getValueDataNitrit(),User.class);
            sUser.put(user.getUserID(),user);
        }
        db.close();
        return sUser;
    }

    public static HashMap<String,DynamicParameterPenghasilan> getAppConfig(){
        HashMap<String ,DynamicParameterPenghasilan> sAppConfig = new HashMap<>();
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_APPLICATION_CONFIGURATION);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = iterable.iterator(); iterator.hasNext();) {
            Gson gson = new Gson();
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            DynamicParameterPenghasilan dynamicParameterPenghasilan = gson.fromJson(dataNitrit.getValueDataNitrit(),DynamicParameterPenghasilan.class);
            sAppConfig.put(dynamicParameterPenghasilan.getEstCode(),dynamicParameterPenghasilan);
        }
        db.close();
        return sAppConfig;
    }

    public static HashMap<String,SPK> getDataAllSPKPanen(){
        HashMap<String ,SPK> sSupervisionList = new HashMap<>();
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_SPK);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("param1", SPK.descriptionName[SPK.descriptionPanen]));

        for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            SPK item = gson.fromJson(dataNitrit.getValueDataNitrit(), SPK.class);
            sSupervisionList.put(item.getIdSPK(),item);
        }
        db.close();
        return sSupervisionList;
    }

    public static HashMap<String,Estate> getEstateMapping(){
        HashMap<String ,Estate> sEstateMapping = new HashMap<>();
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_ESTATE_MAPPING);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = iterable.iterator(); iterator.hasNext();) {
            Gson gson = new Gson();
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Estate estate = gson.fromJson(dataNitrit.getValueDataNitrit(),Estate.class);
            sEstateMapping.put(estate.getEstCode(),estate);
        }
        db.close();

        if(sEstateMapping.size() == 0){
            sEstateMapping.put(GlobalHelper.getEstate().getEstCode(),GlobalHelper.getEstate());
        }
        return sEstateMapping;
    }

    public static File photoAddStamp(File file, Location location){
        Bitmap src = BitmapFactory.decodeFile(file.getAbsolutePath());
        Bitmap dest = Bitmap.createBitmap(src.getWidth(), src.getHeight(), Bitmap.Config.ARGB_8888);


        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String dateTime = sdf.format(Calendar.getInstance().getTime());
        dateTime = dateTime +"\n" + getEstate().getEstName();

        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.STAMP_IMAGES, Context.MODE_PRIVATE);
        String stringIsi = preferences.getString(HarvestApp.STAMP_IMAGES,null);
        Gson gson  = new Gson();
        StampImage stampImage = gson.fromJson(stringIsi,StampImage.class);
        if(stampImage != null) {
            if (stampImage.getBlock() != null) {
                dateTime = dateTime + " : " + stampImage.getBlock();
            }else{
                File kml = new File(GlobalHelper.decryptFiles(GlobalHelper.TYPE_KMLBLOCK));
                if(kml.exists()) {
                    try {
                        String [] estAfdBlock = GlobalHelper.getAndAssignBlock(kml, location.getLatitude(), location.getLongitude()).split(";");
                        dateTime = dateTime +" : " +estAfdBlock[2];
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }else{
            File kml = new File(GlobalHelper.decryptFiles(GlobalHelper.TYPE_KMLBLOCK));
            if(kml.exists()) {
                try {
                    String [] estAfdBlock = GlobalHelper.getAndAssignBlock(kml, location.getLatitude(), location.getLongitude()).split(";");
                    dateTime = dateTime +" : " +estAfdBlock[2];
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

        dateTime = dateTime +"\n" + String.format("%.5f",location.getLatitude()) + " : " + String.format("%.5f",location.getLongitude());

        Canvas cs = new Canvas(dest);
        cs.drawBitmap(src, 0f, 0f, null);

        Bitmap text = drawText(dateTime,Layout.Alignment.ALIGN_CENTER);
        cs.drawBitmap(text,0f, 0f, null);
        try {

            File folder = new File(file.getParent()+"/addstamp/");
            if(!folder.exists()) folder.mkdirs();
            File fotoOK = new File(folder+ "/"+ file.getName());
            dest.compress(Bitmap.CompressFormat.JPEG, 75, new FileOutputStream(fotoOK));
            file.delete();
            SimpleDateFormat sdfExif = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");
            ExifInterface exif = new ExifInterface(fotoOK.getAbsolutePath());
            exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE, GpsCamera.convert(location.getLatitude()));
            exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, GpsCamera.latitudeRef(location.getLatitude()));
            exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE, GpsCamera.convert(location.getLongitude()));
            exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, GpsCamera.longitudeRef(location.getLongitude()));
            exif.setAttribute(ExifInterface.TAG_DATETIME, sdfExif.format(System.currentTimeMillis()));
            exif.setAttribute(ExifInterface.TAG_DATETIME_DIGITIZED, sdfExif.format(System.currentTimeMillis()));
            exif.setAttribute(ExifInterface.TAG_DATETIME_ORIGINAL, sdfExif.format(System.currentTimeMillis()));
            exif.saveAttributes();

            return fotoOK;
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static File photoAddStampPanen(File file){
        Bitmap src = BitmapFactory.decodeFile(file.getAbsolutePath());
        Bitmap dest = Bitmap.createBitmap(src.getWidth(), src.getHeight(), Bitmap.Config.ARGB_8888);


        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.STAMP_IMAGES, Context.MODE_PRIVATE);
        String stringIsi = preferences.getString(HarvestApp.STAMP_IMAGES,null);
        Gson gson  = new Gson();
        StampImage stampImage = gson.fromJson(stringIsi,StampImage.class);

        String informasi = "";
        if(stampImage != null) {
            if (stampImage.getTph() != null) {
                informasi += "Tph : " + stampImage.getTph().getNamaTph();
            }
        }

        Canvas cs = new Canvas(dest);
        cs.drawBitmap(src, 0f, 0f, null);

        Bitmap text = drawText(informasi, Layout.Alignment.ALIGN_CENTER);
        int watermarkPadding = 15;
        int w = src.getWidth();
        int h = src.getHeight();
        int newWatermarkWidth = w / 3;
        int newWatermarkHeight = (text.getHeight() * newWatermarkWidth) / text.getWidth();
        cs.drawBitmap(text, 0, h - newWatermarkHeight - watermarkPadding, null);

        try {

            File folder = new File(file.getParent()+"/addstamp/");
            if(!folder.exists()) folder.mkdirs();
            File fotoOK = new File(folder+ "/"+ file.getName());
            dest.compress(Bitmap.CompressFormat.JPEG, 75, new FileOutputStream(fotoOK));
            file.delete();

            return fotoOK;
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    public static boolean isTimeAutomatic(Context c) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return Settings.Global.getInt(c.getContentResolver(), Settings.Global.AUTO_TIME, 0) == 1;
        } else {
            return android.provider.Settings.System.getInt(c.getContentResolver(), android.provider.Settings.System.AUTO_TIME, 0) == 1;
        }
    }

    public static File photoAddStampAngkut(File file){
        Bitmap src = BitmapFactory.decodeFile(file.getAbsolutePath());
        Bitmap dest = Bitmap.createBitmap(src.getWidth(), src.getHeight(), Bitmap.Config.ARGB_8888);


        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.STAMP_IMAGES, Context.MODE_PRIVATE);
        String stringIsi = preferences.getString(HarvestApp.STAMP_IMAGES,null);
        Gson gson  = new Gson();
        StampImage stampImage = gson.fromJson(stringIsi,StampImage.class);

        String informasi = "";

        Canvas cs = new Canvas(dest);
        cs.drawBitmap(src, 0f, 0f, null);

        Bitmap text = drawText(informasi,Layout.Alignment.ALIGN_CENTER);
        int watermarkPadding = 15;
        int w = src.getWidth();
        int h = src.getHeight();
        int newWatermarkWidth = w / 3;
        int newWatermarkHeight = (text.getHeight() * newWatermarkWidth) / text.getWidth();
        cs.drawBitmap(text,0, h - newWatermarkHeight - watermarkPadding, null);

        try {

            File folder = new File(file.getParent()+"/addstamp/");
            if(!folder.exists()) folder.mkdirs();
            File fotoOK = new File(folder+ "/"+ file.getName());
            dest.compress(Bitmap.CompressFormat.JPEG, 75, new FileOutputStream(fotoOK));
            file.delete();

            return fotoOK;
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    public static Bitmap drawText(String text,Layout.Alignment alignment) {

        // Get text dimensions
        TextPaint textPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG | Paint.LINEAR_TEXT_FLAG);
        textPaint.setStyle(Paint.Style.FILL);
        textPaint.setColor(ContextCompat.getColor(HarvestApp.getContext(), R.color.Black));
        textPaint.setTextSize(Utils.convertDpToPx(HarvestApp.getContext(),5));

        StaticLayout mTextLayout = new StaticLayout(text, textPaint, Utils.convertDpToPx(HarvestApp.getContext(),75), alignment, 1.0f, 0.0f, false);

        // Create bitmap and canvas to draw to
        Bitmap b = Bitmap.createBitmap(mTextLayout.getWidth(), mTextLayout.getHeight(), Bitmap.Config.ARGB_4444);
        Canvas c = new Canvas(b);

        // Draw background
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.LINEAR_TEXT_FLAG);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(ContextCompat.getColor(HarvestApp.getContext(),R.color.White));
        c.drawPaint(paint);

        // Draw text
        c.save();
        c.translate(0, 0);
        mTextLayout.draw(c);
        c.restore();

        return b;
    }

    public static void copyFile(File source, File dest,String name) throws IOException {
        InputStream is = null;
        OutputStream os = null;
        if(!dest.exists()){
            try {
                dest.mkdirs();
            }catch (Exception e) {
                e.printStackTrace();
            }
        }

        Log.d("copyFile source ",  source.getAbsolutePath());
        Log.d("copyFile fileDest ",  dest.getPath() +"/"+ name);

        File fileDest = new File(dest.getPath() +"/"+ name);
        try {
            is = new FileInputStream(source);
            os = new FileOutputStream(fileDest);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        } finally {
            is.close();
            os.close();
        }
    }

    public static void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);

        fileOrDirectory.delete();
    }

    public static void moveRecursive(File fileOrDirectory,File fileOrDirectoryMove,String sampai) {
        if (fileOrDirectory.isDirectory()) {
            for (File child : fileOrDirectory.listFiles()) {
                if (child.isDirectory()) {
                    fileOrDirectoryMove = new File(fileOrDirectoryMove + "/" + child.getName());
                    moveRecursive(child, fileOrDirectoryMove,sampai);
                    return;
                } else {
                    try {
                        moveFile(child, fileOrDirectoryMove, child.getName());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            String [] arrayDir = fileOrDirectory.getAbsolutePath().split("/");
            File backWardFolder = new File(fileOrDirectory.getAbsolutePath().replace("/"+arrayDir[arrayDir.length - 1],""));

            String [] arrayDirMove = fileOrDirectoryMove.getAbsolutePath().split("/");
            File backWardFolderMove = new File(fileOrDirectoryMove.getAbsolutePath().replace("/"+arrayDirMove[arrayDir.length - 1],""));

            if(!arrayDir[arrayDir.length - 1].equals(sampai)) {
                moveRecursive(backWardFolder, backWardFolderMove,sampai);
                return;
            }

        }
    }

    public static File moveFile(File file, File dir,
                                String name) throws IOException {
        String ext = "";
        try {
            ext = file.getName().substring(
                    file.getName().lastIndexOf("."), file.getName().length());
        }catch (Exception e){
            Log.e("extension tidak ada " , e.toString());
        }
        if (!dir.exists()) {
            try {
                dir.mkdirs();
            }catch (Exception e) {
                e.printStackTrace();
            }
        }

        File newFile = new File(dir, name + ext);
        if(newFile.exists())newFile.delete();
        newFile.createNewFile();
        FileChannel outputChannel = null;
        FileChannel inputChannel = null;
        try {
//
            outputChannel = new FileOutputStream(newFile).getChannel();
            inputChannel = new FileInputStream(file).getChannel();
            inputChannel.transferTo(0, inputChannel.size(), outputChannel);
            inputChannel.close();
            file.delete();
        } finally {
            if (inputChannel != null) inputChannel.close();
            if (outputChannel != null) outputChannel.close();
        }
        return newFile;
    }

    public static String readFileContent(String path){
        try{
            File root = new File(path);
            File file = new File(root.getParentFile(),root.getName());

            if(!root.getParentFile().exists()){
                root.getParentFile().mkdirs();
            }

            if(!file.exists()){
                file.createNewFile();
            }

            FileInputStream fis = new FileInputStream(file);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader bufferedReader = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String line;
            while((line=bufferedReader.readLine())!=null){
                sb.append(line);
            }
            return sb.toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(HarvestApp.getContext(),
                    e.toString(), Toast.LENGTH_SHORT);
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(HarvestApp.getContext(),
                    e.toString(), Toast.LENGTH_SHORT);
        }
        return null;
    }

    public static void writeFileContent(String path, String sBody){
        try{
            File fpath = new File(path);
            if(!fpath.getParentFile().exists()){
                fpath.getParentFile().mkdirs();
            }
            FileOutputStream fos = new FileOutputStream(fpath);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fos);
            outputStreamWriter.write(sBody);
            outputStreamWriter.flush();
            outputStreamWriter.close();
        }catch (IOException e){
            e.printStackTrace();
            Toast.makeText(HarvestApp.getContext(),
                    e.toString(), Toast.LENGTH_SHORT);
        }
    }

    public static void writeFileContentAppend(String path, String sBody){
        try{
            File fpath = new File(path);
            if(!fpath.getParentFile().exists()){
                fpath.getParentFile().mkdirs();
            }
            FileWriter fw = new FileWriter(fpath,true); //the true will append the new data
            fw.write(sBody);//appends the string to the file
            fw.close();
        }catch (IOException e){
            e.printStackTrace();
            Toast.makeText(HarvestApp.getContext(),
                    e.toString(), Toast.LENGTH_SHORT);
        }
    }

    public static <T> List<T> removeNullUsingIterator(List<T> list)
    {

        // Create an iterator from the list
        Iterator<T> itr = list.iterator();

        // Find and remove all null
        while (itr.hasNext()) {
            if (itr.next() == null)
                itr.remove(); // remove nulls
        }

        // Return the null
        return list;
    }

    public static CounterTransaksi getCounterNumberSharePrefYear(int listFolder){
        SimpleDateFormat sdfD = new SimpleDateFormat("yyyy");
        SharedPreferences prefer = HarvestApp.getContext().getSharedPreferences(HarvestApp.COUNTER_TRANSKASI, Context.MODE_PRIVATE);
        String counterTransaksiPref = prefer.getString(HarvestApp.COUNTER_TRANSKASI,null);
        HashMap<String,CounterTransaksi> hashMap = new HashMap<>();
        Gson gson = new Gson();
        CounterTransaksi counterTransaksi = null;
        if(counterTransaksiPref!= null){
            hashMap = gson.fromJson(counterTransaksiPref, HashMap.class);
            counterTransaksi = gson.fromJson(String.valueOf(hashMap.get(GlobalHelper.LIST_FOLDER[listFolder])),CounterTransaksi.class);
            if(counterTransaksi != null){
                if(counterTransaksi.getTglCounter().equalsIgnoreCase(sdfD.format(System.currentTimeMillis()))
                        && counterTransaksi.getUserId().equalsIgnoreCase(getUser().getUserID())){
                    return counterTransaksi;
                }else{
                    return null;
                }
            }
        }
        return counterTransaksi;
    }

    public static int getCountNumberYear(int listFolder){
        SimpleDateFormat sdfD = new SimpleDateFormat("yyyy");
        File file = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+  GlobalHelper.LIST_FOLDER[listFolder] + "/" + "COUNT_"+sdfD.format(System.currentTimeMillis()),getUser().getUserID() );
        String sCount = null;
        if(file.exists()){
            sCount = readFileContent(file.getAbsolutePath());
        }

        CounterTransaksi counterTransaksi = getCounterNumberSharePrefYear(listFolder);

        int count = 1;
        if(sCount != null){
            if(!sCount.isEmpty()) {
                count = Integer.parseInt(sCount);
            }
        }

        if(counterTransaksi != null){
            if(counterTransaksi.getCounter() > count){
                count = counterTransaksi.getCounter();
            }
        }
        return count;
    }

    public static void setCountNumberPlusOneYear(int listFolder){
        // ambil niali terakhir
        int count = getCountNumberYear(listFolder);
        count = count + 1;
        setCountNumberYear(listFolder,count);
    }

    public static void setCountNumberYear(int listFolder,int count){

        CounterTransaksi counterTransaksi = getCounterNumberSharePrefYear(listFolder);
        if(counterTransaksi != null) {
            if (counterTransaksi.getCounter() > count) {
                count = counterTransaksi.getCounter();
            }
        }

        SimpleDateFormat sdfD = new SimpleDateFormat("yyyy");
        File file = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+  GlobalHelper.LIST_FOLDER[listFolder] + "/" + "COUNT_"+sdfD.format(System.currentTimeMillis()),getUser().getUserID() );
        writeFileContent(file.getAbsolutePath(),String.valueOf(count));
        setCounterNumberHarePrefYear(listFolder,count);
    }

    public static void setCounterNumberHarePrefYear(int listFolder,int count){
        SimpleDateFormat sdfD = new SimpleDateFormat("yyyy");
        SharedPreferences prefer = HarvestApp.getContext().getSharedPreferences(HarvestApp.COUNTER_TRANSKASI, Context.MODE_PRIVATE);
        String counterTransaksiPref = prefer.getString(HarvestApp.COUNTER_TRANSKASI,null);
        HashMap<String,CounterTransaksi> hashMap = new HashMap<>();
        Gson gson = new Gson();
        if(counterTransaksiPref!= null){
            hashMap = gson.fromJson(counterTransaksiPref, HashMap.class);
        }
        CounterTransaksi counterTransaksi = new CounterTransaksi(GlobalHelper.LIST_FOLDER[listFolder],
                sdfD.format(System.currentTimeMillis()),
                getUser().getUserID(),
                count);
        hashMap.put(GlobalHelper.LIST_FOLDER[listFolder],counterTransaksi);

        SharedPreferences.Editor editor = prefer.edit();
        editor.putString(HarvestApp.COUNTER_TRANSKASI,gson.toJson(hashMap));
        editor.apply();
    }

    public static CounterTransaksi getCounterNumberSharePref(int listFolder){
        SimpleDateFormat sdfD = new SimpleDateFormat("yyyyMMdd");
        SharedPreferences prefer = HarvestApp.getContext().getSharedPreferences(HarvestApp.COUNTER_TRANSKASI, Context.MODE_PRIVATE);
        String counterTransaksiPref = prefer.getString(HarvestApp.COUNTER_TRANSKASI,null);
        HashMap<String,CounterTransaksi> hashMap = new HashMap<>();
        Gson gson = new Gson();
        CounterTransaksi counterTransaksi = null;
        if(counterTransaksiPref!= null){
            hashMap = gson.fromJson(counterTransaksiPref, HashMap.class);
            counterTransaksi = gson.fromJson(String.valueOf(hashMap.get(GlobalHelper.LIST_FOLDER[listFolder])),CounterTransaksi.class);
            if(counterTransaksi != null){
                if(counterTransaksi.getTglCounter().equalsIgnoreCase(sdfD.format(System.currentTimeMillis()))
                        && counterTransaksi.getUserId().equalsIgnoreCase(getUser().getUserID())){
                    return counterTransaksi;
                }else{
                    return null;
                }
            }
        }
        return counterTransaksi;
    }

    public static int getCountNumber(int listFolder){
        SimpleDateFormat sdfD = new SimpleDateFormat("yyyyMMdd");
        File file = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+  GlobalHelper.LIST_FOLDER[listFolder] + "/" + "COUNT_"+sdfD.format(System.currentTimeMillis()),getUser().getUserID() );
        String sCount = null;
        if(file.exists()){
            sCount = readFileContent(file.getAbsolutePath());
        }

        CounterTransaksi counterTransaksi = getCounterNumberSharePref(listFolder);

        int count = 1;
        if(sCount != null){
            if(!sCount.isEmpty()) {
                count = Integer.parseInt(sCount);
            }
        }

        if(counterTransaksi != null){
            if(counterTransaksi.getCounter() > count){
                count = counterTransaksi.getCounter();
            }
        }
        return count;
    }

    public static void setCountNumberPlusOne(int listFolder){
        // ambil niali terakhir
        int count = getCountNumber(listFolder);
        count = count + 1;
        setCountNumber(listFolder,count);
    }

    public static void setCountNumber(int listFolder,int count){

        CounterTransaksi counterTransaksi = getCounterNumberSharePref(listFolder);
        if(counterTransaksi != null) {
            if (counterTransaksi.getCounter() > count) {
                count = counterTransaksi.getCounter();
            }
        }

        SimpleDateFormat sdfD = new SimpleDateFormat("yyyyMMdd");
        File file = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+  GlobalHelper.LIST_FOLDER[listFolder] + "/" + "COUNT_"+sdfD.format(System.currentTimeMillis()),getUser().getUserID() );
        writeFileContent(file.getAbsolutePath(),String.valueOf(count));
        setCounterNumberHarePref(listFolder,count);
    }

    public static void setCounterNumberHarePref(int listFolder,int count){
        SimpleDateFormat sdfD = new SimpleDateFormat("yyyyMMdd");
        SharedPreferences prefer = HarvestApp.getContext().getSharedPreferences(HarvestApp.COUNTER_TRANSKASI, Context.MODE_PRIVATE);
        String counterTransaksiPref = prefer.getString(HarvestApp.COUNTER_TRANSKASI,null);
        HashMap<String,CounterTransaksi> hashMap = new HashMap<>();
        Gson gson = new Gson();
        if(counterTransaksiPref!= null){
            hashMap = gson.fromJson(counterTransaksiPref, HashMap.class);
        }
        CounterTransaksi counterTransaksi = new CounterTransaksi(GlobalHelper.LIST_FOLDER[listFolder],
                sdfD.format(System.currentTimeMillis()),
                getUser().getUserID(),
                count);
        hashMap.put(GlobalHelper.LIST_FOLDER[listFolder],counterTransaksi);

        SharedPreferences.Editor editor = prefer.edit();
        editor.putString(HarvestApp.COUNTER_TRANSKASI,gson.toJson(hashMap));
        editor.apply();
    }

    public static Set<String> getIdUpload(int listFolder){
        SimpleDateFormat sdfD = new SimpleDateFormat("yyyyMMdd");
        File file = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+  GlobalHelper.LIST_FOLDER[listFolder] + "/" + "UPLOAD_"+sdfD.format(System.currentTimeMillis()),sdfD.format(System.currentTimeMillis()) );
        String sData = readFileContent(file.getAbsolutePath());
        Set<String> dataId = new HashSet<>();
        if(sData.isEmpty()){
            return dataId;
        }
        String[] dataSplit = sData.split(";");
        for (String split:dataSplit) {
            dataId.add(split);
        }

        return dataId;
    }

    public static void setIdUpload(int listFolder,String id){
        // ambil niali terakhir
        SimpleDateFormat sdfD = new SimpleDateFormat("yyyyMMdd");
        File file = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+  GlobalHelper.LIST_FOLDER[listFolder] + "/" + "UPLOAD_"+sdfD.format(System.currentTimeMillis()),sdfD.format(System.currentTimeMillis()) );
        writeFileContentAppend(file.getAbsolutePath(),String.valueOf(id));
    }

    public static String setUrlFoto(String urlFoto){
        urlFoto = urlFoto.replace("http://172.30.1.122","");
        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
        if(preferences.getBoolean(HarvestApp.CONNECTION_PREF,false)){
            return BuildConfig.HOST_PUBLIC + urlFoto;
        }else{
            if(NetworkHelper.getIPAddress(true).startsWith("192.168.0")){
                return BuildConfig.HOST_BUFFER + urlFoto;
            }else{
                return BuildConfig.HOST + urlFoto;
            }
        }
    }

    public static int getZoomlvl(double scale){
        if(scale <= 288895.27 && scale >= 144447.62){
            return 11;
        }else if (scale <= 144447.63 && scale >= 72223.80 ){
            return 12;
        }else if (scale <= 72223.81 && scale >= 36111.8 ){
            return 13;
        }else if (scale <= 36111.9 && scale >= 18055.94 ){
            return 14;
        }else if (scale <= 18055.95 && scale >= 9027.96  ){
            return 15;
        }else if (scale <= 9027.97 && scale >= 4513.97  ){
            return 16;
        }else if (scale <= 4513.98 && scale >= 2256.98  ){
            return 17;
        }else if (scale <= 2256.99 && scale >= 1128.48  ){
            return 18;
        }else{
            return 19;
        }
    }

    public static void dialogHelpDescription(Context context, String param){
        View v = LayoutInflater.from(context)
                .inflate(R.layout.help_description,null);
        HtmlTextView tvDesc = v.findViewById(R.id.tv_des);

        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_INFORMATION);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", param));
        for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            tvDesc.setText(dataNitrit.getValueDataNitrit());
            break;
        }
        db.close();

        AlertDialog dialogInformation = new AlertDialog.Builder(context,R.style.MyAlertDialogStyle).create();
        dialogInformation.setView(v);
        dialogInformation.show();
    }

    public static void generateUserFiles(Context context,String filename, String sBody) {
        try {
            File root = new File(getDatabasePath());
            if (!root.exists()) {
                root.mkdirs();
            }

            File gpxfile = new File(root, Encrypts.encrypt(filename));
            FileWriter writer = new FileWriter(gpxfile);
            writer.write(sBody);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean aktifKembali(JSONObject jModule){
//        SharedPreferences prefer = HarvestApp.getContext().getSharedPreferences(HarvestApp.SYNC_TIME, Context.MODE_PRIVATE);
//        String sync_time = prefer.getString(HarvestApp.SYNC_TIME,null);
//        Gson gson = new Gson();
//        LastSyncTime lastSyncTime = gson.fromJson(sync_time,LastSyncTime.class);
//        HashMap<String,SyncTime> syncTimeHashMap = lastSyncTime.getSyncTimeHashMap();
//        SyncTime syncTimeTransaksi = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
//        if(syncTimeTransaksi.isSelected() && !syncTimeTransaksi.isFinish()){
//            return false;
//        }

        boolean aktifKembali = false;
        if(!fileDbNitrit(TABLE_ESTATE_MAPPING).exists()){
            aktifKembali = true;
            Toast.makeText(HarvestApp.getContext(),"Table "+TABLE_ESTATE_MAPPING + " Tidak Ada",Toast.LENGTH_LONG).show();
        }

        if(!fileDbNitrit(TABLE_APPLICATION_CONFIGURATION).exists()){
            aktifKembali = true;
            Toast.makeText(HarvestApp.getContext(),"Table "+TABLE_APPLICATION_CONFIGURATION + " Tidak Ada",Toast.LENGTH_LONG).show();
        }

        if(!aktifKembali) {
            if(!fileDbNitrit(TABLE_USER_HARVEST).exists()){
                aktifKembali = true;
                Toast.makeText(HarvestApp.getContext(),"Table "+TABLE_USER_HARVEST + " Tidak Ada",Toast.LENGTH_LONG).show();
            }else{
                if(getAllUser().values().size() == 0){
                    aktifKembali = true;
                    Toast.makeText(HarvestApp.getContext(),"Table "+TABLE_USER_HARVEST + " Kosong",Toast.LENGTH_LONG).show();
                }
            }
        }

        if(!aktifKembali) {
            if (!fileDbNitrit(TABEL_TPH).exists()) {
                aktifKembali = true;
                Toast.makeText(HarvestApp.getContext(),"Table "+TABEL_TPH + " Tidak Ada",Toast.LENGTH_LONG).show();
            }
        }

        if(!aktifKembali) {
            if (!fileDbNitrit(TABEL_QCQuestionAnswer).exists()) {
                aktifKembali = true;
                Toast.makeText(HarvestApp.getContext(),"Table "+TABEL_QCQuestionAnswer + " Tidak Ada",Toast.LENGTH_LONG).show();
            }
        }

        if(!aktifKembali) {
            File cekFileLastSync = new File(GlobalHelper.getDatabasePathHMS(), Encrypts.encrypt(GlobalHelper.LAST_SYNC));
            if (!cekFileLastSync.exists()) {
                aktifKembali = true;
            }
        }

        return aktifKembali;
    }

    public static boolean harusBackup(){

        boolean harusBackup = false;
        if(!harusBackup){
            File f = new File(GlobalHelper.getDatabasePathHMSTemp() + getLastGetdataName());
            if(f.exists()){
                harusBackup = true;
            }
        }
        if(!harusBackup){
            File f = new File(GlobalHelper.getDatabasePathHMSTemp() + GlobalHelper.getCountBadResponseName());
            if(f.exists()){
                harusBackup = true;
            }
        }

        if(!harusBackup){
            File f  = new File(GlobalHelper.getDatabasePathHMSTemp() + "QCHMSTemp.zip");
            if (f.exists()) {
                harusBackup = true;
            }
        }
        return harusBackup;
    }

    public static String getLastGetdataName(){
        return GlobalHelper.LAST_GETDATA + "_"+GlobalHelper.getEstate().getEstCode();
    }

    public static String getCountBadResponseName(){
        return GlobalHelper.COUNT_BAD_RESPONSE + "_"+GlobalHelper.getEstate().getEstCode();
    }

    public static String getUploadHistoryName(){
        return GlobalHelper.UPLOAD_HISTORY + "_"+GlobalHelper.getEstate().getEstCode();
    }

    public static String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("0x");
        if (src == null || src.length <= 0) {
            return null;
        }

        char[] buffer = new char[2];
        for (int i = 0; i < src.length; i++) {
            buffer[0] = Character.forDigit((src[i] >>> 4) & 0x0F, 16);
            buffer[1] = Character.forDigit(src[i] & 0x0F, 16);
            System.out.println(buffer);
            stringBuilder.append(buffer);
        }

        return stringBuilder.toString();
    }

    public static int getTotalJanjang(HasilPanen hasilPanen){
        return (int) hasilPanen.getJanjangNormal() +
                hasilPanen.getBusukNJangkos() +
// udh nga di ikutkan lagi               hasilPanen.getBuahMentah() +
                hasilPanen.getBuahLewatMatang() +
                hasilPanen.getBuahAbnormal();
    }

    public static int getTotalJanjangPendapatan(HasilPanen hasilPanen,DynamicParameterPenghasilan dynamicParameterPenghasilan){
        hasilPanen.setJanjangNormal(dynamicParameterPenghasilan.getJanjangNormal() == 0.0 ? 0 : (int) Math.ceil(hasilPanen.getJanjangNormal() / dynamicParameterPenghasilan.getJanjangNormal()));
        hasilPanen.setBuahMentah(dynamicParameterPenghasilan.getBuahMentah() == 0.0 ? 0 : (int) Math.ceil(hasilPanen.getBuahMentah() / dynamicParameterPenghasilan.getBuahMentah()));
        hasilPanen.setBusukNJangkos(dynamicParameterPenghasilan.getBusukNJangkos() == 0.0 ? 0 : (int) Math.ceil(hasilPanen.getBusukNJangkos() / dynamicParameterPenghasilan.getBusukNJangkos()));
        hasilPanen.setBuahLewatMatang(dynamicParameterPenghasilan.getBuahLewatMatang() == 0.0 ? 0 : (int) Math.ceil(hasilPanen.getBuahLewatMatang() / dynamicParameterPenghasilan.getBuahLewatMatang()));
        hasilPanen.setBuahAbnormal(dynamicParameterPenghasilan.getBuahAbnormal() == 0.0 ? 0 : (int) Math.ceil(hasilPanen.getBuahAbnormal() / dynamicParameterPenghasilan.getBuahAbnormal()));
        hasilPanen.setTangkaiPanjang(dynamicParameterPenghasilan.getTangkaiPanjang() == 0.0 ? 0 : (int) Math.ceil(hasilPanen.getTangkaiPanjang() / dynamicParameterPenghasilan.getTangkaiPanjang()));
        hasilPanen.setBuahDimakanTikus(dynamicParameterPenghasilan.getBuahDimakanTikus() == 0.0 ? 0 : (int) Math.ceil(hasilPanen.getBuahDimakanTikus() / dynamicParameterPenghasilan.getBuahDimakanTikus()));
        hasilPanen.setBrondolan(dynamicParameterPenghasilan.getBrondolan() == 0.0 ? 0 : (int) Math.ceil(hasilPanen.getBrondolan() / dynamicParameterPenghasilan.getBrondolan()));
        return hasilPanen.getJanjangNormal() +
                hasilPanen.getBuahMentah() +
                hasilPanen.getBusukNJangkos() +
                hasilPanen.getBuahLewatMatang() +
                hasilPanen.getBuahAbnormal() +
                hasilPanen.getTangkaiPanjang() +
                hasilPanen.getBuahDimakanTikus() +
                hasilPanen.getBrondolan();
    }

    public static boolean enableToNewTransaksi(){
        try {
            JSONObject jModule = GlobalHelper.getModule();
            if (jModule != null) {
                if ((jModule.getString("mdlAccCode").equals("QHS3")) || (jModule.getString("mdlAccCode").equals("QHS4"))) {
                    if (getAllUser().get(GlobalHelper.getUser().getUserID()) == null) {
                        Toast.makeText(HarvestApp.getContext(), "Nama Tidak Ada Di Table User ", Toast.LENGTH_LONG).show();
                        return false;
                    } else {
                        return true;
                    }
                }
            }
            return false;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean isDoubleClick(){
        long clickTime = System.currentTimeMillis();
        if((clickTime - lastClickTime) < DOUBLE_CLICK_TIME_DELTA){
            lastClickTime = clickTime;
            Log.d("GlobalHelper", "isDoubleClick " );
            return true;
        }
        lastClickTime = clickTime;
        return false;
    }

    public static String getCharForNumber(int i) {
        char[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        if (i > 25) {
            return null;
        }
        return Character.toString(alphabet[i]);
    }

    public static boolean isNumeric(String s){
        try{
            Long i = Long.parseLong(s);
            return  true;
        }catch (Exception e){
            return false;
        }
    }
}
