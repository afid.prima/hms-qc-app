package id.co.ams_plantation.harvestqcsap.presenter;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import id.co.ams_plantation.harvestqcsap.connection.Tag;
import id.co.ams_plantation.harvestqcsap.model.FilePdf;
import id.co.ams_plantation.harvestqcsap.presenter.base.ApiPresenter;
import id.co.ams_plantation.harvestqcsap.util.SyncHistoryHelper;
import id.co.ams_plantation.harvestqcsap.model.SyncTransaksi;
import id.co.ams_plantation.harvestqcsap.util.GlobalHelper;
import id.co.ams_plantation.harvestqcsap.util.PathUrl;
import id.co.ams_plantation.harvestqcsap.view.ApiView;

public class ConnectionPresenter extends ApiPresenter {

    public ConnectionPresenter(ApiView view){attachView(view);}

    public void GetMasterUserByEstate(String estCode){
        get(PathUrl.GetMasterUserByEstate + estCode + "&userID="+ GlobalHelper.getUser().getUserID(),new HashMap<>(), Tag.GetMasterUserByEstate);
    }

    public void GetPemanenListByEstate(String estCode){
        get(PathUrl.GetPemanenListByEstate + estCode + "&userID="+ GlobalHelper.getUser().getUserID(),new HashMap<>(), Tag.GetPemanenListByEstate);
    }

    public void GetTphListByEstate(String estCode){
        get(PathUrl.GetTphListByEstate + estCode,new HashMap<>(), Tag.GetTphListByEstate);
    }

    public void GetQCQuestionAnswer(){
        get(PathUrl.GetQCQuestionAnswer ,new HashMap<>(), Tag.GetQCQuestionAnswer);
    }

    public void GetSessionQcFotoPanen(String estCode,JSONArray bodyParameter){
        postBodyRawArray(PathUrl.GetSessionQcFotoPanen + estCode + "&userID="+ GlobalHelper.getUser().getUserID(),
                bodyParameter,
                Tag.GetSessionQcFotoPanen
        );
    }

    public void GetQcBuahByEstate(String estCode){
        get(PathUrl.GetQCMutuBuahByEstate + estCode,new HashMap<>(), Tag.GetQcBuahByEstate);
    }

    public void GetSensusBJRByEstate(String estCode){
        get(PathUrl.GetSensusBJRByEstate + estCode,new HashMap<>(), Tag.GetSensusBJRByEstate);
    }

    public void GetQCMutuAncakByEstate(String estCode){
        get(PathUrl.GetQCMutuAncakByEstate + estCode,new HashMap<>(), Tag.GetQCMutuAncakByEstate);
    }

    public void UploadQcBuahImages(JSONArray bodyParameter) {
        postBodyRawArray(PathUrl.PostQcBuahImages,bodyParameter,Tag.PostQcBuahImages);
    }

    public void UploadQcBuah(JSONArray bodyParameter) {
        postBodyRawArray(PathUrl.PostQcBuah ,bodyParameter,Tag.PostQcBuah);
    }

    public void UploadSensusBJRImages(JSONArray bodyParameter) {
        postBodyRawArray(PathUrl.PostSensusBJRImages,bodyParameter,Tag.PostSensusBjrImages);
    }

    public void UploadSensusBjr(JSONArray bodyParameter) {
        postBodyRawArray(PathUrl.PostSensusBJR ,bodyParameter,Tag.PostSensusBjr);
    }

    public void UploadQcAncakImages(JSONArray bodyParameter) {
        postBodyRawArray(PathUrl.PostQcAncakImages,bodyParameter,Tag.PostQcAncakImages);
    }

    public void UploadQcAncak(JSONArray bodyParameter) {
        postBodyRawArray(PathUrl.PostQcAncak ,bodyParameter,Tag.PostQcAncak);
    }


    public void UploadSessionQcFotoPanen(JSONArray bodyParameter) {
        postBodyRawArray(PathUrl.PostSessionQcFotoPanen ,bodyParameter,Tag.PostSessionQcFotoPanen);
    }

    public void UploadCrashReport(JSONArray bodyParameter){
        postBodyRawArrayAdmin(PathUrl.CrashReportList ,bodyParameter,Tag.CrashReportList);
    }

    public void GetApplicationConfiguration(String estCode){
        get(PathUrl.GetApplicationConfiguration + estCode,new HashMap<>(), Tag.GetApplicationConfiguration);
    }

    public void GetMasterSPKByEstate(String estCode){
        get(PathUrl.GetMasterSPKByEstate + estCode,new HashMap<>(), Tag.GetMasterSPKByEstate);
    }

    public void GetQCMutuAncakHeaderCounterByUserID(){
        get(PathUrl.GetQCMutuAncakHeaderCounterByUserID + GlobalHelper.getUser().getUserID(),new HashMap<>(), Tag.GetQCMutuAncakHeaderCounterByUserID);
    }

    public void GetQCBuahCounterByUserID(){
        get(PathUrl.GetQCBuahCounterByUserID + GlobalHelper.getUser().getUserID(),new HashMap<>(), Tag.GetQCBuahCounterByUserID);
    }

    public void GetEstateMapping(String estCode){
        get(PathUrl.GetEstateMapping + estCode,new HashMap<>(), Tag.GetEstateMapping);
    }

    public void GetChoiceSelection(JSONObject bodyParameter) {
        postBodyRaw(PathUrl.GetChoiceSelection,bodyParameter,Tag.GetChoiceSelection);
    }

    public void InsertNewSession(JSONObject bodyParameter) {
        postBodyRaw(PathUrl.InsertNewSession,bodyParameter,Tag.InsertNewSession);
    }

    public void GetAllMapFilesByEstate(){
        getAdminApps(PathUrl.GetAllMapFilesByEstate + GlobalHelper.getUser().getUserID() + "&username=" + GlobalHelper.getUser().getUserName() + "&estate="+ GlobalHelper.getEstate().getEstCode(),new HashMap<>(), Tag.GetAllMapFilesByEstate);
    }

    public String downloadReport(FilePdf filePdf){
        try {

            URL url = new URL(filePdf.getUrl());
            HttpURLConnection urlConnection = (HttpURLConnection) url
                    .openConnection();

            urlConnection.setRequestMethod("GET");
            urlConnection.setDoOutput(true);

            // connect
            urlConnection.connect();


            FileOutputStream fileOutput = new FileOutputStream(filePdf.getFullPath());

            // Stream used for reading the data from the internet
            InputStream inputStream = urlConnection.getInputStream();

            // this is the total size of the file which we are
            // downloading
//            totalsize = urlConnection.getContentLength();
//            setText("Starting PDF download...");

            // create a buffer...
            byte[] buffer = new byte[1024 * 1024];
            int bufferLength = 0;

            while ((bufferLength = inputStream.read(buffer)) > 0) {
                fileOutput.write(buffer, 0, bufferLength);
//                downloadedSize += bufferLength;
//                per = ((float) downloadedSize / totalsize) * 100;
//                setText("Total PDF File size  : "
//                        + (totalsize / 1024)
//                        + " KB\n\nDownloading PDF " + (int) per
//                        + "% complete");
            }
            // close the output stream when complete //
            fileOutput.close();
            return "ok";

        } catch (final MalformedURLException e) {
            return "Report Gagal Di Downlaod Coba Akses "+filePdf.getUrl() + " Dari Hp Anda";
        } catch (final IOException e) {
            return "Report Gagal Di Downlaod Coba Akses "+filePdf.getUrl() + " Dari Hp Anda";
        } catch (final Exception e) {
            return "Report Gagal Di Downlaod Coba Akses "+filePdf.getUrl() + " Dari Hp Anda";
        }
    }
}
