package id.co.ams_plantation.harvestqcsap.security;

import android.util.Base64;

import java.security.MessageDigest;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import id.co.ams_plantation.harvestqcsap.BuildConfig;


public class HashHelper {

    private static byte[] getKey() {
        try {
            byte[] key = BuildConfig.APPLICATION_ID.getBytes("UTF-8");
            MessageDigest sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16);
            return key;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String encrypt(String input) {
        byte[] result = null;
        try {
            SecretKeySpec sKey = new SecretKeySpec(getKey(), "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, sKey);
            result = cipher.doFinal(input.getBytes());
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return Base64.encodeToString(result, Base64.DEFAULT);
    }

    public static String decrypt(String input) {
        byte[] result = null;
        try {
            SecretKeySpec secretKeySpec = new SecretKeySpec(getKey(), "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
            result = cipher.doFinal(Base64.decode(input, Base64.DEFAULT));
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        assert result != null;
        return new String(result);
    }
}
