package id.co.ams_plantation.harvestqcsap.TableView.holder;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder;

import id.co.ams_plantation.harvestqcsap.TableView.model.TableViewCell;
import id.co.ams_plantation.harvestqcsap.TableView.model.TableViewRowHeader;
import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.model.QcAncak;
import id.co.ams_plantation.harvestqcsap.model.SensusBjr;

/**
 * Created by user on 12/24/2018.
 */

public class AncakRowHeaderViewHolder extends AbstractViewHolder {

    private final TextView row_header_textview;
    private final LinearLayout row_header_container;
    private TableViewCell cell;

    public AncakRowHeaderViewHolder(View layout) {
        super(layout);
        row_header_textview = (TextView) itemView.findViewById(R.id.row_header_textview);
        row_header_container = (LinearLayout) itemView.findViewById(R.id.row_header_container);
    }

    public void setCell(TableViewRowHeader cell) {
        String [] id = cell.getId().split("-");
        this.cell = cell;
        row_header_textview.setText(String.valueOf(cell.getData()));
        switch (Integer.parseInt(id[2])){
            case QcAncak.Save:
                row_header_container.setBackgroundColor(SensusBjr.COLOR_SAVE);
                break;
            case QcAncak.Upload:
                row_header_container.setBackgroundColor(SensusBjr.COLOR_UPLOAD);
                break;
            case 2:
                row_header_container.setBackgroundColor(SensusBjr.COLOR_UPLOAD);
                break;

        }
    }

    public String getCellId(){
        return this.cell.getId();
    }
}
