package id.co.ams_plantation.harvestqcsap.util;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

public class SharedPrefHelper {
    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "AMS";

    public static final String KEY_GENGSELECTED = "gang";
    public static final String KEY_TipeKongsi = "tipeKongsi";
    // Constructor
    public SharedPrefHelper(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void commit(String key,String name){
        // Storing name in pref
        editor.putString(key, name);
        // commit changes
        editor.commit();
    }

    public HashMap<String, String> getSharePref(){
        HashMap<String, String> Hses = new HashMap<String, String>();
        // user name
        Hses.put(KEY_GENGSELECTED, pref.getString(KEY_GENGSELECTED, null));
        // return user
        return Hses;
    }

    public String getName(String key){
        return  pref.getString(key, null);
    }

}
