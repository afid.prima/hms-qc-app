package id.co.ams_plantation.harvestqcsap.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import id.co.ams_plantation.harvestqcsap.HarvestApp;
import id.co.ams_plantation.harvestqcsap.model.HasilPanen;
import id.co.ams_plantation.harvestqcsap.model.SPK;
import id.co.ams_plantation.harvestqcsap.model.TransaksiPanen;
import id.co.ams_plantation.harvestqcsap.ui.BaseActivity;
import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.adapter.QrItemAdapter;
import id.co.ams_plantation.harvestqcsap.model.DataNitrit;
import id.co.ams_plantation.harvestqcsap.model.DynamicParameterPenghasilan;
import id.co.ams_plantation.harvestqcsap.model.Estate;
import id.co.ams_plantation.harvestqcsap.model.HeaderTableTransaksi;
import id.co.ams_plantation.harvestqcsap.model.Kongsi;
import id.co.ams_plantation.harvestqcsap.model.ModelRecyclerView;
import id.co.ams_plantation.harvestqcsap.model.ModelTphFragment;
import id.co.ams_plantation.harvestqcsap.model.NFCFlagTap;
import id.co.ams_plantation.harvestqcsap.model.Pemanen;
import id.co.ams_plantation.harvestqcsap.model.SubPemanen;
import id.co.ams_plantation.harvestqcsap.model.SupervisionList;
import id.co.ams_plantation.harvestqcsap.model.TPH;
import id.co.ams_plantation.harvestqcsap.model.User;
import id.co.ams_plantation.harvestqcsap.service.BluetoothService;

/**
 * Created by user on 12/18/2018.
 */

public class TransaksiPanenHelper {
    FragmentActivity activity;
    AlertDialog listrefrence;

    public TransaksiPanenHelper(FragmentActivity activity) {
        this.activity = activity;
    }

    public static TransaksiPanen toDecompre(JSONObject value){
        try {

            SimpleDateFormat format = new SimpleDateFormat("ddMMyy HHmm");
            JSONArray e = (JSONArray) value.get("e");
            JSONArray f = (JSONArray) value.get("f");

            JSONObject eObject = (JSONObject) e.get(0);
            String [] aAncakBaris = eObject.getString("a").split("_");
            String [] aHasilPanen = eObject.getString("d").split("_");
            Boolean alasBrondol = aHasilPanen[10].equalsIgnoreCase("y")? true : false;
            HasilPanen hasilPanen = new HasilPanen((int) Long.parseLong(aHasilPanen[0], 16),
                    (int) Long.parseLong(aHasilPanen[1], 16),
                    (int) Long.parseLong(aHasilPanen[2], 16),
                    (int) Long.parseLong(aHasilPanen[3], 16),
                    (int) Long.parseLong(aHasilPanen[4], 16),
                    (int) Long.parseLong(aHasilPanen[5], 16),
                    (int) Long.parseLong(aHasilPanen[6], 16),
                    (int) Long.parseLong(aHasilPanen[7], 16),
                    (int) Long.parseLong(aHasilPanen[8], 16),
                    (int) Long.parseLong(aHasilPanen[9], 16),
                    alasBrondol
            );

            hasilPanen.setWaktuPanen(format.parse(value.get("a").toString().substring(5,11) +" "+
                    eObject.getString("c").split("_")[0]).getTime());
            TPH tph = new TPH();
            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABEL_TPH);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            Cursor<DataNitrit> cursorTph = repository.find(ObjectFilters.eq("idDataNitrit", value.getString("d")));
            for (Iterator iterator = cursorTph.iterator(); iterator.hasNext();) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                if(dataNitrit.getIdDataNitrit().equalsIgnoreCase(value.getString("d"))){
                    Gson gson = new Gson();
                    tph = gson.fromJson(dataNitrit.getValueDataNitrit(),TPH.class);
                    break;
                }
            }
            db.close();

            if(tph.getNoTph() == null){
                tph.setNoTph(value.getString("d"));
            }
            if(tph.getEstCode() == null) {
                tph.setEstCode(value.getString("b"));
            }
            if(tph.getBlock() == null) {
                tph.setBlock(value.getString("c"));
            }
//            tph.setNoTph(value.getString("d"));

            Pemanen pemanen = new Pemanen();
//            pemanen.setKodePemanen(eObject.getString("b"));

            JSONArray jsonArray = new JSONArray();
            if(value.has("i")){
                jsonArray = value.getJSONArray("i");
            }
            ArrayList<SubPemanen> subPemanens = new ArrayList<>();

            db = GlobalHelper.getTableNitrit(GlobalHelper.TABEL_PEMANEN);
            repository = db.getRepository(DataNitrit.class);
            Iterable<DataNitrit> iterable = repository.find().project(DataNitrit.class);
            if(value.has("i")) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    String [] splitB2 = jsonArray.get(i).toString().split(";");
                    Pemanen pemanenX = new Pemanen(splitB2[2],"Tidak Ada Di DB");
                    for (Iterator iterator = iterable.iterator(); iterator.hasNext(); ) {
                        DataNitrit dataNitrit = (DataNitrit) iterator.next();
                        Gson gson = new Gson();
                        if (dataNitrit.getIdDataNitrit().equalsIgnoreCase(splitB2[2])) {
                            pemanenX = gson.fromJson(dataNitrit.getValueDataNitrit(), Pemanen.class);
                            break;
                        }
                    }
                    if(i == 0 ){
                        pemanen = pemanenX;
                    }
                    HasilPanen hasilPanenX = new HasilPanen(
                            Integer.parseInt(splitB2[6]),
                            Integer.parseInt(splitB2[7]),
                            Integer.parseInt(splitB2[8]),
                            Integer.parseInt(splitB2[9]),
                            Integer.parseInt(splitB2[10]),
                            Integer.parseInt(splitB2[11]),
                            Integer.parseInt(splitB2[12]),
                            Integer.parseInt(splitB2[13]),
                            0,
                            Integer.parseInt(splitB2[5]),
                            Integer.parseInt(splitB2[4])
                    );

                    subPemanens.add(new SubPemanen(i,
                            splitB2[0],
                            Integer.parseInt(splitB2[1]),
                            hasilPanenX,
                            Integer.parseInt(splitB2[3]),
                            pemanenX
                        )
                    );
                }
            }else{
                for (Iterator iterator = iterable.iterator(); iterator.hasNext();) {
                    DataNitrit dataNitrit = (DataNitrit) iterator.next();
                    if(dataNitrit.getIdDataNitrit().equalsIgnoreCase(eObject.getString("b"))) {
                        Gson gson = new Gson();
                        pemanen = gson.fromJson(dataNitrit.getValueDataNitrit(),Pemanen.class);
                        break;
                    }
                }
            }
            db.close();

            Kongsi kongsi = new Kongsi(value.getInt("h"),subPemanens);

            SPK spk = null;
            if(value.has("j")) {
                db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_SPK);
                repository = db.getRepository(DataNitrit.class);
                Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", value.getString("j")));
                for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                    DataNitrit dataNitrit = (DataNitrit) iterator.next();
                    Gson gson = new Gson();
                    spk = gson.fromJson(dataNitrit.getValueDataNitrit(), SPK.class);
                }
                db.close();
            }

            TransaksiPanen transaksiPanen = new TransaksiPanen();
            transaksiPanen.setIdTPanen( value.get("a").toString());
            transaksiPanen.setCreateBy(value.get("a").toString().substring(11));
            transaksiPanen.setCreateDate(format.parse(value.get("a").toString().substring(5,11) +" "+
                    eObject.getString("c").split("_")[1]).getTime());
            transaksiPanen.setLatitude(Double.parseDouble(f.getString(0).replace(",",".")));
            transaksiPanen.setLongitude(Double.parseDouble(f.getString(1).replace(",",".")));
            transaksiPanen.setTph(tph);
            transaksiPanen.setPemanen(pemanen);
            transaksiPanen.setHasilPanen(hasilPanen);
            transaksiPanen.setKongsi(kongsi);
            transaksiPanen.setSpk(spk);

            if(value.has("b")) {
                transaksiPanen.setEstCode(value.get("b").toString());
            }

            NFCFlagTap nfcFlagTap = new NFCFlagTap();
            if(value.has("y")) {
                nfcFlagTap.setUserTap(value.get("y").toString());
            }
            if(value.has("x")) {
                nfcFlagTap.setWaktuTap(Long.parseLong(value.get("x").toString()));
            }
            if(value.has("w")){
                nfcFlagTap.setCountTap(Integer.parseInt(value.get("w").toString()));
            }
            transaksiPanen.setNfcFlagTap(nfcFlagTap);

            if(aAncakBaris.length > 0) {
                transaksiPanen.setAncak(aAncakBaris[0]);
                //transaksiPanen.setBaris(aAncakBaris[1]);
            }

            return transaksiPanen;

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public String toCompres(TransaksiPanen transaksiPanen){
        SimpleDateFormat sdfT = new SimpleDateFormat("HHmm");
        SimpleDateFormat sdfD = new SimpleDateFormat("ddMMyy");
        String alasBrondol = transaksiPanen.getHasilPanen().isAlasanBrondolan() == true ? "y" : "n";
        try {
            JSONArray subPemanen = new JSONArray();
            Collections.sort(transaksiPanen.getKongsi().getSubPemanens(), new Comparator<SubPemanen>() {
                @Override
                public int compare(SubPemanen o1, SubPemanen o2) {
                    return o1.getNoUrutKongsi() > o2.getNoUrutKongsi() ? 0 : (o1.getNoUrutKongsi() < o2.getNoUrutKongsi()) ? 1 : -1;
                }
            });

            for(int i = 0 ; i < transaksiPanen.getKongsi().getSubPemanens().size();i++){
                SubPemanen subPemanenX = transaksiPanen.getKongsi().getSubPemanens().get(i);
                subPemanen.put(subPemanenX.getOph()+";"+
                        subPemanenX.getPersentase()+";"+
                        transaksiPanen.getKongsi().getSubPemanens().get(i).getPemanen().getKodePemanen()+";"+
                        subPemanenX.getTipePemanen()+";"+
                        subPemanenX.getHasilPanen().getTotalJanjangPendapatan()+";"+
                        subPemanenX.getHasilPanen().getTotalJanjang()+";"+
                        subPemanenX.getHasilPanen().getJanjangNormal()+";"+
                        subPemanenX.getHasilPanen().getBuahMentah()+";"+
                        subPemanenX.getHasilPanen().getBusukNJangkos()+";"+
                        subPemanenX.getHasilPanen().getBuahLewatMatang()+";"+
                        subPemanenX.getHasilPanen().getBuahAbnormal()+";"+
                        subPemanenX.getHasilPanen().getTangkaiPanjang()+";"+
                        subPemanenX.getHasilPanen().getBuahDimakanTikus()+";"+
                        subPemanenX.getHasilPanen().getBrondolan());
            }

            String kongsi = String.valueOf(transaksiPanen.getKongsi().getTipeKongsi());

            JSONObject objPanen = new JSONObject();
//            objPanen.put("a",transaksiPanen.getAncak()+"_"+transaksiPanen.getBaris());
            objPanen.put("a",transaksiPanen.getAncak());
            objPanen.put("b",transaksiPanen.getPemanen().getKodePemanen());
            objPanen.put("c",sdfT.format(transaksiPanen.getHasilPanen().getWaktuPanen()) +"_"+sdfT.format(transaksiPanen.getCreateDate()));
            objPanen.put("d",Integer.toHexString(transaksiPanen.getHasilPanen().getJanjangNormal())+"_"+
                    Integer.toHexString(transaksiPanen.getHasilPanen().getBuahMentah())+"_"+
                    Integer.toHexString(transaksiPanen.getHasilPanen().getBusukNJangkos())+"_"+
                    Integer.toHexString(transaksiPanen.getHasilPanen().getBuahLewatMatang())+"_"+
                    Integer.toHexString(transaksiPanen.getHasilPanen().getBuahAbnormal())+"_"+
                    Integer.toHexString(transaksiPanen.getHasilPanen().getTangkaiPanjang())+"_"+
                    Integer.toHexString(transaksiPanen.getHasilPanen().getBuahDimakanTikus())+"_"+
                    Integer.toHexString(transaksiPanen.getHasilPanen().getTotalJanjang())+"_"+
                    Integer.toHexString(transaksiPanen.getHasilPanen().getTotalJanjangPendapatan())+"_"+
                    Integer.toHexString(transaksiPanen.getHasilPanen().getBrondolan())+"_"+
                    alasBrondol);

            JSONArray array = new JSONArray();
            array.put(objPanen);

            JSONArray latlon = new JSONArray();
            latlon.put(String.format("%.5f",transaksiPanen.getLatitude()));
            latlon.put(String.format("%.5f",transaksiPanen.getLongitude()));

            JSONObject object = new JSONObject();
            object.put("a",transaksiPanen.getIdTPanen());
            object.put("b",transaksiPanen.getTph().getEstCode());
            object.put("c",transaksiPanen.getTph().getBlock());
            object.put("d",transaksiPanen.getTph().getNoTph());
            object.put("e",array);
            object.put("f",latlon);
            if(transaksiPanen.getSubstitute() != null){
                object.put("g",transaksiPanen.getSubstitute().getNik() + "_"+ transaksiPanen.getSubstitute().getEstate());
            }
            object.put("h",kongsi);
            object.put("i",subPemanen);
            if(transaksiPanen.getSpk() != null) {
                object.put("j", transaksiPanen.getSpk().getIdSPK());
            }
            object.put("z",GlobalHelper.TYPE_NFC_BIRU);

            Log.d("json object",object.toString());

            return GlobalHelper.compress(object.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    public void tapNFC(TransaksiPanen transaksiPanen){
        // pastikan nfc biru
//        if (((TphActivity) activity).myTag == null) {
//            Toast.makeText(HarvestApp.getContext(), activity.getResources().getString(R.string.nfc_error), Toast.LENGTH_LONG).show();
//        }else {
//            if (((TphActivity) activity).TYPE_NFC == GlobalHelper.TYPE_NFC_BIRU) {
//                if (((TphActivity) activity).valueNFC != null) {
//                    if (((TphActivity) activity).valueNFC.length() == 5) {
//                        String compres = toCompres(transaksiPanen);
//                        if (NfcHelper.write(compres, ((TphActivity) activity).myTag,true)) {
//                            Toast.makeText(HarvestApp.getContext(), HarvestApp.getContext().getResources().getString(R.string.nfc_write_success), Toast.LENGTH_LONG).show();
//                            ((TphActivity) activity).backProses();
//                        } else {
//                            Toast.makeText(HarvestApp.getContext(), HarvestApp.getContext().getResources().getString(R.string.nfc_write_error), Toast.LENGTH_LONG).show();
//                        }
//                    } else {
//                        Toast.makeText(HarvestApp.getContext(), activity.getResources().getString(R.string.nfc_not_empty), Toast.LENGTH_LONG).show();
//                    }
//                }else {
//                    Toast.makeText(HarvestApp.getContext(), activity.getResources().getString(R.string.nfc_not_empty), Toast.LENGTH_LONG).show();
//                }
//            } else {
//                Toast.makeText(HarvestApp.getContext(), activity.getResources().getString(R.string.not_nfc_blue), Toast.LENGTH_LONG).show();
//            }
//        }
    }

//    public void printBluetooth(TransaksiPanen transaksiPanen){
//        if (((BaseActivity) activity).bluetoothHelper.mService.getState() != BluetoothService.STATE_CONNECTED) {
//            ((BaseActivity) activity).bluetoothHelper.showListBluetoothPrinter();
//        }else {
//            String compres = toCompres(transaksiPanen);
//            ((BaseActivity) activity).bluetoothHelper.printPanen(compres, transaksiPanen);
//            ((TphActivity) activity).backProses();
//        }
//    }

    public void showQr(TransaksiPanen transaksiPanen,QrHelper qrHelper){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyy HH:mm");
        String alasBrondol = transaksiPanen.getHasilPanen().isAlasanBrondolan() == true ? "Yes" : "No";
        String compres = toCompres(transaksiPanen);
        View view = LayoutInflater.from(activity).inflate(R.layout.show_qr,null);
        RelativeLayout isi = (RelativeLayout) view.findViewById(R.id.isi);
//        TextView tvHeaderQr = (TextView) view.findViewById(R.id.tvHeaderQr);
//        ImageView iv_qrcode = (ImageView) view.findViewById(R.id.iv_qrcode);
        Button shareqr = (Button) view.findViewById(R.id.btn_shareqr);
        RecyclerView rv = (RecyclerView) view.findViewById(R.id.rv);
//        tvHeaderQr.setText();
//        try {
//            iv_qrcode.setImageBitmap(qrHelper.encodeAsBitmap(compres));
//        } catch (WriterException e) {
//            e.printStackTrace();
//        }

        Estate estate = GlobalHelper.getEstateByEstCode(transaksiPanen.getTph().getEstCode());
        ArrayList<ModelRecyclerView> arrayList = new ArrayList<>();
        arrayList.add(new ModelRecyclerView("headerQr",activity.getResources().getString(R.string.transaksi_panen),"",""));
//        arrayList.add(new ModelRecyclerView("Qr",compres,"",""));
        arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.info_transaksi),"",""));
        if(estate != null) {
            arrayList.add(new ModelRecyclerView("detail", "PT", estate.getCompanyShortName(), ""));
            arrayList.add(new ModelRecyclerView("detail", "Estate", estate.getEstName(), ""));
        }else{
            arrayList.add(new ModelRecyclerView("detail", "PT", activity.getResources().getString(R.string.dont_have_estate) + transaksiPanen.getTph().getEstCode(), ""));
            arrayList.add(new ModelRecyclerView("detail", "Estate", activity.getResources().getString(R.string.dont_have_estate) + transaksiPanen.getTph().getEstCode(), ""));
        }

        User userSelected = null;
        if(qrHelper.context instanceof BaseActivity){
            userSelected = GlobalHelper.dataAllUser.get(transaksiPanen.getUpdateBy());
            arrayList.add(new ModelRecyclerView("detail","Input Oleh" ,userSelected.getUserFullName(),""));
        }else{
            arrayList.add(new ModelRecyclerView("detail","Input Oleh" ,transaksiPanen.getUpdateBy(),""));
        }

        arrayList.add(new ModelRecyclerView("detail","Waktu Input" ,dateFormat.format(transaksiPanen.getCreateDate()),""));

        arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.info_tph),"",""));
        if(transaksiPanen.getTph().getNamaTph() != null) {
            arrayList.add(new ModelRecyclerView("detail", "Afdeling", transaksiPanen.getTph().getAfdeling(), ""));
            arrayList.add(new ModelRecyclerView("detail", "Block", transaksiPanen.getTph().getBlock(), ""));
//            arrayList.add(new ModelRecyclerView("detail", "Ancak", transaksiPanen.getAncak(), ""));
            arrayList.add(new ModelRecyclerView("detail", "Nama TPH", transaksiPanen.getTph().getNamaTph(), ""));
        }else{
            arrayList.add(new ModelRecyclerView("detail", "Afdeling", activity.getResources().getString(R.string.dont_have_tph) + transaksiPanen.getTph().getNoTph(), ""));
            arrayList.add(new ModelRecyclerView("detail", "Block", transaksiPanen.getTph().getBlock(), ""));
//            arrayList.add(new ModelRecyclerView("detail", "Ancak", transaksiPanen.getAncak(), ""));
            arrayList.add(new ModelRecyclerView("detail", "Nama TPH", activity.getResources().getString(R.string.dont_have_tph) + transaksiPanen.getTph().getNoTph(), ""));
        }
//        arrayList.add(new ModelRecyclerView("detail","Baris" ,transaksiPanen.getBaris(),""));
        arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.panen_tph) ,"",""));
        if(transaksiPanen.getKongsi().getSubPemanens() != null) {
            arrayList.add(new ModelRecyclerView("subPemanenHeader", "Gang", "Pemanen", "%","Jjg","Brdl","Tipe"));
            for(int i = 0 ; i < transaksiPanen.getKongsi().getSubPemanens().size(); i++){
                SubPemanen subPemanen = transaksiPanen.getKongsi().getSubPemanens().get(i);
                arrayList.add(new ModelRecyclerView("subPemanen",
                        subPemanen.getPemanen().getGank(),
                        subPemanen.getPemanen().getNama(),
                        String.valueOf(subPemanen.getPersentase()),
                        String.valueOf(subPemanen.getHasilPanen().getTotalJanjang()),
                        String.valueOf(subPemanen.getHasilPanen().getBrondolan()),
                        SubPemanen.arrayCodeTipePemanen[subPemanen.getTipePemanen()]
                ));
            }
        }else if(transaksiPanen.getPemanen().getNama() != null) {
            arrayList.add(new ModelRecyclerView("detail", "Gang", transaksiPanen.getPemanen().getGank(), ""));
            arrayList.add(new ModelRecyclerView("detail", "Pemanen", transaksiPanen.getPemanen().getNama(), ""));
        }else{
            arrayList.add(new ModelRecyclerView("detail", "Gang", activity.getResources().getString(R.string.dont_have_pemanen) + transaksiPanen.getPemanen().getKodePemanen(), ""));
            arrayList.add(new ModelRecyclerView("detail", "Pemanen", activity.getResources().getString(R.string.dont_have_pemanen) + transaksiPanen.getPemanen().getKodePemanen(), ""));
        }
        arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.info_panen) ,"",""));
        arrayList.add(new ModelRecyclerView("detail","Σ Jjg" ,String.format("%,d",transaksiPanen.getHasilPanen().getTotalJanjang()),"Jjg"));
        arrayList.add(new ModelRecyclerView("detail","Σ Jjg Pendapatan" ,String.format("%,d",transaksiPanen.getHasilPanen().getTotalJanjangPendapatan()),"Jjg"));
        arrayList.add(new ModelRecyclerView("detail","Janjang Masak" ,String.format("%,d",transaksiPanen.getHasilPanen().getJanjangNormal()),"Jjg"));
        arrayList.add(new ModelRecyclerView("detail","Buah Mentah" ,String.format("%,d",transaksiPanen.getHasilPanen().getBuahMentah()),"Jjg"));
        arrayList.add(new ModelRecyclerView("detail","Busuk & Jangkos" ,String.format("%,d",transaksiPanen.getHasilPanen().getBusukNJangkos()),"Jjg"));
        arrayList.add(new ModelRecyclerView("detail","Buah Lewat Matang" ,String.format("%,d",transaksiPanen.getHasilPanen().getBuahLewatMatang()),"Jjg"));
        arrayList.add(new ModelRecyclerView("detail","Buah Abnormal" ,String.format("%,d",transaksiPanen.getHasilPanen().getBuahAbnormal()),"Jjg"));
        arrayList.add(new ModelRecyclerView("detail","Tangkai Panjang" ,String.format("%,d",transaksiPanen.getHasilPanen().getTangkaiPanjang()),"Jjg"));
        arrayList.add(new ModelRecyclerView("detail","Dimakan Tikus" ,String.format("%,d",transaksiPanen.getHasilPanen().getBuahDimakanTikus()),"Jjg"));
        arrayList.add(new ModelRecyclerView("detail","Brondolan" ,String.format("%,d",transaksiPanen.getHasilPanen().getBrondolan()),"Kg"));
        arrayList.add(new ModelRecyclerView("detail","Alas Brondolan" ,alasBrondol,""));
//        arrayList.add(new ModelRecyclerView("detail","Waktu Panen" ,dateFormat.format(transaksiPanen.getHasilPanen().getWaktuPanen()),""));

        rv.setLayoutManager(new LinearLayoutManager(activity));
        QrItemAdapter adapter = new QrItemAdapter(activity,arrayList);
        rv.setAdapter(adapter);

        shareqr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                qrHelper.share(activity,rv);
            }
        });
        listrefrence = WidgetHelper.showListReference(listrefrence,view,activity);
    }

    public boolean validasiBolehEdit(TransaksiPanen selectedTransaksiPanen){
        if((selectedTransaksiPanen.getCreateBy().equals(GlobalHelper.getUser().getUserID())) &&
                (selectedTransaksiPanen.getStatus() == TransaksiPanen.TAP || selectedTransaksiPanen.getStatus() == TransaksiPanen.SAVE_ONLY)){
            return true;
        }else{
            return false;
        }
    }

    public boolean hapusDataNFC(TransaksiPanen selectedTransaksiPanen){
        //nanti di cek
//        if(((TphActivity)activity).TYPE_NFC == GlobalHelper.TYPE_NFC_BIRU) {
//            try {
//                if(((TphActivity)activity).valueNFC != null) {
//                    if(((TphActivity)activity).valueNFC.equals(GlobalHelper.FORMAT_NFC_PANEN)){
//                        return true;
//                    }else {
//                        TransaksiPanen transaksiPanen = toDecompre(new JSONObject(((TphActivity) activity).valueNFC));
//                        if(transaksiPanen != null) {
//                            if (transaksiPanen.getIdTPanen().equals(selectedTransaksiPanen.getIdTPanen())) {
//                                if (NfcHelper.write(GlobalHelper.FORMAT_NFC_PANEN, ((TphActivity) activity).myTag,true)) {
//                                    ((TphActivity) activity).valueNFC = GlobalHelper.FORMAT_NFC_PANEN;
//                                    return true;
//                                } else {
//                                    return false;
//                                }
//                            } else {
//                                Toast.makeText(HarvestApp.getContext(), activity.getResources().getString(R.string.data_not_valid), Toast.LENGTH_SHORT).show();
//                                return false;
//                            }
//                        } else {
//                            Toast.makeText(HarvestApp.getContext(), activity.getResources().getString(R.string.data_not_valid), Toast.LENGTH_SHORT).show();
//                            return false;
//                        }
//                    }
//                }else{
//                    Toast.makeText(HarvestApp.getContext(),activity.getResources().getString(R.string.format_not_valid),Toast.LENGTH_SHORT).show();
//                    return false;
//                }
//            } catch (JSONException e) {
//                e.printStackTrace();
//                Toast.makeText(HarvestApp.getContext(),activity.getResources().getString(R.string.format_not_valid),Toast.LENGTH_SHORT).show();
//                return false;
//            }
//        }else{
//            Toast.makeText(HarvestApp.getContext(),activity.getResources().getString(R.string.nfc_wrong),Toast.LENGTH_SHORT).show();
            return false;
//        }
    }

    public ArrayList<HeaderTableTransaksi> setHeaderTableTransaksi(){
        ArrayList<HeaderTableTransaksi> headerTableTransaksis = new ArrayList<>();
        headerTableTransaksis.add(new HeaderTableTransaksi("block","Block",HeaderTableTransaksi.Status_Hitung_Banyak_Data,HeaderTableTransaksi.Status_Calculated_In_Summary_Is_Show));
        headerTableTransaksis.add(new HeaderTableTransaksi("tph","TPH",HeaderTableTransaksi.Status_Hitung_Banyak_Data,HeaderTableTransaksi.Status_Calculated_In_Summary_Is_Show));
        headerTableTransaksis.add(new HeaderTableTransaksi("idNFC","idNFC",HeaderTableTransaksi.Status_Hitung_Banyak_Data,HeaderTableTransaksi.Status_Calculated_In_Summary_Is_Show));
        headerTableTransaksis.add(new HeaderTableTransaksi("nik","NIK",HeaderTableTransaksi.Status_Hitung_Banyak_Data,HeaderTableTransaksi.Status_Calculated_In_Summary_Is_Show));
        headerTableTransaksis.add(new HeaderTableTransaksi("pemanen","Pemanen",HeaderTableTransaksi.Status_Hitung_Banyak_Data,HeaderTableTransaksi.Status_Calculated_In_Summary_Not_Show));
        headerTableTransaksis.add(new HeaderTableTransaksi("totalJanjang","Σ Janjang",HeaderTableTransaksi.Status_Hitung_Nilai,HeaderTableTransaksi.Status_Calculated_In_Summary_Is_Show));
        headerTableTransaksis.add(new HeaderTableTransaksi("totalJanjangPendapatan","Σ Janjang Pendapatan",HeaderTableTransaksi.Status_Hitung_Nilai,HeaderTableTransaksi.Status_Calculated_In_Summary_Is_Show));

        DynamicParameterPenghasilan dynamicParameterPenghasilan = GlobalHelper.getAppConfig().get(GlobalHelper.getEstate().getEstCode());
//        if(dynamicParameterPenghasilan.getJanjangNormal()<= 0.00){
        headerTableTransaksis.add(new HeaderTableTransaksi("janjangNormal","Normal",HeaderTableTransaksi.Status_Hitung_Nilai,HeaderTableTransaksi.Status_Calculated_In_Summary_Is_Show));
//        }
//        if (dynamicParameterPenghasilan.getBuahMentah() <= 0){
        headerTableTransaksis.add(new HeaderTableTransaksi("buahMentah","Mentah",HeaderTableTransaksi.Status_Hitung_Nilai,HeaderTableTransaksi.Status_Calculated_In_Summary_Is_Show));
//        }
//        if (dynamicParameterPenghasilan.getBusukNJangkos() <= 0){
        headerTableTransaksis.add(new HeaderTableTransaksi("busukNJangkos","Busuk Jangkos",HeaderTableTransaksi.Status_Hitung_Nilai,HeaderTableTransaksi.Status_Calculated_In_Summary_Is_Show));
//        }
//        if (dynamicParameterPenghasilan.getBuahLewatMatang() <= 0){
        headerTableTransaksis.add(new HeaderTableTransaksi("buahLewatMatang","Lewat Matang",HeaderTableTransaksi.Status_Hitung_Nilai,HeaderTableTransaksi.Status_Calculated_In_Summary_Is_Show));
//        }
//        if (dynamicParameterPenghasilan.getBuahAbnormal() <= 0){
        headerTableTransaksis.add(new HeaderTableTransaksi("buahAbnormal","Abnormal",HeaderTableTransaksi.Status_Hitung_Nilai,HeaderTableTransaksi.Status_Calculated_In_Summary_Is_Show));
//        }
//        if (dynamicParameterPenghasilan.getTangkaiPanjang() <= 0){
        headerTableTransaksis.add(new HeaderTableTransaksi("tangkaiPanjang","Tangkai Panjang",HeaderTableTransaksi.Status_Hitung_Nilai,HeaderTableTransaksi.Status_Calculated_In_Summary_Is_Show));
//        }
//        if (dynamicParameterPenghasilan.getBuahDimakanTikus() <= 0){
        headerTableTransaksis.add(new HeaderTableTransaksi("buahDimakanTikus","Dimakan Tikus",HeaderTableTransaksi.Status_Hitung_Nilai,HeaderTableTransaksi.Status_Calculated_In_Summary_Is_Show));
//        }
        headerTableTransaksis.add(new HeaderTableTransaksi("brondolan","Σ Brondolan",HeaderTableTransaksi.Status_Hitung_Nilai,HeaderTableTransaksi.Status_Calculated_In_Summary_Is_Show));

        return headerTableTransaksis;
    }

    public static String getValuleForTable(HeaderTableTransaksi headerTableTransaksi, ModelTphFragment modelTphFragment){
        switch (headerTableTransaksi.getNamaAtribut()){
            case "block":
                return modelTphFragment.getTransaksiPanen().getTph().getBlock();
            case "tph":
                return modelTphFragment.getTransaksiPanen().getTph().getNamaTph();
            case "pemanen":
                return modelTphFragment.getSubPemanen().getPemanen().getNama();
            case "nik":
                return modelTphFragment.getSubPemanen().getPemanen().getNik();
            case "totalJanjang":
                return String.valueOf(modelTphFragment.getSubPemanen().getHasilPanen().getTotalJanjang());
            case "totalJanjangPendapatan":
                return String.valueOf(modelTphFragment.getSubPemanen().getHasilPanen().getTotalJanjangPendapatan());
            case "janjangNormal":
                return String.valueOf(modelTphFragment.getSubPemanen().getHasilPanen().getJanjangNormal());
            case "buahMentah":
                return String.valueOf(modelTphFragment.getSubPemanen().getHasilPanen().getBuahMentah());
            case "busukNJangkos":
                return String.valueOf(modelTphFragment.getSubPemanen().getHasilPanen().getBusukNJangkos());
            case "buahLewatMatang":
                return String.valueOf(modelTphFragment.getSubPemanen().getHasilPanen().getBuahLewatMatang());
            case "buahAbnormal":
                return String.valueOf(modelTphFragment.getSubPemanen().getHasilPanen().getBuahAbnormal());
            case "tangkaiPanjang":
                return String.valueOf(modelTphFragment.getSubPemanen().getHasilPanen().getTangkaiPanjang());
            case "buahDimakanTikus":
                return String.valueOf(modelTphFragment.getSubPemanen().getHasilPanen().getBuahDimakanTikus());
            case "brondolan":
                return String.valueOf(modelTphFragment.getSubPemanen().getHasilPanen().getBrondolan());
            case "idNFC":{
                return modelTphFragment.getTransaksiPanen().getIdNfc() == null ? "-" : String.valueOf(modelTphFragment.getTransaksiPanen().getIdNfc().replace("0x","").toUpperCase());
            }
        }
        return "";
    }

    public ArrayList<HeaderTableTransaksi> setHeaderTableQcMutuBuah(){
        ArrayList<HeaderTableTransaksi> headerTableTransaksis = new ArrayList<>();
        headerTableTransaksis.add(new HeaderTableTransaksi("block","Block",HeaderTableTransaksi.Status_Hitung_Banyak_Data,HeaderTableTransaksi.Status_Calculated_In_Summary_Is_Show));
//        headerTableTransaksis.add(new HeaderTableTransaksi("nik","NIK",HeaderTableTransaksi.Status_Hitung_Banyak_Data,HeaderTableTransaksi.Status_Calculated_In_Summary_Is_Show));
        headerTableTransaksis.add(new HeaderTableTransaksi("nfc","ID NFC",HeaderTableTransaksi.Status_Hitung_Banyak_Data,HeaderTableTransaksi.Status_Calculated_In_Summary_Not_Show));
        headerTableTransaksis.add(new HeaderTableTransaksi("totalJanjang","Σ Janjang",HeaderTableTransaksi.Status_Hitung_Nilai,HeaderTableTransaksi.Status_Calculated_In_Summary_Is_Show));
        headerTableTransaksis.add(new HeaderTableTransaksi("totalJanjangPendapatan","Σ Janjang Pendapatan",HeaderTableTransaksi.Status_Hitung_Nilai,HeaderTableTransaksi.Status_Calculated_In_Summary_Is_Show));

        DynamicParameterPenghasilan dynamicParameterPenghasilan = GlobalHelper.getAppConfig().get(GlobalHelper.getEstate().getEstCode());
//        if(dynamicParameterPenghasilan.getJanjangNormal()<= 0.00){
        headerTableTransaksis.add(new HeaderTableTransaksi("janjangNormal","Normal",HeaderTableTransaksi.Status_Hitung_Nilai,HeaderTableTransaksi.Status_Calculated_In_Summary_Is_Show));
//        }
//        if (dynamicParameterPenghasilan.getBuahMentah() <= 0){
        headerTableTransaksis.add(new HeaderTableTransaksi("buahMentah","Mentah",HeaderTableTransaksi.Status_Hitung_Nilai,HeaderTableTransaksi.Status_Calculated_In_Summary_Is_Show));
//        }
//        if (dynamicParameterPenghasilan.getBusukNJangkos() <= 0){
        headerTableTransaksis.add(new HeaderTableTransaksi("busukNJangkos","Busuk Jangkos",HeaderTableTransaksi.Status_Hitung_Nilai,HeaderTableTransaksi.Status_Calculated_In_Summary_Is_Show));
//        }
//        if (dynamicParameterPenghasilan.getBuahLewatMatang() <= 0){
        headerTableTransaksis.add(new HeaderTableTransaksi("buahLewatMatang","Lewat Matang",HeaderTableTransaksi.Status_Hitung_Nilai,HeaderTableTransaksi.Status_Calculated_In_Summary_Is_Show));
//        }
//        if (dynamicParameterPenghasilan.getBuahAbnormal() <= 0){
        headerTableTransaksis.add(new HeaderTableTransaksi("buahAbnormal","Abnormal",HeaderTableTransaksi.Status_Hitung_Nilai,HeaderTableTransaksi.Status_Calculated_In_Summary_Is_Show));
//        }
//        if (dynamicParameterPenghasilan.getTangkaiPanjang() <= 0){
        headerTableTransaksis.add(new HeaderTableTransaksi("tangkaiPanjang","Tangkai Panjang",HeaderTableTransaksi.Status_Hitung_Nilai,HeaderTableTransaksi.Status_Calculated_In_Summary_Is_Show));
//        }
//        if (dynamicParameterPenghasilan.getBuahDimakanTikus() <= 0){
        headerTableTransaksis.add(new HeaderTableTransaksi("buahDimakanTikus","Dimakan Tikus",HeaderTableTransaksi.Status_Hitung_Nilai,HeaderTableTransaksi.Status_Calculated_In_Summary_Is_Show));
//        }
        headerTableTransaksis.add(new HeaderTableTransaksi("brondolan","Σ Brondolan",HeaderTableTransaksi.Status_Hitung_Nilai,HeaderTableTransaksi.Status_Calculated_In_Summary_Is_Show));

        return headerTableTransaksis;
    }

    public static String getValuleForTableQcMutuBuah(HeaderTableTransaksi headerTableTransaksi,TransaksiPanen transaksiPanen){
        switch (headerTableTransaksi.getNamaAtribut()){
            case "block":{
                return transaksiPanen.getTph().getBlock();
            }
//            case "pemanen":{
//                return transaksiPanen.getPemanen().getNama();
//            }
            case "nfc":{
                if(transaksiPanen.getIdNfc() != null){
                    return transaksiPanen.getIdNfc();
                }else{
                    return "";
                }
            }
            case "totalJanjang":{
                return String.valueOf(transaksiPanen.getHasilPanen().getTotalJanjang());
            }
            case "totalJanjangPendapatan":{
                return String.valueOf(transaksiPanen.getHasilPanen().getTotalJanjangPendapatan());
            }
            case "janjangNormal":{
                return String.valueOf(transaksiPanen.getHasilPanen().getJanjangNormal());
            }
            case "buahMentah":{
                return String.valueOf(transaksiPanen.getHasilPanen().getBuahMentah());
            }
            case "busukNJangkos":{
                return String.valueOf(transaksiPanen.getHasilPanen().getBusukNJangkos());
            }
            case "buahLewatMatang":{
                return String.valueOf(transaksiPanen.getHasilPanen().getBuahLewatMatang());
            }
            case "buahAbnormal":{
                return String.valueOf(transaksiPanen.getHasilPanen().getBuahAbnormal());
            }
            case "tangkaiPanjang":{
                return String.valueOf(transaksiPanen.getHasilPanen().getTangkaiPanjang());
            }
            case "buahDimakanTikus":{
                return String.valueOf(transaksiPanen.getHasilPanen().getBuahDimakanTikus());
            }
            case "brondolan":{
                return String.valueOf(transaksiPanen.getHasilPanen().getBrondolan());
            }
//            case "idNFC":{
//                return transaksiPanen.getIdNfc() == null ? "-" : String.valueOf(transaksiPanen.getIdNfc().replace("0x","").toUpperCase());
//            }
        }
        return "";
    }

    public ArrayList<ModelRecyclerView> getSummaryTransaksi(ArrayList<HeaderTableTransaksi> headerTableTransaksis,ArrayList<ModelTphFragment> modelTphFragments){
        ArrayList<ModelRecyclerView> summaryTableTransaksis = new ArrayList<>();
        summaryTableTransaksis.add(new ModelRecyclerView("headerQr",activity.getResources().getString(R.string.summary_panen),"",""));
        for(int i = 0 ; i < headerTableTransaksis.size();i++){
            HeaderTableTransaksi headerTableTransaksi = headerTableTransaksis.get(i);
            Set<String> banyakData = new HashSet<>();
            int nilai = 0;
            String eom = "";
            String nama = headerTableTransaksi.getViewAtribut();
            if(headerTableTransaksi.getCalculateInSummary() == HeaderTableTransaksi.Status_Calculated_In_Summary_Is_Show){

                for(int j = 0 ; j < modelTphFragments.size();j++){
                    String value = "";
                    ModelTphFragment modelTphFragment = modelTphFragments.get(j);
                    switch (headerTableTransaksis.get(i).getNamaAtribut()){
                        case "block":{
                            value = modelTphFragment.getTransaksiPanen().getTph().getBlock();
                            eom = "Block";
                            break;
                        }
                        case "tph":{
                            value = modelTphFragment.getTransaksiPanen().getTph().getNoTph();
                            eom = "Tph";
                            break;
                        }
                        case "idNFC":{
                            value = modelTphFragment.getTransaksiPanen().getIdNfc();
                            eom = "Kartu";
                            break;
                        }
                        case "pemanen":{
                            value = modelTphFragment.getSubPemanen().getPemanen().getNama();
                            eom = "Pemanen";
                            break;
                        }
                        case "nik":{
                            nama = "Pemanen";
                            value = modelTphFragment.getSubPemanen().getPemanen().getNik();
                            eom = "Pemanen";
                            break;
                        }
                        case "totalJanjang":{
                            value = String.valueOf(modelTphFragment.getSubPemanen().getHasilPanen().getTotalJanjang());
                            eom = "Jjg";
                            break;
                        }
                        case "totalJanjangPendapatan":{
                            value = String.valueOf(modelTphFragment.getSubPemanen().getHasilPanen().getTotalJanjangPendapatan());
                            eom = "Jjg";
                            break;
                        }
                        case "janjangNormal":{
                            value = String.valueOf(modelTphFragment.getSubPemanen().getHasilPanen().getJanjangNormal());
                            eom = "Jjg";
                            break;
                        }
                        case "buahMentah":{
                            value = String.valueOf(modelTphFragment.getSubPemanen().getHasilPanen().getBuahMentah());
                            eom = "Jjg";
                            break;
                        }
                        case "busukNJangkos":{
                            value = String.valueOf(modelTphFragment.getSubPemanen().getHasilPanen().getBusukNJangkos());
                            eom = "Jjg";
                            break;
                        }
                        case "buahLewatMatang":{
                            value = String.valueOf(modelTphFragment.getSubPemanen().getHasilPanen().getBuahLewatMatang());
                            eom = "Jjg";
                            break;
                        }
                        case "buahAbnormal":{
                            value = String.valueOf(modelTphFragment.getSubPemanen().getHasilPanen().getBuahAbnormal());
                            eom = "Jjg";
                            break;
                        }
                        case "tangkaiPanjang":{
                            value = String.valueOf(modelTphFragment.getSubPemanen().getHasilPanen().getTangkaiPanjang());
                            eom = "Jjg";
                            break;
                        }
                        case "buahDimakanTikus":{
                            value = String.valueOf(modelTphFragment.getSubPemanen().getHasilPanen().getBuahDimakanTikus());
                            eom = "Jjg";
                            break;
                        }
                        case "brondolan":{
                            value = String.valueOf(modelTphFragment.getSubPemanen().getHasilPanen().getBrondolan());
                            eom = "Kg";
                            break;
                        }
                    }

                    if(headerTableTransaksi.getStatusHitung() == HeaderTableTransaksi.Status_Hitung_Banyak_Data){
                        if(value != null) {
                            if(!value.isEmpty()) {
                                banyakData.add(value);
                            }
                        }
                    }else if (headerTableTransaksi.getStatusHitung() == HeaderTableTransaksi.Status_Hitung_Nilai){
                        nilai += Integer.parseInt(value);
                    }
                }

                summaryTableTransaksis.add(new ModelRecyclerView("summary",
                        nama,
                        headerTableTransaksi.getStatusHitung() == HeaderTableTransaksi.Status_Hitung_Banyak_Data ? String.valueOf(banyakData.size()) :
                        headerTableTransaksi.getStatusHitung() == HeaderTableTransaksi.Status_Hitung_Nilai  ? String.valueOf(nilai) : "",
                        eom));
            }
        }
        return summaryTableTransaksis;
    }

    public HasilPanen combineHasilPanen(HasilPanen hasilPanen1,HasilPanen hasilPanen2){
        hasilPanen1.setJanjangNormal(hasilPanen1.getJanjangNormal() + hasilPanen2.getJanjangNormal());
        hasilPanen1.setBuahMentah(hasilPanen1.getBuahMentah() + hasilPanen2.getBuahMentah());
        hasilPanen1.setBusukNJangkos(hasilPanen1.getBusukNJangkos() + hasilPanen2.getBusukNJangkos());
        hasilPanen1.setBuahLewatMatang(hasilPanen1.getBuahLewatMatang() + hasilPanen2.getBuahLewatMatang());
        hasilPanen1.setBuahAbnormal(hasilPanen1.getBuahAbnormal() + hasilPanen2.getBuahAbnormal());
        hasilPanen1.setTangkaiPanjang(hasilPanen1.getTangkaiPanjang() + hasilPanen2.getTangkaiPanjang());
        hasilPanen1.setBuahDimakanTikus(hasilPanen1.getBuahDimakanTikus() + hasilPanen2.getBuahDimakanTikus());
        hasilPanen1.setTotalJanjang(hasilPanen1.getTotalJanjang() + hasilPanen2.getTotalJanjang());
        hasilPanen1.setTotalJanjangPendapatan(hasilPanen1.getTotalJanjangPendapatan() + hasilPanen2.getTotalJanjangPendapatan());
        hasilPanen1.setBrondolan(hasilPanen1.getBrondolan() + hasilPanen2.getBrondolan());
        return hasilPanen1;
    }

    public Boolean cekSisaHasilPanen(HasilPanen hasilPanen1 , HasilPanen hasilPanen2){
        if( hasilPanen1.getJanjangNormal() != hasilPanen2.getJanjangNormal() ||
            hasilPanen1.getBuahMentah() != hasilPanen2.getBuahMentah() ||
            hasilPanen1.getBusukNJangkos() != hasilPanen2.getBusukNJangkos() ||
            hasilPanen1.getBuahLewatMatang() != hasilPanen2.getBuahLewatMatang() ||
            hasilPanen1.getBuahAbnormal() != hasilPanen2.getBuahAbnormal() ||
            hasilPanen1.getTangkaiPanjang() != hasilPanen2.getTangkaiPanjang() ||
            hasilPanen1.getBuahDimakanTikus() != hasilPanen2.getBuahDimakanTikus() ||
            hasilPanen1.getBrondolan() != hasilPanen2.getBrondolan() ||
            hasilPanen1.getTotalJanjang() != hasilPanen2.getTotalJanjang() ||
            hasilPanen1.getTotalJanjangPendapatan() != hasilPanen2.getTotalJanjangPendapatan()
        ){
            return true;
        }else{
            return false;
        }
    }

    public Boolean cekSisaHasilPanen(HasilPanen hasilPanen1 ){
        if( hasilPanen1.getJanjangNormal() != 0 ||
                hasilPanen1.getBuahMentah() != 0 ||
                hasilPanen1.getBusukNJangkos() != 0 ||
                hasilPanen1.getBuahLewatMatang() != 0 ||
                hasilPanen1.getBuahAbnormal() != 0 ||
                hasilPanen1.getTangkaiPanjang() != 0 ||
                hasilPanen1.getBuahDimakanTikus() != 0 ||
                hasilPanen1.getBrondolan() != 0 ||
                hasilPanen1.getTotalJanjang() != 0 ||
                hasilPanen1.getTotalJanjangPendapatan() != 0
                ){
            return true;
        }else{
            return false;
        }
    }

    public static String getOph(String idTPanen ,String oph){
        if(!oph.contains("-")){
            oph = idTPanen + "-" + oph;
        }
        return oph;
    }
}
