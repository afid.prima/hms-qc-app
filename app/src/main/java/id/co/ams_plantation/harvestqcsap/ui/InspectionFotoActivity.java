package id.co.ams_plantation.harvestqcsap.ui;

import static id.co.ams_plantation.harvestqcsap.util.GlobalHelper.MIN_PERSEN_QC_FOTO;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;

import java.io.File;

import id.co.ams_plantation.harvestqcsap.Fragment.InspectionFotoFragment;
import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.model.DataNitrit;
import id.co.ams_plantation.harvestqcsap.model.SessionQcFotoPanen;
import id.co.ams_plantation.harvestqcsap.util.GlobalHelper;
import id.co.ams_plantation.harvestqcsap.util.SessionFotoPanenHelper;
import id.co.ams_plantation.harvestqcsap.util.WidgetHelper;

/**
 * Created on : 21,October,2021
 * Author     : Afid
 */

public class InspectionFotoActivity extends BaseActivity{
    public SessionQcFotoPanen sessionQcFotoPanen;
    public SessionFotoPanenHelper sessionFotoPanenHelper;
    public int idxFotoQc;
    public int persen;

    @Override
    protected void initPresenter() {}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_layout);
        getSupportActionBar().hide();

        toolBarSetup();

        Gson gson = new Gson();
        Intent intent = getIntent();
        sessionFotoPanenHelper = new SessionFotoPanenHelper(this);

        File currentSession = new File(SessionFotoPanenHelper.FULLPATH_SESSION_CURRENT);
        if(currentSession.exists()){
            sessionQcFotoPanen = sessionFotoPanenHelper.getCurentSession();
        }

        if(sessionQcFotoPanen == null) {
            if (intent.getExtras() != null) {
                String sessionQcFotoPanenS = intent.getExtras().getString("sessionQcFotoPanen");
                if (sessionQcFotoPanenS != null) {
                    sessionQcFotoPanen = gson.fromJson(sessionQcFotoPanenS, SessionQcFotoPanen.class);
                    sessionFotoPanenHelper.updateCurentSession(sessionQcFotoPanen);
                }
            }
        }
        idxFotoQc = 0;
        cekHasilQc();
        main();
    }

    private void main() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_container, InspectionFotoFragment.getInstance())
                .commit();
        this.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    private void toolBarSetup() {
        getSupportActionBar().hide();
        ImageView imgBack = (ImageView) findViewById(R.id.toolbar_icon);
        TextView textToolbar = (TextView) findViewById(R.id.toolbar_text);
        imgBack.setVisibility(View.VISIBLE);
        imgBack.setImageDrawable(new IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_arrow_left)
                .colorRes(R.color.White)
                .sizeDp(24));
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backProses();
            }
        });
        textToolbar.setText(getResources().getString(R.string.qc_photo_panen));
    }

    public void backProses() {
        if(sessionQcFotoPanen.getStatus() != SessionQcFotoPanen.Status_Upload_Qc) {
            if(persen >= MIN_PERSEN_QC_FOTO) {
                AlertDialog alertDialog = new AlertDialog.Builder(this, R.style.MyAlertDialogStyle)
                        .setTitle("Perhatian")
                        .setMessage("Apakah Anda Yakin Dengan Hasil Qc Yang Anda Lakukan")
                        .setNegativeButton("Belum Yakin", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setPositiveButton("Sudah Yakin", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                new LongOperation().execute();
                            }
                        })
                        .create();
                alertDialog.show();

            }else{
                AlertDialog alertDialog = new AlertDialog.Builder(this, R.style.MyAlertDialogStyle)
                        .setTitle("Perhatian")
                        .setMessage("Hasil Qc Anda Belum Sampai "+String.valueOf(MIN_PERSEN_QC_FOTO) + "%")
                        .setNegativeButton("Kembali Qc", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setPositiveButton("Akhiri Qc", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                new LongOperation().execute();
                            }
                        })
                        .create();
                alertDialog.show();
            }
        }else{
            File currentSession = new File(SessionFotoPanenHelper.FULLPATH_SESSION_CURRENT);
            if (currentSession.exists()) {
                currentSession.delete();
            }
            setResult(GlobalHelper.RESULT_QC_FOTO_PANEN);
            finish();
        }
    }

    public void cekHasilQc(){
        int sudahQc = 0 ;
        for(int i = 0 ; i < sessionQcFotoPanen.getQcFotoPanen().size(); i++){
            if(sessionQcFotoPanen.getQcFotoPanen().get(i).getDataQcFotoPanen() != null){
                sudahQc++;
            }
        }

        persen = (int) (
                Double.parseDouble(String.valueOf(sudahQc)) /
                Double.parseDouble(String.valueOf(sessionQcFotoPanen.getQcFotoPanen().size())) * 100.00
        );
    }

    public void updateSession(){
        sessionQcFotoPanen.setUpdateDate(System.currentTimeMillis());
        if(persen >= 99){
            sessionQcFotoPanen.setStatus(SessionQcFotoPanen.Status_Done_Qc);
        }
        Gson gson = new Gson();
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_FOTO_PANEN);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", sessionQcFotoPanen.getIdSession()));
        DataNitrit dataNitrit = new DataNitrit(sessionQcFotoPanen.getIdSession(),gson.toJson(sessionQcFotoPanen));
        if(cursor.size() > 0) {
            repository.update(dataNitrit);
        }else{
            repository.insert(dataNitrit);
        }
        db.close();
    }

    @Override
    public void onBackPressed() {
        backProses();
    }

    private class LongOperation extends AsyncTask<String, String, String> {
        private AlertDialog alertDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            alertDialog = WidgetHelper.showWaitingDialog(InspectionFotoActivity.this, getResources().getString(R.string.wait));
        }

        @Override
        protected String doInBackground(String... params) {
            cekHasilQc();
            updateSession();
            File currentSession = new File(SessionFotoPanenHelper.FULLPATH_SESSION_CURRENT);
            if (currentSession.exists()) {
                currentSession.delete();
            }

            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            Toast.makeText(InspectionFotoActivity.this,"Anda Sudah Qc "+persen + " % Dari Total "+ sessionQcFotoPanen.getQcFotoPanen().size() + " Foto",Toast.LENGTH_LONG).show();
            setResult(GlobalHelper.RESULT_QC_FOTO_PANEN);
            finish();
            alertDialog.dismiss();
        }
    }
}
