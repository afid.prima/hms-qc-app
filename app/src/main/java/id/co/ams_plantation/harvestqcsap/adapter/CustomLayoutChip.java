package id.co.ams_plantation.harvestqcsap.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.chip.adapter.ChipBuilder;

import id.co.ams_plantation.harvestqcsap.R;

public class CustomLayoutChip extends ChipBuilder {

    @Override
    public View getChip(LayoutInflater inflater, String data) {
        View view = inflater.inflate(R.layout.view_chip_custom, null);
        TextView txtChip = (TextView) view.findViewById(R.id.txt_chip);
        txtChip.setText(data);
        return view;
    }
}