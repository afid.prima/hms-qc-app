package id.co.ams_plantation.harvestqcsap.model;

import java.util.ArrayList;

public class SyncTransaksi {
    String afdeling;
    String userId;
    ArrayList<Estate> estates;

    public SyncTransaksi() {
    }

    public SyncTransaksi(String afdeling, String userId) {
        this.afdeling = afdeling;
        this.userId = userId;
    }

    public String getAfdeling() {
        return afdeling;
    }

    public void setAfdeling(String afdeling) {
        this.afdeling = afdeling;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public ArrayList<Estate> getEstates() {
        return estates;
    }

    public void setEstates(ArrayList<Estate> estates) {
        this.estates = estates;
    }
}
