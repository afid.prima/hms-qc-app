package id.co.ams_plantation.harvestqcsap.model;

import com.esri.core.geometry.Polygon;

import java.util.ArrayList;

public class Block {
    String Block;
    Boolean selected;
    ArrayList<Polygon> polygons;

    public Block(String block, Boolean selected) {
        Block = block;
        this.selected = selected;
    }

    public Block(String block, Boolean selected, ArrayList<Polygon> polygons) {
        Block = block;
        this.selected = selected;
        this.polygons = polygons;
    }

    public String getBlock() {
        return Block;
    }

    public void setBlock(String block) {
        Block = block;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    public ArrayList<Polygon> getPolygons() {
        return polygons;
    }

    public void setPolygons(ArrayList<Polygon> polygons) {
        this.polygons = polygons;
    }
}
