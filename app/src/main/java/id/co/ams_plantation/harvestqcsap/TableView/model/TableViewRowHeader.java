package id.co.ams_plantation.harvestqcsap.TableView.model;

/**
 * Created by user on 12/24/2018.
 */

public class TableViewRowHeader extends TableViewCell {

    public TableViewRowHeader(String id) {
        super(id);
    }

    public TableViewRowHeader(String id, String data) {
        super(id, data);
    }
}
