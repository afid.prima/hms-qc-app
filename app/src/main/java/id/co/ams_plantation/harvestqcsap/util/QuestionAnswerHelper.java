package id.co.ams_plantation.harvestqcsap.util;

import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import id.co.ams_plantation.harvestqcsap.Fragment.QcMutuAncakInputEntryFragment;
import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.adapter.AnswerItemAdapter;
import id.co.ams_plantation.harvestqcsap.adapter.QrItemAdapter;
import id.co.ams_plantation.harvestqcsap.model.Answer;
import id.co.ams_plantation.harvestqcsap.model.QuestionAnswer;
import id.co.ams_plantation.harvestqcsap.ui.QcMutuAncakActivity;

/**
 * Created on : 04,October,2021
 * Author     : Afid
 */

public class QuestionAnswerHelper {
    FragmentActivity activity;
    AlertDialog listrefrence;

    public QuestionAnswerHelper(FragmentActivity activity) {
        this.activity = activity;
    }

    public void showQuestionSelect(QuestionAnswer questionAnswer, ArrayList<Answer> answer){
        View view = LayoutInflater.from(activity).inflate(R.layout.question_answer_layout,null);
        TextView tvQuestion = (TextView) view.findViewById(R.id.tvQuestion);
        RecyclerView rvAnswer = (RecyclerView) view.findViewById(R.id.rvAnswer);
        LinearLayout lsave = (LinearLayout) view.findViewById(R.id.lsave);

        tvQuestion.setText(questionAnswer.getQcQuestion());
        rvAnswer.setLayoutManager(new LinearLayoutManager(activity));
        AnswerItemAdapter answerAdapter = new AnswerItemAdapter(activity,questionAnswer,answer);
        rvAnswer.setAdapter(answerAdapter);

        lsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listrefrence.dismiss();
                if (activity instanceof QcMutuAncakActivity) {
                    if (((QcMutuAncakActivity) activity).getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof QcMutuAncakInputEntryFragment) {
                        QcMutuAncakInputEntryFragment fragment = (QcMutuAncakInputEntryFragment) ((QcMutuAncakActivity) activity).getSupportFragmentManager().findFragmentById(R.id.content_container);
                        fragment.updateRv(questionAnswer);
                    }
                }
            }
        });

        listrefrence = WidgetHelper.showFormDialog(listrefrence,view,activity);
    }
}
