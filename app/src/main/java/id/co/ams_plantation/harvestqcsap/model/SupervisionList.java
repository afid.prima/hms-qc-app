package id.co.ams_plantation.harvestqcsap.model;

import android.support.annotation.Nullable;

public class SupervisionList {

    private String nik;
    private String afdeling;
    private String gank;
    private String nama;
    private String estate;
    private String estCodeSAP;
    private Long userID;

    /**
     * No args constructor for use in serialization
     *
     */
    public SupervisionList() {
    }

    /**
     *
     * @param nik
     * @param nama
     * @param gank
     * @param afdeling
     * @param estate
     * @param userID
     */
    public SupervisionList(String nik, String afdeling, String gank, String nama, String estate, Long userID) {
        super();
        this.nik = nik;
        this.afdeling = afdeling;
        this.gank = gank;
        this.nama = nama;
        this.estate = estate;
        this.userID = userID;
    }

    @Nullable
    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getAfdeling() {
        return afdeling;
    }

    public void setAfdeling(String afdeling) {
        this.afdeling = afdeling;
    }

    public String getGank() {
        return gank;
    }

    public void setGank(String gank) {
        this.gank = gank;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getEstate() {
        return estate;
    }

    public void setEstate(String estate) {
        this.estate = estate;
    }

    public Long getUserID() {
        return userID;
    }

    public void setUserID(Long userID) {
        this.userID = userID;
    }

    public String getEstCodeSAP() {
        return estCodeSAP;
    }

    public void setEstCodeSAP(String estCodeSAP) {
        this.estCodeSAP = estCodeSAP;
    }
}
