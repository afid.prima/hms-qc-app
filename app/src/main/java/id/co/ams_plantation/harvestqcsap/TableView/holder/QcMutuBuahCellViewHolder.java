package id.co.ams_plantation.harvestqcsap.TableView.holder;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder;

import id.co.ams_plantation.harvestqcsap.TableView.model.TableViewCell;
import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.model.TransaksiPanen;

/**
 * Created on : 02,Oktober,2020
 * Author     : Afid
 */

public class QcMutuBuahCellViewHolder extends AbstractViewHolder {

    public final TextView cell_textview;
    public final LinearLayout cell_container;
    private TableViewCell cell;

    public QcMutuBuahCellViewHolder(View itemView) {
        super(itemView);
        cell_textview = (TextView) itemView.findViewById(R.id.cell_data);
        cell_container = (LinearLayout) itemView.findViewById(R.id.cell_container);
    }

    public void setCell(TableViewCell cell) {
        this.cell = cell;
        cell_textview.setText(String.valueOf(cell.getData()));
        // If your TableView should have auto resize for cells & columns.
        // Then you should consider the below lines. Otherwise, you can ignore them.

        // It is necessary to remeasure itself.
        cell_container.getLayoutParams().width = LinearLayout.LayoutParams.WRAP_CONTENT;

        String [] id = cell.getId().split("-");
        switch (Integer.parseInt(id[2])){
            case TransaksiPanen.SAVE_ONLY:
                cell_container.setBackgroundColor(TransaksiPanen.COLOR_SAVE_ONLY);
                break;
            case TransaksiPanen.TAP:
                cell_container.setBackgroundColor(TransaksiPanen.COLOR_TAP);
                break;
            case TransaksiPanen.UPLOAD:
                cell_container.setBackgroundColor(TransaksiPanen.COLOR_UPLOAD);
                break;
            case TransaksiPanen.SUDAH_DIANGKUT:
                cell_container.setBackgroundColor(TransaksiPanen.COLOR_SUDAH_DIANGKUT);
                break;
            case TransaksiPanen.SUDAH_DIPKS:
                cell_container.setBackgroundColor(TransaksiPanen.COLOR_SUDAH_DIPKS);
                break;
        }

        cell_textview.requestLayout();
    }

}
