package id.co.ams_plantation.harvestqcsap.preference;

import android.content.Context;


public class PreferenceHelper {

    private static final String LANGUAGE = "language";
    private static final String USER = "user";

    private static PreferenceHelper helper;
    private static DataPreference dataPreference;

    public static PreferenceHelper instance(Context context) {
        if (helper == null) helper = new PreferenceHelper(context);
        return helper;
    }

    private PreferenceHelper(Context context) {
        if (dataPreference == null) dataPreference = DataPreference.getInstance(context);
    }

    public String getLanguage(Context context) {
        if (dataPreference == null) dataPreference = DataPreference.getInstance(context);
        return dataPreference.getData(LANGUAGE, "en");
    }

    public void setLanguageIndonesia(Context context) {
        if (dataPreference == null) dataPreference = DataPreference.getInstance(context);
        dataPreference.setData(LANGUAGE, "in");
    }

    public void setLanguageEnglish(Context context) {
        if (dataPreference == null) dataPreference = DataPreference.getInstance(context);
        dataPreference.setData(LANGUAGE, "en");
    }
}
