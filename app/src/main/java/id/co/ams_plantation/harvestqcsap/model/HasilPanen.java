package id.co.ams_plantation.harvestqcsap.model;

/**
 * Created by user on 12/3/2018.
 */

public class HasilPanen {
    int janjangNormal;
    int buahMentah;
    int busukNJangkos;
    int buahLewatMatang;
    int buahAbnormal;
    int tangkaiPanjang;
    int buahDimakanTikus;
    int brondolan;
    int totalJanjangBuruk;
    int totalJanjang;
    int totalJanjangPendapatan;
    Long waktuPanen;
    boolean alasanBrondolan;

    public HasilPanen() {
        this.janjangNormal = 0;
        this.buahMentah = 0;
        this.busukNJangkos = 0;
        this.buahLewatMatang = 0;
        this.buahAbnormal = 0;
        this.tangkaiPanjang = 0;
        this.buahDimakanTikus = 0;
        this.brondolan = 0;
        this.totalJanjangBuruk = 0;
        this.totalJanjang = 0;
        this.totalJanjangPendapatan = 0;
    }

    public HasilPanen(int janjangNormal, int buahMentah, int busukNJangkos, int buahLewatMatang, int buahAbnormal, int tangkaiPanjang, int buahDimakanTikus, int brondolan, int totalJanjangBuruk, int totalJanjang, int totalJanjangPendapatan) {
        this.janjangNormal = janjangNormal;
        this.buahMentah = buahMentah;
        this.busukNJangkos = busukNJangkos;
        this.buahLewatMatang = buahLewatMatang;
        this.buahAbnormal = buahAbnormal;
        this.tangkaiPanjang = tangkaiPanjang;
        this.buahDimakanTikus = buahDimakanTikus;
        this.brondolan = brondolan;
        this.totalJanjangBuruk = totalJanjangBuruk;
        this.totalJanjang = totalJanjang;
        this.totalJanjangPendapatan = totalJanjangPendapatan;
    }

    public HasilPanen(int janjangNormal, int buahMentah, int busukNJangkos, int buahLewatMatang, int buahAbnormal, int tangkaiPanjang, int buahDimakanTikus, int totalJanjang, int totalJanjangPendapatan, int brondolan, boolean alasanBrondolan) {
        this.janjangNormal = janjangNormal;
        this.buahMentah = buahMentah;
        this.busukNJangkos = busukNJangkos;
        this.buahLewatMatang = buahLewatMatang;
        this.buahAbnormal = buahAbnormal;
        this.buahDimakanTikus = buahDimakanTikus;
        this.tangkaiPanjang = tangkaiPanjang;
        this.totalJanjang = totalJanjang;
        this.totalJanjangPendapatan = totalJanjangPendapatan;
        this.brondolan = brondolan;
        this.alasanBrondolan = alasanBrondolan;
    }

    public HasilPanen(int janjangNormal, int buahMentah, int busukNJangkos, int buahLewatMatang, int buahAbnormal, int tangkaiPanjang,int buahDimakanTikus, int brondolan, int totalJanjangBuruk,int totalJanjang,int totalJanjangPendapatan, Long waktuPanen, boolean alasanBrondolan) {
        this.janjangNormal = janjangNormal;
        this.buahMentah = buahMentah;
        this.busukNJangkos = busukNJangkos;
        this.buahLewatMatang = buahLewatMatang;
        this.buahAbnormal = buahAbnormal;
        this.tangkaiPanjang = tangkaiPanjang;
        this.brondolan = brondolan;
        this.totalJanjangBuruk = totalJanjangBuruk;
        this.buahDimakanTikus = buahDimakanTikus;
        this.totalJanjangPendapatan = totalJanjangPendapatan;
        this.totalJanjang = totalJanjang;
        this.waktuPanen = waktuPanen;
        this.alasanBrondolan = alasanBrondolan;
    }

    public int getTotalJanjangBuruk() {
        return totalJanjangBuruk;
    }

    public void setTotalJanjangBuruk(int totalJanjangBuruk) {
        this.totalJanjangBuruk = totalJanjangBuruk;
    }

    public Long getWaktuPanen() {
        return waktuPanen;
    }

    public void setWaktuPanen(Long waktuPanen) {
        this.waktuPanen = waktuPanen;
    }

    public int getTangkaiPanjang() {
        return tangkaiPanjang;
    }

    public void setTangkaiPanjang(int tangkaiPanjang) {
        this.tangkaiPanjang = tangkaiPanjang;
    }

    public int getJanjangNormal() {
        return janjangNormal;
    }

    public void setJanjangNormal(int janjangNormal) {
        this.janjangNormal = janjangNormal;
    }

    public int getBuahMentah() {
        return buahMentah;
    }

    public void setBuahMentah(int buahMentah) {
        this.buahMentah = buahMentah;
    }

    public int getBusukNJangkos() {
        return busukNJangkos;
    }

    public void setBusukNJangkos(int busukNJangkos) {
        this.busukNJangkos = busukNJangkos;
    }

    public int getBuahLewatMatang() {
        return buahLewatMatang;
    }

    public void setBuahLewatMatang(int buahLewatMatang) {
        this.buahLewatMatang = buahLewatMatang;
    }

    public int getBuahAbnormal() {
        return buahAbnormal;
    }

    public void setBuahAbnormal(int buahAbnormal) {
        this.buahAbnormal = buahAbnormal;
    }

    public int getBuahDimakanTikus() {
        return buahDimakanTikus;
    }

    public void setBuahDimakanTikus(int buahDimakanTikus) {
        this.buahDimakanTikus = buahDimakanTikus;
    }

    public int getTotalJanjangPendapatan() {
        return totalJanjangPendapatan;
    }

    public void setTotalJanjangPendapatan(int totalJanjangPendapatan) {
        this.totalJanjangPendapatan = totalJanjangPendapatan;
    }

    public int getBrondolan() {
        return brondolan;
    }

    public void setBrondolan(int brondolan) {
        this.brondolan = brondolan;
    }

    public boolean isAlasanBrondolan() {
        return alasanBrondolan;
    }

    public void setAlasanBrondolan(boolean alasanBrondolan) {
        this.alasanBrondolan = alasanBrondolan;
    }

    public int getTotalJanjang() {
        return totalJanjang;
    }

    public void setTotalJanjang(int totalJanjang) {
        this.totalJanjang = totalJanjang;
    }
}
