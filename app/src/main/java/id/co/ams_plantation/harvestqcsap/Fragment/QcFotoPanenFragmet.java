package id.co.ams_plantation.harvestqcsap.Fragment;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.evrencoskun.tableview.TableView;
import com.evrencoskun.tableview.listener.ITableViewListener;
import com.google.gson.Gson;
import com.mikepenz.iconics.utils.Utils;
import com.roughike.swipeselector.OnSwipeItemSelectedListener;
import com.roughike.swipeselector.SwipeItem;
import com.roughike.swipeselector.SwipeSelector;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.ObjectRepository;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import id.co.ams_plantation.harvestqcsap.HarvestApp;
import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.TableView.adapter.SessionQcFotoPanenAdapter;
import id.co.ams_plantation.harvestqcsap.TableView.holder.ColumnHeaderViewHolder;
import id.co.ams_plantation.harvestqcsap.TableView.holder.SessionQcFotoPanenRowHeaderViewHolder;
import id.co.ams_plantation.harvestqcsap.TableView.popup.ColumnHeaderPopup;
import id.co.ams_plantation.harvestqcsap.TableView.popup.RowHeaderLongPressPopup;
import id.co.ams_plantation.harvestqcsap.TableView.view_model.SessionQcFotoPanenTableViewModel;
import id.co.ams_plantation.harvestqcsap.connection.ServiceResponse;
import id.co.ams_plantation.harvestqcsap.model.DataNitrit;
import id.co.ams_plantation.harvestqcsap.model.FilePdf;
import id.co.ams_plantation.harvestqcsap.model.QuestionAnswer;
import id.co.ams_plantation.harvestqcsap.model.SessionQcFotoPanen;
import id.co.ams_plantation.harvestqcsap.model.User;
import id.co.ams_plantation.harvestqcsap.presenter.ConnectionPresenter;
import id.co.ams_plantation.harvestqcsap.ui.BaseActivity;
import id.co.ams_plantation.harvestqcsap.ui.InspectionFotoActivity;
import id.co.ams_plantation.harvestqcsap.ui.MainMenuActivity;
import id.co.ams_plantation.harvestqcsap.util.CompleteTextViewHelper;
import id.co.ams_plantation.harvestqcsap.util.GlobalHelper;
import id.co.ams_plantation.harvestqcsap.util.SessionFotoPanenHelper;
import id.co.ams_plantation.harvestqcsap.util.WidgetHelper;
import id.co.ams_plantation.harvestqcsap.view.ApiView;

/**
 * Created on : 12,October,2021
 * Author     : Afid
 */

public class QcFotoPanenFragmet extends Fragment implements ApiView {
    int LongOperation;
    public static final int STATUS_LONG_OPERATION_NONE = 0;
    public static final int STATUS_LONG_OPERATION_SEARCH = 1;
    public static final int STATUS_LONG_OPERATION_SELECT_ITEM = 2;
    public static final int STATUS_LONG_OPERATION_GET_CHOICE_SELECTION = 3;
    public static final int STATUS_LONG_OPERATION_SET_DATA_CHOICE_SELECTION = 4;
    public static final int STATUS_LONG_OPERATION_INSERT_NEW_SESSION = 5;
    public static final int STATUS_LONG_OPERATION_SET_NEW_SESSION = 6;
    public static final int STATUS_LONG_OPERATION_CEK_NEW_SESSION = 7;
    public static final int STATUS_LONG_OPERATION_DELETE = 8;
    public static final int STATUS_LONG_OPERATION_DOWNLOAD_REPORT_SESSION = 9;
    public static final int STATUS_LONG_OPERATION_VIEW_REPORT_SESSION = 10;

    RelativeLayout ltableview;
    TableView mTableView;
    CompleteTextViewHelper etSearch;
    Button btnSearch;
    TextView tvFoto;
    TextView tvFotoQc;

    SessionQcFotoPanenTableViewModel mTableViewModel;
    SessionQcFotoPanenAdapter adapter;
    SessionFotoPanenHelper sessionFotoPanenHelper;
    SessionQcFotoPanen seletedSessionQcFotoPanen;
    boolean isVisibleToUser;
    MainMenuActivity mainMenuActivity;
    LinearLayout llNewData;
    QuestionAnswer questionAnswerMethode;
    QuestionAnswer questionAnswerSelection;
    ServiceResponse responseApi;

    String afdelingObj;
    JSONObject postBody;
    String idSession;
    Date dateSelected;
    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");

    HashMap<String, SessionQcFotoPanen> origin;
    Set<String> listSearch;
    int totalFoto;
    int totalQcFoto;
    FilePdf filePdf;

    public static QcFotoPanenFragmet getInstance(){
        QcFotoPanenFragmet fragment = new QcFotoPanenFragmet();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.qc_foto_layout,null,false);
        mainMenuActivity = (MainMenuActivity) getActivity();
        assert mainMenuActivity != null;
        mainMenuActivity.connectionPresenter = new ConnectionPresenter(this);
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        this.isVisibleToUser = isVisibleToUser;
        main();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SwipeSelector menuSwipeSelector = view.findViewById(R.id.menuSwipeSelector);
        etSearch = view.findViewById(R.id.etSearch);
        btnSearch = view.findViewById(R.id.btnSearch);
        mTableView = view.findViewById(R.id.tableview);
        llNewData = view.findViewById(R.id.llNewData);
        ltableview = view.findViewById(R.id.ltableview);
        tvFoto = view.findViewById(R.id.tvFoto);
        tvFotoQc = view.findViewById(R.id.tvFotoQc);
        dateSelected = new Date();

        ArrayList<SwipeItem> swipeItems= new ArrayList<>();
        Long now = System.currentTimeMillis();
        for(int i = 0; i < GlobalHelper.MAX_LAST_DAY_DATA; i++){
            Long l = now - ( i * 24 * 60 * 60 * 1000);
            swipeItems.add(new SwipeItem(i,sdf.format(l),""));
        }
        Collections.reverse(swipeItems);

        SwipeItem[] tmpStrSwipe = new SwipeItem[swipeItems.size()];
        tmpStrSwipe = swipeItems.toArray(tmpStrSwipe);
        menuSwipeSelector.setItems(tmpStrSwipe);
        menuSwipeSelector.selectItemAt(swipeItems.size() - 1);
        menuSwipeSelector.setOnItemSelectedListener(new OnSwipeItemSelectedListener() {
            @Override
            public void onItemSelected(SwipeItem item) {
                try {
                    Date date = sdf.parse(item.title);
                    dateSelected = date;
                    startLongOperation(STATUS_LONG_OPERATION_NONE);
                    Log.d("item Date ", String.valueOf(date.getTime()) + " "+ sdf.format(date));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });

        Gson gson = new Gson();
        Nitrite dbQA = GlobalHelper.getTableNitrit(GlobalHelper.TABEL_QCQuestionAnswer);
        ObjectRepository<DataNitrit> repositoryQA = dbQA.getRepository(DataNitrit.class);
        Iterable<DataNitrit> Iterable = repositoryQA.find().project(DataNitrit.class);
        for (Iterator iterator = Iterable.iterator(); iterator.hasNext();) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            QuestionAnswer questionAnswer = gson.fromJson(dataNitrit.getValueDataNitrit(),QuestionAnswer.class);
            if(questionAnswer.getIdQuestionAnswer().toUpperCase().equals("QAA06")){
                questionAnswerMethode = questionAnswer;
            }else if(questionAnswer.getIdQuestionAnswer().toUpperCase().equals("QAA07")){
                questionAnswerSelection = questionAnswer;
            }
        }
        dbQA.close();

        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.AFDELING_BLOCK, Context.MODE_PRIVATE);
        afdelingObj = preferences.getString(HarvestApp.AFDELING_BLOCK,null);

        sessionFotoPanenHelper = new SessionFotoPanenHelper(getActivity());
        main();

        llNewData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(afdelingObj == null){
                    Toast.makeText(HarvestApp.getContext(), "Mohon Download KML", Toast.LENGTH_LONG).show();
                }else if (afdelingObj.trim().isEmpty()){
                    Toast.makeText(HarvestApp.getContext(), "Mohon Download KML", Toast.LENGTH_LONG).show();

                }else {
                    if (questionAnswerMethode == null || questionAnswerSelection == null) {
                        Toast.makeText(HarvestApp.getContext(), "Master Question Untuk Foto Qc Belum Ada Mohon SYNC Ulang sampai Berhasil", Toast.LENGTH_LONG).show();
                    } else {
                        startLongOperation(STATUS_LONG_OPERATION_CEK_NEW_SESSION);
                    }
                }
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etSearch.getText().toString().isEmpty()){
                    new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_NONE));
                }else{
                    new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_SEARCH));
                }
            }
        });
    }

    private void main(){
        if (isVisibleToUser && getActivity() != null) {
            mainMenuActivity.ivLogo.setVisibility(View.VISIBLE);
            mainMenuActivity.btnFilter.setVisibility(View.GONE);
            startLongOperation(STATUS_LONG_OPERATION_NONE);
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }

    private void updateDataRv(int status){
        origin = new HashMap<>();
        listSearch = new android.support.v4.util.ArraySet<>();
        totalFoto = 0;
        totalQcFoto = 0;

        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_FOTO_PANEN);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = Iterable.iterator(); iterator.hasNext();) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            SessionQcFotoPanen sessionQcFotoPanen = gson.fromJson(dataNitrit.getValueDataNitrit(),SessionQcFotoPanen.class);

            if(GlobalHelper.dataAllUser != null){
                User user =  GlobalHelper.dataAllUser.get(sessionQcFotoPanen.getCreateBy());
                long tglYangDipakai = sessionQcFotoPanen.getCreateDate();
                if(sessionQcFotoPanen.getUpdateDate() != 0){
                    tglYangDipakai = sessionQcFotoPanen.getUpdateDate();
                }
                if (sdf.format(dateSelected).equals(sdf.format(new Date(tglYangDipakai)))) {
                    if(sessionQcFotoPanen.getStatus() != SessionQcFotoPanen.Status_Deleted) {
                        if (status == STATUS_LONG_OPERATION_SEARCH) {
                            if (sessionQcFotoPanen.getAfdeling().toLowerCase().equals(etSearch.getText().toString().toLowerCase()) ||
                                    sessionQcFotoPanen.getMethode().getQcAnsware().toLowerCase().equals(etSearch.getText().toString().toLowerCase()) ||
                                    SessionFotoPanenHelper.getSelection(sessionQcFotoPanen).toLowerCase().equals(etSearch.getText().toString().toLowerCase()) ||
                                    SessionFotoPanenHelper.getRequestDateSession(sessionQcFotoPanen).toLowerCase().equals(etSearch.getText().toString().toLowerCase()) ||
                                    String.valueOf(sessionQcFotoPanen.getQcFotoPanen().size()).equals(etSearch.getText().toString().toLowerCase()) ||
                                    String.valueOf(SessionFotoPanenHelper.getPersen(sessionQcFotoPanen)).equals(etSearch.getText().toString().toLowerCase())
                            ) {
                                addRowTable(sessionQcFotoPanen);
                            }

                        } else if (status == STATUS_LONG_OPERATION_NONE) {
                            addRowTable(sessionQcFotoPanen);
                        }

                        listSearch.add(sessionQcFotoPanen.getAfdeling());
                        listSearch.add(sessionQcFotoPanen.getMethode().getQcAnsware());
                        listSearch.add(SessionFotoPanenHelper.getSelection(sessionQcFotoPanen));
                        listSearch.add(SessionFotoPanenHelper.getRequestDateSession(sessionQcFotoPanen));
                        listSearch.add(String.valueOf(sessionQcFotoPanen.getQcFotoPanen().size()));
                        listSearch.add(String.valueOf(SessionFotoPanenHelper.getPersen(sessionQcFotoPanen)));
                    }
                }
            }
        }
        db.close();

    }

    private void addRowTable(SessionQcFotoPanen sessionQcFotoPanen){
        totalFoto += sessionQcFotoPanen.getQcFotoPanen().size();
        for(int i = 0 ; i < sessionQcFotoPanen.getQcFotoPanen().size(); i++){
            if(sessionQcFotoPanen.getQcFotoPanen().get(i).getDataQcFotoPanen() != null){
                totalQcFoto ++;
            }
        }
        origin.put(sessionQcFotoPanen.getIdSession(), sessionQcFotoPanen);
    }

    private void updateUIRV(int status){
        if(status == STATUS_LONG_OPERATION_NONE){
            etSearch.setText("");
        }

        List<String> lSearch = new ArrayList<>();
        if(listSearch.size() > 0){
            lSearch.addAll(listSearch);
        }

        if(mainMenuActivity != null) {
            ArrayAdapter<String> adapterSearch = new ArrayAdapter<String>(mainMenuActivity,
                    android.R.layout.simple_dropdown_item_1line, lSearch);
            etSearch.setAdapter(adapterSearch);
            etSearch.setThreshold(1);
            adapterSearch.notifyDataSetChanged();
        }

        if(origin.size() > 0 ) {
            ArrayList<SessionQcFotoPanen> list = new ArrayList<>(origin.values());

            Collections.sort(list, new Comparator<SessionQcFotoPanen>() {
                @Override
                public int compare(SessionQcFotoPanen o1, SessionQcFotoPanen o2) {
                    return Long.compare(o2.getCreateDate(), o1.getCreateDate());
                }
            });

            ltableview.setVisibility(View.VISIBLE);
            mTableViewModel = new SessionQcFotoPanenTableViewModel(getContext(), list);
            // Create TableView Adapter
            adapter = new SessionQcFotoPanenAdapter(getContext(), mTableViewModel);
            mTableView.setAdapter(adapter);
            mTableView.setRowHeaderWidth(Utils.convertDpToPx(Objects.requireNonNull(getActivity()),100));
            mTableView.setTableViewListener(new ITableViewListener() {
                @Override
                public void onCellClicked(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

                }

                @Override
                public void onCellLongPressed(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

                }

                @Override
                public void onColumnHeaderClicked(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {
                    if (columnHeaderView instanceof ColumnHeaderViewHolder) {
                        // Create Long Press Popup
                        ColumnHeaderPopup popup = new ColumnHeaderPopup(
                                (ColumnHeaderViewHolder) columnHeaderView, mTableView);
                        // Show
                        popup.show();
                    }
                }

                @Override
                public void onColumnHeaderLongPressed(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {

                }

                @Override
                public void onRowHeaderClicked(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {
                    if (rowHeaderView instanceof SessionQcFotoPanenRowHeaderViewHolder) {

                        String [] sid = ((SessionQcFotoPanenRowHeaderViewHolder) rowHeaderView).getCellId().split("-");
                        seletedSessionQcFotoPanen = origin.get(sid[1]);
                        startLongOperation(STATUS_LONG_OPERATION_SELECT_ITEM);

                    }

                }

                @Override
                public void onRowHeaderLongPressed(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {
                    RowHeaderLongPressPopup popup = new RowHeaderLongPressPopup(rowHeaderView, mTableView,mainMenuActivity);
                    popup.show();
                }
            });
            adapter.setAllItems(mTableViewModel.getColumnHeaderList(), mTableViewModel
                    .getRowHeaderList(), mTableViewModel.getCellList());


//            mTableFilter = new Filter(mTableView);
        }else{
            ltableview.setVisibility(View.GONE);
        }

        Date d = new Date(System.currentTimeMillis());
        if(sdf.format(d).equals(sdf.format(dateSelected))){
            showNewTranskasi();
        }else{
            llNewData.setVisibility(View.GONE);
        }

        tvFoto.setText(String.valueOf(totalFoto));
        tvFotoQc.setText(String.valueOf(totalQcFoto));
    }

    private void showNewTranskasi(){
        if(GlobalHelper.enableToNewTransaksi()){
            llNewData.setVisibility(View.VISIBLE);
        }else{
            llNewData.setVisibility(View.GONE);
        }
    }

    public void preGetChoiceSelection(JSONObject object){
        postBody = object;
        startLongOperation(STATUS_LONG_OPERATION_GET_CHOICE_SELECTION);
    }
    public void insertNewSession(JSONObject object){
        postBody = object;
        startLongOperation(STATUS_LONG_OPERATION_INSERT_NEW_SESSION);

    }

    public void hapusSession(String id){
        this.idSession = id;
        AlertDialog alertDialog = new AlertDialog.Builder(mainMenuActivity, R.style.MyAlertDialogStyle)
                .setTitle("Perhatian")
                .setMessage("Apakah Ada Yakin Untuk Hapus Session "+idSession)
                .setNegativeButton("Jangan Hapus", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton("Yakin Hapus", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        startLongOperation(STATUS_LONG_OPERATION_DELETE);
                    }
                })
                .create();
        alertDialog.show();
    }

    public void downloadReportSession(String id){
        this.idSession = id;
        startLongOperation(STATUS_LONG_OPERATION_DOWNLOAD_REPORT_SESSION);
    }

    public void viewReportSession(String id){
        this.idSession = id;
        if(filePdf != null) {
            if (filePdf.getRemarks().equals(id)) {
                mainMenuActivity.openPDF(filePdf);
            }else{
                startLongOperation(STATUS_LONG_OPERATION_VIEW_REPORT_SESSION);
            }
        }else {
            startLongOperation(STATUS_LONG_OPERATION_VIEW_REPORT_SESSION);
        }
    }

    public void startLongOperation(int status){
        new LongOperation().execute(String.valueOf(status));
        LongOperation = status;
    }

    @Override
    public void successResponse(ServiceResponse serviceResponse) {
        switch (serviceResponse.getTag()) {
            case GetChoiceSelection:
                if (((BaseActivity) Objects.requireNonNull(getActivity())).alertDialogBase != null) {
                    ((BaseActivity) getActivity()).alertDialogBase.cancel();
                }
                responseApi = serviceResponse ;
                startLongOperation(STATUS_LONG_OPERATION_SET_DATA_CHOICE_SELECTION);
                break;
            case InsertNewSession:
                if (((BaseActivity) Objects.requireNonNull(getActivity())).alertDialogBase != null) {
                    ((BaseActivity) getActivity()).alertDialogBase.cancel();
                }
                responseApi = serviceResponse ;
                startLongOperation(STATUS_LONG_OPERATION_SET_NEW_SESSION);
                break;

        }
        Log.d("QcFotoPanen","successResponse " +serviceResponse.getTag());
    }

    @Override
    public void badResponse(ServiceResponse serviceResponse) {
        switch (serviceResponse.getTag()) {
            case GetChoiceSelection:
                Toast.makeText(HarvestApp.getContext(),"Gagal GetNameInputByTPanen "+serviceResponse.getHost(),Toast.LENGTH_SHORT).show();
                if (((BaseActivity) Objects.requireNonNull(getActivity())).alertDialogBase != null) {
                    ((BaseActivity) getActivity()).alertDialogBase.cancel();
                }
                break;
            case InsertNewSession:
                Toast.makeText(HarvestApp.getContext(),"Gagal Insert New Session "+serviceResponse.getHost(),Toast.LENGTH_SHORT).show();
                if (((BaseActivity) Objects.requireNonNull(getActivity())).alertDialogBase != null) {
                    ((BaseActivity) getActivity()).alertDialogBase.cancel();
                }
                break;
        }
        Log.d("QcFotoPanen","successResponse " +serviceResponse.getTag());
    }

    private class LongOperation extends AsyncTask<String, Void, String> {
        //        private AlertDialog alertDialogAllpoin ;
        String dataString;
        String message;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (((BaseActivity) Objects.requireNonNull(getActivity())).alertDialogBase != null) {
                ((BaseActivity) getActivity()).alertDialogBase.cancel();
            }
            ((BaseActivity) getActivity()).alertDialogBase = WidgetHelper.showWaitingDialog(
                    getActivity(),getResources().getString(R.string.wait)
            );
        }

        @Override
        protected String doInBackground(String... strings) {
            switch (Integer.parseInt(strings[0])){
                case STATUS_LONG_OPERATION_NONE:{
                    updateDataRv(Integer.parseInt(strings[0]));
                    break;
                }
                case STATUS_LONG_OPERATION_SEARCH:{
                    updateDataRv(Integer.parseInt(strings[0]));
                    break;
                }
                case STATUS_LONG_OPERATION_SELECT_ITEM:{
                    Gson gson = new Gson();
                    dataString = gson.toJson(seletedSessionQcFotoPanen);
                    break;
                }
                case STATUS_LONG_OPERATION_GET_CHOICE_SELECTION:{
                    mainMenuActivity.connectionPresenter.GetChoiceSelection(postBody);
                    break;
                }
                case STATUS_LONG_OPERATION_SET_DATA_CHOICE_SELECTION:{
                    sessionFotoPanenHelper.setUpTypeValue(responseApi);
                    break;
                }
                case STATUS_LONG_OPERATION_INSERT_NEW_SESSION:{
                    mainMenuActivity.connectionPresenter.InsertNewSession(postBody);
                    break;
                }
                case STATUS_LONG_OPERATION_SET_NEW_SESSION:{
                    message = sessionFotoPanenHelper.setUpNewSession(responseApi);
                    break;
                }
                case STATUS_LONG_OPERATION_CEK_NEW_SESSION:{
                    message = sessionFotoPanenHelper.cekSessionQc();
                    break;
                }
                case STATUS_LONG_OPERATION_DELETE:{
                    message = sessionFotoPanenHelper.hapusSession(idSession);
                    break;
                }
                case STATUS_LONG_OPERATION_DOWNLOAD_REPORT_SESSION:{
                    try {
                        filePdf = sessionFotoPanenHelper.getUrlnFileName(idSession);
                        if(filePdf != null) {
                            message = mainMenuActivity.connectionPresenter.downloadReport(filePdf);
                        }else{
                            message = "link download belum tersedia mohon selesaikan qc dan sync ulang";
                        }
                    } catch (Exception e){
                        message = e.getMessage();
                    }
                    break;
                }
                case STATUS_LONG_OPERATION_VIEW_REPORT_SESSION:{
                    filePdf = sessionFotoPanenHelper.getUrlnFileName(idSession);
                    if(filePdf != null) {
                        if(filePdf.getFullPath().exists()){
                            message = "ok";
                        }else{
                            message = "File Pdf Belum Di Download";
                        }
                    }else{
                        message = "link download belum tersedia mohon selesaikan qc dan sync ulang";
                    }
                    break;
                }
            }
            return strings[0];
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            switch (Integer.parseInt(result)){
                case STATUS_LONG_OPERATION_NONE:{
                    updateUIRV(Integer.parseInt(result));
                    if (((BaseActivity) Objects.requireNonNull(getActivity())).alertDialogBase != null) {
                        ((BaseActivity) getActivity()).alertDialogBase.cancel();
                    }
                    break;
                }
                case STATUS_LONG_OPERATION_SEARCH:{
                    updateUIRV(Integer.parseInt(result));
                    if (((BaseActivity) Objects.requireNonNull(getActivity())).alertDialogBase != null) {
                        ((BaseActivity) getActivity()).alertDialogBase.cancel();
                    }
                    break;
                }
                case STATUS_LONG_OPERATION_SELECT_ITEM:{

                    Intent intent = new Intent(getActivity(), InspectionFotoActivity.class);
                    intent.putExtra("sessionQcFotoPanen",dataString);
                    startActivityForResult(intent,GlobalHelper.RESULT_QC_FOTO_PANEN);
                    if (((BaseActivity) Objects.requireNonNull(getActivity())).alertDialogBase != null) {
                        ((BaseActivity) getActivity()).alertDialogBase.cancel();
                    }
                    break;
                }
                case STATUS_LONG_OPERATION_SET_DATA_CHOICE_SELECTION:{
                    if (((BaseActivity) Objects.requireNonNull(getActivity())).alertDialogBase != null) {
                        ((BaseActivity) getActivity()).alertDialogBase.cancel();
                    }
                    sessionFotoPanenHelper.setUpChoiceSelection();
                    break;
                }
                case STATUS_LONG_OPERATION_SET_NEW_SESSION:{
                    if (((BaseActivity) Objects.requireNonNull(getActivity())).alertDialogBase != null) {
                        ((BaseActivity) getActivity()).alertDialogBase.cancel();
                    }
                    if(message.equals("ok")) {
                        startLongOperation(STATUS_LONG_OPERATION_NONE);
                    }else{
                        Toast.makeText(HarvestApp.getContext(),message,Toast.LENGTH_SHORT).show();
                    }
                    break;
                }
                case STATUS_LONG_OPERATION_CEK_NEW_SESSION:{
                    if (((BaseActivity) Objects.requireNonNull(getActivity())).alertDialogBase != null) {
                        ((BaseActivity) getActivity()).alertDialogBase.cancel();
                    }
                    if(!message.equals("ok")){
                        Toast.makeText(HarvestApp.getContext(),message,Toast.LENGTH_LONG).show();
                    }else{
                        sessionFotoPanenHelper.sessionForm(questionAnswerMethode, questionAnswerSelection,afdelingObj);
                    }
                    break;
                }
                case STATUS_LONG_OPERATION_DELETE:{
                    if (((BaseActivity) Objects.requireNonNull(getActivity())).alertDialogBase != null) {
                        ((BaseActivity) getActivity()).alertDialogBase.cancel();
                    }
                    if(!message.equals("ok")){
                        Toast.makeText(HarvestApp.getContext(),message,Toast.LENGTH_LONG).show();
                    }else{
                        Toast.makeText(HarvestApp.getContext(),"Berhasil Di Hapus",Toast.LENGTH_SHORT).show();
                    }
                    startLongOperation(STATUS_LONG_OPERATION_NONE);
                    break;
                }
                case STATUS_LONG_OPERATION_DOWNLOAD_REPORT_SESSION:{
                    if (((BaseActivity) Objects.requireNonNull(getActivity())).alertDialogBase != null) {
                        ((BaseActivity) getActivity()).alertDialogBase.cancel();
                    }
                    if(!message.equals("ok")){
                        Toast.makeText(HarvestApp.getContext(),message,Toast.LENGTH_LONG).show();
                    }else{
                        Toast.makeText(HarvestApp.getContext(),"Berhasil Download",Toast.LENGTH_SHORT).show();
                        viewReportSession(idSession);
                    }
                    break;
                }
                case STATUS_LONG_OPERATION_VIEW_REPORT_SESSION:{
                    if (((BaseActivity) Objects.requireNonNull(getActivity())).alertDialogBase != null) {
                        ((BaseActivity) getActivity()).alertDialogBase.cancel();
                    }
                    if(!message.equals("ok")){
                        Toast.makeText(HarvestApp.getContext(),message,Toast.LENGTH_LONG).show();
                    }else{
                        mainMenuActivity.openPDF(filePdf);
                    }
                    break;
                }
            }

        }
    }

}
