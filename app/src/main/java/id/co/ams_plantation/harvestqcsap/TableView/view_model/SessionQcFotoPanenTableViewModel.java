package id.co.ams_plantation.harvestqcsap.TableView.view_model;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import id.co.ams_plantation.harvestqcsap.TableView.model.TableViewCell;
import id.co.ams_plantation.harvestqcsap.TableView.model.TableViewColumnHeader;
import id.co.ams_plantation.harvestqcsap.TableView.model.TableViewRowHeader;
import id.co.ams_plantation.harvestqcsap.model.SessionQcFotoPanen;
import id.co.ams_plantation.harvestqcsap.util.SessionFotoPanenHelper;

/**
 * Created on : 26,October,2021
 * Author     : Afid
 */

public class SessionQcFotoPanenTableViewModel {
    private Context mContext;
    private ArrayList<SessionQcFotoPanen> qcSessionQcFotoPanens;
    String [] HeaderColumn = {
            "Tgl Request",
            "Afdeling",
            "Method",
            "Selection",
            "Sample",
            "% QC"
    };

    public SessionQcFotoPanenTableViewModel(Context context, ArrayList<SessionQcFotoPanen> hSessionQcFotoPanen) {
        mContext = context;
        qcSessionQcFotoPanens = new ArrayList<>();
        qcSessionQcFotoPanens = hSessionQcFotoPanen;
    }

    private List<TableViewRowHeader> getSimpleRowHeaderList() {
        List<TableViewRowHeader> list = new ArrayList<>();
        for (int i = 0; i < qcSessionQcFotoPanens.size(); i++) {
            String id = i + "-" + qcSessionQcFotoPanens.get(i).getIdSession() + "-" + qcSessionQcFotoPanens.get(i).getStatus();
            TableViewRowHeader header = new TableViewRowHeader(id, qcSessionQcFotoPanens.get(i).getIdSession().substring(0,11));
            list.add(header);
        }

        return list;
    }

    /**
     * This is a dummy model list test some cases.
     */
    private List<TableViewColumnHeader> getRandomColumnHeaderList() {
        List<TableViewColumnHeader> list = new ArrayList<>();

        for (int i = 0; i < HeaderColumn.length; i++) {
            TableViewColumnHeader header = new TableViewColumnHeader(String.valueOf(i), HeaderColumn[i]);
            list.add(header);
        }

        return list;
    }

    /**
     * This is a dummy model list test some cases.
     */
    private List<List<TableViewCell>> getCellListForSortingTest() {
        List<List<TableViewCell>> list = new ArrayList<>();
        for (int i = 0; i < qcSessionQcFotoPanens.size(); i++) {
            List<TableViewCell> cellList = new ArrayList<>();
            for (int j = 0; j < HeaderColumn.length; j++) {
                // Create dummy id.
                String id = j + "-" + qcSessionQcFotoPanens.get(i).getIdSession() + "-" + qcSessionQcFotoPanens.get(i).getStatus() ;
                TableViewCell cell = null;
                switch (j){
                    case 0:
                        cell = new TableViewCell(id, SessionFotoPanenHelper.getRequestDateSession(qcSessionQcFotoPanens.get(i)));
                        break;
                    case 1:
                        cell = new TableViewCell(id, qcSessionQcFotoPanens.get(i).getAfdeling());
                        break;
                    case 2:
                        cell = new TableViewCell(id, qcSessionQcFotoPanens.get(i).getMethode().getQcAnsware());
                        break;
                    case 3:
                        cell = new TableViewCell(id, SessionFotoPanenHelper.getSelection(qcSessionQcFotoPanens.get(i)));
                        break;
                    case 4:
                        cell = new TableViewCell(id, qcSessionQcFotoPanens.get(i).getQcFotoPanen().size());
                        break;
                    case 5:
                        cell = new TableViewCell(id, SessionFotoPanenHelper.getPersen(qcSessionQcFotoPanens.get(i)));
                        break;
                }
                cellList.add(cell);
            }
            list.add(cellList);
        }

        return list;
    }

//    /**
//     * This is a dummy model list test some cases.
//     */
//    private List<List<TableViewCell>> getRandomCellList() {
//        List<List<TableViewCell>> list = new ArrayList<>();
//        for (int i = 0; i < ROW_SIZE; i++) {
//            List<TableViewCell> cellList = new ArrayList<>();
//            list.add(cellList);
//            for (int j = 0; j < COLUMN_SIZE; j++) {
//                String text = "cell " + j + " " + i;
//                int random = new Random().nextInt();
//                if (random % 2 == 0 || random % 5 == 0 || random == j) {
//                    text = "large cell  " + j + " " + i + getRandomString() + ".";
//                }
//
//                // Create dummy id.
//                String id = j + "-" + i;
//
//                TableViewCell cell = new TableViewCell(id, text);
//                cellList.add(cell);
//            }
//        }
//
//        return list;
//    }
//
//    /**
//     * This is a dummy model list test some cases.
//     */
//    private List<List<TableViewCell>> getEmptyCellList() {
//        List<List<TableViewCell>> list = new ArrayList<>();
//        for (int i = 0; i < ROW_SIZE; i++) {
//            List<TableViewCell> cellList = new ArrayList<>();
//            list.add(cellList);
//            for (int j = 0; j < COLUMN_SIZE; j++) {
//
//                // Create dummy id.
//                String id = j + "-" + i;
//
//                TableViewCell cell = new TableViewCell(id, "");
//                cellList.add(cell);
//            }
//        }
//
//        return list;
//    }
//
//    private List<TableViewRowHeader> getEmptyRowHeaderList() {
//        List<TableViewRowHeader> list = new ArrayList<>();
//        for (int i = 0; i < ROW_SIZE; i++) {
//            TableViewRowHeader header = new TableViewRowHeader(String.valueOf(i), "");
//            list.add(header);
//        }
//
//        return list;
//    }
//
//    /**
//     * This is a dummy model list test some cases.
//     */
//    public static List<List<TableViewCell>> getRandomCellList(int startIndex) {
//        List<List<TableViewCell>> list = new ArrayList<>();
//        for (int i = 0; i < ROW_SIZE; i++) {
//            List<Cell> cellList = new ArrayList<>();
//            list.add(cellList);
//            for (int j = 0; j < COLUMN_SIZE; j++) {
//                String text = "cell " + j + " " + (i + startIndex);
//                int random = new Random().nextInt();
//                if (random % 2 == 0 || random % 5 == 0 || random == j) {
//                    text = "large cell  " + j + " " + (i + startIndex) + getRandomString() + ".";
//                }
//
//                String id = j + "-" + (i + startIndex);
//
//                Cell cell = new Cell(id, text);
//                cellList.add(cell);
//            }
//        }
//
//        return list;
//    }


    private static String getRandomString() {
        Random r = new Random();
        String str = " a ";
        for (int i = 0; i < r.nextInt(); i++) {
            str = str + " a ";
        }

        return str;
    }

//    public Drawable getDrawable(int value, boolean isGender) {
//        if (isGender) {
//            return value == BOY ? mBoyDrawable : mGirlDrawable;
//        } else {
//            return value == SAD ? mSadDrawable : mHappyDrawable;
//        }
//    }

    public List<List<TableViewCell>> getCellList() {
        return getCellListForSortingTest();
    }

    public List<TableViewRowHeader> getRowHeaderList() {
        return getSimpleRowHeaderList();
    }

    public List<TableViewColumnHeader> getColumnHeaderList() {
        return getRandomColumnHeaderList();
    }

}
