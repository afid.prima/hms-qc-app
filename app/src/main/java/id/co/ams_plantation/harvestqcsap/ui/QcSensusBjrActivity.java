package id.co.ams_plantation.harvestqcsap.ui;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.esri.android.map.GraphicsLayer;
import com.esri.android.map.ags.ArcGISLocalTiledLayer;
import com.esri.android.map.ogc.kml.KmlLayer;
import com.esri.android.runtime.ArcGISRuntime;
import com.esri.core.geometry.SpatialReference;
import com.esri.core.runtime.LicenseLevel;
import com.esri.core.runtime.LicenseResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;

import java.io.File;

import butterknife.BindView;
import id.co.ams_plantation.harvestqcsap.Fragment.QcSensusBjrInputFragment;
import id.co.ams_plantation.harvestqcsap.encryptor.Encrypts;
import id.co.ams_plantation.harvestqcsap.model.QcMutuBuah;
import id.co.ams_plantation.harvestqcsap.model.TransaksiPanen;
import id.co.ams_plantation.harvestqcsap.util.GlobalHelper;
import id.co.ams_plantation.harvestqcsap.view.TphView;
import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.model.Estate;
import id.co.ams_plantation.harvestqcsap.model.SensusBjr;
import id.co.ams_plantation.harvestqcsap.util.QrHelper;
import id.co.ams_plantation.harvestqcsap.util.WidgetHelper;

import static id.co.ams_plantation.harvestqcsap.util.GlobalHelper.REQUEST_RESOLVE_ERROR;

public class QcSensusBjrActivity extends BaseActivity implements TphView, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener ,com.google.android.gms.location.LocationListener{

    public Estate estate;
    public int TYPE_NFC;
    public String valueNFC;
    public QrHelper qrHelper;
    public TransaksiPanen selectedTransaksiPanen;
    public SensusBjr selectedSensusBjr;

    public ArcGISLocalTiledLayer tiledLayer;
    public KmlLayer kmlLayer;
    public SpatialReference spatialReference;
    public GraphicsLayer graphicsLayer;
    public GraphicsLayer graphicsLayerTPH;
    public GraphicsLayer graphicsLayerLangsiran;
    public GraphicsLayer graphicsLayerText;
    public String locKmlMap;
    public boolean isMapLoaded;

    Activity activity;

    RelativeLayout lShowHide;
    RelativeLayout lShow;
    RelativeLayout lHide;
    ImageView ivShow;
    ImageView ivHide;

    @BindView(R.id.myCoordinatorLayout)
    public CoordinatorLayout myCoordinatorLayout;

    public Snackbar searchingGPS;
//    /****************************************** NFC ******************************************************************/
//    public Tag myTag;
//    public NfcAdapter nfcAdapter;
//    public PendingIntent pendingIntent;
//    public IntentFilter writeTagFilters[];
//
//    /****************************************************************************************************************/
//    private static Handler handler;

    @Override
    protected void initPresenter() {}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LicenseResult licenseResult = ArcGISRuntime.setClientId("uFfCzKhpVfi0ggjz");
        LicenseLevel licenseLevel = ArcGISRuntime.License.getLicenseLevel();
        Log.e("License Result", licenseResult.toString() + " | " + licenseLevel.toString());
        setContentView(R.layout.new_layout_tph);
        getSupportActionBar().hide();

        mapView = findViewById(R.id.map);
        zoomToLocation = findViewById(R.id.zoomToLocation);
        rlMap = findViewById(R.id.rlMap);
        lShowHide = findViewById(R.id.lShowHide);
        ivShow = findViewById(R.id.ivShow);
        ivHide = findViewById(R.id.ivHide);
        lShow = findViewById(R.id.lShow);
        lHide = findViewById(R.id.lHide);

        qrHelper = new QrHelper(this);
        activity = this;
        zoomToLocation.setImageDrawable(new
                IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_my_location)
                .sizeDp(24)
                .colorRes(R.color.Black));
        ivShow.setImageDrawable(new
                IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_chevron_down).sizeDp(24)
                .colorRes(R.color.Black));

        ivHide.setImageDrawable(new
                IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_chevron_up).sizeDp(24)
                .colorRes(R.color.Black));

        rlMap.setVisibility(View.GONE);
        lShow.setVisibility(View.GONE);
        lShowHide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(lHide.getVisibility() == View.GONE){
                    rlMap.setVisibility(View.VISIBLE);
                    lHide.setVisibility(View.VISIBLE);
                    lShow.setVisibility(View.GONE);
                }else{
                    rlMap.setVisibility(View.GONE);
                    lHide.setVisibility(View.GONE);
                    lShow.setVisibility(View.VISIBLE);
                }
            }
        });

        String strEst =  Encrypts.decrypt(GlobalHelper.getFileContent(Encrypts.encrypt(GlobalHelper.SELECTED_ESTATE)));
        Gson gson = new Gson();
        estate = (Estate) gson.fromJson(strEst, Estate.class);

        Intent intent = getIntent();
        if(intent.getExtras() != null) {
            String transaksi = intent.getExtras().getString("transaksiPanen");
            if (transaksi != null) {
                selectedTransaksiPanen = gson.fromJson(transaksi, TransaksiPanen.class);
            }

            String SqcMutuBuah = intent.getExtras().getString("qcBJR");
            if (SqcMutuBuah != null) {
                selectedSensusBjr = gson.fromJson(SqcMutuBuah, SensusBjr.class);
                selectedTransaksiPanen = selectedSensusBjr.getTransaksiPanen();
            }
        }

        toolBarSetup();
        main();
        mapSetup();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void main() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_container, QcSensusBjrInputFragment.getInstance())
                .commit();
    }

    private void mapSetup(){

        graphicsLayer = new GraphicsLayer();
        graphicsLayerTPH = new GraphicsLayer();
        graphicsLayerLangsiran = new GraphicsLayer();
        graphicsLayerText = new GraphicsLayer();

        rlMap.setVisibility(View.VISIBLE);

        mapView.removeAll();
        graphicsLayer.removeAll();
        mapView.setMinScale(250000.0d);
        mapView.setMaxScale(1000.0d);
        mapView.enableWrapAround(true);

        String locTiledMap = GlobalHelper.decryptFiles(GlobalHelper.TYPE_VKM);
        if(locTiledMap != null){
            File file = new File(locTiledMap);
            if(!file.exists()){
                locTiledMap = null;
            }
        }

        if(locTiledMap != null){

            locKmlMap = GlobalHelper.decryptFiles(GlobalHelper.TYPE_KMLBLOCK);
            if (locKmlMap != null) {
                kmlLayer = new KmlLayer(locKmlMap);
//                kmlLayer.setOpacity(0.03f);
                mapView.addLayer(kmlLayer);
                kmlLayer.setVisible(false);
            }
            tiledLayer = new ArcGISLocalTiledLayer(locTiledMap, true);
            mapView.addLayer(tiledLayer);
        }else{
            locTiledMap = GlobalHelper.getFilePath(GlobalHelper.TYPE_VKM, GlobalHelper.getEstate());
            if (locTiledMap != null) {
                locTiledMap = GlobalHelper.decryptFiles(GlobalHelper.TYPE_VKM);
                tiledLayer = new ArcGISLocalTiledLayer(locTiledMap, true);
                mapView.addLayer(tiledLayer);
            }

            locKmlMap = GlobalHelper.decryptFiles(GlobalHelper.TYPE_KMLBLOCK);
            if(locKmlMap!=null){
                kmlLayer = new KmlLayer(locKmlMap);
//                kmlLayer.setOpacity(0.8f);
                mapView.addLayer(kmlLayer);
                kmlLayer.setVisible(true);
            }
        }

        graphicsLayer.setMinScale(150000d);
        graphicsLayer.setMaxScale(1000d);
        graphicsLayerTPH.setMinScale(150000d);
        graphicsLayerTPH.setMaxScale(1000d);
        graphicsLayerLangsiran.setMinScale(150000d);
        graphicsLayerLangsiran.setMaxScale(1000d);
        graphicsLayerText.setMinScale(70000d);
        graphicsLayerText.setMaxScale(1000d);
        mapView.addLayer(graphicsLayer);
        mapView.addLayer(graphicsLayerTPH);
        mapView.addLayer(graphicsLayerLangsiran);
        mapView.addLayer(graphicsLayerText);
        mapView.invalidate();
    }

    private void toolBarSetup() {
        getSupportActionBar().hide();
        ImageView imgBack = (ImageView) findViewById(R.id.toolbar_icon);
        TextView textToolbar = (TextView) findViewById(R.id.toolbar_text);
        imgBack.setVisibility(View.VISIBLE);
        imgBack.setImageDrawable(new IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_arrow_left)
                .colorRes(R.color.White)
                .sizeDp(24));
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backProses();
            }
        });
        textToolbar.setText(getResources().getString(R.string.sensus_bjr));
    }

    public void backProses() {
        if (searchingGPS != null) {
            searchingGPS.dismiss();
        }
        setResult(GlobalHelper.RESULT_QC_SENSUS_BJR);
        finish();
    }

    private void createLocationListener() {
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

//        searchingGPS = Snackbar.make(myCoordinatorLayout,getResources().getString(R.string.search_gps),Snackbar.LENGTH_INDEFINITE);
//        searchingGPS.show();
        WidgetHelper.warningFindGps(this,myCoordinatorLayout);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1,
                1f, mLocationListener);
    }

//    @Override
//    protected void onNewIntent(Intent intent) {
//        setIntent(intent);
//        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
//            myTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
//            Log.i("tagNFC", GlobalHelper.bytesToHexString(myTag.getId()));
//            Log.i("tagNFC", Arrays.toString(myTag.getTechList()));
//        }
//        valueNFC = NfcHelper.readFromIntent(intent);
//        int formatNFC = NfcHelper.cekFormatNFC(valueNFC);
//        if (valueNFC != null) {
//            if (formatNFC != GlobalHelper.TYPE_NFC_SALAH) {
//                if(valueNFC.length() == 5) {
//                    if(NfcHelper.stat != NfcHelper.NFC_TIDAK_BISA_WRITE){
////                        if (getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof QcSensusBjrInputFragment) {
////                            QcSensusBjrInputFragment fragment = (QcSensusBjrInputFragment) getSupportFragmentManager().findFragmentById(R.id.content_container);
////                            fragment.startLongOperation(NfcHelper.stat);
////                        }
//                    }else{
//                        Toast.makeText(this, getResources().getString(R.string.nfc_empty) + " " + GlobalHelper.LIST_NFC[formatNFC], Toast.LENGTH_SHORT).show();
//                    }
//                }else{
//                    cekDataPassing(valueNFC, this);
//                }
//                TYPE_NFC = formatNFC;
//            } else {
//                Toast.makeText(HarvestApp.getContext(), HarvestApp.getContext().getResources().getString(R.string.format_not_valid) + " 999", Toast.LENGTH_SHORT).show();
//            }
//        }else {
//            Toast.makeText(this,R.string.format_not_valid,Toast.LENGTH_SHORT).show();
//        }
//    }

    @Override
    public void onBackPressed() {
        backProses();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mGoogleApiClient != null) {

            if (!mGoogleApiClient.isConnected()) {
                mGoogleApiClient.reconnect();
            }
        }
//        NfcHelper.WriteModeOn(nfcAdapter,this,pendingIntent,writeTagFilters);
    }

    @Override
    public void onPause(){
        super.onPause();
        if(mGoogleApiClient != null) {

            if (!mGoogleApiClient.isConnected()) {
                mGoogleApiClient.reconnect();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mGoogleApiClient != null) {

            if (!mGoogleApiClient.isConnected()) {
                mGoogleApiClient.reconnect();
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        WidgetHelper.warningFindGps(this,myCoordinatorLayout);
        starLocationUpdate();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(QcSensusBjrActivity.class.getName(), "GoogleAPIClient connection suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(this, REQUEST_RESOLVE_ERROR);
            } catch (IntentSender.SendIntentException e) {
                // Error with resolution, try again
                mGoogleApiClient.connect();
            }
        }else{
            WidgetHelper.showSnackBar(myCoordinatorLayout,"onConnectionFailed");
//            searchingGPS = Snackbar.make(myCoordinatorLayout,"",Snackbar.LENGTH_INDEFINITE);
//            searchingGPS.show();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        locationChangeListener(location);
    }
}
