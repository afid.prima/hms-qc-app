package id.co.ams_plantation.harvestqcsap.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;

import com.google.gson.Gson;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.ObjectRepository;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;

import id.co.ams_plantation.harvestqcsap.HarvestApp;
import id.co.ams_plantation.harvestqcsap.encryptor.Encrypts;
import id.co.ams_plantation.harvestqcsap.model.StampImage;
import id.co.ams_plantation.harvestqcsap.ui.QcMutuAncakActivity;
import id.co.ams_plantation.harvestqcsap.model.DataNitrit;
import id.co.ams_plantation.harvestqcsap.model.QcAncak;
import id.co.ams_plantation.harvestqcsap.model.QcAncakPohon;


public class MutuAncakHelper {

    FragmentActivity activity;
    QcMutuAncakActivity qcMutuAncakActivity;

    public MutuAncakHelper(FragmentActivity activity) {
        this.activity = activity;
        if(activity instanceof QcMutuAncakActivity){
            qcMutuAncakActivity = (QcMutuAncakActivity)  activity;
        }
    }


    public void hapusFileQcAncak(Boolean withDB){
        File fileAngkutSelected = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECTED_QC_ANCAK] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECTED_QC_ANCAK] );
        File fileAngkutCounter = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+  GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECTED_QC_ANCAK] + "/" + "COUNT",GlobalHelper.getUser().getUserID() );
        File fileDBAngkut = new File(GlobalHelper.getDatabasePathHMS() + "/" + Encrypts.encrypt(GlobalHelper.TABLE_QC_ANCAK_POHON) + "/" +Encrypts.encrypt(GlobalHelper.TABLE_QC_ANCAK_POHON)+ ".db");
        if(fileAngkutCounter.exists()){
            fileAngkutCounter.delete();
        }
        if(fileAngkutSelected.exists()){
            fileAngkutSelected.delete();
        }
        if(fileDBAngkut.exists() && withDB){
            fileDBAngkut.delete();
        }
    }

    public void createNewMutuAncak(QcAncak qcAncak){;
        qcAncak.setQcAncakPohons(new ArrayList<>());
        Gson gson = new Gson();
        String object = gson.toJson(qcAncak);

        File file = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECTED_QC_ANCAK] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECTED_QC_ANCAK] );
        GlobalHelper.writeFileContent(file.getAbsolutePath(),object);

        if(qcAncak.getTph() != null){

            StampImage stampImage = new StampImage();
            stampImage.setTph(qcAncak.getTph());
            stampImage.setBlock(qcAncak.getTph().getBlock());

            SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.STAMP_IMAGES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(HarvestApp.STAMP_IMAGES,gson.toJson(stampImage));
            editor.apply();
        }
    }

    public String readMutuAncakSelected(){
        File fileAngkutSelected = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECTED_QC_ANCAK] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECTED_QC_ANCAK] );
        if(fileAngkutSelected.exists()){
            return GlobalHelper.readFileContent(fileAngkutSelected.getAbsolutePath());
        }else{
            return null;
        }
    }

    public boolean validasiAncak(){
        if(qcMutuAncakActivity.seletedQcAncak.getTph() == null){
            WidgetHelper.warningFindGps(qcMutuAncakActivity,qcMutuAncakActivity.myCoordinatorLayout);
//            qcMutuAncakActivity.searchingGPS = Snackbar.make(qcMutuAncakActivity.myCoordinatorLayout,qcMutuAncakActivity.getResources().getString(R.string.please_input_tph_name),Snackbar.LENGTH_LONG);
//            qcMutuAncakActivity.searchingGPS.show();
            return false;
        }
        if (!qcMutuAncakActivity.currentLocationOK()){
//            qcMutuAncakActivity.searchingGPS = Snackbar.make(qcMutuAncakActivity.myCoordinatorLayout,qcMutuAncakActivity.getResources().getString(R.string.search_gps),Snackbar.LENGTH_INDEFINITE);
//            qcMutuAncakActivity.searchingGPS.show();
            WidgetHelper.warningFindGps(qcMutuAncakActivity,qcMutuAncakActivity.myCoordinatorLayout);
            return false;
        }

        return true;
    }

    public boolean saveQcAncak(){
        try {
            boolean isNew = false;
            int tBuahMatahari = 0;
            int tJanjangPanen = 0;
            int tBuahTinggal = 0;
            int tBrondolan = 0;
            int tOverPrun = 0;
            int tPelepahSengkel = 0;
            int tSusunanPelepah = 0;
            ArrayList<QcAncakPohon> pohons = new ArrayList<>();
            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_ANCAK_POHON);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
            for (Iterator iterator = Iterable.iterator(); iterator.hasNext();) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                QcAncakPohon pohon = gson.fromJson(dataNitrit.getValueDataNitrit(),QcAncakPohon.class);
                pohons.add(pohon);
                tBuahMatahari += pohon.getBuahMatahari();
                tJanjangPanen += pohon.getJanjangPanen();
                tBuahTinggal += pohon.getBuahTinggal();
                tBrondolan += pohon.getBrondolanTinggal();
                tOverPrun += pohon.isOverPrun() == true ? 1 : 0;
                tPelepahSengkel += pohon.isPelepahSengkel() == true ? 1 : 0;
                tSusunanPelepah += pohon.isSusunanPelepah() == true ? 1 : 0;
            }
            db.close();

            qcMutuAncakActivity.seletedQcAncak.setStatus(QcAncak.Save);
            qcMutuAncakActivity.seletedQcAncak.setCreateBy(Integer.parseInt(GlobalHelper.getUser().getUserID()));
            qcMutuAncakActivity.seletedQcAncak.setFinishTime(System.currentTimeMillis());
            qcMutuAncakActivity.seletedQcAncak.setTotalJanjangPanen(tJanjangPanen);
            qcMutuAncakActivity.seletedQcAncak.setTotalBuahTinggal(tBuahTinggal);
            qcMutuAncakActivity.seletedQcAncak.setTotalBrondolan(tBrondolan);
            qcMutuAncakActivity.seletedQcAncak.setTotalOverPrun(tOverPrun);
            qcMutuAncakActivity.seletedQcAncak.setTotalPelepahSengkelan(tPelepahSengkel);
            qcMutuAncakActivity.seletedQcAncak.setTotalSusunanPelepah(tSusunanPelepah);
            qcMutuAncakActivity.seletedQcAncak.setTotalBuahMatahari(tBuahMatahari);
            qcMutuAncakActivity.seletedQcAncak.setLatitude(qcMutuAncakActivity.currentlocation.getLatitude());
            qcMutuAncakActivity.seletedQcAncak.setLongitude(qcMutuAncakActivity.currentlocation.getLongitude());
            qcMutuAncakActivity.seletedQcAncak.setQcAncakPohons(pohons);

            if(qcMutuAncakActivity.seletedQcAncak.getIdQcAncak() == null){
                isNew = true;
                SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyHHmm");
                qcMutuAncakActivity.seletedQcAncak.setIdQcAncak("Q" + String.format("%04d", GlobalHelper.getCountNumber(GlobalHelper.LIST_FOLDER_SELECTED_QC_ANCAK))
                        + sdf.format(System.currentTimeMillis()) + GlobalHelper.getUser().getUserID());
            }

            Nitrite db1 = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_ANCAK);
            ObjectRepository<DataNitrit> repository1 = db1.getRepository(DataNitrit.class);
            Gson gson = new Gson();
            DataNitrit dataNitrit = new DataNitrit(qcMutuAncakActivity.seletedQcAncak.getIdQcAncak(),gson.toJson(qcMutuAncakActivity.seletedQcAncak));
            if(isNew) {
                repository1.insert(dataNitrit);
                GlobalHelper.setCountNumberPlusOne(GlobalHelper.LIST_FOLDER_SELECTED_QC_ANCAK);
            }else{
                repository1.update(dataNitrit);
            }
            db1.close();

            qcMutuAncakActivity.tphHelper.saveTPH(qcMutuAncakActivity.seletedQcAncak.getTph(),false);
            hapusFileQcAncak(true);
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public boolean hapusQcAncak(){
        if(qcMutuAncakActivity.seletedQcAncak.getIdQcAncak() == null){
            hapusFileQcAncak(true);
            return true;
        }
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_ANCAK);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Gson gson = new Gson();
        DataNitrit dataNitrit = new DataNitrit(qcMutuAncakActivity.seletedQcAncak.getIdQcAncak(),gson.toJson(qcMutuAncakActivity.seletedQcAncak));
        repository.remove(dataNitrit);
        db.close();
        hapusFileQcAncak(true);
        return true;
    }
}
