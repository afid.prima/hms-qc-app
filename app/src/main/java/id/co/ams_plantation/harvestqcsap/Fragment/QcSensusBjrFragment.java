package id.co.ams_plantation.harvestqcsap.Fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.evrencoskun.tableview.TableView;
import com.evrencoskun.tableview.listener.ITableViewListener;
import com.google.gson.Gson;
import com.mikepenz.iconics.utils.Utils;
import com.roughike.swipeselector.OnSwipeItemSelectedListener;
import com.roughike.swipeselector.SwipeItem;
import com.roughike.swipeselector.SwipeSelector;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.ObjectRepository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import id.co.ams_plantation.harvestqcsap.TableView.adapter.SensusBjrAdapter;
import id.co.ams_plantation.harvestqcsap.TableView.holder.ColumnHeaderViewHolder;
import id.co.ams_plantation.harvestqcsap.TableView.holder.SensusBjrRowHeaderViewHolder;
import id.co.ams_plantation.harvestqcsap.TableView.popup.ColumnHeaderPopup;
import id.co.ams_plantation.harvestqcsap.TableView.view_model.SensusBjrTableViewModel;
import id.co.ams_plantation.harvestqcsap.model.DataNitrit;
import id.co.ams_plantation.harvestqcsap.model.SensusBjr;
import id.co.ams_plantation.harvestqcsap.model.User;
import id.co.ams_plantation.harvestqcsap.ui.BaseActivity;
import id.co.ams_plantation.harvestqcsap.ui.MainMenuActivity;
import id.co.ams_plantation.harvestqcsap.ui.QcMutuBuahActivity;
import id.co.ams_plantation.harvestqcsap.ui.QcSensusBjrActivity;
import id.co.ams_plantation.harvestqcsap.util.CompleteTextViewHelper;
import id.co.ams_plantation.harvestqcsap.util.GlobalHelper;
import id.co.ams_plantation.harvestqcsap.util.WidgetHelper;
import id.co.ams_plantation.harvestqcsap.R;

/**
 * Created by user on 12/4/2018.
 */

public class QcSensusBjrFragment extends Fragment {

    public static final int STATUS_LONG_OPERATION_NONE = 0;
    public static final int STATUS_LONG_OPERATION_SEARCH = 1;
    public static final int STATUS_LONG_OPERATION_SELECT_ITEM = 2;
    public static final int STATUS_LONG_OPERATION_NEW_DATA = 3;

    HashMap<String, SensusBjr> origin;
    SensusBjrAdapter adapter;
    TableView mTableView;
//    Filter mTableFilter;
    SensusBjrTableViewModel mTableViewModel;
    CompleteTextViewHelper etSearch;
    Button btnSearch;
    TextView tvQcTph;
    RelativeLayout ltableview;

    LinearLayout llSearch;
    LinearLayout llNewData;
    LinearLayout ll_total1;

    Set<String> listSearch;
    Date dateSelected;
    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
    SensusBjr selectedSensusBjr;

    boolean isVisibleToUser;
    MainMenuActivity mainMenuActivity;

    public static QcSensusBjrFragment getInstance(){
        QcSensusBjrFragment fragment = new QcSensusBjrFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.qc_buah_layout,null,false);
        mainMenuActivity = (MainMenuActivity) getActivity();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SwipeSelector menuSwipeSelector = view.findViewById(R.id.menuSwipeSelector);
        etSearch = view.findViewById(R.id.etSearch);
        btnSearch = view.findViewById(R.id.btnSearch);
        tvQcTph = view.findViewById(R.id.tvQcTph);
        ltableview = view.findViewById(R.id.ltableview);
        mTableView = view.findViewById(R.id.tableview);
        llSearch = view.findViewById(R.id.llSearch);
        llNewData = view.findViewById(R.id.llNewData);
        ll_total1 = view.findViewById(R.id.ll_total1);

        dateSelected = new Date();
        origin = new HashMap<>();

        llSearch.setVisibility(View.GONE);
        llNewData.setVisibility(View.GONE);
        ll_total1.setVisibility(View.GONE);

        ArrayList<SwipeItem> swipeItems= new ArrayList<>();
        Long now = System.currentTimeMillis();
        for(int i = 0; i < GlobalHelper.MAX_LAST_DAY_DATA; i++){
            Long l = now - ( i * 24 * 60 * 60 * 1000);
            swipeItems.add(new SwipeItem(i,sdf.format(l),""));
        }
        Collections.reverse(swipeItems);

        SwipeItem[] tmpStrSwipe = new SwipeItem[swipeItems.size()];
        tmpStrSwipe = swipeItems.toArray(tmpStrSwipe);
        menuSwipeSelector.setItems(tmpStrSwipe);
        menuSwipeSelector.selectItemAt(swipeItems.size() - 1);
        menuSwipeSelector.setOnItemSelectedListener(new OnSwipeItemSelectedListener() {
            @Override
            public void onItemSelected(SwipeItem item) {
                try {
                    Date date = sdf.parse(item.title);
                    dateSelected = date;
                    startLongOperation(STATUS_LONG_OPERATION_NONE);
                    Log.d("item Date ", String.valueOf(date.getTime()) + " "+ sdf.format(date));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });

//        llNewData.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startLongOperation(STATUS_LONG_OPERATION_NEW_DATA);
//            }
//        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etSearch.getText().toString().isEmpty()){
                    new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_NONE));
                }else{
                    new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_SEARCH));
                }
            }
        });

        startLongOperation(STATUS_LONG_OPERATION_NONE);
        main();
    }

    private void main(){
        if (isVisibleToUser && getActivity() != null) {
            mainMenuActivity.ivLogo.setVisibility(View.VISIBLE);
            mainMenuActivity.btnFilter.setVisibility(View.GONE);
            startLongOperation(STATUS_LONG_OPERATION_NONE);
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }

    private void updateDataRv(int status){
        origin = new HashMap<>();
        listSearch = new android.support.v4.util.ArraySet<>();

        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_BJR);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = Iterable.iterator(); iterator.hasNext();) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();

            SensusBjr sensusBjr = gson.fromJson(dataNitrit.getValueDataNitrit(),SensusBjr.class);
            User user =  GlobalHelper.dataAllUser.get(sensusBjr.getCreateBy());
            if (sdf.format(dateSelected).equals(sdf.format(new Date(sensusBjr.getCreateDate())))) {
                if (status == STATUS_LONG_OPERATION_SEARCH) {
                    if (sensusBjr.getTph().getNamaTph().toLowerCase().contains(etSearch.getText().toString().toLowerCase()) ||
                            sensusBjr.getTph().getBlock().toLowerCase().contains(etSearch.getText().toString().toLowerCase()) ||
                            user.getUserFullName().toLowerCase().contains(etSearch.getText().toString().toLowerCase()) ||
                            String.valueOf(sensusBjr.getRataRataBjr()).contains(etSearch.getText().toString().toLowerCase()) ||
                            String.valueOf(sensusBjr.getBeratBrondolan()).contains(etSearch.getText().toString().toLowerCase())) {
                        addRowTable(sensusBjr);
                    }
                } else if (status == STATUS_LONG_OPERATION_NONE) {
                    addRowTable(sensusBjr);
                }
                if(user != null) {
                    listSearch.add(user.getUserFullName());
                }
                listSearch.add(sensusBjr.getTph().getNamaTph());
                listSearch.add(sensusBjr.getTph().getBlock());
                listSearch.add(String.valueOf(sensusBjr.getRataRataBjr()));
                listSearch.add(String.valueOf(sensusBjr.getBeratBrondolan()));
            }

        }
        db.close();

    }

    private void addRowTable(SensusBjr sensusBjr){
        origin.put(sensusBjr.getIdTPanen(), sensusBjr);
    }

    private void updateUIRV(int status){
        if(status == STATUS_LONG_OPERATION_NONE){
            etSearch.setText("");
        }

        List<String> lSearch = new ArrayList<>();
        lSearch.addAll(listSearch);
        lSearch = GlobalHelper.removeNullUsingIterator(lSearch);

        if(lSearch == null){
            lSearch = new ArrayList<>();
        }

        if(lSearch.size() > 0){
            llSearch.setVisibility(View.VISIBLE);
        }else{
            llSearch.setVisibility(View.GONE);
        }

        if(mainMenuActivity != null) {
            ArrayAdapter<String> adapterSearch = new ArrayAdapter<String>(mainMenuActivity,
                    android.R.layout.simple_dropdown_item_1line, lSearch);
            etSearch.setAdapter(adapterSearch);
            etSearch.setThreshold(1);
            adapterSearch.notifyDataSetChanged();
        }

        if(origin.size() > 0 ) {
            ArrayList<SensusBjr> list = new ArrayList<>(origin.values());

            Collections.sort(list, new Comparator<SensusBjr>() {
                @Override
                public int compare(SensusBjr o1, SensusBjr o2) {
                    return o1.getCreateDate() > o2.getCreateDate() ? -1 : (o1.getCreateDate() < o2.getCreateDate()) ? 1 : 0;
                }
            });

            ltableview.setVisibility(View.VISIBLE);
            mTableViewModel = new SensusBjrTableViewModel(getContext(), list);
            // Create TableView Adapter
            adapter = new SensusBjrAdapter(getContext(), mTableViewModel);
            mTableView.setAdapter(adapter);
            mTableView.setRowHeaderWidth(Utils.convertDpToPx(getActivity(),100));
            mTableView.setTableViewListener(new ITableViewListener() {
                @Override
                public void onCellClicked(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

                }

                @Override
                public void onCellLongPressed(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

                }

                @Override
                public void onColumnHeaderClicked(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {
                    if (columnHeaderView != null && columnHeaderView instanceof ColumnHeaderViewHolder) {
                        // Create Long Press Popup
                        ColumnHeaderPopup popup = new ColumnHeaderPopup(
                                (ColumnHeaderViewHolder) columnHeaderView, mTableView);
                        // Show
                        popup.show();
                    }
                }

                @Override
                public void onColumnHeaderLongPressed(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {

                }

                @Override
                public void onRowHeaderClicked(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {
                    if (rowHeaderView != null && rowHeaderView instanceof SensusBjrRowHeaderViewHolder) {

                        String [] sid = ((SensusBjrRowHeaderViewHolder) rowHeaderView).getCellId().split("-");
                        selectedSensusBjr = origin.get(sid[1]);
                        startLongOperation(STATUS_LONG_OPERATION_SELECT_ITEM);

                    }

                }

                @Override
                public void onRowHeaderLongPressed(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {

                }
            });
            adapter.setAllItems(mTableViewModel.getColumnHeaderList(), mTableViewModel
                    .getRowHeaderList(), mTableViewModel.getCellList());


//            mTableFilter = new Filter(mTableView);
        }else{
            ltableview.setVisibility(View.GONE);
        }

        tvQcTph.setText(String.valueOf(origin.size()) + " Tph");
        Date d = new Date(System.currentTimeMillis());
        if(sdf.format(d).equals(sdf.format(dateSelected))){
            if(GlobalHelper.enableToNewTransaksi()){
                llNewData.setVisibility(View.VISIBLE);
            }else{
                llNewData.setVisibility(View.GONE);
            }
        }else{
            llNewData.setVisibility(View.GONE);
        }
    }

    public void startLongOperation(int status){
        new LongOperation().execute(String.valueOf(status));
    }

    private class LongOperation extends AsyncTask<String, Void, String> {
//        private AlertDialog alertDialogAllpoin ;
        String dataString;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (((BaseActivity) getActivity()).alertDialogBase != null) {
                ((BaseActivity) getActivity()).alertDialogBase.cancel();
            }
            ((BaseActivity) getActivity()).alertDialogBase = WidgetHelper.showWaitingDialog(getActivity(),getResources().getString(R.string.wait));
        }

        @Override
        protected String doInBackground(String... strings) {
            switch (Integer.parseInt(strings[0])){
                case STATUS_LONG_OPERATION_NONE:{
                    updateDataRv(Integer.parseInt(strings[0]));
                    break;
                }
                case STATUS_LONG_OPERATION_SEARCH:{
                    updateDataRv(Integer.parseInt(strings[0]));
                    break;
                }
                case STATUS_LONG_OPERATION_SELECT_ITEM:{
                    Gson gson = new Gson();
                    dataString = gson.toJson(selectedSensusBjr);
                    break;
                }
            }
            return strings[0];
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            switch (Integer.parseInt(result)){
                case STATUS_LONG_OPERATION_NONE:{
                    updateUIRV(Integer.parseInt(result));

                    if (((BaseActivity) getActivity()).alertDialogBase != null) {
                        ((BaseActivity) getActivity()).alertDialogBase.cancel();
                    }
                    break;
                }
                case STATUS_LONG_OPERATION_SEARCH:{
                    updateUIRV(Integer.parseInt(result));

                    if (((BaseActivity) getActivity()).alertDialogBase != null) {
                        ((BaseActivity) getActivity()).alertDialogBase.cancel();
                    }
                    break;
                }
                case STATUS_LONG_OPERATION_SELECT_ITEM:{
                    Intent intent = new Intent(getActivity(), QcSensusBjrActivity.class);
                    intent.putExtra("qcBJR",dataString);
                    startActivityForResult(intent,GlobalHelper.RESULT_QC_SENSUS_BJR);
                    if (((BaseActivity) getActivity()).alertDialogBase != null) {
                        ((BaseActivity) getActivity()).alertDialogBase.cancel();
                    }
                    break;
                }
                case STATUS_LONG_OPERATION_NEW_DATA:{
//                    Intent intent = new Intent(getActivity(), QRScan.class);
//                    startActivityForResult(intent,GlobalHelper.RESULT_SCAN_QR);
//                    if (((BaseActivity) getActivity()).alertDialogBase != null) {
//                        ((BaseActivity) getActivity()).alertDialogBase.cancel();
//                    }
                    break;
                }
            }

        }
    }
}
