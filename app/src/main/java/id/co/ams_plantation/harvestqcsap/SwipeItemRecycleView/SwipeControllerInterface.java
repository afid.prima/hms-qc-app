package id.co.ams_plantation.harvestqcsap.SwipeItemRecycleView;

public interface SwipeControllerInterface {
    void onItemMove(int fromPosition, int toPosition);
}
