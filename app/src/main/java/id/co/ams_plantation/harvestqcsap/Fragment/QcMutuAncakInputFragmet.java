package id.co.ams_plantation.harvestqcsap.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.util.ArraySet;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.esri.android.map.LocationDisplayManager;
import com.esri.android.map.event.OnStatusChangedListener;
import com.esri.core.geometry.GeometryEngine;
import com.esri.core.geometry.Point;
import com.esri.core.geometry.SpatialReference;
import com.esri.core.map.Graphic;
import com.esri.core.symbol.CompositeSymbol;
import com.esri.core.symbol.SimpleLineSymbol;
import com.esri.core.symbol.SimpleMarkerSymbol;
import com.esri.core.symbol.TextSymbol;
import com.evrencoskun.tableview.TableView;
import com.evrencoskun.tableview.listener.ITableViewListener;
import com.google.gson.Gson;
import com.mikepenz.iconics.utils.Utils;
import com.weiwangcn.betterspinner.library.BetterSpinner;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.ObjectRepository;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import id.co.ams_plantation.harvestqcsap.HarvestApp;
import id.co.ams_plantation.harvestqcsap.TableView.adapter.AncakPohonAdapter;
import id.co.ams_plantation.harvestqcsap.TableView.holder.AncakPohonRowHeaderViewHolder;
import id.co.ams_plantation.harvestqcsap.TableView.holder.ColumnHeaderViewHolder;
import id.co.ams_plantation.harvestqcsap.TableView.popup.ColumnHeaderPopup;
import id.co.ams_plantation.harvestqcsap.TableView.view_model.AncakPohonTableViewModel;
import id.co.ams_plantation.harvestqcsap.adapter.SelectTphAdapter;
import id.co.ams_plantation.harvestqcsap.model.Answer;
import id.co.ams_plantation.harvestqcsap.model.DataNitrit;
import id.co.ams_plantation.harvestqcsap.model.NearestBlock;
import id.co.ams_plantation.harvestqcsap.model.QcAncak;
import id.co.ams_plantation.harvestqcsap.model.QcAncakPohon;
import id.co.ams_plantation.harvestqcsap.model.QuestionAnswer;
import id.co.ams_plantation.harvestqcsap.model.TPH;
import id.co.ams_plantation.harvestqcsap.ui.BaseActivity;
import id.co.ams_plantation.harvestqcsap.ui.QcMutuAncakActivity;
import id.co.ams_plantation.harvestqcsap.util.CompleteTextViewHelper;
import id.co.ams_plantation.harvestqcsap.util.GlobalHelper;
import id.co.ams_plantation.harvestqcsap.util.MutuAncakHelper;
import id.co.ams_plantation.harvestqcsap.util.TphHelper;
import id.co.ams_plantation.harvestqcsap.util.WidgetHelper;
import id.co.ams_plantation.harvestqcsap.R;
import ng.max.slideview.SlideView;

/**
 * Created by user on 12/4/2018.
 */

public class QcMutuAncakInputFragmet extends Fragment {

    public static final int STATUS_LONG_OPERATION_NONE = 0;
    public static final int STATUS_LONG_OPERATION_SEARCH = 1;
    public static final int STATUS_LONG_OPERATION_SELECT_ITEM = 2;
    public static final int STATUS_LONG_OPERATION_NEW_DATA = 3;
    public static final int STATUS_LONG_OPERATION_SETUP_DATA_TPH = 4;
    public static final int STATUS_LONG_OPERATION_SAVE = 5;
    public static final int STATUS_LONG_OPERATION_DELETED = 6;
    public static final int STATUS_LONG_OPERATION_SETUP_POINT = 7;

    private static String TAG = QcMutuAncakInputFragmet.class.getSimpleName();
    private String blockName;
    HashMap<String, QcAncakPohon> origin;
    AncakPohonAdapter adapter;
    TableView mTableView;
//    Filter mTableFilter;
    AncakPohonTableViewModel mTableViewModel;
    CompleteTextViewHelper etSearch;
    Button btnSearch;
    TextView tvCreateTime;
    CompleteTextViewHelper etTph;
    CompleteTextViewHelper etBlock;
//    AppCompatEditText etAncak;
    AppCompatEditText etBrondolan;
    BetterSpinner etPB;
//    ChipView etBaris;
    RelativeLayout ltableview;
    TextView tvTotalPokok;
    TextView tvPokokPanen;
    TextView tvTotalJanjang;
    TextView tvTotalBuahTinggal;
    TextView tvTotalBrondolan;
    TextView tvTotalBuahTinggalSegar;
    TextView tvTotalBuahTinggalBusuk;
    TextView tvTotalBrondolanSegarTinggal;
    TextView tvTotalBrondolanLamaTinggal;
    TextView tvTampilDetail;
    Button btnClsoe;

    LinearLayout llTotalDetail;
    LinearLayout llTotalDetail2;
    LinearLayout lPB;
    LinearLayout llNewData;

    LocationDisplayManager locationDisplayManager;

    SelectTphAdapter adapterTph;

    Set<String> listSearch;
    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
    SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

    BaseActivity baseActivity;
    QcMutuAncakActivity qcMutuAncakActivity;
    MutuAncakHelper mutuAncakHelper;
    ArrayList<TPH> allTph;
    TphHelper tphHelper;
    TPH tphTerdekat;
    File kml;

    public double latitude;
    public double longitude;

    int totalPokokPanen;
    int TotalJanjang;
    int TotalBuahTinggal;
    int TotalBrondolan;
    int totalBuahTinggalSegar;
    int totalBuahTinggalBusuk;
    int totalBrondolanSegarTinggal;
    int totalBrondolanLamaTinggal;
    QcAncak selectedQcAncak;
    QuestionAnswer questionAnswerPenyebabBrondolan;
    QuestionAnswer questionAnswerKondisiAncak;
    ArrayList<QcAncakPohon> qcPohons;
    private Activity activity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
            activity = (Activity) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activity = null;
    }

    public static QcMutuAncakInputFragmet getInstance(){
        QcMutuAncakInputFragmet fragment = new QcMutuAncakInputFragmet();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.qc_ancak_input_layout,null,false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        etSearch = view.findViewById(R.id.etSearch);
        btnSearch = view.findViewById(R.id.btnSearch);
        tvCreateTime = view.findViewById(R.id.tvCreateTime);
        etTph = view.findViewById(R.id.etTph);
        etBlock = view.findViewById(R.id.etBlock);
//        etAncak = view.findViewById(R.id.etAncak);
        etBrondolan = view.findViewById(R.id.etBrondolan);
        etPB = view.findViewById(R.id.etPB);
        lPB = view.findViewById(R.id.lPB);
        llTotalDetail = view.findViewById(R.id.llTotalDetail);
        llTotalDetail2 = view.findViewById(R.id.llTotalDetail2);
        tvTampilDetail = view.findViewById(R.id.tvTampilDetail);
//        etBaris = view.findViewById(R.id.etBaris);
        ltableview = view.findViewById(R.id.ltableview);
        mTableView = view.findViewById(R.id.tableview);
        llNewData = view.findViewById(R.id.llNewData);
        tvTotalPokok = view.findViewById(R.id.tvTotalPokok);
        tvPokokPanen = view.findViewById(R.id.tvPokokPanen);
        tvTotalJanjang = view.findViewById(R.id.tvTotalJanjang);
        tvTotalBuahTinggal = view.findViewById(R.id.tvTotalBuahTinggal);
        tvTotalBrondolan = view.findViewById(R.id.tvTotalBrondolan);
        tvTotalBuahTinggalSegar = view.findViewById(R.id.tvTotalBuahTinggalSegar);
        tvTotalBuahTinggalBusuk = view.findViewById(R.id.tvTotalBuahTinggalBusuk);
        tvTotalBrondolanSegarTinggal = view.findViewById(R.id.tvTotalBrondolanSegarTinggal);
        tvTotalBrondolanLamaTinggal = view.findViewById(R.id.tvTotalBrondolanLamaTinggal);
        btnClsoe = view.findViewById(R.id.btnClsoe);

        baseActivity = ((BaseActivity) activity);
        mutuAncakHelper = new MutuAncakHelper((FragmentActivity) activity);
        if(activity instanceof  QcMutuAncakActivity){
            qcMutuAncakActivity = (QcMutuAncakActivity) activity;
            if(qcMutuAncakActivity.seletedQcAncak != null) {
                selectedQcAncak = qcMutuAncakActivity.seletedQcAncak;
            }else{
                selectedQcAncak = new QcAncak();
                selectedQcAncak.setStatus(QcAncak.NotSave);
                selectedQcAncak.setStartTime(System.currentTimeMillis());
            }
        }

        llTotalDetail2.setVisibility(View.GONE);
        tvTampilDetail.setText("Hide");
        origin = new HashMap<>();
        allTph = new ArrayList<>();
        tphHelper = new TphHelper((FragmentActivity) activity);
        llNewData.setVisibility(selectedQcAncak.getStatus() == QcAncak.NotSave ? View.VISIBLE : selectedQcAncak.getStatus() == QcAncak.Save ? View.VISIBLE :View.GONE);
        kml = new File(GlobalHelper.decryptFiles(GlobalHelper.TYPE_KMLBLOCK));

        if(qcMutuAncakActivity.questionAnswers != null){
            questionAnswerPenyebabBrondolan = qcMutuAncakActivity.questionAnswers.get("QAA01");
            List<String> strings = new ArrayList<>();
            for(int i = 0 ; i < questionAnswerPenyebabBrondolan.getAnswer().size(); i++){
                strings.add(questionAnswerPenyebabBrondolan.getAnswer().get(i).getQcAnsware());
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity,
                    R.layout.simple_dropdown_item_1line, strings);
            etPB.setAdapter(adapter);
            etPB.setText(strings.get(0));

            questionAnswerKondisiAncak = qcMutuAncakActivity.questionAnswers.get("QAA05");
        }

        if(qcMutuAncakActivity.currentlocation != null) {
            latitude = qcMutuAncakActivity.currentlocation.getLatitude();
            longitude = qcMutuAncakActivity.currentlocation.getLongitude();
        }

        if(qcMutuAncakActivity.seletedQcAncak != null) {
            if (sdf.format(qcMutuAncakActivity.seletedQcAncak.getStartTime()).equals(sdf.format(System.currentTimeMillis()))) {
                llNewData.setVisibility(selectedQcAncak.getStatus() == QcAncak.NotSave ? View.VISIBLE : selectedQcAncak.getStatus() == QcAncak.Save ? View.VISIBLE :View.GONE);
            } else {
                llNewData.setVisibility(View.GONE);
            }
        }else{
            llNewData.setVisibility(View.VISIBLE);
        }
        setUpValueDeklarasiQcAncak();

        baseActivity.zoomToLocation.setVisibility(View.VISIBLE);

        baseActivity.mapView.setOnStatusChangedListener(new OnStatusChangedListener() {
            @Override
            public void onStatusChanged(Object o, STATUS status) {
                if(o==baseActivity.mapView && status==STATUS.INITIALIZED){
                    qcMutuAncakActivity.spatialReference = baseActivity.mapView.getSpatialReference();
                    qcMutuAncakActivity.isMapLoaded = true;
                    locationListenerSetup();
                    new QcMutuAncakInputFragmet.LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_SETUP_POINT));
                }
            }
        });

        baseActivity.zoomToLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new QcMutuAncakInputFragmet.LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_SETUP_POINT));
            }
        });

        etBlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateNearestBlock(false);
                etBlock.showDropDown();
            }
        });

        etBlock.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                etTph.setText("");
                setDropDownTph();
            }
        });

        etBrondolan.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                showhideAlasanBrondolan();
            }
        });

        etPB.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setPenyebabBrondolan();
            }
        });

        etTph.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                adapterTph = (SelectTphAdapter) parent.getAdapter();
                setTph(adapterTph.getItemAt(position));
            }
        });

        llNewData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                simpanBaris();
                if (qcMutuAncakActivity.seletedQcAncak != null) {
                    if(qcMutuAncakActivity.seletedQcAncak.getTph() != null) {
                        int pokok = TPH.BANYAK_POKOK;
                        if(qcMutuAncakActivity.seletedQcAncak.getTph().getJumlahPokok() > 0){
                            pokok = qcMutuAncakActivity.seletedQcAncak.getTph().getJumlahPokok();
                        }
                        if(origin.size() < pokok) {
                            startLongOperation(STATUS_LONG_OPERATION_NEW_DATA);
                        }else{
                            Toast.makeText(HarvestApp.getContext(),"Jumlah pokok di qc sudah sebanyak jumlah pokok pada tph",Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(HarvestApp.getContext(),"Mohon Pilih Tph Atau Ancak Unutk Di Qc",Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(HarvestApp.getContext(),"Mohon Pilih Tph Atau Ancak Unutk Di Qc",Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etSearch.getText().toString().isEmpty()){
                    new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_NONE));
                }else{
                    new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_SEARCH));
                }
            }
        });

        btnClsoe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!GlobalHelper.isDoubleClick()){
                    closeMutuAncak();
                }
            }
        });

        llTotalDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(llTotalDetail2.getVisibility() == View.VISIBLE){
                    llTotalDetail2.setVisibility(View.GONE);
                    tvTampilDetail.setText("Hide");
                }else{
                    llTotalDetail2.setVisibility(View.VISIBLE);
                    tvTampilDetail.setText("Show");
                }
            }
        });

        startLongOperation(STATUS_LONG_OPERATION_NONE);
    }

    public void closeMutuAncak(){
        if(selectedQcAncak.getTph() == null){
            Toast.makeText(HarvestApp.getContext(),"Mohon Pilih TPH ",Toast.LENGTH_SHORT).show();
            new QcMutuAncakInputFragmet.LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_SETUP_POINT));
            return;
        }
        double distance = GlobalHelper.distance(baseActivity.currentlocation.getLatitude(),selectedQcAncak.getTph().getLatitude(),baseActivity.currentlocation.getLongitude(),selectedQcAncak.getTph().getLongitude());
        if(distance <= 1000) {
            konfirmasiAncak();
        }else{
            Toast.makeText(HarvestApp.getContext(),"Anda Terlalu Jauh Dengan TPH "+ selectedQcAncak.getTph().getNamaTph(),Toast.LENGTH_SHORT).show();
        }
    }

    private void showhideAlasanBrondolan(){
        if(!etBrondolan.getText().toString().isEmpty()) {
            if (Integer.parseInt(etBrondolan.getText().toString()) > 0) {
                lPB.setVisibility(View.VISIBLE);
            } else {
                lPB.setVisibility(View.GONE);
            }
        }else{
            lPB.setVisibility(View.GONE);
        }
        setPenyebabBrondolan();
    }

    private void updateNearestBlock(boolean updateAdapter){
        if (baseActivity.currentLocationOK()) {
            latitude = baseActivity.currentlocation.getLatitude();
            longitude = baseActivity.currentlocation.getLongitude();

            Set<String> allBlock = GlobalHelper.getNearBlock(kml,latitude,longitude);
            Set<String> bloks = new ArraySet<>();
            for(String est : allBlock){
                try {
                    String [] split = est.split(";");
                    bloks.add(split[2]);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            if(etBlock.getAdapter() == null || updateAdapter) {
                ArrayAdapter<String> adapterBlock = new ArrayAdapter<String>
                        (activity, android.R.layout.select_dialog_item, bloks.toArray(new String[bloks.size()]));

                etBlock.setThreshold(1);
                etBlock.setAdapter(adapterBlock);
                try {
                    String[] estAfdBlock = GlobalHelper.getAndAssignBlock(kml, latitude, longitude).split(";");
                    etBlock.setText(estAfdBlock[2]);
//                    setDropdownTph();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            WidgetHelper.warningFindGps(qcMutuAncakActivity,qcMutuAncakActivity.myCoordinatorLayout);
        }
    }



    public void locationListenerSetup(){

        if(baseActivity.mapView!=null && baseActivity.mapView.isLoaded()){

            locationDisplayManager = baseActivity.mapView.getLocationDisplayManager();
            locationDisplayManager.setAllowNetworkLocation(false);
            locationDisplayManager.setAccuracyCircleOn(true);
            locationDisplayManager.setLocationListener(new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    baseActivity.currentlocation = location;
//                    zoomToLocation(location.getLatitude(), location.getLongitude(), spatialReference);
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {

                }

                @Override
                public void onProviderEnabled(String s) {

                }

                @Override
                public void onProviderDisabled(String s) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setMessage("GPS anda tidak hidup! Apakah anda ingin menghidupkan GPS?")
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();

                                }
                            });
                    final AlertDialog alert = builder.create();
                    alert.show();

                }


            });
            locationDisplayManager.start();
        }
    }

    public void setupPoint(){
        if(qcMutuAncakActivity.currentlocation != null) {
            latitude = qcMutuAncakActivity.currentlocation.getLatitude();
            longitude = qcMutuAncakActivity.currentlocation.getLongitude();
        }

//        if(selectedTransaksiPanen != null) {
//            latitude = selectedTransaksiPanen.getLatitude();
//            longitude = selectedTransaksiPanen.getLongitude();
//        }

//        Set<String> block = new ArraySet<>();
        Gson gson = new Gson();
        String dataString = gson.toJson(tphHelper.getNearBlock(latitude,longitude));
        NearestBlock nearestBlock = gson.fromJson(dataString,NearestBlock.class);
        ArrayList<TPH> tphArrayList = tphHelper.setDataTph(GlobalHelper.getEstate().getEstCode(),nearestBlock.getBlocks().toArray(new String[nearestBlock.getBlocks().size()]));
        for(TPH tph: tphArrayList){
            addGraphicTph(tph);
        }
//        if(qcMutuBuahActivity.currentlocation != null) {

        if (selectedQcAncak != null) {
            if (selectedQcAncak.getStatus() == QcAncak.Save) {
                addGrapTemp(selectedQcAncak.getTph().getLatitude(), selectedQcAncak.getTph().getLatitude());
            }
        }

        zoomToLocation(latitude, longitude, qcMutuAncakActivity.spatialReference);
//        }
        if(nearestBlock.getBlock() != null){
            blockName = nearestBlock.getBlock();
            allTph = tphHelper.setDataTph(GlobalHelper.getEstate().getEstCode(), blockName);
            tphTerdekat = tphHelper.cariTphTerdekat(allTph);
        }
    }

    public void zoomToLocation(double lat, double lon, SpatialReference spatialReference) {
        Log.d(TAG, "zoomToLocation: "+lat +":"+lon);
        Point mapPoint = GeometryEngine.project(lon,lat,spatialReference);
        try {
            baseActivity.mapView.centerAt(mapPoint,true);
            baseActivity.mapView.zoomTo(mapPoint,15000f);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void addGraphicTph(TPH tph){

        Point fromPoint = new Point(tph.getLongitude(), tph.getLatitude());
        Point toPoint = (Point) GeometryEngine.project(fromPoint, SpatialReference.create(SpatialReference.WKID_WGS84), qcMutuAncakActivity.spatialReference);

        CompositeSymbol cms = new CompositeSymbol();
        int color = tph.getResurvey() == 1 ? TPH.COLOR_TPH_RESURVEY : TPH.COLOR_TPH;
        SimpleMarkerSymbol sms = new SimpleMarkerSymbol(color, Utils.convertDpToPx(activity, GlobalHelper.MARKER_SIZE_TRANSKASI), SimpleMarkerSymbol.STYLE.CIRCLE);
        SimpleLineSymbol sls = new SimpleLineSymbol(TPH.COLOR_BELUMUPLOAD, GlobalHelper.GARISTEPI_SIZE, SimpleLineSymbol.STYLE.SOLID);
        if(tph.getStatus() == TPH.STATUS_TPH_MASTER || tph.getStatus() == TPH.STATUS_TPH_UPLOAD){
            sls = new SimpleLineSymbol(TPH.COLOR_UPLOAD, GlobalHelper.GARISTEPI_SIZE, SimpleLineSymbol.STYLE.SOLID);
        }
        sms.setOutline(sls);
        cms.add(sms);

        Graphic pointGraphic = new Graphic(toPoint, cms);
        qcMutuAncakActivity.graphicsLayerTPH.addGraphic(pointGraphic);

        TextSymbol.HorizontalAlignment horizontalAlignment = TextSymbol.HorizontalAlignment.CENTER;
        TextSymbol.VerticalAlignment verticalAlignment = TextSymbol.VerticalAlignment.BOTTOM;

        int Tcolor = Color.parseColor("#000000");
        TextSymbol txtSymbol = new TextSymbol(GlobalHelper.TEXT_SIZE_TRANSKASI,tph.getNamaTph(),Tcolor) ;
        txtSymbol.setHorizontalAlignment(horizontalAlignment);
        txtSymbol.setVerticalAlignment(verticalAlignment);

        pointGraphic = new Graphic(toPoint,txtSymbol);
        qcMutuAncakActivity.graphicsLayerText.addGraphic(pointGraphic);
        Log.d(TAG, "addGraphicTph: "+ tph.getNamaTph());
    }

    public void addGrapTemp(Double lat, Double lon){
        if(baseActivity.mapView!=null && baseActivity.mapView.isLoaded()){
            Point fromPoint = new Point(lon, lat);
            Point toPoint = (Point) GeometryEngine.project(fromPoint, SpatialReference.create(SpatialReference.WKID_WGS84), qcMutuAncakActivity.spatialReference);

            CompositeSymbol cms = new CompositeSymbol();
            SimpleMarkerSymbol sms = new SimpleMarkerSymbol(Color.GREEN, Utils.convertDpToPx(activity, GlobalHelper.MARKER_SIZE), SimpleMarkerSymbol.STYLE.X);
            SimpleLineSymbol sls = new SimpleLineSymbol(Color.GREEN, GlobalHelper.GARISTEPI_SIZE, SimpleLineSymbol.STYLE.SOLID);
            sms.setOutline(sls);
            cms.add(sms);
            Graphic pointGraphic = new Graphic(toPoint, cms);
            qcMutuAncakActivity.graphicsLayer.addGraphic(pointGraphic);
            Log.d(TAG, "addGrapTemp: "+lat + ":"+ lon);
        }
    }

    public void removeGraphicPokok(){
        qcMutuAncakActivity.graphicsLayerPokok.removeAll();
        qcMutuAncakActivity.graphicsLayerPokokText.removeAll();
    }

    public void konfirmasiAncak(){
        if(qcMutuAncakActivity.seletedQcAncak == null && origin.size() == 0) {
            qcMutuAncakActivity.closeActivity();
            return;
        }
        if(questionAnswerKondisiAncak != null){
            if(questionAnswerKondisiAncak.getAnswer().size() > 0){
                View dialogView = LayoutInflater.from(activity).inflate(R.layout.konfirmasi_ancak, null);
                BetterSpinner etKA = (BetterSpinner) dialogView.findViewById(R.id.etKA);
                SlideView btnCancelDialog = (SlideView) dialogView.findViewById(R.id.btnCancel);
                SlideView btnOkDialog = (SlideView) dialogView.findViewById(R.id.btnOK);

                final AlertDialog dialog = new AlertDialog.Builder(activity, R.style.MyAlertDialogStyle)
                        .setView(dialogView)
                        .setCancelable(false)
                        .create();

                List<String> strings = new ArrayList<>();
                Answer answer = null;
                for(int i = 0 ; i < questionAnswerKondisiAncak.getAnswer().size(); i++){
                    Answer answer1 = questionAnswerKondisiAncak.getAnswer().get(i);
                    strings.add(answer1.getQcAnsware());

                    if(selectedQcAncak.getKondisiAncak() != null){
                        if(selectedQcAncak.getKondisiAncak().toLowerCase().equals(answer1.getIdQcAnswer())){
                            answer = answer1;
                        }
                    }
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity,
                        R.layout.simple_dropdown_item_1line, strings);
                etKA.setAdapter(adapter);

                if(answer == null) {
                    etKA.setText(strings.get(0));
                }else{
                    etKA.setText(answer.getQcAnsware());
                }

                btnOkDialog.setText("Sudah");
                btnCancelDialog.setText("Belum");

                btnCancelDialog.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
                    @Override
                    public void onSlideComplete(SlideView slideView) {
                        dialog.dismiss();
                    }
                });
                btnOkDialog.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
                    @Override
                    public void onSlideComplete(SlideView slideView) {
                        for(int i = 0 ; i < questionAnswerKondisiAncak.getAnswer().size(); i++){
                            Answer answer1 = questionAnswerKondisiAncak.getAnswer().get(i);
                            if(answer1.getQcAnsware().equals(etKA.getText().toString())){
                                selectedQcAncak.setKondisiAncak(answer1.getIdQcAnswer());
                                selectedQcAncak.setKondisiAncakText(answer1.getQcAnsware());
                                break;
                            }
                        }

                        dialog.dismiss();
                        closeQcAncak();
                    }
                });
                dialog.show();
            }else{
                closeQcAncak();
            }
        }else{
            closeQcAncak();
        }
    }

    public void closeQcAncak(){
        //untuk data angkut baru saja yang bisa masuk ke sini
        //jika data transaksi angkut yang lama maka bisa share print dan tap nfc saja
        if(qcMutuAncakActivity.seletedQcAncak == null && origin.size() == 0){
            qcMutuAncakActivity.closeActivity();
        }else if(qcMutuAncakActivity.seletedQcAncak.getStatus() == QcAncak.NotSave || qcMutuAncakActivity.seletedQcAncak.getStatus() == QcAncak.Save) {
            if(llNewData.getVisibility() == View.VISIBLE) {
                if (qcMutuAncakActivity.currentLocationOK()) {
//                // unutk proses angkut radius tidak di ikutkan karena tidak memungkinkan tap nfc pada tahap ini
//                if (selectedLangsiran != null) {
//                    double distance = GlobalHelper.distance(selectedLangsiran.getLatitude(),baseActivity.currentlocation.getLatitude(),
//                            selectedLangsiran.getLongitude(),baseActivity.currentlocation.getLongitude());
//                    if(distance > GlobalHelper.RADIUS){
//                        Toast.makeText(HarvestApp.getContext(),activity.getResources().getString(R.string.radius_to_long_with_langsiran),Toast.LENGTH_SHORT).show();
//                        return;
//                    }
//                }
                } else {
                    qcMutuAncakActivity.searchingGPS = Snackbar.make(qcMutuAncakActivity.myCoordinatorLayout, getResources().getString(R.string.search_gps), Snackbar.LENGTH_INDEFINITE);
                    qcMutuAncakActivity.searchingGPS.show();
                    return;
                }

                View dialogView = LayoutInflater.from(activity).inflate(R.layout.yes_no, null);
                TextView tvDialogKet = (TextView) dialogView.findViewById(R.id.ket1);
                SlideView btnCancelDialog = (SlideView) dialogView.findViewById(R.id.btnCancel);
                SlideView btnOkDialog = (SlideView) dialogView.findViewById(R.id.btnOK);

                final AlertDialog dialog = new AlertDialog.Builder(activity, R.style.MyAlertDialogStyle)
                        .setView(dialogView)
                        .setCancelable(false)
                        .create();

                final SpannableStringBuilder sb = new SpannableStringBuilder(etTph.getText().toString());
                final ForegroundColorSpan fcs = new ForegroundColorSpan(activity.getResources().getColor(R.color.Red));
                final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
                sb.setSpan(fcs, 0, etTph.getText().toString().length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                sb.setSpan(bss, 0, etTph.getText().toString().length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

                String text = "Apakah Anda Sudah Selesai Qc Ancak Untuk Tph " + sb;
                if(origin.size() == 0) {
                    text += "\nQc di Ancak "+ sb + " Akan Dihapus";
                }

                tvDialogKet.setText(text);
                btnOkDialog.setText("Sudah");
                btnCancelDialog.setText("Belum");

                btnCancelDialog.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
                    @Override
                    public void onSlideComplete(SlideView slideView) {
                        dialog.dismiss();
                    }
                });
                btnOkDialog.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
                    @Override
                    public void onSlideComplete(SlideView slideView) {
                        dialog.dismiss();
                        if(origin.size() > 0) {
                            new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_SAVE));
                        }else{
                            new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_DELETED));
                        }
                    }
                });
                dialog.show();
            }else{
                qcMutuAncakActivity.closeActivity();
            }
        }else{
            qcMutuAncakActivity.closeActivity();
        }
    }

    public void simpanBaris(){
        ArrayList<Integer> basirs = new ArrayList<>();
        for(int i = 0; i <= GlobalHelper.MAX_BARIS_TPH;i++){
            basirs.add(i);
        }
        if(selectedQcAncak.getTph() != null) {
            selectedQcAncak.getTph().setBaris(basirs);
        }
        selectedQcAncak.setBrondolanDiTph(Integer.parseInt(etBrondolan.getText().toString().isEmpty() ? "0" : etBrondolan.getText().toString()));
        qcMutuAncakActivity.mutuAncakHelper.createNewMutuAncak(selectedQcAncak);

        qcMutuAncakActivity.seletedQcAncak = selectedQcAncak;
        qcMutuAncakActivity.seletedQcAncak.setQcAncakPohons(qcPohons);
    }

    private void setUpValueDeklarasiQcAncak(){
        if (selectedQcAncak.getTph() != null) {
            setTph(selectedQcAncak.getTph());
            etBrondolan.setText(String.valueOf(selectedQcAncak.getBrondolanDiTph()));
        } else {
            String[] estAfdBlock = GlobalHelper.getAndAssignBlock(kml, latitude, longitude).split(";");
            try {
                etBlock.setText(estAfdBlock[2]);
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        tvCreateTime.setText(sdf.format(selectedQcAncak.getStartTime()));
        showhideAlasanBrondolan();

        if(selectedQcAncak.getPenyebabBrondolan() != null && lPB.getVisibility() == View.VISIBLE){
           for(int i = 0 ; i < questionAnswerPenyebabBrondolan.getAnswer().size();i++){
               Answer answer = questionAnswerPenyebabBrondolan.getAnswer().get(i);
               if(selectedQcAncak.getPenyebabBrondolan().toLowerCase().equals(answer.getIdQcAnswer().toLowerCase())){
                   etPB.setText(answer.getQcAnsware());
               }
           }
        }
    }

    private void setDropDownTph(){
        new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_SETUP_DATA_TPH));
    }

    private void updateDataRv(int status){
        origin = new HashMap<>();
        qcPohons = new ArrayList<>();
        listSearch = new android.support.v4.util.ArraySet<>();

        totalPokokPanen = 0;
        TotalJanjang = 0;
        TotalBuahTinggal = 0;
        TotalBrondolan = 0;
        totalBuahTinggalSegar = 0;
        totalBuahTinggalBusuk = 0;
        totalBrondolanSegarTinggal = 0;
        totalBrondolanLamaTinggal = 0;
        removeGraphicPokok();

        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_ANCAK_POHON);
        Log.d("lokasi pohon",db.getContext().getFilePath());
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = Iterable.iterator(); iterator.hasNext();) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            QcAncakPohon qcAncakPohon = gson.fromJson(dataNitrit.getValueDataNitrit(),QcAncakPohon.class);
            qcPohons.add(qcAncakPohon);
            qcMutuAncakActivity.addGraphicPokok(qcAncakPohon);
            if (status == STATUS_LONG_OPERATION_SEARCH) {
                if (timeFormat.format(qcAncakPohon.getWaktuQcAncakPohon()).toLowerCase().contains(etSearch.getText().toString().toLowerCase()) ||
                        qcAncakPohon.getIdPohon().toLowerCase().contains(etSearch.getText().toString().toLowerCase()) ||
                        String.valueOf(qcAncakPohon.getNoBaris()).contains(etSearch.getText().toString().toLowerCase()) ||
                        String.valueOf(qcAncakPohon.getJanjangPanen()).contains(etSearch.getText().toString().toLowerCase()) ||
                        String.valueOf(qcAncakPohon.getBuahTinggal()).contains(etSearch.getText().toString().toLowerCase()) ||
                        String.valueOf(qcAncakPohon.getBrondolanTinggal()).contains(etSearch.getText().toString().toLowerCase())) {
                    addRowTable(qcAncakPohon);
                }
            } else if (status == STATUS_LONG_OPERATION_NONE) {
                addRowTable(qcAncakPohon);
            }
            listSearch.add(timeFormat.format(qcAncakPohon.getWaktuQcAncakPohon()));
            listSearch.add(qcAncakPohon.getIdPohon());
            listSearch.add(String.valueOf(qcAncakPohon.getNoBaris()));
            listSearch.add(String.valueOf(qcAncakPohon.getJanjangPanen()));
            listSearch.add(String.valueOf(qcAncakPohon.getBrondolanTinggal()));

        }
        db.close();
    }

    private void addRowTable(QcAncakPohon qcAncakPohon){
        Gson gson = new Gson();
        Log.d("detail pohon",gson.toJson(qcAncakPohon));
        origin.put(qcAncakPohon.getIdQcAncakPohon(), qcAncakPohon);
        TotalJanjang += qcAncakPohon.getJanjangPanen();
        TotalBuahTinggal += qcAncakPohon.getBuahTinggal();
        TotalBrondolan += qcAncakPohon.getBrondolanTinggal();
        totalBuahTinggalSegar += qcAncakPohon.getBuahTinggalSegar();
        totalBuahTinggalBusuk += qcAncakPohon.getBuahTinggalBusuk();
        totalBrondolanSegarTinggal += qcAncakPohon.getBrondolanSegarTinggal();
        totalBrondolanLamaTinggal += qcAncakPohon.getBrondolanLamaTinggal();
        if(qcAncakPohon.getJanjangPanen() > 0){
            totalPokokPanen++;
        }
    }

    private void updateUIRV(int status){
        if(status == STATUS_LONG_OPERATION_NONE){
            etSearch.setText("");
        }

        List<String> lSearch = new ArrayList<>();
        if(listSearch.size() > 0){
            lSearch.addAll(listSearch);
        }
        ArrayAdapter<String> adapterSearch = new ArrayAdapter<String>(activity,
                android.R.layout.simple_dropdown_item_1line, lSearch);
        etSearch.setAdapter(adapterSearch);
        etSearch.setThreshold(1);
        adapterSearch.notifyDataSetChanged();

        if(origin.size() > 0 ) {
            ArrayList<QcAncakPohon> list = new ArrayList<>(origin.values());

            Collections.sort(list, new Comparator<QcAncakPohon>() {
                @Override
                public int compare(QcAncakPohon o1, QcAncakPohon o2) {
                    return Long.compare(o2.getWaktuQcAncakPohon(), o1.getWaktuQcAncakPohon());
                }
            });

            ltableview.setVisibility(View.VISIBLE);
            mTableViewModel = new AncakPohonTableViewModel(getContext(), list);
            // Create TableView Adapter
            adapter = new AncakPohonAdapter(getContext(), mTableViewModel);
            mTableView.setAdapter(adapter);
            mTableView.setRowHeaderWidth(Utils.convertDpToPx(activity,100));
            mTableView.setTableViewListener(new ITableViewListener() {
                @Override
                public void onCellClicked(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

                }

                @Override
                public void onCellLongPressed(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

                }

                @Override
                public void onColumnHeaderClicked(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {
                    if (columnHeaderView != null && columnHeaderView instanceof ColumnHeaderViewHolder) {
                        // Create Long Press Popup
                        ColumnHeaderPopup popup = new ColumnHeaderPopup(
                                (ColumnHeaderViewHolder) columnHeaderView, mTableView);
                        // Show
                        popup.show();
                    }
                }

                @Override
                public void onColumnHeaderLongPressed(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {

                }

                @Override
                public void onRowHeaderClicked(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {
                    if (rowHeaderView != null && rowHeaderView instanceof AncakPohonRowHeaderViewHolder) {
                        simpanBaris();
                        String [] sid = ((AncakPohonRowHeaderViewHolder) rowHeaderView).getCellId().split("-");
                        qcMutuAncakActivity.seletedQcAncakPohon = origin.get(sid[1]);
                        startLongOperation(STATUS_LONG_OPERATION_SELECT_ITEM);

                    }

                }

                @Override
                public void onRowHeaderLongPressed(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {

                }
            });
            adapter.setAllItems(mTableViewModel.getColumnHeaderList(), mTableViewModel
                    .getRowHeaderList(), mTableViewModel.getCellList());

            tvPokokPanen.setText(String.valueOf(totalPokokPanen));
            tvTotalPokok.setText(String.valueOf(origin.size()));
            tvTotalJanjang.setText(String.valueOf(TotalJanjang));
            tvTotalBuahTinggal.setText(String.valueOf(TotalBuahTinggal));
            tvTotalBrondolan.setText(String.valueOf(TotalBrondolan));
            tvTotalBuahTinggalSegar.setText(String.valueOf(totalBuahTinggalSegar));
            tvTotalBuahTinggalBusuk.setText(String.valueOf(totalBuahTinggalBusuk));
            tvTotalBrondolanSegarTinggal.setText(String.valueOf(totalBrondolanSegarTinggal));
            tvTotalBrondolanLamaTinggal.setText(String.valueOf(totalBrondolanLamaTinggal));
        }else{
            ltableview.setVisibility(View.GONE);
        }

    }

    private void setPenyebabBrondolan(){
        if(lPB.getVisibility() == View.VISIBLE){
            for(int i = 0 ; i < questionAnswerPenyebabBrondolan.getAnswer().size(); i++) {
                Answer answer = questionAnswerPenyebabBrondolan.getAnswer().get(i);
                if (etPB.getText().toString().toLowerCase().equals(answer.getQcAnsware().toLowerCase())){
                    selectedQcAncak.setPenyebabBrondolan(answer.getIdQcAnswer());
                    break;
                }
            }
        }else{
            selectedQcAncak.setPenyebabBrondolan("");
        }
    }

    private void setTph(TPH tph) {
        if (tph == null) {
            etTph.setText("");
//            etAncak.setText("");
        } else {
            double distance = GlobalHelper.distance(latitude,tph.getLatitude(),longitude,tph.getLongitude());
            if(selectedQcAncak.getStatus() == QcAncak.Save){
                distance = GlobalHelper.distance(selectedQcAncak.getLatitude(),tph.getLatitude(),selectedQcAncak.getLongitude(),tph.getLongitude());
                zoomToLocation(selectedQcAncak.getTph().getLatitude(),selectedQcAncak.getTph().getLongitude(),qcMutuAncakActivity.spatialReference);
            }
            if(distance <= GlobalHelper.RADIUS) {
                etTph.setText(tph.getNamaTph());
                etBlock.setText(tph.getBlock());
                GlobalHelper.showKeyboard((FragmentActivity) activity);
                selectedQcAncak.setTph(tph);
                mutuAncakHelper.createNewMutuAncak(selectedQcAncak);
            }else if(tphTerdekat != null && selectedQcAncak.getStatus() == QcAncak.NotSave){
                if((tph.getNoTph().equals(tphTerdekat.getNoTph()))){
                    etTph.setText(tph.getNamaTph());
                    etBlock.setText(tph.getBlock());
                    GlobalHelper.showKeyboard((FragmentActivity) activity);
                    selectedQcAncak.setTph(tph);
                    mutuAncakHelper.createNewMutuAncak(selectedQcAncak);
                }else{
                    etTph.setText(tphTerdekat.getNamaTph());
                    etBlock.setText(tphTerdekat.getBlock());
                    GlobalHelper.showKeyboard((FragmentActivity) activity);
                    selectedQcAncak.setTph(tphTerdekat);
                    mutuAncakHelper.createNewMutuAncak(selectedQcAncak);
                }
            }else if (selectedQcAncak.getStatus() == QcAncak.Save){
                etBlock.setText(selectedQcAncak.getTph().getBlock());
                etTph.setText(selectedQcAncak.getTph().getNamaTph());
                Toast.makeText(HarvestApp.getContext(),"Anda Terlalu Jauh Dari "+ selectedQcAncak.getTph().getNamaTph() + " Yang Sebelumnya Di Simpan",Toast.LENGTH_SHORT).show();
            }else{
                etBlock.setText(selectedQcAncak.getTph().getBlock());
                etTph.setText(selectedQcAncak.getTph().getNamaTph());
                Toast.makeText(HarvestApp.getContext(),"Anda Terlalu Jauh Dari "+ tph.getNamaTph(),Toast.LENGTH_SHORT).show();
            }

        }
    }

    public void startLongOperation(int status){
        new LongOperation().execute(String.valueOf(status));
    }

    private class LongOperation extends AsyncTask<String, Void, String> {
        boolean validasi;
        int status = 0;

        public LongOperation(int status) {
            this.status = status;
        }

        public LongOperation() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (((BaseActivity) activity).alertDialogBase != null) {
                ((BaseActivity) activity).alertDialogBase.cancel();
            }
            ((BaseActivity) activity).alertDialogBase = WidgetHelper.showWaitingDialog(activity,getResources().getString(R.string.wait));
            validasi = true;
            if(status == STATUS_LONG_OPERATION_SAVE){
                validasi = mutuAncakHelper.validasiAncak();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            if(validasi) {
                switch (Integer.parseInt(strings[0])) {
                    case STATUS_LONG_OPERATION_NONE:
                        updateDataRv(Integer.parseInt(strings[0]));
                        break;

                    case STATUS_LONG_OPERATION_SEARCH:
                        updateDataRv(Integer.parseInt(strings[0]));
                        break;

                    case STATUS_LONG_OPERATION_SETUP_DATA_TPH:
                        allTph = new ArrayList<>();
                        allTph = tphHelper.setDataTph(GlobalHelper.getEstate().getEstCode(), etBlock.getText().toString());
                        if (((BaseActivity) activity).currentLocationOK() && (selectedQcAncak.getTph() == null || qcMutuAncakActivity.seletedQcAncak == null)) {
                            tphTerdekat = tphHelper.cariTphTerdekat(allTph);
                        }
                        break;
                    case STATUS_LONG_OPERATION_SAVE:
                        validasi = mutuAncakHelper.saveQcAncak();
                        break;
                    case STATUS_LONG_OPERATION_DELETED:
                        validasi = mutuAncakHelper.hapusQcAncak();
                        break;
                    case STATUS_LONG_OPERATION_SETUP_POINT:
                        setupPoint();
                        break;
                }
            }
            return strings[0];
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            switch (Integer.parseInt(result)){
                case STATUS_LONG_OPERATION_NONE:
                    updateUIRV(Integer.parseInt(result));

                    if (((BaseActivity) activity).alertDialogBase != null) {
                        ((BaseActivity) activity).alertDialogBase.cancel();
                    }

                    if(allTph.size() == 0){
                        startLongOperation(STATUS_LONG_OPERATION_SETUP_DATA_TPH);
                    }
                    break;

                case STATUS_LONG_OPERATION_SEARCH:
                    updateUIRV(Integer.parseInt(result));

                    if (((BaseActivity) activity).alertDialogBase != null) {
                        ((BaseActivity) activity).alertDialogBase.cancel();
                    }
                    break;

                case STATUS_LONG_OPERATION_SELECT_ITEM:
                    if (!((BaseActivity) activity).currentLocationOK() && qcMutuAncakActivity.seletedQcAncak.getStatus() == QcAncak.NotSave){
                        qcMutuAncakActivity.searchingGPS = Snackbar.make(qcMutuAncakActivity.myCoordinatorLayout,getResources().getString(R.string.search_gps),Snackbar.LENGTH_INDEFINITE);
                        qcMutuAncakActivity.searchingGPS.show();
                    }else if(qcMutuAncakActivity.seletedQcAncak.getTph().getBaris() == null){
                        qcMutuAncakActivity.searchingGPS = Snackbar.make(qcMutuAncakActivity.myCoordinatorLayout,"Mohon Isi Baris",Snackbar.LENGTH_INDEFINITE);
                        qcMutuAncakActivity.searchingGPS.show();
                    }else if(qcMutuAncakActivity.seletedQcAncak.getTph().getBaris().size() == 0){
                        qcMutuAncakActivity.searchingGPS = Snackbar.make(qcMutuAncakActivity.myCoordinatorLayout,"Mohon Isi Baris",Snackbar.LENGTH_INDEFINITE);
                        qcMutuAncakActivity.searchingGPS.show();
                    }else{
                        qcMutuAncakActivity.getSupportFragmentManager().beginTransaction()
                                .replace(R.id.content_container, QcMutuAncakInputEntryFragment.getInstance())
                                .commit();
                    }

                    if (((BaseActivity) activity).alertDialogBase != null) {
                        ((BaseActivity) activity).alertDialogBase.cancel();
                    }
                    break;

                case STATUS_LONG_OPERATION_NEW_DATA:
                    if (!((BaseActivity) activity).currentLocationOK()){
                        qcMutuAncakActivity.searchingGPS = Snackbar.make(qcMutuAncakActivity.myCoordinatorLayout,getResources().getString(R.string.search_gps),Snackbar.LENGTH_INDEFINITE);
                        qcMutuAncakActivity.searchingGPS.show();
                    }else if(qcMutuAncakActivity.seletedQcAncak.getTph().getBaris() == null){
                        qcMutuAncakActivity.searchingGPS = Snackbar.make(qcMutuAncakActivity.myCoordinatorLayout,"Mohon Isi Baris",Snackbar.LENGTH_INDEFINITE);
                        qcMutuAncakActivity.searchingGPS.show();
                    }else if(qcMutuAncakActivity.seletedQcAncak.getTph().getBaris().size() == 0){
                        qcMutuAncakActivity.searchingGPS = Snackbar.make(qcMutuAncakActivity.myCoordinatorLayout,"Mohon Isi Baris",Snackbar.LENGTH_INDEFINITE);
                        qcMutuAncakActivity.searchingGPS.show();
                    }else{
                        qcMutuAncakActivity.getSupportFragmentManager().beginTransaction()
                                .replace(R.id.content_container, QcMutuAncakInputEntryFragment.getInstance())
                                .commit();
                    }

                    if (((BaseActivity) activity).alertDialogBase != null) {
                        ((BaseActivity) activity).alertDialogBase.cancel();
                    }
                    break;

                case STATUS_LONG_OPERATION_SETUP_DATA_TPH:
                    if(allTph.size() > 0 && allTph != null) {
                        adapterTph = new SelectTphAdapter(activity, R.layout.row_setting, allTph);
                        etTph.setThreshold(1);
                        etTph.setAdapter(adapterTph);
                        adapterTph.notifyDataSetChanged();

                        if (tphTerdekat != null) {
                            setTph(tphTerdekat);
                        }
                    }
                        if (((BaseActivity) activity).alertDialogBase != null) {
                            ((BaseActivity) activity).alertDialogBase.cancel();
                        }
                    break;
                case STATUS_LONG_OPERATION_SETUP_POINT:{
                    if(blockName != null){
                        etBlock.setText(blockName);
                    }
                    if(allTph.size() > 0 && allTph != null) {
                        adapterTph = new SelectTphAdapter(activity, R.layout.row_setting, allTph);
                        etTph.setThreshold(1);
                        etTph.setAdapter(adapterTph);
                        adapterTph.notifyDataSetChanged();

                        if (tphTerdekat != null) {
                            setTph(tphTerdekat);
                        }
                    }
                    if (((BaseActivity) activity).alertDialogBase != null) {
                        ((BaseActivity) activity).alertDialogBase.cancel();
                    }
                    break;
                }
                case STATUS_LONG_OPERATION_SAVE:
                    if (((BaseActivity) activity).alertDialogBase != null) {
                        ((BaseActivity) activity).alertDialogBase.cancel();
                    }
                    if(validasi) {
                        Toast.makeText(HarvestApp.getContext(),"Berhasil Simpan Qc Ancak",Toast.LENGTH_SHORT).show();
                        qcMutuAncakActivity.keawalFragment();
                    }else{
                        Toast.makeText(HarvestApp.getContext(),"Gagal Simpan Qc Ancak",Toast.LENGTH_SHORT).show();
                    }
                    break;
                case STATUS_LONG_OPERATION_DELETED:
                    if (((BaseActivity) activity).alertDialogBase != null) {
                        ((BaseActivity) activity).alertDialogBase.cancel();
                    }
                    if(validasi) {
                        Toast.makeText(HarvestApp.getContext(),"Berhasil Hapus Qc Ancak",Toast.LENGTH_SHORT).show();
                        qcMutuAncakActivity.keawalFragment();
                    }else{
                        Toast.makeText(HarvestApp.getContext(),"Gagal Hapus Qc Ancak",Toast.LENGTH_SHORT).show();
                    }
                    break;
            }

        }
    }
}
