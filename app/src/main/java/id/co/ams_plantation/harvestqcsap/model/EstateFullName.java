package id.co.ams_plantation.harvestqcsap.model;

/**
 * Created by user on 11/21/2018.
 */

public class EstateFullName {
    String companyShortName;
    String companyFullName;

    public String getCompanyShortName() {
        return companyShortName;
    }

    public void setCompanyShortName(String companyShortName) {
        this.companyShortName = companyShortName;
    }

    public String getCompanyFullName() {
        return companyFullName;
    }

    public void setCompanyFullName(String companyFullName) {
        this.companyFullName = companyFullName;
    }
}
