package id.co.ams_plantation.harvestqcsap.Fragment;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.google.gson.Gson;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.ObjectRepository;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import id.co.ams_plantation.harvestqcsap.HarvestApp;
import id.co.ams_plantation.harvestqcsap.adapter.AnswerAdapter;
import id.co.ams_plantation.harvestqcsap.model.Answer;
import id.co.ams_plantation.harvestqcsap.model.DataNitrit;
import id.co.ams_plantation.harvestqcsap.model.QcAncak;
import id.co.ams_plantation.harvestqcsap.model.QcAncakPohon;
import id.co.ams_plantation.harvestqcsap.model.QuestionAnswer;
import id.co.ams_plantation.harvestqcsap.model.TPH;
import id.co.ams_plantation.harvestqcsap.ui.BaseActivity;
import id.co.ams_plantation.harvestqcsap.ui.QcMutuAncakActivity;
import id.co.ams_plantation.harvestqcsap.util.CompleteTextViewHelper;
import id.co.ams_plantation.harvestqcsap.util.GlobalHelper;
import id.co.ams_plantation.harvestqcsap.util.QuestionAnswerHelper;
import id.co.ams_plantation.harvestqcsap.util.WidgetHelper;
import id.co.ams_plantation.harvestqcsap.R;
import pl.aprilapps.easyphotopicker.EasyImage;

public class QcMutuAncakInputEntryFragment extends Fragment {

    static final int LongOperation_Save = 0;
    static final int LongOperation_Deleted = 1;
    private static String TAG = QcMutuAncakInputEntryFragment.class.getSimpleName();

    View tphphoto;
    ImageView ivPhoto;
    ImageView imagecacnel;
    ImageView imagesview;
    SliderLayout sliderLayout;
    RelativeLayout rl_ipf_takepicture;

    CompleteTextViewHelper etBaris;
    AppCompatEditText etPohon;
    AppCompatEditText etJanjang;
    AppCompatEditText etBTS;
    AppCompatEditText etBTB;
    AppCompatEditText etBST;
    AppCompatEditText etBLT;
    AppCompatEditText etBM;
    RecyclerView rvPBT;
    RecyclerView rvPB;
    RecyclerView rvLT;
    LinearLayout lPBTChoice;
    LinearLayout lPBChoice;
    LinearLayout lLTChoice;
    Button etPBT;
    Button etPB;
    Button etLT;
    CheckBox cbOP;
    CheckBox cbSP;
    CheckBox cbPS;
    TextView tvTotalPokokTph;
    TextView tvTotalPokok;
    TextView tvTotalJanjang;
    TextView tvTotalBuahTinggal;
    TextView tvTotalBrondolan;
    LinearLayout lsave;
    LinearLayout lPBT;
    LinearLayout lPB;
    LinearLayout lLT;
    LinearLayout lldeleted;
    LinearLayout lcancel;
    Set<String> allBaris;

    BaseActivity baseActivity;
    QcMutuAncakActivity qcMutuAncakActivity;
    QcAncakPohon selectedAncakPohon;

    HashMap<String,Answer> selectedAnswerPenyebabBuahTinggal;
    HashMap<String,Answer> selectedAnswerPenyebabBrondolan;
    HashMap<String,Answer> selectedAnswerLokasiTinggal;
    QuestionAnswer questionAnswerPenyebabBuahTinggal;
    QuestionAnswer questionAnswerPenyebabBrondolan;
    QuestionAnswer questionAnswerLokasiTinggal;

    ArrayList<File> ALselectedImage;

    QuestionAnswerHelper questionAnswerHelper;
    private Activity activity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
            activity = (Activity) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activity = null;
    }

    public static QcMutuAncakInputEntryFragment getInstance(){
        QcMutuAncakInputEntryFragment fragment = new QcMutuAncakInputEntryFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.qc_ancak_input_entry_layout,null,false);
        return view;
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tphphoto = (View) view.findViewById(R.id.tphphoto);
        ivPhoto = (ImageView) view.findViewById(R.id.ivPhoto);
        imagecacnel = (ImageView) view.findViewById(R.id.imagecacnel);
        imagesview = (ImageView) view.findViewById(R.id.imagesview);
        rl_ipf_takepicture = (RelativeLayout) view.findViewById(R.id.rl_ipf_takepicture);
        sliderLayout = (SliderLayout) view.findViewById(R.id.sliderLayout);

        etBaris = view.findViewById(R.id.etBaris);
        etPohon = view.findViewById(R.id.etPohon);
        etJanjang = view.findViewById(R.id.etJanjang);
        etBTS = view.findViewById(R.id.etBTS);
        etBTB = view.findViewById(R.id.etBTB);
        etBST = view.findViewById(R.id.etBST);
        etBLT = view.findViewById(R.id.etBLT);
        etBM = view.findViewById(R.id.etBM);
        rvPBT = view.findViewById(R.id.rvPBT);
        rvPB = view.findViewById(R.id.rvPB);
        rvLT = view.findViewById(R.id.rvLT);
        lPBTChoice = view.findViewById(R.id.lPBTChoice);
        lPBChoice = view.findViewById(R.id.lPBChoice);
        lLTChoice = view.findViewById(R.id.lLTChoice);
        etPBT = view.findViewById(R.id.etPBT);
        etPB = view.findViewById(R.id.etPB);
        etLT = view.findViewById(R.id.etLT);
        cbOP = view.findViewById(R.id.cbOP);
        cbSP = view.findViewById(R.id.cbSP);
        cbPS = view.findViewById(R.id.cbPS);
        lPBT = view.findViewById(R.id.lPBT);
        lPB = view.findViewById(R.id.lPB);
        lLT = view.findViewById(R.id.lLT);
        lsave = view.findViewById(R.id.lsave);
        tvTotalPokokTph = view.findViewById(R.id.tvTotalPokokTph);
        tvTotalPokok = view.findViewById(R.id.tvTotalPokok);
        tvTotalJanjang = view.findViewById(R.id.tvTotalJanjang);
        tvTotalBuahTinggal = view.findViewById(R.id.tvTotalBuahTinggal);
        tvTotalBrondolan = view.findViewById(R.id.tvTotalBrondolan);
        lldeleted = view.findViewById(R.id.lldeleted);
        lcancel = view.findViewById(R.id.lcancel);

        baseActivity = ((BaseActivity) activity);
        questionAnswerHelper = new QuestionAnswerHelper((FragmentActivity) activity);

        if(activity instanceof  QcMutuAncakActivity){
            qcMutuAncakActivity = (QcMutuAncakActivity) activity;
        }
        selectedAnswerPenyebabBuahTinggal = new HashMap<String,Answer>();
        selectedAnswerPenyebabBrondolan = new HashMap<String,Answer>();
        selectedAnswerLokasiTinggal = new HashMap<String,Answer>();
        ALselectedImage = new ArrayList<>();
        allBaris = new android.support.v4.util.ArraySet<>();;
        for(Integer integer : qcMutuAncakActivity.seletedQcAncak.getTph().getBaris()){
            allBaris.add(String.valueOf(integer));
        }

        ArrayAdapter<String> adapterBaris = new ArrayAdapter<String>
                (activity,android.R.layout.select_dialog_item, allBaris.toArray(new String[allBaris.size()]));

        lPBTChoice.setVisibility(View.GONE);
        lPBChoice.setVisibility(View.GONE);
        lLTChoice.setVisibility(View.GONE);

        if(qcMutuAncakActivity.questionAnswers != null){
            questionAnswerPenyebabBuahTinggal = qcMutuAncakActivity.questionAnswers.get("QAA02");
//            List<String> stringsPBT = new ArrayList<>();
//            for(int i = 0 ; i < questionAnswerPenyebabBuahTinggal.getAnswer().size(); i++){
//                stringsPBT.add(questionAnswerPenyebabBuahTinggal.getAnswer().get(i).getQcAnsware());
//            }
//            ArrayAdapter<String> adapterPBT = new ArrayAdapter<String>(getActivity(),
//                    R.layout.simple_dropdown_item_1line, stringsPBT);
//            etPBT.setAdapter(adapterPBT);
//            etPBT.setText(stringsPBT.get(0));

            questionAnswerPenyebabBrondolan = qcMutuAncakActivity.questionAnswers.get("QAA03");
//            List<String> stringsPB = new ArrayList<>();
//            for(int i = 0 ; i < questionAnswerPenyebabBrondolan.getAnswer().size(); i++){
//                stringsPB.add(questionAnswerPenyebabBrondolan.getAnswer().get(i).getQcAnsware());
//            }
//            ArrayAdapter<String> adapterPB = new ArrayAdapter<String>(getActivity(),
//                    R.layout.simple_dropdown_item_1line, stringsPB);
//            etPB.setAdapter(adapterPB);
//            etPB.setText(stringsPB.get(0));

            questionAnswerLokasiTinggal = qcMutuAncakActivity.questionAnswers.get("QAA04");
//            List<String> stringsLT = new ArrayList<>();
//            for(int i = 0 ; i < questionAnswerLokasiTinggal.getAnswer().size(); i++){
//                stringsLT.add(questionAnswerLokasiTinggal.getAnswer().get(i).getQcAnsware());
//            }
//            ArrayAdapter<String> adapterLT = new ArrayAdapter<String>(getActivity(),
//                    R.layout.simple_dropdown_item_1line, stringsLT);
//            etLT.setAdapter(adapterLT);
//            etLT.setText(stringsLT.get(0));
        }

        etBaris.setThreshold(1);
        etBaris.setAdapter(adapterBaris);
        etBaris.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etBaris.showDropDown();
            }
        });
        etBaris.setText(String.valueOf(qcMutuAncakActivity.seletedQcAncak.getTph().getBaris().get(0)));
        int pokok = TPH.BANYAK_POKOK;
        if(qcMutuAncakActivity.seletedQcAncak.getTph().getJumlahPokok() > 0){
            pokok = qcMutuAncakActivity.seletedQcAncak.getTph().getJumlahPokok();
        }
        tvTotalPokokTph.setText(String.valueOf(pokok));

        if(qcMutuAncakActivity.seletedQcAncakPohon != null){
            selectedAncakPohon = qcMutuAncakActivity.seletedQcAncakPohon;
            qcMutuAncakActivity.seletedQcAncakPohon = null;
            setValue();
            lldeleted.setVisibility(View.VISIBLE);
        }else{
            lldeleted.setVisibility(View.GONE);
        }

        if(qcMutuAncakActivity.seletedQcAncak.getStatus() == QcAncak.Upload){
            lldeleted.setVisibility(View.GONE);
            lsave.setVisibility(View.GONE);
        }
        baseActivity.zoomToLocation.setVisibility(View.GONE);

        setSummaryQcAncak();
        showHidePenyebabBuahTinggal();
        showHidePenyebabBrondolan();
        showHidePenyebabLokasiTinggal();

        etJanjang.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if(etJanjang.getText().toString().equals("")){
                        etJanjang.setText("0");
                    }
                }
            }
        });

        etBTS.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if(etBTS.getText().toString().equals("")){
                        etBTS.setText("0");
                    }
                }
            }
        });

        etBTS.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                showHidePenyebabBuahTinggal();
                showHidePenyebabLokasiTinggal();
            }
        });

        etBTB.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if(etBTB.getText().toString().equals("")){
                        etBTB.setText("0");
                    }
                }
            }
        });

        etBTB.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                showHidePenyebabBuahTinggal();
                showHidePenyebabLokasiTinggal();
            }
        });

        etBST.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if(etBST.getText().toString().equals("")){
                        etBST.setText("0");
                    }
                }
            }
        });

        etBST.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                showHidePenyebabBrondolan();
                showHidePenyebabLokasiTinggal();
            }
        });

        etBLT.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if(etBLT.getText().toString().equals("")){
                        etBLT.setText("0");
                    }
                }
            }
        });

        etBLT.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                showHidePenyebabBrondolan();
                showHidePenyebabLokasiTinggal();
            }
        });

        etBM.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if(etBM.getText().toString().equals("")){
                        etBM.setText("0");
                    }
                }
            }
        });

        etPBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GlobalHelper.hideKeyboard((FragmentActivity) activity);
                Collection<Answer> values = selectedAnswerPenyebabBuahTinggal.values();
                questionAnswerHelper.showQuestionSelect(questionAnswerPenyebabBuahTinggal,new ArrayList<>(values));
            }
        });

        etPB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GlobalHelper.hideKeyboard((FragmentActivity) activity);
                Collection<Answer> values = selectedAnswerPenyebabBrondolan.values();
                questionAnswerHelper.showQuestionSelect(questionAnswerPenyebabBrondolan,new ArrayList<>(values));
            }
        });

        etLT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GlobalHelper.hideKeyboard((FragmentActivity) activity);
                Collection<Answer> values = selectedAnswerLokasiTinggal.values();
                questionAnswerHelper.showQuestionSelect(questionAnswerLokasiTinggal,new ArrayList<>(values));
            }
        });

        ivPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ALselectedImage.size() > 2) {
                    Toast.makeText(HarvestApp.getContext(),activity.getResources().getString(R.string.warning_take_foto),Toast.LENGTH_LONG).show();
                }else {
                    GlobalHelper.TAG_CAMERA = GlobalHelper.TAG_CAMERA_QC_ANCAK;
                    EasyImage.openCamera(activity,1);
                }

            }
        });

        lsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!GlobalHelper.isDoubleClick()){
                    new LongOperation().execute(String.valueOf(LongOperation_Save));
                }
            }
        });

        lcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!GlobalHelper.isDoubleClick()){
                    qcMutuAncakActivity.backProses();
                }
            }
        });

        lldeleted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!GlobalHelper.isDoubleClick()){
                    new LongOperation().execute(String.valueOf(LongOperation_Deleted));
                }
            }
        });
    }

    private void showHidePenyebabBuahTinggal(){
        int bts = 0;
        int btb = 0;
        if(!etBTS.getText().toString().isEmpty() ){
            if (Integer.parseInt(etBTS.getText().toString()) > 0) {
                bts = Integer.parseInt(etBTS.getText().toString());
            }
        }
        if(!etBTB.getText().toString().isEmpty() ){
            if (Integer.parseInt(etBTB.getText().toString()) > 0) {
                btb = Integer.parseInt(etBTB.getText().toString());
            }
        }

        if(bts > 0 || btb > 0 ){
            lPBT.setVisibility(View.VISIBLE);
        }else {
            lPBT.setVisibility(View.GONE);
            lPBTChoice.setVisibility(View.GONE);
            selectedAnswerPenyebabBuahTinggal = new HashMap<>();
        }
    }

    private  void showHidePenyebabBrondolan(){
        int bst = 0;
        int blt = 0;
        if(!etBST.getText().toString().isEmpty() ){
            if (Integer.parseInt(etBST.getText().toString()) > 0) {
                bst = Integer.parseInt(etBST.getText().toString());
            }
        }
        if(!etBLT.getText().toString().isEmpty() ){
            if (Integer.parseInt(etBLT.getText().toString()) > 0) {
                blt = Integer.parseInt(etBLT.getText().toString());
            }
        }

        if(bst > 0 || blt > 0 ){
            lPB.setVisibility(View.VISIBLE);
        }else {
            lPB.setVisibility(View.GONE);
            lPBChoice.setVisibility(View.GONE);
            selectedAnswerPenyebabBrondolan = new HashMap<>();
        }
    }


    private  void showHidePenyebabLokasiTinggal(){

        int bts = 0;
        int btb = 0;
        int bst = 0;
        int blt = 0;

        if(!etBST.getText().toString().isEmpty() ){
            if (Integer.parseInt(etBST.getText().toString()) > 0) {
                bst = Integer.parseInt(etBST.getText().toString());
            }
        }
        if(!etBLT.getText().toString().isEmpty() ){
            if (Integer.parseInt(etBLT.getText().toString()) > 0) {
                blt = Integer.parseInt(etBLT.getText().toString());
            }
        }
        if(!etBTS.getText().toString().isEmpty() ){
            if (Integer.parseInt(etBTS.getText().toString()) > 0) {
                bts = Integer.parseInt(etBTS.getText().toString());
            }
        }
        if(!etBTB.getText().toString().isEmpty() ){
            if (Integer.parseInt(etBTB.getText().toString()) > 0) {
                btb = Integer.parseInt(etBTB.getText().toString());
            }
        }

        if(bts > 0 || btb > 0 || bst > 0 || blt > 0 ){
            lLT.setVisibility(View.VISIBLE);
        }else {
            lLT.setVisibility(View.GONE);
            lLTChoice.setVisibility(View.GONE);
            selectedAnswerLokasiTinggal = new HashMap<>();
        }
    }

    private void setValue(){
        etBaris.setText(String.valueOf(selectedAncakPohon.getNoBaris()));
        etPohon.setText(String.valueOf(selectedAncakPohon.getIdPohon()));
        etJanjang.setText(String.valueOf(selectedAncakPohon.getJanjangPanen()));
        etBTS.setText(String.valueOf(selectedAncakPohon.getBuahTinggalSegar()));
        etBTB.setText(String.valueOf(selectedAncakPohon.getBuahTinggalBusuk()));
        etBST.setText(String.valueOf(selectedAncakPohon.getBrondolanSegarTinggal()));
        etBLT.setText(String.valueOf(selectedAncakPohon.getBrondolanLamaTinggal()));
        etBM.setText(String.valueOf(selectedAncakPohon.getBuahMatahari()));

        cbOP.setChecked(selectedAncakPohon.isOverPrun());
        cbSP.setChecked(selectedAncakPohon.isPelepahSengkel());
        cbPS.setChecked(selectedAncakPohon.isSusunanPelepah());

        selectedAnswerPenyebabBuahTinggal = new HashMap<>();
        selectedAnswerPenyebabBrondolan = new HashMap<>();
        selectedAnswerLokasiTinggal = new HashMap<>();

        if(selectedAncakPohon.getPenyebabBuahTinggals() != null){
            if(selectedAncakPohon.getPenyebabBuahTinggals().getAnswer() != null){
                for(int i = 0 ; i < selectedAncakPohon.getPenyebabBuahTinggals().getAnswer().size();i++) {
                    Answer answer = selectedAncakPohon.getPenyebabBuahTinggals().getAnswer().get(i);
                    selectedAnswerPenyebabBuahTinggal.put(answer.getIdQcAnswer(),answer);
                }
                updateRv(questionAnswerPenyebabBuahTinggal);
            }
        }

        if(selectedAncakPohon.getPenyebabBrondolans() != null){
            if(selectedAncakPohon.getPenyebabBrondolans().getAnswer() != null){
                for(int i = 0 ; i < selectedAncakPohon.getPenyebabBrondolans().getAnswer().size();i++) {
                    Answer answer = selectedAncakPohon.getPenyebabBrondolans().getAnswer().get(i);
                    selectedAnswerPenyebabBrondolan.put(answer.getIdQcAnswer(),answer);
                }
                updateRv(questionAnswerPenyebabBrondolan);
            }
        }

        if(selectedAncakPohon.getLokasiTertinggals() != null){
            if(selectedAncakPohon.getLokasiTertinggals().getAnswer() != null){
                for(int i = 0 ; i < selectedAncakPohon.getLokasiTertinggals().getAnswer().size();i++) {
                    Answer answer = selectedAncakPohon.getLokasiTertinggals().getAnswer().get(i);
                    selectedAnswerLokasiTinggal.put(answer.getIdQcAnswer(),answer);
                }
                updateRv(questionAnswerLokasiTinggal);
            }
        }

        ALselectedImage = new ArrayList<>();
        if(selectedAncakPohon.getFoto() != null) {
            if(selectedAncakPohon.getFoto().size() > 0) {
                for (String s : selectedAncakPohon.getFoto()) {
                    File file = new File(s);
                    ALselectedImage.add(file);
                }
            }
        }

        setupimageslide();
    }

    public void updateMultiSelect(boolean isChecked,QuestionAnswer questionAnswer,Answer answer){
        if(isChecked){
            if(questionAnswer.getIdQuestionAnswer().equals(questionAnswerPenyebabBuahTinggal.getIdQuestionAnswer())){
                selectedAnswerPenyebabBuahTinggal.put(answer.getIdQcAnswer(),answer);
            }else if (questionAnswer.getIdQuestionAnswer().equals(questionAnswerPenyebabBrondolan.getIdQuestionAnswer())){
                selectedAnswerPenyebabBrondolan.put(answer.getIdQcAnswer(),answer);
            }else if (questionAnswer.getIdQuestionAnswer().equals(questionAnswerLokasiTinggal.getIdQuestionAnswer())){
                selectedAnswerLokasiTinggal.put(answer.getIdQcAnswer(),answer);
            }
        }else{
            if(questionAnswer.getIdQuestionAnswer().equals(questionAnswerPenyebabBuahTinggal.getIdQuestionAnswer())) {
                if (selectedAnswerPenyebabBuahTinggal.size() > 0) {
                    for (Map.Entry<String, Answer> ans : selectedAnswerPenyebabBuahTinggal.entrySet()) {
                        if (ans.getValue().getIdQcAnswer().toLowerCase().equals(answer.getIdQcAnswer().toLowerCase())) {
                            selectedAnswerPenyebabBuahTinggal.remove(answer.getIdQcAnswer());
                            break;
                        }
                    }
                }
            }else if (questionAnswer.getIdQuestionAnswer().equals(questionAnswerPenyebabBrondolan.getIdQuestionAnswer())){
                if (selectedAnswerPenyebabBrondolan.size() > 0) {
                    for (Map.Entry<String, Answer> ans : selectedAnswerPenyebabBrondolan.entrySet()) {
                        if (ans.getValue().getIdQcAnswer().toLowerCase().equals(answer.getIdQcAnswer().toLowerCase())) {
                            selectedAnswerPenyebabBrondolan.remove(answer.getIdQcAnswer());
                            break;
                        }
                    }
                }
            }else if (questionAnswer.getIdQuestionAnswer().equals(questionAnswerLokasiTinggal.getIdQuestionAnswer())){
                if (selectedAnswerLokasiTinggal.size() > 0) {
                    for (Map.Entry<String, Answer> ans : selectedAnswerLokasiTinggal.entrySet()) {
                        if (ans.getValue().getIdQcAnswer().toLowerCase().equals(answer.getIdQcAnswer().toLowerCase())) {
                            selectedAnswerLokasiTinggal.remove(answer.getIdQcAnswer());
                            break;
                        }
                    }
                }
            }
        }
    }

    public void updateRv(QuestionAnswer questionAnswer){
        if(questionAnswer.getIdQuestionAnswer().equals(questionAnswerPenyebabBuahTinggal.getIdQuestionAnswer())){
            if(selectedAnswerPenyebabBuahTinggal.size() > 0){
                lPBTChoice.setVisibility(View.VISIBLE);
                Collection<Answer> values = selectedAnswerPenyebabBuahTinggal.values();
                rvPBT.setLayoutManager(new GridLayoutManager(activity,2));
                AnswerAdapter answerAdapter = new AnswerAdapter(activity,new ArrayList<>(values));
                rvPBT.setAdapter(answerAdapter);
            }else{
                lPBTChoice.setVisibility(View.GONE);
            }
        }else if (questionAnswer.getIdQuestionAnswer().equals(questionAnswerPenyebabBrondolan.getIdQuestionAnswer())){
            if(selectedAnswerPenyebabBrondolan.size() > 0){
                lPBChoice.setVisibility(View.VISIBLE);
                Collection<Answer> values = selectedAnswerPenyebabBrondolan.values();
                rvPB.setLayoutManager(new GridLayoutManager(activity,2));
                AnswerAdapter answerAdapter = new AnswerAdapter(activity,new ArrayList<>(values));
                rvPB.setAdapter(answerAdapter);
            }else{
                lPBChoice.setVisibility(View.GONE);
            }
        }else if (questionAnswer.getIdQuestionAnswer().equals(questionAnswerLokasiTinggal.getIdQuestionAnswer())){
            if(selectedAnswerLokasiTinggal.size() > 0){
                lLTChoice.setVisibility(View.VISIBLE);
                Collection<Answer> values = selectedAnswerLokasiTinggal.values();
                rvLT.setLayoutManager(new GridLayoutManager(activity,2));
                AnswerAdapter answerAdapter = new AnswerAdapter(activity,new ArrayList<>(values));
                rvLT.setAdapter(answerAdapter);
            }else{
                lLTChoice.setVisibility(View.GONE);
            }
        }
    }

    private void setSummaryQcAncak(){
        int TotalJanjang =0;
        int TotalBuahTinggal =0;
        int TotalBrondolan =0;
        for(int i =0;i<qcMutuAncakActivity.seletedQcAncak.getQcAncakPohons().size();i++){
            QcAncakPohon qcAncakPohon = qcMutuAncakActivity.seletedQcAncak.getQcAncakPohons().get(i);
            TotalJanjang += qcAncakPohon.getJanjangPanen();
            TotalBuahTinggal += qcAncakPohon.getBuahTinggalBusuk() + qcAncakPohon.getBuahTinggalSegar();
            TotalBrondolan += qcAncakPohon.getBrondolanLamaTinggal() + qcAncakPohon.getBrondolanSegarTinggal();
        }
        tvTotalPokok.setText(String.valueOf(qcMutuAncakActivity.seletedQcAncak.getQcAncakPohons().size()));
        tvTotalJanjang.setText(String.valueOf(TotalJanjang));
        tvTotalBuahTinggal.setText(String.valueOf(TotalBuahTinggal));
        tvTotalBrondolan.setText(String.valueOf(TotalBrondolan));
    }

    private void clearFormInput(){
        etPohon.requestFocus();
        etPohon.setText("");
        etJanjang.setText("");
        etBTS.setText("");
        etBTB.setText("");
        etBST.setText("");
        etBLT.setText("");
        etBM.setText("");
        cbOP.setChecked(false);
        cbPS.setChecked(false);
        cbSP.setChecked(false);
        GlobalHelper.showKeyboard((FragmentActivity) activity);
        ALselectedImage = new ArrayList<>();
        selectedAnswerPenyebabBuahTinggal = new HashMap<>();
        selectedAnswerPenyebabBrondolan = new HashMap<>();
        selectedAnswerLokasiTinggal = new HashMap<>();
        lPBTChoice.setVisibility(View.GONE);
        lPBChoice.setVisibility(View.GONE);
        lLTChoice.setVisibility(View.GONE);
        setupimageslide();
    }

    private void cekBanyakPokok(){
        int pokok = TPH.BANYAK_POKOK;
        if(qcMutuAncakActivity.seletedQcAncak.getTph().getJumlahPokok() > 0){
            pokok = qcMutuAncakActivity.seletedQcAncak.getTph().getJumlahPokok();
        }

        if(qcMutuAncakActivity.seletedQcAncak.getQcAncakPohons().size() < pokok){
            int sisa = pokok - qcMutuAncakActivity.seletedQcAncak.getQcAncakPohons().size();
            Toast.makeText(HarvestApp.getContext(),"Kuota pokok yang dapat di qc "+ sisa,Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(HarvestApp.getContext(),"Jumlah pokok di qc sudah sebanyak jumlah pokok pada tph",Toast.LENGTH_SHORT).show();
            qcMutuAncakActivity.backProses();
        }
    }

    private boolean validasiQcAncakBaris(){
        if(etBaris.getText().toString().isEmpty()){
            Toast.makeText(HarvestApp.getContext(),"Mohon Pilih Baris",Toast.LENGTH_SHORT).show();
            etBaris.requestFocus();
            return false;
        }

        if(!allBaris.contains(etBaris.getText().toString())){
            Toast.makeText(HarvestApp.getContext(),"Baris Yang Anda Input Tidak Ada",Toast.LENGTH_SHORT).show();
            etBaris.requestFocus();
            return false;
        }

        if(etPohon.getText().toString().isEmpty()){
            Toast.makeText(HarvestApp.getContext(),"Mohon Input No Pohon",Toast.LENGTH_SHORT).show();
            etPohon.requestFocus();
            return false;
        }

        if (!etBM.getText().toString().isEmpty()) {
            if(Integer.parseInt(etBM.getText().toString()) >= 4){
                Toast.makeText(HarvestApp.getContext(), "Buah Matahari Terlalu Besar", Toast.LENGTH_SHORT).show();
                etBM.requestFocus();
                return false;
            }
        }

        if(lPBT.getVisibility() == View.VISIBLE){
            if(selectedAnswerPenyebabBuahTinggal == null){
                Toast.makeText(HarvestApp.getContext(), "Mohon Pilih Penyebab Buah Tinggal", Toast.LENGTH_SHORT).show();
                return false;
            }else if(selectedAnswerPenyebabBuahTinggal.size() == 0){
                Toast.makeText(HarvestApp.getContext(), "Mohon Pilih Penyebab Buah Tinggal", Toast.LENGTH_SHORT).show();
                return false;
            }
        }

        if(lPB.getVisibility() == View.VISIBLE){
            if(selectedAnswerPenyebabBrondolan == null){
                Toast.makeText(HarvestApp.getContext(), "Mohon Pilih Penyebab Brondolan", Toast.LENGTH_SHORT).show();
                return false;
            }else if(selectedAnswerPenyebabBrondolan.size() == 0){
                Toast.makeText(HarvestApp.getContext(), "Mohon Pilih Penyebab Brondolan", Toast.LENGTH_SHORT).show();
                return false;
            }
        }

        if(lLT.getVisibility() == View.VISIBLE){
            if(selectedAnswerLokasiTinggal == null){
                Toast.makeText(HarvestApp.getContext(), "Mohon Pilih Lokasi Tinggal", Toast.LENGTH_SHORT).show();
                return false;
            }else if(selectedAnswerLokasiTinggal.size() == 0){
                Toast.makeText(HarvestApp.getContext(), "Mohon Pilih Lokasi Tinggal", Toast.LENGTH_SHORT).show();
                return false;
            }
        }

        return true;
    }

    private void saveQcAncakBaris(){
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyHHmm");
        String idTransaksi ;
        if(selectedAncakPohon != null){
            idTransaksi = selectedAncakPohon.getIdQcAncakPohon();
        }else {
            idTransaksi = "P" + String.format("%04d", GlobalHelper.getCountNumber(GlobalHelper.LIST_FOLDER_SELECTED_QC_ANCAK_POHON))
                    + sdf.format(System.currentTimeMillis()) + GlobalHelper.getUser().getUserID();
        }

        QuestionAnswer saveQuestionanswerPenyebabBuahTinggal = new QuestionAnswer();
        QuestionAnswer saveQuestionanswerPenyebabBrondolan = new QuestionAnswer();
        QuestionAnswer saveQuestionanswerLokasiTinggal = new QuestionAnswer();

        if(lPBT.getVisibility() == View.VISIBLE) {
            Collection<Answer> values = selectedAnswerPenyebabBuahTinggal.values();
            saveQuestionanswerPenyebabBuahTinggal = new QuestionAnswer(
                    new ArrayList<>(values),
                    questionAnswerPenyebabBuahTinggal.getIdQcQuestion(),
                    questionAnswerPenyebabBuahTinggal.getIdQuestionAnswer(),
                    questionAnswerPenyebabBuahTinggal.getQcQuestion(),
                    questionAnswerPenyebabBuahTinggal.getQcType(),
                    questionAnswerPenyebabBuahTinggal.getQuestionAnswerType()
            );
        }

        if(lPB.getVisibility() == View.VISIBLE){
            Collection<Answer> values = selectedAnswerPenyebabBrondolan.values();
            saveQuestionanswerPenyebabBrondolan = new QuestionAnswer(
                    new ArrayList<>(values),
                    questionAnswerPenyebabBrondolan.getIdQcQuestion(),
                    questionAnswerPenyebabBrondolan.getIdQuestionAnswer(),
                    questionAnswerPenyebabBrondolan.getQcQuestion(),
                    questionAnswerPenyebabBrondolan.getQcType(),
                    questionAnswerPenyebabBrondolan.getQuestionAnswerType()
            );
        }

        if(lLT.getVisibility() == View.VISIBLE){
            Collection<Answer> values = selectedAnswerLokasiTinggal.values();
            saveQuestionanswerLokasiTinggal = new QuestionAnswer(
                    new ArrayList<>(values),
                    questionAnswerLokasiTinggal.getIdQcQuestion(),
                    questionAnswerLokasiTinggal.getIdQuestionAnswer(),
                    questionAnswerLokasiTinggal.getQcQuestion(),
                    questionAnswerLokasiTinggal.getQcType(),
                    questionAnswerLokasiTinggal.getQuestionAnswerType()
            );
        }

        HashSet<String> sFoto = new HashSet<>();
        if(ALselectedImage.size() > 0){
            for(File file : ALselectedImage){
                sFoto.add(file.getAbsolutePath());
            }
        }

        QcAncakPohon qcAncakPohon = new QcAncakPohon(idTransaksi,
            Integer.parseInt(etBaris.getText().toString().isEmpty() ? "0" : etBaris.getText().toString()),
            etPohon.getText().toString(),
            Integer.parseInt(etJanjang.getText().toString().isEmpty() ? "0" : etJanjang.getText().toString()),
            Integer.parseInt(etBTS.getText().toString().isEmpty() ? "0" : etBTS.getText().toString()),
            Integer.parseInt(etBTB.getText().toString().isEmpty() ? "0" : etBTB.getText().toString()),
            "",
                saveQuestionanswerPenyebabBuahTinggal,
            Integer.parseInt(etBST.getText().toString().isEmpty() ? "0" : etBST.getText().toString()),
            Integer.parseInt(etBLT.getText().toString().isEmpty() ? "0" : etBLT.getText().toString()),
            "",
                saveQuestionanswerPenyebabBrondolan,
            "",
                saveQuestionanswerLokasiTinggal,
            cbOP.isChecked(),
            cbPS.isChecked(),
            cbSP.isChecked(),
            Integer.parseInt(etBM.getText().toString().isEmpty() ? "0" : etBM.getText().toString()),
            ((BaseActivity) activity).currentlocation.getLatitude(),
            ((BaseActivity) activity).currentlocation.getLongitude(),
            System.currentTimeMillis(),
            sFoto
        );

        qcAncakPohon.setBuahTinggal( qcAncakPohon.getBuahTinggalSegar() + qcAncakPohon.getBuahTinggalBusuk());
        qcAncakPohon.setBrondolanTinggal( qcAncakPohon.getBrondolanLamaTinggal() + qcAncakPohon.getBrondolanSegarTinggal());

        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_ANCAK_POHON);
        Log.d("lokasi pohon",db.getContext().getFilePath());
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Gson gson = new Gson();
        DataNitrit dataNitrit = new DataNitrit(qcAncakPohon.getIdQcAncakPohon(),gson.toJson(qcAncakPohon));
        if(selectedAncakPohon == null) {
            repository.insert(dataNitrit);
            Log.d("input pohon",gson.toJson(dataNitrit));
            GlobalHelper.setCountNumberPlusOne(GlobalHelper.LIST_FOLDER_SELECTED_QC_ANCAK_POHON);
        }else{
            repository.update(dataNitrit);
            for(int i = 0 ; i < qcMutuAncakActivity.seletedQcAncak.getQcAncakPohons().size();i++){
                QcAncakPohon qcAncakPohonX = qcMutuAncakActivity.seletedQcAncak.getQcAncakPohons().get(i);
                if(qcAncakPohonX.getIdQcAncakPohon().equalsIgnoreCase(qcAncakPohon.getIdQcAncakPohon())){
                    qcMutuAncakActivity.seletedQcAncak.getQcAncakPohons().remove(i);
                    break;
                }
            }
            selectedAncakPohon = null;
        }

        qcMutuAncakActivity.seletedQcAncak.getQcAncakPohons().add(qcAncakPohon);
        db.close();
        qcMutuAncakActivity.addGraphicPokok(qcAncakPohon);
    }

    private void deletedQcAncakBaris(){
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_ANCAK_POHON);
        Log.d("lokasi pohon",db.getContext().getFilePath());
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Gson gson = new Gson();
        DataNitrit dataNitrit = new DataNitrit(selectedAncakPohon.getIdQcAncakPohon(),gson.toJson(selectedAncakPohon));
        if(selectedAncakPohon != null) {
            repository.remove(dataNitrit);
            for(int i = 0 ; i < qcMutuAncakActivity.seletedQcAncak.getQcAncakPohons().size();i++){
                QcAncakPohon qcAncakPohonX = qcMutuAncakActivity.seletedQcAncak.getQcAncakPohons().get(i);
                if(qcAncakPohonX.getIdQcAncakPohon().equalsIgnoreCase(selectedAncakPohon.getIdQcAncakPohon())){
                    qcMutuAncakActivity.seletedQcAncak.getQcAncakPohons().remove(i);
                    break;
                }
            }
        }
        db.close();
    }

    public void setImages(File images){
        ALselectedImage.add(images);
        setupimageslide();
    }

    private void setupimageslide(){
        Set<File> sf = new HashSet<>();
        sf.addAll(ALselectedImage);
        ALselectedImage.clear();
        ALselectedImage.addAll(sf);

        if(ALselectedImage.size() > 1){
            ivPhoto.setVisibility(View.GONE);
        }else{
            ivPhoto.setVisibility(View.VISIBLE);
        }
        if(ALselectedImage.size() != 0){
//            tphphoto.setVisibility(View.VISIBLE);
            imagecacnel.setVisibility(View.VISIBLE);
            imagesview.setVisibility(View.VISIBLE);
            sliderLayout.setVisibility(View.VISIBLE);
        }else{
//            tphphoto.setVisibility(View.GONE);
            imagecacnel.setVisibility(View.GONE);
            imagesview.setVisibility(View.GONE);
            sliderLayout.setVisibility(View.GONE);
        }
        sliderLayout.removeAllSliders();
        for(File file : ALselectedImage){
            TextSliderView textSliderView = new TextSliderView(activity);
            // initialize a SliderLayout
            if(file.toString().startsWith("http:")){
                String surl = file.toString();
                if(file.toString().startsWith("http://")){

                }else if(file.toString().startsWith("http:/")){
                    surl =surl.replace("http:/","http://");
                }

                textSliderView
                        .image(GlobalHelper.setUrlFoto(surl))
                        .setScaleType(BaseSliderView.ScaleType.Fit);
            }else {
                textSliderView
                        .image(file)
                        .setScaleType(BaseSliderView.ScaleType.Fit);
            }
            sliderLayout.addSlider(textSliderView);
        }
        sliderLayout.stopAutoCycle();
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.FlipHorizontal);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setCustomAnimation(new DescriptionAnimation());
        imagecacnel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(GSApp.getContext(),"slider getCurrentPosition " + sliderLayout.getCurrentPosition(),Toast.LENGTH_SHORT).show();
                try {
                    if (sliderLayout.getCurrentPosition() == 0 && ALselectedImage.size() == 1) {
                        ALselectedImage.clear();
                        sliderLayout.removeAllSliders();
                        sliderLayout.setVisibility(View.GONE);
                        imagecacnel.setVisibility(View.GONE);
                        imagesview.setVisibility(View.GONE);
                    } else {
                        ALselectedImage.remove(sliderLayout.getCurrentPosition());
                        sliderLayout.removeSliderAt(sliderLayout.getCurrentPosition());
                    }
                }catch (Exception e){
                    Log.e("sliderLayout", e.toString());
                }
            }
        });
        imagesview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WidgetHelper.showImagesZoom(activity,ALselectedImage);
            }
        });
    }

    private class LongOperation extends AsyncTask<String, Void, String> {
        private AlertDialog alertDialog;
        private Boolean validasi;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            alertDialog = WidgetHelper.showWaitingDialog(activity, getResources().getString(R.string.wait));
            validasi = validasiQcAncakBaris();
        }

        @Override
        protected String doInBackground(String... params) {
            if (validasi) {
                switch (Integer.parseInt(params[0])) {
                    case LongOperation_Save:
                        saveQcAncakBaris();
                        break;
                    case LongOperation_Deleted:
                        deletedQcAncakBaris();
                        break;
                }
            }
            return String.valueOf(params[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(validasi) {
                switch (Integer.parseInt(result)) {
                    case LongOperation_Save:
                        setSummaryQcAncak();
                        clearFormInput();
                        cekBanyakPokok();
                        Toast.makeText(HarvestApp.getContext(),getResources().getString(R.string.save_done),Toast.LENGTH_SHORT).show();
                        break;
                    case LongOperation_Deleted:
                        qcMutuAncakActivity.backProses();
                        Toast.makeText(HarvestApp.getContext(),getResources().getString(R.string.dg_message_delete_success_inst2),Toast.LENGTH_SHORT).show();
                        break;
                }
            }
            if (alertDialog != null) {
                alertDialog.cancel();
            }
        }
    }
}
