package id.co.ams_plantation.harvestqcsap.ui;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.esri.android.map.MapView;
import com.esri.android.runtime.ArcGISRuntime;
import com.esri.core.runtime.LicenseLevel;
import com.esri.core.runtime.LicenseResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;

import butterknife.BindView;
import id.co.ams_plantation.harvestqcsap.Fragment.TphInputFragment;
import id.co.ams_plantation.harvestqcsap.encryptor.Encrypts;
import id.co.ams_plantation.harvestqcsap.model.NearestBlock;
import id.co.ams_plantation.harvestqcsap.util.GlobalHelper;
import id.co.ams_plantation.harvestqcsap.util.NfcHelper;
import id.co.ams_plantation.harvestqcsap.view.TphView;
import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.model.Estate;
import id.co.ams_plantation.harvestqcsap.model.QcMutuBuah;
import id.co.ams_plantation.harvestqcsap.util.QrHelper;
import id.co.ams_plantation.harvestqcsap.util.WidgetHelper;

import static id.co.ams_plantation.harvestqcsap.util.GlobalHelper.REQUEST_RESOLVE_ERROR;

public class QcMutuBuahActivity extends BaseActivity implements TphView, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener ,com.google.android.gms.location.LocationListener {

    public Estate estate;
    public int TYPE_NFC;
    public String valueNFC;
    public QrHelper qrHelper;
    public QcMutuBuah qcMutuBuah;
    public NearestBlock nearestBlock;
    Activity activity;

    RelativeLayout lShowHide;
    RelativeLayout lShow;
    RelativeLayout lHide;
    ImageView ivShow;
    ImageView ivHide;

    @BindView(R.id.myCoordinatorLayout)
    public CoordinatorLayout myCoordinatorLayout;

    public Snackbar searchingGPS;
    /****************************************** NFC ******************************************************************/
    public Tag myTag;
    public NfcAdapter nfcAdapter;
    public PendingIntent pendingIntent;
    public IntentFilter writeTagFilters[];

    /****************************************************************************************************************/
    private static Handler handler;

    @Override
    protected void initPresenter() {}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LicenseResult licenseResult = ArcGISRuntime.setClientId("uFfCzKhpVfi0ggjz");
        LicenseLevel licenseLevel = ArcGISRuntime.License.getLicenseLevel();
        Log.e("License Result", licenseResult.toString() + " | " + licenseLevel.toString());
        setContentView(R.layout.new_layout_tph);
        getSupportActionBar().hide();

        mapView = findViewById(R.id.map);
        zoomToLocation = findViewById(R.id.zoomToLocation);
        rlMap = findViewById(R.id.rlMap);
        lShowHide = findViewById(R.id.lShowHide);
        ivShow = findViewById(R.id.ivShow);
        ivHide = findViewById(R.id.ivHide);
        lShow = findViewById(R.id.lShow);
        lHide = findViewById(R.id.lHide);

        qrHelper = new QrHelper(this);
        activity = this;

        zoomToLocation.setImageDrawable(new
                IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_my_location)
                .sizeDp(24)
                .colorRes(R.color.Black));
        ivShow.setImageDrawable(new
                IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_chevron_down).sizeDp(24)
                .colorRes(R.color.Black));

        ivHide.setImageDrawable(new
                IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_chevron_up).sizeDp(24)
                .colorRes(R.color.Black));

        rlMap.setVisibility(View.GONE);
        lShow.setVisibility(View.GONE);
        lShowHide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(lHide.getVisibility() == View.GONE){
                    rlMap.setVisibility(View.VISIBLE);
                    lHide.setVisibility(View.VISIBLE);
                    lShow.setVisibility(View.GONE);
                }else{
                    rlMap.setVisibility(View.GONE);
                    lHide.setVisibility(View.GONE);
                    lShow.setVisibility(View.VISIBLE);
                }
            }
        });

        String strEst =  Encrypts.decrypt(GlobalHelper.getFileContent(Encrypts.encrypt(GlobalHelper.SELECTED_ESTATE)));
        Gson gson = new Gson();
        estate = (Estate) gson.fromJson(strEst, Estate.class);

////        set di baseActivity
//        createLocationListener();

        toolBarSetup();

        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            String SqcMutuBuah = intent.getExtras().getString("qcMutuBuah");
            if (SqcMutuBuah != null) {
                qcMutuBuah = gson.fromJson(SqcMutuBuah, QcMutuBuah.class);
            }
        }
        main();
    }

    @Override
    public void onStart() {
        super.onStart();
        if(mGoogleApiClient != null) {
            if (!mGoogleApiClient.isConnected()) {
                mGoogleApiClient.connect();
            }
        }
    }

    private void main() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_container, TphInputFragment.getInstance())
                .commit();
    }

    private void toolBarSetup() {
        getSupportActionBar().hide();
        ImageView imgBack = (ImageView) findViewById(R.id.toolbar_icon);
        TextView textToolbar = (TextView) findViewById(R.id.toolbar_text);
        imgBack.setVisibility(View.VISIBLE);
        imgBack.setImageDrawable(new IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_arrow_left)
                .colorRes(R.color.White)
                .sizeDp(24));
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backProses();
            }
        });
        textToolbar.setText(getResources().getString(R.string.qc_mutu_buah));
    }

    public void backProses() {
        if (searchingGPS != null) {
            searchingGPS.dismiss();
        }
        setResult(GlobalHelper.RESULT_QC_MUTU_BUAH);
        finish();
    }

    private void createLocationListener() {
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

//        searchingGPS = Snackbar.make(myCoordinatorLayout,getResources().getString(R.string.search_gps),Snackbar.LENGTH_INDEFINITE);
//        searchingGPS.show();
        WidgetHelper.warningFindGps(this,myCoordinatorLayout);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1,
                1f, mLocationListener);
    }

//    @Override
//    protected void onNewIntent(Intent intent) {
//        setIntent(intent);
//        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
//            myTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
//            Log.i("tagNFC", GlobalHelper.bytesToHexString(myTag.getId()));
//            Log.i("tagNFC", Arrays.toString(myTag.getTechList()));
//        }
//        valueNFC = NfcHelper.readFromIntent(intent);
//        int formatNFC = NfcHelper.cekFormatNFC(valueNFC);
//        if (valueNFC != null) {
//            if (formatNFC != GlobalHelper.TYPE_NFC_SALAH) {
//                if(valueNFC.length() == 5) {
//                    if(NfcHelper.stat != NfcHelper.NFC_TIDAK_BISA_WRITE){
//                        if (getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof TphInputFragment) {
//                            TphInputFragment fragment = (TphInputFragment) getSupportFragmentManager().findFragmentById(R.id.content_container);
//                            fragment.startLongOperation(NfcHelper.stat);
//                        }
//                    }else{
//                        Toast.makeText(this, getResources().getString(R.string.nfc_empty) + " " + GlobalHelper.LIST_NFC[formatNFC], Toast.LENGTH_SHORT).show();
//                    }
//                }else{
//                    cekDataPassing(valueNFC, this);
//                }
//                TYPE_NFC = formatNFC;
//            } else {
//                Toast.makeText(HarvestApp.getContext(), HarvestApp.getContext().getResources().getString(R.string.format_not_valid) + " 999", Toast.LENGTH_SHORT).show();
//            }
//        }else {
//            Toast.makeText(this,R.string.format_not_valid,Toast.LENGTH_SHORT).show();
//        }
//    }

    @Override
    public void onBackPressed() {
        backProses();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(mGoogleApiClient != null) {
            if (!mGoogleApiClient.isConnected()) {
                mGoogleApiClient.reconnect();
            }
        }
        NfcHelper.WriteModeOn(nfcAdapter,this,pendingIntent,writeTagFilters);
    }

    @Override
    public void onPause(){
        super.onPause();
        if(mGoogleApiClient != null) {
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            }
        }
        NfcHelper.WriteModeOff(nfcAdapter,this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if(mGoogleApiClient != null) {
            if (mGoogleApiClient.isConnected()) {
                mGoogleApiClient.disconnect();
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        WidgetHelper.warningFindGps(this,myCoordinatorLayout);
        starLocationUpdate();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(QcMutuBuahActivity.class.getName(), "GoogleAPIClient connection suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(this, REQUEST_RESOLVE_ERROR);
            } catch (IntentSender.SendIntentException e) {
                // Error with resolution, try again
                mGoogleApiClient.connect();
            }
        }else{
            WidgetHelper.showSnackBar(myCoordinatorLayout,"onConnectionFailed");
//            searchingGPS = Snackbar.make(myCoordinatorLayout,"",Snackbar.LENGTH_INDEFINITE);
//            searchingGPS.show();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        locationChangeListener(location);
    }
}
