package id.co.ams_plantation.harvestqcsap.TableView.view_model;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import id.co.ams_plantation.harvestqcsap.TableView.model.TableViewCell;
import id.co.ams_plantation.harvestqcsap.TableView.model.TableViewColumnHeader;
import id.co.ams_plantation.harvestqcsap.TableView.model.TableViewRowHeader;
import id.co.ams_plantation.harvestqcsap.model.HeaderTableTransaksi;
import id.co.ams_plantation.harvestqcsap.model.ModelTphFragment;
import id.co.ams_plantation.harvestqcsap.util.TransaksiPanenHelper;

/**
 * Created by user on 12/24/2018.
 */

public class TPHTableViewModel {
    private Context mContext;
    private ArrayList<ModelTphFragment> modelTphFragments;
//    String [] HeaderColumn = {"Block","Pemanen"};
    ArrayList<HeaderTableTransaksi> headerTableTransaksis;


    public TPHTableViewModel(Context context, ArrayList<ModelTphFragment> hTransaksiPanens, ArrayList<HeaderTableTransaksi> aHeaderTableTransaksis) {
        mContext = context;
        modelTphFragments = new ArrayList<>();
        modelTphFragments = hTransaksiPanens;
        headerTableTransaksis = aHeaderTableTransaksis;
    }

    private List<TableViewRowHeader> getSimpleRowHeaderList() {
        List<TableViewRowHeader> list = new ArrayList<>();
        for (int i = 0; i < modelTphFragments.size(); i++) {
            String id = i + "_" + modelTphFragments.get(i).getOph() + "_"+ modelTphFragments.get(i).getTransaksiPanen().getStatus();
            TableViewRowHeader header = new TableViewRowHeader(id, modelTphFragments.get(i).getOph().substring(0,5) + "-" + modelTphFragments.get(i).getOph().substring(modelTphFragments.get(i).getOph().length() - 1));
            list.add(header);
        }

        return list;
    }

//    /**
//     * This is a dummy model list test some cases.
//     */
//    public static List<TableViewRowHeader> getSimpleRowHeaderList(int startIndex) {
//        List<TableViewRowHeader> list = new ArrayList<>();
//        for (int i = 0; i < transaksiPanens.size(); i++) {
//            TableViewRowHeader header = new TableViewRowHeader(String.valueOf(i), "row " + (startIndex + i));
//            list.add(header);
//        }
//
//        return list;
//    }


//    private List<TableViewColumnHeader> getSimpleColumnHeaderList() {
//        List<TableViewColumnHeader> list = new ArrayList<>();
//
//        for (int i = 0; i < COLUMN_SIZE; i++) {
//            String title = "column " + i;
//            if (i % 6 == 2) {
//                title = "large column " + i;
//            } else if (i == MOOD_COLUMN_INDEX) {
//                title = "mood";
//            } else if (i == GENDER_COLUMN_INDEX) {
//                title = "gender";
//            }
//            TableViewColumnHeader header = new TableViewColumnHeader(String.valueOf(i), title);
//            list.add(header);
//        }
//
//        return list;
//    }

    /**
     * This is a dummy model list test some cases.
     */
    private List<TableViewColumnHeader> getRandomColumnHeaderList() {
        List<TableViewColumnHeader> list = new ArrayList<>();

        for (int i = 0; i < headerTableTransaksis.size(); i++) {
            TableViewColumnHeader header = new TableViewColumnHeader(String.valueOf(i), headerTableTransaksis.get(i).getViewAtribut());
            list.add(header);
        }

        return list;
    }

//    private List<List<TableViewCell>> getSimpleCellList() {
//        List<List<TableViewCell>> list = new ArrayList<>();
//        for (int i = 0; i < ROW_SIZE; i++) {
//            List<TableViewCell> cellList = new ArrayList<>();
//
//            for (int j = 0; j < COLUMN_SIZE; j++) {
//                String text = "cell " + j + " " + i;
//                if (j % 4 == 0 && i % 5 == 0) {
//                    text = "large cell " + j + " " + i + ".";
//                }
//                String id = j + "-" + i;
//
//                TableViewCell cell = new TableViewCell(id, text);
//                cellList.add(cell);
//            }
//            list.add(cellList);
//        }
//
//        return list;
//    }

    /**
     * This is a dummy model list test some cases.
     */
    private List<List<TableViewCell>> getCellListForSortingTest() {
        List<List<TableViewCell>> list = new ArrayList<>();
        for (int i = 0; i < modelTphFragments.size(); i++) {
            List<TableViewCell> cellList = new ArrayList<>();
            for (int j = 0; j < headerTableTransaksis.size(); j++) {
                // Create dummy id.
                String id = j + "_" + modelTphFragments.get(i).getOph() + "_"+ modelTphFragments.get(i).getTransaksiPanen().getStatus();

                TableViewCell cell = null;
                String isiCell = TransaksiPanenHelper.getValuleForTable(headerTableTransaksis.get(j), modelTphFragments.get(i));
                cell = new TableViewCell(id, isiCell);
                cell.setColor(modelTphFragments.get(i).getColor());
                cellList.add(cell);
            }
            list.add(cellList);
        }

        return list;
    }

//    /**
//     * This is a dummy model list test some cases.
//     */
//    private List<List<TableViewCell>> getRandomCellList() {
//        List<List<TableViewCell>> list = new ArrayList<>();
//        for (int i = 0; i < ROW_SIZE; i++) {
//            List<TableViewCell> cellList = new ArrayList<>();
//            list.add(cellList);
//            for (int j = 0; j < COLUMN_SIZE; j++) {
//                String text = "cell " + j + " " + i;
//                int random = new Random().nextInt();
//                if (random % 2 == 0 || random % 5 == 0 || random == j) {
//                    text = "large cell  " + j + " " + i + getRandomString() + ".";
//                }
//
//                // Create dummy id.
//                String id = j + "-" + i;
//
//                TableViewCell cell = new TableViewCell(id, text);
//                cellList.add(cell);
//            }
//        }
//
//        return list;
//    }
//
//    /**
//     * This is a dummy model list test some cases.
//     */
//    private List<List<TableViewCell>> getEmptyCellList() {
//        List<List<TableViewCell>> list = new ArrayList<>();
//        for (int i = 0; i < ROW_SIZE; i++) {
//            List<TableViewCell> cellList = new ArrayList<>();
//            list.add(cellList);
//            for (int j = 0; j < COLUMN_SIZE; j++) {
//
//                // Create dummy id.
//                String id = j + "-" + i;
//
//                TableViewCell cell = new TableViewCell(id, "");
//                cellList.add(cell);
//            }
//        }
//
//        return list;
//    }
//
//    private List<TableViewRowHeader> getEmptyRowHeaderList() {
//        List<TableViewRowHeader> list = new ArrayList<>();
//        for (int i = 0; i < ROW_SIZE; i++) {
//            TableViewRowHeader header = new TableViewRowHeader(String.valueOf(i), "");
//            list.add(header);
//        }
//
//        return list;
//    }
//
//    /**
//     * This is a dummy model list test some cases.
//     */
//    public static List<List<TableViewCell>> getRandomCellList(int startIndex) {
//        List<List<TableViewCell>> list = new ArrayList<>();
//        for (int i = 0; i < ROW_SIZE; i++) {
//            List<Cell> cellList = new ArrayList<>();
//            list.add(cellList);
//            for (int j = 0; j < COLUMN_SIZE; j++) {
//                String text = "cell " + j + " " + (i + startIndex);
//                int random = new Random().nextInt();
//                if (random % 2 == 0 || random % 5 == 0 || random == j) {
//                    text = "large cell  " + j + " " + (i + startIndex) + getRandomString() + ".";
//                }
//
//                String id = j + "-" + (i + startIndex);
//
//                Cell cell = new Cell(id, text);
//                cellList.add(cell);
//            }
//        }
//
//        return list;
//    }


    private static String getRandomString() {
        Random r = new Random();
        String str = " a ";
        for (int i = 0; i < r.nextInt(); i++) {
            str = str + " a ";
        }

        return str;
    }

//    public Drawable getDrawable(int value, boolean isGender) {
//        if (isGender) {
//            return value == BOY ? mBoyDrawable : mGirlDrawable;
//        } else {
//            return value == SAD ? mSadDrawable : mHappyDrawable;
//        }
//    }

    public List<List<TableViewCell>> getCellList() {
        return getCellListForSortingTest();
    }

    public List<TableViewRowHeader> getRowHeaderList() {
        return getSimpleRowHeaderList();
    }

    public List<TableViewColumnHeader> getColumnHeaderList() {
        return getRandomColumnHeaderList();
    }

}