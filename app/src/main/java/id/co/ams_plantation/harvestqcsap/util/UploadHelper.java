package id.co.ams_plantation.harvestqcsap.util;

import static id.co.ams_plantation.harvestqcsap.connection.Tag.PostQcAncak;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;
import com.weiwangcn.betterspinner.library.BetterSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import co.ceryle.segmentedbutton.SegmentedButtonGroup;
import id.co.ams_plantation.harvestqcsap.HarvestApp;
import id.co.ams_plantation.harvestqcsap.connection.ServiceRequest;
import id.co.ams_plantation.harvestqcsap.connection.Tag;
import id.co.ams_plantation.harvestqcsap.model.Estate;
import id.co.ams_plantation.harvestqcsap.model.QcAncak;
import id.co.ams_plantation.harvestqcsap.model.QcAncakPohon;
import id.co.ams_plantation.harvestqcsap.model.TransaksiPanen;
import id.co.ams_plantation.harvestqcsap.ui.MainMenuActivity;
import id.co.ams_plantation.harvestqcsap.ui.MapActivity;
import id.co.ams_plantation.harvestqcsap.Fragment.SettingFragment;
import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.model.LastSyncTime;
import id.co.ams_plantation.harvestqcsap.model.QcMutuBuah;
import id.co.ams_plantation.harvestqcsap.model.SupervisionList;
import id.co.ams_plantation.harvestqcsap.model.SyncHistroy;
import id.co.ams_plantation.harvestqcsap.model.SyncHistroyItem;
import id.co.ams_plantation.harvestqcsap.model.SyncTime;
import id.co.ams_plantation.harvestqcsap.model.SyncTransaksi;
import id.co.ams_plantation.harvestqcsap.ui.ActiveActivity;
import id.co.ams_plantation.harvestqcsap.ui.BaseActivity;
import ng.max.slideview.SlideView;


public class UploadHelper {

    Context context;
    MapActivity mapActivity;
    SettingFragment settingFragment;
    MainMenuActivity mainMenuActivity;
    ActiveActivity activeActivity;
    AlertDialog alertDialog;
    AlertDialog alertDialog2;
    AlertDialog alertDialogPilihJaringan;
    Snackbar snackbar;
    SyncHistroy syncHistroy;

    Long uploadDataTime;
    Long syncDataTransaksi;
    Long syncDataMaster;
    LastSyncTime lastSyncTime;

    public UploadHelper(Context context) {
        this.context = context;
    }

    public void SelectUploadSync(){
        if(context instanceof MapActivity){
            mapActivity = ((MapActivity) context);
        }else if (context instanceof MainMenuActivity){
            mainMenuActivity = ((MainMenuActivity) context);
            Fragment fragment = ((MainMenuActivity.TabPagerAdapter) ((MainMenuActivity)context).viewPager.getAdapter()).getItem(((MainMenuActivity)context).viewPager.getCurrentItem());
            if(fragment instanceof SettingFragment) {
                settingFragment = ((SettingFragment) fragment);
            }
        }

        View baseView = LayoutInflater.from(context).inflate(R.layout.upload_layout,null);
        TextView tvLastSinkron = (TextView) baseView.findViewById(R.id.tv_last_sinkron);
        TextView tvComment = (TextView) baseView.findViewById(R.id.tvComment);
        TextView tvUpload = (TextView) baseView.findViewById(R.id.tv_upload);
        LinearLayout btnUploadSinkron = (LinearLayout) baseView.findViewById(R.id.btn_upload_sinkron);
        LinearLayout btnSinkronOnly = (LinearLayout) baseView.findViewById(R.id.btn_sinkron_only);
        ImageView ivUploadSinkron = (ImageView) baseView.findViewById(R.id.iv_upload_sinkron);
        ImageView ivSinkronOnly = (ImageView) baseView.findViewById(R.id.iv_sinkron_only);
//        final RadioButton rdLokal = (RadioButton) baseView.findViewById(R.id.rd_use_lokal);
//        RadioButton rdPublik = (RadioButton) baseView.findViewById(R.id.rd_use_public);

        ivUploadSinkron.setImageDrawable(new IconicsDrawable(context)
                .icon(MaterialDesignIconic.Icon.gmi_upload)
                .colorRes(R.color.Gray)
        );
        ivSinkronOnly.setImageDrawable(new IconicsDrawable(context)
                .icon(MaterialDesignIconic.Icon.gmi_refresh_sync)
                .colorRes(R.color.Gray)
        );

        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECT_BUFFER_SERVER, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        Boolean connectBufferServer;
        if(NetworkHelper.getIPAddress(true).startsWith("192.168.0")){
            editor.putBoolean(HarvestApp.CONNECT_BUFFER_SERVER,true);
            connectBufferServer = true;
        }else{
            editor.putBoolean(HarvestApp.CONNECT_BUFFER_SERVER,false);
            connectBufferServer =false;
        }
        editor.apply();

        String lastSync = null;
        if(mapActivity != null) {
            if(mapActivity.setUpDataSyncHelper != null) {
                lastSync = mapActivity.setUpDataSyncHelper.getLastSync();
            }
        } else if (settingFragment != null) {
            if (settingFragment.setUpDataSyncHelper != null) {
                lastSync = settingFragment.setUpDataSyncHelper.getLastSync();
            }
        }
        if(lastSync != null){
            long timeLastSinkron = System.currentTimeMillis();
            try {
                JSONObject object = new JSONObject(lastSync);
                timeLastSinkron = object.getLong("Time");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd-MMMM-yyyy HH:mm");
            Date dateLastSinkron = new Date(timeLastSinkron);
            Calendar calendarNow = Calendar.getInstance();
            calendarNow.setTime(new Date(System.currentTimeMillis()-dateLastSinkron.getTime()));
            String tglLastSync = sdf.format(dateLastSinkron);
            SpannableString spanLastSinkron = new SpannableString("Terakhir sinkronisasi : "+tglLastSync);
            spanLastSinkron.setSpan(new ForegroundColorSpan(Color.BLUE),
                    spanLastSinkron.toString().indexOf(tglLastSync),
                    spanLastSinkron.toString().indexOf(tglLastSync)+tglLastSync.length(),
                    Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            tvComment.setVisibility(View.GONE);
            btnUploadSinkron.setEnabled(true);
            btnUploadSinkron.setVisibility(View.VISIBLE);
            tvLastSinkron.setVisibility(View.VISIBLE);
            tvLastSinkron.setText(spanLastSinkron);
        }else{
//            tvLastSinkron.setText("Terakhir sinkronisasi : belum sinkronisasi");
//            tvLastSinkron.setVisibility(View.GONE);
//            tvComment.setVisibility(View.VISIBLE);
        }

        SharedPreferences prefer = HarvestApp.getContext().getSharedPreferences(HarvestApp.SYNC_TIME, Context.MODE_PRIVATE);
        String sync_time = prefer.getString(HarvestApp.SYNC_TIME,null);
        Gson gson = new Gson();
        lastSyncTime = gson.fromJson(sync_time,LastSyncTime.class);
        syncHistroy = SyncHistoryHelper.getSyncHistory();
        if(syncHistroy != null){
            if(syncHistroy.getAction().equalsIgnoreCase(SyncHistroy.Action_Sync)){
                SyncHistoryHelper.insertHistorySync(SyncHistroy.Sync_Berhasil);
                syncHistroy = null;
//                if(lastSyncTime == null){
//                    btnUploadSinkron.setVisibility(View.GONE);
//                }if(lastSyncTime.getSyncTimeHashMap() == null){
//                    btnUploadSinkron.setVisibility(View.GONE);
//                }else if(lastSyncTime.getSyncTimeHashMap().get(SyncTime.SYNC_DATA_MASTER).isSelected()) {
//                    btnUploadSinkron.setVisibility(View.GONE);
//                }
            }else if (syncHistroy.getAction().equalsIgnoreCase(SyncHistroy.Action_Upload)) {
                boolean syncOnlyHide = true;
                lastSyncTime = gson.fromJson(sync_time, LastSyncTime.class);
                if(lastSyncTime != null) {
                    if (lastSyncTime.getSyncTimeHashMap().size() > 0) {
                        for (SyncTime syncTime : lastSyncTime.getSyncTimeHashMap().values()) {
                            if (syncTime.getNamaListCekBox().equalsIgnoreCase(SyncTime.UPLOAD_TRANSAKSI)) {
                                syncOnlyHide = !syncTime.isFinish();
                                break;
                            }
                        }
                    }
                }else{
                    lastSyncTime = new LastSyncTime();
                }
                if (syncOnlyHide) {
                    btnSinkronOnly.setVisibility(View.GONE);
                }else{
                    btnSinkronOnly.setVisibility(View.VISIBLE);
                }
            }
        }

        if(settingFragment != null){
            if(settingFragment.adaDataTransaksi) {
                btnSinkronOnly.setVisibility(View.GONE);
            }
        }

        btnUploadSinkron.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String messagePilihJaringan = "Silahkan pilih jaringan untuk upload!";
                ServiceRequest serviceRequest = new ServiceRequest();

                View view = LayoutInflater.from(context).inflate(R.layout.public_network_sync_dialog,null);
                TextView tvKet1PilihJaringan = (TextView) view.findViewById(R.id.ket1);
                SlideView btnJaringanPublik= (SlideView) view.findViewById(R.id.btnJaringanPublik);
                SlideView btnJaringanLokal = (SlideView) view.findViewById(R.id.btnJaringanKantor);

                if(connectBufferServer){
                    btnJaringanLokal.setText(context.getResources().getString(R.string.wifi_hms));
                }else{
                    btnJaringanLokal.setText(context.getResources().getString(R.string.wifi_kebun));
                }

                tvKet1PilihJaringan.setText(messagePilihJaringan);
                btnJaringanLokal.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
                    @Override
                    public void onSlideComplete(SlideView slideView) {
                        alertDialogPilihJaringan.dismiss();

                        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean(HarvestApp.CONNECTION_PREF,false);
                        editor.apply();
                        String message = "Apakah anda yakin untuk melakukan upload ? Data terbaru anda akan disimpan ke server!";
                        String redString = "Data terbaru anda akan disimpan ke server!";
                        SpannableString spannableMessage = new SpannableString(message);
                        spannableMessage.setSpan(new ForegroundColorSpan(Color.RED),message.indexOf(redString),message.indexOf(redString)+redString.length(),Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                        spannableMessage.setSpan(new StyleSpan(Typeface.BOLD),message.indexOf(redString),message.indexOf(redString)+redString.length(),Spanned.SPAN_INCLUSIVE_INCLUSIVE);

                        View view = LayoutInflater.from(context).inflate(R.layout.sync_dialog,null);
                        TextView tvket1 = (TextView) view.findViewById(R.id.ket1);
                        SlideView btnCancel= (SlideView) view.findViewById(R.id.btnCancel);
                        SlideView btnOk = (SlideView) view.findViewById(R.id.btnOK);

                        tvket1.setText(spannableMessage);
                        btnCancel.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
                            @Override
                            public void onSlideComplete(SlideView slideView) {
                                alertDialog2.dismiss();
                            }
                        });
                        btnOk.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
                            @Override
                            public void onSlideComplete(SlideView slideView) {
                                alertDialog2.dismiss();
                                alertDialog.dismiss();
                                if(mapActivity != null){
                                    if(syncHistroy == null) {
                                        syncHistroy = SyncHistoryHelper.creaetNewSyncHistory(SyncHistroy.Sync_From_MapActivity, SyncHistroy.Action_Upload);
                                    }
                                    ceklistUploadnDownload(true);
                                }else if(settingFragment!= null){
                                    if(syncHistroy == null) {
                                        syncHistroy = SyncHistoryHelper.creaetNewSyncHistory(SyncHistroy.Sync_From_Setting, SyncHistroy.Action_Upload);
                                    }
                                    if(settingFragment.segmentedButtonGroup != null) {
                                        settingFragment.segmentedButtonGroup.setPosition(SettingFragment.ButtonGroup_Lokal);
                                    }
                                    ceklistUploadnDownload(true);
                                }
                            }
                        });
                        alertDialog2 = WidgetHelper.showFormDialog(alertDialog2,view,context);
                    }
                });

                btnJaringanPublik.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
                    @Override
                    public void onSlideComplete(SlideView slideView) {
                        alertDialogPilihJaringan.dismiss();

                        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean(HarvestApp.CONNECTION_PREF,true);
                        editor.apply();

                        String message = "Apakah anda yakin untuk melakukan upload ? Data terbaru anda akan disimpan ke server!";
                        String redString = "Data terbaru anda akan disimpan ke server!";
                        SpannableString spannableMessage = new SpannableString(message);
                        spannableMessage.setSpan(new ForegroundColorSpan(Color.RED),message.indexOf(redString),message.indexOf(redString)+redString.length(),Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                        spannableMessage.setSpan(new StyleSpan(Typeface.BOLD),message.indexOf(redString),message.indexOf(redString)+redString.length(),Spanned.SPAN_INCLUSIVE_INCLUSIVE);

                        View view = LayoutInflater.from(context).inflate(R.layout.sync_dialog,null);
                        TextView tvket1 = (TextView) view.findViewById(R.id.ket1);
                        SlideView btnCancel= (SlideView) view.findViewById(R.id.btnCancel);
                        SlideView btnOk = (SlideView) view.findViewById(R.id.btnOK);

                        tvket1.setText(spannableMessage);
                        btnCancel.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
                            @Override
                            public void onSlideComplete(SlideView slideView) {
                                alertDialog2.dismiss();
                            }
                        });
                        btnOk.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
                            @Override
                            public void onSlideComplete(SlideView slideView) {
                                alertDialog2.dismiss();
                                alertDialog.dismiss();

                                if(mapActivity != null){
                                    if(syncHistroy == null) {
                                        syncHistroy = SyncHistoryHelper.creaetNewSyncHistory(SyncHistroy.Sync_From_MapActivity, SyncHistroy.Action_Upload);
                                    }
                                    ceklistUploadnDownload(true);
                                }else if(settingFragment!= null){
                                    if(syncHistroy == null) {
                                        syncHistroy = SyncHistoryHelper.creaetNewSyncHistory(SyncHistroy.Sync_From_Setting, SyncHistroy.Action_Upload);
                                    }
                                    if(settingFragment.segmentedButtonGroup != null) {
                                        settingFragment.segmentedButtonGroup.setPosition(SettingFragment.ButtonGroup_Public);
                                    }
                                    ceklistUploadnDownload(true);
                                }
                            }
                        });
                        alertDialog2 = WidgetHelper.showFormDialog(alertDialog2,view,context);
                    }
                });
                alertDialogPilihJaringan = WidgetHelper.showFormDialog(alertDialogPilihJaringan,view,context);
                alertDialogPilihJaringan.setCancelable(false);



            }
        });
        btnSinkronOnly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String messagePilihJaringan = "Silahkan pilih jaringan untuk sinkronisasi!";
                ServiceRequest serviceRequest = new ServiceRequest();

                View view = LayoutInflater.from(context).inflate(R.layout.public_network_sync_dialog,null);
                TextView tvKet1PilihJaringan = (TextView) view.findViewById(R.id.ket1);
                SlideView btnJaringanPublik= (SlideView) view.findViewById(R.id.btnJaringanPublik);
                SlideView btnJaringanLokal = (SlideView) view.findViewById(R.id.btnJaringanKantor);

                if(connectBufferServer){
                    btnJaringanLokal.setText(context.getResources().getString(R.string.wifi_hms));
                }else{
                    btnJaringanLokal.setText(context.getResources().getString(R.string.wifi_kebun));
                }
                tvKet1PilihJaringan.setText(messagePilihJaringan);

                btnJaringanLokal.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
                    @Override
                    public void onSlideComplete(SlideView slideView) {
                        alertDialogPilihJaringan.dismiss();

                        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean(HarvestApp.CONNECTION_PREF,false);
                        editor.apply();

                        String message = "Apakah anda yakin untuk melakukan sinkronisasi ? Data hasil input yang belum diupload akan terhapus!";
                        String redString = "Data hasil input yang belum diupload akan terhapus!";
                        SpannableString spannableMessage = new SpannableString(message);
                        spannableMessage.setSpan(new ForegroundColorSpan(Color.RED),message.indexOf(redString),message.indexOf(redString)+redString.length(),Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        spannableMessage.setSpan(new StyleSpan(Typeface.BOLD),message.indexOf(redString),message.indexOf(redString)+redString.length(),Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                        View view = LayoutInflater.from(context).inflate(R.layout.sync_dialog,null);
                        TextView tvket1 = (TextView) view.findViewById(R.id.ket1);
                        SlideView btnCancel= (SlideView) view.findViewById(R.id.btnCancel);
                        SlideView btnOk = (SlideView) view.findViewById(R.id.btnOK);

                        tvket1.setText(spannableMessage);
                        btnCancel.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
                            @Override
                            public void onSlideComplete(SlideView slideView) {
                                alertDialog2.dismiss();
                            }
                        });
                        btnOk.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
                            @Override
                            public void onSlideComplete(SlideView slideView) {
                                alertDialog2.dismiss();
                                alertDialog.dismiss();
                                if(mapActivity != null){
                                    if(syncHistroy == null) {
                                        syncHistroy = SyncHistoryHelper.creaetNewSyncHistory(SyncHistroy.Sync_From_MapActivity, SyncHistroy.Action_Sync);
                                    }
                                    ceklistUploadnDownload(false);
                                }else if(settingFragment!= null){
                                    if(syncHistroy == null) {
                                        syncHistroy = SyncHistoryHelper.creaetNewSyncHistory(SyncHistroy.Sync_From_Setting, SyncHistroy.Action_Sync);
                                    }
                                    if(settingFragment.segmentedButtonGroup != null) {
                                        settingFragment.segmentedButtonGroup.setPosition(SettingFragment.ButtonGroup_Lokal);
                                    }
                                    ceklistUploadnDownload(false);
                                }
                            }
                        });
                        alertDialog2 = WidgetHelper.showFormDialog(alertDialog2,view,context);
                    }
                });
                btnJaringanPublik.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
                    @Override
                    public void onSlideComplete(SlideView slideView) {
                        alertDialogPilihJaringan.dismiss();

                        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean(HarvestApp.CONNECTION_PREF,true);
                        editor.apply();
                        String message = "Apakah anda yakin untuk melakukan sinkronisasi ? Data hasil input yang belum diupload akan terhapus!";
                        String redString = "Data hasil input yang belum diupload akan terhapus!";
                        SpannableString spannableMessage = new SpannableString(message);
                        spannableMessage.setSpan(new ForegroundColorSpan(Color.RED),message.indexOf(redString),message.indexOf(redString)+redString.length(),Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                        spannableMessage.setSpan(new StyleSpan(Typeface.BOLD),message.indexOf(redString),message.indexOf(redString)+redString.length(),Spanned.SPAN_INCLUSIVE_INCLUSIVE);

                        View view = LayoutInflater.from(context).inflate(R.layout.sync_dialog,null);
                        TextView tvket1 = (TextView) view.findViewById(R.id.ket1);
                        SlideView btnCancel= (SlideView) view.findViewById(R.id.btnCancel);
                        SlideView btnOk = (SlideView) view.findViewById(R.id.btnOK);

                        tvket1.setText(spannableMessage);
                        btnCancel.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
                            @Override
                            public void onSlideComplete(SlideView slideView) {
                                alertDialog2.dismiss();
                            }
                        });
                        btnOk.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
                            @Override
                            public void onSlideComplete(SlideView slideView) {
                                alertDialog2.dismiss();
                                alertDialog.dismiss();
                                if(mapActivity != null){
                                    if(syncHistroy == null) {
                                        syncHistroy = SyncHistoryHelper.creaetNewSyncHistory(SyncHistroy.Sync_From_MapActivity, SyncHistroy.Action_Sync);
                                    }
                                    ceklistUploadnDownload(false);
                                }else if(settingFragment!= null){
                                    if(syncHistroy == null) {
                                        syncHistroy = SyncHistoryHelper.creaetNewSyncHistory(SyncHistroy.Sync_From_Setting, SyncHistroy.Action_Sync);
                                    }
                                    if(settingFragment.segmentedButtonGroup != null) {
                                        settingFragment.segmentedButtonGroup.setPosition(SettingFragment.ButtonGroup_Public);
                                    }
                                    ceklistUploadnDownload(false);
                                }
                            }
                        });
                        alertDialog2 = WidgetHelper.showFormDialog(alertDialog2,view,context);
                    }
                });
                alertDialogPilihJaringan = WidgetHelper.showFormDialog(alertDialogPilihJaringan,view,context);
                alertDialogPilihJaringan.setCancelable(false);
            }
        });

        alertDialog = WidgetHelper.showFormDialog(alertDialog,baseView,context);
    }

    public void ceklistUploadnDownload(boolean withUplaod){

        if(context instanceof MapActivity){
            mapActivity = ((MapActivity) context);
        }else if (context instanceof MainMenuActivity){
            mainMenuActivity = ((MainMenuActivity) context);
            Fragment fragment = ((MainMenuActivity.TabPagerAdapter) ((MainMenuActivity)context).viewPager.getAdapter()).getItem(((MainMenuActivity)context).viewPager.getCurrentItem());
            if(fragment instanceof SettingFragment) {
                settingFragment = ((SettingFragment) fragment);
            }
        }else if (context instanceof ActiveActivity){
            activeActivity = ((ActiveActivity) context);
        }

        View baseView = LayoutInflater.from(context).inflate(R.layout.ceklist_upload_download,null);
        CardView ll_upload_data = (CardView) baseView.findViewById(R.id.ll_upload_data);
        TextView tv_upload_data_time = (TextView) baseView.findViewById(R.id.tv_upload_data_time);
        TextView tv_sync_data_transaksi_date_time = (TextView) baseView.findViewById(R.id.tv_sync_data_transaksi_date_time);
        TextView tv_sync_data_master_date_time = (TextView) baseView.findViewById(R.id.tv_sync_data_master_date_time);
        TextView tv_sync_group_estate = (TextView) baseView.findViewById(R.id.tv_sync_group_estate);
        CheckBox cb_upload_data = (CheckBox) baseView.findViewById(R.id.cb_upload_data);
        CheckBox cb_sync_data_transaksi = (CheckBox) baseView.findViewById(R.id.cb_sync_data_transaksi);
        CheckBox cb_sync_data_master = (CheckBox) baseView.findViewById(R.id.cb_sync_data_master);
        ImageView iv_cb_upload_data = (ImageView) baseView.findViewById(R.id.iv_cb_upload_data);
        ImageView iv_cb_sync_data_transaksi = (ImageView) baseView.findViewById(R.id.iv_cb_sync_data_transaksi);
        ImageView iv_cb_sync_data_master = (ImageView) baseView.findViewById(R.id.iv_cb_sync_data_master);
//        BetterSpinner bsAfdeling = (BetterSpinner) baseView.findViewById(R.id.bsAfdeling);
//        SegmentedButtonGroup segmentedInputData = (SegmentedButtonGroup) baseView.findViewById(R.id.segmentedInputData);
        SegmentedButtonGroup segmentedGroupEstate = (SegmentedButtonGroup) baseView.findViewById(R.id.segmentedGroupEstate);
        Button btnOk = (Button) baseView.findViewById(R.id.btnOk);
        Button btnClose = (Button) baseView.findViewById(R.id.btnCancel);

        uploadDataTime = System.currentTimeMillis();
        syncDataTransaksi = System.currentTimeMillis();
        syncDataMaster = System.currentTimeMillis();

        lastSyncTime = null;

        SharedPreferences prefer = HarvestApp.getContext().getSharedPreferences(HarvestApp.SYNC_TIME, Context.MODE_PRIVATE);
        String sync_time = prefer.getString(HarvestApp.SYNC_TIME,null);
        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.AFDELING_BLOCK, Context.MODE_PRIVATE);
        String afdelingObj = preferences.getString(HarvestApp.AFDELING_BLOCK,null);
        SharedPreferences afdelingPref = HarvestApp.getContext().getSharedPreferences(HarvestApp.SYNC_TRANSAKSI, Context.MODE_PRIVATE);
        String syncafdelingS = afdelingPref.getString(HarvestApp.SYNC_TRANSAKSI,null);
        SyncTransaksi syncTransaksi = new SyncTransaksi();
        if(syncafdelingS != null){
            Gson gson = new Gson();
            syncTransaksi = gson.fromJson(syncafdelingS,SyncTransaksi.class);
        }
        if(!withUplaod){
            ll_upload_data.setVisibility(View.GONE);
        }else{
            cb_upload_data.setChecked(true);
        }

        if(mainMenuActivity != null || activeActivity != null){
            int positionSelected = 0;
            List<String> lAfd = new ArrayList<>();
//            lAfd.add(FilterHelper.No_Filter_Afdeling);
            try {
//                JSONObject objAfdeling = new JSONObject(afdelingObj);
//                for(int i = 0; i<objAfdeling.names().length(); i++){
//                    lAfd.add(objAfdeling.names().get(i).toString());
//                    if(syncTransaksi.getAfdeling()!= null){
//                        if(syncTransaksi.getAfdeling().equalsIgnoreCase(objAfdeling.names().get(i).toString())){
//                            positionSelected = i;
//                        }
//                    }
//                }

                if(syncTransaksi.getEstates() == null){
                    segmentedGroupEstate.setPosition(1);
                }else if (syncTransaksi.getEstates().size() == 1){
                    segmentedGroupEstate.setPosition(1);
                }else{
                    segmentedGroupEstate.setPosition(0);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

//            ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
//                    android.R.layout.simple_dropdown_item_1line, lAfd);
//            bsAfdeling.setAdapter(adapter);
//            bsAfdeling.setText(lAfd.get(positionSelected));
        }else{
            SharedPreferences.Editor editorAfdelingPref = afdelingPref.edit();
            editorAfdelingPref.remove(HarvestApp.SYNC_TRANSAKSI);
//            bsAfdeling.setVisibility(View.GONE);
//            segmentedInputData.setVisibility(View.GONE);
        }

        cb_upload_data.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!isChecked && withUplaod){
                    cb_upload_data.setChecked(true);
                }
            }
        });

        cb_sync_data_master.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(sync_time == null){
                    cb_sync_data_master.setChecked(true);
                }else {
                    if (!isChecked) {
                        SyncTime syncTime = lastSyncTime.getSyncTimeHashMap().get(SyncTime.SYNC_DATA_MASTER);
                        if(!syncTime.isFinish()){
                            cb_sync_data_master.setChecked(true);
                        }
                    }
                }
            }
        });

        cb_sync_data_transaksi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if(sync_time == null){
//                    cb_sync_data_transaksi.setChecked(true);
//                }else {
//                    if (!isChecked) {
                        //SyncTime syncTime = lastSyncTime.getSyncTimeHashMap().get(SyncTime.SYNC_DATA_TRANSAKSI);
//                        if(!syncTime.isFinish()){
                            cb_sync_data_transaksi.setChecked(isChecked);
//                        }
//                    }
//                }
            }
        });

        if(sync_time == null){
            tv_upload_data_time.setText("");
            tv_sync_data_transaksi_date_time.setText("");
            tv_sync_data_master_date_time.setText("");
        }else{
            SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd-MMMM-yyyy HH:mm");
            Gson gson = new Gson();
            lastSyncTime = gson.fromJson(sync_time,LastSyncTime.class);
            if(lastSyncTime.getSyncTimeHashMap().size() > 0) {
                for (SyncTime syncTime : lastSyncTime.getSyncTimeHashMap().values()) {
                    if (syncTime.getNamaListCekBox().equalsIgnoreCase(SyncTime.UPLOAD_TRANSAKSI)) {
                        tv_upload_data_time.setText(sdf.format(syncTime.getTimeSync()));
                        cb_upload_data.setChecked(syncTime.isSelected());
                        iv_cb_upload_data.setImageDrawable(new
                                IconicsDrawable(context)
                                .icon(syncTime.isFinish() ? MaterialDesignIconic.Icon.gmi_check_circle : MaterialDesignIconic.Icon.gmi_close_circle)
                                .sizeDp(20)
                                .colorRes(syncTime.isFinish() ? R.color.Green : R.color.Red));
                        uploadDataTime = syncTime.getTimeSync();
                    } else if (syncTime.getNamaListCekBox().equalsIgnoreCase(SyncTime.SYNC_DATA_MASTER)) {
                        tv_sync_data_master_date_time.setText(sdf.format(syncTime.getTimeSync()));
                        cb_sync_data_master.setChecked(syncTime.isSelected());
                        iv_cb_sync_data_master.setImageDrawable(new
                                IconicsDrawable(context)
                                .icon(syncTime.isFinish() ? MaterialDesignIconic.Icon.gmi_check_circle : MaterialDesignIconic.Icon.gmi_close_circle)
                                .sizeDp(20)
                                .colorRes(syncTime.isFinish() ? R.color.Green : R.color.Red));
                        syncDataMaster = syncTime.getTimeSync();
                    } else if (syncTime.getNamaListCekBox().equalsIgnoreCase(SyncTime.SYNC_DATA_TRANSAKSI)) {
                        tv_sync_data_transaksi_date_time.setText(sdf.format(syncTime.getTimeSync()));
                        cb_sync_data_transaksi.setChecked(syncTime.isSelected());
                        iv_cb_sync_data_transaksi.setImageDrawable(new
                                IconicsDrawable(context)
                                .icon(syncTime.isFinish() ? MaterialDesignIconic.Icon.gmi_check_circle : MaterialDesignIconic.Icon.gmi_close_circle)
                                .sizeDp(20)
                                .colorRes(syncTime.isFinish() ? R.color.Green : R.color.Red));
                        syncDataTransaksi = syncTime.getTimeSync();
                    }
                }
            }else{
                tv_upload_data_time.setText("");
                tv_sync_data_transaksi_date_time.setText("");
                tv_sync_data_master_date_time.setText("");
            }

        }

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!cb_upload_data.isChecked() && !cb_sync_data_transaksi.isChecked() && !cb_sync_data_master.isChecked()) {
                    Toast.makeText(HarvestApp.getContext(),"Mohon Pilih Salah Satu",Toast.LENGTH_SHORT).show();
                    return;
                }

                HashMap<String,SyncTime> syncTimeHashMap = new HashMap<>();
                if (cb_upload_data.isChecked() && ll_upload_data.getVisibility() == View.VISIBLE) {
                    SyncTime syncTime = new SyncTime(SyncTime.UPLOAD_TRANSAKSI,System.currentTimeMillis(),true);
                    syncTimeHashMap.put(syncTime.getNamaListCekBox(),syncTime);
                } else {
                    if (lastSyncTime == null) {
                        SyncTime syncTime = new SyncTime(SyncTime.UPLOAD_TRANSAKSI, uploadDataTime,false,true);
                        syncTimeHashMap.put(syncTime.getNamaListCekBox(),syncTime);
                        Log.d(this.getClass() + " upload_data ",syncTime.toString());
                    } else {
                        SyncTime syncTime = lastSyncTime.getSyncTimeHashMap().get(SyncTime.UPLOAD_TRANSAKSI);
                        syncTimeHashMap.put(syncTime.getNamaListCekBox(), syncTime);
                        Log.d(this.getClass() + " upload_data ",syncTime.toString());
                    }
                }

                if (cb_sync_data_transaksi.isChecked()) {
                    SyncTime syncTime = new SyncTime(SyncTime.SYNC_DATA_TRANSAKSI,System.currentTimeMillis(),true);
                    syncTimeHashMap.put(syncTime.getNamaListCekBox(),syncTime);
                } else {
                    if (lastSyncTime == null) {
                        SyncTime syncTime = new SyncTime(SyncTime.SYNC_DATA_TRANSAKSI, syncDataTransaksi, false, true);
                        syncTimeHashMap.put(syncTime.getNamaListCekBox(), syncTime);
                        Log.d(this.getClass() + " data_transaksi ",syncTime.toString());
                    } else {
                        SyncTime syncTime = lastSyncTime.getSyncTimeHashMap().get(SyncTime.SYNC_DATA_TRANSAKSI);
                        syncTime.setSelected(false);
                        syncTimeHashMap.put(syncTime.getNamaListCekBox(), syncTime);
                        Log.d(this.getClass() + " data_transaksi ",syncTime.toString());
                    }
                }

                if (cb_sync_data_master.isChecked()) {
                    SyncTime syncTime = new SyncTime(SyncTime.SYNC_DATA_MASTER,System.currentTimeMillis(),true);
                    syncTimeHashMap.put(syncTime.getNamaListCekBox(),syncTime);
                } else {
                    if (lastSyncTime == null) {
                        SyncTime syncTime = new SyncTime(SyncTime.SYNC_DATA_MASTER,syncDataMaster,false,true);
                        syncTimeHashMap.put(syncTime.getNamaListCekBox(),syncTime);
                        Log.d(this.getClass() + " data_master ",syncTime.toString());
                    } else {
                        SyncTime syncTime = lastSyncTime.getSyncTimeHashMap().get(SyncTime.SYNC_DATA_MASTER);
                        syncTimeHashMap.put(syncTime.getNamaListCekBox(), syncTime);
                        Log.d(this.getClass() + " data_master ",syncTime.toString());
                    }
                }

                LastSyncTime lastSyncTime1 = new LastSyncTime(GlobalHelper.getEstate(),syncTimeHashMap);
                Gson gson = new Gson();
                SharedPreferences.Editor editor = prefer.edit();
                editor.putString(HarvestApp.SYNC_TIME,gson.toJson(lastSyncTime1));
                editor.apply();

                if (cb_upload_data.isChecked() && ll_upload_data.getVisibility() == View.VISIBLE) {
                    Log.d(this.getClass().toString(),"upload Dulu");
                    if(mainMenuActivity != null){
                        SharedPreferences.Editor editorAfdelingPref = afdelingPref.edit();
                        String syncTransaksi = setSyncTransaksi(segmentedGroupEstate.getPosition());
                        if(syncTransaksi == null){
                            editorAfdelingPref.remove(HarvestApp.SYNC_TRANSAKSI);
                            ArrayList<Estate> estates = new ArrayList<>();
                            estates.add(GlobalHelper.getEstate());
                            mainMenuActivity.estatesMapping = estates;
                            mainMenuActivity.idxEstate = 0;
                        }else {
                            editorAfdelingPref.putString(HarvestApp.SYNC_TRANSAKSI,syncTransaksi);
                            SyncTransaksi syncTrans = gson.fromJson(syncTransaksi,SyncTransaksi.class);
                            if(syncTrans.getEstates().size() > 0) {
                                mainMenuActivity.estatesMapping = syncTrans.getEstates();
                            }else{
                                ArrayList<Estate> estates = new ArrayList<>();
                                estates.add(GlobalHelper.getEstate());
                                mainMenuActivity.estatesMapping = estates;
                            }
                            mainMenuActivity.idxEstate = 0;
                        }
                        editorAfdelingPref.apply();

                        settingFragment.startLongOperation(SettingFragment.LongOperation_Upload_QcBuah);
                    }
                }else{
                    Log.d(this.getClass().toString(),"langsung Sync");
                    if(mainMenuActivity != null){
                        SharedPreferences.Editor editorAfdelingPref = afdelingPref.edit();
                        String syncTransaksi = setSyncTransaksi(segmentedGroupEstate.getPosition());
                        if(syncTransaksi == null){
                            editorAfdelingPref.remove(HarvestApp.SYNC_TRANSAKSI);
                            ArrayList<Estate> estates = new ArrayList<>();
                            estates.add(GlobalHelper.getEstate());
                            mainMenuActivity.estatesMapping = estates;
                            mainMenuActivity.idxEstate = 0;
                        }else {
                            editorAfdelingPref.putString(HarvestApp.SYNC_TRANSAKSI,syncTransaksi);
                            SyncTransaksi syncTrans = gson.fromJson(syncTransaksi,SyncTransaksi.class);
                            if(syncTrans.getEstates().size() > 0) {
                                mainMenuActivity.estatesMapping = syncTrans.getEstates();
                            }else{
                                ArrayList<Estate> estates = new ArrayList<>();
                                estates.add(GlobalHelper.getEstate());
                                mainMenuActivity.estatesMapping = estates;
                            }
                            mainMenuActivity.idxEstate = 0;
                        }
                        editorAfdelingPref.apply();

                        settingFragment.startLongOperation(SettingFragment.LongOperation_BackUp_HMS);
                    }else if (activeActivity != null){
                        SharedPreferences.Editor editorAfdelingPref = afdelingPref.edit();
                        String syncTransaksi = setSyncTransaksi(segmentedGroupEstate.getPosition());
                        if(syncTransaksi == null){
                            editorAfdelingPref.remove(HarvestApp.SYNC_TRANSAKSI);
                            ArrayList<Estate> estates = new ArrayList<>();
                            estates.add(GlobalHelper.getEstate());
                            activeActivity.estatesMapping = estates;
                            activeActivity.idxEstate = 0;
                        }else {
                            editorAfdelingPref.putString(HarvestApp.SYNC_TRANSAKSI,syncTransaksi);
                            SyncTransaksi syncTrans = gson.fromJson(syncTransaksi,SyncTransaksi.class);
                            if(syncTrans.getEstates().size() > 0) {
                                activeActivity.estatesMapping = syncTrans.getEstates();
                            }else{
                                ArrayList<Estate> estates = new ArrayList<>();
                                estates.add(GlobalHelper.getEstate());
                                activeActivity.estatesMapping = estates;
                            }
                            activeActivity.idxEstate = 0;
                        }
                        editorAfdelingPref.apply();

                        activeActivity.startLongOperation(activeActivity.LongOperation_BackUpHMS);
                    }

                }
                alertDialog.dismiss();
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog = WidgetHelper.showFormDialog(alertDialog,baseView,context);
    }

    public static String setSyncTransaksi(int positionEstate){
        SyncTransaksi syncTransaksi = new SyncTransaksi();

        if(positionEstate == 1){
            ArrayList<Estate> estates = new ArrayList<>();
            estates.add(GlobalHelper.getEstate());
            syncTransaksi.setEstates(estates);
        }else{
            HashMap<String,Estate> hEstateMaping= GlobalHelper.getEstateMapping();
            ArrayList<Estate> estates = new ArrayList<>();
            if(hEstateMaping.size() > 0 ) {
                for (Estate estate : hEstateMaping.values()) {
                    estates.add(estate);
                }
            }
            syncTransaksi.setEstates(estates);
        }
        Gson gson = new Gson();
        return gson.toJson(syncTransaksi);
    }

    public void uploadImages(Object longOperation, Tag tag, JSONArray jsonArray, int idx){
        JSONArray array = new JSONArray();
        try {
            HashMap<String,JSONObject> hashMapArray = new HashMap<>();
//            array.put(jsonArray.get(idx));
            ((BaseActivity) context).idxUpload = idx;
            String text = "Upload Images ";
            Log.d(this.getClass()+" "+ text+" "+tag,String.valueOf(idx));

            int partitionUpload = GlobalHelper.PARTITION_UPLOAD;
            //cek index jarray
            JSONArray jArrayPreUpload = new JSONArray();
            for(int x = 0 ; x < partitionUpload;x++) {
                int index = idx + x;
                if (index < jsonArray.length()) {
                    JSONObject jObj = (JSONObject) jsonArray.get(index);
                    if (jObj.getInt("idx") == index) {
                        jArrayPreUpload.put(jObj);
                    } else {
                        for (int a = 0 ; a < jsonArray.length(); a++) {
                            JSONObject objX = (JSONObject) jsonArray.get(a);
                            if (objX.getInt("idx") == index) {
                                jArrayPreUpload.put(objX);
                                break;
                            }
                        }
                    }
                }
            }

            //set up prepare upload
            for (int a = 0 ; a < jArrayPreUpload.length(); a++){
                JSONObject objPreUpload = (JSONObject) jArrayPreUpload.get(a);

                switch (tag){
                    case PostQcBuahImages:{
                        Gson gson = new Gson();
                        QcMutuBuah qcMutuBuah = gson.fromJson(objPreUpload.toString(), QcMutuBuah.class);
                        TransaksiPanen transaksiPanen = qcMutuBuah.getTransaksiPanenQc();

                        JSONObject objImagePanen = new JSONObject();
                        JSONArray imagePanen = new JSONArray();
                        if (transaksiPanen.getFoto() != null) {
                            for (String foto : transaksiPanen.getFoto()) {
                                JSONObject objImage = new JSONObject();
                                File file = new File(foto);
                                if (file.exists() && !file.getAbsolutePath().contains("http:")) {
                                    objImage.put("filename", file.getName());
                                    objImage.put("filesize", file.getTotalSpace());
                                    objImage.put("content", GlobalHelper.getEncoded64ImageStringFromBitmap(file.getAbsolutePath()));
                                    objImage.put("metadata", GlobalHelper.getMetaDataFromImages(file.getAbsolutePath()));
                                    imagePanen.put(objImage);
                                }

                            }
                            objImagePanen.put("idQCMutuBuah", qcMutuBuah.getIdQCMutuBuah());
                            objImagePanen.put("estCode", transaksiPanen.getTph().getEstCode());
                            objImagePanen.put("photoBy", transaksiPanen.getUpdateBy());
                            objImagePanen.put("images", imagePanen);
                            hashMapArray.put(qcMutuBuah.getIdQCMutuBuah(),objImagePanen);
//                            array.put(objImagePanen);
                        }
                        break;
                    }
                    case PostSensusBjrImages:{
                        JSONArray imageBjr = objPreUpload.getJSONArray("foto");

                        JSONObject objImagePanen = new JSONObject();
                        JSONArray imagePanen = new JSONArray();
                        if (imageBjr != null) {
                            for (int i = 0 ;i < imageBjr.length();i++) {
                                JSONObject objImage = new JSONObject();
                                File file = new File(imageBjr.get(i).toString());
                                if (file.exists() && !file.getAbsolutePath().contains("http:")) {
                                    objImage.put("filename", file.getName());
                                    objImage.put("filesize", file.getTotalSpace());
                                    objImage.put("content", GlobalHelper.getEncoded64ImageStringFromBitmap(file.getAbsolutePath()));
                                    objImage.put("metadata", GlobalHelper.getMetaDataFromImages(file.getAbsolutePath()));
                                    imagePanen.put(objImage);
                                }

                            }

                            JSONObject objTph = objPreUpload.getJSONObject("tph");
                            objImagePanen.put("idTPanen", objPreUpload.getString("idTPanen"));
                            objImagePanen.put("estCode", objTph.getString("estCode"));
                            objImagePanen.put("photoBy", objPreUpload.getString("createBy"));
                            objImagePanen.put("images", imagePanen);

                            hashMapArray.put(objPreUpload.getString("idTPanen"),objImagePanen);
//                            array.put(objImagePanen);
                        }
                        break;
                    }
                    case PostQcAncakImages:{

                        Gson gson = new Gson();
                        QcAncak qcAncak = gson.fromJson(objPreUpload.toString(), QcAncak.class);

                        for(int i = 0 ; i < qcAncak.getQcAncakPohons().size(); i++){
                            JSONObject objImageAncak = new JSONObject();
                            JSONArray imageAncak = new JSONArray();
                            QcAncakPohon qcAncakPohon = qcAncak.getQcAncakPohons().get(i);
                            if(qcAncakPohon.getFoto() != null) {
                                if (qcAncakPohon.getFoto().size() > 0) {
                                    boolean masuk = false;
                                    for (String foto : qcAncakPohon.getFoto()) {
                                        JSONObject objImage = new JSONObject();
                                        File file = new File(foto);
                                        if (file.exists() && !file.getAbsolutePath().contains("http:")) {
                                            objImage.put("filename", file.getName());
                                            objImage.put("filesize", file.getTotalSpace());
                                            objImage.put("content", GlobalHelper.getEncoded64ImageStringFromBitmap(file.getAbsolutePath()));
                                            objImage.put("metadata", GlobalHelper.getMetaDataFromImages(file.getAbsolutePath()));
                                            imageAncak.put(objImage);
                                            masuk = true;
                                        }
                                    }
                                    if (masuk && hashMapArray.size() < GlobalHelper.PARTITION_UPLOAD_IMAGE) {
                                        objImageAncak.put("idQcAncakPohon", qcAncakPohon.getIdQcAncakPohon());
                                        objImageAncak.put("estCode", qcAncak.getTph().getEstCode());
                                        objImageAncak.put("photoBy", qcAncak.getCreateBy());
                                        objImageAncak.put("images", imageAncak);

                                        hashMapArray.put(qcAncakPohon.getIdQcAncakPohon(), objImageAncak);
                                    }
//                                array.put(objImageAncak);
                                }
                            }
                        }
                        break;
                    }
                }
            }

            if(hashMapArray.size() > 0){
                for(JSONObject object : hashMapArray.values()){
                    array.put(object);
                }
            }

            switch (tag) {
                case PostQcBuahImages: {
                    text += "Qc Buah Images";
                    if (context instanceof MainMenuActivity) {
                        ((MainMenuActivity) context).connectionPresenter.UploadQcBuahImages(array);
                    }
                    break;
                }
                case PostSensusBjrImages:{
                    text += "Sensus Bjr Images";
                    if (context instanceof MainMenuActivity) {
                        ((MainMenuActivity) context).connectionPresenter.UploadSensusBJRImages(array);
                    }
                    break;
                }
                case PostQcAncakImages:{
                    text += "Qc Ancak Images";
                    if (context instanceof MainMenuActivity) {
                        ((MainMenuActivity) context).connectionPresenter.UploadQcAncakImages(array);
                    }
                    break;
                }
            }

            SyncHistroyItem item = new SyncHistroyItem(tag,idx,jsonArray.length());
            SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

            JSONObject objProgres = new JSONObject();
            objProgres.put("ket",text);
            objProgres.put("persen",(idx*100/jsonArray.length()));
            objProgres.put("count",idx);
            objProgres.put("total",(jsonArray.length()));
            boolean fromLongOperation = false;
            if(longOperation instanceof SettingFragment.LongOperation){
//                ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                fromLongOperation = true;
            }else if(longOperation instanceof ActiveActivity.LongOperation){
//                ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                fromLongOperation = true;
            }else if(longOperation instanceof MapActivity.LongOperation){
//                ((MapActivity.LongOperation) longOperation).publishProgress(objProgres);
                fromLongOperation = true;
            }

            if(!fromLongOperation){
                if(settingFragment != null){
                    mainMenuActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateProgres(objProgres,settingFragment.myCoordinatorLayout);
                        }
                    });
                }else if (mapActivity != null){
                    mapActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateProgres(objProgres,mapActivity.coor);
                        }
                    });
                }
            }

            if(idx == jsonArray.length() - 1){
                if(snackbar != null) {
                    snackbar.dismiss();
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void uploadData(Object longOperation,Tag tag,JSONArray jsonArray, int idx){
        JSONArray array = new JSONArray();
        ((BaseActivity) context).idxUpload = idx;
        String text = "Upload Data ";
        Log.d(this.getClass()+" "+ text+" "+tag,String.valueOf(idx));

        int partitionUpload = GlobalHelper.PARTITION_UPLOAD;
        switch (tag){
            case PostQcAncak:{
                partitionUpload = GlobalHelper.PARTITION_UPLOAD_ANCAK;
                break;
            }
        }
        int idxData = 0;
        try {
            PackageInfo pi = context.getPackageManager().getPackageInfo(context.getPackageName(),0);
            String userId = GlobalHelper.getUser().getUserID();
            for(int x = 0 ; x < partitionUpload;x++) {
                int index = idx + x;
                idxData = index;
                if (index < jsonArray.length()) {
                    JSONObject jObj = (JSONObject) jsonArray.get(index);
                    if (jObj.getInt("idx") == index) {
                        Log.d(this.getClass() + " "+ tag,String.valueOf(index));
                        jObj.put("userId",userId);
                        jObj.put("versionApp",pi.versionName);
                        array.put(jObj);
                    }else {
                        for (int a = 0; a < jsonArray.length(); a++) {
                            JSONObject obj = (JSONObject) jsonArray.get(a);
                            if (obj.getInt("idx") == index) {
                                Log.d(this.getClass() + " " + tag, String.valueOf(index));
                                jObj.put("userId",userId);
                                jObj.put("versionApp",pi.versionName);
                                array.put(obj);
                                break;
                            }
                        }
                    }
                }else{
                    break;
                }
            }

            switch (tag){
                case PostQcBuah: {
                    text += "Qc Buah Images";
                    if (context instanceof MainMenuActivity) {
                        ((MainMenuActivity) context).connectionPresenter.UploadQcBuah(array);
                    }
                    break;
                }
                case PostSensusBjr:{
                    text += "Sensus Bjr";
                    if (context instanceof MainMenuActivity) {
                        ((MainMenuActivity) context).connectionPresenter.UploadSensusBjr(array);
                    }
                    break;
                }
                case PostQcAncak:{
                    text += "Qc Ancak";
                    if (context instanceof MainMenuActivity) {
                        ((MainMenuActivity) context).connectionPresenter.UploadQcAncak(array);
                    }
                    break;
                }
                case PostSessionQcFotoPanen:{
                    text += "Qc Foto Panen";
                    if (context instanceof MainMenuActivity) {
                        ((MainMenuActivity) context).connectionPresenter.UploadSessionQcFotoPanen(array);
                    }
                    break;
                }
            }

            JSONObject objProgres = new JSONObject();
            objProgres.put("ket",text);
            objProgres.put("persen",(idx*100/jsonArray.length()));
            objProgres.put("count",idx);
            objProgres.put("total",(jsonArray.length()));
            boolean fromLongOperation = false;
            if(longOperation instanceof SettingFragment.LongOperation){
//                ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                fromLongOperation = true;
            }else if(longOperation instanceof ActiveActivity.LongOperation){
//                ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                fromLongOperation = true;
            }else if(longOperation instanceof MapActivity.LongOperation){
//                ((MapActivity.LongOperation) longOperation).publishProgress(objProgres);
                fromLongOperation = true;
            }

            if(!fromLongOperation){
                if(settingFragment != null){
                    mainMenuActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateProgres(objProgres,settingFragment.myCoordinatorLayout);
                        }
                    });
                }else if (mapActivity != null){
                    mapActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateProgres(objProgres,mapActivity.coor);
                        }
                    });
                }
            }

            if(idx == jsonArray.length() - 1){
                if(snackbar != null) {
                    snackbar.dismiss();
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void listTransaksiDone(Object longOperation,Tag tag,JSONArray jsonArray, int idx){
        try {
            String idString = "";
            int index = 0;
            String text = "Upload Data ";

            int partitionUpload = GlobalHelper.PARTITION_UPLOAD;
            switch (tag){
                case PostQcAncak:{
                    partitionUpload = GlobalHelper.PARTITION_UPLOAD_ANCAK;
                    break;
                }
            }
            for(int x = 0 ; x < partitionUpload;x++) {
                index = idx + x;
                if (index < jsonArray.length()) {
                    JSONObject jObj = (JSONObject) jsonArray.get(index);
                    if (jObj.getInt("idx") == index) {
                        Log.d(this.getClass() + " listTransaksiDone "+tag,"idxLangsungDapat");

                        JSONObject object = transaksiListDone(tag,jObj);
                        if(object != null) {
                            if(object.has("idString")) {
                                idString += object.getString("idString");
                                text = object.getString("text");
                            }
                        }
                    }else {
                        Log.d(this.getClass() + " listTransaksiDone "+tag,"idxLooping Dahulu");

                        for (int a = 0; a < jsonArray.length(); a++) {
                            JSONObject obj = (JSONObject) jsonArray.get(a);
                            if (obj.getInt("idx") == index) {

                                JSONObject object = transaksiListDone(tag,obj);
                                if(object != null) {
                                    if(object.has("idString")) {
                                        idString += object.getString("idString");
                                        text = object.getString("text");
                                    }
                                }
                                break;
                            }
                        }
                    }
                }else{
                    index --;
                    break;
                }
            }

            String estCode = GlobalHelper.getEstate().getEstCode();
            if(settingFragment != null){
                estCode = mainMenuActivity.estatesMapping.get(mainMenuActivity.idxEstate).getEstCode();
            }

            //update status menjadi upload
            switch(tag){
                case PostQcBuah:
                    SetUpDataSyncHelper.updateStatusUploadQcBuah(idString,estCode);
//                    GlobalHelper.setIdUpload(GlobalHelper.LIST_FOLDER_QC_MUTU_BUAH,idString);
                    break;
                case PostSensusBjr:
                    SetUpDataSyncHelper.updateStatusUploadSensusBjr(idString,estCode);
//                    GlobalHelper.setIdUpload(GlobalHelper.LIST_FOLDER_QC_SENSUS_BJR,idString);
                    break;
                case PostQcAncak:
                    SetUpDataSyncHelper.updateStatusUploadQcAncak(idString,estCode);
//                    GlobalHelper.setIdUpload(GlobalHelper.LIST_FOLDER_QC_MUTU_ANCAK,idString);
                    break;
                case PostSessionQcFotoPanen:
                    SetUpDataSyncHelper.updateStatusUploadSessionQcFotoPanen(idString,estCode);
//                    GlobalHelper.setIdUpload(GlobalHelper.LIST_FOLDER_QC_MUTU_ANCAK,idString);
                    break;
            }

            int persen = index + 1;
            JSONObject objProgres = new JSONObject();
            objProgres.put("ket",text);
            objProgres.put("persen",(persen*100/jsonArray.length()));
            objProgres.put("count",persen);
            objProgres.put("total",(jsonArray.length()));
            boolean fromLongOperation = false;
            if(longOperation instanceof SettingFragment.LongOperation){
//                ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                fromLongOperation = true;
            }else if(longOperation instanceof ActiveActivity.LongOperation){
//                ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                fromLongOperation = true;
            }else if(longOperation instanceof MapActivity.LongOperation){
//                ((MapActivity.LongOperation) longOperation).publishProgress(objProgres);
                fromLongOperation = true;
            }

            SyncHistroyItem item = new SyncHistroyItem(tag,persen,jsonArray.length());
            SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

            if(!fromLongOperation){
                if(settingFragment != null){
                    mainMenuActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateProgres(objProgres,settingFragment.myCoordinatorLayout);
                        }
                    });
                }else if (mapActivity != null){
                    mapActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateProgres(objProgres,mapActivity.coor);
                        }
                    });
                }
            }
            ((BaseActivity) context).idxUpload = index;
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public JSONObject transaksiListDone(Tag tag,JSONObject obj){
        JSONObject object = new JSONObject();
        try {
            switch (tag) {
                case PostQcBuah:
                    object.put("text", "Upload Data Qc Buah");
                    object.put("idString", obj.getString("idQCMutuBuah") + ";");
                    break;
                case PostSensusBjr:
                    object.put("text", "Upload Data Sensus BJR");
                    object.put("idString", obj.getString("idQCSensusBJR") + ";");
                    break;
                case PostQcAncak:
                    object.put("text", "Upload Data Qc Ancak");
                    object.put("idString", obj.getString("idQcAncak") + ";");
                    break;
                case PostSessionQcFotoPanen:
                    object.put("text", "Upload Data Session Qc Foto Panen");
                    object.put("idString", obj.getString("idSession") + ";");
                    break;
            }
            return  object;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void updateProgres(JSONObject objProgres, CoordinatorLayout coor){
        try {
            if(snackbar== null){
                snackbar = Snackbar.make(coor,objProgres.getString("ket"),Snackbar.LENGTH_INDEFINITE);
            }
            snackbar.setText(String.format(objProgres.getString("ket") + " %d %s [%d/%d] ",
                    objProgres.getInt("persen"), "%", objProgres.getInt("count"), objProgres.getInt("total")));
            snackbar.show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
