package id.co.ams_plantation.harvestqcsap.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import id.co.ams_plantation.harvestqcsap.HarvestApp;
import id.co.ams_plantation.harvestqcsap.connection.ServiceRequest;
import id.co.ams_plantation.harvestqcsap.connection.ServiceResponse;
import id.co.ams_plantation.harvestqcsap.encryptor.Encrypts;
import id.co.ams_plantation.harvestqcsap.presenter.ConnectionPresenter;
import id.co.ams_plantation.harvestqcsap.util.SyncHistoryHelper;
import id.co.ams_plantation.harvestqcsap.util.UploadHelper;
import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.model.LastSyncTime;
import id.co.ams_plantation.harvestqcsap.model.SyncHistroy;
import id.co.ams_plantation.harvestqcsap.model.SyncTime;
import id.co.ams_plantation.harvestqcsap.util.GlobalHelper;
import id.co.ams_plantation.harvestqcsap.util.NetworkHelper;
import id.co.ams_plantation.harvestqcsap.util.SetUpDataSyncHelper;
import id.co.ams_plantation.harvestqcsap.util.SyncTimeHelper;
import id.co.ams_plantation.harvestqcsap.util.WidgetHelper;
import id.co.ams_plantation.harvestqcsap.view.ApiView;
import ir.mahdi.mzip.zip.ZipArchive;
import ng.max.slideview.SlideView;
import pl.droidsonroids.gif.GifImageView;

public class ActiveActivity extends BaseActivity implements ApiView {

    int LongOperation;
    final int LongOperation_GetMasterUserByEstate = 0;
    final int LongOperation_GetPemanenListByEstate = 1;
    final int LongOperation_GetTphListByEstate = 2;
    final int LongOperation_GetQCQuestionAnswer = 3;
    public final int LongOperation_BackUpHMS = 8;
    final int LongOperation_GetApplicationConfiguration = 12;
    final int LongOperation_GetEstateMapping = 13;
    final int LongOperation_RestoreHMSTemp = 16;
    final int LongOperation_GetQcBuahByEstate = 17;
    final int LongOperation_GetSensusBJRByEstate = 18;
    final int LongOperation_GetQcAncakByEstate = 19;
    final int LongOperation_GetMasterSPKByEstate= 20;
    final int LongOperation_GetSessionQcFotoPanen = 21;

    private ConnectionPresenter presenter;
    Button btnSynchronize;
    ImageView ivUserEntry;
    ImageView ivCompanyEntry;
    TextView tvUserEntry;
    TextView tvCompanyEntry;
    TextView keterangan_entry;
    GifImageView giv_loader;
    CoordinatorLayout myCoordinatorLayout;
    SetUpDataSyncHelper setUpDataSyncHelper;
    AlertDialog alertDialogPilihJaringan;
    ServiceResponse responseApi;

    public AlertDialog progressDialog;
    UploadHelper uploadHelper;

    boolean isOnFetchingProcess = false;
    @Override
    protected void initPresenter() {
        presenter = new ConnectionPresenter(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active);
        getSupportActionBar().hide();

        setUpDataSyncHelper = new SetUpDataSyncHelper(this);
        uploadHelper = new UploadHelper(this);

        btnSynchronize = (Button) findViewById(R.id.btnSynchronize);
        ivUserEntry = (ImageView) findViewById(R.id.iv_user_entry);
        ivCompanyEntry = (ImageView) findViewById(R.id.iv_company_entry);
        tvUserEntry = (TextView) findViewById(R.id.user_entry);
        tvCompanyEntry = (TextView) findViewById(R.id.company_entry);
        keterangan_entry = (TextView) findViewById(R.id.keterangan_entry);
        giv_loader = (GifImageView) findViewById(R.id.giv_loader);
        myCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.myCoordinatorLayout);

        ivUserEntry.setImageDrawable(
                new IconicsDrawable(this)
                        .icon(MaterialDesignIconic.Icon.gmi_account)
                        .sizeDp(24)
                        .colorRes(R.color.Gray)
        );

        ivCompanyEntry.setImageDrawable(
                new IconicsDrawable(this)
                        .icon(MaterialDesignIconic.Icon.gmi_home)
                        .sizeDp(24)
                        .colorRes(R.color.Gray)
        );

        tvUserEntry.setText(GlobalHelper.getUser().getUserFullName());
        tvCompanyEntry.setText(GlobalHelper.getEstate().getCompanyShortName() + " - "
                + GlobalHelper.getEstate().getEstCode() + " : "
                + GlobalHelper.getEstate().getEstName());

        btnSynchronize.setText(getResources().getString(R.string.active));
        btnSynchronize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String isi = GlobalHelper.readFileContent(GlobalHelper.getDatabasePathHMSTemp() + GlobalHelper.getCountBadResponseName());
                int idx = 0;
                if(!isi.isEmpty()){
                    idx = Integer.parseInt(isi);
                }
                if (idx > GlobalHelper.MAX_BAD_RESPONSE) {
                    showRestoreOrActive();
                } else {
                    showActive();
                }
            }
        });
    }

    public void showRestoreOrActive(){
        String message = "Silahkan pilih Restore Atau Sync Kembali";

        View view = LayoutInflater.from(ActiveActivity.this).inflate(R.layout.public_network_sync_dialog,null);
        TextView tvKet1PilihJaringan = (TextView) view.findViewById(R.id.ket1);
        TextView tvKet2PilihJaringan = (TextView) view.findViewById(R.id.ket2);
        SlideView btnJaringanPublik= (SlideView) view.findViewById(R.id.btnJaringanPublik);
        SlideView btnJaringanLokal = (SlideView) view.findViewById(R.id.btnJaringanKantor);
        tvKet1PilihJaringan.setText(message);
        tvKet2PilihJaringan.setText("Geser Untuk Restore Data Dari HMS Temp Atau Sync Kembali");
        btnJaringanPublik.setText("Sync");
        btnJaringanLokal.setText("Restore");

        isOnFetchingProcess = false;
        btnJaringanLokal.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(SlideView slideView) {
                isOnFetchingProcess = true;
                startLongOperation(LongOperation_RestoreHMSTemp);
                SyncHistoryHelper.insertHistorySync(SyncHistroy.Sync_Gagal);
                alertDialogPilihJaringan.dismiss();
            }
        });
        btnJaringanPublik.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(SlideView slideView) {
                isOnFetchingProcess = true;
                alertDialogPilihJaringan.dismiss();
                showActive();
            }
        });

        alertDialogPilihJaringan = WidgetHelper.showFormDialog(alertDialogPilihJaringan,view,ActiveActivity.this);
        alertDialogPilihJaringan.setCancelable(false);
        alertDialogPilihJaringan.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if(!isOnFetchingProcess){
                    if (progressDialog != null) {
                        if(progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                    }
                }
            }
        });
    }

    public void showActive(){

        String messagePilihJaringan = "Silahkan pilih jaringan untuk aktivasi!";
        ServiceRequest serviceRequest = new ServiceRequest();

        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECT_BUFFER_SERVER, MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        Boolean connectBufferServer;
        if(NetworkHelper.getIPAddress(true).startsWith("192.168.0")){
            editor.putBoolean(HarvestApp.CONNECT_BUFFER_SERVER,true);
            connectBufferServer = true;
        }else{
            editor.putBoolean(HarvestApp.CONNECT_BUFFER_SERVER,false);
            connectBufferServer =false;
        }
        editor.apply();

        View view = LayoutInflater.from(ActiveActivity.this).inflate(R.layout.public_network_sync_dialog,null);
        TextView tvKet1PilihJaringan = (TextView) view.findViewById(R.id.ket1);
        SlideView btnJaringanPublik= (SlideView) view.findViewById(R.id.btnJaringanPublik);
        SlideView btnJaringanLokal = (SlideView) view.findViewById(R.id.btnJaringanKantor);
        tvKet1PilihJaringan.setText(messagePilihJaringan);

        if(connectBufferServer){
            btnJaringanLokal.setText(getResources().getString(R.string.wifi_hms));
        }else{
            btnJaringanLokal.setText(getResources().getString(R.string.wifi_kebun));
        }

        isOnFetchingProcess = false;
        btnJaringanLokal.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(SlideView slideView) {
                isOnFetchingProcess = true;

                SyncHistroy syncHistroy = SyncHistoryHelper.getSyncHistory();
                if(syncHistroy != null) {
                    if (!syncHistroy.getSyncFrom().equalsIgnoreCase(SyncHistroy.Sync_From_Active)) {
                        SyncHistoryHelper.insertHistorySync(SyncHistroy.Sync_Gagal);
                        syncHistroy = SyncHistoryHelper.creaetNewSyncHistory(SyncHistroy.Sync_From_Active, SyncHistroy.Action_Sync);
                    }
                }else{
                    syncHistroy = SyncHistoryHelper.creaetNewSyncHistory(SyncHistroy.Sync_From_Active, SyncHistroy.Action_Sync);
                }

                SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean(HarvestApp.CONNECTION_PREF,false);
                editor.apply();

                SharedPreferences pref = HarvestApp.getContext().getSharedPreferences(HarvestApp.SYNC_TIME, MODE_PRIVATE);
                String sync_time = pref.getString(HarvestApp.SYNC_TIME,null);
                HashMap<String, SyncTime> syncTimeHashMap = new HashMap<>();
                if(sync_time != null) {
                    Gson gson = new Gson();
                    LastSyncTime lastSyncTime = gson.fromJson(sync_time,LastSyncTime.class);
                    if (lastSyncTime.getEstate().getEstCode().equals(GlobalHelper.getEstate().getEstCode())){
                        uploadHelper.ceklistUploadnDownload(false);
                        alertDialogPilihJaringan.dismiss();
                        return;
                    }else{
                        syncTimeHashMap.put(SyncTime.UPLOAD_TRANSAKSI,new SyncTime(SyncTime.UPLOAD_TRANSAKSI,System.currentTimeMillis(),false,true));
                        syncTimeHashMap.put(SyncTime.SYNC_DATA_MASTER,new SyncTime(SyncTime.SYNC_DATA_MASTER,System.currentTimeMillis(),true,false));
                        syncTimeHashMap.put(SyncTime.SYNC_DATA_TRANSAKSI,new SyncTime(SyncTime.SYNC_DATA_TRANSAKSI,System.currentTimeMillis(),true,false));
                    }
                }else{
                    syncTimeHashMap.put(SyncTime.UPLOAD_TRANSAKSI,new SyncTime(SyncTime.UPLOAD_TRANSAKSI,System.currentTimeMillis(),false,true));
                    syncTimeHashMap.put(SyncTime.SYNC_DATA_MASTER,new SyncTime(SyncTime.SYNC_DATA_MASTER,System.currentTimeMillis(),true,false));
                    syncTimeHashMap.put(SyncTime.SYNC_DATA_TRANSAKSI,new SyncTime(SyncTime.SYNC_DATA_TRANSAKSI,System.currentTimeMillis(),true,false));
                }

                LastSyncTime lastSyncTime = new LastSyncTime(GlobalHelper.getEstate(),syncTimeHashMap);
                Gson gson = new Gson();
                SharedPreferences.Editor edit = pref.edit();
                edit.putString(HarvestApp.SYNC_TIME,gson.toJson(lastSyncTime));
                edit.apply();

                startLongOperation(LongOperation_BackUpHMS);
                alertDialogPilihJaringan.dismiss();
            }
        });
        btnJaringanPublik.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(SlideView slideView) {
                isOnFetchingProcess = true;

                SyncHistroy syncHistroy = SyncHistoryHelper.getSyncHistory();
                if(syncHistroy != null) {
                    if (!syncHistroy.getSyncFrom().equalsIgnoreCase(SyncHistroy.Sync_From_Active)) {
                        SyncHistoryHelper.insertHistorySync(SyncHistroy.Sync_Gagal);
                        syncHistroy = SyncHistoryHelper.creaetNewSyncHistory(SyncHistroy.Sync_From_Active, SyncHistroy.Action_Sync);
                    }
                }else{
                    syncHistroy = SyncHistoryHelper.creaetNewSyncHistory(SyncHistroy.Sync_From_Active, SyncHistroy.Action_Sync);
                }

                SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean(HarvestApp.CONNECTION_PREF,true);
                editor.apply();

                SharedPreferences pref = HarvestApp.getContext().getSharedPreferences(HarvestApp.SYNC_TIME, MODE_PRIVATE);
                String sync_time = pref.getString(HarvestApp.SYNC_TIME,null);
                HashMap<String, SyncTime> syncTimeHashMap = new HashMap<>();
                if(sync_time != null) {
                    uploadHelper.ceklistUploadnDownload(false);
                    alertDialogPilihJaringan.dismiss();
                    return;
                }else{
                    syncTimeHashMap.put(SyncTime.UPLOAD_TRANSAKSI,new SyncTime(SyncTime.UPLOAD_TRANSAKSI,System.currentTimeMillis(),false,true));
                    syncTimeHashMap.put(SyncTime.SYNC_DATA_MASTER,new SyncTime(SyncTime.SYNC_DATA_MASTER,System.currentTimeMillis(),true,false));
                    syncTimeHashMap.put(SyncTime.SYNC_DATA_TRANSAKSI,new SyncTime(SyncTime.SYNC_DATA_TRANSAKSI,System.currentTimeMillis(),true,false));
                }

                LastSyncTime lastSyncTime = new LastSyncTime(GlobalHelper.getEstate(),syncTimeHashMap);
                Gson gson = new Gson();
                SharedPreferences.Editor edit = pref.edit();
                edit.putString(HarvestApp.SYNC_TIME,gson.toJson(lastSyncTime));
                edit.apply();

                startLongOperation(LongOperation_BackUpHMS);
                alertDialogPilihJaringan.dismiss();
            }
        });

        alertDialogPilihJaringan = WidgetHelper.showFormDialog(alertDialogPilihJaringan,view,ActiveActivity.this);
        alertDialogPilihJaringan.setCancelable(false);
        alertDialogPilihJaringan.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if(!isOnFetchingProcess){
                    if (progressDialog != null) {
                        if(progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                    }
                }
            }
        });
    }

    public void nextIntent(){
        Log.d("ACTIVE SYNC","nextIntent");
        try {
            JSONObject jModule = GlobalHelper.getModule();
            if(jModule == null){
                Toast.makeText(HarvestApp.getContext(),getResources().getString(R.string.cannot_acses),Toast.LENGTH_LONG).show();
                Intent intent = getPackageManager().getLaunchIntentForPackage("id.co.ams_plantation.amsadminapps");
//                if(BuildConfig.BUILD_VARIANT.equals("dev")){
//                    intent = getPackageManager().getLaunchIntentForPackage("id.co.ams_plantation.amsadminapps.dev");
//                }
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return;
            }
            if ((jModule.getString("mdlAccCode").equals("QHS3")) && (jModule.getString("subMdlAccCode").equals("P5"))){
                setIntent(ActiveActivity.this,MapActivity.class);
            }else {
                GlobalHelper.setUpAllData();
                setIntent(ActiveActivity.this, MainMenuActivity.class);
            }
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void successResponse(ServiceResponse serviceResponse) {
        switch (serviceResponse.getTag()){
            case GetMasterUserByEstate:
                responseApi = serviceResponse ;
                startLongOperation(LongOperation_GetMasterUserByEstate);
                break;
            case GetPemanenListByEstate:
                responseApi = serviceResponse ;
                startLongOperation(LongOperation_GetPemanenListByEstate);
                break;
            case GetQCQuestionAnswer:
                responseApi = serviceResponse ;
                startLongOperation(LongOperation_GetQCQuestionAnswer);
                Log.d("ACTIVE SYNC","10 a GetTphListByEstate "+ responseApi.getData() );
                break;
            case GetTphListByEstate:
                responseApi = serviceResponse ;
                startLongOperation(LongOperation_GetTphListByEstate);
                Log.d("ACTIVE SYNC","10 a GetTphListByEstate "+ responseApi.getData() );
                break;
            case GetQcBuahByEstate:
                responseApi = serviceResponse ;
                startLongOperation(LongOperation_GetQcBuahByEstate);
                break;
            case GetSensusBJRByEstate:
                responseApi = serviceResponse ;
                startLongOperation(LongOperation_GetSensusBJRByEstate);
                break;
            case GetQCMutuAncakByEstate:
                responseApi = serviceResponse ;
                startLongOperation(LongOperation_GetQcAncakByEstate);
                break;
            case GetSessionQcFotoPanen:
                responseApi = serviceResponse;
                startLongOperation(LongOperation_GetSessionQcFotoPanen);
                break;
            case GetApplicationConfiguration:
                responseApi = serviceResponse;
                startLongOperation(LongOperation_GetApplicationConfiguration);
                break;
            case GetEstateMapping:
                responseApi = serviceResponse;
                startLongOperation(LongOperation_GetEstateMapping);
                break;
            case GetMasterSPKByEstate:
                responseApi = serviceResponse ;
                startLongOperation(LongOperation_GetMasterSPKByEstate);
                break;
            case GetQCMutuAncakHeaderCounterByUserID:
                if(setUpDataSyncHelper.UpdateQCMutuAncakCounterByUserID(serviceResponse)){
                    presenter.GetQCBuahCounterByUserID();
                    progressDialog.dismiss();
                }else{
                    Toast.makeText(HarvestApp.getContext(),"Gagal UpdateQCMutuAncakCounterByUserID",Toast.LENGTH_SHORT).show();
                    //btnSynchronize.setProgress(false);
                    progressDialog.dismiss();
                }
                break;
            case GetQCBuahCounterByUserID:
                if(setUpDataSyncHelper.UpdateQCMutuBuahCounterByUserID(serviceResponse)){
                    if(setUpDataSyncHelper.setUpLastSync(myCoordinatorLayout)) {
                        SyncTimeHelper.updateFinishSyncTime(SyncTime.SYNC_DATA_MASTER);
                        if (!GlobalHelper.aktifKembali(GlobalHelper.getModule())){
                            nextIntent();
                        }
                        progressDialog.dismiss();
                    }else{
                        Toast.makeText(HarvestApp.getContext(),"Gagal Insert setUpLastSync",Toast.LENGTH_SHORT).show();
                        //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                    }
                }else{
                    Toast.makeText(HarvestApp.getContext(),"Gagal UpdateQCMutuAncakCounterByUserID",Toast.LENGTH_SHORT).show();
                    //btnSynchronize.setProgress(false);
                    progressDialog.dismiss();
                }
                break;
        }

        Log.d("ActiveActivity ","succes " +serviceResponse.getTag().toString());
    }

    @Override
    public void badResponse(ServiceResponse serviceResponse) {
        switch (serviceResponse.getTag()){
            case GetMasterUserByEstate:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetMasterUserByEstate",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                progressDialog.dismiss();
                break;
            case GetPemanenListByEstate:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetPemanenListByEstate",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                progressDialog.dismiss();
                break;
            case GetTphListByEstate:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetTphListByEstate",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                break;
            case GetQCQuestionAnswer:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetTphListByEstate",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                progressDialog.dismiss();
                break;
            case GetQcBuahByEstate:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetQcBuahByEstate",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                progressDialog.dismiss();
                break;
            case GetSensusBJRByEstate:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetSensusBJRByEstate",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                progressDialog.dismiss();
                break;
            case GetQCMutuAncakByEstate:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetQCMutuAncakByEstate",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                progressDialog.dismiss();
                break;
            case GetSessionQcFotoPanen:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetSessionQcFotoPanen",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                progressDialog.dismiss();
                break;
            case GetApplicationConfiguration:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetApplicationConfiguration",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                progressDialog.dismiss();
                break;
            case GetMasterSPKByEstate:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetMasterSPKByEstate",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                progressDialog.dismiss();
                break;
            case GetEstateMapping:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetEstateMapping",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                progressDialog.dismiss();
                break;
            case GetQCMutuAncakHeaderCounterByUserID:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetQCMutuAncakHeaderCounterByUserID",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                progressDialog.dismiss();
                break;
            case GetQCBuahCounterByUserID:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetQCMutuAncakHeaderCounterByUserID",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                progressDialog.dismiss();
                break;

        }
        Log.d("ActiveActivity ","badRespon " +serviceResponse.getTag().toString());
    }

    public void startLongOperation(int paramLongOperation){
        LongOperation = paramLongOperation;
        new LongOperation().execute(String.valueOf(LongOperation));
    }

    public class LongOperation extends AsyncTask<String, String, String> {
        boolean skip = false;
        JSONObject objSetUp;
        Snackbar snackbar;
        String text ="";

        public void publishProgress(JSONObject objProgres){
            onProgressUpdate(String.valueOf(objProgres));
        }

        @Override
        protected void onPreExecute() {
            switch (LongOperation){
                case LongOperation_BackUpHMS:
                    text = "Back Up HMS... ";
                    break;
                case LongOperation_GetMasterUserByEstate:
                    text = "Master User By Estate... ";
                    break;
                case LongOperation_GetPemanenListByEstate:
                    text = "Master Pemanen By Estate... ";
                    break;
                case LongOperation_GetTphListByEstate:
                    text = "Tph By Estate... ";
                    break;
                case LongOperation_GetQCQuestionAnswer:
                    text = "Qc Question Answare... ";
                    break;
                case LongOperation_GetQcBuahByEstate:
                    text = "Qc Buah By Estate... ";
                    break;
                case LongOperation_GetSensusBJRByEstate:
                    text = "Sensus BJR By Estate... ";
                    break;
                case LongOperation_GetQcAncakByEstate:
                    text = "Qc Ancak By Estate... ";
                    break;
                case LongOperation_GetSessionQcFotoPanen:
                    text = "Qc Session Foto Panen By Estate... ";
                    break;
                case LongOperation_GetApplicationConfiguration:
                    text = "Aplication Configuration... ";
                    break;
                case LongOperation_GetMasterSPKByEstate:
                    text= "Get Master SPK By Estate...";
                    break;
                case LongOperation_RestoreHMSTemp:
                    text = "Restore Data HMS TEMP";
                    break;
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (progressDialog != null) {
                         if(progressDialog.isShowing()) {
                             progressDialog.dismiss();
                         }
                    }
                    progressDialog = WidgetHelper.showWaitingDialog(ActiveActivity.this, text);
                }
            });
            Log.d("ActiveActivity ","onPreExecute "+text);
            snackbar = Snackbar.make(myCoordinatorLayout,text,Snackbar.LENGTH_INDEFINITE);
            objSetUp = new JSONObject();
        }

        @Override
        protected String doInBackground(String... strings) {
            switch (Integer.parseInt(strings[0])){
                case LongOperation_RestoreHMSTemp:
                    ZipArchive zipArchive = new ZipArchive();
                    zipArchive.unzip(GlobalHelper.getDatabasePathHMSTemp() + "QCHMSTemp.zip",
                            Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES , "");

                    File cekFileLastSync = new File(GlobalHelper.getDatabasePathHMS(), Encrypts.encrypt(GlobalHelper.LAST_SYNC));
                    if (cekFileLastSync.exists()) {
                        setUpDataSyncHelper.clearHmsTemp();
                    }

                    break;
                case LongOperation_BackUpHMS:
                    if(estatesMapping == null){
                        estatesMapping = new ArrayList<>();
                        estatesMapping.add(GlobalHelper.getEstate());
                        idxEstate = 0;
                    }
                    Log.d("ACTIVE SYNC","0 BackUpHMS");
                    setUpDataSyncHelper.backupDataHMS(estatesMapping,this);
                    break;
                case LongOperation_GetMasterUserByEstate:
                    skip = !setUpDataSyncHelper.SetMasterUserByEstate(responseApi,this);
                    break;
                case LongOperation_GetPemanenListByEstate:
                    skip = !setUpDataSyncHelper.SetPemanenListByEstate(responseApi,this);
                    break;
                case LongOperation_GetTphListByEstate:
                    skip = !setUpDataSyncHelper.downLoadTph(responseApi,this);
                    Log.d("ACTIVE SYNC","10 b GetTphListByEstate "+ skip );
                    break;
                case LongOperation_GetQCQuestionAnswer:
                    skip = !setUpDataSyncHelper.downLoadQCQuestionAnswer(responseApi,this);
                    Log.d("ACTIVE SYNC","11 LongOperation_GetQCQuestionAnswer "+ skip );
                    break;
                case LongOperation_GetQcBuahByEstate:
                    skip = !setUpDataSyncHelper.downLoadQcBuah(responseApi,this);
                    Log.d("ACTIVE SYNC","15 a LongOperation_GetQcBuahByEstate "+ skip );
                    break;
                case LongOperation_GetSensusBJRByEstate:
                    skip = !setUpDataSyncHelper.downLoadSensusBJR(responseApi,this);
                    Log.d("ACTIVE SYNC","16 a LongOperation_GetSensusBJRByEstate "+ skip );
                    break;
                case LongOperation_GetQcAncakByEstate:
                    skip = !setUpDataSyncHelper.downLoadQcAncak(responseApi,this);
                    Log.d("ACTIVE SYNC","17 a GetQcAncakByEstate "+ skip );
                    break;
                case LongOperation_GetSessionQcFotoPanen:
                    skip = !setUpDataSyncHelper.downLoadSessionQcFotoPanen(responseApi,this);
                    Log.d("ACTIVE SYNC","18 a GetQcAncakByEstate "+ skip );
                    break;
                case LongOperation_GetApplicationConfiguration:
                    skip = !setUpDataSyncHelper.downLoadApplicationConfiguration(responseApi,this);
                    break;
                case LongOperation_GetEstateMapping:
                    skip = !setUpDataSyncHelper.getEstateMapping(responseApi,this);
                    break;
                case LongOperation_GetMasterSPKByEstate:
                    skip = !setUpDataSyncHelper.downLoadMasterSpk(responseApi,this);
                    break;
            }
            String Flg = skip == true ? "T" : "F";
            Log.d("ActiveActivity ", "doInBackground "+text + "skip " + Flg);
//            Log.d("ActiveActivity ", "data "+responseApi);
            return strings[0];
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject objProgres = new JSONObject(String.valueOf(values[0]));
                        snackbar.setText(String.format(objProgres.getString("ket") + " %d %s [%d/%d] ",
                                objProgres.getInt("persen"), "%", objProgres.getInt("count"), objProgres.getInt("total")));
                        snackbar.show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            snackbar.dismiss();
            switch (Integer.parseInt(result)){
                case LongOperation_RestoreHMSTemp:
                    if(GlobalHelper.harusBackup()){
                        progressDialog.dismiss();
                        Toast.makeText(HarvestApp.getContext(),"Gagal Restore HMS Temp",Toast.LENGTH_LONG).show();
                        showActive();
                    }else{
                        nextIntent();
                    }
                    break;
                case LongOperation_BackUpHMS:
                    Log.d("ACTIVE SYNC","0 GetEstateMapping");
                    presenter.GetEstateMapping(setUpDataSyncHelper.estCodeSyncNow());
                    break;
                case LongOperation_GetEstateMapping:
                    if(skip){
                        Toast.makeText(HarvestApp.getContext(),"Gagal Insert LongOperation_GetEstateMapping",Toast.LENGTH_SHORT).show();
                        //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                    }else{
                        if(estatesMapping.size() -1 == idxEstate){
                            idxEstate = 0;
                            Log.d("ACTIVE SYNC","1 GetApplicationConfiguration");
                            presenter.GetApplicationConfiguration(setUpDataSyncHelper.estCodeSyncNow());
                        }else{
                            idxEstate++;
                            presenter.GetEstateMapping(setUpDataSyncHelper.estCodeSyncNow());
                        }
                    }
                    break;
                case LongOperation_GetApplicationConfiguration:
                    if(skip){
                        Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetApplicationConfiguration",Toast.LENGTH_SHORT).show();
                        //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                    }else{
                        if(estatesMapping.size() -1 == idxEstate){
                            idxEstate = 0;
                            Log.d("ACTIVE SYNC","2 GetMasterSPKByEstate");
                            presenter.GetMasterSPKByEstate(setUpDataSyncHelper.estCodeSyncNow());
                        }else{
                            idxEstate++;
                            presenter.GetApplicationConfiguration(setUpDataSyncHelper.estCodeSyncNow());
                        }
                    }
                    break;

                case LongOperation_GetMasterSPKByEstate:
                    if(skip){
                        Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetMasterSPKByEstate",Toast.LENGTH_SHORT).show();
                        //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                    }else{
                        if(estatesMapping.size() -1 == idxEstate){
                            idxEstate = 0;
                            Log.d("ACTIVE SYNC","3 GetMasterUserByEstate");
                            presenter.GetMasterUserByEstate(setUpDataSyncHelper.estCodeSyncNow());
                        }else{
                            idxEstate++;
                            presenter.GetMasterSPKByEstate(setUpDataSyncHelper.estCodeSyncNow());
                        }
                    }
                    break;
                case LongOperation_GetMasterUserByEstate:
                    if(skip){
                        Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetMasterUserByEstate",Toast.LENGTH_SHORT).show();
                        //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                    }else {
                        if(estatesMapping.size() -1 == idxEstate){
                            idxEstate = 0;
                            Log.d("ACTIVE SYNC","4 GetPemanenListByEstate");
                            presenter.GetPemanenListByEstate(setUpDataSyncHelper.estCodeSyncNow());
                        }else{
                            idxEstate++;
                            presenter.GetMasterUserByEstate(setUpDataSyncHelper.estCodeSyncNow());
                        }
                    }
                    break;
                case LongOperation_GetPemanenListByEstate:
                    if(skip){
                        Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetPemanenListByEstate",Toast.LENGTH_SHORT).show();
                        //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                    }else{
                        if(estatesMapping.size() -1 == idxEstate){
                            idxEstate = 0;
                            Log.d("ACTIVE SYNC","5 GetTphListByEstate");
                            presenter.GetTphListByEstate(setUpDataSyncHelper.estCodeSyncNow());
                        }else{
                            idxEstate++;
                            presenter.GetPemanenListByEstate(setUpDataSyncHelper.estCodeSyncNow());
                        }
                    }
                    break;
                case LongOperation_GetTphListByEstate:
                    if(skip){
                        Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetTphListByEstate",Toast.LENGTH_SHORT).show();
                        //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                    }else{
                        if(estatesMapping.size() -1 == idxEstate){
                            idxEstate = 0;
                            Log.d("ACTIVE SYNC","6 GetQcBuahByEstate");
                            presenter.GetQCQuestionAnswer();
                        }else{
                            idxEstate++;
                            presenter.GetTphListByEstate(setUpDataSyncHelper.estCodeSyncNow());
                        }
                    }
                    break;
                case LongOperation_GetQCQuestionAnswer:
                    if(skip){
                        Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetQCQuestionAnswer",Toast.LENGTH_SHORT).show();
                        //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                    }else{
                        if(estatesMapping.size() -1 == idxEstate){
                            idxEstate = 0;
                            Log.d("ACTIVE SYNC","6 GetQcBuahByEstate");
                            presenter.GetQcBuahByEstate(setUpDataSyncHelper.estCodeSyncNow());
                        }else{
                            idxEstate++;
                            presenter.GetQCQuestionAnswer();
                        }
                    }
                    break;
                case LongOperation_GetQcBuahByEstate:
                    if(skip){
                        Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetQcBuahByEstate",Toast.LENGTH_SHORT).show();
                        //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                    }else{
                        if(estatesMapping.size() -1 == idxEstate){
                            idxEstate = 0;
                            Log.d("ACTIVE SYNC","7 GetSensusBJRByEstate");
                            presenter.GetSensusBJRByEstate(setUpDataSyncHelper.estCodeSyncNow());
                        }else{
                            idxEstate++;
                            presenter.GetQcBuahByEstate(setUpDataSyncHelper.estCodeSyncNow());
                        }
                    }
                    break;
                case LongOperation_GetSensusBJRByEstate:
                    if(skip){
                        Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetSensusBJRByEstate",Toast.LENGTH_SHORT).show();
                        //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                    }else{
                        if(estatesMapping.size() -1 == idxEstate){
                            idxEstate = 0;
                            Log.d("ACTIVE SYNC","8 GetQcAncakByEstate");
                            presenter.GetQCMutuAncakByEstate(setUpDataSyncHelper.estCodeSyncNow());
                        }else{
                            idxEstate++;
                            presenter.GetSensusBJRByEstate(setUpDataSyncHelper.estCodeSyncNow());
                        }
                    }
                    break;
                case LongOperation_GetQcAncakByEstate:
                    if(skip){
                        Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetQcAncakByEstate",Toast.LENGTH_SHORT).show();
                        //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                    }else{
                        if(estatesMapping.size() -1 == idxEstate){
                            idxEstate = 0;
                            Log.d("ACTIVE SYNC","9 LongOperation_GetSessionQcFotoPanen");
                            presenter.GetSessionQcFotoPanen(setUpDataSyncHelper.estCodeSyncNow(),new JSONArray());
                        }else{
                            idxEstate++;
                            presenter.GetQCMutuAncakByEstate(setUpDataSyncHelper.estCodeSyncNow());
                        }
                    }
                    break;

                case LongOperation_GetSessionQcFotoPanen:
                    if(skip){
                        Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetSessionQcFotoPanen",Toast.LENGTH_SHORT).show();
                        //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                    }else{
                        if(estatesMapping.size() -1 == idxEstate){
                            idxEstate = 0;
                            Log.d("ACTIVE SYNC","10 GetTPanenCounterByUserID");
                            presenter.GetQCMutuAncakHeaderCounterByUserID();
                        }else{
                            idxEstate++;
                            presenter.GetSessionQcFotoPanen(setUpDataSyncHelper.estCodeSyncNow(),new JSONArray());
                        }
                    }
                    break;

            }
        }
    }
}
