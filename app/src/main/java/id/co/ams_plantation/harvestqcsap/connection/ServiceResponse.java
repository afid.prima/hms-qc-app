package id.co.ams_plantation.harvestqcsap.connection;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;



public class ServiceResponse implements Serializable {
    private Tag tag;
    private String host;
    private String path;
    private Method method;

    @JSONField(name = "error_code")
    private int errorCode;

    @JSONField(name = "error_message")
    private String message;

    private Object data;
    private Object meta;
    private int code;
    private int httpCode;



    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host){
        this.host = host;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path){
        this.path = path;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method){
        this.method = method;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Object getMeta() {
        return meta;
    }

    public void setMeta(Object meta) {
        this.meta = meta;
    }

    public int getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(int httpCode) {
        this.httpCode = httpCode;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }


}
