package id.co.ams_plantation.harvestqcsap.util;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.text.SpannableString;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;

import java.io.File;
import java.util.List;

import id.co.ams_plantation.harvestqcsap.ui.BaseActivity;
import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.adapter.ImageViewPagerPinchToZoomAdapter;

/**
 * Created by user on 11/21/2018.
 */

public class WidgetHelper {
    static AlertDialog ad;
    public static void showImagesZoom(Context context,List<File> ALselectedImage){
        View view = LayoutInflater.from(context).inflate(R.layout.imageszoom, null);
        ViewPager pinchtozoom_pager = (ViewPager) view.findViewById(R.id.pinchtozoom_pager);
        ImageView iv_iz_close = (ImageView) view.findViewById(R.id.iv_iz_close);

        iv_iz_close.setImageDrawable(new
                IconicsDrawable(context)
                .icon(MaterialDesignIconic.Icon.gmi_close).sizeDp(21)
                .colorRes(R.color.Black));

        ImageViewPagerPinchToZoomAdapter imageViewPagerPinchToZoomAdapter = new ImageViewPagerPinchToZoomAdapter(ALselectedImage);
        pinchtozoom_pager.setOffscreenPageLimit(3);
        pinchtozoom_pager.setAdapter(imageViewPagerPinchToZoomAdapter);

        iv_iz_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ad.isShowing()){
                    ad.dismiss();}
            }
        });
        ad = showListReference(ad,view,context);
    }

    public static AlertDialog showWaitingDialog(Context context, String message){
        View view = (View) LayoutInflater.from(context).inflate(R.layout.custom_dialog_waiting,null);
        TextView tvMessage = (TextView) view.findViewById(R.id.cust_tv_progress);
        tvMessage.setText(message);
        AlertDialog alertDialog = new AlertDialog
                .Builder(context, R.style.MyAlertDialogStyle)
                .setView(view)
                .setCancelable(false)
                .create();
        alertDialog.show();
        return alertDialog;
    }

    public static AlertDialog showOKDialog(Context context,
                                           String title,
                                           String message,
                                           DialogInterface.OnClickListener onClickListener){
        AlertDialog alertDialog = new AlertDialog
                .Builder(context, R.style.MyAlertDialogStyle)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK",onClickListener)
                .create();
        alertDialog.show();
        return alertDialog;
    }
    public static AlertDialog showOKDialogWithHighline(Context context,
                                                    String title,
                                                    SpannableString message,
                                                    DialogInterface.OnClickListener onClickListener){
        AlertDialog alertDialog = new AlertDialog
                .Builder(context, R.style.MyAlertDialogStyle)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK",onClickListener)
                .create();
        alertDialog.show();
        return alertDialog;
    }
    public static AlertDialog showCancelDialog(Context contextApdater,String message,DialogInterface.OnClickListener onClickListener){
        View view = (View) LayoutInflater.from(contextApdater).inflate(R.layout.custom_dialog_waiting,null);
        TextView tvMessage = (TextView) view.findViewById(R.id.cust_tv_progress);
        tvMessage.setText(message);
        AlertDialog alertDialog = new AlertDialog
                .Builder(contextApdater, R.style.MyAlertDialogStyle)
                .setView(view)
                .setCancelable(false)
                .setNegativeButton(contextApdater.getResources().getString(R.string.cancel),onClickListener)
                .create();
        alertDialog.show();
        return alertDialog;
    }

    public static AlertDialog showWaitingDialogALLpoin(Context context,String message){
        View view = (View) LayoutInflater.from(context).inflate(R.layout.custom_dialog_waiting,null);
        TextView tvMessage = (TextView) view.findViewById(R.id.cust_tv_progress);
        tvMessage.setText(message);
        AlertDialog alertDialog = new AlertDialog
                .Builder(context, R.style.MyAlertDialogStyle)
                .setView(view)
                .setCancelable(false)
                .create();
        alertDialog.show();
        return alertDialog;
    }

    public static AlertDialog CloseAlertDialog(AlertDialog alertDialog){
        if(alertDialog!= null){
            alertDialog.dismiss();
        }
        return alertDialog;
    }

    public static AlertDialog showOKCancelDialog(Context context,
                                                 String title,
                                                 String message,
                                                 String btnCancelTitle,
                                                 String btnOkTitle,
                                                 boolean cancelable,
                                                 DialogInterface.OnClickListener onClickListener,
                                                 DialogInterface.OnClickListener onCancelListener){
        AlertDialog alertDialog = new AlertDialog
                .Builder(context, R.style.MyAlertDialogStyle)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(cancelable)
                .setPositiveButton(btnOkTitle,onClickListener)
                .setNegativeButton(btnCancelTitle, onCancelListener)

                .create();
        return alertDialog;
    }

    public static AlertDialog showFormDialog(AlertDialog alertDialog,View view,Context contextApdater){
        alertDialog = new AlertDialog.Builder(contextApdater,R.style.MyAlertDialogStyle)
                .setCancelable(false)
                .setView(view)
                .create();
        final AlertDialog finalAlertDialog = alertDialog;
        alertDialog.setOnKeyListener(new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    finalAlertDialog.dismiss();
                }
                return true;
            }
        });
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.UpDownAnimation;
        alertDialog.show();
        return alertDialog;
    }

    public static AlertDialog showListReference(AlertDialog alertDialog,View view,Context context){
        alertDialog = new AlertDialog.Builder(context,R.style.MyAlertDialogStyle)
                .setView(view)
                .create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.UpDownAnimation;
        alertDialog.show();
        return alertDialog;
    }

    public static Snackbar showSnackBar(CoordinatorLayout view, String message){
        Snackbar bar = Snackbar.make(view, message, Snackbar.LENGTH_INDEFINITE);
        bar.show();
        return bar;
    }

    public static AlertDialog showCalibrationGpsDialog(Context context){
        if(context instanceof BaseActivity){
            if(((BaseActivity) context).alertDialogBase != null){
                ((BaseActivity) context).alertDialogBase.dismiss();
            }
        }
        View view = (View) LayoutInflater.from(context).inflate(R.layout.gps_calibration_dialog,null);
        TextView tvAccuracy = (TextView) view.findViewById(R.id.tvAccuracy);
        Button btnClose = (Button) view.findViewById(R.id.btnClose);

        tvAccuracy.setText(String.format("Akurasi GPS Anda : %.0f M",((BaseActivity) context).currentlocation.getAccuracy()));

        AlertDialog alertDialog = new AlertDialog
                .Builder(context, R.style.MyAlertDialogStyle)
                .setView(view)
                .setCancelable(false)
                .create();
        alertDialog.show();

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if(context instanceof BaseActivity){
                    ((BaseActivity) context).currentLocationOK();
                }
            }
        });
        return alertDialog;
    }

    public static Snackbar warningFindGps(Context context, CoordinatorLayout coordinatorLayout){
        String warning = context.getResources().getString(R.string.search_gps);
        try {
            if (context instanceof BaseActivity) {
                warning = String.format(context.getResources().getString(R.string.search_gps_with_accuracy) +" %.0f M",((BaseActivity) context).currentlocation.getAccuracy());
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        Snackbar snackbar = Snackbar.make(coordinatorLayout, warning, Snackbar.LENGTH_INDEFINITE);
        snackbar.show();
        return snackbar;
    }

    public static Snackbar warningRadius(Context context, CoordinatorLayout coordinatorLayout,double distance){
        String warning = context.getResources().getString(R.string.radius_to_long);
        try {
            if (context instanceof BaseActivity) {
                warning = String.format(context.getResources().getString(R.string.radius_to_long) +" %.0f M",distance);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        Snackbar snackbar = Snackbar.make(coordinatorLayout, warning, Snackbar.LENGTH_INDEFINITE);
        snackbar.show();
        return snackbar;
    }

    public static Snackbar warningRadiusTph(Context context, CoordinatorLayout coordinatorLayout,double distance){
        String warning = context.getResources().getString(R.string.radius_to_long_with_tph);
        try {
            if (context instanceof BaseActivity) {
                warning = String.format(context.getResources().getString(R.string.radius_to_long_with_tph) +" %.0f M",distance);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        Snackbar snackbar = Snackbar.make(coordinatorLayout, warning, Snackbar.LENGTH_INDEFINITE);
        snackbar.show();
        return snackbar;
    }

//
//    public static AlertDialog showFormDialogTransparant(AlertDialog alertDialog,View view,Context contextApdater){
//        alertDialog = new AlertDialog.Builder(contextApdater,R.style.MyAlertDialogStyleTransparant)
//                .setCancelable(false)
//                .setView(view)
//                .create();
//        alertDialog.getWindow().getAttributes().windowAnimations = R.style.UpDownAnimation;
//        //alertDialog.getWindow().getDecorView().getBackground().setColorFilter(new LightingColorFilter(00F, 00F));
//        alertDialog.show();
//        return alertDialog;
//    }
//
//    public static AlertDialog showFormDialogBotom(AlertDialog alertDialog,View view,Context contextApdater){
//        alertDialog = new AlertDialog.Builder(contextApdater,R.style.MyAlertDialogStyle)
//                .setCancelable(false)
//                .setView(view)
//                .create();
//        alertDialog.getWindow().getAttributes().gravity = Gravity.BOTTOM;
//        alertDialog.show();
//        return alertDialog;
//    }
//
//
//    public static AlertDialog showAlertDialogImages(AlertDialog alertDialog,View view,Context contextApdater){
//        alertDialog = new AlertDialog.Builder(contextApdater)
//                .setView(view)
//                .create();
//        alertDialog.getWindow().getAttributes().windowAnimations = R.style.UpDownAnimation;
//        alertDialog.show();
//        return alertDialog;
//    }
}
