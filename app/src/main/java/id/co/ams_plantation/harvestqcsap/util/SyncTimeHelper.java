package id.co.ams_plantation.harvestqcsap.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.util.HashMap;

import id.co.ams_plantation.harvestqcsap.HarvestApp;
import id.co.ams_plantation.harvestqcsap.connection.Tag;
import id.co.ams_plantation.harvestqcsap.encryptor.Encrypts;
import id.co.ams_plantation.harvestqcsap.model.LastSyncTime;
import id.co.ams_plantation.harvestqcsap.model.SyncTime;

public class SyncTimeHelper {

    public static boolean cekSyncTime(Tag tag){

        SharedPreferences prefer = HarvestApp.getContext().getSharedPreferences(HarvestApp.SYNC_TIME, Context.MODE_PRIVATE);
        String sync_time = prefer.getString(HarvestApp.SYNC_TIME,null);
        Gson gson = new Gson();
        LastSyncTime lastSyncTime = gson.fromJson(sync_time,LastSyncTime.class);
        HashMap<String,SyncTime> syncTimeHashMap=lastSyncTime.getSyncTimeHashMap();
        SyncTime syncTime;
        switch (tag){
            case PostQcBuahImages:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case PostQcBuah:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case PostSensusBjrImages:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case PostSensusBjr:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case PostQcAncakImages:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case PostQcAncak:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;

            case GetSensusBJRByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetQcBuahByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetQCMutuAncakByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetQCMutuAncakHeaderCounterByUserID:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetQCBuahCounterByUserID:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;


            case GetTphListByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetQCQuestionAnswer:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetMasterUserByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetPemanenListByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetApplicationConfiguration:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetMasterSPKByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetEstateMapping:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
        }
        return false;
    }

    public static void updateSyncTime(Tag tag){
        SharedPreferences prefer = HarvestApp.getContext().getSharedPreferences(HarvestApp.SYNC_TIME, Context.MODE_PRIVATE);
        String sync_time = prefer.getString(HarvestApp.SYNC_TIME,null);
        Gson gson = new Gson();
        LastSyncTime lastSyncTime = gson.fromJson(sync_time,LastSyncTime.class);
        HashMap<String,SyncTime> syncTimeHashMap = lastSyncTime.getSyncTimeHashMap();
        SyncTime syncTime;
        switch (tag){

            case PostQcBuahImages:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.UPLOAD_TRANSAKSI,syncTime);
                break;
            case PostQcBuah:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.UPLOAD_TRANSAKSI,syncTime);
                break;
            case PostSensusBjrImages:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.UPLOAD_TRANSAKSI,syncTime);
                break;
            case PostSensusBjr:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.UPLOAD_TRANSAKSI,syncTime);
                break;
            case PostQcAncakImages:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.UPLOAD_TRANSAKSI,syncTime);
                break;
            case PostQcAncak:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.UPLOAD_TRANSAKSI,syncTime);
                break;


            case GetSensusBJRByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_TRANSAKSI,syncTime);
                break;
            case GetQcBuahByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_TRANSAKSI,syncTime);
                break;
            case GetQCMutuAncakByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_TRANSAKSI,syncTime);
                break;

            case GetQCMutuAncakHeaderCounterByUserID:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_TRANSAKSI,syncTime);
                break;

            case GetQCBuahCounterByUserID:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_TRANSAKSI,syncTime);
                break;


            case GetTphListByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_MASTER,syncTime);
                break;
            case GetQCQuestionAnswer:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_MASTER,syncTime);
                break;

            case GetMasterUserByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_MASTER,syncTime);
                break;
            case GetPemanenListByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_MASTER,syncTime);
                break;

            case GetApplicationConfiguration:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_MASTER,syncTime);
                break;

            case GetMasterSPKByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_MASTER,syncTime);
                break;

            case GetEstateMapping:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_MASTER,syncTime);
                break;
        }

        lastSyncTime.setSyncTimeHashMap(syncTimeHashMap);
        SharedPreferences.Editor editor = prefer.edit();
        editor.putString(HarvestApp.SYNC_TIME,gson.toJson(lastSyncTime));
        editor.apply();
    }

    public static boolean cekDbTranskasi(String dbName){
        boolean tblTransaksi = false;
        dbName = dbName.replace(".db","");
        dbName = Encrypts.decrypt(dbName);
        switch (dbName){
//            case GlobalHelper.TABEL_TPH:
//                tblTransaksi = true;
//                break;
//            case GlobalHelper.TABLE_LANGSIRAN:
//                tblTransaksi = true;
//                break;

            case GlobalHelper.TABLE_QC_ANCAK:
                tblTransaksi = true;
                break;
            case GlobalHelper.TABLE_QC_ANCAK_POHON:
                tblTransaksi = true;
                break;
            case GlobalHelper.TABLE_QC_BJR:
                tblTransaksi = true;
                break;
            case GlobalHelper.TABLE_QC_BUAH:
                tblTransaksi = true;
                break;
        }
        return tblTransaksi;
    }

    public static boolean cekDbMaster(String dbName){
        boolean tbl = false;
        dbName = dbName.replace(".db","");
        dbName = Encrypts.decrypt(dbName);
        switch (dbName){
            case GlobalHelper.TABEL_TPH:
                tbl = true;
                break;
            case GlobalHelper.TABEL_PEMANEN:
                tbl = true;
                break;
            case GlobalHelper.TABLE_USER_HARVEST:
                tbl = true;
                break;
            case GlobalHelper.TABLE_APPLICATION_CONFIGURATION:
                tbl = true;
                break;
        }
        return tbl;
    }

    public static boolean cekFolderTransaksi(String folderName){
        boolean folder = false;
        if(folderName.equalsIgnoreCase(GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_QC_SENSUS_BJR])){
            folder = true;
        }else if(folderName.equalsIgnoreCase(GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECTED_QC_ANCAK])){
            folder = true;
        }else if(folderName.equalsIgnoreCase(GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECTED_QC_ANCAK_POHON])){
            folder = true;
        }else if(folderName.equalsIgnoreCase(GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_QC_MUTU_BUAH])){
            folder = true;
        }else if(folderName.equalsIgnoreCase(GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_QC_MUTU_ANCAK])){
            folder = true;
        }
        return folder;
    }

    public static boolean cekFolderMaster(String folderName){
        boolean folder = false;
        if(folderName.equalsIgnoreCase(GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_QR])){
            folder = true;
        }
        if(folderName.equalsIgnoreCase(GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_TPH])){
            folder = true;
        }
        return folder;
    }

    public static void updateFinishSyncTime(String namaListCekBox){

        SharedPreferences pref = HarvestApp.getContext().getSharedPreferences(HarvestApp.SYNC_TIME,Context.MODE_PRIVATE);
        String sync_time = pref.getString(HarvestApp.SYNC_TIME,null);
        Gson gson = new Gson();
        LastSyncTime lastSyncTime = gson.fromJson(sync_time, LastSyncTime.class);
        HashMap<String, SyncTime> syncTimeHashMap = lastSyncTime.getSyncTimeHashMap();
        SyncTime syncTime = syncTimeHashMap.get(namaListCekBox);
        if(syncTime.isSelected()) {
            syncTime.setSelected(false);
            syncTime.setFinish(true);
            syncTime.setTimeSync(System.currentTimeMillis());
        }

        syncTimeHashMap.put(syncTime.getNamaListCekBox(),syncTime);
        lastSyncTime.setSyncTimeHashMap(syncTimeHashMap);
        SharedPreferences.Editor edit = pref.edit();
        edit.putString(HarvestApp.SYNC_TIME,gson.toJson(lastSyncTime));
        edit.apply();
    }
}
