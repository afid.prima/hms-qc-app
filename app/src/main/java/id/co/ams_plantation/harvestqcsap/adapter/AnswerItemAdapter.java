package id.co.ams_plantation.harvestqcsap.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;

import id.co.ams_plantation.harvestqcsap.Fragment.QcMutuAncakInputEntryFragment;
import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.model.Answer;
import id.co.ams_plantation.harvestqcsap.model.ModelRecyclerView;
import id.co.ams_plantation.harvestqcsap.model.QuestionAnswer;
import id.co.ams_plantation.harvestqcsap.ui.QcMutuAncakActivity;

/**
 * Created on : 04,October,2021
 * Author     : Afid
 */

public class AnswerItemAdapter extends RecyclerView.Adapter<AnswerItemAdapter.Holder>{
    Context context;
    QuestionAnswer questionAnswer;
    ArrayList<Answer> selectedAnsware;
    public AnswerItemAdapter(Context context, QuestionAnswer questionAnswer,ArrayList<Answer> selectedAnsware){
        this.context = context;
        this.questionAnswer = questionAnswer;
        this.selectedAnsware = selectedAnsware;
    }

    @Override
    public AnswerItemAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.row_answer_choice,parent,false);
        return new AnswerItemAdapter.Holder(v);
    }

    @Override
    public void onBindViewHolder(AnswerItemAdapter.Holder holder, int position) {
        holder.setValue(questionAnswer.getAnswer().get(position));
    }

    @Override
    public int getItemCount() {
        return questionAnswer.getAnswer().size();
    }

    @Override
    public long getItemId(int position) {
//        return super.getItemId(position);
        return position;
    }

    @Override
    public int getItemViewType(int position) {
//        return super.getItemViewType(position);
        return position;
    }

    public class Holder extends RecyclerView.ViewHolder {

        LinearLayout lAnswer;
        CheckBox cbAnswer;
        TextView tvAnswer;

        public Holder(View itemView) {
            super(itemView);
            lAnswer = itemView.findViewById(R.id.lAnswer);
            cbAnswer = itemView.findViewById(R.id.cbAnswer);
            tvAnswer = itemView.findViewById(R.id.tvAnswer);

            lAnswer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (context instanceof QcMutuAncakActivity) {
                        if (((QcMutuAncakActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof QcMutuAncakInputEntryFragment) {

                        }
                    }
                }
            });
        }

        public void setValue(Answer answer) {
            Gson gson = new Gson();
            tvAnswer.setText(gson.toJson(answer));
            cbAnswer.setText(answer.getQcAnsware());
            if(selectedAnsware != null){
                if(selectedAnsware.size() > 0 ){
                    for(int i = 0 ; i < selectedAnsware.size(); i++){
                        Answer answer1 = selectedAnsware.get(i);
                        if(answer.getIdQcAnswer().toLowerCase().equals(answer1.getIdQcAnswer().toLowerCase())){
                            cbAnswer.setChecked(true);
                            break;
                        }
                    }
                }
            }

            cbAnswer.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (context instanceof QcMutuAncakActivity) {
                        if (((QcMutuAncakActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof QcMutuAncakInputEntryFragment) {
                            QcMutuAncakInputEntryFragment fragment = (QcMutuAncakInputEntryFragment) ((QcMutuAncakActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container);

                            Answer answer1 = gson.fromJson(tvAnswer.getText().toString(),Answer.class);
                            fragment.updateMultiSelect(isChecked,questionAnswer,answer1);
                        }
                    }
                }
            });

            lAnswer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (context instanceof QcMutuAncakActivity) {
                        if (((QcMutuAncakActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof QcMutuAncakInputEntryFragment) {
                            QcMutuAncakInputEntryFragment fragment = (QcMutuAncakInputEntryFragment) ((QcMutuAncakActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container);
                            boolean getCheck = !cbAnswer.isChecked();
                            Answer answer1 = gson.fromJson(tvAnswer.getText().toString(),Answer.class);
                            fragment.updateMultiSelect(getCheck,questionAnswer,answer1);
                        }
                    }
                }
            });
        }
    }
}
