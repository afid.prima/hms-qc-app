package id.co.ams_plantation.harvestqcsap.util;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.ObjectRepository;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import id.co.ams_plantation.harvestqcsap.HarvestApp;
import id.co.ams_plantation.harvestqcsap.model.TransaksiPanen;
import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.model.DataNitrit;
import id.co.ams_plantation.harvestqcsap.model.Estate;
import id.co.ams_plantation.harvestqcsap.model.User;
import id.co.ams_plantation.harvestqcsap.service.BluetoothService;
import id.co.ams_plantation.harvestqcsap.ui.BaseActivity;
import id.co.command.sdk.Command;
import id.co.command.sdk.PrintPicture;
import id.co.command.sdk.PrinterCommand;
import jp.wasabeef.recyclerview.adapters.SlideInLeftAnimationAdapter;

import static id.co.ams_plantation.harvestqcsap.util.QrHelper.BARCODE_HEIGHT;
import static id.co.ams_plantation.harvestqcsap.util.QrHelper.BARCODE_WIDTH;
import static id.co.ams_plantation.harvestqcsap.util.QrHelper.QR_HEIGHT;
import static id.co.ams_plantation.harvestqcsap.util.QrHelper.QR_WIDTH;

/**
 * Created by user on 11/22/2018.
 */

public class BluetoothHelper {

    public static final String TAG = "BluetoothHelper";
    // Message types sent from the BluetoothService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    public static final int MESSAGE_CONNECTION_LOST = 6;
    public static final int MESSAGE_UNABLE_CONNECT = 7;

    // Key names received from the BluetoothService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    public static BluetoothDevice bluetoothDeviceSelected;

    // Name of the connected device
    public String mConnectedDeviceName = null;
    // Local Bluetooth adapter
    public BluetoothAdapter mBluetoothAdapter = null;
    // Member object for the services
    public BluetoothService mService = null;
    public String BluetoothAddres = "";

    AlertDialog alertDialog;
    Context context;

    public BluetoothHelper(Context context) {
        this.context = context;
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            Toast.makeText(this.context, context.getResources().getString(R.string.bt_not_available),
                    Toast.LENGTH_LONG).show();
        } else {
            mService = new BluetoothService(this.context, mHandler);
            if(bluetoothDeviceSelected != null){
                Log.d("bluetoothDeviceSelected",bluetoothDeviceSelected.getAddress());
                mService.connect(bluetoothDeviceSelected);
            }
        }
    }

    public void showListBluetoothPrinter(){
        alertDialog = null;
        final View view = LayoutInflater.from(context).inflate(R.layout.list_popup,null);
        TextView tv_header = (TextView) view.findViewById(R.id.tv_header);

        RecyclerView rv_object = (RecyclerView) view.findViewById(R.id.rv_object);
        ImageView iv_nobluetooth = (ImageView) view.findViewById(R.id.iv_nobluetooth);

        tv_header.setText(context.getResources().getString(R.string.bluetooth_device));
        // Get the local Bluetooth adapter
        BluetoothAdapter mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        // Get a set of currently paired devices
        Set<BluetoothDevice> pairedDevices = mBtAdapter.getBondedDevices();


        if(pairedDevices.size() > 0){
            iv_nobluetooth.setVisibility(View.GONE);
            rv_object.setLayoutManager(new LinearLayoutManager(context));
            final AdapterBluetoothPrinter adapter = new AdapterBluetoothPrinter(context,pairedDevices);
            final SlideInLeftAnimationAdapter adapterLeft = new SlideInLeftAnimationAdapter(adapter);
            adapterLeft.setFirstOnly(false);
//       adapterLeft.setInterpolator(new OvershootInterpolator(1f));
            adapterLeft.setDuration(400);
            rv_object.setAdapter(adapterLeft);
        }
        alertDialog = WidgetHelper.showListReference(alertDialog,view,context);
    }

    public void selectBluetooth(BluetoothDevice bluetoothDevice){
        if (BluetoothAdapter.checkBluetoothAddress(bluetoothDevice.getAddress())) {
            BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(bluetoothDevice.getAddress());
            bluetoothDeviceSelected = device;
            mService.connect(device);
        }
        alertDialog.dismiss();
    }

    /*
         *SendDataByte
         */
    public void SendDataByte(byte[] data) {

        if (mService.getState() != BluetoothService.STATE_CONNECTED) {
            Toast.makeText(context, context.getResources().getString(R.string.bluetooth_not_connected) , Toast.LENGTH_SHORT)
                    .show();
            return;
        }
        mService.write(data);
    }

    public void printPanen(String textQr, TransaksiPanen transaksiPanen) {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy HH:mm");
        String alasBrondol = transaksiPanen.getHasilPanen().isAlasanBrondolan() == true ? "Yes" : "No";
        Estate estate = GlobalHelper.getEstateByEstCode(transaksiPanen.getTph().getEstCode());
        String format1 = "%-11s";
        String format2 = "%-15s";
        String format3 = "%-15s";
        String format4 = "%14s";
        String format5 = "%-14s";
        String format6 = "%-14s|";
        try {
            Command.ESC_Align[2] = 0x01;
            SendDataByte(Command.ESC_Align);
            Command.GS_ExclamationMark[2] = 0x11;
            SendDataByte(Command.GS_ExclamationMark);
            SendDataByte(PrinterCommand.POS_Print_Text(context.getResources().getString(R.string.transaksi_panen) + "\n", "GBK", 0, 1, 1, 35));

//            String oktextQr = textQr.replace("\n", "");
//            Log.d("BluetoothHelpder textQr", oktextQr);
//            byte[] qrcode = PrinterCommand.getBarCommand(oktextQr, 0, 3, 8);
////            byte[] qrcode = PrinterCommand.getBarCommand("Zijiang Electronic Thermal Receipt Printer!", 0, 3, 8);
            Command.ESC_Align[2] = 0x01;
            SendDataByte(Command.ESC_Align);
//            SendDataByte(qrcode);

//            byte[] data = PrintPicture.POS_PrintBMP(setQr(oktextQr), 384, 0);
//            SendDataByte(data);

            Command.ESC_Align[2] = 0x00;
            SendDataByte(PrinterCommand.POS_Set_Bold(1));
            SendDataByte(Command.ESC_Align);
            Command.GS_ExclamationMark[2] = 0x00;
            SendDataByte(Command.GS_ExclamationMark);
            SendDataByte(PrinterCommand.POS_Print_Text("========="+ context.getResources().getString(R.string.info_transaksi)+"=========\n", "GBK", 0, 0, 0, 0));
            if(estate != null) {
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "PT") + " : " + String.format(format2, estate.getCompanyShortName()) + "\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Estate") + " : " + String.format(format2, estate.getEstName()) + "\n", "GBK", 0, 0, 0, 0));
            }else{
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "PT") + " : " + String.format(format2, context.getResources().getString(R.string.dont_have_estate) + transaksiPanen.getTph().getEstCode()) + "\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Estate") + " : " + String.format(format2, context.getResources().getString(R.string.dont_have_estate) + transaksiPanen.getTph().getEstCode()) + "\n", "GBK", 0, 0, 0, 0));
            }

            User userSelected = null;
            if(this.context instanceof BaseActivity){
                userSelected = GlobalHelper.dataAllUser.get(transaksiPanen.getCreateBy());
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Input Oleh")+" : "+String.format(format2, userSelected != null ? userSelected.getUserFullName() : "-")+"\n", "GBK", 0, 0, 0, 0));
            }else{
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Input Oleh")+" : "+String.format(format2, transaksiPanen.getCreateBy())+"\n", "GBK", 0, 0, 0, 0));
            }
            if(transaksiPanen.getSubstitute() != null){
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Krani Penganti")+" : "+String.format(format2, transaksiPanen.getSubstitute().getNama())+"\n", "GBK", 0, 0, 0, 0));
            }

            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Waktu Input")+" : "+String.format(format2, sdf.format(transaksiPanen.getCreateDate()))+"\n", "GBK", 0, 0, 0, 0));

            SendDataByte(PrinterCommand.POS_Print_Text("============"+ context.getResources().getString(R.string.info_tph)+"============\n", "GBK", 0, 0, 0, 0));
            if(transaksiPanen.getTph().getNamaTph() != null) {
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Afdeling") + " : " + String.format(format2, transaksiPanen.getTph().getAfdeling()) + "\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Block") + " : " + String.format(format2, transaksiPanen.getTph().getBlock()) + "\n", "GBK", 0, 0, 0, 0));
//                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Ancak")+" : " + String.format(format2, transaksiPanen.getAncak()) + "\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Nama TPH")+" : " + String.format(format2, transaksiPanen.getTph().getNamaTph()) + "\n", "GBK", 0, 0, 0, 0));
            }else{
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Afdeling") + " : " + String.format(format2, context.getResources().getString(R.string.dont_have_tph) + transaksiPanen.getTph().getNoTph()) + "\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Block") + " : " + String.format(format2, context.getResources().getString(R.string.dont_have_tph) + transaksiPanen.getTph().getNoTph()) + "\n", "GBK", 0, 0, 0, 0));
//                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Ancak")+" : " + String.format(format2, transaksiPanen.getAncak()) + "\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Nama TPH")+" : " + String.format(format2, context.getResources().getString(R.string.dont_have_tph) + transaksiPanen.getTph().getNoTph()) + "\n", "GBK", 0, 0, 0, 0));
            }
//            SendDataByte(PrinterCommand.POS_Print_Text("Baris: "+transaksiPanen.getBaris()+"\n", "GBK", 0, 0, 0, 0));
//
            SendDataByte(PrinterCommand.POS_Print_Text("=========="+ context.getResources().getString(R.string.panen_tph)+"==========\n", "GBK", 0, 0, 0, 0));
            if(transaksiPanen.getKongsi().getSubPemanens() != null) {
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format6, "     Gang     ") + " " +
                        String.format(format5, "    Pemanen   ") + "\n", "GBK", 0, 0, 0, 0));
                for(int i = 0 ; i < transaksiPanen.getKongsi().getSubPemanens().size(); i++){
                    SendDataByte(PrinterCommand.POS_Print_Text(String.format(format6, transaksiPanen.getKongsi().getSubPemanens().get(i).getPemanen().getGank())+ " " +
                            String.format(format5, transaksiPanen.getKongsi().getSubPemanens().get(i).getPemanen().getNama()) + "\n", "GBK", 0, 0, 0, 0));
                }
            }else if(transaksiPanen.getPemanen().getNama() != null) {
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format3, "Gang")+" : " + String.format(format5, transaksiPanen.getPemanen().getGank()) + "\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format3, "Pemanen")+" : " + String.format(format5, transaksiPanen.getPemanen().getNama()) + "\n", "GBK", 0, 0, 0, 0));
            }else{
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format3, "Gang")+" : " + String.format(format5, context.getResources().getString(R.string.dont_have_pemanen) + transaksiPanen.getPemanen().getKodePemanen()) + "\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format3, "Pemanen")+" : " + String.format(format5, context.getResources().getString(R.string.dont_have_pemanen) + transaksiPanen.getPemanen().getKodePemanen()) + "\n", "GBK", 0, 0, 0, 0));
            }
            SendDataByte(PrinterCommand.POS_Print_Text("==========="+ context.getResources().getString(R.string.info_panen)+"===========\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format5, "Σ Jjg")+" : "+String.format(format4, String.format("%,d",transaksiPanen.getHasilPanen().getTotalJanjang()) + " Jjg")+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format5, "Σ Jjg Pendapat")+" : "+String.format(format4, String.format("%,d",transaksiPanen.getHasilPanen().getTotalJanjangPendapatan()) + " Jjg")+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format3, "Janjang Masak")+" : "+String.format(format4, String.format("%,d",transaksiPanen.getHasilPanen().getJanjangNormal()) + " Jjg")+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format3, "Mentah")+" : "+String.format(format4, String.format("%,d",transaksiPanen.getHasilPanen().getBuahMentah()) + " Jjg")+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format3, "Busuk & Jangkos")+" : "+String.format(format4, String.format("%,d",transaksiPanen.getHasilPanen().getBusukNJangkos()) + " Jjg")+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format3, "Lewat Matang")+" : "+String.format(format4, String.format("%,d",transaksiPanen.getHasilPanen().getBuahLewatMatang()) + " Jjg")+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format3, "Abnormal")+" : "+String.format(format4, String.format("%,d",transaksiPanen.getHasilPanen().getBuahAbnormal()) + " Jjg")+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format3, "Tangkai Panjang")+" : "+String.format(format4, String.format("%,d",transaksiPanen.getHasilPanen().getTangkaiPanjang()) + " Jjg")+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format3, "Dimakan Tikus")+" : "+String.format(format4, String.format("%,d",transaksiPanen.getHasilPanen().getBuahDimakanTikus()) + " Jjg")+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format3, "Brondolan")+" : "+String.format(format4, String.format("%,d",transaksiPanen.getHasilPanen().getBrondolan())+"  Kg")+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format3, "Alas Brondolan")+" : "+String.format(format5,alasBrondol)+"\n", "GBK", 0, 0, 0, 0));
//            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format3,"Waktu Panen")+" : "+sdf.format(transaksiPanen.getHasilPanen().getWaktuPanen())+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte("================================\n".getBytes("GBK"));
            SendDataByte(PrinterCommand.POS_Print_Text("Print "+ GlobalHelper.getUser().getUserFullName()+" "+ sdf.format(System.currentTimeMillis()) +"\n", "GBK", 0, 0, 0, 2));
            SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(58));
            SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(58));
            SendDataByte(Command.GS_V_m_n);
        }catch (Exception e){
            e.printStackTrace();
//        } catch (UnsupportedEncodingException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
        }

    }

    private Bitmap setQr(String text){
        try {

            QRCodeWriter writer = new QRCodeWriter();

            Log.i(TAG, "生成的文本：" + text);
            if (text == null || "".equals(text) || text.length() < 1) {
                //Toast.makeText(this, "Kosong Bos", Toast.LENGTH_SHORT).show();
                return null;
            }

            // 把输入的文本转为二维码
            BitMatrix martix = writer.encode(text, BarcodeFormat.QR_CODE, QR_WIDTH, QR_HEIGHT);

            System.out.println("w:" + martix.getWidth() + "h:"
                    + martix.getHeight());

            Hashtable<EncodeHintType, String> hints = new Hashtable<EncodeHintType, String>();
            hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
            BitMatrix bitMatrix = new QRCodeWriter().encode(text,
                    BarcodeFormat.QR_CODE, QR_WIDTH, QR_HEIGHT, hints);
            int[] pixels = new int[QR_WIDTH * QR_HEIGHT];
            for (int y = 0; y < QR_HEIGHT; y++) {
                for (int x = 0; x < QR_WIDTH; x++) {
                    if (bitMatrix.get(x, y)) {
                        pixels[y * QR_WIDTH + x] = 0xff000000;
                    } else {
                        pixels[y * QR_WIDTH + x] = 0xffffffff;
                    }

                }
            }

            Bitmap bitmap = Bitmap.createBitmap(QR_WIDTH, QR_HEIGHT,
                    Bitmap.Config.ARGB_8888);

            bitmap.setPixels(pixels, 0, QR_WIDTH, 0, 0, QR_WIDTH, QR_HEIGHT);
            return bitmap;
        } catch (WriterException e) {
            e.printStackTrace();
            return null;
        }
    }

    private Bitmap setBarcode(String text){
        try {
            MultiFormatWriter writer = new MultiFormatWriter();
            String finalData = Uri.encode(text);

            // Use 1 as the height of the matrix as this is a 1D Barcode.
            BitMatrix bm = writer.encode(finalData, BarcodeFormat.CODE_128, BARCODE_WIDTH, 1);
            int bmWidth = bm.getWidth();

            Bitmap imageBitmap = Bitmap.createBitmap(bmWidth, BARCODE_HEIGHT, Bitmap.Config.ARGB_8888);

            for (int i = 0; i < bmWidth; i++) {
                // Paint columns of width 1
                int[] column = new int[BARCODE_HEIGHT];
                Arrays.fill(column, bm.get(i, 0) ? Color.BLACK : Color.WHITE);
                imageBitmap.setPixels(column, 0, 1, i, 0, 1, BARCODE_HEIGHT);
            }

            return imageBitmap;
        } catch (WriterException e) {
            e.printStackTrace();
            return null;
        }
    }

    /****************************************************************************************************/
    @SuppressLint("HandlerLeak")
    public final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_STATE_CHANGE:
                    Log.i("HandlerLeak", "MESSAGE_STATE_CHANGE: " + msg.arg1);
                    switch (msg.arg1) {
                        case BluetoothService.STATE_CONNECTED:
//					mTitle.setText(R.string.title_connected_to);
//					mTitle.append(mConnectedDeviceName);
                            break;
                        case BluetoothService.STATE_CONNECTING:
//					mTitle.setText(R.string.title_connecting);
                            break;
                        case BluetoothService.STATE_LISTEN:
                        case BluetoothService.STATE_NONE:
//					mTitle.setText(R.string.title_not_connected);
                            break;
                    }
                    break;
                case MESSAGE_WRITE:

                    break;
                case MESSAGE_READ:

                    break;
                case MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                    Toast.makeText(HarvestApp.getContext(),
                            "Connected to " + mConnectedDeviceName,
                            Toast.LENGTH_SHORT).show();
                    break;
                case MESSAGE_TOAST:
                    Toast.makeText(HarvestApp.getContext(),
                            msg.getData().getString(TOAST), Toast.LENGTH_SHORT)
                            .show();
                    break;
                case MESSAGE_CONNECTION_LOST:    //蓝牙已断开连接
                    Toast.makeText(HarvestApp.getContext(), "Device connection was lost",
                            Toast.LENGTH_SHORT).show();
                    break;
                case MESSAGE_UNABLE_CONNECT:     //无法连接设备
                    Toast.makeText(HarvestApp.getContext(), "Unable to connect device",
                            Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    class AdapterBluetoothPrinter extends RecyclerView.Adapter<AdapterBluetoothPrinter.Holder> {

        Context context;
        private List<BluetoothDevice> originLists;

        public AdapterBluetoothPrinter(Context context, Set<BluetoothDevice> pairedDevices){
            this.context = context;
            originLists = new ArrayList<BluetoothDevice>(pairedDevices);
        }

        @Override
        public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(context).inflate(R.layout.list_popup_item,parent,false);
            return new Holder(v);
        }

        @Override
        public void onBindViewHolder(AdapterBluetoothPrinter.Holder holder, int position) {
            holder.setValue(originLists.get(position));
        }

        @Override
        public int getItemCount() {
            return originLists.size();
        }

        public class Holder extends RecyclerView.ViewHolder {
            TextView item_blockId;
            TextView item_lastUpdate;
            View view;

            public Holder(View itemView) {
                super(itemView);
                item_blockId = (TextView) itemView.findViewById(R.id.item_blockId);
                item_lastUpdate = (TextView) itemView.findViewById(R.id.item_lastUpdate);
                view = itemView;
            }

            public void setValue(final BluetoothDevice bluetoothDevice) {
                item_blockId.setText(bluetoothDevice.getName());
                item_lastUpdate.setText(bluetoothDevice.getAddress());

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        selectBluetooth(bluetoothDevice);
                    }
                });
            }

        }
    }
}
