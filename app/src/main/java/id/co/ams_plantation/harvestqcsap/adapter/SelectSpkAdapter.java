package id.co.ams_plantation.harvestqcsap.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.model.SPK;

public class SelectSpkAdapter extends ArrayAdapter<SPK> {

    public ArrayList<SPK> items;
    public ArrayList<SPK> itemsAll;
    public ArrayList<SPK> suggestions;
    private int viewResourceId;

    public SelectSpkAdapter(@NonNull Context context, int resource, @NonNull ArrayList<SPK> data) {
        super(context, resource, data);
        this.items = new ArrayList<>();
        this.items.addAll(data);
        this.itemsAll = (ArrayList<SPK>) this.items.clone();
        this.suggestions = new ArrayList<>();
        this.viewResourceId = resource;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(viewResourceId, null);
        }
        if (items.size() > position) {
            SPK spk = items.get(position);
            if (spk != null) {
                ImageView iv = (ImageView) v.findViewById(R.id.iv);
                TextView tv = (TextView) v.findViewById(R.id.tv);
                TextView tv1 = (TextView) v.findViewById(R.id.tv1);
                iv.setVisibility(View.GONE);
                tv.setText(spk.getIdSPK());
                tv1.setText(spk.getMaterialGroupDesc());
            }
        }
        return v;
    }

    public SPK getItemAt(int position) {
        return items.get(position);
    }

    @Override
    public int getCount() {
        return items != null ? items.size() : 0;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            String str = ((SPK) (resultValue)).getIdSPK();
            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (SPK pks : itemsAll) {
                    if (pks.getIdSPK().toLowerCase().contains(constraint.toString().toLowerCase()) || pks.getMaterialGroupDesc().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(pks);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<SPK> filteredList = (ArrayList<SPK>) results.values;
//            clear();
            items.clear();
//            itemsAll.clear();
            if (results != null && results.count > 0) {
//                addAll(filteredList);
//                itemsAll.addAll(filteredList);
//                for (Anggota anggota : filteredList){
//                    items.add(anggota);
//                }
                items.addAll(filteredList);
            } else {
//                clear();
                items.clear();
//                itemsAll.clear();
            }
            notifyDataSetChanged();
        }
    };
}