package id.co.ams_plantation.harvestqcsap.model;

public class StampImage {
    Pemanen pemanen;
    TPH tph;
    String block;
    String Bin;

    public StampImage() {
    }

    public Pemanen getPemanen() {
        return pemanen;
    }

    public void setPemanen(Pemanen pemanen) {
        this.pemanen = pemanen;
    }

    public TPH getTph() {
        return tph;
    }

    public void setTph(TPH tph) {
        this.tph = tph;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getBin() {
        return Bin;
    }

    public void setBin(String bin) {
        Bin = bin;
    }
}
