package id.co.ams_plantation.harvestqcsap.model;

import id.co.ams_plantation.harvestqcsap.connection.Tag;

public class SyncHistroyItem {
    public static final int Status_Method_Post = 0;
    public static final int Status_Method_Get = 1;

    int noUrut;
    Tag tag;
    String menu;
    String keterangan;
    int currentPersen;
    int finishPersen;
    boolean statusComplated;
    int statusMethod;
    long updateTime;

    public SyncHistroyItem(Tag tag, int currentPersen, int finishPersen) {
        this.tag = tag;
        this.currentPersen = currentPersen;
        this.finishPersen = finishPersen;
        this.updateTime = System.currentTimeMillis();
    }

    public SyncHistroyItem(int noUrut, Tag tag, String menu, String keterangan, int currentPersen, int finishPersen, boolean statusComplated, int statusMethod) {
        this.noUrut = noUrut;
        this.tag = tag;
        this.menu = menu;
        this.keterangan = keterangan;
        this.currentPersen = currentPersen;
        this.finishPersen = finishPersen;
        this.statusComplated = statusComplated;
        this.statusMethod = statusMethod;
        this.updateTime = System.currentTimeMillis();;
    }

    public int getNoUrut() {
        return noUrut;
    }

    public void setNoUrut(int noUrut) {
        this.noUrut = noUrut;
    }

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public int getCurrentPersen() {
        return currentPersen;
    }

    public void setCurrentPersen(int currentPersen) {
        this.currentPersen = currentPersen;
    }

    public int getFinishPersen() {
        return finishPersen;
    }

    public void setFinishPersen(int finishPersen) {
        this.finishPersen = finishPersen;
    }

    public boolean isStatusComplated() {
        return statusComplated;
    }

    public void setStatusComplated(boolean statusComplated) {
        this.statusComplated = statusComplated;
    }

    public int getStatusMethod() {
        return statusMethod;
    }

    public void setStatusMethod(int statusMethod) {
        this.statusMethod = statusMethod;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }
}
