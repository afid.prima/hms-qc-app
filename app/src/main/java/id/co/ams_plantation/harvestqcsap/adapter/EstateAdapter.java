package id.co.ams_plantation.harvestqcsap.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import net.rehacktive.waspdb.WaspDb;
import net.rehacktive.waspdb.WaspFactory;
import net.rehacktive.waspdb.WaspHash;

import java.util.ArrayList;
import java.util.List;

import id.co.ams_plantation.harvestqcsap.HarvestApp;
import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.model.Estate;
import id.co.ams_plantation.harvestqcsap.ui.MainMenuActivity;
import id.co.ams_plantation.harvestqcsap.util.GlobalHelper;

/**
 * Created on : 06,November,2021
 * Author     : Afid
 */

public class EstateAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {
    List<Estate> estateLists;
    List<Estate> originLists;
    Context context;
    String filterText = "";

    public EstateAdapter(Context context, ArrayList<Estate> estates){
        originLists = new ArrayList<>();
        estateLists = estates;
        originLists.addAll(estates);
        this.context = context;
    }

    public void setData(ArrayList<Estate> estates){
        estateLists.clear();
        originLists.clear();
        estateLists.addAll(estates);
        originLists.addAll(estates);
        notifyDataSetChanged();
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.estate_chooser_item_list,parent,false);
        final EstateViewHolder holder = new EstateViewHolder(itemView);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Estate estate = estateLists.get(holder.getLayoutPosition());
                ((MainMenuActivity) context).changeEstateHelper.setEstate(estate);
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        bindData((EstateViewHolder)holder,position);
    }

    private void bindData(EstateViewHolder holder,final int position){
        Estate estate = estateLists.get(position);
        String estString = estate.getEstCode()+" - "+estate.getEstName();
        int startPos = estString.toLowerCase().indexOf(filterText.toString());
        int endPos = startPos + filterText.length();
        if(startPos!=-1){
            SpannableString spannable = new SpannableString(estString);
            holder.estateName.setTextColor(ContextCompat.getColor(((MainMenuActivity) context),
                    android.R.color.primary_text_light));
            spannable.setSpan(new StyleSpan(Typeface.NORMAL),0,spannable.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            spannable.setSpan(new StyleSpan(Typeface.BOLD),startPos,endPos,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            spannable.setSpan(new ForegroundColorSpan(Color.RED),startPos,endPos,Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            holder.estateName.setText(spannable);
        }

    }

    @Override
    public int getItemCount() {
        return estateLists!=null?estateLists.size():0;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                ArrayList<Estate> filteredEstates = new ArrayList<>();
                String string = constraint.toString().toLowerCase();
                for(Estate estate:originLists){
                    if(estate.getEstCode().toLowerCase().contains(string) ||
                            estate.getEstName().toLowerCase().contains(string)){
                        filteredEstates.add(estate);
                    }
                }
                filterText = string;
                if(filteredEstates.size()>0){
                    results.count = filteredEstates.size();
                    results.values = filteredEstates;
                    return results;
                }else {
                    return null;
                }
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if(results!=null){
                    estateLists = (ArrayList<Estate>)results.values;
                }else{
                    estateLists.clear();
                }
                notifyDataSetChanged();
            }
        };
    }

    public final class EstateViewHolder extends  RecyclerView.ViewHolder{
        public TextView estateName;
        public EstateViewHolder(View itemView) {
            super(itemView);
            estateName = (TextView) itemView.findViewById(R.id.estate_name);
        }
    }
}
