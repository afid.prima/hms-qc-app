package id.co.ams_plantation.harvestqcsap.model;

public class JanjangSensus {
    int idxJanjang;
    int janjang;

    public JanjangSensus(int idxJanjang, int janjang) {
        this.idxJanjang = idxJanjang;
        this.janjang = janjang;
    }

    public int getIdxJanjang() {
        return idxJanjang;
    }

    public void setIdxJanjang(int idxJanjang) {
        this.idxJanjang = idxJanjang;
    }

    public int getJanjang() {
        return janjang;
    }

    public void setJanjang(int janjang) {
        this.janjang = janjang;
    }
}
