package id.co.ams_plantation.harvestqcsap.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.model.SubPemanen;

/**
 * Created on : 22,October,2021
 * Author     : Afid
 */

public class RowPemanenAdapter extends RecyclerView.Adapter<RowPemanenAdapter.Holder>{
    Context context;
    ArrayList<SubPemanen> originLists;

    public RowPemanenAdapter(Context context, ArrayList<SubPemanen> originLists) {
        this.context = context;
        this.originLists = originLists;
    }

    @NonNull
    @Override
    public RowPemanenAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.row_object,viewGroup,false);
        return new RowPemanenAdapter.Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RowPemanenAdapter.Holder holder, int i) {
        holder.setValue(originLists.get(i));
    }

    @Override
    public int getItemCount() {
        return originLists.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView item_ket1;

        public Holder(View itemView) {
            super(itemView);

            item_ket1 = itemView.findViewById(R.id.item_ket1);
        }

        public void setValue(final SubPemanen value) {
            if(value.getPemanen() != null) {
                item_ket1.setText(value.getPemanen().getGank() + " - "+ value.getPemanen().getNik() + " - " +value.getPemanen().getNama());
            }
        }
    }
}
