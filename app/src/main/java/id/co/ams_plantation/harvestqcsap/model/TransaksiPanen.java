package id.co.ams_plantation.harvestqcsap.model;

import org.json.JSONObject;

import java.util.HashSet;

import id.co.ams_plantation.harvestqcsap.HarvestApp;
import id.co.ams_plantation.harvestqcsap.R;

/**
 * Created by user on 12/3/2018.
 */

public class TransaksiPanen {

    public static final int SAVE_ONLY = 0;
    public static final int TAP = 1;
    public static final int UPLOAD = 2;
    public static final int SUDAH_DIANGKUT = 3;
    public static final int SUDAH_DIPKS = 4;
    public static final int DELETED = 5;

    public static final int COLOR_SAVE_ONLY = HarvestApp.getContext().getResources().getColor(R.color.Gray);
    public static final int COLOR_TAP = HarvestApp.getContext().getResources().getColor(R.color.Yellow);
    public static final int COLOR_UPLOAD = HarvestApp.getContext().getResources().getColor(R.color.SkyBlue);
    public static final int COLOR_SUDAH_DIANGKUT = HarvestApp.getContext().getResources().getColor(R.color.Orange);
    public static final int COLOR_SUDAH_DIPKS = HarvestApp.getContext().getResources().getColor(R.color.Green);

    String idTPanen;
    double latitude;
    double longitude;
    String createBy;
    long createDate;
    String updateBy;
    long updateDate;
    String ancak;
    String baris;
    int status;
    int restan;
    String estCode;
    String block;
    Pemanen pemanen;
    TPH tph;
    HasilPanen hasilPanen;
    HashSet<String> foto;
    String idNfc;
    SupervisionList substitute;
    Kongsi kongsi;
    SPK spk;
    NFCFlagTap nfcFlagTap;

    public TransaksiPanen() {
    }

    public TransaksiPanen(String idTPanen, double latitude, double longitude, String createBy, long createDate, String updateBy, long updateDate, String ancak, String baris, Pemanen pemanen, TPH tph, HasilPanen hasilPanen, HashSet<String> foto,int status,int restan,Kongsi kongsi,SPK spk) {
        this.idTPanen = idTPanen;
        this.latitude = latitude;
        this.longitude = longitude;
        this.createBy = createBy;
        this.createDate = createDate;
        this.updateBy = updateBy;
        this.updateDate = updateDate;
        this.ancak = ancak;
        this.baris = baris;
        this.pemanen = pemanen;
        this.tph = tph;
        this.hasilPanen = hasilPanen;
        this.foto = foto;
        this.status = status;
        this.restan = restan;
        this.kongsi = kongsi;
        this.spk = spk;
    }

    public String getEstCode() {
        return estCode;
    }

    public void setEstCode(String estCode) {
        this.estCode = estCode;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public int getRestan() {
        return restan;
    }

    public void setRestan(int restan) {
        this.restan = restan;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public HashSet<String> getFoto() {
        return foto;
    }

    public void setFoto(HashSet<String> foto) {
        this.foto = foto;
    }

    public String getAncak() {
        return ancak;
    }

    public void setAncak(String ancak) {
        this.ancak = ancak;
    }

    public String getBaris() {
        return baris;
    }

    public void setBaris(String baris) {
        this.baris = baris;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public long getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(long updateDate) {
        this.updateDate = updateDate;
    }

    public String getIdTPanen() {
        return idTPanen;
    }

    public void setIdTPanen(String idTPanen) {
        this.idTPanen = idTPanen;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public Pemanen getPemanen() {
        return pemanen;
    }

    public void setPemanen(Pemanen pemanen) {
        this.pemanen = pemanen;
    }

    public TPH getTph() {
        return tph;
    }

    public void setTph(TPH tph) {
        this.tph = tph;
    }

    public HasilPanen getHasilPanen() {
        return hasilPanen;
    }

    public void setHasilPanen(HasilPanen hasilPanen) {
        this.hasilPanen = hasilPanen;
    }

    public static void toDecompre(JSONObject object) {
    }

    public String getIdNfc() {
        return idNfc;
    }

    public void setIdNfc(String idNfc) {
        this.idNfc = idNfc;
    }

    public SupervisionList getSubstitute() {
        return substitute;
    }

    public void setSubstitute(SupervisionList substitute) {
        this.substitute = substitute;
    }

    public Kongsi getKongsi() {
        return kongsi;
    }

    public void setKongsi(Kongsi kongsi) {
        this.kongsi = kongsi;
    }

    public SPK getSpk() {
        return spk;
    }

    public void setSpk(SPK spk) {
        this.spk = spk;
    }

    public NFCFlagTap getNfcFlagTap() {
        return nfcFlagTap;
    }

    public void setNfcFlagTap(NFCFlagTap nfcFlagTap) {
        this.nfcFlagTap = nfcFlagTap;
    }
}
