package id.co.ams_plantation.harvestqcsap.util;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.ObjectRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import id.co.ams_plantation.harvestqcsap.Fragment.TphInputFragment;
import id.co.ams_plantation.harvestqcsap.HarvestApp;
import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.model.DataNitrit;
import id.co.ams_plantation.harvestqcsap.model.Pemanen;
import jp.wasabeef.recyclerview.adapters.SlideInLeftAnimationAdapter;

/**
 * Created by user on 12/12/2018.
 */

public class PemanenHelper {

    AlertDialog listrefrence;
    ArrayList<Pemanen> filter;
    ArrayList<Pemanen> origin;
    FragmentActivity activity;

    AppCompatEditText etUrut;
    AppCompatEditText etKemandoran;
    AppCompatEditText etNama;
    AppCompatEditText etGang;
    Button btnCancel;
    Button btnSave;
    Pemanen selectedPemanen;

    public PemanenHelper(FragmentActivity activity) {
        this.activity = activity;
    }

    public ArrayList<Pemanen> getListPemanenByGangWithCompare(HashMap<String,Pemanen> allPemanen,ArrayList<String> compareKodePemanen,String kodeGang){
        ArrayList<Pemanen> pemanenArrayList = new ArrayList<>();
        for(HashMap.Entry<String,Pemanen> entry : allPemanen.entrySet()){
            if(entry.getValue().getGank() != null) {
                if (entry.getValue().getGank().equals(kodeGang)) {
                    pemanenArrayList.add(entry.getValue());
                }
            }
        }
        return pemanenArrayList;
    }

    public boolean validationPemanen(){
        if(etKemandoran.getText().toString().isEmpty()){
            Toast.makeText(HarvestApp.getContext(),"Mohon Input Kemandoran",Toast.LENGTH_SHORT).show();
            etKemandoran.requestFocus();
            return false;
        }

        if(etUrut.getText().toString().isEmpty()){
            Toast.makeText(HarvestApp.getContext(),"Mohon Input No Urut",Toast.LENGTH_SHORT).show();
            etUrut.requestFocus();
            return false;
        }
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABEL_PEMANEN);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = iterable.iterator(); iterator.hasNext();) {
            Gson gson = new Gson();
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Pemanen pemanen = gson.fromJson(dataNitrit.getValueDataNitrit(), Pemanen.class);

            if(pemanen.getKemandoran() == null && pemanen.getNoUrut() == 0){

            }else if((pemanen.getGank().equals(selectedPemanen.getGank())) && (pemanen.getNoUrut() == Integer.parseInt(etUrut.getText().toString()))){
                Toast.makeText(HarvestApp.getContext(),"Nomor Sudah Ada Dipakai Oleh "+ pemanen.getNama(),Toast.LENGTH_SHORT).show();
                db.close();
                return false;
            }
        }
        db.close();

        selectedPemanen.setKemandoran(etKemandoran.getText().toString());
        selectedPemanen.setNoUrut(Integer.parseInt(etUrut.getText().toString()));
        selectedPemanen.setStatus(Pemanen.Pemanen_NotUplaod);
        return true;
    }

    public boolean savePemanen(){
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABEL_PEMANEN);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        DataNitrit dataNitrit = null;
        Gson gson = new Gson();
        dataNitrit = new DataNitrit(selectedPemanen.getKodePemanen(),gson.toJson(selectedPemanen));
        repository.update(dataNitrit);
        db.close();
        return true;
    }

    private class LongOperation extends AsyncTask<String, Void, String> {
        AlertDialog alertDialogAllpoin;
        boolean validasi;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            alertDialogAllpoin = WidgetHelper.showWaitingDialog(activity, activity.getResources().getString(R.string.wait));
            validasi = validationPemanen();
        }

        @Override
        protected String doInBackground(String... strings) {
            if(validasi){
                if(savePemanen()){
                    return "1";
                }
            }
            return "0";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(alertDialogAllpoin!=null){
                alertDialogAllpoin.dismiss();
            }
            switch (Integer.parseInt(result)){
                case 0:
                    if(validasi) {
                        Toast.makeText(HarvestApp.getContext(), "Gagal UpdatePemanen", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case 1:
//                    if(listrefrence != null){
//                        listrefrence.dismiss();
//                    }
//                    if(activity instanceof PemanenActivity){
//                        ((PemanenActivity) activity).closeForm(true);
//                    }
                    Toast.makeText(HarvestApp.getContext(),"Berhasil UpdatePemanen", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    }

    class ChoseTphAdapter extends RecyclerView.Adapter<ChoseTphAdapter.Holder> {
        Context contextApdater;

        public ChoseTphAdapter(Context context) {
            this.contextApdater = context;
        }

        @Override
        public ChoseTphAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(contextApdater).inflate(R.layout.row_infowindow_chose,parent,false);
            return new Holder(v);
        }

        @Override
        public void onBindViewHolder(Holder holder, int position) {
            holder.setValue(filter.get(position));
        }

        @Override
        public int getItemCount() {
            return filter != null ? filter.size(): 0 ;
        }

        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults results = new FilterResults();
                    filter = new ArrayList<>();
                    String string = constraint.toString().toLowerCase();
                    for (Pemanen pemanen : origin) {
                        if(pemanen.getNama().toLowerCase().contains(string) || pemanen.getGank().toLowerCase().contains(string)){
                            filter.add(pemanen);
                        }
                    }
                    if (filter.size() > 0) {
                        results.count = filter.size();
                        results.values = filter;
                        return results;
                    } else {
                        return null;
                    }
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null) {
                        filter = (ArrayList<Pemanen>) results.values;
                    } else {
                        filter.clear();
                    }
                    notifyDataSetChanged();

                }
            };
        }

        public class Holder extends RecyclerView.ViewHolder {
            TextView tv_riw_chose;
            TextView tv_riw_chose1;
            CardView cv_riw_chose;
            public Holder(View itemView) {
                super(itemView);
                cv_riw_chose = (CardView) itemView.findViewById(R.id.cv_riw_chose);
                tv_riw_chose = (TextView) itemView.findViewById(R.id.tv_riw_chose);
                tv_riw_chose1 = (TextView) itemView.findViewById(R.id.tv_riw_chose1);
            }

            public void setValue(Pemanen value) {
                tv_riw_chose.setText(value.getNama());
                tv_riw_chose1.setText(value.getGank());
                cv_riw_chose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Fragment fragment = (Fragment) activity.getSupportFragmentManager().findFragmentById(R.id.content_container);
                        if(fragment instanceof TphInputFragment){
//                            ((TphInputFragment) fragment).setPemanen(value);
                        }
                        listrefrence.dismiss();
                    }
                });
            }
        }
    }
}
