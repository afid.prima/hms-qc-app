package id.co.ams_plantation.harvestqcsap.view;

// Create all method blueprint here

import id.co.ams_plantation.harvestqcsap.model.ExampleModel;

public interface ExampleView extends BaseView {
    void setExampleScreen(ExampleModel exampleModel);
}
