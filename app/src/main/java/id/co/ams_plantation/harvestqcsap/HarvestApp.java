package id.co.ams_plantation.harvestqcsap;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.downloader.PRDownloader;
import com.downloader.PRDownloaderConfig;


import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import id.co.ams_plantation.harvestqcsap.util.GlobalHelper;

/**
 * REPLACE THIS WITH YOUR Application Context
 */

public class HarvestApp extends Application {
    private static Context context;
    public static final String CONNECTION_PREF = "CONNECTION_PREF";
    public static final String LATLON_PENDAFTARAN = "LATLON_PENDAFTARAN";
    public static final String AFDELING_BLOCK = "AFDELING_BLOCK";
    public static final String STAMP_IMAGES = "STAMP_IMAGES";
    public static final String CONNECT_BUFFER_SERVER = "CONNECT_BUFFER_SERVER";
    public static final String SYNC_HISTORY = "SYNC_HISTORY";
    public static final String SYNC_TIME = "SYNC_TIME";
    public static final String COUNTER_TRANSKASI = "COUNTER_TRANSKASI";
    public static final String SYNC_TRANSAKSI = "SYNC_TRANSAKSI";

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        HarvestApp.context = getApplicationContext();

        PRDownloaderConfig config = PRDownloaderConfig.newBuilder()
                .setDatabaseEnabled(true)
                .build();
        PRDownloader.initialize(this, config);

        try {
            GlobalHelper.overrideFont(getContext(),"DEFAULT",GlobalHelper.FONT_CORBERT);
            GlobalHelper.overrideFont(getContext(),"SERIF",GlobalHelper.FONT_CORBERT);
        } catch (Exception e) {
            Log.e("Can not set custom font","Fail");
        }
        CustomActivityOnCrash.install(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public static Context getContext(){
        return HarvestApp.context;
    }

}
