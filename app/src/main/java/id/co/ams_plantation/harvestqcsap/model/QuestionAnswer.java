package id.co.ams_plantation.harvestqcsap.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created on : 07,September,2021
 * Author     : Afid
 */

public class QuestionAnswer {

    @SerializedName("Answer")
    @Expose
    private ArrayList<Answer> answer = null;
    @SerializedName("IdQcQuestion")
    @Expose
    private String idQcQuestion;
    @SerializedName("IdQuestionAnswer")
    @Expose
    private String idQuestionAnswer;
    @SerializedName("QcQuestion")
    @Expose
    private String qcQuestion;
    @SerializedName("QcType")
    @Expose
    private String qcType;
    @SerializedName("QuestionAnswerType")
    @Expose
    private String questionAnswerType;

    public QuestionAnswer() {
    }

    public QuestionAnswer(ArrayList<Answer> answer, String idQcQuestion, String idQuestionAnswer, String qcQuestion, String qcType, String questionAnswerType) {
        this.answer = answer;
        this.idQcQuestion = idQcQuestion;
        this.idQuestionAnswer = idQuestionAnswer;
        this.qcQuestion = qcQuestion;
        this.qcType = qcType;
        this.questionAnswerType = questionAnswerType;
    }

    public ArrayList<Answer> getAnswer() {
        return answer;
    }

    public void setAnswer(ArrayList<Answer> answer) {
        this.answer = answer;
    }

    public String getIdQcQuestion() {
        return idQcQuestion;
    }

    public void setIdQcQuestion(String idQcQuestion) {
        this.idQcQuestion = idQcQuestion;
    }

    public String getIdQuestionAnswer() {
        return idQuestionAnswer;
    }

    public void setIdQuestionAnswer(String idQuestionAnswer) {
        this.idQuestionAnswer = idQuestionAnswer;
    }

    public String getQcQuestion() {
        return qcQuestion;
    }

    public void setQcQuestion(String qcQuestion) {
        this.qcQuestion = qcQuestion;
    }

    public String getQcType() {
        return qcType;
    }

    public void setQcType(String qcType) {
        this.qcType = qcType;
    }

    public String getQuestionAnswerType() {
        return questionAnswerType;
    }

    public void setQuestionAnswerType(String questionAnswerType) {
        this.questionAnswerType = questionAnswerType;
    }
}
