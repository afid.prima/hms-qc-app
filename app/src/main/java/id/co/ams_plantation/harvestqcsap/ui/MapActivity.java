package id.co.ams_plantation.harvestqcsap.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.esri.android.map.Callout;
import com.esri.android.map.GraphicsLayer;
import com.esri.android.map.LocationDisplayManager;
import com.esri.android.map.MapView;
import com.esri.android.map.ags.ArcGISLocalTiledLayer;
import com.esri.android.map.event.OnPanListener;
import com.esri.android.map.event.OnSingleTapListener;
import com.esri.android.map.event.OnStatusChangedListener;
import com.esri.android.map.event.OnZoomListener;
import com.esri.android.map.ogc.kml.KmlLayer;
import com.esri.android.runtime.ArcGISRuntime;
import com.esri.core.geometry.GeometryEngine;
import com.esri.core.geometry.Point;
import com.esri.core.geometry.Polygon;
import com.esri.core.geometry.SpatialReference;
import com.esri.core.map.Graphic;
import com.esri.core.runtime.LicenseLevel;
import com.esri.core.runtime.LicenseResult;
import com.esri.core.symbol.CompositeSymbol;
import com.esri.core.symbol.SimpleLineSymbol;
import com.esri.core.symbol.SimpleMarkerSymbol;
import com.esri.core.symbol.TextSymbol;
import com.google.gson.Gson;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.iconics.utils.Utils;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;
import com.nightonke.boommenu.BoomButtons.ButtonPlaceEnum;
import com.nightonke.boommenu.BoomButtons.HamButton;
import com.nightonke.boommenu.BoomButtons.OnBMClickListener;
import com.nightonke.boommenu.BoomButtons.TextOutsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.ButtonEnum;
import com.nightonke.boommenu.Piece.PiecePlaceEnum;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import co.ceryle.segmentedbutton.SegmentedButtonGroup;
import id.co.ams_plantation.harvestqcsap.HarvestApp;
import id.co.ams_plantation.harvestqcsap.connection.ServiceResponse;
import id.co.ams_plantation.harvestqcsap.Fragment.SettingFragment;
import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.adapter.SelectedAfdelingBlock;
import id.co.ams_plantation.harvestqcsap.connection.Tag;
import id.co.ams_plantation.harvestqcsap.model.Block;
import id.co.ams_plantation.harvestqcsap.model.SyncTime;
import id.co.ams_plantation.harvestqcsap.model.TPH;
import id.co.ams_plantation.harvestqcsap.presenter.ConnectionPresenter;
import id.co.ams_plantation.harvestqcsap.util.AfdelingBlockHelper;
import id.co.ams_plantation.harvestqcsap.util.GlobalHelper;
import id.co.ams_plantation.harvestqcsap.util.InfoWindow;
import id.co.ams_plantation.harvestqcsap.util.InfoWindowChose;
import id.co.ams_plantation.harvestqcsap.util.RestoreHelper;
import id.co.ams_plantation.harvestqcsap.util.SetUpDataSyncHelper;
import id.co.ams_plantation.harvestqcsap.util.SyncHistoryHelper;
import id.co.ams_plantation.harvestqcsap.util.SyncTimeHelper;
import id.co.ams_plantation.harvestqcsap.util.TphHelper;
import id.co.ams_plantation.harvestqcsap.util.UploadHelper;
import id.co.ams_plantation.harvestqcsap.util.WidgetHelper;
import id.co.ams_plantation.harvestqcsap.view.ApiView;

public class MapActivity extends BaseActivity {

    public static final int LongOperation_Setup_Data_AfdelingBlock = 0;
    public static final int LongOperation_Setup_Poin = 1;
    public static final int LongOperation_Setup_FormTPH = 2;
//    public static final int LongOperation_Setup_FormLangsiran = 3;
//
//    public static final int Status_Close_Form_Close = 0;
//    public static final int Status_Close_Form_Save = 1;
//    public static final int Status_Close_Form_Deleted = 2;
//    public static final int Status_Close_Form_Update = 3;

    public MapView mapView;
    public LocationDisplayManager locationDisplayManager;
    ArcGISLocalTiledLayer tiledLayer;
    KmlLayer kmlLayer;

    public FloatingActionButton fabCurrentLocation;
    public FloatingActionButton fabEstateLocation;

    public RelativeLayout container;
    public RelativeLayout rl_main;
    public SlidingUpPanelLayout mLayout;

    public static Long l1;

    RelativeLayout btnShowForm;
    RelativeLayout topLayout;
    CardView cvAfdBlock;
    RecyclerView rvAfdBlock;
    CheckBox checkBoxTphResurvey;
    CheckBox checkBoxTph;
    CheckBox checkBoxLangsir;
    CheckBox checkBoxInputHariIni;
    Button btnSelectAfdBlock;
    CardView footerCard;
    LinearLayout llheadumano;
//    BoomMenuButton bmb;
//    SegmentedButtonGroup segmentedButtonGroup;
    public CoordinatorLayout coor;

    Button btnCancelTph;
    Button btnCancelTransit;

    View tph_new;
    View langsiran_new;
    View pks_new;

    TextView sheetHeader;
    TextView tvDate;
    TextView tvLocation;
    ImageView ivchevronup;
    ImageView ivchevrondown;
    ImageView ivchevronupAfd;
    ImageView ivchevrondownAfd;

    CardView cvToken;
    TextView tvToken;

    Location location;

    TextView footerEstate;
    TextView footerUser;

    Snackbar sbGPS;
    boolean isMapLoaded=false;

    public int countTph =0;
    public int countTphResurvey =0;
    public int countLangsiran =0;
    public int countHariIni =0;

    boolean manual;
    public GraphicsLayer graphicsLayer;
    public GraphicsLayer graphicsLayerTPH;
    public GraphicsLayer graphicsLayerTPHResurvey;
    public GraphicsLayer graphicsLayerLangsiran;
    public GraphicsLayer graphicsLayerText;
    public GraphicsLayer graphicsLayerToday;

    public HashMap<Integer,Integer> hashpointToday = new HashMap<>();
    public HashMap<Integer,Integer> hashText = new HashMap<>();
    public Callout callout;

    public SpatialReference spatialReference;
    public ImageView ivCrosshair;
    public LinearLayout ll_latlon;
    public TextView tv_lat_manual,tv_lon_manual,tv_dis_manual,tv_deg_manual;

    public TphHelper tphHelper;
    public String locKmlMap;
    public SetUpDataSyncHelper setUpDataSyncHelper;
    UploadHelper uploadHelper;
    RestoreHelper restoreHelper;
    SyncHistoryHelper syncHistoryHelper;

    public TreeMap<String,ArrayList<Block>> afdelingBlocks;
    ArrayList<Polygon> blocksSelected;
    AfdelingBlockHelper afdelingBlockHelper;

    boolean isTph;
    int idGrapTemp;
    int idGrapSelected;

    ArrayList<File> ALselectedImage;

    JSONObject objSetUp;
    ServiceResponse responseApi;
    public AlertDialog alertDialog;
    SimpleDateFormat dateFormat;

    @Override
    protected void initPresenter() {}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LicenseResult licenseResult = ArcGISRuntime.setClientId("uFfCzKhpVfi0ggjz");
        LicenseLevel licenseLevel = ArcGISRuntime.License.getLicenseLevel();
        Log.e("License Result", licenseResult.toString() + " | " + licenseLevel.toString());
        setContentView(R.layout.map_activity);
        getSupportActionBar().hide();

        mLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        rl_main = (RelativeLayout) findViewById(R.id.rl_main);
        mapView = (MapView) findViewById(R.id.map);
        topLayout = (RelativeLayout) findViewById(R.id.topLayout);
        container = (RelativeLayout) findViewById(R.id.container);
        fabCurrentLocation = (FloatingActionButton) findViewById(R.id.fabZoomToLocation);
        fabEstateLocation = (FloatingActionButton) findViewById(R.id.fabZoomToEstate);
        cvAfdBlock = (CardView) findViewById(R.id.cvAfdBlock);
        rvAfdBlock = (RecyclerView) findViewById(R.id.rvAfdBlock);
        checkBoxTph = (CheckBox) findViewById(R.id.checkBoxTph);
        checkBoxTphResurvey = (CheckBox) findViewById(R.id.checkBoxTphResurvey);
        checkBoxLangsir = (CheckBox) findViewById(R.id.checkBoxLangsir);
        checkBoxInputHariIni = (CheckBox) findViewById(R.id.checkBoxInputHariIni);
        btnSelectAfdBlock = (Button) findViewById(R.id.btnSelectAfdBlock);
        footerCard = (CardView) findViewById(R.id.footer_card);
//        bmb = (BoomMenuButton) findViewById(R.id.bmb);
        btnShowForm = (RelativeLayout) findViewById(R.id.btnShowForm);
        llheadumano = (LinearLayout) findViewById(R.id.llheadumano);
        coor = (CoordinatorLayout) findViewById(R.id.myCoordinatorLayout);

        tph_new = (View) findViewById(R.id.tph_new);
        langsiran_new = (View) findViewById(R.id.langsiran_new);
        pks_new = (View) findViewById(R.id.pks_new);

        //segmentedButtonGroup = (SegmentedButtonGroup) findViewById(R.id.segmentedButtonGroup);
        sheetHeader = (TextView) findViewById(R.id.sheetHeader);
        tvDate = (TextView) findViewById(R.id.tvDate);
        tvLocation = (TextView) findViewById(R.id.tvLocation);
        ivchevronup = (ImageView) findViewById(R.id.ivchevronup);
        ivchevrondown = (ImageView) findViewById(R.id.ivchevrondown);
        ivchevronupAfd = (ImageView) findViewById(R.id.ivchevronupAfd);
        ivchevrondownAfd = (ImageView) findViewById(R.id.ivchevrondownAfd);

        btnCancelTph = (Button) findViewById(R.id.btnCancel);
        btnCancelTransit = (Button) findViewById(R.id.btnCancelLangsiran);

        footerEstate = (TextView) findViewById(R.id.footerEstate);
        footerUser = (TextView) findViewById(R.id.footerUser);

        ivCrosshair = (ImageView) findViewById(R.id.iv_crosshair);
        ll_latlon = (LinearLayout) findViewById(R.id.ll_latlon);
        tv_lat_manual = (TextView) findViewById(R.id.tv_lat_manual);
        tv_lon_manual = (TextView) findViewById(R.id.tv_lon_manual);
        tv_deg_manual = (TextView) findViewById(R.id.tv_deg_manual);
        tv_dis_manual = (TextView) findViewById(R.id.tv_dis_manual);

        cvToken = (CardView) findViewById(R.id.cvToken);
        tvToken = (TextView) findViewById(R.id.tvToken);

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        try {
            Long exptDate = sdf.parse(GlobalHelper.getUser().getExpirationDate()).getTime();
            long sisaHari = exptDate - System.currentTimeMillis();
            sisaHari = sisaHari / (24 * 60 * 60 * 1000);
            if(sisaHari <= GlobalHelper.NOTIF_TOKEN_DAYS){
                tvToken.setText(sisaHari+" "+getResources().getString(R.string.days_left));
            }else{
                cvToken.setVisibility(View.GONE);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        afdelingBlockHelper = new AfdelingBlockHelper(this);
        dateFormat = new SimpleDateFormat("dd-MMMM-yyyy");

        graphicsLayer = new GraphicsLayer();
        graphicsLayerTPH = new GraphicsLayer();
        graphicsLayerTPHResurvey = new GraphicsLayer();
        graphicsLayerLangsiran = new GraphicsLayer();
        graphicsLayerText = new GraphicsLayer();
        graphicsLayerToday =new GraphicsLayer();

        manual = false;
        tphHelper = new TphHelper(MapActivity.this);
        setUpDataSyncHelper = new SetUpDataSyncHelper(MapActivity.this);
        uploadHelper = new UploadHelper(MapActivity.this);
        restoreHelper = new RestoreHelper(MapActivity.this);
        syncHistoryHelper = new SyncHistoryHelper(MapActivity.this);

        mLayout.setTouchEnabled(false);
        llheadumano.setVisibility(View.GONE);
        ivchevrondownAfd.setVisibility(View.GONE);

        footerUser.setText(GlobalHelper.getUser().getUserFullName());
        footerEstate.setText(GlobalHelper.getEstate().getCompanyShortName() + " - "
                + GlobalHelper.getEstate().getEstName());
        fabEstateLocation.setImageDrawable(new
                IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_landscape).sizeDp(24)
                .colorRes(R.color.White));
        fabCurrentLocation.setImageDrawable(new
                IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_my_location)
                .sizeDp(24)
                .colorRes(R.color.White));
        ivchevronup.setImageDrawable(new
                IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_chevron_up)
                .sizeDp(18)
                .colorRes(R.color.Gray));
        ivchevrondown.setImageDrawable(new
                IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_chevron_down)
                .sizeDp(18)
                .colorRes(R.color.Gray));
        ivchevronupAfd.setImageDrawable(new
                IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_chevron_up)
                .sizeDp(18)
                .colorRes(R.color.Gray));
        ivchevrondownAfd.setImageDrawable(new
                IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_chevron_down)
                .sizeDp(18)
                .colorRes(R.color.Gray));
        ivchevronupAfd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rvAfdBlock.setVisibility(View.GONE);
                ivchevronupAfd.setVisibility(View.GONE);
                ivchevrondownAfd.setVisibility(View.VISIBLE);
            }
        });
        ivchevrondownAfd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rvAfdBlock.setVisibility(View.VISIBLE);
                ivchevronupAfd.setVisibility(View.VISIBLE);
                ivchevrondownAfd.setVisibility(View.GONE);
            }
        });
        fabEstateLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mapView.isLoaded()){
                    if(tiledLayer!=null){
                        mapView.setExtent(tiledLayer.getFullExtent());
                    }
                }
            }
        });
        mapView.setOnSingleTapListener(new OnSingleTapListener() {
            @Override
            public void onSingleTap(float v, float v1) {
                if(isMapLoaded){
                    try {
                        defineTapSetup(v, v1);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        });
        fabCurrentLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mapView.isLoaded()){
                    if( locationDisplayManager!=null) {
                        if (locationDisplayManager.getLocation() != null)
                            zoomToLocation(locationDisplayManager.getLocation().getLatitude()
                                    , locationDisplayManager.getLocation().getLongitude(), spatialReference);

                        if (idGrapTemp != 0) {
                            location = locationDisplayManager.getLocation();
                            ((BaseActivity) MapActivity.this).currentlocation = locationDisplayManager.getLocation();
                            addGrapTemp(location.getLatitude(), location.getLongitude());
                        }
                    }
                }

            }
        });
        checkBoxTph.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                graphicsLayerTPH.setVisible(isChecked);
                if(isChecked && checkBoxInputHariIni.isChecked()){
                    checkBoxInputHariIni.setChecked(false);
                }
            }
        });
        checkBoxTphResurvey.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                graphicsLayerTPHResurvey.setVisible(isChecked);
                if(isChecked && checkBoxInputHariIni.isChecked()){
                    checkBoxInputHariIni.setChecked(false);
                }
            }
        });
        checkBoxLangsir.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                graphicsLayerLangsiran.setVisible(isChecked);
                if(isChecked && checkBoxInputHariIni.isChecked()){
                    checkBoxInputHariIni.setChecked(false);
                }
            }
        });
        checkBoxInputHariIni.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                graphicsLayerToday.setVisible(isChecked);
                if(isChecked){
                    checkBoxLangsir.setChecked(false);
                    checkBoxTphResurvey.setChecked(false);
                    checkBoxTph.setChecked(false);
                }else{
                    checkBoxLangsir.setChecked(true);
                    checkBoxTphResurvey.setChecked(true);
                    checkBoxTph.setChecked(true);
                }
            }
        });
        btnSelectAfdBlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                afdelingBlockHelper.setUpTreeeView();
            }
        });
//        segmentedButtonGroup.setOnClickedButtonListener(new SegmentedButtonGroup.OnClickedButtonListener() {
//            @Override
//            public void onClickedButton(int position) {
//                switch (position){
//                    case 0:{
//                        manual = false;
//                        switchCrossHairUI(false);
//                        double lon = 0;
//                        double lat = 0;
//                        if (isTph) {
//                            lat = tphHelper.latitude;
//                            lon = tphHelper.longitude;
//                        } else {
//                            lat = tujuanHelper.latitude;
//                            lon = tujuanHelper.longitude;
//                        }
//
//                        JSONObject object = new JSONObject();
//                        try {
//                            object.put("lat", lat);
//                            object.put("lon",lon);
//
//                            SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.LATLON_PENDAFTARAN, MODE_PRIVATE);
//                            SharedPreferences.Editor editor = preferences.edit();
//                            editor.putString(HarvestApp.LATLON_PENDAFTARAN,object.toString());
//                            editor.apply();
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//
//                        addGrapTemp(lat,lon);
//                        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
//                        break;
//                    }
//                    case 1:{
//                        manual = true;
//                        setDefaultManual();
//                        break;
//                    }
//                }
//            }
//        });
        mLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                if(slideOffset == 0){
                    mLayout.setTouchEnabled(true);
                }
                Log.i("MapActivity", "onPanelSlide, offset " + slideOffset);
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {

                if(footerCard.getVisibility() == View.VISIBLE){
                    mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                }
                if(manual && newState ==  SlidingUpPanelLayout.PanelState.EXPANDED){
                    mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                    Toast.makeText(HarvestApp.getContext(),getResources().getString(R.string.please_stop_manual_point),Toast.LENGTH_SHORT).show();
                }
                if(newState ==  SlidingUpPanelLayout.PanelState.EXPANDED){
                    ivchevronup.setVisibility(View.GONE);
                    ivchevrondown.setVisibility(View.VISIBLE);
                }else if (newState ==  SlidingUpPanelLayout.PanelState.ANCHORED){
                    ivchevronup.setVisibility(View.GONE);
                    ivchevrondown.setVisibility(View.VISIBLE);
                }else if (newState ==  SlidingUpPanelLayout.PanelState.COLLAPSED){
                    ivchevronup.setVisibility(View.VISIBLE);
                    ivchevrondown.setVisibility(View.GONE);
                }
            }
        });
        btnShowForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED){
                    mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                }if(mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.ANCHORED){
                    mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                }else if (mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED){
                    mLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                }
            }
        });
//        mLayout.setFadeOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
//            }
//        });

        mapSetup();
        checkBoxInputHariIni.setChecked(false);
        new LongOperation().execute(String.valueOf(LongOperation_Setup_Data_AfdelingBlock));
    }

    public void setUpFormTph(TPH tph){
        if(locationDisplayManager.getLocation() == null) {
            sbGPS = Snackbar.make(coor, "Searching GPS", Snackbar.LENGTH_INDEFINITE);
            sbGPS.show();
            return;
        }
        LogIntervalStep("getLocation");
        llheadumano.setVisibility(View.VISIBLE);
        mLayout.setTouchEnabled(true);
        mLayout.setPanelHeight(Utils.convertDpToPx(this,65));

        location = locationDisplayManager.getLocation();
        footerCard.setVisibility(View.GONE);
//        bmb.setVisibility(View.GONE);
        cvAfdBlock.setVisibility(View.GONE);
        //segmentedButtonGroup.setVisibility(View.VISIBLE);

        graphicsLayerTPH.setVisible(false);
        graphicsLayerTPHResurvey.setVisible(false);
        graphicsLayerLangsiran.setVisible(false);
        graphicsLayerText.setVisible(false);
        graphicsLayer.removeAll();
        idGrapTemp = 0;

        isTph = true;
        tph_new.setVisibility(View.VISIBLE);
        langsiran_new.setVisibility(View.GONE);
        pks_new.setVisibility(View.GONE);
        btnCancelTph.setVisibility(View.VISIBLE);

        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.ANCHORED);

        SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd-MMMM-yyyy");

        sheetHeader.setText(tph == null ? getResources().getString(R.string.new_tph): tph.getNamaTph());
        tvDate.setText(sdf.format(System.currentTimeMillis()));
        tvLocation.setText(String.format("Lat : %.5f | Lon : %.5f | Acc : %.0f", location.getLatitude(),
                location.getLongitude(), location.getAccuracy()));
        LogIntervalStep("prepareSetUpUmano");

        // jika dari map popupset latlon seusia denggan tph yang di pilih
        if(tph == null) {
            idGrapSelected = 0;
            tphHelper.latitude = location.getLatitude();
            tphHelper.longitude = location.getLongitude();
            tphHelper.latawal = location.getLatitude();
            tphHelper.longawal = location.getLongitude();
        }
        tphHelper.setupFormInput(tph_new,tph);
        LogIntervalStep("setUpUmanoDone");

        addGrapTemp(tphHelper.latawal,tphHelper.longawal);
        LogIntervalStep("addGrapTemp");

        zoomToLocation(tphHelper.latawal,tphHelper.longawal,spatialReference);

        LogIntervalStep("zoomToLocation");
    }

    public void closeForm(int stat){
        footerCard.setVisibility(View.VISIBLE);
//        bmb.setVisibility(View.VISIBLE);
        cvAfdBlock.setVisibility(View.VISIBLE);
        //segmentedButtonGroup.setVisibility(View.GONE);
        llheadumano.setVisibility(View.GONE);
        mLayout.setTouchEnabled(false);
        mLayout.setPanelHeight(footerCard.getHeight());
        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);

        graphicsLayer.removeAll();
        idGrapTemp = 0;
        graphicsLayerLangsiran.setVisible(checkBoxLangsir.isChecked());
        graphicsLayerTPH.setVisible(checkBoxTph.isChecked());
        graphicsLayerTPHResurvey.setVisible(checkBoxTphResurvey.isChecked());
        graphicsLayerText.setVisible(true);

        updateCounterKet();
        idGrapSelected = 0;
        isTph = false;
    }

    public void setAfdelingBlocksValue(){
        rvAfdBlock.setLayoutManager(new LinearLayoutManager(this));
        SelectedAfdelingBlock adapter = new SelectedAfdelingBlock(this,afdelingBlocks);
        rvAfdBlock.setAdapter(adapter);
        Log.e("setAfdelingBlocksValue","1");
        if (alertDialog != null) {
            alertDialog.dismiss();
        }
        new LongOperation().execute(String.valueOf(LongOperation_Setup_Poin));

        Log.e("setAfdelingBlocksValue","2");
    }

    public void addGrapTemp(Double lat, Double lon){
        if(mapView!=null && mapView.isLoaded()){
            Point fromPoint = new Point(lon, lat);
            Point toPoint = (Point) GeometryEngine.project(fromPoint, SpatialReference.create(SpatialReference.WKID_WGS84), spatialReference);

            CompositeSymbol cms = new CompositeSymbol();
            SimpleMarkerSymbol sms = new SimpleMarkerSymbol(Color.GREEN, Utils.convertDpToPx(this, GlobalHelper.MARKER_SIZE), SimpleMarkerSymbol.STYLE.X);
            SimpleLineSymbol sls = new SimpleLineSymbol(Color.GREEN, GlobalHelper.GARISTEPI_SIZE, SimpleLineSymbol.STYLE.SOLID);
            sms.setOutline(sls);
            cms.add(sms);
            Graphic pointGraphic = new Graphic(toPoint, cms);
            LogIntervalStep("addGrapTemp 1");
            if(isTph) {
                tphHelper.setupBlokAfdeling(lat, lon);
            }
            if(idGrapTemp!= 0){
                graphicsLayer.removeGraphic(idGrapTemp);
            }
            LogIntervalStep("addGrapTemp 2");
            idGrapTemp = graphicsLayer.addGraphic(pointGraphic);
            LogIntervalStep("addGrapTemp 3");
        }else{
            idGrapTemp = 0;
        }
    }


    public void setupPoint(){
        removeAllGraphic();

        countTph =0;
        countLangsiran =0;
        countTphResurvey =0;
        countHariIni = 0;
        ArrayList<String> list = new ArrayList<>();
        blocksSelected = new ArrayList<>();

        for ( TreeMap.Entry<String,ArrayList<Block>> entry : afdelingBlocks.entrySet() ) {
            for(int i = 0; i < entry.getValue().size();i++){
                if(entry.getValue().get(i).getSelected()){
                    list.add(entry.getValue().get(i).getBlock());
                    for (int j = 0; j < entry.getValue().get(i).getPolygons().size();j++) {
                        blocksSelected.add(entry.getValue().get(i).getPolygons().get(j));
                    }
                }
            }
        }

        ArrayList<TPH> tphArrayList = tphHelper.setDataTph(GlobalHelper.getEstate().getEstCode(),list);
        JSONArray array = new JSONArray();
        for(TPH tph: tphArrayList){
            addGraphicTph(tph);
            if(tph.getResurvey() == 1){
                countTphResurvey++;
            }else {
                if(tph.getStatus() != TPH.STATUS_TPH_INACTIVE_QC_MOBILE && tph.getStatus() != TPH.STATUS_TPH_DELETED) {
                    countTph++;
                }
            }

            if(tph.getStatus() == TPH.STATUS_TPH_BELUM_UPLOAD){
                countHariIni++;
            }
            Gson gson = new Gson();
            try {
                array.put(new JSONObject(gson.toJson(tph)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        GlobalHelper.writeFileContent(GlobalHelper.getDatabasePathHMSTemp() + "/maptph.json",array.toString());

        if(blocksSelected.size() == 1) {
            Polygon polygon = new Polygon();
            for(Polygon geometry:blocksSelected) {
                if (geometry != null) {
                    polygon = (Polygon) GeometryEngine.project(blocksSelected.get(0), SpatialReference.create(SpatialReference.WKID_WGS84), mapView.getSpatialReference());
                    break;
                }
            }
            mapView.setExtent(polygon);
        }else if (blocksSelected.size() > 1){
            Polygon[] geometries = new Polygon[blocksSelected.size()];
            for (int i = 0 ; i < blocksSelected.size(); i++) {
                Polygon geometry = blocksSelected.get(i);
                if (geometry != null) {
                    Polygon polygon = (Polygon) GeometryEngine.project(geometry,
                            SpatialReference.create(SpatialReference.WKID_WGS84),
                            mapView.getSpatialReference());
                    geometries[i]= polygon;
                }
            }
            mapView.setExtent(GeometryEngine.union(geometries,mapView.getSpatialReference()));
        }else{
            if(tiledLayer!=null){
                mapView.setExtent(tiledLayer.getFullExtent());
            }
        }
    }

    public void addGraphicTph(TPH tph){

        Point fromPoint = new Point(tph.getLongitude(), tph.getLatitude());
        Point toPoint = (Point) GeometryEngine.project(fromPoint, SpatialReference.create(SpatialReference.WKID_WGS84), spatialReference);

        CompositeSymbol cms = new CompositeSymbol();
        int color = tph.getResurvey() == 1 ? TPH.COLOR_TPH_RESURVEY : TPH.COLOR_TPH;
        SimpleMarkerSymbol sms = new SimpleMarkerSymbol(color, Utils.convertDpToPx(this, GlobalHelper.MARKER_SIZE), SimpleMarkerSymbol.STYLE.CIRCLE);
        SimpleLineSymbol sls = new SimpleLineSymbol(TPH.COLOR_BELUMUPLOAD, GlobalHelper.GARISTEPI_SIZE, SimpleLineSymbol.STYLE.SOLID);
        if(tph.getStatus() == TPH.STATUS_TPH_MASTER || tph.getStatus() == TPH.STATUS_TPH_UPLOAD){
            sls = new SimpleLineSymbol(TPH.COLOR_UPLOAD, GlobalHelper.GARISTEPI_SIZE, SimpleLineSymbol.STYLE.SOLID);
        }
        sms.setOutline(sls);
        cms.add(sms);

        Graphic pointGraphic = new Graphic(toPoint, cms);
        int idgrap = 0;
        if(tph.getResurvey() == 1) {
            idgrap = graphicsLayerTPHResurvey.addGraphic(pointGraphic);
        }else{
            if(tph.getStatus() != TPH.STATUS_TPH_INACTIVE_QC_MOBILE && tph.getStatus() != TPH.STATUS_TPH_DELETED) {
                idgrap = graphicsLayerTPH.addGraphic(pointGraphic);
            }
        }
//        hashpoint.put(idgrap,tpHnLangsiran);

        if(tph.getStatus() == TPH.STATUS_TPH_BELUM_UPLOAD){
            hashpointToday.put(idgrap,graphicsLayerToday.addGraphic(pointGraphic));
        }

        TextSymbol.HorizontalAlignment horizontalAlignment = TextSymbol.HorizontalAlignment.CENTER;
        TextSymbol.VerticalAlignment verticalAlignment = TextSymbol.VerticalAlignment.BOTTOM;

        int Tcolor = Color.parseColor("#000000");
        TextSymbol txtSymbol = new TextSymbol(13,tph.getNamaTph(),Tcolor) ;
        txtSymbol.setHorizontalAlignment(horizontalAlignment);
        txtSymbol.setVerticalAlignment(verticalAlignment);

        pointGraphic = new Graphic(toPoint,txtSymbol);
        if(tph.getResurvey() == 1) {
            hashText.put(idgrap,graphicsLayerText.addGraphic(pointGraphic));
        }else{
            if(tph.getStatus() != TPH.STATUS_TPH_INACTIVE_QC_MOBILE && tph.getStatus() != TPH.STATUS_TPH_DELETED) {
                hashText.put(idgrap,graphicsLayerText.addGraphic(pointGraphic));
            }
        }
    }

    private void mapSetup(){
        //
        mapView.removeAll();
        graphicsLayer.removeAll();
        mapView.setMinScale(250000.0d);
        mapView.setMaxScale(1000.0d);
        mapView.enableWrapAround(true);

        String locTiledMap = GlobalHelper.decryptFiles(GlobalHelper.TYPE_VKM);
        if(locTiledMap != null){
            File file = new File(locTiledMap);
            if(!file.exists()){
                locTiledMap = null;
            }
        }

        if(locTiledMap != null){

            locKmlMap = GlobalHelper.decryptFiles(GlobalHelper.TYPE_KMLBLOCK);
            if (locKmlMap != null) {
                kmlLayer = new KmlLayer(locKmlMap);
//                kmlLayer.setOpacity(0.03f);
                mapView.addLayer(kmlLayer);
                kmlLayer.setVisible(false);
            }
            tiledLayer = new ArcGISLocalTiledLayer(locTiledMap, true);
            mapView.addLayer(tiledLayer);
        }else{
            locTiledMap = GlobalHelper.getFilePath(GlobalHelper.TYPE_VKM, GlobalHelper.getEstate());
            if (locTiledMap != null) {
                locTiledMap = GlobalHelper.decryptFiles(GlobalHelper.TYPE_VKM);
                tiledLayer = new ArcGISLocalTiledLayer(locTiledMap, true);
                mapView.addLayer(tiledLayer);
            }

            locKmlMap = GlobalHelper.decryptFiles(GlobalHelper.TYPE_KMLBLOCK);
            if(locKmlMap!=null){
                kmlLayer = new KmlLayer(locKmlMap);
//                kmlLayer.setOpacity(0.8f);
                mapView.addLayer(kmlLayer);
                kmlLayer.setVisible(true);
            }
        }

        mapView.setOnStatusChangedListener(new OnStatusChangedListener() {
            @Override
            public void onStatusChanged(Object o, STATUS status) {
                if(o==mapView && status==STATUS.INITIALIZED){
                    spatialReference = mapView.getSpatialReference();
                    isMapLoaded = true;
                    locationListenerSetup();
                    fabEstateLocation.callOnClick();
                }
            }
        });
        mapView.setOnPanListener(new OnPanListener() {
            @Override
            public void prePointerMove(float v, float v1, float v2, float v3) {
                if(mapView.isLoaded()){
                    if(mapView.getSpatialReference()!=null){
                        Point prePoint = mapView.getCenter();
                        Point point = (Point) GeometryEngine.project(prePoint,mapView.getSpatialReference(),SpatialReference.create(SpatialReference.WKID_WGS84));
                    }

                }
            }

            @Override
            public void postPointerMove(float v, float v1, float v2, float v3) {
                if(mapView.isLoaded()){
                    if(mapView.getSpatialReference()!=null){
                        if(manual) {
                            Point prePoint = mapView.getCenter();
                            Point point = (Point) GeometryEngine.project(prePoint, mapView.getSpatialReference(), SpatialReference.create(SpatialReference.WKID_WGS84));
                            double imeter = GlobalHelper.distance(location.getLatitude(), point.getY(), location.getLongitude(), point.getX());
                            double bearing = GlobalHelper.bearing(location.getLatitude(), point.getY(), location.getLongitude(), point.getX());
                            tv_lat_manual.setText(String.format("%.5f", point.getY()));
                            tv_lon_manual.setText(String.format("%.5f", point.getX()));
                            tv_dis_manual.setText(String.format("%.0f m", imeter));
                            tv_deg_manual.setText(String.format("%.0f", bearing));
                            if (point.getY() != 0.0 && point.getX() != 0.0) {
                                if (imeter <= GlobalHelper.MAX_RADIUS_MAPMENU) {
                                    if (isTph) {
                                        tphHelper.latitude = point.getY();
                                        tphHelper.longitude = point.getX();
                                    }
                                    ll_latlon.setBackgroundColor(Color.WHITE);
                                } else {
                                    ll_latlon.setBackgroundColor(Color.RED);
                                }
                            }
                        }
                    }

                }
            }

            @Override
            public void prePointerUp(float v, float v1, float v2, float v3) {

            }

            @Override
            public void postPointerUp(float v, float v1, float v2, float v3) {

            }
        });
        mapView.setOnZoomListener(new OnZoomListener() {
            @Override
            public void preAction(float v, float v1, double v2) {
                if(mapView.isLoaded()){
                    if(mapView.getSpatialReference()!=null){

                        Point prePoint = mapView.getCenter();
                        Point point = (Point) GeometryEngine.project(prePoint,mapView.getSpatialReference(),SpatialReference.create(SpatialReference.WKID_WGS84));
                    }
                }
            }

            @Override
            public void postAction(float v, float v1, double v2) {
                if(mapView.isLoaded()){
                    if(mapView.getSpatialReference()!=null){

                        Point prePoint = mapView.getCenter();
                        Point point = (Point) GeometryEngine.project(prePoint,mapView.getSpatialReference(),SpatialReference.create(SpatialReference.WKID_WGS84));
                        //ll_txt_latlon.setText(String.format("Lat\t: %.5f\t Lon\t: %.5f",point.getY(),point.getX()));
                    }

                }
            }
        });
        graphicsLayer.setMinScale(150000d);
        graphicsLayer.setMaxScale(1000d);
        graphicsLayerLangsiran.setMinScale(150000d);
        graphicsLayerLangsiran.setMaxScale(1000d);
        graphicsLayerTPHResurvey.setMinScale(150000d);
        graphicsLayerTPHResurvey.setMaxScale(1000d);
        graphicsLayerTPH.setMinScale(150000d);
        graphicsLayerTPH.setMaxScale(1000d);
        graphicsLayerText.setMinScale(15000d);
        graphicsLayerText.setMaxScale(1000d);
        graphicsLayerToday.setMaxScale(1000d);
        graphicsLayerToday.setMaxScale(1000d);
        mapView.addLayer(graphicsLayer);
        mapView.addLayer(graphicsLayerTPHResurvey);
        mapView.addLayer(graphicsLayerTPH);
        mapView.addLayer(graphicsLayerLangsiran);
        mapView.addLayer(graphicsLayerToday);
        mapView.addLayer(graphicsLayerText);
        mapView.invalidate();
        fabEstateLocation.callOnClick();
    }

    public void removeAllGraphic() {
        if(mapView!=null && mapView.isLoaded()){
            graphicsLayerLangsiran.removeAll();
            graphicsLayerTPHResurvey.removeAll();
            graphicsLayerTPH.removeAll();
            graphicsLayerText.removeAll();
            graphicsLayerToday.removeAll();
            hashText.clear();
            hashpointToday.clear();
        }
    }

    public void locationListenerSetup(){

        if(mapView!=null && mapView.isLoaded()){

            sbGPS = Snackbar.make(coor,"Searching GPS",Snackbar.LENGTH_INDEFINITE);

            sbGPS.show();
            locationDisplayManager = mapView.getLocationDisplayManager();
            locationDisplayManager.setAllowNetworkLocation(false);
            locationDisplayManager.setAccuracyCircleOn(true);
            locationDisplayManager.setLocationListener(new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {

                    ((BaseActivity) MapActivity.this).currentlocation = location;
                    if(sbGPS!=null){
                        if(sbGPS.isShown())sbGPS.dismiss();
                        if(location.getAccuracy()>GlobalHelper.MAX_ACCURACY_MAPMENU){
                            sbGPS = Snackbar.make(coor,"GPS Tidak akurat",Snackbar.LENGTH_INDEFINITE).setActionTextColor(Color.YELLOW);
                            //Log.e("curr acc",locationStation.getAccuracy()+" m");
                            sbGPS.show();
                        }else{
                            if(sbGPS!=null) {
                                if (sbGPS.isShown()) sbGPS.dismiss();
                            }
                        }
                    }
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {

                }

                @Override
                public void onProviderEnabled(String s) {
                    if(sbGPS!=null)if(sbGPS.isShown())sbGPS.dismiss();
                }

                @Override
                public void onProviderDisabled(String s) {
                    sbGPS = Snackbar.make(coor,"No GPS Found",Snackbar.LENGTH_INDEFINITE)
                            .setAction("Turn On", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                }
                            });
                    sbGPS.show();
                    final AlertDialog.Builder builder = new AlertDialog.Builder(MapActivity.this);
                    builder.setMessage("GPS anda tidak hidup! Apakah anda ingin menghidupkan GPS?")
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();

                                }
                            });
                    final AlertDialog alert = builder.create();
                    alert.show();

                }


            });
            locationDisplayManager.start();
        }
    }

    public void zoomToLocation(double lat, double lon, SpatialReference spatialReference) {
        Point mapPoint = GeometryEngine.project(lon,lat,spatialReference);
        try {
            mapView.centerAt(mapPoint,true);
            mapView.zoomTo(mapPoint,15000f);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void defineTapSetup(float x,float y){
        List<Integer> grapIdSelected = new ArrayList<>();
        int[] graphicIDs;
        if(graphicsLayerTPH.isVisible()) {
            graphicIDs = graphicsLayerTPH.getGraphicIDs(x, y, Utils.convertDpToPx(this, GlobalHelper.MARKER_TOUCH_SIZE));
            if (graphicIDs.length > 0) {
                for (int gid : graphicIDs) {
                    grapIdSelected.add(gid);
//                for (HashMap.Entry<Integer, TPHnLangsiran> entry : hashpoint.entrySet()) {
//                    if(gid == entry.getKey()){
//                        tphList.add(entry.getValue());
//                    }
//                }
                }
            }
        }

        if(graphicsLayerTPHResurvey.isVisible()) {
            graphicIDs = graphicsLayerTPHResurvey.getGraphicIDs(x, y, Utils.convertDpToPx(this, GlobalHelper.MARKER_TOUCH_SIZE));
            if (graphicIDs.length > 0) {
                for (int gid : graphicIDs) {
                    grapIdSelected.add(gid);
                }
            }
        }

        if(graphicsLayerLangsiran.isVisible()) {
            graphicIDs = graphicsLayerLangsiran.getGraphicIDs(x, y, Utils.convertDpToPx(this, GlobalHelper.MARKER_TOUCH_SIZE));
            if (graphicIDs.length > 0) {
                for (int gid : graphicIDs) {
                    grapIdSelected.add(gid);
//                for (HashMap.Entry<Integer, TPHnLangsiran> entry : hashpoint.entrySet()) {
//                    if(gid == entry.getKey()){
//                        tphList.add(entry.getValue());
//                    }
//                }
                }
            }
        }

        if(graphicsLayerToday.isVisible()){
            graphicIDs = graphicsLayerToday.getGraphicIDs(x, y, Utils.convertDpToPx(this, GlobalHelper.MARKER_TOUCH_SIZE));
            if (graphicIDs.length > 0) {
                for (int gid : graphicIDs) {
                    for (Map.Entry<Integer, Integer> entry : hashpointToday.entrySet()) {
                        if (gid == entry.getValue()) {
//                            tphList.add(hashpoint.get(entry.getKey()));
                            break;
                        }
                    }
                    grapIdSelected.add(gid);
                }
            }
        }

        calloutSetup();
        if (callout.isShowing()) {
            callout.hide();
        } else {
            if (grapIdSelected.size() == 1) {
                //jika dalam satu tap hanya ada satu titik maka kesini
                int idx = -1;
                String graptype = "";
                Graphic graphic = graphicsLayerTPH.getGraphic(grapIdSelected.get(0));
                if(graphic == null){
                    graphic = graphicsLayerTPHResurvey.getGraphic(grapIdSelected.get(0));
                }
                if(graphic == null){
                    graphic = graphicsLayerLangsiran.getGraphic(grapIdSelected.get(0));
                }
                if(graphic == null){
                    graphic = graphicsLayerToday.getGraphic(grapIdSelected.get(0));
                }
                if (graphic.getGeometry() instanceof Point) {
                    idx = grapIdSelected.get(0);
                }
//                showInfoWindow(idx,tphList.get(0));
            }else if (grapIdSelected.size() > 1){
//                showChoseInfoWindow(grapIdSelected, tphList);
            }
        }
    }

    private void calloutSetup() {
        this.callout = mapView.getCallout();
        this.callout.setStyle(R.xml.statisticspop);
        if (this.callout.isShowing()) {
            this.callout.hide();
        }
    }

    private void showChoseInfoWindow(List<Integer> graphicIDs,List<TPH> tphList){
        ArrayList<TPH> windowChoseArrayList = new ArrayList<>();
        GraphicsLayer layer = new GraphicsLayer();
        for (int i = 0; i < graphicIDs.size(); i++) {
            Graphic graphic = graphicsLayerTPH.getGraphic(graphicIDs.get(i));
            layer = graphicsLayerTPH;
            int idx =0;
            if(graphic == null){
                graphic = graphicsLayerTPHResurvey.getGraphic(graphicIDs.get(i));
                layer = graphicsLayerTPHResurvey;
            }else if(graphic == null){
                graphic = graphicsLayerLangsiran.getGraphic(graphicIDs.get(i));
                layer = graphicsLayerLangsiran;
            }
            if(graphic == null){
                return;
            }
            if (graphic.getGeometry() instanceof Point) {
                idx = graphicIDs.get(i);
            }

            if(idx != 0){
//                for (HashMap.Entry<Integer, TPHnLangsiran> entry : hashpoint.entrySet()) {
//                    if(idx == entry.getKey()){
//                        windowChoseArrayList.add(hashpoint.get(idx));
//                    }
//                }
            }
        }

        Collections.sort(windowChoseArrayList, new Comparator<TPH>() {
            @Override
            public int compare(TPH o1, TPH o2) {
                return o1.getNoTph().compareTo(o2.getNoTph());
            }
        });

        if(windowChoseArrayList.size()>1){
            try {
//                ViewGroup content = InfoWindowChose.setupInfoWindowsChoseTphnLangsiran(this, windowChoseArrayList);
//                callout.setContent(content);
                GraphicsLayer gl = graphicsLayerTPH;
                Graphic graphic = gl.getGraphic(graphicIDs.get(graphicIDs.size() -1));
                if(graphic == null){
                    gl = graphicsLayerTPHResurvey;
                    graphic = gl.getGraphic(graphicIDs.get(graphicIDs.size() -1));
                }else if(graphic == null){
                    gl = graphicsLayerLangsiran;
                    graphic = gl.getGraphic(graphicIDs.get(graphicIDs.size() -1));
                }else if (graphic == null){
                    gl = graphicsLayerToday;
                    graphic = gl.getGraphic(graphicIDs.get(graphicIDs.size() -1));
                }


                int idx =0;
//                if (graphic.getGeometry() instanceof Point) {
//                    idx = graphicIDs.get(graphicIDs.size() -1);
//                }

                Point realpoin = (Point) gl.getGraphic(idx).getGeometry();

                Point wgs84poin = (Point) GeometryEngine.project(realpoin,
                        gl.getSpatialReference(),
                        SpatialReference.create(SpatialReference.WKID_WGS84));
                Point mapPoint = (Point) GeometryEngine.project(wgs84poin,
                        SpatialReference.create(SpatialReference.WKID_WGS84),
                        mapView.getSpatialReference());
                callout.setCoordinates(mapPoint);
                mapView.centerAt(mapPoint,true);
//                callout.animatedShow(mapPoint,content);
            }catch (Exception e){
                e.printStackTrace();
            }
        }else if (windowChoseArrayList.size() == 1){
//            showInfoWindow(graphicIDs.get(0),windowChoseArrayList.get(0));
        }
    }

    public void showInfoWindow(int idx,TPH tph){

        ViewGroup content = null;
        Point realpoin = null;
        Point mapPoint = null;
        if(tph.getResurvey() == 1 ){
//            content = InfoWindow.setupInfoWindows(this, tph.getTph());
            realpoin = (Point) graphicsLayerTPHResurvey.getGraphic(idx).getGeometry();
            Point wgs84poin = (Point) GeometryEngine.project(realpoin,
                    graphicsLayerTPHResurvey.getSpatialReference(),
                    SpatialReference.create(SpatialReference.WKID_WGS84));
            mapPoint = (Point) GeometryEngine.project(wgs84poin,
                    SpatialReference.create(SpatialReference.WKID_WGS84),
                    mapView.getSpatialReference());
        }else {
            GraphicsLayer layer;
            if(graphicsLayerTPH.isVisible()){
                layer = graphicsLayerTPH;
            }else{
                layer = graphicsLayerToday;
            }
//                content = InfoWindow.setupInfoWindows(this, tph.getTph());
            realpoin = (Point) layer.getGraphic(idx).getGeometry();
            Point wgs84poin = (Point) GeometryEngine.project(realpoin,
                    layer.getSpatialReference(),
                    SpatialReference.create(SpatialReference.WKID_WGS84));
            mapPoint = (Point) GeometryEngine.project(wgs84poin,
                    SpatialReference.create(SpatialReference.WKID_WGS84),
                    mapView.getSpatialReference());
        }

        if(mapPoint!= null) {
            callout.setContent(content);
            callout.setCoordinates(mapPoint);
            mapView.centerAt(mapPoint,true);
            mapView.zoomTo(mapPoint,15000f);
            callout.animatedShow(mapPoint,content);
        }
    }

    private void setupDataAfdelingBlock(){
        afdelingBlocks = GlobalHelper.getAllAfdelingBlock(new File(locKmlMap));
        if(locationDisplayManager != null && locationDisplayManager.getLocation() != null) {
            String[] blockAfdeling = GlobalHelper.getAndAssignBlock(new File(locKmlMap),
                    locationDisplayManager.getLocation().getLatitude(),
                    locationDisplayManager.getLocation().getLongitude()).split(";");
            for (TreeMap.Entry<String,ArrayList<Block>> entry : afdelingBlocks.entrySet()) {
                String key = entry.getKey();
                ArrayList<Block> blocks = afdelingBlocks.get(key);
                for (int i = 0; i < blocks.size(); i++) {
                    try {
                        if ((key.equals(blockAfdeling[1])) && (blocks.get(i).getBlock().equals(blockAfdeling[2]))) {

                            Block block = new Block(blockAfdeling[2], true,blocks.get(i).getPolygons());
                            blocks.remove(i);
                            blocks.add(block);
                            break;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                Collections.sort(blocks, new Comparator<Block>() {
                    @Override
                    public int compare(Block o1, Block o2) {
                        return o1.getBlock().compareToIgnoreCase(o2.getBlock());
                    }
                });

                afdelingBlocks.put(key, blocks);
            }
        }
    }

    private void setDefaultManual(){
        double lon = 0;
        double lat = 0;
        if (isTph) {
            lat = tphHelper.latitude;
            lon = tphHelper.longitude;
        }

        tv_lat_manual.setText(String.format("%.5f",lat));
        tv_lon_manual.setText(String.format("%.5f",lon));
        tv_deg_manual.setText("0");
        tv_dis_manual.setText("0 m");
        ll_latlon.setBackgroundColor(Color.WHITE);
        zoomToLocation(lat,lon,spatialReference);
        GlobalHelper.hideKeyboard(this);

        //switchCrossHairUI(true);
//        btnSave.setVisibility(View.VISIBLE);
    }

    public void updateCounterKet(){
        checkBoxTph.setText(String.format("Total Tph : %,d",countTph));
        checkBoxTphResurvey.setText(String.format("Total Tph Resurvey : %,d",countTphResurvey));
        checkBoxLangsir.setText(String.format("Total Langsiran : %,d",countLangsiran));
        checkBoxInputHariIni.setText(String.format("Blm Upload : %,d",countHariIni));

        checkBoxInputHariIni.setChecked(false);

        if (alertDialog != null) {
            alertDialog.dismiss();
        }
    }

    public void viewSelectedNetwork(){
        View dialogView = LayoutInflater.from(this).inflate(R.layout.selected_network, null);
        SegmentedButtonGroup sbgNetwork = dialogView.findViewById(R.id.segmentedButtonGroup);
        Button btnClose = dialogView.findViewById(R.id.btnClose);

        final AlertDialog dialog = new AlertDialog.Builder(this, R.style.MyAlertDialogStyle)
                .setView(dialogView)
                .setCancelable(false)
                .create();

        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, MODE_PRIVATE);
        if(preferences.getBoolean(HarvestApp.CONNECTION_PREF,false)){
            sbgNetwork.setPosition(SettingFragment.ButtonGroup_Public);
        }else{
            sbgNetwork.setPosition(SettingFragment.ButtonGroup_Lokal);
        }

        sbgNetwork.setOnClickedButtonListener(new SegmentedButtonGroup.OnClickedButtonListener() {
            @Override
            public void onClickedButton(int position) {
                switch (position){
                    case 0: {
                        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean(HarvestApp.CONNECTION_PREF, false);
                        editor.apply();
                        break;
                    }
                    case 1: {
                        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean(HarvestApp.CONNECTION_PREF, true);
                        editor.apply();
                        break;
                    }
                }
                dialog.dismiss();
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void backProses() {
        setResult(GlobalHelper.RESULT_MAPACTIVITY);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onBackPressed() {
        AlertDialog alertDialog = new AlertDialog.Builder(this,R.style.MyAlertDialogStyle)
                .setTitle("Perhatian")
                .setMessage("Yakin keluar Manu Map?")
                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                        backProses();
                    }
                })
                .create();
        alertDialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

//    public void showLongOperation(TPHnLangsiran tpHnLangsiran,String param){
//        new LongOperation(tpHnLangsiran).execute(param);
//    }

//    public void startLongOperation(int statLongOperation,TPHnLangsiran tpHnLangsiranSelected){
//        if(tpHnLangsiranSelected != null){
//            new LongOperation(tpHnLangsiranSelected).execute(String.valueOf(statLongOperation));
//        }else{
//            new LongOperation(statLongOperation).execute(String.valueOf(statLongOperation));
//        }
//    }

    public static void LogIntervalStep(String step){
//        Long lm = System.currentTimeMillis() - l1;
//        l1 = System.currentTimeMillis();
//        Log.d("new tph "+step, String.valueOf((lm / 1000) % 60) + " Second");
    }

    public class LongOperation extends AsyncTask<String, String, String> {
        int statLongOperation;
        boolean skip = false;
        Snackbar snackbar;
        boolean error = false;

        public LongOperation() {
        }

        public LongOperation(int statLongOperation) {
            this.statLongOperation = statLongOperation;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if(alertDialog != null){
                if(alertDialog.isShowing()){
                    alertDialog.dismiss();
                }
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    switch (statLongOperation){
                        default:
                            alertDialog = WidgetHelper.showWaitingDialog(MapActivity.this, getResources().getString(R.string.wait));
                    }
                }
            });

            objSetUp = new JSONObject();
            skip = false;
            error = false;

        }

        @Override
        protected String doInBackground(String... strings) {
            switch (Integer.parseInt(strings[0])) {
                case LongOperation_Setup_Data_AfdelingBlock:
                    setupDataAfdelingBlock();
                    break;
                case LongOperation_Setup_Poin:
                    Log.e("LongOperation","LongOperation_Setup_Poin dobackground");
                    setupPoint();
                    break;
            }
            return String.valueOf(Integer.parseInt(strings[0]));
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject objProgres = new JSONObject(String.valueOf(values[0]));

                        if(snackbar== null){
                            snackbar = Snackbar.make(coor,objProgres.getString("ket"),Snackbar.LENGTH_INDEFINITE);
                        }
                        snackbar.setText(String.format(objProgres.getString("ket") + " %d %s [%d/%d] ",
                                objProgres.getInt("persen"), "%", objProgres.getInt("count"), objProgres.getInt("total")));
                        snackbar.show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(snackbar != null){
                snackbar.dismiss();
            }
            if(result.equals("JSONException")){
                Toast.makeText(HarvestApp.getContext(),"Gagal Prepare Data Long Operation",Toast.LENGTH_LONG).show();
                alertDialog.dismiss();
            }else {
                switch (Integer.parseInt(result)) {
                    case LongOperation_Setup_Data_AfdelingBlock:
                        setAfdelingBlocksValue();
                        if (alertDialog != null) {
                            alertDialog.dismiss();
                        }
                        break;
                    case LongOperation_Setup_Poin:
                        Log.e("LongOperation","LongOperation_Setup_Poin finish");
                        updateCounterKet();
                        if (alertDialog != null) {
                            if(alertDialog.isShowing()) {
                                alertDialog.dismiss();
                            }
                        }

                        Log.e("LongOperation","LongOperation_Setup_Poin done");
                        break;
                    case LongOperation_Setup_FormTPH:
                        LogIntervalStep("lo");
//                        setUpFormTph(tpHnLangsiran != null ? tpHnLangsiran.getTph() : null);
                        if (alertDialog != null) {
                            alertDialog.dismiss();
                        }
                        break;
                }

            }
        }

        public void publishProgress(JSONObject objProgres){
            onProgressUpdate(String.valueOf(objProgres));
        }
    }
}
