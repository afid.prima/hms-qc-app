package id.co.ams_plantation.harvestqcsap.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.util.Log;

import com.google.gson.Gson;

import net.rehacktive.waspdb.WaspDb;
import net.rehacktive.waspdb.WaspFactory;
import net.rehacktive.waspdb.WaspHash;

import org.apache.commons.io.FileUtils;
import org.dizitart.no2.Nitrite;
import org.dizitart.no2.RecordIterable;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import cat.ereza.customactivityoncrash.model.CrashDetailAnalyticInfo;
import id.co.ams_plantation.harvestqcsap.Fragment.SettingFragment;
import id.co.ams_plantation.harvestqcsap.HarvestApp;
import id.co.ams_plantation.harvestqcsap.connection.ServiceResponse;
import id.co.ams_plantation.harvestqcsap.connection.Tag;
import id.co.ams_plantation.harvestqcsap.encryptor.Encrypts;
import id.co.ams_plantation.harvestqcsap.model.Answer;
import id.co.ams_plantation.harvestqcsap.model.AppInfo;
import id.co.ams_plantation.harvestqcsap.model.ChoiceSelection;
import id.co.ams_plantation.harvestqcsap.model.Estate;
import id.co.ams_plantation.harvestqcsap.model.HasilPanen;
import id.co.ams_plantation.harvestqcsap.model.Maps;
import id.co.ams_plantation.harvestqcsap.model.QcFotoPanen;
import id.co.ams_plantation.harvestqcsap.model.QuestionAnswer;
import id.co.ams_plantation.harvestqcsap.model.SessionQcFotoPanen;
import id.co.ams_plantation.harvestqcsap.model.SyncGroupEstate;
import id.co.ams_plantation.harvestqcsap.model.TransaksiPanen;
import id.co.ams_plantation.harvestqcsap.ui.ActiveActivity;
import id.co.ams_plantation.harvestqcsap.ui.BaseActivity;
import id.co.ams_plantation.harvestqcsap.ui.MainMenuActivity;
import id.co.ams_plantation.harvestqcsap.ui.MapActivity;
import id.co.ams_plantation.harvestqcsap.BuildConfig;
import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.model.DataNitrit;
import id.co.ams_plantation.harvestqcsap.model.GangList;
import id.co.ams_plantation.harvestqcsap.model.JanjangSensus;
import id.co.ams_plantation.harvestqcsap.model.LastSyncTime;
import id.co.ams_plantation.harvestqcsap.model.Pemanen;
import id.co.ams_plantation.harvestqcsap.model.QcAncak;
import id.co.ams_plantation.harvestqcsap.model.QcMutuBuah;
import id.co.ams_plantation.harvestqcsap.model.SensusBjr;
import id.co.ams_plantation.harvestqcsap.model.SubPemanen;
import id.co.ams_plantation.harvestqcsap.model.SupervisionList;
import id.co.ams_plantation.harvestqcsap.model.SyncHistroy;
import id.co.ams_plantation.harvestqcsap.model.SyncHistroyItem;
import id.co.ams_plantation.harvestqcsap.model.SyncTime;
import id.co.ams_plantation.harvestqcsap.model.TPH;
import ir.mahdi.mzip.zip.ZipArchive;

import static id.co.ams_plantation.harvestqcsap.util.GlobalHelper.encryptString;
import static id.co.ams_plantation.harvestqcsap.util.GlobalHelper.setupHMSFolder;

public class SetUpDataSyncHelper {
    Context context;
    String TAG =  this.getClass().getName();

    public SetUpDataSyncHelper(Context context) {
        this.context = context;
    }


    public ArrayList<AppInfo> setUpAllMapFilesByEstate(ServiceResponse responseApi, Object longOperation){
        try {

            ArrayList<AppInfo> mapFiles = new ArrayList<>();
            JSONArray jsonArray = new JSONArray(responseApi.getData().toString());
            Gson gson = new Gson();
            for(int i = 0 ;i < jsonArray.length();i++) {
                Maps map = gson.fromJson(jsonArray.get(i).toString(),Maps.class);
                if(map != null) {
                    if (map.getPrefixName().equals("BLOCK") || map.getPrefixName().equals("VKM")) {
                        AppInfo appInfo = new AppInfo();
                        appInfo.setMaps(map);
                        if (appInfo.getMaps() != null) {
                            appInfo.setUrl(DownloadMapHelper.getUri(appInfo));
                            appInfo.setFilePath(DownloadMapHelper.getFilePath(appInfo.getMaps()));
                            appInfo.setFileName(encryptString(appInfo.getMaps().getFilename()));
                            mapFiles.add(appInfo);
                        }
                    }
                }
            }
            return mapFiles;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String estCodeSyncNow() {
        if (context instanceof MainMenuActivity) {
            MainMenuActivity mainMenuActivity = ((MainMenuActivity) context);
            return mainMenuActivity.estatesMapping.get(mainMenuActivity.idxEstate).getEstCode();
        } else if (context instanceof ActiveActivity) {
            ActiveActivity activeActivity = ((ActiveActivity) context);
            return activeActivity.estatesMapping.get(activeActivity.idxEstate).getEstCode();
        } else {
            return GlobalHelper.getEstate().getEstCode();
        }
    }

    public boolean SetMasterUserByEstate(ServiceResponse responseApi , Object longOperation){
        try {
            String estCode = estCodeSyncNow();
            if(estCode == null){
                return false;
            }

            if (responseApi.getCode() == 304) {
                return true;
            }
            JSONArray jsonArray = new JSONArray(responseApi.getData().toString());

            int  count = 0;
            hapusDb(estCode,GlobalHelper.TABLE_USER_HARVEST);

            Nitrite db = GlobalHelper.getTableNitrit(estCode,GlobalHelper.TABLE_USER_HARVEST);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            for(int i = 0 ;i < jsonArray.length();i++){
                JSONObject objUserEstate = (JSONObject) jsonArray.get(i);
                Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", objUserEstate.getString("UserID")));
                DataNitrit dataNitrit = new DataNitrit(objUserEstate.getString("UserID"),String.valueOf(objUserEstate));
                if(cursor.size() ==0) {
                    repository.insert(dataNitrit);
                }

                JSONObject objProgres = new JSONObject();
                objProgres.put("ket",context.getResources().getString(R.string.update_data_user)+ " "+ estCode);
                objProgres.put("persen",(count*100/jsonArray.length()));
                objProgres.put("count",count);
                objProgres.put("total",jsonArray.length());
                count++;
                if(longOperation instanceof SettingFragment.LongOperation){
                    ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                }else if(longOperation instanceof ActiveActivity.LongOperation){
                    ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                }
                //Log.d("SetMasterUserByEstate",String.valueOf(i));
            }
            db.close();

            int countEstateMapping = ((BaseActivity) context).estatesMapping.size();
            int idxEstate = ((BaseActivity) context).estatesMapping.size();

            if(countEstateMapping - 1 == idxEstate) {
                SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(), count, jsonArray.length());
                SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

                addGetDataTag(responseApi.getTag());
            }
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean SetPemanenListByEstate(ServiceResponse responseApi,Object longOperation){
        try {
            String estCode = estCodeSyncNow();
            if(estCode == null){
                return false;
            }

            if (responseApi.getCode() == 304) {
                return true;
            }
            JSONArray jsonArray = new JSONArray(responseApi.getData().toString());

            int  count = 0;
            hapusDb(estCode,GlobalHelper.TABEL_PEMANEN);

            Nitrite db = GlobalHelper.getTableNitrit(estCode,GlobalHelper.TABEL_PEMANEN);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            for(int i = 0 ;i < jsonArray.length();i++){
                JSONObject objUserEstate = (JSONObject) jsonArray.get(i);
                DataNitrit dataNitrit = new DataNitrit(objUserEstate.getString("kodePemanen"),String.valueOf(objUserEstate));
                Log.d(TAG, "SetPemanenListByEstate: "+ objUserEstate.getString("kodePemanen"));
                repository.insert(dataNitrit);

                JSONObject objProgres = new JSONObject();
                objProgres.put("ket",context.getResources().getString(R.string.update_data_pemanen)+ " "+ estCode);
                objProgres.put("persen",jsonArray.length() == 0 ? 0:(count*100/jsonArray.length()));
                objProgres.put("count",count);
                objProgres.put("total",jsonArray.length());
                count++;
                if(longOperation instanceof SettingFragment.LongOperation){
                    ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                }else if(longOperation instanceof ActiveActivity.LongOperation){
                    ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                }
            }
            Gson gson = new Gson();
            JSONObject objUserEstate = new JSONObject(gson.toJson(SubPemanen.pemanenSPK));
            DataNitrit dataNitrit = new DataNitrit(objUserEstate.getString("kodePemanen"),String.valueOf(objUserEstate));
            repository.insert(dataNitrit);
            db.close();

            int countEstateMapping = ((BaseActivity) context).estatesMapping.size();
            int idxEstate = ((BaseActivity) context).estatesMapping.size();

            if(countEstateMapping - 1 == idxEstate) {
                SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(), count, jsonArray.length());
                SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

                addGetDataTag(responseApi.getTag());
            }
            RestoreMasterHelper.backupTempMasterSAP(estCode,GlobalHelper.TABEL_PEMANEN);
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean hapusAncakImages(ServiceResponse serviceResponse){
        boolean balik = true;
        try{
            JSONArray jsonArray = new JSONArray(serviceResponse.getData().toString());
            for(int i = 0 ; i < jsonArray.length();i++){
                balik = false;
                File file = new File(Environment.getExternalStorageDirectory() +
                        GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 + "/" +
                        GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_QC_MUTU_ANCAK] + "/"+
                        jsonArray.get(i)
                    );

                if(file.exists()){
                    file.delete();
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
            return true;
        }
        return balik;
    }

    public JSONObject UploadQcAncak(boolean withProgress, Object longOperation){
        try {

            String estCode = estCodeSyncNow();
            if(estCode == null){
                return null;
            }

            JSONArray dataQcAncak = new JSONArray();

            int count = 1;
            int uploadCount = 0;

            Nitrite db = GlobalHelper.getTableNitrit(estCode,GlobalHelper.TABLE_QC_ANCAK);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
            for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                QcAncak qcAncak = gson.fromJson(dataNitrit.getValueDataNitrit(), QcAncak.class);
                if (qcAncak.getStatus() == QcAncak.Save) {
                        JSONObject obj = new JSONObject(dataNitrit.getValueDataNitrit());

                        JSONObject objTph = obj.getJSONObject("tph");
                        objTph.remove("baris");

                        obj.put("tph", objTph);
                        obj.put("idx", uploadCount);
                        dataQcAncak.put(obj);
                        uploadCount++;
                }

                if(withProgress){
                    JSONObject objProgres = new JSONObject();
                    objProgres.put("ket",context.getResources().getString(R.string.setup_data_Qc_Ancak)+ " "+ estCode);
                    objProgres.put("persen",(count*100/((RecordIterable<DataNitrit>) Iterable).size()));
                    objProgres.put("count",count);
                    objProgres.put("total",((RecordIterable<DataNitrit>) Iterable).size());
                    count++;
                    if(longOperation instanceof SettingFragment.LongOperation){
                        ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                    }else if(longOperation instanceof ActiveActivity.LongOperation){
                        ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                    }
                }
            }
            db.close();

            JSONObject objReturn = new JSONObject();
            objReturn.put("data",dataQcAncak);
            return objReturn;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public JSONObject UploadSessionQcFotoPanen(boolean withProgress, Object longOperation){
        try {
            String estCode = estCodeSyncNow();
            if(estCode == null){
                return null;
            }

            JSONArray dataPanen = new JSONArray();

            int count = 1;
            int uploadCount = 0;

            Nitrite db = GlobalHelper.getTableNitrit(estCode,GlobalHelper.TABLE_QC_FOTO_PANEN);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
            for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                SessionQcFotoPanen sessionQcFotoPanen = gson.fromJson(dataNitrit.getValueDataNitrit(), SessionQcFotoPanen.class);
                if (sessionQcFotoPanen.getStatus() == SessionQcFotoPanen.Status_Done_Qc ||
                        sessionQcFotoPanen.getStatus() == SessionQcFotoPanen.Status_OutStanding_Qc ||
                        sessionQcFotoPanen.getStatus() == SessionQcFotoPanen.Status_Deleted) {
                    boolean masuk = false;
                    for(int i = 0 ; i < sessionQcFotoPanen.getQcFotoPanen().size();i++){
                        QcFotoPanen qcFotoPanen = sessionQcFotoPanen.getQcFotoPanen().get(i);
                        if(qcFotoPanen.getDataQcFotoPanen() != null){
                            masuk = true;
                            break;
                        }
                    }

                    if(sessionQcFotoPanen.getStatus() == SessionQcFotoPanen.Status_Deleted){
                        masuk = true;
                    }

                    if(masuk) {
                        JSONObject objPanen = new JSONObject(dataNitrit.getValueDataNitrit());
                        objPanen.put("idx", uploadCount);
                        dataPanen.put(objPanen);
                        uploadCount++;
                    }
                }

                if(withProgress){
                    JSONObject objProgres = new JSONObject();
                    objProgres.put("ket",context.getResources().getString(R.string.setup_data_session_foto_panen) + " "+ estCode);
                    objProgres.put("persen",(count*100/((RecordIterable<DataNitrit>) Iterable).size()));
                    objProgres.put("count",count);
                    objProgres.put("total",((RecordIterable<DataNitrit>) Iterable).size());
                    count++;
                    if(longOperation instanceof SettingFragment.LongOperation){
                        ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                    }else if(longOperation instanceof ActiveActivity.LongOperation){
                        ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                    }
                }
            }
            db.close();

            JSONObject objReturn = new JSONObject();
            objReturn.put("data",dataPanen);
            return objReturn;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public JSONObject GetSessionQcFotoPanen(Object longOperation){
        try {
            JSONArray dataImg = new JSONArray();
            String fullPath = Environment.getExternalStorageDirectory() +
                    GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 + "/" +
                    GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_QC_FOTO_PANEN];
            File dirImages = new File(fullPath);
            if(dirImages.isDirectory()){
                for(File file : dirImages.listFiles()){
                    String [] split = file.getName().split("_");
                    if(split.length == 3 ){
                        if(GlobalHelper.isNumeric(split[0])){
                            Long tglFoto = Long.parseLong(split[0]);
                            Long batasFoto = System.currentTimeMillis() - GlobalHelper.MAX_FOTO_PANEN;

                            if(tglFoto>batasFoto){
                                dataImg.put(file.getName());
                            }else{
                                file.delete();
                            }
                        }else{
                            file.delete();
                        }
                    }else{
                        file.delete();
                    }
                }
            }

            JSONObject objReturn = new JSONObject();
            objReturn.put("data",dataImg);
            return objReturn;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public JSONObject UploadQcBuah(boolean withProgress, Object longOperation){
        try {
            String estCode = estCodeSyncNow();
            if(estCode == null){
                return null;
            }

            JSONArray dataPanen = new JSONArray();
            JSONArray dataImagePanen = new JSONArray();

            int count = 1;
            int uploadCount = 0;

            Nitrite db = GlobalHelper.getTableNitrit(estCode,GlobalHelper.TABLE_QC_BUAH);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
            for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                QcMutuBuah qcMutuBuah = gson.fromJson(dataNitrit.getValueDataNitrit(), QcMutuBuah.class);
                TransaksiPanen transaksiPanen = qcMutuBuah.getTransaksiPanenQc();
                if (transaksiPanen.getStatus() == TransaksiPanen.SAVE_ONLY || transaksiPanen.getStatus() == TransaksiPanen.TAP) {
                    JSONObject objPanen= new JSONObject(dataNitrit.getValueDataNitrit());
                    objPanen.put("idx",uploadCount);
                    dataPanen.put(objPanen);
                    uploadCount++;
                }

                if(withProgress){
                    JSONObject objProgres = new JSONObject();
                    objProgres.put("ket",context.getResources().getString(R.string.setup_data_TransaksiPanen) + " "+ estCode);
                    objProgres.put("persen",(count*100/((RecordIterable<DataNitrit>) Iterable).size()));
                    objProgres.put("count",count);
                    objProgres.put("total",((RecordIterable<DataNitrit>) Iterable).size());
                    count++;
                    if(longOperation instanceof SettingFragment.LongOperation){
                        ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                    }else if(longOperation instanceof ActiveActivity.LongOperation){
                        ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                    }
                }
            }
            db.close();

            JSONObject objReturn = new JSONObject();
            objReturn.put("data",dataPanen);
            return objReturn;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public JSONObject UploadSensusBjr(boolean withProgress, Object longOperation){
        try {
            String estCode = estCodeSyncNow();
            if(estCode == null){
                return null;
            }

            JSONArray dataBjr = new JSONArray();

            int count = 1;
            int uploadCount = 0;

            Nitrite db = GlobalHelper.getTableNitrit(estCode,GlobalHelper.TABLE_QC_BJR);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
            for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                SensusBjr sensusBjr = gson.fromJson(dataNitrit.getValueDataNitrit(),SensusBjr.class);
                if (sensusBjr.getStatus() == SensusBjr.SAVE) {
                    JSONArray allBerat = new JSONArray();
                    for(int i = 0; i < sensusBjr.getBeratJanjang().size();i++){
                        int idx = i + 1;
                        allBerat.put(i,sensusBjr.getBeratJanjang().get(idx).getJanjang());
                    }

                    JSONObject objBjr= new JSONObject(dataNitrit.getValueDataNitrit());
                    objBjr.put("idx",uploadCount);
                    objBjr.put("beratJanjang",allBerat);
                    dataBjr.put(objBjr);
                    uploadCount++;
                }

                if(withProgress){
                    JSONObject objProgres = new JSONObject();
                    objProgres.put("ket",context.getResources().getString(R.string.setup_data_Sensus_Bjr)+ " "+ estCode);
                    objProgres.put("persen",(count*100/((RecordIterable<DataNitrit>) Iterable).size()));
                    objProgres.put("count",count);
                    objProgres.put("total",((RecordIterable<DataNitrit>) Iterable).size());
                    count++;
                    if(longOperation instanceof SettingFragment.LongOperation){
                        ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                    }else if(longOperation instanceof ActiveActivity.LongOperation){
                        ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                    }
                }
            }
            db.close();

            JSONObject objReturn = new JSONObject();
            objReturn.put("data",dataBjr);
//            Log.d("objReturn Panen", String.valueOf(objReturn));
            return objReturn;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public JSONObject UploadCrashReport(boolean withProgress, Object longOperation){
        try {
            JSONArray dataCrash = new JSONArray();

            int count = 1;
            int uploadCount = 0;
            WaspDb db = WaspFactory.openOrCreateDatabase(cat.ereza.customactivityoncrash.helper.GlobalHelper.getDatabasePath(),
                    cat.ereza.customactivityoncrash.helper.GlobalHelper.DB_MASTER,
                    cat.ereza.customactivityoncrash.helper.GlobalHelper.getDatabasePass());
            WaspHash crashTbl = db.openOrCreateHash(cat.ereza.customactivityoncrash.helper.GlobalHelper.CrashReport);
            if(crashTbl.getAllValues().size() > 0) {
                for (Object obj : crashTbl.getAllValues()) {
                    CrashDetailAnalyticInfo model = (CrashDetailAnalyticInfo) obj;
                    Gson gson = new Gson();
                    PackageManager manager = HarvestApp.getContext().getPackageManager();
                    PackageInfo info = manager.getPackageInfo(context.getPackageName(), PackageManager.GET_ACTIVITIES);

                    JSONObject object = new JSONObject(gson.toJson(model));
                    object.put("VersionCode",info.versionCode );
                    object.put("VersionName",info.versionName);
                    dataCrash.put(object);

                    if(withProgress){
                        JSONObject objProgres = new JSONObject();
                        objProgres.put("ket",context.getResources().getString(R.string.upload_crash));
                        objProgres.put("persen",(count*100/crashTbl.getAllValues().size()));
                        objProgres.put("count",count);
                        objProgres.put("total",(crashTbl.getAllValues().size()));
                        count++;
                        if(longOperation instanceof SettingFragment.LongOperation){
                            ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                        }else if(longOperation instanceof ActiveActivity.LongOperation){
                            ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                        }
                    }
                }
            }

            JSONObject objReturn = new JSONObject();
            objReturn.put("data",dataCrash);
            return objReturn;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean downLoadTph(ServiceResponse responseApi, Object longOperation){
        try {
            String estCode = estCodeSyncNow();
            if(estCode == null){
                return false;
            }

            if (responseApi.getCode() == 304) {
                return true;
            }
            JSONArray data = new JSONArray(responseApi.getData().toString());

            hapusDb(estCode,GlobalHelper.TABEL_TPH);

            int count = 0;
            Nitrite db = GlobalHelper.getTableNitrit(estCode,GlobalHelper.TABEL_TPH);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);

            for (int i = 0; i < data.length(); i++) {
                JSONObject objTph = (JSONObject) data.get(i);
                DataNitrit dataNitrit = new DataNitrit(objTph.getString("noTph"),
                        objTph.toString(),
                        objTph.getString("block"),
                        objTph.getString("afdeling"),
                        objTph.get("createBy").toString());
                repository.insert(dataNitrit);

                JSONObject objProgres = new JSONObject();
                objProgres.put("ket", context.getResources().getString(R.string.update_data_tph)+ " "+ estCode);
                objProgres.put("persen", (count * 100 / data.length()));
                objProgres.put("count", count);
                objProgres.put("total", (data.length()));
                count++;
                if (longOperation instanceof SettingFragment.LongOperation) {
                    ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                } else if (longOperation instanceof ActiveActivity.LongOperation) {
                    ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                } else if (longOperation instanceof MapActivity.LongOperation) {
                    ((MapActivity.LongOperation) longOperation).publishProgress(objProgres);
                }
            }
            db.close();

            int countEstateMapping = ((BaseActivity) context).estatesMapping.size();
            int idxEstate = ((BaseActivity) context).estatesMapping.size();

            if(countEstateMapping - 1 == idxEstate) {
                SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(), count, data.length());
                SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

                addGetDataTag(responseApi.getTag());
            }
            RestoreMasterHelper.backupTempMasterSAP(estCode,GlobalHelper.TABEL_TPH);
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
//            Toast.makeText(HarvestApp.getContext(),"SetUpDateSync downLoadTph", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public boolean downLoadQCQuestionAnswer(ServiceResponse responseApi, Object longOperation){
        try {
            String estCode = estCodeSyncNow();
            if(estCode == null){
                return false;
            }

            if (responseApi.getCode() == 304) {
                return true;
            }
            JSONArray data = new JSONArray(responseApi.getData().toString());

            hapusDb(estCode,GlobalHelper.TABEL_QCQuestionAnswer);

            int count = 0;
            Nitrite db = GlobalHelper.getTableNitrit(estCode,GlobalHelper.TABEL_QCQuestionAnswer);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);

            for (int i = 0; i < data.length(); i++) {
                JSONObject objTph = (JSONObject) data.get(i);
                DataNitrit dataNitrit = new DataNitrit(objTph.getString("IdQuestionAnswer"),
                        objTph.toString(),
                        objTph.getString("IdQcQuestion"),
                        objTph.getString("QcQuestion"));
                repository.insert(dataNitrit);

                JSONObject objProgres = new JSONObject();
                objProgres.put("ket", context.getResources().getString(R.string.update_data_qc_qa));
                objProgres.put("persen", (count * 100 / data.length()));
                objProgres.put("count", count);
                objProgres.put("total", (data.length()));
                count++;
                if (longOperation instanceof SettingFragment.LongOperation) {
                    ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                } else if (longOperation instanceof ActiveActivity.LongOperation) {
                    ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                } else if (longOperation instanceof MapActivity.LongOperation) {
                    ((MapActivity.LongOperation) longOperation).publishProgress(objProgres);
                }
            }
            db.close();

            int countEstateMapping = ((BaseActivity) context).estatesMapping.size();
            int idxEstate = ((BaseActivity) context).estatesMapping.size();

            if(countEstateMapping - 1 == idxEstate) {
                SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(), count, data.length());
                SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

                addGetDataTag(responseApi.getTag());
            }
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
//            Toast.makeText(HarvestApp.getContext(),"SetUpDateSync downLoadTph", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public boolean downLoadApplicationConfiguration(ServiceResponse responseApi, Object longOperation){
        try {
            String estCode = estCodeSyncNow();
            if(estCode == null){
                return false;
            }
            if (responseApi.getCode() == 304) {
                return true;
            }
            hapusDb(estCode,GlobalHelper.TABLE_APPLICATION_CONFIGURATION);

            int count = 0;
            Nitrite db = GlobalHelper.getTableNitrit(estCode,GlobalHelper.TABLE_APPLICATION_CONFIGURATION);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            JSONObject objAppConfig = new JSONObject(responseApi.getData().toString());
            JSONObject objAppConfigDynamicParameterPenghasilan = objAppConfig.getJSONObject("dynamicParameterPenghasilan");
            DataNitrit dataNitrit = new DataNitrit(objAppConfigDynamicParameterPenghasilan.getString("estCode"), objAppConfigDynamicParameterPenghasilan.toString());
            repository.insert(dataNitrit);

            JSONObject objProgres = new JSONObject();
            objProgres.put("ket", context.getResources().getString(R.string.update_data_aplication_configuration)+ " "+ estCode);
            objProgres.put("persen", 100);
            objProgres.put("count", 100);
            objProgres.put("total", 100);
            if (longOperation instanceof SettingFragment.LongOperation) {
                ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
            } else if (longOperation instanceof ActiveActivity.LongOperation) {
                ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
            }
            db.close();

            int countEstateMapping = ((BaseActivity) context).estatesMapping.size();
            int idxEstate = ((BaseActivity) context).estatesMapping.size();

            if(countEstateMapping - 1 == idxEstate) {
                SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(), 1, 1);
                SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

                addGetDataTag(responseApi.getTag());
            }
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
//            Toast.makeText(HarvestApp.getContext(),"SetUpDateSync downLoadTransaksiSPB", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public boolean downLoadMasterSpk(ServiceResponse responseApi, Object longOperation){
        try {
            String estCode = estCodeSyncNow();
            if(estCode == null){
                return false;
            }

            if (responseApi.getCode() == 304) {
                return true;
            }
            JSONArray data = new JSONArray(responseApi.getData().toString());
            hapusDb(estCode,GlobalHelper.TABLE_SPK);

            int count = 0;
            Nitrite db = GlobalHelper.getTableNitrit(estCode,GlobalHelper.TABLE_SPK);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);

            for (int i = 0; i < data.length(); i++) {
                JSONObject objISCC = (JSONObject) data.get(i);
                Gson gson = new Gson();
                DataNitrit dataNitrit = new DataNitrit(objISCC.getString("idSPK"), objISCC.toString(),objISCC.getString("description"));
                repository.insert(dataNitrit);

                JSONObject objProgres = new JSONObject();
                objProgres.put("ket", context.getResources().getString(R.string.update_data_SPK)+ " "+ estCode);
                objProgres.put("persen", (count * 100 / data.length()));
                objProgres.put("count", count);
                objProgres.put("total", (data.length()));
                count++;
                if (longOperation instanceof SettingFragment.LongOperation) {
                    ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                } else if (longOperation instanceof ActiveActivity.LongOperation) {
                    ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                }
            }
            db.close();

            int countEstateMapping = ((BaseActivity) context).estatesMapping.size();
            int idxEstate = ((BaseActivity) context).estatesMapping.size();

            if(countEstateMapping - 1 == idxEstate) {
                SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(), count, data.length());
                SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

                addGetDataTag(responseApi.getTag());
            }
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
//            Toast.makeText(HarvestApp.getContext(),"SetUpDateSync downLoadTransaksiSPB", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public boolean downLoadSensusBJR(ServiceResponse responseApi, Object longOperation){
        try {
            String estCode = estCodeSyncNow();
            if(estCode == null){
                return false;
            }

            if (responseApi.getCode() == 304) {
                return true;
            }
            JSONArray data = new JSONArray(responseApi.getData().toString());
            hapusDb(estCode,GlobalHelper.TABLE_QC_BJR);

            int count = 0;
            Nitrite db = GlobalHelper.getTableNitrit(estCode,GlobalHelper.TABLE_QC_BJR);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);

            for (int i = 0; i < data.length(); i++) {
                JSONObject objBJR = (JSONObject) data.get(i);

                JSONArray arrayBeratJanjang = objBJR.getJSONArray("beratJanjang");
                HashMap<Integer,JanjangSensus> janjangSensusHashMap = new HashMap<>();
                for(int x = 0 ; x < arrayBeratJanjang.length();x++){
                    int jjgIdx = x + 1;
                    JanjangSensus janjangSensus = new JanjangSensus(jjgIdx,Integer.parseInt(String.valueOf(arrayBeratJanjang.get(x).toString())));
                    janjangSensusHashMap.put(janjangSensus.getIdxJanjang(),janjangSensus);
                }
                objBJR.remove("beratJanjang");
                Gson gson = new Gson();
                SensusBjr sensusBjr = gson.fromJson(objBJR.toString(),SensusBjr.class);
                sensusBjr.setBeratJanjang(janjangSensusHashMap);

                DataNitrit dataNitrit = new DataNitrit(sensusBjr.getIdQCSensusBJR(), gson.toJson(sensusBjr),sensusBjr.getIdTPanen());
                repository.insert(dataNitrit);

                JSONObject objProgres = new JSONObject();
                objProgres.put("ket", context.getResources().getString(R.string.download_sensus_bjr_list)+ " "+ estCode);
                objProgres.put("persen", (count * 100 / data.length()));
                objProgres.put("count", count);
                objProgres.put("total", (data.length()));
                count++;
                if (longOperation instanceof SettingFragment.LongOperation) {
                    ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                } else if (longOperation instanceof ActiveActivity.LongOperation) {
                    ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                }
            }
            db.close();

            int countEstateMapping = ((BaseActivity) context).estatesMapping.size();
            int idxEstate = ((BaseActivity) context).estatesMapping.size();

            if(countEstateMapping - 1 == idxEstate) {
                SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(), count, data.length());
                SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

                addGetDataTag(responseApi.getTag());
            }
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
//            Toast.makeText(HarvestApp.getContext(),"SetUpDateSync downLoadTransaksiSPB", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public boolean downLoadQcBuah(ServiceResponse responseApi, Object longOperation){
        try {
            String estCode = estCodeSyncNow();
            if(estCode == null){
                return false;
            }

            if (responseApi.getCode() == 304) {
                return true;
            }
            JSONArray data = new JSONArray(responseApi.getData().toString());
            hapusDb(estCode,GlobalHelper.TABLE_QC_BUAH);

            int count = 0;
            Nitrite db = GlobalHelper.getTableNitrit(estCode,GlobalHelper.TABLE_QC_BUAH);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);

            for (int i = 0; i < data.length(); i++) {
                JSONObject objBuah = (JSONObject) data.get(i);
                Gson gson= new Gson();
                QcMutuBuah qcMutuBuah = new QcMutuBuah();
                String idTPanen = "";
                if(objBuah.has("idTPanen")){
                    qcMutuBuah = gson.fromJson(objBuah.toString(),QcMutuBuah.class);
                    idTPanen = String.valueOf(objBuah.getString("idTPanen"));
                }else{
                    qcMutuBuah.setIdQCMutuBuah(String.valueOf(objBuah.getString("idQCMutuBuah")));
                    JSONObject objTPanen =  new JSONObject(objBuah.get("transaksiPanenQc").toString());

                    TPH tph = gson.fromJson(objTPanen.get("tph").toString(),TPH.class);
                    HasilPanen hasilPanen = gson.fromJson(objTPanen.get("hasilPanen").toString(),HasilPanen.class);
                    TransaksiPanen transaksiPanenQc = new TransaksiPanen(
                        null,
                            objTPanen.getDouble("latitude"),
                            objTPanen.getDouble("longitude"),
                            objTPanen.getString("createBy"),
                            objTPanen.getLong("createDate"),
                            objTPanen.getString("updateBy"),
                            objTPanen.getLong("updateDate"),
                            objTPanen.getString("ancak"),
                            objTPanen.getString("baris"),
                            null,
                            tph,
                            hasilPanen,
                            null,
                            objTPanen.getInt("status"),
                            objTPanen.getInt("restan"),
                            null,
                            null
                    );
                    qcMutuBuah.setTransaksiPanenQc(transaksiPanenQc);
                    qcMutuBuah.setTransaksiPanenKrani(transaksiPanenQc);
                }


                DataNitrit dataNitrit = new DataNitrit(qcMutuBuah.getIdQCMutuBuah(), gson.toJson(qcMutuBuah),idTPanen);
                repository.insert(dataNitrit);


                JSONObject objProgres = new JSONObject();
                objProgres.put("ket", context.getResources().getString(R.string.download_qc_buah_list)+ " "+ estCode);
                objProgres.put("persen", (count * 100 / data.length()));
                objProgres.put("count", count);
                objProgres.put("total", (data.length()));
                count++;
                if (longOperation instanceof SettingFragment.LongOperation) {
                    ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                } else if (longOperation instanceof ActiveActivity.LongOperation) {
                    ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                }
            }
            db.close();

            int countEstateMapping = ((BaseActivity) context).estatesMapping.size();
            int idxEstate = ((BaseActivity) context).estatesMapping.size();

            if(countEstateMapping - 1 == idxEstate) {
                SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(), count, data.length());
                SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

                addGetDataTag(responseApi.getTag());
            }
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
//            Toast.makeText(HarvestApp.getContext(),"SetUpDateSync downLoadTransaksiSPB", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public boolean downLoadQcAncak(ServiceResponse responseApi, Object longOperation){
        try {
            String estCode = estCodeSyncNow();
            if(estCode == null){
                return false;
            }

            if (responseApi.getCode() == 304) {
                return true;
            }
            JSONArray data = new JSONArray(responseApi.getData().toString());
            hapusDb(estCode,GlobalHelper.TABLE_QC_ANCAK);

            int count = 0;
            Nitrite db = GlobalHelper.getTableNitrit(estCode,GlobalHelper.TABLE_QC_ANCAK);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);

            for (int i = 0; i < data.length(); i++) {
                JSONObject objBuah = (JSONObject) data.get(i);
                DataNitrit dataNitrit = new DataNitrit(String.valueOf(objBuah.getString("idQcAncak")), objBuah.toString());
                repository.insert(dataNitrit);

                JSONObject objProgres = new JSONObject();
                objProgres.put("ket", context.getResources().getString(R.string.download_qc_ancak_list)+ " "+ estCode);
                objProgres.put("persen", (count * 100 / data.length()));
                objProgres.put("count", count);
                objProgres.put("total", (data.length()));
                count++;
                if (longOperation instanceof SettingFragment.LongOperation) {
                    ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                } else if (longOperation instanceof ActiveActivity.LongOperation) {
                    ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                }
            }
            db.close();

            int countEstateMapping = ((BaseActivity) context).estatesMapping.size();
            int idxEstate = ((BaseActivity) context).estatesMapping.size();

            if(countEstateMapping - 1 == idxEstate) {
                SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(), count, data.length());
                SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

                addGetDataTag(responseApi.getTag());
            }
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
//            Toast.makeText(HarvestApp.getContext(),"SetUpDateSync downLoadTransaksiSPB", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public boolean downLoadSessionQcFotoPanen(ServiceResponse responseApi, Object longOperation){
        try {
            String estCode = estCodeSyncNow();
            if(estCode == null){
                return false;
            }

            if (responseApi.getCode() == 304) {
                return true;
            }

            JSONArray dataArray = new JSONArray(responseApi.getData().toString());
            hapusDb(estCode,GlobalHelper.TABLE_QC_FOTO_PANEN);

            int count = 0;
            Nitrite db = GlobalHelper.getTableNitrit(estCode,GlobalHelper.TABLE_QC_FOTO_PANEN);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);

            for (int i = 0; i < dataArray.length(); i++) {

                Gson gson = new Gson();
                JSONObject data = (JSONObject) dataArray.get(i);
                JSONArray dataQcFotoPanenArray = data.getJSONArray("QcFotoPanen");

                if(dataQcFotoPanenArray.length()> 0) {

                    HashMap<String, QcFotoPanen> qcFotoPanenHashMap = new HashMap<>();

                    SessionQcFotoPanen sessionQcFotoPanen = new SessionQcFotoPanen(
                            data.getString("idSession"),
                            String.valueOf(data.getInt("createBy")),
                            data.getString("requestDate"),
                            data.getString("estCode"),
                            data.getString("afdeling"),
                            gson.fromJson(data.get("methode").toString(), Answer.class),
                            gson.fromJson(data.get("selection").toString(), Answer.class),
                            gson.fromJson(data.get("sectionValue").toString(), ChoiceSelection.class),
                            data.getInt("sample"),
                            data.getLong("createDate"),
                            data.getLong("updateDate"),
                            data.getInt("status")
                    );

                    if(data.has("urlDownload")){
                        sessionQcFotoPanen.setUrlDownload(data.getString("urlDownload"));
                    }

                    if(data.has("filePDF")){
                        sessionQcFotoPanen.setFilePDF(data.getString("filePDF"));
                    }

                    int sudahQc = 0;
                    for (int x = 0; x < dataQcFotoPanenArray.length(); x++) {
                        JSONObject obj = new JSONObject(dataQcFotoPanenArray.get(x).toString());
                        if (obj.has("dataQcFotoPanen")) {
                            sudahQc++;
                        }
                        JSONArray jImg = obj.getJSONArray("foto");
                        HashSet<String> foto = new HashSet<>();
                        for (int j = 0; j < jImg.length(); j++) {
                            JSONObject objImg = jImg.getJSONObject(j);
                            String fullPath = Environment.getExternalStorageDirectory() +
                                    GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 + "/" +
                                    GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_QC_FOTO_PANEN] + "/" +
                                    objImg.get("imgName");
                            if (objImg.has("img")) {
                                if (GlobalHelper.decode64ImageStringToBitMap(objImg.getString("img"), fullPath)) {
                                    foto.add(fullPath);
                                }
                            } else {
                                foto.add(fullPath);
                            }
                        }
                        obj.remove("foto");
                        QcFotoPanen qcFotoPanen = gson.fromJson(obj.toString(), QcFotoPanen.class);
                        qcFotoPanen.setFoto(foto);
                        qcFotoPanenHashMap.put(qcFotoPanen.getIdTPanen(), qcFotoPanen);
                    }

                    ArrayList<QcFotoPanen> qcFotoPanens = new ArrayList<QcFotoPanen>(qcFotoPanenHashMap.values());
                    Collections.sort(qcFotoPanens, new Comparator<QcFotoPanen>() {
                        @Override
                        public int compare(QcFotoPanen o1, QcFotoPanen o2) {
                            return o2.getCreateDate() > o1.getCreateDate() ? -1 : (o2.getCreateDate() < o1.getCreateDate()) ? 1 : 0;
                        }
                    });
                    sessionQcFotoPanen.setQcFotoPanen(qcFotoPanens);

                    int persen = (int) (Double.parseDouble(String.valueOf(sudahQc)) /
                            Double.parseDouble(String.valueOf(dataQcFotoPanenArray.length())) *
                            100.00
                    );

                    if (persen <= 99) {
                        sessionQcFotoPanen.setStatus(SessionQcFotoPanen.Status_OutStanding_Qc);
                    }
                    Log.d("downLoadSession", sessionQcFotoPanen.getIdSession());
                    DataNitrit dataNitrit = new DataNitrit(sessionQcFotoPanen.getIdSession(), gson.toJson(sessionQcFotoPanen));
                    repository.insert(dataNitrit);
                }

                JSONObject objProgres = new JSONObject();
                objProgres.put("ket", context.getResources().getString(R.string.download_qc_Session_Foto_Panen)+ " "+ estCode);
                objProgres.put("persen", (count * 100 / dataArray.length()));
                objProgres.put("count", count);
                objProgres.put("total", (dataArray.length()));
                count++;
                if (longOperation instanceof SettingFragment.LongOperation) {
                    ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                } else if (longOperation instanceof ActiveActivity.LongOperation) {
                    ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                }
            }
            db.close();

            int countEstateMapping = ((BaseActivity) context).estatesMapping.size();
            int idxEstate = ((BaseActivity) context).estatesMapping.size();

            if(countEstateMapping - 1 == idxEstate) {
                SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(), count, dataArray.length());
                SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

                addGetDataTag(responseApi.getTag());
            }
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
//            Toast.makeText(HarvestApp.getContext(),"SetUpDateSync downLoadTransaksiSPB", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public boolean getEstateMapping(ServiceResponse responseApi, Object longOperation){
        try {
            String estCode = estCodeSyncNow();
            if(estCode == null){
                return false;
            }

            if (responseApi.getCode() == 304) {
                return true;
            }
            JSONArray data = new JSONArray(responseApi.getData().toString());
            hapusDb(estCode,GlobalHelper.TABLE_ESTATE_MAPPING);

            int count = 0;
            Nitrite db = GlobalHelper.getTableNitrit(estCode,GlobalHelper.TABLE_ESTATE_MAPPING);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);

            for (int i = 0; i < data.length(); i++) {
                JSONObject objBuah = (JSONObject) data.get(i);
                Estate estate = new Estate(objBuah.getString("EstCode"),
                        objBuah.getString("EstNewCode") + " - " + objBuah.getString("NewEstName"),
                        objBuah.getString("CompanyCode"));
                Gson gson = new Gson();
                JSONObject estMaping = new JSONObject(gson.toJson(estate));
                DataNitrit dataNitrit = new DataNitrit(estate.getEstCode(), estMaping.toString());
                repository.insert(dataNitrit);

                JSONObject objProgres = new JSONObject();
                objProgres.put("ket", context.getResources().getString(R.string.download_estate_mapping) + " "+ estCode);
                objProgres.put("persen", (count * 100 / data.length()));
                objProgres.put("count", count);
                objProgres.put("total", (data.length()));
                count++;
                if (longOperation instanceof SettingFragment.LongOperation) {
                    ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                } else if (longOperation instanceof ActiveActivity.LongOperation) {
                    ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                }
            }
            db.close();

            int countEstateMapping = ((BaseActivity) context).estatesMapping.size();
            int idxEstate = ((BaseActivity) context).estatesMapping.size();

            if(countEstateMapping - 1 == idxEstate) {
                SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(), count, data.length());
                SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

                addGetDataTag(responseApi.getTag());
            }
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
//            Toast.makeText(HarvestApp.getContext(),"SetUpDateSync downLoadTransaksiSPB", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public boolean UpdateQCMutuAncakCounterByUserID(ServiceResponse serviceResponse){
        try {
            if (serviceResponse.getCode() == 304) {
                return true;
            }
            JSONObject object = new JSONObject(String.valueOf(serviceResponse.getData()));
            SyncHistroyItem item = new SyncHistroyItem(serviceResponse.getTag(),1,1);
            SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);
            GlobalHelper.setCountNumber(GlobalHelper.LIST_FOLDER_SELECTED_QC_ANCAK,object.getInt("nextCounter"));
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean UpdateQCMutuBuahCounterByUserID(ServiceResponse serviceResponse){
        try {
            if (serviceResponse.getCode() == 304) {
                return true;
            }
            JSONObject object = new JSONObject(String.valueOf(serviceResponse.getData()));
            SyncHistroyItem item = new SyncHistroyItem(serviceResponse.getTag(),1,1);
            SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);
            GlobalHelper.setCountNumber(GlobalHelper.LIST_FOLDER_SELECTED_QC_MUTUBUAH,object.getInt("nextCounter"));
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void updateStatusUploadQcBuah(String id,String estCode){

        Nitrite db = GlobalHelper.getTableNitrit(estCode,GlobalHelper.TABLE_QC_BUAH);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        String [] arrayId = id.split(";");
        for(int i = 0 ; i < arrayId.length;i++) {
            Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", arrayId[i]));
            for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                QcMutuBuah qcMutuBuah = gson.fromJson(dataNitrit.getValueDataNitrit(), QcMutuBuah.class);
                TransaksiPanen transaksiPanen = qcMutuBuah.getTransaksiPanenQc();
                transaksiPanen.setStatus(TransaksiPanen.UPLOAD);
                qcMutuBuah.setTransaksiPanenQc(transaksiPanen);
                dataNitrit.setValueDataNitrit(gson.toJson(qcMutuBuah));
                repository.update(dataNitrit);
                break;
            }
        }
        db.close();
    }

    public static void updateStatusUploadSensusBjr(String id,String estCode){
        Nitrite db = GlobalHelper.getTableNitrit(estCode,GlobalHelper.TABLE_QC_BJR);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        String [] arrayId = id.split(";");
        for(int i = 0 ; i < arrayId.length;i++) {
            Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", arrayId[i]));
            for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                SensusBjr sensusBjr = gson.fromJson(dataNitrit.getValueDataNitrit(), SensusBjr.class);
                sensusBjr.setStatus(SensusBjr.UPLOAD);
                dataNitrit.setValueDataNitrit(gson.toJson(sensusBjr));
                repository.update(dataNitrit);
                break;
            }
        }
        db.close();
    }

    public static void updateStatusUploadQcAncak(String id,String estCode){
        Nitrite db = GlobalHelper.getTableNitrit(estCode,GlobalHelper.TABLE_QC_ANCAK);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        String [] arrayId = id.split(";");
        for(int i = 0 ; i < arrayId.length;i++) {
            Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", arrayId[i]));
            for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                QcAncak qcAncak = gson.fromJson(dataNitrit.getValueDataNitrit(), QcAncak.class);
                qcAncak.setStatus(QcAncak.Upload);
                dataNitrit.setValueDataNitrit(gson.toJson(qcAncak));
                repository.update(dataNitrit);
                break;
            }
        }
        db.close();
    }

    public static void updateStatusUploadSessionQcFotoPanen(String id,String estCode){
        Nitrite db = GlobalHelper.getTableNitrit(estCode,GlobalHelper.TABLE_QC_FOTO_PANEN);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        String [] arrayId = id.split(";");
        for(int i = 0 ; i < arrayId.length;i++) {
            Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", arrayId[i]));
            for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                SessionQcFotoPanen sessionQcFotoPanen = gson.fromJson(dataNitrit.getValueDataNitrit(), SessionQcFotoPanen.class);
                if(sessionQcFotoPanen.getStatus() != SessionQcFotoPanen.Status_Deleted) {
                    sessionQcFotoPanen.setStatus(SessionQcFotoPanen.Status_Upload_Qc);
                    dataNitrit.setValueDataNitrit(gson.toJson(sessionQcFotoPanen));
                    repository.update(dataNitrit);
                }
                break;
            }
        }
        db.close();
    }

    public static boolean cekGetDataTag(Tag tag){
        String [] isiLastGetData = GlobalHelper.readFileContent(GlobalHelper.getDatabasePathHMSTemp() + GlobalHelper.getLastGetdataName()).split(";");
        for(int i = 0 ; i < isiLastGetData.length;i++){
            if(isiLastGetData[i].toString().equals(tag.toString())){
                return true;
            }
        }
        return SyncTimeHelper.cekSyncTime(tag);
    }

    public static void addGetDataTag(Tag tag){
        GlobalHelper.writeFileContentAppend(GlobalHelper.getDatabasePathHMSTemp() + GlobalHelper.getLastGetdataName(),tag+";");
        SyncTimeHelper.updateSyncTime(tag);
    }

    public static void addPointCounterBadResponse(){
        String isi = GlobalHelper.readFileContent(GlobalHelper.getDatabasePathHMSTemp() + GlobalHelper.getCountBadResponseName());
        if(isi.isEmpty()){
            GlobalHelper.writeFileContent(GlobalHelper.getDatabasePathHMSTemp() + GlobalHelper.getCountBadResponseName(),"1");
        }else{
            int idx = Integer.parseInt(isi);
            idx++;
            GlobalHelper.writeFileContent(GlobalHelper.getDatabasePathHMSTemp() + GlobalHelper.getCountBadResponseName(),String.valueOf(idx));
        }
    }

    public boolean backupDataHMS(ArrayList<Estate> estates,Object longOperation){
        SharedPreferences prefer = HarvestApp.getContext().getSharedPreferences(HarvestApp.SYNC_TIME, Context.MODE_PRIVATE);
        String sync_time = prefer.getString(HarvestApp.SYNC_TIME,null);
        Gson gson = new Gson();
        LastSyncTime lastSyncTime = gson.fromJson(sync_time,LastSyncTime.class);
        HashMap<String,SyncTime> syncTimeHashMap = lastSyncTime.getSyncTimeHashMap();
        SyncTime syncTimeTransaksi = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
        SyncTime syncTimeMaster = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);

        if(!syncTimeTransaksi.isSelected() && !syncTimeMaster.isSelected()){
            return true;
        }

        File dbBackup = new File(GlobalHelper.getDatabasePathHMSBackup());
        String[] dirListing = dbBackup.list();
        Arrays.sort(dirListing);

        if (longOperation instanceof ActiveActivity.LongOperation) {
            return true;
        }

        //batasi file restore jika mencapai atas maka dihapus file yang terakhir
        if(dirListing.length > GlobalHelper.SIZE_RESTORE) {
            int selisih = dirListing.length - GlobalHelper.SIZE_RESTORE;
            for (int i = 0; i < selisih; i++){
                File f = new File(dbBackup,dirListing[i]);
                if(f.isDirectory()){
                    try {
                        FileUtils.cleanDirectory(f);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else if (f.isFile()){
                    Log.d("hapus file backup",f.getAbsolutePath());
                    f.delete();
                }
            }
        }

        zipDBHms(estates,longOperation,"SYNC");
        return true;
    }

    public void zipDBHms(ArrayList<Estate> estates,Object longOperation,String param){
        SharedPreferences prefer = HarvestApp.getContext().getSharedPreferences(HarvestApp.SYNC_TIME, Context.MODE_PRIVATE);
        String sync_time = prefer.getString(HarvestApp.SYNC_TIME,null);
        Gson gson = new Gson();
        LastSyncTime lastSyncTime = gson.fromJson(sync_time,LastSyncTime.class);
        HashMap<String,SyncTime> syncTimeHashMap = lastSyncTime.getSyncTimeHashMap();
        SyncTime syncTimeTransaksi = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
        SyncTime syncTimeMaster = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);

        String sumber = "";
        if (longOperation instanceof SettingFragment.LongOperation) {
            sumber = "Menu Setting "+ param ;
        } else if (longOperation instanceof ActiveActivity.LongOperation) {
            sumber = "Active "+param;
        } else if (longOperation instanceof MapActivity.LongOperation) {
            sumber = "Map "+param;
        }

        File extStatisticsFolder = new File( BuildConfig.BUILD_VARIANT.equals("dev") ? Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES + "/QCHMSSAPTESTING":Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES + "/QCHMSSAP");
        File f = new File(GlobalHelper.getDatabasePathHMSBackup() +"/"+System.currentTimeMillis()+"_"+sumber+".zip");

        ZipArchive zipArchive = new ZipArchive();
        zipArchive.zip(extStatisticsFolder.getPath(),f.getPath(),"");

        if(param.equals("SYNC")) {
            try {
                GlobalHelper.copyFile(f,new File(GlobalHelper.getDatabasePathHMSTemp()) ,"QCHMSTemp.zip");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        HashMap<Integer,File> fileDbHms = GlobalHelper.getBackUpPathHMS();
        int dbCount = 1;
        for(HashMap.Entry<Integer,File> entry : fileDbHms.entrySet()){
            int count = 0;
            File dir = entry.getValue();
//            boolean hapus = false;
//            if(param.equals("SYNC")) {
//                if(syncTimeTransaksi.isSelected()){
//                    String sDir = dir.getAbsolutePath();
//                    if(sDir.contains(GlobalHelper.EXTERNAL_DIR_FILES+"/HMS/db2")){
//                        hapus = true;
//                    }
//                }
//                if(syncTimeMaster.isSelected()){
//                    String sDir = dir.getAbsolutePath();
//                    if(sDir.contains(GlobalHelper.EXTERNAL_DIR_FILES+"/HMS/db/"+ GlobalHelper.getEstate().getEstCode())){
//                        hapus = true;
//                    }
//                }
//            }
            for (File fileEntry : dir.listFiles()) {
//                if(hapus) {
                    try {
                        if (fileEntry.isDirectory()) {
                            if (param.equals("SYNC")) {
                                String sDir = dir.getAbsolutePath();
                                if (sDir.contains(BuildConfig.BUILD_VARIANT.equals("dev") ? GlobalHelper.EXTERNAL_DIR_FILES + "/QCHMSSAPTESTING/db/" + GlobalHelper.getEstate().getEstCode() : GlobalHelper.EXTERNAL_DIR_FILES + "/QCHMSSAP/db/" + GlobalHelper.getEstate().getEstCode())) {
//                                    biar nga keseringan aktive
//                                    if(syncTimeTransaksi.isSelected()){
//                                        if(SyncTimeHelper.cekDbTranskasi(fileEntry.getName())){
//                                            FileUtils.cleanDirectory(fileEntry);
//                                            Log.d(this.getClass() + " hapus dir 1a ", fileEntry.getAbsolutePath() + "("+Encrypts.decrypt(fileEntry.getName()) + ")");
//                                        }
//                                    }
//                                    if(syncTimeMaster.isSelected()){
//                                        if(SyncTimeHelper.cekDbMaster(fileEntry.getName())){
//                                            FileUtils.cleanDirectory(fileEntry);
//                                            Log.d(this.getClass() + " hapus dir 1a M ", fileEntry.getAbsolutePath() + "("+Encrypts.decrypt(fileEntry.getName()) + ")");
//                                        }
//                                    }
                                } else {
                                    if(syncTimeTransaksi.isSelected()){
                                        if(SyncTimeHelper.cekFolderTransaksi(fileEntry.getName())) {
                                            if(SyncTimeHelper.cekFolderTransaksi(fileEntry.getName())) {
                                                FileUtils.cleanDirectory(fileEntry);
                                                Log.d(this.getClass() + " hapus dir 1b ", fileEntry.getAbsolutePath());
                                            }
                                        }
                                    }
                                    if(syncTimeMaster.isSelected()){
                                        if(SyncTimeHelper.cekFolderMaster(fileEntry.getName())) {
                                            if(SyncTimeHelper.cekFolderMaster(fileEntry.getName())) {
                                                FileUtils.cleanDirectory(fileEntry);
                                                Log.d(this.getClass() + " hapus dir 1b M ", fileEntry.getAbsolutePath());
                                            }
                                        }
                                    }
                                }
                            } else {
                                FileUtils.cleanDirectory(fileEntry);
                                Log.d(this.getClass() + " hapus dir 1c ", fileEntry.getAbsolutePath());
                            }
                        } else if (fileEntry.isFile()) {
                            if (param.equals("SYNC")) {
                                String sDir = dir.getAbsolutePath();
                                if (sDir.contains(BuildConfig.BUILD_VARIANT.equals("dev") ? GlobalHelper.EXTERNAL_DIR_FILES + "/QCHMSSAPTESTING/db/" + GlobalHelper.getEstate().getEstCode() : GlobalHelper.EXTERNAL_DIR_FILES + "/QCHMSSAP/db/" + GlobalHelper.getEstate().getEstCode())) {
//                                    if(syncTimeTransaksi.isSelected()){
//                                        if(SyncTimeHelper.cekDbTranskasi(dir.getName())){
//                                            fileEntry.delete();
//                                            Log.d(this.getClass() + " hapus file 1a", fileEntry.getAbsolutePath());
//                                        }
//                                    }
                                } else {
                                    fileEntry.delete();
                                    Log.d(this.getClass() + " hapus file 2b", fileEntry.getAbsolutePath());
                                }
                            } else {
                                fileEntry.delete();
                                Log.d(this.getClass() + " hapus file 2c", fileEntry.getAbsolutePath());
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
//                }

                try {
                    JSONObject objProgres = new JSONObject();
                    objProgres.put("ket", "Backup DB " + dbCount);
                    objProgres.put("persen", (count * 100 / dir.listFiles().length));
                    objProgres.put("count", count);
                    objProgres.put("total", (dir.listFiles().length));
                    count++;
                    if (longOperation instanceof SettingFragment.LongOperation) {
                        ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                    } else if (longOperation instanceof ActiveActivity.LongOperation) {
                        ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                    } else if (longOperation instanceof MapActivity.LongOperation) {
                        ((MapActivity.LongOperation) longOperation).publishProgress(objProgres);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            try {
//                if(hapus) {
                    if (param.equals("SYNC")) {
                        String sDir = dir.getAbsolutePath();
                        if (sDir.contains(BuildConfig.BUILD_VARIANT.equals("dev") ? GlobalHelper.EXTERNAL_DIR_FILES + "/QCHMSSAPTESTING/db/" + GlobalHelper.getEstate().getEstCode() : GlobalHelper.EXTERNAL_DIR_FILES + "/QCHMSSAP/db/" + GlobalHelper.getEstate().getEstCode())) {
//                            if(syncTimeTransaksi.isSelected()){
//                                if(SyncTimeHelper.cekDbTranskasi(dir.getName())){
//                                    FileUtils.cleanDirectory(dir);
//                                    Log.d(this.getClass() + " hapus dir 1a ", dir.getAbsolutePath() + "("+Encrypts.decrypt(dir.getName()) + ")");
//                                }
//                            }
//                            if(syncTimeMaster.isSelected()){
//                                if(SyncTimeHelper.cekDbMaster(dir.getName())){
//                                    FileUtils.cleanDirectory(dir);
//                                    Log.d(this.getClass() + " hapus dir 1a M ", dir.getAbsolutePath() + "("+Encrypts.decrypt(dir.getName()) + ")");
//                                }
//                            }
                        } else {
                            if(syncTimeTransaksi.isSelected()){
                                if(SyncTimeHelper.cekFolderTransaksi(dir.getName())) {
                                    if(SyncTimeHelper.cekFolderTransaksi(dir.getName())) {
                                        FileUtils.cleanDirectory(dir);
                                        Log.d(this.getClass() + " hapus dir 1b ", dir.getAbsolutePath());
                                    }
                                }
                            }
                            if(syncTimeMaster.isSelected()){
                                if(SyncTimeHelper.cekFolderMaster(dir.getName())) {
                                    if(SyncTimeHelper.cekFolderMaster(dir.getName())) {
                                        FileUtils.cleanDirectory(dir);
                                        Log.d(this.getClass() + " hapus dir 1b M ", dir.getAbsolutePath());
                                    }
                                }
                            }
                        }
                    } else {
                        FileUtils.cleanDirectory(dir);
                        Log.d(this.getClass() + " hapus dir 2c", dir.getAbsolutePath());
                    }
//                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            dbCount++;
        }

        for(Estate estate : estates) {
            setupHMSFolder(estate.getEstCode());
        }

    }

    public boolean setUpLastSync(CoordinatorLayout myCoordinatorLayout){
        try {

            ArrayList<Estate> estates = new ArrayList<>();
            if (context instanceof MainMenuActivity) {
                MainMenuActivity mainMenuActivity = ((MainMenuActivity) context);
                estates = mainMenuActivity.estatesMapping;
            } else if (context instanceof ActiveActivity) {
                ActiveActivity activeActivity = ((ActiveActivity) context);
                estates = activeActivity.estatesMapping;
            } else {
                estates.add(GlobalHelper.getEstate());
            }

            for(Estate estate : estates) {
                File db = new File(GlobalHelper.getDatabasePathHMS(estate.getEstCode()));
                for (File file : db.listFiles()) {
                    if (!file.isDirectory()) {
                        file.delete();
                    }
                }

                File file = new File(db, Encrypts.encrypt(GlobalHelper.LAST_SYNC));


                PackageManager manager = HarvestApp.getContext().getPackageManager();
                PackageInfo info = manager.getPackageInfo(context.getPackageName(), PackageManager.GET_ACTIVITIES);

                JSONObject object = new JSONObject();
                object.put("Time", System.currentTimeMillis());
                object.put("PackageName", info.packageName);
                object.put("VersionCode", info.versionCode);
                object.put("VersionName", info.versionName);
                GlobalHelper.writeFileContent(file.getAbsolutePath(), String.valueOf(object));
            }

            SyncHistoryHelper.insertHistorySync(SyncHistroy.Sync_Berhasil);

            clearHmsTemp();

            GlobalHelper.setUpAllData();
            Snackbar snackbar = Snackbar.make(myCoordinatorLayout,"Sync Berhasil",Snackbar.LENGTH_INDEFINITE);
            snackbar.show();
            return true;
        }catch (Exception e){
            Log.e("setUpLastSync",e.toString());
            return false;
        }

    }

    public String getLastSync(){
        File file = new File(GlobalHelper.getDatabasePathHMS(),Encrypts.encrypt(GlobalHelper.LAST_SYNC));
        String isi = GlobalHelper.readFileContent(file.getAbsolutePath());
        return isi.isEmpty() ? null : isi;
    }

    public void clearHmsTemp(){
        File f = new File(GlobalHelper.getDatabasePathHMSTemp() + GlobalHelper.getLastGetdataName());
        if(f.exists()){
            Log.d(GlobalHelper.getLastGetdataName(),f.getAbsolutePath());
            f.delete();
        }

        File f1 = new File(GlobalHelper.getDatabasePathHMSTemp() + GlobalHelper.getCountBadResponseName());
        if(f1.exists()){
            f1.delete();
        }

        File hmsTemp = new File(GlobalHelper.getDatabasePathHMSTemp() + "QCHMSTemp.zip");
        if (hmsTemp.exists()) {
            hmsTemp.delete();
        }
    }

    private void hapusDb(String namaTable){
        ArrayList<String> allDb = GlobalHelper.getAllFileDB(namaTable,false);
        for(String dbFile : allDb) {
            File fileDb = new File(dbFile);
            if (fileDb.exists()) {
                fileDb.delete();
            }
        }
    }

    private void hapusDb(String estCode,String namaTable){
        ArrayList<String> allDb = GlobalHelper.getAllFileDB(estCode,namaTable,false);
        for(String dbFile : allDb) {
            File fileDb = new File(dbFile);
            if (fileDb.exists()) {
                fileDb.delete();
            }
        }
    }
}
