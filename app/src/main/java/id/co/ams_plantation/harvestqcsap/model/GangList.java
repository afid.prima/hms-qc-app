package id.co.ams_plantation.harvestqcsap.model;

public class GangList {

    private String idGangList;
    private String gank;
    private String afdeling;

    /**
     * No args constructor for use in serialization
     *
     */
    public GangList() {
    }

    public GangList(String gank, String afdeling) {
        this.idGangList = gank+"_"+afdeling;
        this.gank = gank;
        this.afdeling = afdeling;
    }

    public GangList(String idGangList, String gank, String afdeling) {
        this.idGangList = idGangList;
        this.gank = gank;
        this.afdeling = afdeling;
    }

    public String getIdGangList() {
        return idGangList;
    }

    public void setIdGangList(String idGangList) {
        this.idGangList = idGangList;
    }

    public String getGank() {
        return gank;
    }

    public void setGank(String gank) {
        this.gank = gank;
    }

    public String getAfdeling() {
        return afdeling;
    }

    public void setAfdeling(String afdeling) {
        this.afdeling = afdeling;
    }
}
