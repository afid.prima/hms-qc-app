package id.co.ams_plantation.harvestqcsap.util;

import static id.co.ams_plantation.harvestqcsap.Fragment.QcFotoPanenFragmet.STATUS_LONG_OPERATION_NONE;
import static id.co.ams_plantation.harvestqcsap.model.SessionQcFotoPanen.Status_Deleted;
import static id.co.ams_plantation.harvestqcsap.model.SessionQcFotoPanen.Status_Upload_Qc;

import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;
import com.rpolicante.keyboardnumber.KeyboardNumberPicker;
import com.rpolicante.keyboardnumber.KeyboardNumberPickerHandler;
import com.weiwangcn.betterspinner.library.BetterSpinner;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import id.co.ams_plantation.harvestqcsap.Fragment.QcFotoPanenFragmet;
import id.co.ams_plantation.harvestqcsap.HarvestApp;
import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.adapter.SelectedChoiceSelectionAdapter;
import id.co.ams_plantation.harvestqcsap.connection.ServiceResponse;
import id.co.ams_plantation.harvestqcsap.model.Answer;
import id.co.ams_plantation.harvestqcsap.model.ChoiceSelection;
import id.co.ams_plantation.harvestqcsap.model.DataNitrit;
import id.co.ams_plantation.harvestqcsap.model.FilePdf;
import id.co.ams_plantation.harvestqcsap.model.QcFotoPanen;
import id.co.ams_plantation.harvestqcsap.model.QcMutuBuah;
import id.co.ams_plantation.harvestqcsap.model.QuestionAnswer;
import id.co.ams_plantation.harvestqcsap.model.SessionQcFotoPanen;
import id.co.ams_plantation.harvestqcsap.model.User;
import id.co.ams_plantation.harvestqcsap.ui.MainMenuActivity;
import ru.slybeaver.slycalendarview.SlyCalendarDialog;

/**
 * Created on : 12,October,2021
 * Author     : Afid
 */

public class SessionFotoPanenHelper {
    public static String SESSION_CURRENT = "SESSION_CURRENT.json";
    public static String FULLPATH_SESSION_CURRENT = Environment.getExternalStorageDirectory() +
            GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 + "/" +
            GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_QC_FOTO_PANEN] + "/" +
            SESSION_CURRENT;
    public static String FULLPATH_REPORT = Environment.getExternalStorageDirectory() +GlobalHelper.REPORT_DIR_FILES + "/";

    FragmentActivity activity;
    AlertDialog listrefrence;

    AppCompatEditText etTgl;
    BetterSpinner etAfdeling;
    LinearLayout lAfdeling;
    LinearLayout lType;
    CompleteTextViewHelper etTypeValue;
    TextView tvType;

    SimpleDateFormat formatDate = new SimpleDateFormat("dd MMMM yyyy");
    SimpleDateFormat sdfApi = new SimpleDateFormat("yyyyMMdd");

    Long tglFoto;
    QuestionAnswer questionAnswerMethode;
    QuestionAnswer questionAnswerSelection;
    Answer method;
    Answer selection;
    ChoiceSelection choiceSelection;
    ArrayList<ChoiceSelection> choiceSelections;
    JSONObject objSend;

    public SessionFotoPanenHelper(FragmentActivity activity) {
        this.activity = activity;
    }

    public void sessionForm(QuestionAnswer questionAnswerMethode,QuestionAnswer questionAnswerSelection,String afdelingObj){
        View view = LayoutInflater.from(activity).inflate(R.layout.new_session_layout,null);
        etTgl = view.findViewById(R.id.etTgl);
        etAfdeling = view.findViewById(R.id.etAfdeling);
        EditText etSample = view.findViewById(R.id.etSample);
        BetterSpinner etMethod = view.findViewById(R.id.etMethod);
        BetterSpinner etSection = view.findViewById(R.id.etSection);
        ImageView ivSample = view.findViewById(R.id.ivSample);
        ImageView ivInformation = view.findViewById(R.id.ivInformation);
        etTypeValue = view.findViewById(R.id.etTypeValue);
        tvType = view.findViewById(R.id.tvType);
        lAfdeling = view.findViewById(R.id.lType);
        lType = view.findViewById(R.id.lType);
        LinearLayout lsave = view.findViewById(R.id.lsave);
        LinearLayout lcancel = view.findViewById(R.id.lcancel);

        this.questionAnswerMethode = questionAnswerMethode;
        this.questionAnswerSelection = questionAnswerSelection;
        lType.setVisibility(View.GONE);
        ivInformation.setVisibility(View.GONE);
        tglFoto = System.currentTimeMillis() - (1 * 24 * 3600 * 1000);
        etTgl.setText(formatDate.format(tglFoto));
        etSample.setText("20");
//        etSample.setInputType(InputType.TYPE_CLASS_NUMBER);
//        etSample.setRawInputType(Configuration.KEYBOARD_12KEY);

        try {
            List<String> lAfd = new ArrayList<>();
            lAfd.add("All");
            JSONObject objAfdeling = new JSONObject(afdelingObj);
            if(objAfdeling.names() != null) {
                for (int i = 0; i < objAfdeling.names().length(); i++) {
                    lAfd.add(objAfdeling.names().get(i).toString());
                }
            }

            ArrayAdapter<String> adapterAfdeling = new ArrayAdapter<String>(activity,
                    android.R.layout.simple_dropdown_item_1line, lAfd);
            etAfdeling.setAdapter(adapterAfdeling);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        List<String> lMethod = new ArrayList<>();
        List<String> lSelection = new ArrayList<>();
        choiceSelections = new ArrayList<>();

        for(int i = 0 ;i < questionAnswerMethode.getAnswer().size(); i++){
            lMethod.add(questionAnswerMethode.getAnswer().get(i).getQcAnsware());
        }
        for(int i = 0 ;i < questionAnswerSelection.getAnswer().size(); i++){
            lSelection.add(questionAnswerSelection.getAnswer().get(i).getQcAnsware());
        }

        ArrayAdapter<String> adapterMethod = new ArrayAdapter<String>(activity,
                android.R.layout.simple_dropdown_item_1line, lMethod);
        ArrayAdapter<String> adapterSelection = new ArrayAdapter<String>(activity,
                android.R.layout.simple_dropdown_item_1line, lSelection);

        etMethod.setAdapter(adapterMethod);
        etSection.setAdapter(adapterSelection);

        final KeyboardNumberPicker picker = new KeyboardNumberPicker
                .Builder(1001)
                .create();

        ivSample.setImageDrawable(new
                IconicsDrawable(activity)
                .icon(MaterialDesignIconic.Icon.gmi_keyboard).sizeDp(42)
                .paddingDp(8)
                .colorRes(R.color.Black));

        ivSample.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picker.clearKeyboard();
                picker.show(activity.getSupportFragmentManager(), "KeyboardNumberPicker");
            }
        });

        ivInformation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                GlobalHelper.dialogHelpDescription(activity,"NewSessionQc");
            }
        });

        picker.setListenerPickerHandler(new KeyboardNumberPickerHandler() {
            @Override
            public void onConfirmAction(KeyboardNumberPicker picker, String value) {
                etSample.setText(value);
            }

            @Override
            public void onCancelAction(KeyboardNumberPicker picker) {
                Log.d("KeyboardNumberPicker", "Cancel action");
            }
        });

        etMethod.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setMethod(etMethod.getText().toString());
            }
        });

        etSection.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setSelection(etSection.getText().toString());
            }
        });

        etTgl.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openDateRangePicker();
            }
        });

        etTgl.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                HitApiPCSWeb();
            }
        });

        etTypeValue.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SelectedChoiceSelectionAdapter adapter = (SelectedChoiceSelectionAdapter) parent.getAdapter();
                ChoiceSelection choiceSelection = adapter.getItemAt(position);
                Log.d(this.getClass().toString(), choiceSelection.getText());
                setChoiceSelection(choiceSelection);
            }
        });

        etSample.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picker.clearKeyboard();
                picker.show(activity.getSupportFragmentManager(), "KeyboardNumberPicker");
            }
        });

        lsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etAfdeling.getText().toString().isEmpty()){
                    Toast.makeText(HarvestApp.getContext(),"Mohon Pilih Afdeling",Toast.LENGTH_SHORT).show();
                    return;
                }else if (method == null){
                    Toast.makeText(HarvestApp.getContext(),"Mohon Pilih Method",Toast.LENGTH_SHORT).show();
                    return;
                }else if (selection == null){
                    Toast.makeText(HarvestApp.getContext(),"Mohon Pilih Selection",Toast.LENGTH_SHORT).show();
                    return;
                }

                if(!selection.getIdQcAnswer().toUpperCase().equals("AA26")) {
                    if (choiceSelections.size() > 0) {
                        if (choiceSelection == null) {
                            Toast.makeText(HarvestApp.getContext(), "Mohon Pilih " + tvType.getText().toString(), Toast.LENGTH_SHORT).show();
                            return;
                        } else {
                            if (!choiceSelection.getType().toLowerCase().equals(selection.getQcAnsware().toLowerCase())) {
                                Toast.makeText(HarvestApp.getContext(), "Mohon Pilih " + selection.getQcAnsware() + " Yang Sesuai", Toast.LENGTH_SHORT).show();
                                etTypeValue.setText("");
                                etTypeValue.showDropDown();
                                return;
                            }
                        }
                    } else {
                        choiceSelection = new ChoiceSelection();
                    }
                } else {
                    choiceSelection = new ChoiceSelection();
                }

                if(etSample.getText().toString().isEmpty()){
                    Toast.makeText(HarvestApp.getContext(), "Mohon Input Banyak nya Sample",Toast.LENGTH_SHORT).show();
                    return;
                }else{
                    int sample = Integer.parseInt(etSample.getText().toString());
                    if(sample > 20){
                        Toast.makeText(HarvestApp.getContext(), "Request Foto Terlalu Banyak Max 20",Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

                try {
                    objSend = new JSONObject();
                    Gson gson = new Gson();
                    objSend.put("requestDate",sdfApi.format(tglFoto));
                    objSend.put("createBy",GlobalHelper.getUser().getUserID());
                    objSend.put("estCode",GlobalHelper.getEstate().getEstCode());
                    objSend.put("afdeling",etAfdeling.getText().toString());
                    objSend.put("methode",new JSONObject(gson.toJson(method)));
                    objSend.put("selection",new JSONObject(gson.toJson(selection)));
                    objSend.put("sectionValue",new JSONObject(gson.toJson(choiceSelection)));
                    objSend.put("sample",etSample.getText().toString());
                    Log.d(this.getClass().toString(),objSend.toString());

                    SessionQcFotoPanen cekSessionQcFotoPanen = cekSession(objSend);
                    if(cekSessionQcFotoPanen != null){
                        updateTanggalCreateDate(cekSessionQcFotoPanen);
                        Toast.makeText(HarvestApp.getContext(),"Parameter yang anda input sama dengan session "+ cekSessionQcFotoPanen.getIdSession(),Toast.LENGTH_LONG).show();

                        if (activity instanceof MainMenuActivity) {
                            Fragment fragment = ((MainMenuActivity) activity).getFragmentCurrent();
                            if (fragment instanceof QcFotoPanenFragmet) {
                                ((QcFotoPanenFragmet) fragment).startLongOperation(STATUS_LONG_OPERATION_NONE);
                            }
                        }
                    }else {
                        if (activity instanceof MainMenuActivity) {
                            Fragment fragment = ((MainMenuActivity) activity).getFragmentCurrent();
                            if (fragment instanceof QcFotoPanenFragmet) {
                                ((QcFotoPanenFragmet) fragment).insertNewSession(objSend);
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                listrefrence.dismiss();
            }
        });

        lcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listrefrence.dismiss();
            }
        });

        listrefrence = WidgetHelper.showFormDialog(listrefrence,view,activity);
    }


    public String cekSessionQc(){
        String message = "ok";
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_FOTO_PANEN);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = Iterable.iterator(); iterator.hasNext();) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            SessionQcFotoPanen sessionQcFotoPanen = gson.fromJson(dataNitrit.getValueDataNitrit(),SessionQcFotoPanen.class);

            if(message.equals("ok")){
                if(sessionQcFotoPanen.getStatus() != Status_Deleted) {
                    Long batas = System.currentTimeMillis() - (1 * 24 * 3600 * 1000);
                    Long waktu = sessionQcFotoPanen.getUpdateDate() != 0 ? sessionQcFotoPanen.getUpdateDate() : sessionQcFotoPanen.getCreateDate();

                    Long selishBatas = TimeUnit.MILLISECONDS.toDays(waktu - batas);
                    if (selishBatas == 0 && selishBatas <= 2) {


                        int persen = getPersen(sessionQcFotoPanen);

                        if (persen < GlobalHelper.MIN_PERSEN_QC_FOTO) {
                            message = "Mohon Selesaikan Id Qc " +
                                    sessionQcFotoPanen.getIdSession().substring(0, 11) +
                                    " pada Tgl " +
                                    formatDate.format(waktu);
                        }
                    }
                }
            }

        }
        db.close();
        return  message;
    }

    public void updateCurentSession(SessionQcFotoPanen sessionQcFotoPanen){
        Gson gson = new Gson();
        String sessionCurrent = gson.toJson(sessionQcFotoPanen);
        GlobalHelper.writeFileContent(FULLPATH_SESSION_CURRENT,sessionCurrent);
    }

    public SessionQcFotoPanen getCurentSession(){
        Gson gson = new Gson();
        return  gson.fromJson(GlobalHelper.readFileContent(FULLPATH_SESSION_CURRENT),SessionQcFotoPanen.class);
    }

    private SessionQcFotoPanen cekSession(JSONObject object){
        Gson gson = new Gson();
        SessionQcFotoPanen returnSessionQcFotoPanen = null;
        SessionQcFotoPanen sessionQcFotoPanen = gson.fromJson(object.toString(),SessionQcFotoPanen.class);
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_FOTO_PANEN);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = Iterable.iterator(); iterator.hasNext();) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            SessionQcFotoPanen sessionQcFotoPanen1 = gson.fromJson(dataNitrit.getValueDataNitrit(),SessionQcFotoPanen.class);
            if(
                sessionQcFotoPanen1.getRequestDate().equals(sessionQcFotoPanen.getRequestDate())
                && sessionQcFotoPanen1.getEstCode().equals(sessionQcFotoPanen.getEstCode())
                && sessionQcFotoPanen1.getAfdeling().equals(sessionQcFotoPanen.getAfdeling())
                && sessionQcFotoPanen1.getSample() == sessionQcFotoPanen.getSample()
                && sessionQcFotoPanen1.getMethode().getIdQcAnswer().equals(sessionQcFotoPanen.getMethode().getIdQcAnswer())
                && sessionQcFotoPanen1.getSelection().getIdQcAnswer().equals(sessionQcFotoPanen.getSelection().getIdQcAnswer())
            ){
                if(sessionQcFotoPanen1.getSectionValue().getValue() != null) {
                    if (sessionQcFotoPanen1.getSectionValue().getValue().equals(sessionQcFotoPanen.getSectionValue().getValue())) {
                        returnSessionQcFotoPanen = sessionQcFotoPanen1;
                    }
                }else{
                    returnSessionQcFotoPanen = sessionQcFotoPanen1;
                }
            }
        }
        db.close();
        return returnSessionQcFotoPanen;
    }


    private void updateTanggalCreateDate(SessionQcFotoPanen sessionQcFotoPanen){
        Gson gson = new Gson();
        sessionQcFotoPanen.setUpdateDate(System.currentTimeMillis());

        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_FOTO_PANEN);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", sessionQcFotoPanen.getIdSession()));
        DataNitrit dataNitrit = new DataNitrit(sessionQcFotoPanen.getIdSession(),gson.toJson(sessionQcFotoPanen));
        if(cursor.size() > 0) {
            repository.update(dataNitrit);
        }else{
            repository.insert(dataNitrit);
        }
        db.close();
    }

    private void setMethod(String answer){
        for(int i = 0 ; i < questionAnswerMethode.getAnswer().size();i++){
            if(questionAnswerMethode.getAnswer().get(i).getQcAnsware().toUpperCase().equals(answer.toUpperCase())){
                method = questionAnswerMethode.getAnswer().get(i);
                break;
            }
        }
        showHideDynamicType();
    }

    private void setSelection(String answer){
        for(int i = 0 ; i < questionAnswerSelection.getAnswer().size();i++){
            if(questionAnswerSelection.getAnswer().get(i).getQcAnsware().toUpperCase().equals(answer.toUpperCase())){
                selection = questionAnswerSelection.getAnswer().get(i);
                break;
            }
        }
        showHideDynamicType();
    }

    private void setChoiceSelection(ChoiceSelection choiceSelection){
        etTypeValue.setText(choiceSelection.getText());
        this.choiceSelection = choiceSelection;
    }

    private void showHideDynamicType(){

        boolean visible = false;
        if(selection != null){
            if(!selection.getIdQcAnswer().toUpperCase().equals("AA26")){
                visible = true;
            }
        }

        if(visible){
            lAfdeling.setVisibility(View.GONE);
            lType.setVisibility(View.VISIBLE);
            tvType.setText(selection.getQcAnsware());
        }else{
            lAfdeling.setVisibility(View.VISIBLE);
            lType.setVisibility(View.GONE);
        }

        HitApiPCSWeb();
    }

    private void HitApiPCSWeb(){
        if( lType.getVisibility() == View.GONE){
            return;
        }else if(etTgl.getText().toString().isEmpty()){
            Toast.makeText(HarvestApp.getContext(),"Mohon Isi Tgl Dahulu",Toast.LENGTH_SHORT).show();
            etTgl.requestFocus();
            return;
        }else if(etAfdeling.getText().toString().isEmpty()){
            Toast.makeText(HarvestApp.getContext(),"Mohon Isi Afdeling Dahulu",Toast.LENGTH_SHORT).show();
            etAfdeling.requestFocus();
            return;
        }

        try {
            JSONObject object = new JSONObject();

            object.put("estCode", GlobalHelper.getEstate().getEstCode());
            object.put("dateParam", sdfApi.format(tglFoto));
            object.put("afdeling", etAfdeling.getText().toString());

            if(object.length() > 0) {
                if (activity instanceof MainMenuActivity) {
                    Fragment fragment = ((MainMenuActivity) activity).getFragmentCurrent();
                    if (fragment instanceof QcFotoPanenFragmet) {
                        ((QcFotoPanenFragmet) fragment).preGetChoiceSelection(object);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String setUpNewSession(ServiceResponse responseApi){
        SessionQcFotoPanen sessionQcFotoPanen = new SessionQcFotoPanen();
        Answer methode = new Answer();
        Answer selection = new Answer();
        ChoiceSelection sectionValue = new ChoiceSelection();
        HashMap<String,QcFotoPanen> qcFotoPanenHashMap = new HashMap<>();
        Gson gson = new Gson();
        try{

            JSONObject data = new JSONObject(responseApi.getData().toString());

            if(!data.has("methode")){
                return "Tidak Ada Panen Pada Parameter Ini";
            }

            if(data.has("methode")){
                methode = gson.fromJson(data.get("methode").toString(),Answer.class);
            }
            if(data.has("selection")){
                selection = gson.fromJson(data.get("selection").toString(),Answer.class);
            }
            if(data.has("sectionValue")){
                sectionValue = gson.fromJson(data.get("sectionValue").toString(),ChoiceSelection.class);
            }

            sessionQcFotoPanen = new SessionQcFotoPanen(
                    data.getString("idSession"),
                    String.valueOf(data.getInt("createBy")),
                    data.getString("requestDate"),
                    data.getString("estCode"),
                    data.getString("afdeling"),
                    methode,
                    selection,
                    sectionValue,
                    data.getInt("sample"),
                    System.currentTimeMillis(),
                    System.currentTimeMillis(),
                    sessionQcFotoPanen.Status_OutStanding_Qc
            );

            int sudahQc = 0;
            JSONArray dataArray = data.getJSONArray("QcFotoPanen");
            if(dataArray.length() == 0 ){
                return "Request Tidak Benar Mohon Di Cek";
            }
            for(int i = 0 ; i < dataArray.length();i++){
                JSONObject obj = new JSONObject(dataArray.get(i).toString());
                if(obj.has("dataQcFotoPanen")){
                    sudahQc++;
                }

                JSONArray jImg = obj.getJSONArray("foto");
                HashSet<String> foto = new HashSet<>();
                for(int j = 0 ; j < jImg.length(); j++){
                    JSONObject objImg = jImg.getJSONObject(j);
                    String fullPath = Environment.getExternalStorageDirectory() +
                            GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 + "/" +
                            GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_QC_FOTO_PANEN] + "/" +
                            objImg.get("imgName");
                    if(GlobalHelper.decode64ImageStringToBitMap(objImg.getString("img"),fullPath)){
                        foto.add(fullPath);
                    }
                }
                obj.remove("foto");
                QcFotoPanen qcFotoPanen = gson.fromJson(obj.toString(),QcFotoPanen.class);
                qcFotoPanen.setFoto(foto);
                qcFotoPanenHashMap.put(qcFotoPanen.getIdTPanen(),qcFotoPanen);
            }

            ArrayList<QcFotoPanen> qcFotoPanens = new ArrayList<QcFotoPanen>(qcFotoPanenHashMap.values());
            Collections.sort(qcFotoPanens, new Comparator<QcFotoPanen>() {
                @Override
                public int compare(QcFotoPanen o1, QcFotoPanen o2) {
                    return o2.getCreateDate() > o1.getCreateDate() ? -1 : (o2.getCreateDate() < o1.getCreateDate()) ? 1 : 0;
                }
            });
            sessionQcFotoPanen.setQcFotoPanen(qcFotoPanens);
            if(sudahQc > 0){
                int persen = (int) (Double.parseDouble(String.valueOf(sudahQc)) /
                        Double.parseDouble(String.valueOf(dataArray.length())) *
                        100.00
                );

                if(persen >= 99){
                    sessionQcFotoPanen.setStatus(SessionQcFotoPanen.Status_Done_Qc);
                }
            }


            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_FOTO_PANEN);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", sessionQcFotoPanen.getIdSession()));
            DataNitrit dataNitrit = new DataNitrit(sessionQcFotoPanen.getIdSession(),gson.toJson(sessionQcFotoPanen));
            if(cursor.size() > 0) {
                repository.update(dataNitrit);
            }else{
                repository.insert(dataNitrit);
            }
            db.close();
            return "ok";
        }catch (JSONException e) {
            e.printStackTrace();
            return  e.toString();
        }
    }

    public String hapusSession(String idSession){
        String message = "ok";
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_FOTO_PANEN);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", idSession));
        for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            SessionQcFotoPanen sessionQcFotoPanen = gson.fromJson(dataNitrit.getValueDataNitrit(), SessionQcFotoPanen.class);
            if(sessionQcFotoPanen.getStatus() != Status_Upload_Qc) {
                int persen = getPersen(sessionQcFotoPanen);
                if (persen < GlobalHelper.MIN_PERSEN_QC_FOTO) {
                    sessionQcFotoPanen.setStatus(Status_Deleted);
                    DataNitrit nitrit = new DataNitrit(sessionQcFotoPanen.getIdSession(),gson.toJson(sessionQcFotoPanen));
                    repository.update(nitrit);
                }else{
                    message = "Tidak Bisa Di Hapus Proses Qc >= " + GlobalHelper.MIN_PERSEN_QC_FOTO + " % Qc";
                }
            }else{
                message = "Sudah Di Upload Tidak Bisa Di Hapus";
            }
            break;
        }
        db.close();
        return  message;
    }

    public void setUpTypeValue(ServiceResponse responseApi){
        choiceSelections = new ArrayList<>();
        try {
            JSONArray data = new JSONArray(responseApi.getData().toString());
            for (int i = 0; i < data.length(); i++) {
                JSONObject obj = (JSONObject) data.get(i);
                Gson gson = new Gson();
                choiceSelections.add(gson.fromJson(obj.toString(),ChoiceSelection.class));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setUpChoiceSelection(){
        ArrayList<ChoiceSelection> canChoice = new ArrayList();
        for(int i = 0 ; i < choiceSelections.size();i++) {
            ChoiceSelection choiceSelection = choiceSelections.get(i);
            if (tvType.getText().toString().toLowerCase().equals(choiceSelection.getType().toLowerCase())) {
                canChoice.add(choiceSelection);
            }
        }

        SelectedChoiceSelectionAdapter adapter = new SelectedChoiceSelectionAdapter(activity, R.layout.row_object, canChoice);
        etTypeValue.setAdapter(adapter);
        etTypeValue.showDropDown();

        if(canChoice.size() == 0){
            Toast.makeText(HarvestApp.getContext(),"Tidak ada yang bisa di pilih",Toast.LENGTH_SHORT).show();
        }
    }

    private void openDateRangePicker(){

        SlyCalendarDialog dialog = new SlyCalendarDialog()
                .setSingle(true)
                .setHeaderColor(activity.getResources().getColor(R.color.Green))
                .setSelectedColor(activity.getResources().getColor(R.color.md_orange_400))
                .setCallback(new SlyCalendarDialog.Callback() {
                    @Override
                    public void onCancelled() {

                    }

                    @Override
                    public void onDataSelected(Calendar firstDate, Calendar secondDate, int hours, int minutes) {
                        if(firstDate != null) {
                            long batas = System.currentTimeMillis() - GlobalHelper.MAX_FOTO_PANEN;
                            if (System.currentTimeMillis() < firstDate.getTimeInMillis()) {
                                Toast.makeText(HarvestApp.getContext(), "Tanggal Foto Harus Di Bawah Atau Sama Dengan Tgl Hari Ini ", Toast.LENGTH_SHORT).show();
                                openDateRangePicker();
                                return;
                            }else if(batas > firstDate.getTimeInMillis()){
                                batas += (1 * 24 * 3600 * 1000);
                                Toast.makeText(HarvestApp.getContext(), "Tidak Bisa Request Foto Kurang Dari "+formatDate.format(batas), Toast.LENGTH_SHORT).show();
                                openDateRangePicker();
                                return;
                            }
                            tglFoto = firstDate.getTimeInMillis();
                            etTgl.setText(formatDate.format(tglFoto));
                        }
                    }
                });
        dialog.show(activity.getSupportFragmentManager(), "TAG_SLYCALENDAR");
    }

    public static String getRequestDateSession(SessionQcFotoPanen sessionQcFotoPanen){
        return sessionQcFotoPanen.getRequestDate().substring(6,8)
                + "-"+ sessionQcFotoPanen.getRequestDate().substring(4,6)
                + "-"+ sessionQcFotoPanen.getRequestDate().substring(0,4);
    }

    public static int getPersen(SessionQcFotoPanen sessionQcFotoPanen){
        int sudahQc = 0 ;
        for(int x = 0 ; x < sessionQcFotoPanen.getQcFotoPanen().size(); x++){
            if(sessionQcFotoPanen.getQcFotoPanen().get(x).getDataQcFotoPanen() != null){
                sudahQc++;
            }
        }

        int persen = (int) (Double.parseDouble(String.valueOf(sudahQc)) /
                Double.parseDouble(String.valueOf(sessionQcFotoPanen.getQcFotoPanen().size())) *
                100.00
        );

        return persen;
    }

    public static String getSelection(SessionQcFotoPanen sessionQcFotoPanen){
        String selection = sessionQcFotoPanen.getSelection().getQcAnsware();
        if(sessionQcFotoPanen.getSectionValue().getText() != null) {
            if (selection.toUpperCase().equals(sessionQcFotoPanen.getSectionValue().getType().toUpperCase())) {
                selection += " " + sessionQcFotoPanen.getSectionValue().getText();
            }
        }
        return selection;
    }

    public FilePdf getUrlnFileName(String idSession){
//        String fileUrl = "https://app.gis-div.com/pcsweb/Service/DownloadPDFHandler2.ashx?InspectorName=Afid Prima Maulana&InspectorBy=&kebun=TH3B%20-%20Kebun%20THP%202&estCode=RAM&sessionID=QS0013011211263&TypeDownload=WEB&module=HMS&remarks=&userIds=1872&reportCode=Rpt33&pageId=HMS_LaporanHarianQCFotoPanenRincianInspectorBySession.ascx&reportCategory=Daily&fileRepX=HMS_LaporanHarianQCFotoPanenRincianInspectorBySession&dateParam=11/30/2021&namaFile=211130_RPT33_Laporan%20Harian%20QC%20Foto%20Panen%20Rincian%20Inspector_Afid%20Prima%20Maulana_1638260422000";
//        String fileName = "211130_RPT33_Laporan Harian QC Foto Panen Rincian Inspector_Afid Prima Maulana_1638260422000.pdf";
        String fileUrl = null;
        String fileName = null;
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_FOTO_PANEN);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", idSession));
        for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            SessionQcFotoPanen sessionQcFotoPanen = gson.fromJson(dataNitrit.getValueDataNitrit(), SessionQcFotoPanen.class);
            if(sessionQcFotoPanen.getIdSession().equals(idSession)){
                if(sessionQcFotoPanen.getUrlDownload() != null) {
                    fileUrl = sessionQcFotoPanen.getUrlDownload() + ".pdf";
                    fileName = idSession + " " + sessionQcFotoPanen.getFilePDF();
                }
            }
            break;
        }
        db.close();

        File folder = new File(FULLPATH_REPORT);
        folder.mkdir();

        if(fileUrl != null && fileName != null) {
            File pdfFile = new File(folder, fileName);
            fileUrl = fileUrl.replace("http://","https://");
            return new FilePdf(
                    idSession,
                    fileUrl,
                    fileName,
                    pdfFile
            );
        }else{
            return null;
        }

    }
}
