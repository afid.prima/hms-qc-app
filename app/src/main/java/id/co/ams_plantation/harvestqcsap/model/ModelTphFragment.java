package id.co.ams_plantation.harvestqcsap.model;

/**
 * Created on : 01,Oktober,2020
 * Author     : Afid
 */

public class ModelTphFragment {
    String oph;
    TransaksiPanen transaksiPanen;
    SubPemanen subPemanen;
    int color;

    public ModelTphFragment(String oph, TransaksiPanen transaksiPanen, SubPemanen subPemanen,int color) {
        this.oph = oph;
        this.transaksiPanen = transaksiPanen;
        this.subPemanen = subPemanen;
        this.color = color;
    }

    public String getOph() {
        return oph;
    }

    public void setOph(String oph) {
        this.oph = oph;
    }

    public TransaksiPanen getTransaksiPanen() {
        return transaksiPanen;
    }

    public void setTransaksiPanen(TransaksiPanen transaksiPanen) {
        this.transaksiPanen = transaksiPanen;
    }

    public SubPemanen getSubPemanen() {
        return subPemanen;
    }

    public void setSubPemanen(SubPemanen subPemanen) {
        this.subPemanen = subPemanen;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
