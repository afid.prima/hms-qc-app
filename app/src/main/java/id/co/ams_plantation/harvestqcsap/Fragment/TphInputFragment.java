package id.co.ams_plantation.harvestqcsap.Fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.GnssStatus;
import android.location.Location;
import android.location.LocationListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.util.ArraySet;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.esri.android.map.GraphicsLayer;
import com.esri.android.map.LocationDisplayManager;
import com.esri.android.map.ags.ArcGISLocalTiledLayer;
import com.esri.android.map.event.OnStatusChangedListener;
import com.esri.android.map.ogc.kml.KmlLayer;
import com.esri.core.geometry.GeometryEngine;
import com.esri.core.geometry.Point;
import com.esri.core.geometry.SpatialReference;
import com.esri.core.map.Graphic;
import com.esri.core.symbol.CompositeSymbol;
import com.esri.core.symbol.SimpleLineSymbol;
import com.esri.core.symbol.SimpleMarkerSymbol;
import com.esri.core.symbol.TextSymbol;
import com.evrencoskun.tableview.TableView;
import com.google.gson.Gson;
import com.labo.kaji.fragmentanimations.MoveAnimation;
import com.mikepenz.iconics.utils.Utils;
import com.philliphsu.bottomsheetpickers.BottomSheetPickerDialog;
import com.philliphsu.bottomsheetpickers.time.BottomSheetTimePickerDialog;
import com.philliphsu.bottomsheetpickers.time.numberpad.NumberPadTimePickerDialog;
import com.weiwangcn.betterspinner.library.BetterSpinner;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import co.ceryle.segmentedbutton.SegmentedButtonGroup;
import id.co.ams_plantation.harvestqcsap.SwipeItemRecycleView.SwipeController;
import id.co.ams_plantation.harvestqcsap.SwipeItemRecycleView.SwipeControllerActions;
import id.co.ams_plantation.harvestqcsap.HarvestApp;
import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.adapter.SelectGangAdapter;
import id.co.ams_plantation.harvestqcsap.adapter.SelectPemanenAdapter;
import id.co.ams_plantation.harvestqcsap.adapter.SelectSpkAdapter;
import id.co.ams_plantation.harvestqcsap.adapter.SelectTphAdapter;
import id.co.ams_plantation.harvestqcsap.adapter.SubPemanenAdapter;
import id.co.ams_plantation.harvestqcsap.model.DataNitrit;
import id.co.ams_plantation.harvestqcsap.model.DynamicParameterPenghasilan;
import id.co.ams_plantation.harvestqcsap.model.HasilPanen;
import id.co.ams_plantation.harvestqcsap.model.Kongsi;
import id.co.ams_plantation.harvestqcsap.model.NearestBlock;
import id.co.ams_plantation.harvestqcsap.model.Pemanen;
import id.co.ams_plantation.harvestqcsap.model.QcMutuBuah;
import id.co.ams_plantation.harvestqcsap.model.SPK;
import id.co.ams_plantation.harvestqcsap.model.StampImage;
import id.co.ams_plantation.harvestqcsap.model.SubPemanen;
import id.co.ams_plantation.harvestqcsap.model.TPH;
import id.co.ams_plantation.harvestqcsap.model.TransaksiPanen;
import id.co.ams_plantation.harvestqcsap.service.BluetoothService;
import id.co.ams_plantation.harvestqcsap.ui.BaseActivity;
import id.co.ams_plantation.harvestqcsap.ui.QcMutuBuahActivity;
import id.co.ams_plantation.harvestqcsap.util.CompleteTextViewHelper;
import id.co.ams_plantation.harvestqcsap.util.GlobalHelper;
import id.co.ams_plantation.harvestqcsap.util.ModelRecyclerViewHelper;
import id.co.ams_plantation.harvestqcsap.util.NfcHelper;
import id.co.ams_plantation.harvestqcsap.util.SharedPrefHelper;
import id.co.ams_plantation.harvestqcsap.util.TphHelper;
import id.co.ams_plantation.harvestqcsap.util.TransaksiPanenHelper;
import id.co.ams_plantation.harvestqcsap.util.ValidationUtil;
import id.co.ams_plantation.harvestqcsap.util.WidgetHelper;
import ng.max.slideview.SlideView;
import pl.aprilapps.easyphotopicker.EasyImage;
import ru.slybeaver.slycalendarview.SlyCalendarDialog;

import static org.dizitart.no2.objects.filters.ObjectFilters.and;
import static org.dizitart.no2.objects.filters.ObjectFilters.eq;

/**
 * Created by user on 12/4/2018.
 */

public class TphInputFragment extends Fragment implements
        BottomSheetTimePickerDialog.OnTimeSetListener {

    final int infoNormal = 0;
    final int infoMentah = 1;
    final int infoBusuk = 2;
    final int infoLewatMatang = 3;
    final int infoAbnormal = 4;
    final int infoTangkaiPanjang = 5;
    final int infoTotalJanjang= 6;
    final int infoTotalJanjangPendapatan= 7;
    final int infoBuahDimakanTikus= 8;

    final int LongOperation_Setup_Data_TPH = 11;
    final int LongOperation_Save_Data_Qc = 12;
    final int LongOperation_Delet_Data_Qc = 13;
//    public final int LongOperation_Save_Panen_T2_NFC = 14;
//    final int LongOperation_Save_Panen_T2_Print = 15;
//    final int LongOperation_Save_Panen_T2_Share = 16;
//    final int LongOperation_Save_Panen_T2_MANUAL = 14;
    final int LongOperation_Setup_Point = 15;
    final int LongOperation_Setup_SPK = 16;
    final int LongOperation_UpdateLocation_Setup_Point = 17;

    final int YesNo_Batal = 14;
    final int YesNo_SimpanQc = 15;
    final int YesNo_HapusQc = 16;
    final int YesNo_UpdateQc = 17;
    final int YesNo_Pilih_TPH = 19;

    public double latitude;
    public double longitude;
    int totalJjgJelek;
    int totalJjg;
    int totalJjgPendapatan;

    SegmentedButtonGroup tipeKongsi;
    LinearLayout lSisa;
    RecyclerView rvPemanen;
    TextView tvSisa;
    TextView tvSisa2;
    TextView tvSisa3;
    CompleteTextViewHelper etPemanenSisa;
    CompleteTextViewHelper etSpk;
    SubPemanenAdapter subPemanenAdapter;
    SwipeController swipeController = null;

    AppCompatEditText etSearch;
    Button btnSearch;
    TableView tableview;

//    RecyclerView rv;
//    LinearLayout lnfc;
//    LinearLayout lprint;
//    LinearLayout lshare;
    LinearLayout lldeleted;
//    LinearLayout newTph;
    LinearLayout historyTransaksi;
    LinearLayout lsave;
    LinearLayout lcancel;
    LinearLayout lWarning;
    LinearLayout llkeyBoard;
    LinearLayout ketAction1;
    LinearLayout ketAction2;
    LinearLayout ketAction3;

    View tphphoto;
    View tphKrani;
    ImageView ivPhoto;
    ImageView imagecacnel;
    ImageView imagesview;
    SliderLayout sliderLayout;
    RelativeLayout rl_ipf_takepicture;

    LinearLayout lInfoTph;
    LinearLayout lInfoSubPemanen;
    View pemanenInfo;
    LinearLayout lTph;
    LinearLayout lBlock;
    LinearLayout lAncak;
    LinearLayout lTglPanen;
    LinearLayout lTphBayangan;
    LinearLayout lStatus;
    LinearLayout lNoKartu;
    LinearLayout lKraniInfo;
    CompleteTextViewHelper etNotph;
    CompleteTextViewHelper etBlock;
    AppCompatEditText etAncak;
    AppCompatEditText etTglPanen;
////    AppCompatEditText etBaris;
//    CompleteTextViewHelper etGang;
//    CompleteTextViewHelper etPemanen;
    CompleteTextViewHelper etSubstitute;
    CheckBox cbTphBayangan;
    CheckBox cbBorongan;
    CheckBox cbKraniSubstitute;

    TextView tvInfoTph;
    TextView tvTph;

    LinearLayout lBorongan;
    LinearLayout lNormal;
    LinearLayout lMentah;
    LinearLayout lBusuk;
    LinearLayout lLewatMatang;
    LinearLayout lAbnormal;
    LinearLayout lTangkaiPanjang;
    LinearLayout lBrondolan;
    LinearLayout lBuahDimakanTikus;
    LinearLayout lTotalJanjangBuruk;
    LinearLayout lTotalJanjang;
    LinearLayout lTotalJanjangPendapatan;
    LinearLayout lJarak;
    LinearLayout lAlasBrondol;
    LinearLayout lInfoPemanen;
    View vTphPanen;
    View vTphphoto;
    LinearLayout lSubstitute;
    TextView tvNormal;
    TextView tvNormalSymbol;
    TextView tvMeasureNormal;
    TextView tvMentah;
    TextView tvMentahSymbol;
    TextView tvMeasureMentah;
    AppCompatEditText etNormal;
    AppCompatEditText etMentah;
    AppCompatEditText etBusuk;
    AppCompatEditText etLewatMatang;
    AppCompatEditText etAbnormal;
    AppCompatEditText etBuahDimakanTikus;
    AppCompatEditText etBrondolan;
    AppCompatEditText etTangkaiPanjang;
    AppCompatEditText etJarak;
    AppCompatEditText etTotalJanjangBuruk;
    AppCompatEditText etTotalJanjang;
    AppCompatEditText etTotalJanjangPendapatan;
    AppCompatEditText etNoNFC;
//    ImageView ivTph;
    ImageView ivNormal;
    ImageView ivTotal;
    ImageView ivTotalPendapatan;
    ImageView ivMentah;
    ImageView ivBusuk;
    ImageView ivLewatMatang;
    ImageView ivAbnormal;
    ImageView ivTangkaiPanjang;
    ImageView ivBuahDimakanTikus;
    BetterSpinner lsAlasBrondol;
    BetterSpinner lsStatus;

    LinearLayout infoAngkut;
    TextView tvTotalAngkut;
    TextView tvTotalBrondolan;
    TextView tvTotalJanjang;
    TextView tvTotalBeratEstimasi;

    LocationDisplayManager locationDisplayManager;
    ArcGISLocalTiledLayer tiledLayer;
    KmlLayer kmlLayer;
    SpatialReference spatialReference;
    GraphicsLayer graphicsLayer;
    GraphicsLayer graphicsLayerTPH;
    GraphicsLayer graphicsLayerLangsiran;
    GraphicsLayer graphicsLayerText;
    String locKmlMap;
    boolean isMapLoaded;

    public Pemanen pemanenSelected;
    Pemanen pemanenSisa;
    HashMap<String,Pemanen> pemanenOperator;
    HashMap<String,Pemanen> pemanenOperatorTonase;
    String blockSelected;
    TransaksiPanen selectedTransaksiPanen;
    String dataTransaksiNFC;
    TPH tphSelected;
    BaseActivity baseActivity;
    QcMutuBuahActivity qcMutuBuahActivity;

//    PemanenHelper pemanenHelper;
    public TphHelper tphHelper;
    TransaksiPanenHelper transaksiPanenHelper;
    SharedPrefHelper sharedPrefHelper;
    ModelRecyclerViewHelper modelRecyclerViewHelper;

    ArrayList<File> ALselectedImage;
    Long waktuPanen;
    SimpleDateFormat sdfT = new SimpleDateFormat("HH:mm");
    AlertDialog alertDialog;
    File kml;

    Boolean popupTPH = false;
    SelectGangAdapter adapterGang;
    SelectPemanenAdapter adapterPemanen;

    ArrayList<TPH> allTph;
    SelectTphAdapter adapterTph;
    TPH tphTerdekat;

    ArrayList<TPH> allTPH;
    TPH selectedTPH;
    TPH TPHTerdekat;
    DynamicParameterPenghasilan dynamicParameterPenghasilan;
    boolean checkBoxTphBayanganEfect;
    double bjr;
    TransaksiPanen idPanenAngkut;
    ArrayList<SubPemanen> subPemanens;
    HashMap<String,Pemanen> pemanens;

    SPK spkSelected;
    HashMap<String,SPK> allSpk;
    SelectSpkAdapter adapterSpk;

    StampImage stampImage;
    private String blockCharacterSet = "~#^|$%&*!";
    private static String TAG = TphInputFragment.class.getSimpleName();
    private int isManual = 0;
    private String blockName;
    int idKongsi = 0;
    HasilPanen sisaJjgBrdl;
    Set<String> idNfc;
    boolean cariTphTerdekat = true;
    boolean hidePemanen = false;
    String idQcMutubuahSelected = null;
    SimpleDateFormat formatDate = new SimpleDateFormat("dd MMMM yyyy");

    public static TphInputFragment getInstance() {
        Log.i(TAG, "getInstance: no bundle");
        TphInputFragment tphInputFragment = new TphInputFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("isManual", 0);
        tphInputFragment.setArguments(bundle);
        return tphInputFragment;
    }

    /*
     * Existing param isManual
     * 1 = manual
     */
    public static TphInputFragment getInstance(int isManual) {
        Log.i(TAG, "getInstance: WITH bundle");
        Bundle bundle = new Bundle();
        bundle.putInt("isManual", isManual);
        TphInputFragment tphInputFragment = new TphInputFragment();
        tphInputFragment.setArguments(bundle);
        return tphInputFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments()!=null){
            if(getArguments().getInt("isManual") != 0){
                isManual = getArguments().getInt("isManual");
            }
        }
        Log.i(TAG, "arguments: "+ getArguments().getInt("isManual")+ " isManual : "+ isManual);
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return MoveAnimation.create(MoveAnimation.UP,enter,600);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tph_input, null, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        waktuPanen = System.currentTimeMillis() - (2*60*60*1000);
        totalJjgJelek = 0;
        totalJjg = 0;
        bjr = 0.0;
        cariTphTerdekat = true;

//        pemanenHelper = new PemanenHelper(getActivity());
        tphHelper = new TphHelper(getActivity());
        transaksiPanenHelper = new TransaksiPanenHelper(getActivity());
        sharedPrefHelper = new SharedPrefHelper(getActivity());
        modelRecyclerViewHelper = new ModelRecyclerViewHelper(getActivity());

        baseActivity = ((BaseActivity) getActivity());
        if (getActivity() instanceof QcMutuBuahActivity){
            qcMutuBuahActivity = ((QcMutuBuahActivity) getActivity());
            if(qcMutuBuahActivity.qcMutuBuah != null) {
                if(qcMutuBuahActivity.qcMutuBuah.getTransaksiPanenKrani() != null){
                    if (qcMutuBuahActivity.qcMutuBuah.getTransaksiPanenKrani().getIdTPanen() != null) {
                        selectedTransaksiPanen = qcMutuBuahActivity.qcMutuBuah.getTransaksiPanenKrani();
                        if (qcMutuBuahActivity.qcMutuBuah.getTransaksiPanenQc() != null) {
                            selectedTransaksiPanen = qcMutuBuahActivity.qcMutuBuah.getTransaksiPanenQc();
                        }
                        if (selectedTransaksiPanen.getTph().getNamaTph() == null) {
                            Toast.makeText(HarvestApp.getContext(),"Anda Tidak Punya Master TPH Ini",Toast.LENGTH_SHORT).show();
                            qcMutuBuahActivity.backProses();
//                            qcMutuBuahActivity.searchingGPS = Snackbar.make(qcMutuBuahActivity.myCoordinatorLayout, "Anda Tidak Punya Master TPH Ini", Snackbar.LENGTH_INDEFINITE);
//                            qcMutuBuahActivity.searchingGPS.show();
                            return;
                        }
                    } else if (qcMutuBuahActivity.qcMutuBuah.getTransaksiPanenQc() != null){
                        idQcMutubuahSelected = qcMutuBuahActivity.qcMutuBuah.getIdQCMutuBuah();
                        selectedTransaksiPanen = qcMutuBuahActivity.qcMutuBuah.getTransaksiPanenQc();
                        hidePemanen = true;
                    }
                }else if (qcMutuBuahActivity.qcMutuBuah.getTransaksiPanenQc() != null){
                    idQcMutubuahSelected = qcMutuBuahActivity.qcMutuBuah.getIdQCMutuBuah();
                    selectedTransaksiPanen = qcMutuBuahActivity.qcMutuBuah.getTransaksiPanenQc();
                    hidePemanen = true;
                }else{
                    hidePemanen = true;
                }
            }else{
                hidePemanen = true;
            }
        }

        tphKrani = (View) view.findViewById(R.id.tphKrani);
        tphphoto = (View) view.findViewById(R.id.tphphoto);
        ivPhoto = (ImageView) view.findViewById(R.id.ivPhoto);
        imagecacnel = (ImageView) view.findViewById(R.id.imagecacnel);
        imagesview = (ImageView) view.findViewById(R.id.imagesview);
        rl_ipf_takepicture = (RelativeLayout) view.findViewById(R.id.rl_ipf_takepicture);
        sliderLayout = (SliderLayout) view.findViewById(R.id.sliderLayout);

        lBorongan = (LinearLayout) view.findViewById(R.id.lBorongan);
        lKraniInfo = (LinearLayout) view.findViewById(R.id.lKraniInfo);
        lInfoTph = (LinearLayout) view.findViewById(R.id.lInfoTph);
        lInfoSubPemanen = (LinearLayout) view.findViewById(R.id.lInfoSubPemanen);
        pemanenInfo = (View) view.findViewById(R.id.pemanenInfo);
//        lAngkut = (LinearLayout) view.findViewById(R.id.lAngkut);
//        angkutInfo = (View) view.findViewById(R.id.angkutInfo);
        lTph = (LinearLayout) view.findViewById(R.id.lTph);
        lBlock = (LinearLayout) view.findViewById(R.id.lBlock);
        lAncak = (LinearLayout) view.findViewById(R.id.lAncak);
        lTglPanen = (LinearLayout) view.findViewById(R.id.lTglPanen);
        lTphBayangan = (LinearLayout) view.findViewById(R.id.lTphBayangan);
        lStatus = (LinearLayout) view.findViewById(R.id.lStatus);
        lNoKartu = (LinearLayout) view.findViewById(R.id.lNoKartu);
        etNotph = (CompleteTextViewHelper) view.findViewById(R.id.etNotph);
        etBlock = (CompleteTextViewHelper) view.findViewById(R.id.etBlock);
        etAncak = (AppCompatEditText) view.findViewById(R.id.etAncak);
        etTglPanen = (AppCompatEditText) view.findViewById(R.id.etTglPanen);
        cbTphBayangan = (CheckBox) view.findViewById(R.id.cbTphBayangan);
        cbKraniSubstitute = (CheckBox) view.findViewById(R.id.cb_krani_substitute);
//        etBaris = (AppCompatEditText) view.findViewById(R.id.etBaris);
//        etGang = (CompleteTextViewHelper) view.findViewById(R.id.etGang);
//        etPemanen = (CompleteTextViewHelper) view.findViewById(R.id.etPemanen);InsertSPB

        cbBorongan = (CheckBox) view.findViewById(R.id.cb_borongan);
        etSpk = (CompleteTextViewHelper) view.findViewById(R.id.etSpk);

        tipeKongsi = (SegmentedButtonGroup) view.findViewById(R.id.tipeKongsi);
        lSisa = (LinearLayout) view.findViewById(R.id.lSisa);
        rvPemanen = (RecyclerView) view.findViewById(R.id.rvPemanen);
        tvSisa = (TextView) view.findViewById(R.id.tvSisa);
        tvSisa2 = (TextView) view.findViewById(R.id.tvSisa2);
        tvSisa3 = (TextView) view.findViewById(R.id.tvSisa3);
        etPemanenSisa = (CompleteTextViewHelper) view.findViewById(R.id.etPemanenSisa);

        etSearch = (AppCompatEditText) view.findViewById(R.id.etSearch);
        btnSearch = (Button) view.findViewById(R.id.btnSearch);
        tableview = (TableView) view.findViewById(R.id.tableview);
        tvInfoTph = (TextView) view.findViewById(R.id.tvInfoTph);
        tvTph = (TextView) view.findViewById(R.id.tvTph);

        lNormal = (LinearLayout) view.findViewById(R.id.lNormal);
        lMentah = (LinearLayout) view.findViewById(R.id.lMentah);
        lBusuk = (LinearLayout) view.findViewById(R.id.lBusuk);
        lLewatMatang = (LinearLayout) view.findViewById(R.id.lLewatMatang);
        lAbnormal = (LinearLayout) view.findViewById(R.id.lAbnormal);
        lTangkaiPanjang = (LinearLayout) view.findViewById(R.id.lTangkaiPanjang);
        lBuahDimakanTikus = (LinearLayout) view.findViewById(R.id.lBuahDimakanTikus);
        lBrondolan = (LinearLayout) view.findViewById(R.id.lBrondolan);
        lTotalJanjangBuruk = (LinearLayout) view.findViewById(R.id.lTotalJanjangBuruk);
        lTotalJanjang = (LinearLayout) view.findViewById(R.id.lTotalJanjang);
        lTotalJanjangPendapatan = (LinearLayout) view.findViewById(R.id.lTotalJanjangPendapatan);
        lJarak = (LinearLayout) view.findViewById(R.id.lJarak);
        lAlasBrondol = (LinearLayout) view.findViewById(R.id.lAlasBrondol);
        lSubstitute = (LinearLayout) view.findViewById(R.id.lSubstitute);
//        LinearLayout lInfoPemanen;
//        LinearLayout lTphPanen;
        lInfoPemanen = (LinearLayout) view.findViewById(R.id.lInfoPemanen) ;
        vTphPanen = (View) view.findViewById(R.id.tphPanen);
        vTphphoto = (View) view.findViewById(R.id.tphphoto);
        tvNormal = (TextView) view.findViewById(R.id.tvNormal);
        tvNormalSymbol = (TextView) view.findViewById(R.id.tvNormalSymbol);
        tvMeasureNormal = (TextView) view.findViewById(R.id.tvMeasureNormal);
        tvMentah = (TextView) view.findViewById(R.id.tvMentah);
        tvMentahSymbol = (TextView) view.findViewById(R.id.tvMentahSymbol);
        tvMeasureMentah = (TextView) view.findViewById(R.id.tvMeasureMentah);
        etNormal = (AppCompatEditText) view.findViewById(R.id.etNormal);
        etMentah = (AppCompatEditText) view.findViewById(R.id.etMentah);
        etBusuk = (AppCompatEditText) view.findViewById(R.id.etBusuk);
        etLewatMatang = (AppCompatEditText) view.findViewById(R.id.etLewatMatang);
        etBuahDimakanTikus = (AppCompatEditText) view.findViewById(R.id.etBuahDimakanTikus);
        etAbnormal = (AppCompatEditText) view.findViewById(R.id.etAbnormal);
        etBrondolan = (AppCompatEditText) view.findViewById(R.id.etBrondolan);
        etTangkaiPanjang = (AppCompatEditText) view.findViewById(R.id.etTangkaiPanjang);
        etTotalJanjangBuruk = (AppCompatEditText) view.findViewById(R.id.etTotalJanjangBuruk);
        etTotalJanjang = (AppCompatEditText) view.findViewById(R.id.etTotalJanjang);
        etTotalJanjangPendapatan = (AppCompatEditText) view.findViewById(R.id.etTotalJanjangPendapatan);
        etNoNFC = (AppCompatEditText) view.findViewById(R.id.etNoNFC);
        etJarak = (AppCompatEditText) view.findViewById(R.id.etJarak);
        ivTotal = (ImageView) view.findViewById(R.id.ivTotal);
        ivTotalPendapatan = (ImageView) view.findViewById(R.id.ivTotalPendapatan);
        ivNormal = (ImageView) view.findViewById(R.id.ivNormal);
//        ivTph = (ImageView) view.findViewById(R.id.ivTph);
        ivMentah = (ImageView) view.findViewById(R.id.ivMentah);
        ivBusuk = (ImageView) view.findViewById(R.id.ivBusuk);
        ivLewatMatang = (ImageView) view.findViewById(R.id.ivLewatMatang);
        ivAbnormal = (ImageView) view.findViewById(R.id.ivAbnormal);
        ivBuahDimakanTikus = (ImageView) view.findViewById(R.id.ivBuahDimakanTikus);
        ivTangkaiPanjang = (ImageView) view.findViewById(R.id.ivTangkaiPanjang);
        lsAlasBrondol = (BetterSpinner) view.findViewById(R.id.lsAlasBrondol);
        etSubstitute = (CompleteTextViewHelper) view.findViewById(R.id.etSubstitute);
        lsStatus = (BetterSpinner) view.findViewById(R.id.lsStatusSpin);

        lWarning = (LinearLayout) view.findViewById(R.id.lWarning);
        llkeyBoard = (LinearLayout) view.findViewById(R.id.llkeyBoard);
        ketAction1 = (LinearLayout) view.findViewById(R.id.ketAction1);
        ketAction2 = (LinearLayout) view.findViewById(R.id.ketAction2);
        ketAction3 = (LinearLayout) view.findViewById(R.id.ketAction3);

        infoAngkut = (LinearLayout) view.findViewById(R.id.infoAngkut);
        tvTotalAngkut = (TextView) view.findViewById(R.id.tvTotalAngkut);
        tvTotalJanjang = (TextView) view.findViewById(R.id.tvTotalJanjang);
        tvTotalBrondolan = (TextView) view.findViewById(R.id.tvTotalBrondolan);
        tvTotalBeratEstimasi = (TextView) view.findViewById(R.id.tvTotalBeratEstimasi);
//        lnfc = (LinearLayout) view.findViewById(R.id.lnfc);
//        lprint = (LinearLayout) view.findViewById(R.id.lprint);
//        lshare = (LinearLayout) view.findViewById(R.id.lshare);
        historyTransaksi = (LinearLayout) view.findViewById(R.id.historyTransaksi);
        lsave = (LinearLayout) view.findViewById(R.id.lsave);
        lldeleted = (LinearLayout) view.findViewById(R.id.lldeleted);
        lcancel = (LinearLayout) view.findViewById(R.id.lcancel);
//        newTph = (LinearLayout) view.findViewById(R.id.newTph);

        lAncak.setVisibility(View.GONE);
        historyTransaksi.setVisibility(View.GONE);
        ketAction3.setVisibility(View.GONE);
        checkBoxTphBayanganEfect = true;
        infoAngkut.setVisibility(View.GONE);

        main();
    }

    private void main(){
        lJarak.setVisibility(View.GONE);
        lTotalJanjangBuruk.setVisibility(View.GONE);
        lWarning.setVisibility(View.GONE);
        llkeyBoard.setVisibility(View.GONE);
        lldeleted.setVisibility(View.GONE);
        lBorongan.setVisibility(View.GONE);
//        etNotph.setFocusable(false);
//        etBlock.setFocusable(false);
//        etGang.setFocusable(false);
//        etPemanen.setFocusable(false);
        etJarak.setFocusable(false);
        etTotalJanjangBuruk.setFocusable(false);
        etTotalJanjangPendapatan.setFocusable(false);

        ALselectedImage = new ArrayList<>();
        List<String> bAlas = new ArrayList<>();
        bAlas.add("Yes");
        bAlas.add("No");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, bAlas);
        lsAlasBrondol.setAdapter(adapter);
        lsAlasBrondol.setText(bAlas.get(0));
        //etJarak.setText(sdfT.format(waktuPanen));

        kml = new File(GlobalHelper.decryptFiles(GlobalHelper.TYPE_KMLBLOCK));
        HashMap<String,DynamicParameterPenghasilan> appConfig = GlobalHelper.getAppConfig();
        dynamicParameterPenghasilan = appConfig.get(GlobalHelper.getEstate().getEstCode());

        subPemanens = new ArrayList<>();

        if(qcMutuBuahActivity != null){
            mainQcMutuBuah();
            mapSetup();
            etNormal.setFocusable(false);
        }

        if(selectedTransaksiPanen != null){
            setUpValueTransaksiPanen();
            if(!hidePemanen) {
                subPemanens = new ArrayList<>();
                if (selectedTransaksiPanen.getKongsi().getSubPemanens() != null) {
                    subPemanens = selectedTransaksiPanen.getKongsi().getSubPemanens();
                } else if (pemanenSelected != null) {
                    subPemanens.add(new SubPemanen(
                            0, pemanenSelected, 100, new HasilPanen()
                    ));
                }
            }else{
                subPemanens.add(new SubPemanen(
                        0, new Pemanen(),100,new HasilPanen()
                ));

                if(sharedPrefHelper.getName(SharedPrefHelper.KEY_TipeKongsi) != null) {
                    idKongsi = Integer.parseInt(sharedPrefHelper.getName(SharedPrefHelper.KEY_TipeKongsi));
                }
                tipeKongsi.setPosition(idKongsi);

                lSisa.setVisibility(View.GONE);
            }
        }else{
            subPemanens.add(new SubPemanen(
                    0, new Pemanen(),100,new HasilPanen()
            ));

            if(sharedPrefHelper.getName(SharedPrefHelper.KEY_TipeKongsi) != null) {
                idKongsi = Integer.parseInt(sharedPrefHelper.getName(SharedPrefHelper.KEY_TipeKongsi));
            }
            tipeKongsi.setPosition(idKongsi);

            lSisa.setVisibility(View.GONE);
        }

        subPemanenAdapter = new SubPemanenAdapter(getActivity(),subPemanens);
        rvPemanen.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rvPemanen.setAdapter(subPemanenAdapter);

        swipeController = new SwipeController(new SwipeControllerActions() {
            @Override
            public void onRightClicked(int position) {
                Toast.makeText(HarvestApp.getContext(),"Hanya Bisa Diubah Pada Menu Panen",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLeftClicked(int position) {
                Toast.makeText(HarvestApp.getContext(),"Hanya Bisa Diubah Pada Menu Panen",Toast.LENGTH_SHORT).show();
            }
        },subPemanenAdapter);

        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
        itemTouchhelper.attachToRecyclerView(rvPemanen);

        tipeKongsi.setOnClickedButtonListener(new SegmentedButtonGroup.OnClickedButtonListener() {
            @Override
            public void onClickedButton(int position) {
                tipeKongsi.setPosition(idKongsi);
                Toast.makeText(HarvestApp.getContext(),"Hanya Bisa Diubah Pada Menu Panen",Toast.LENGTH_SHORT).show();
            }
        });

        etPemanenSisa.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                pemanenSisa = adapterPemanen.getItemAt(position);
                etPemanenSisa.setText(pemanenSisa.getNama());
            }
        });

        etPemanenSisa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etPemanenSisa.showDropDown();
            }
        });

        cbBorongan.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    new LongOperation(getView()).execute(String.valueOf(LongOperation_Setup_SPK));
                }else{
                    lBorongan.setVisibility(View.GONE);
                    etSpk.setText("");
                    spkSelected = null;
                }
            }
        });

        etSpk.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                spkSelected = ((SelectSpkAdapter) parent.getAdapter()).getItemAt(position);
                etSpk.setText(spkSelected.getIdSPK());
            }
        });

        etSpk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etSpk.showDropDown();
            }
        });

        ivPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (baseActivity.currentLocationOK()) {
                    if(ALselectedImage.size() > 3) {
                        Toast.makeText(HarvestApp.getContext(),getActivity().getResources().getString(R.string.warning_take_foto),Toast.LENGTH_LONG).show();
                    }else {
                        takePicture();
                    }
                } else {
                    if(qcMutuBuahActivity != null) {
                        qcMutuBuahActivity.searchingGPS = Snackbar.make(qcMutuBuahActivity.myCoordinatorLayout, getResources().getString(R.string.search_gps), Snackbar.LENGTH_INDEFINITE);
                        qcMutuBuahActivity.searchingGPS.show();
                        WidgetHelper.warningFindGps(qcMutuBuahActivity,qcMutuBuahActivity.myCoordinatorLayout);
                    }
                }

            }
        });

//        ivTph.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (etBlock.getText().toString().isEmpty()) {
//                    Toast.makeText(HarvestApp.getContext(),getActivity().getResources().getString(R.string.please_chose_block),Toast.LENGTH_SHORT).show();
//                    etBlock.showDropDown();
//                } else {
//                    tphHelper.choseTph(etBlock.getText().toString());
//                }
//            }
//        });
        ivTotal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                infoPanenPopUp(infoTotalJanjang);
            }
        });
        ivTotalPendapatan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                infoPanenPopUp(infoTotalJanjangPendapatan);
            }
        });
        ivNormal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                infoPanenPopUp(infoNormal);
            }
        });
        ivMentah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                infoPanenPopUp(infoMentah);
            }
        });
        ivBusuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                infoPanenPopUp(infoBusuk);
            }
        });
        ivLewatMatang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                infoPanenPopUp(infoLewatMatang);
            }
        });
        ivAbnormal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                infoPanenPopUp(infoAbnormal);
            }
        });
        ivTangkaiPanjang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                infoPanenPopUp(infoTangkaiPanjang);
            }
        });
        ivBuahDimakanTikus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                infoPanenPopUp(infoBuahDimakanTikus);
            }
        });

        etTglPanen.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openDateRangePicker();
            }
        });

//        etNormal.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//                setTotalJjg(true);
//                setTotalJjgPendapatan(true);
//            }
//        });
        etMentah.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable s) {
//                if(tphActivity != null) {
                    setTotalJjgJelek(false);
                    setJjgNormal(true);
                    setTotalJjgPendapatan(true);
                    distribusiJanjangSubPemanen(false);
//                }else if(angkutActivity != null) {
//                    if (etMentah.hasFocus()) {
//                        Log.d("etMentah", s.toString());
//                        if (transaksiAngkutNFC != null) {
//                            getBjrAngkut("etMentah");
//                        } else {
//                            int nilai = (int) (Integer.parseInt(s.toString().isEmpty() ? "0" : s.toString()) / bjr);
//                            etNormal.setText(String.valueOf(nilai));
//                        }
//                    }
//                }
            }
        });

        etTotalJanjang.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setJjgNormal(true);
                setTotalJjgPendapatan(true);
                distribusiJanjangSubPemanen(false);
            }
        });

        etLewatMatang.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setTotalJjgJelek(false);
                setJjgNormal(true);
                setTotalJjgPendapatan(true);
                distribusiJanjangSubPemanen(false);
            }
        });

        etBusuk.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setTotalJjgJelek(false);
                setJjgNormal(true);
                setTotalJjgPendapatan(true);
                distribusiJanjangSubPemanen(false);
            }
        });

        etTangkaiPanjang.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setTotalJjgJelek(false);
                setJjgNormal(true);
                setTotalJjgPendapatan(true);
                distribusiJanjangSubPemanen(false);
            }
        });

        etAbnormal.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setTotalJjgJelek(false);
                setJjgNormal(true);
                setTotalJjgPendapatan(true);
                distribusiJanjangSubPemanen(false);
            }
        });

        etBuahDimakanTikus.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setTotalJjgJelek(false);
                setJjgNormal(true);
                setTotalJjgPendapatan(true);
                distribusiJanjangSubPemanen(false);
            }
        });

        etBrondolan.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
//                if(angkutActivity != null) {
//                    if (etBrondolan.hasFocus()) {
//                        Log.d("etBrondolan", s.toString());
//                        if (transaksiAngkutNFC != null) {
//                            getBjrAngkut("etBrondolan");
//                        }else if(selectedTransaksiPanen != null) {
//                            distribusiJanjangSubPemanen(false);
//                        }
//                    }
//                }else
                if (qcMutuBuahActivity != null){
                    distribusiJanjangSubPemanen(false);
                }
            }
        });

        etNoNFC.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length() == 8){
                    if(ValidationUtil.isValidNfc(editable.toString().replaceAll("[^A-Za-z0-9]","").toUpperCase())){
                        etNoNFC.setTextColor(HarvestApp.getContext().getResources().getColor(R.color.Green));
//                        Toast.makeText(getActivity(), "No kartu benar!", Toast.LENGTH_SHORT).show();
                    }else{
                        etNoNFC.setTextColor(HarvestApp.getContext().getResources().getColor(R.color.red_color));
                        Toast.makeText(getActivity(), "Format no kartu salah, mohon periksa kembali", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

//        lnfc.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(selectedTransaksiPanen == null){
//                    new LongOperation(view).execute(String.valueOf(LongOperation_NFC_With_Save));
//                }else {
//                    if (selectedTransaksiPanen.getStatus() == TransaksiPanen.SAVE_ONLY) {
//                        new LongOperation(view).execute(String.valueOf(LongOperation_NFC_With_Save));
//                    } else {
//                        new LongOperation(view).execute(String.valueOf(LongOperation_NFC_Only));
//                    }
//                }
//            }
//        });
//
//        lprint.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (baseActivity.bluetoothHelper.mService.getState() == BluetoothService.STATE_CONNECTED) {
//                    if (selectedTransaksiPanen == null) {
//                        new LongOperation(view).execute(String.valueOf(LongOperation_Print_With_Save));
//                    } else {
//                        if (selectedTransaksiPanen.getStatus() == TransaksiPanen.SAVE_ONLY) {
//                            new LongOperation(view).execute(String.valueOf(LongOperation_Print_With_Save));
//                            new LongOperation(view).execute(String.valueOf(LongOperation_Print_Only));
//                        } else {
//                            new LongOperation(view).execute(String.valueOf(LongOperation_Print_Only));
//                        }
//                    }
//                } else {
//                    transaksiPanenHelper.printBluetooth(selectedTransaksiPanen);
//                }
//            }
//        });
//
//        lshare.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(selectedTransaksiPanen == null) {
//                    new LongOperation(view).execute(String.valueOf(LongOperation_Share_With_Save));
//                }else {
//                    if (selectedTransaksiPanen.getStatus() == TransaksiPanen.SAVE_ONLY) {
//                        new LongOperation(view).execute(String.valueOf(LongOperation_Share_With_Save));
//                    } else {
//                        new LongOperation(view).execute(String.valueOf(LongOperation_Share_Only));
//                    }
//                }
//            }
//        });
//        newTph.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(tphActivity.currentLocationOK()) {
//                    tphActivity.getSupportFragmentManager().beginTransaction()
//                            .replace(R.id.content_container, TphNewInputFragment.getInstance())
//                            .commit();
//                }else{
//                    tphActivity.searchingGPS = Snackbar.make(tphActivity.myCoordinatorLayout,getResources().getString(R.string.search_gps),Snackbar.LENGTH_INDEFINITE);
//                    tphActivity.searchingGPS.show();
//                }
//            }
//        });

        lsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!GlobalHelper.isDoubleClick()) {
                    SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.STAMP_IMAGES, Context.MODE_PRIVATE);
                    String stringIsi = preferences.getString(HarvestApp.STAMP_IMAGES, null);
                    Gson gson = new Gson();
                    StampImage stampImageX = gson.fromJson(stringIsi, StampImage.class);

                    if (ALselectedImage.size() == 0) {
                        takePicture();
                        Toast.makeText(HarvestApp.getContext(), getActivity().getResources().getString(R.string.please_take_picture), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (etTglPanen.getText().toString().equals("")) {
                        Toast.makeText(HarvestApp.getContext(), "Mohon Input Tgl Panen Dahulu", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (qcMutuBuahActivity != null) {
//                    Log.i(TAG, "qcMutuBuahActivity not null");
                        showYesNo("Apakah Anda Yakin Untuk Simpan Data Ini", YesNo_SimpanQc);
                    }
                }
            }
        });

        lldeleted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        lcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!GlobalHelper.isDoubleClick()) {
                    showYesNo("Apakah Anda Yakin Ingin Keluar Form", YesNo_Batal);
                }
            }
        });

        llkeyBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GlobalHelper.showKeyboard(getActivity());
            }
        });

        lsStatus.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(lsStatus.getAdapter().getItem(i).equals("Kartu Rusak")){
                    lNoKartu.setVisibility(View.VISIBLE);
                }else if(lsStatus.getAdapter().getItem(i).equals("Kartu Hilang")){
                    lNoKartu.setVisibility(View.GONE);
                }else{
                    lNoKartu.setVisibility(View.GONE);
                }
            }
        });
        lsStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.i("lsStatusOnItemSelected", "onItemSelected: "+lsStatus.getText().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Log.i("lsStatusOnItemSelected", "onItemSelected: "+lsStatus.getText().toString());
            }
        });

        historyTransaksi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //transaksiAngkutHelper.showHistoryTransaksi(selectedTransaksiAngkut);
            }
        });
    }

    private void mapSetup(){ 

        graphicsLayer = new GraphicsLayer();
        graphicsLayerTPH = new GraphicsLayer();
        graphicsLayerLangsiran = new GraphicsLayer();
        graphicsLayerText = new GraphicsLayer();

        qcMutuBuahActivity.rlMap.setVisibility(View.VISIBLE);

        baseActivity.mapView.removeAll();
        graphicsLayer.removeAll();
        baseActivity.mapView.setMinScale(250000.0d);
        baseActivity.mapView.setMaxScale(1000.0d);
        baseActivity.mapView.enableWrapAround(true);

        String locTiledMap = GlobalHelper.decryptFiles(GlobalHelper.TYPE_VKM);
        if(locTiledMap != null){
            File file = new File(locTiledMap);
            if(!file.exists()){
                locTiledMap = null;
            }
        }

        if(locTiledMap != null){

            locKmlMap = GlobalHelper.decryptFiles(GlobalHelper.TYPE_KMLBLOCK);
            if (locKmlMap != null) {
                kmlLayer = new KmlLayer(locKmlMap);
//                kmlLayer.setOpacity(0.03f);
                baseActivity.mapView.addLayer(kmlLayer);
                kmlLayer.setVisible(false);
            }
            tiledLayer = new ArcGISLocalTiledLayer(locTiledMap, true);
            baseActivity.mapView.addLayer(tiledLayer);
        }else{
            locTiledMap = GlobalHelper.getFilePath(GlobalHelper.TYPE_VKM, GlobalHelper.getEstate());
            if (locTiledMap != null) {
                locTiledMap = GlobalHelper.decryptFiles(GlobalHelper.TYPE_VKM);
                tiledLayer = new ArcGISLocalTiledLayer(locTiledMap, true);
                baseActivity.mapView.addLayer(tiledLayer);
            }

            locKmlMap = GlobalHelper.decryptFiles(GlobalHelper.TYPE_KMLBLOCK);
            if(locKmlMap!=null){
                kmlLayer = new KmlLayer(locKmlMap);
//                kmlLayer.setOpacity(0.8f);
                baseActivity.mapView.addLayer(kmlLayer);
                kmlLayer.setVisible(true);
            }
        }

        baseActivity.mapView.setOnStatusChangedListener(new OnStatusChangedListener() {
            @Override
            public void onStatusChanged(Object o, STATUS status) {
                if(o==baseActivity.mapView && status==STATUS.INITIALIZED){
                    spatialReference = baseActivity.mapView.getSpatialReference();
                    isMapLoaded = true;
                    locationListenerSetup();
                    new LongOperation(getView()).execute(String.valueOf(LongOperation_Setup_Point));
                }
            }
        });

        baseActivity.zoomToLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new LongOperation(getView()).execute(String.valueOf(LongOperation_UpdateLocation_Setup_Point));
            }
        });

        graphicsLayer.setMinScale(150000d);
        graphicsLayer.setMaxScale(1000d);
        graphicsLayerTPH.setMinScale(150000d);
        graphicsLayerTPH.setMaxScale(1000d);
        graphicsLayerLangsiran.setMinScale(150000d);
        graphicsLayerLangsiran.setMaxScale(1000d);
        graphicsLayerText.setMinScale(70000d);
        graphicsLayerText.setMaxScale(1000d);
        baseActivity.mapView.addLayer(graphicsLayer);
        baseActivity.mapView.addLayer(graphicsLayerTPH);
        baseActivity.mapView.addLayer(graphicsLayerLangsiran);
        baseActivity.mapView.addLayer(graphicsLayerText);
        baseActivity.mapView.invalidate();
    }

    public void locationListenerSetup(){

        if(baseActivity.mapView!=null && baseActivity.mapView.isLoaded()){

            locationDisplayManager = baseActivity.mapView.getLocationDisplayManager();
            locationDisplayManager.setAllowNetworkLocation(false);
            locationDisplayManager.setAccuracyCircleOn(true);
            locationDisplayManager.setLocationListener(new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    baseActivity.currentlocation = location;
//                    zoomToLocation(location.getLatitude(), location.getLongitude(), spatialReference);
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {

                }

                @Override
                public void onProviderEnabled(String s) {

                }

                @Override
                public void onProviderDisabled(String s) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("GPS anda tidak hidup! Apakah anda ingin menghidupkan GPS?")
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();

                                }
                            });
                    final AlertDialog alert = builder.create();
                    alert.show();

                }


            });
            locationDisplayManager.start();
        }
    }

    public void setupPoint(){
        if(qcMutuBuahActivity.currentlocation != null) {
            latitude = qcMutuBuahActivity.currentlocation.getLatitude();
            longitude = qcMutuBuahActivity.currentlocation.getLongitude();
        }

        if(selectedTransaksiPanen != null) {
            latitude = selectedTransaksiPanen.getLatitude();
            longitude = selectedTransaksiPanen.getLongitude();
        }

//        Set<String> block = new ArraySet<>();
        Gson gson = new Gson();
        String dataString = gson.toJson(tphHelper.getNearBlock(latitude,longitude));
        NearestBlock nearestBlock = gson.fromJson(dataString,NearestBlock.class);
        ArrayList<TPH> tphArrayList = tphHelper.setDataTph(GlobalHelper.getEstate().getEstCode(),nearestBlock.getBlocks().toArray(new String[nearestBlock.getBlocks().size()]));
        for(TPH tph: tphArrayList){
            addGraphicTph(tph);
        }
//        if(qcMutuBuahActivity.currentlocation != null) {

            if (selectedTransaksiPanen != null) {
                addGrapTemp(selectedTransaksiPanen.getLatitude(), selectedTransaksiPanen.getLongitude());
            }

            zoomToLocation(latitude, longitude, spatialReference);
//        }
        if(nearestBlock.getBlock() != null){
            blockName = nearestBlock.getBlock();
        }
    }

    public void zoomToLocation(double lat, double lon, SpatialReference spatialReference) {
        Log.d(TAG, "zoomToLocation: "+lat +":"+lon);
        Point mapPoint = GeometryEngine.project(lon,lat,spatialReference);
        try {
            baseActivity.mapView.centerAt(mapPoint,true);
            baseActivity.mapView.zoomTo(mapPoint,15000f);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void addGraphicTph(TPH tph){

        Point fromPoint = new Point(tph.getLongitude(), tph.getLatitude());
        Point toPoint = (Point) GeometryEngine.project(fromPoint, SpatialReference.create(SpatialReference.WKID_WGS84), spatialReference);

        CompositeSymbol cms = new CompositeSymbol();
        int color = tph.getResurvey() == 1 ? TPH.COLOR_TPH_RESURVEY : TPH.COLOR_TPH;
        SimpleMarkerSymbol sms = new SimpleMarkerSymbol(color, Utils.convertDpToPx(getActivity(), GlobalHelper.MARKER_SIZE_TRANSKASI), SimpleMarkerSymbol.STYLE.CIRCLE);
        SimpleLineSymbol sls = new SimpleLineSymbol(TPH.COLOR_BELUMUPLOAD, GlobalHelper.GARISTEPI_SIZE, SimpleLineSymbol.STYLE.SOLID);
        if(tph.getStatus() == TPH.STATUS_TPH_MASTER || tph.getStatus() == TPH.STATUS_TPH_UPLOAD){
            sls = new SimpleLineSymbol(TPH.COLOR_UPLOAD, GlobalHelper.GARISTEPI_SIZE, SimpleLineSymbol.STYLE.SOLID);
        }
        sms.setOutline(sls);
        cms.add(sms);

        Graphic pointGraphic = new Graphic(toPoint, cms);
        graphicsLayerTPH.addGraphic(pointGraphic);

        TextSymbol.HorizontalAlignment horizontalAlignment = TextSymbol.HorizontalAlignment.CENTER;
        TextSymbol.VerticalAlignment verticalAlignment = TextSymbol.VerticalAlignment.BOTTOM;

        int Tcolor = Color.parseColor("#000000");
        TextSymbol txtSymbol = new TextSymbol(GlobalHelper.TEXT_SIZE_TRANSKASI,tph.getNamaTph(),Tcolor) ;
        txtSymbol.setHorizontalAlignment(horizontalAlignment);
        txtSymbol.setVerticalAlignment(verticalAlignment);

        pointGraphic = new Graphic(toPoint,txtSymbol);
        graphicsLayerText.addGraphic(pointGraphic);
        Log.d(TAG, "addGraphicTph: "+ tph.getNamaTph());
    }

    public void addGrapTemp(Double lat, Double lon){
        if(baseActivity.mapView!=null && baseActivity.mapView.isLoaded()){
            Point fromPoint = new Point(lon, lat);
            Point toPoint = (Point) GeometryEngine.project(fromPoint, SpatialReference.create(SpatialReference.WKID_WGS84), spatialReference);

            CompositeSymbol cms = new CompositeSymbol();
            SimpleMarkerSymbol sms = new SimpleMarkerSymbol(Color.GREEN, Utils.convertDpToPx(getActivity(), GlobalHelper.MARKER_SIZE), SimpleMarkerSymbol.STYLE.X);
            SimpleLineSymbol sls = new SimpleLineSymbol(Color.GREEN, GlobalHelper.GARISTEPI_SIZE, SimpleLineSymbol.STYLE.SOLID);
            sms.setOutline(sls);
            cms.add(sms);
            Graphic pointGraphic = new Graphic(toPoint, cms);
            graphicsLayer.addGraphic(pointGraphic);
            Log.d(TAG, "addGrapTemp: "+lat + ":"+ lon);
        }
    }

    private void openDateRangePicker(){

        SlyCalendarDialog dialog = new SlyCalendarDialog()
                .setSingle(true)
                .setHeaderColor(getResources().getColor(R.color.Green))
                .setSelectedColor(getResources().getColor(R.color.md_orange_400))
                .setCallback(new SlyCalendarDialog.Callback() {
                    @Override
                    public void onCancelled() {

                    }

                    @Override
                    public void onDataSelected(Calendar firstDate, Calendar secondDate, int hours, int minutes) {
                        long selisih = Math.abs(TimeUnit.MILLISECONDS.toDays(
                                System.currentTimeMillis() - firstDate.getTimeInMillis()
                        ));
                        if (System.currentTimeMillis() < firstDate.getTimeInMillis()){
                            Toast.makeText(HarvestApp.getContext(),"Tanggal Panen Harus Di Bawah Atau Sama Dengan Tgl Hari Ini ",Toast.LENGTH_SHORT).show();
                            openDateRangePicker();
                            return;
                        } else if (selisih > GlobalHelper.MAX_TANGGAL_PANEN){
                            Toast.makeText(HarvestApp.getContext(),"Tanggal Panen Yang Di Pilih Terlalu Jauh",Toast.LENGTH_SHORT).show();
                            openDateRangePicker();
                            return;
                        }
                        waktuPanen = firstDate.getTimeInMillis();
                        etTglPanen.setText(formatDate.format(waktuPanen));
                    }
                });
        dialog.show(getActivity().getSupportFragmentManager(), "TAG_SLYCALENDAR");
    }

    public int cekPersentase(){
        int max = 100;
        int persen = 0;
        for(int i = 0; i < rvPemanen.getAdapter().getItemCount(); i++){
            RecyclerView.ViewHolder viewHolder = rvPemanen.findViewHolderForAdapterPosition(i);
            if(viewHolder != null) {
                View view = viewHolder.itemView;
                AppCompatEditText etPersentase = view.findViewById(R.id.etPersentase);

                if (!etPersentase.getText().toString().isEmpty()) {
                    persen += Integer.parseInt(etPersentase.getText().toString());
                }
            }else{
                SubPemanen subPemanen = subPemanens.get(i);
                persen += subPemanen.getPersentase();
            }
        }

        if(persen <= max){
            return 0;
        }else{
            return persen - max ;
        }
    }

    public void distribusiJanjangSubPemanen(boolean setupAwal){
        HasilPanen hasilPanenX = new HasilPanen();
//        int totalBrondolan = 0;
//        int buahMentah = 0;
//        int tangkaiPanjang = 0;

        if(!etNormal.getText().toString().isEmpty()){
            hasilPanenX.setJanjangNormal(Integer.parseInt(etNormal.getText().toString()));
        }
        if(!etMentah.getText().toString().isEmpty()) {
            hasilPanenX.setBuahMentah(Integer.parseInt(etMentah.getText().toString()));
//            buahMentah = Integer.parseInt(etMentah.getText().toString());
        }
        if(!etBusuk.getText().toString().isEmpty()) {
            hasilPanenX.setBusukNJangkos(Integer.parseInt(etBusuk.getText().toString()));
//            buahMentah = Integer.parseInt(etMentah.getText().toString());
        }
        if(!etLewatMatang.getText().toString().isEmpty()) {
            hasilPanenX.setBuahLewatMatang(Integer.parseInt(etLewatMatang.getText().toString()));
//            buahMentah = Integer.parseInt(etMentah.getText().toString());
        }
        if(!etAbnormal.getText().toString().isEmpty()) {
            hasilPanenX.setBuahAbnormal(Integer.parseInt(etAbnormal.getText().toString()));
//            buahMentah = Integer.parseInt(etMentah.getText().toString());
        }
        if(!etTangkaiPanjang.getText().toString().isEmpty()) {
            hasilPanenX.setTangkaiPanjang(Integer.parseInt(etTangkaiPanjang.getText().toString()));
//            tangkaiPanjang = Integer.parseInt(etTangkaiPanjang.getText().toString());
        }
        if(!etBuahDimakanTikus.getText().toString().isEmpty()) {
            hasilPanenX.setBuahDimakanTikus(Integer.parseInt(etBuahDimakanTikus.getText().toString()));
//            totalJjg = Integer.parseInt(etTotalJanjang.getText().toString());
        }
        if(!etBrondolan.getText().toString().isEmpty()) {
            hasilPanenX.setBrondolan(Integer.parseInt(etBrondolan.getText().toString()));
//            totalBrondolan = Integer.parseInt(etBrondolan.getText().toString());
        }
        if(!etTotalJanjangPendapatan.getText().toString().isEmpty()) {
            hasilPanenX.setTotalJanjangPendapatan(Integer.parseInt(etTotalJanjangPendapatan.getText().toString()));
//            totalJjgPendapatan = Integer.parseInt(etTotalJanjangPendapatan.getText().toString());
        }
        if(!etTotalJanjang.getText().toString().isEmpty()) {
            hasilPanenX.setTotalJanjang(Integer.parseInt(etTotalJanjang.getText().toString()));
//            totalJjg = Integer.parseInt(etTotalJanjang.getText().toString());
        }

        SubPemanen totalDistribusi = new SubPemanen(new HasilPanen(),SubPemanen.idPemnaen);
        ArrayList<SubPemanen> subPemanensX = subPemanens;
        pemanens = new HashMap<>();
        subPemanens = new ArrayList<>();
        pemanenOperator = new HashMap<>();
        pemanenOperatorTonase = new HashMap<>();
        int operatorCount = 0;
        int operatorCountTonase = 0;
        int pemanenCount = 0;

        int nilaiPersenPemanen = 0;
        int nilaiPersenOperator = 0;
        int maxPersenPemanen = 100;
        ArrayList<Integer> intPersenPemanen = new ArrayList<Integer>();
        ArrayList<Integer> intPersenOperator = new ArrayList<Integer>();
        ArrayList<Integer> intPersenOperatorTonase = new ArrayList<Integer>();
        if(tipeKongsi.getPosition() == Kongsi.idMekanis) {
            maxPersenPemanen = dynamicParameterPenghasilan.getPersenPemanen();
            for (int i = 0; i < rvPemanen.getAdapter().getItemCount(); i++) {
                RecyclerView.ViewHolder viewHolder = rvPemanen.findViewHolderForAdapterPosition(i);
                if (viewHolder != null) {
                    View view = viewHolder.itemView;
                    CompleteTextViewHelper lsTipe = view.findViewById(R.id.lsTipe);
                    TextView tvValue = view.findViewById(R.id.tvValue);
                    if (lsTipe.getText().toString().equals(SubPemanen.arrayCodeTipePemanen[SubPemanen.idOperator])) {
                        Gson gson = new Gson();
                        Pemanen pemanen = gson.fromJson(tvValue.getText().toString(), Pemanen.class);
                        pemanenOperator.put(pemanen.getKodePemanen(), pemanen);
                    }else if (lsTipe.getText().toString().equals(SubPemanen.arrayCodeTipePemanen[SubPemanen.idOperatorTonase])){
                        Gson gson = new Gson();
                        Pemanen pemanen = gson.fromJson(tvValue.getText().toString(), Pemanen.class);
                        pemanenOperatorTonase.put(pemanen.getKodePemanen(), pemanen);
                    }
                }
            }

            if(pemanenOperatorTonase.size() > 0 && pemanenOperator.size() == 0){
                maxPersenPemanen = 100;
            }
        }

        for(int i = 0 ; i < rvPemanen.getAdapter().getItemCount(); i ++){
            RecyclerView.ViewHolder viewHolder = rvPemanen.findViewHolderForAdapterPosition(i);
            if (viewHolder != null) {
                View view = viewHolder.itemView;
                CompleteTextViewHelper lsTipe = view.findViewById(R.id.lsTipe);
                if(pemanenOperator.size() > 0 || pemanenOperatorTonase.size() > 0){
                    if (lsTipe.getText().toString().equals(SubPemanen.arrayCodeTipePemanen[SubPemanen.idOperator])) {
                        int persen = 0;
                        if (dynamicParameterPenghasilan.getPersenOperator() > 0 && pemanenOperator.size() > 0) {
                            persen = (int) Math.floor(dynamicParameterPenghasilan.getPersenOperator() / pemanenOperator.size());
                        }

                        intPersenOperator.add(persen);
                        nilaiPersenOperator += persen;
                    } else if (lsTipe.getText().toString().equals(SubPemanen.arrayCodeTipePemanen[SubPemanen.idOperatorTonase])) {
                        intPersenOperatorTonase.add(0);
                    } else {
                        int pemanenSize = rvPemanen.getAdapter().getItemCount() - pemanenOperator.size() - pemanenOperatorTonase.size();
                        int persen = 0;
                        if (pemanenSize > 0) {
                            persen = (int) Math.floor(maxPersenPemanen / pemanenSize);
                        }
                        intPersenPemanen.add(persen);
                        nilaiPersenPemanen += persen;
                    }
                }else{
                    int pemanenSize = rvPemanen.getAdapter().getItemCount();
                    int persen = 0;
                    if (pemanenSize > 0) {
                        persen = (int) Math.floor(maxPersenPemanen / pemanenSize);
                    }
                    intPersenPemanen.add(persen);
                    nilaiPersenPemanen += persen;
                }
            }
        }

        if(intPersenOperator.size() > 0) {
            if (nilaiPersenOperator < dynamicParameterPenghasilan.getPersenOperator()) {
                int jmlOperator = intPersenOperator.size();
                if(intPersenOperatorTonase.size() > 0){
                    jmlOperator = intPersenOperator.size() - intPersenOperatorTonase.size();
                }

                ArrayList<Integer> intPersenOperatorFix = new ArrayList<Integer>();
                int sisa = dynamicParameterPenghasilan.getPersenOperator() - nilaiPersenOperator;
                int ditriButSisa = (int) Math.ceil(sisa / Double.parseDouble(String.valueOf(jmlOperator)));
                for (int i = 0; i < jmlOperator; i++) {
                    intPersenOperatorFix.add(i, intPersenOperator.get(i) + ditriButSisa);
                    sisa -= ditriButSisa;

                    if (sisa == 0) {
                        break;
                    } else if (ditriButSisa > sisa) {
                        ditriButSisa = sisa;
                    }
                }

                intPersenOperator = intPersenOperatorFix;
            }
        }

        if(intPersenPemanen.size() > 0) {
            if (nilaiPersenPemanen < maxPersenPemanen) {
                int sisa = maxPersenPemanen - nilaiPersenPemanen;
                int ditriButSisa = (int) Math.ceil(sisa / Double.parseDouble(String.valueOf(intPersenPemanen.size())));
                for (int i = 0; i < intPersenPemanen.size(); i++) {
                    intPersenPemanen.set(i, intPersenPemanen.get(i) + ditriButSisa);
                    sisa -= ditriButSisa;

                    if (sisa == 0) {
                        break;
                    } else if (ditriButSisa > sisa) {
                        ditriButSisa = sisa;
                    }
                }
            }
        }

        int idxPemanen = 0 ;
        int idxOperator = 0;
        int idxOperatorTonase = 0;

        for(int i = 0; i < rvPemanen.getAdapter().getItemCount(); i++){
            RecyclerView.ViewHolder viewHolder = rvPemanen.findViewHolderForAdapterPosition(i);
            int tipePemanen = SubPemanen.idPemnaen;
            if(viewHolder != null) {
                View view = viewHolder.itemView;
                AppCompatEditText etPersentase = view.findViewById(R.id.etPersentase);
                TextView tvValue = view.findViewById(R.id.tvValue);

                Gson gson = new Gson();
                HasilPanen hasilPanenSubPemanen = new HasilPanen();
//                int totalJanjangSub = 0;
//                int totalBrodolanSub = 0;
//                int totalJanjangPdtSub = 0;
//                int buahMentahSub = 0;
//                int tangkaiPanjangSub = 0;

                Pemanen pemanen = gson.fromJson(tvValue.getText().toString(), Pemanen.class);

                boolean operatorPosision = false;
                if(pemanen != null && (pemanenOperator.size() > 0 || pemanenOperatorTonase.size() > 0)) {
                    if(pemanenOperator.size() > 0){
                        if (pemanenOperator.get(pemanen.getKodePemanen()) != null) {
                            operatorPosision = true;
                            operatorCount ++;
                            tipePemanen = SubPemanen.idOperator;
                        }
                    }
                    if (pemanenOperatorTonase.size() > 0){
                        if (pemanenOperatorTonase.get(pemanen.getKodePemanen()) != null) {
                            operatorCountTonase ++;
                            tipePemanen = SubPemanen.idOperatorTonase;
                        }
                    }
                }

                if(tipePemanen == SubPemanen.idPemnaen){
                    pemanenCount++;
                }

                if(setupAwal){
//                    if(idKongsi == Kongsi.idMekanis){
                        if(tipePemanen == SubPemanen.idPemnaen){
                            if(intPersenPemanen.size() > 0) {
                                if(idxPemanen < intPersenPemanen.size()) {
                                    etPersentase.setText(String.valueOf(intPersenPemanen.get(idxPemanen)));
                                }
                                idxPemanen++;
                            }else{
                                etPersentase.setText(String.valueOf(0));
                            }
                        } else if (tipePemanen == SubPemanen.idOperatorTonase){
                            idxOperatorTonase++;
                            etPersentase.setText(String.valueOf(0));
                        }else {
                            if (intPersenOperator.size() > 0) {
                                if(idxOperator < intPersenOperator.size()) {
                                    etPersentase.setText(String.valueOf(intPersenOperator.get(idxOperator)));
                                }
                                idxOperator++;
                            }else{
                                etPersentase.setText(String.valueOf(0));
                            }
                        }
//                    }else {
//                        etPersentase.setText(String.valueOf(SubPemanenHelper.getPersentase(
//                                dynamicParameterPenghasilan,
//                                rvPemanen.getAdapter().getItemCount(),
//                                i,
//                                idKongsi,
//                                operatorPosision,
//                                pemanenOperator.size(),
//                                operatorCount, pemanenCount
//                        )));
//                    }
                }

                if (!etPersentase.getText().toString().isEmpty()) {
                    if (etPersentase.getText().toString().matches(".*\\d.*")) {
                        if (totalDistribusi.getHasilPanen().getTotalJanjang() <= hasilPanenX.getTotalJanjang() || totalDistribusi.getHasilPanen().getBrondolan() <= hasilPanenX.getBrondolan()) {
                            int persentase = Integer.parseInt(etPersentase.getText().toString());

                            hasilPanenSubPemanen.setJanjangNormal((int) Math.floor((float) (hasilPanenX.getJanjangNormal() * persentase) / 100));
                            hasilPanenSubPemanen.setBuahMentah((int) Math.floor((float) (hasilPanenX.getBuahMentah() * persentase) / 100));
                            hasilPanenSubPemanen.setBusukNJangkos((int) Math.floor((float) (hasilPanenX.getBusukNJangkos() * persentase) / 100));
                            hasilPanenSubPemanen.setBuahLewatMatang((int) Math.floor((float) (hasilPanenX.getBuahLewatMatang() * persentase) / 100));
                            hasilPanenSubPemanen.setBuahAbnormal((int) Math.floor((float) (hasilPanenX.getBuahAbnormal() * persentase) / 100));
                            hasilPanenSubPemanen.setTangkaiPanjang((int) Math.floor((float) (hasilPanenX.getTangkaiPanjang() * persentase) / 100));
                            hasilPanenSubPemanen.setBuahDimakanTikus((int) Math.floor((float) (hasilPanenX.getBuahDimakanTikus() * persentase) / 100));
                            hasilPanenSubPemanen.setBrondolan((int) Math.floor((float) (hasilPanenX.getBrondolan() * persentase) / 100));
                            hasilPanenSubPemanen.setTotalJanjang((int) Math.floor((float) (hasilPanenX.getTotalJanjang() * persentase) / 100));
                            hasilPanenSubPemanen.setTotalJanjangPendapatan((int) Math.floor((float) (hasilPanenX.getTotalJanjangPendapatan() * persentase) / 100));

                            totalDistribusi.setHasilPanen(transaksiPanenHelper.combineHasilPanen(totalDistribusi.getHasilPanen(), hasilPanenSubPemanen));

                            subPemanens.add(i,
                                    new SubPemanen(i,
                                            GlobalHelper.getCharForNumber(i),
                                            persentase,
                                            hasilPanenSubPemanen,
                                            tipePemanen,
                                            pemanen
                                    )
                            );
                            if (pemanen != null) {
                                pemanens.put(pemanen.getKodePemanen(), pemanen);
                            }
                        } else {
                            subPemanens.add(i,
                                    new SubPemanen(i,
                                            GlobalHelper.getCharForNumber(i),
                                            0,
                                            new HasilPanen(),
                                            tipePemanen,
                                            pemanen
                                    )
                            );
                            if (pemanen != null) {
                                pemanens.put(pemanen.getKodePemanen(), pemanen);
                            }
                        }
                    }
                }
            }else {
                SubPemanen subPemanen = subPemanensX.get(i);
                totalDistribusi.setHasilPanen(transaksiPanenHelper.combineHasilPanen(totalDistribusi.getHasilPanen(),subPemanen.getHasilPanen()));

                subPemanens.add(i, subPemanen);
                if (subPemanen.getPemanen() != null) {
                    pemanens.put(subPemanen.getPemanen().getKodePemanen(), subPemanen.getPemanen());
                }
            }
        }

        adapterPemanen = new SelectPemanenAdapter(getActivity(),
                R.layout.row_record,
                new ArrayList<>(pemanens.values())
        );

        if(transaksiPanenHelper.cekSisaHasilPanen(totalDistribusi.getHasilPanen(),hasilPanenX)){

            sisaJjgBrdl = new HasilPanen(
                    hasilPanenX.getJanjangNormal() - totalDistribusi.getHasilPanen().getJanjangNormal(),
                    hasilPanenX.getBuahMentah() - totalDistribusi.getHasilPanen().getBuahMentah(),
                    hasilPanenX.getBusukNJangkos() - totalDistribusi.getHasilPanen().getBusukNJangkos(),
                    hasilPanenX.getBuahLewatMatang() - totalDistribusi.getHasilPanen().getBuahLewatMatang(),
                    hasilPanenX.getBuahAbnormal() - totalDistribusi.getHasilPanen().getBuahAbnormal(),
                    hasilPanenX.getTangkaiPanjang() - totalDistribusi.getHasilPanen().getTangkaiPanjang(),
                    hasilPanenX.getBuahDimakanTikus() - totalDistribusi.getHasilPanen().getBuahDimakanTikus(),
                    hasilPanenX.getBrondolan() - totalDistribusi.getHasilPanen().getBrondolan(),
                    0,
                    hasilPanenX.getTotalJanjang() - totalDistribusi.getHasilPanen().getTotalJanjang(),
                    hasilPanenX.getTotalJanjangPendapatan() - totalDistribusi.getHasilPanen().getTotalJanjangPendapatan()

            );

            HasilPanen distribusiSisa = new HasilPanen(
                    sisaJjgBrdl.getJanjangNormal() != 0 ? (int) Math.ceil(Double.parseDouble(String.valueOf(sisaJjgBrdl.getJanjangNormal())) / Double.parseDouble(String.valueOf(subPemanens.size()))) :0,
                    sisaJjgBrdl.getBuahMentah() != 0 ? (int) Math.ceil(Double.parseDouble(String.valueOf(sisaJjgBrdl.getBuahMentah())) / Double.parseDouble(String.valueOf(subPemanens.size()))) :0,
                    sisaJjgBrdl.getBusukNJangkos() != 0 ? (int) Math.ceil(Double.parseDouble(String.valueOf(sisaJjgBrdl.getBusukNJangkos())) / Double.parseDouble(String.valueOf(subPemanens.size()))) :0,
                    sisaJjgBrdl.getBuahLewatMatang() != 0 ? (int) Math.ceil(Double.parseDouble(String.valueOf(sisaJjgBrdl.getBuahLewatMatang())) / Double.parseDouble(String.valueOf(subPemanens.size()))) :0,
                    sisaJjgBrdl.getBuahAbnormal() != 0 ? (int) Math.ceil(Double.parseDouble(String.valueOf(sisaJjgBrdl.getBuahAbnormal())) / Double.parseDouble(String.valueOf(subPemanens.size()))) :0,
                    sisaJjgBrdl.getTangkaiPanjang() != 0 ? (int) Math.ceil(Double.parseDouble(String.valueOf(sisaJjgBrdl.getTangkaiPanjang())) / Double.parseDouble(String.valueOf(subPemanens.size()))) :0,
                    sisaJjgBrdl.getBuahDimakanTikus() != 0 ? (int) Math.ceil(Double.parseDouble(String.valueOf(sisaJjgBrdl.getBuahDimakanTikus())) / Double.parseDouble(String.valueOf(subPemanens.size()))) :0,
                    sisaJjgBrdl.getBrondolan() != 0 ? (int) Math.ceil(Double.parseDouble(String.valueOf(sisaJjgBrdl.getBrondolan())) / Double.parseDouble(String.valueOf(subPemanens.size()))) :0,
                    0,
                    sisaJjgBrdl.getTotalJanjang() != 0 ? (int) Math.ceil(Double.parseDouble(String.valueOf(sisaJjgBrdl.getTotalJanjang())) / Double.parseDouble(String.valueOf(subPemanens.size()))) :0,
                    sisaJjgBrdl.getTotalJanjangPendapatan() != 0 ? (int) Math.ceil(Double.parseDouble(String.valueOf(sisaJjgBrdl.getTotalJanjangPendapatan())) / Double.parseDouble(String.valueOf(subPemanens.size()))) :0
            );

            ArrayList<SubPemanen> subPemanensXX = new ArrayList<>();
            for(int i = 0 ; i < subPemanens.size(); i++){
                SubPemanen subPemanen = subPemanens.get(i);
                if(subPemanen.getTipePemanen() != SubPemanen.idOperatorTonase) {
                    HasilPanen subPemanaenHasilPanen = subPemanen.getHasilPanen();
                    if (sisaJjgBrdl.getJanjangNormal() > 0) {
                        int tambahDistribusi = distribusiSisa.getJanjangNormal();
                        if (distribusiSisa.getJanjangNormal() > sisaJjgBrdl.getJanjangNormal()) {
                            tambahDistribusi = sisaJjgBrdl.getJanjangNormal();
                        }
                        subPemanaenHasilPanen.setJanjangNormal(subPemanaenHasilPanen.getJanjangNormal() + tambahDistribusi);
                        sisaJjgBrdl.setJanjangNormal(sisaJjgBrdl.getJanjangNormal() - tambahDistribusi);
                    }
                    if (sisaJjgBrdl.getBuahMentah() > 0) {
                        int tambahDistribusi = distribusiSisa.getBuahMentah();
                        if (distribusiSisa.getBuahMentah() > sisaJjgBrdl.getBuahMentah()) {
                            tambahDistribusi = sisaJjgBrdl.getBuahMentah();
                        }
                        subPemanaenHasilPanen.setBuahMentah(subPemanaenHasilPanen.getBuahMentah() + tambahDistribusi);
                        sisaJjgBrdl.setBuahMentah(sisaJjgBrdl.getBuahMentah() - tambahDistribusi);
                    }
                    if (sisaJjgBrdl.getBusukNJangkos() > 0) {
                        int tambahDistribusi = distribusiSisa.getBusukNJangkos();
                        if (distribusiSisa.getBusukNJangkos() > sisaJjgBrdl.getBusukNJangkos()) {
                            tambahDistribusi = sisaJjgBrdl.getBusukNJangkos();
                        }
                        subPemanaenHasilPanen.setBusukNJangkos(subPemanaenHasilPanen.getBusukNJangkos() + tambahDistribusi);
                        sisaJjgBrdl.setBusukNJangkos(sisaJjgBrdl.getBusukNJangkos() - tambahDistribusi);
                    }
                    if (sisaJjgBrdl.getBuahLewatMatang() > 0) {
                        int tambahDistribusi = distribusiSisa.getBuahLewatMatang();
                        if (distribusiSisa.getBuahLewatMatang() > sisaJjgBrdl.getBuahLewatMatang()) {
                            tambahDistribusi = sisaJjgBrdl.getBuahLewatMatang();
                        }
                        subPemanaenHasilPanen.setBuahLewatMatang(subPemanaenHasilPanen.getBuahLewatMatang() + tambahDistribusi);
                        sisaJjgBrdl.setBuahLewatMatang(sisaJjgBrdl.getBuahLewatMatang() - tambahDistribusi);
                    }
                    if (sisaJjgBrdl.getBuahAbnormal() > 0) {
                        int tambahDistribusi = distribusiSisa.getBuahAbnormal();
                        if (distribusiSisa.getBuahAbnormal() > sisaJjgBrdl.getBuahAbnormal()) {
                            tambahDistribusi = sisaJjgBrdl.getBuahAbnormal();
                        }
                        subPemanaenHasilPanen.setBuahAbnormal(subPemanaenHasilPanen.getBuahAbnormal() + tambahDistribusi);
                        sisaJjgBrdl.setBuahAbnormal(sisaJjgBrdl.getBuahAbnormal() - tambahDistribusi);
                    }
                    if (sisaJjgBrdl.getTangkaiPanjang() > 0) {
                        int tambahDistribusi = distribusiSisa.getTangkaiPanjang();
                        if (distribusiSisa.getTangkaiPanjang() > sisaJjgBrdl.getTangkaiPanjang()) {
                            tambahDistribusi = sisaJjgBrdl.getTangkaiPanjang();
                        }
                        subPemanaenHasilPanen.setTangkaiPanjang(subPemanaenHasilPanen.getTangkaiPanjang() + tambahDistribusi);
                        sisaJjgBrdl.setTangkaiPanjang(sisaJjgBrdl.getTangkaiPanjang() - tambahDistribusi);
                    }
                    if (sisaJjgBrdl.getBuahDimakanTikus() > 0) {
                        int tambahDistribusi = distribusiSisa.getBuahDimakanTikus();
                        if (distribusiSisa.getBuahDimakanTikus() > sisaJjgBrdl.getBuahDimakanTikus()) {
                            tambahDistribusi = sisaJjgBrdl.getBuahDimakanTikus();
                        }
                        subPemanaenHasilPanen.setBuahDimakanTikus(subPemanaenHasilPanen.getBuahDimakanTikus() + tambahDistribusi);
                        sisaJjgBrdl.setBuahDimakanTikus(sisaJjgBrdl.getBuahDimakanTikus() - tambahDistribusi);
                    }
                    if (sisaJjgBrdl.getBrondolan() > 0) {
                        int tambahDistribusi = distribusiSisa.getBrondolan();
                        if (distribusiSisa.getBrondolan() > sisaJjgBrdl.getBrondolan()) {
                            tambahDistribusi = sisaJjgBrdl.getBrondolan();
                        }
                        subPemanaenHasilPanen.setBrondolan(subPemanaenHasilPanen.getBrondolan() + tambahDistribusi);
                        sisaJjgBrdl.setBrondolan(sisaJjgBrdl.getBrondolan() - tambahDistribusi);
                    }
                    if (sisaJjgBrdl.getTotalJanjang() > 0) {
                        int tambahDistribusi = distribusiSisa.getTotalJanjang();
                        if (distribusiSisa.getTotalJanjang() > sisaJjgBrdl.getTotalJanjang()) {
                            tambahDistribusi = sisaJjgBrdl.getTotalJanjang();
                        }
                        subPemanaenHasilPanen.setTotalJanjang(subPemanaenHasilPanen.getTotalJanjang() + tambahDistribusi);
                        sisaJjgBrdl.setTotalJanjang(sisaJjgBrdl.getTotalJanjang() - tambahDistribusi);
                    }
                    if (sisaJjgBrdl.getTotalJanjangPendapatan() > 0) {
                        int tambahDistribusi = distribusiSisa.getTotalJanjangPendapatan();
                        if (distribusiSisa.getTotalJanjangPendapatan() > sisaJjgBrdl.getTotalJanjangPendapatan()) {
                            tambahDistribusi = sisaJjgBrdl.getTotalJanjangPendapatan();
                        }
                        subPemanaenHasilPanen.setTotalJanjangPendapatan(subPemanaenHasilPanen.getTotalJanjangPendapatan() + tambahDistribusi);
                        sisaJjgBrdl.setTotalJanjangPendapatan(sisaJjgBrdl.getTotalJanjangPendapatan() - tambahDistribusi);
                    }

                    subPemanen.setHasilPanen(subPemanaenHasilPanen);
                }
                subPemanensXX.add(subPemanen);
            }

            subPemanens = new ArrayList<>();
            for(int i = 0 ; i < subPemanensXX.size(); i++){
                SubPemanen subPemanen = subPemanensXX.get(i);
                HasilPanen hasilPanen = new HasilPanen(
                        0,
                        subPemanen.getHasilPanen().getBuahMentah(),
                        subPemanen.getHasilPanen().getBusukNJangkos(),
                        subPemanen.getHasilPanen().getBuahLewatMatang(),
                        subPemanen.getHasilPanen().getBuahAbnormal(),
                        subPemanen.getHasilPanen().getTangkaiPanjang(),
                        subPemanen.getHasilPanen().getBuahDimakanTikus(),
                        subPemanen.getHasilPanen().getBrondolan(),
                        subPemanen.getHasilPanen().getTotalJanjangBuruk(),
                        subPemanen.getHasilPanen().getTotalJanjang(),
                        0,
                        subPemanen.getHasilPanen().getWaktuPanen(),
                        subPemanen.getHasilPanen().isAlasanBrondolan()
                );

                hasilPanen.setJanjangNormal(hasilPanen.getTotalJanjang() - GlobalHelper.getTotalJanjangPendapatan(new HasilPanen(
                        0,
                        hasilPanen.getBuahMentah(),
                        hasilPanen.getBusukNJangkos(),
                        hasilPanen.getBuahLewatMatang(),
                        hasilPanen.getBuahAbnormal(),
                        hasilPanen.getTangkaiPanjang(),
                        hasilPanen.getBuahDimakanTikus(),
                        hasilPanen.getBrondolan(),
                        0,
                        hasilPanen.getTotalJanjang(),
                        0
                ),dynamicParameterPenghasilan));
                hasilPanen.setTotalJanjangPendapatan(GlobalHelper.getTotalJanjangPendapatan(hasilPanen,dynamicParameterPenghasilan));

                subPemanen.setHasilPanen(new HasilPanen(
                        hasilPanen.getJanjangNormal(),
                        subPemanen.getHasilPanen().getBuahMentah(),
                        subPemanen.getHasilPanen().getBusukNJangkos(),
                        subPemanen.getHasilPanen().getBuahLewatMatang(),
                        subPemanen.getHasilPanen().getBuahAbnormal(),
                        subPemanen.getHasilPanen().getTangkaiPanjang(),
                        subPemanen.getHasilPanen().getBuahDimakanTikus(),
                        subPemanen.getHasilPanen().getBrondolan(),
                        subPemanen.getHasilPanen().getTotalJanjangBuruk(),
                        subPemanen.getHasilPanen().getTotalJanjang(),
                        hasilPanen.getTotalJanjangPendapatan(),
                        subPemanen.getHasilPanen().getWaktuPanen(),
                        subPemanen.getHasilPanen().isAlasanBrondolan()
                ));
                subPemanens.add(subPemanen);
            }

            if(transaksiPanenHelper.cekSisaHasilPanen(sisaJjgBrdl)) {
                lSisa.setVisibility(View.VISIBLE);
                tvSisa.setText("Sisa" +
                        "\nJJg   : " + String.valueOf(sisaJjgBrdl.getTotalJanjang()) +
                        "\nJ Pdt : " + String.valueOf(sisaJjgBrdl.getTotalJanjangPendapatan()) +
                        "\nBrdl  : " + String.valueOf(sisaJjgBrdl.getBrondolan())
                );
                tvSisa2.setText(
                        "\n(N)  : " + String.valueOf(sisaJjgBrdl.getJanjangNormal()) +
                                "\n(A)  : " + String.valueOf(sisaJjgBrdl.getBuahMentah()) +
                                "\n(E) : " + String.valueOf(sisaJjgBrdl.getBusukNJangkos())
                );
                tvSisa3.setText(
                        "\n(0) : " + String.valueOf(sisaJjgBrdl.getBuahLewatMatang()) +
                                "\n(BA) : " + String.valueOf(sisaJjgBrdl.getBuahAbnormal()) +
                                "\n(TP) : " + String.valueOf(sisaJjgBrdl.getTangkaiPanjang()) +
                                "\n(BDT) : " + String.valueOf(sisaJjgBrdl.getBuahDimakanTikus())
                );
                etPemanenSisa.setThreshold(1);
                etPemanenSisa.setAdapter(adapterPemanen);
            }else{
                lSisa.setVisibility(View.GONE);
            }
        }else{
            lSisa.setVisibility(View.GONE);
        }

        adapterPemanen.notifyDataSetChanged();

        for(int i = 0 ; i < subPemanens.size(); i++) {
            SubPemanen subPemanen = subPemanens.get(i);
            RecyclerView.ViewHolder viewHolder = rvPemanen.findViewHolderForAdapterPosition(subPemanen.getNoUrutKongsi());
            if (viewHolder != null) {
                View view = viewHolder.itemView;
                AppCompatEditText etNormalSubPemanen = view.findViewById(R.id.etNormalSubPemanen);
                AppCompatEditText etBuahMentahSubPemanen = view.findViewById(R.id.etBuahMentahSubPemanen);
                AppCompatEditText etBusukSubPemanen = view.findViewById(R.id.etBusukSubPemanen);
                AppCompatEditText etLewatMatangSubPemanen = view.findViewById(R.id.etLewatMatangSubPemanen);
                AppCompatEditText etAbnormalSubPemanen = view.findViewById(R.id.etAbnormalSubPemanen);
                AppCompatEditText etTangkaiPanjangSubPemanen = view.findViewById(R.id.etTangkaiPanjangSubPemanen);
                AppCompatEditText etBuahDimakanTikusSubPemanen = view.findViewById(R.id.etBuahDimakanTikusSubPemanen);
                AppCompatEditText etTotalBrondolanSubPemanen = view.findViewById(R.id.etTotalBrondolanSubPemanen);
                AppCompatEditText etTotalJanjangSubPemanen = view.findViewById(R.id.etTotalJanjangSubPemanen);
                AppCompatEditText etTotalPdtSubPemanen = view.findViewById(R.id.etTotalPdtSubPemanen);
                CompleteTextViewHelper lsTipe = view.findViewById(R.id.lsTipe);

                etNormalSubPemanen.setText(String.valueOf(subPemanen.getHasilPanen().getJanjangNormal()));
                etBuahMentahSubPemanen.setText(String.valueOf(subPemanen.getHasilPanen().getBuahMentah()));
                etBusukSubPemanen.setText(String.valueOf(subPemanen.getHasilPanen().getBusukNJangkos()));
                etLewatMatangSubPemanen.setText(String.valueOf(subPemanen.getHasilPanen().getBuahLewatMatang()));
                etAbnormalSubPemanen.setText(String.valueOf(subPemanen.getHasilPanen().getBuahAbnormal()));
                etTangkaiPanjangSubPemanen.setText(String.valueOf(subPemanen.getHasilPanen().getTangkaiPanjang()));
                etBuahDimakanTikusSubPemanen.setText(String.valueOf(subPemanen.getHasilPanen().getBuahDimakanTikus()));
                etTotalBrondolanSubPemanen.setText(String.valueOf(subPemanen.getHasilPanen().getBrondolan()));
                etTotalJanjangSubPemanen.setText(String.valueOf(subPemanen.getHasilPanen().getTotalJanjang()));
                etTotalPdtSubPemanen.setText(String.valueOf(subPemanen.getHasilPanen().getTotalJanjangPendapatan()));

                lsTipe.setText(SubPemanen.arrayCodeTipePemanen[subPemanen.getTipePemanen()]);
            }

            if(subPemanen.getNoUrutKongsi() == 0){
                pemanenSelected = subPemanen.getPemanen();
            }
        }
    }

    public boolean validasiSubPemanen(){
        int persentase = 0;
        HasilPanen hasilPanenSubPemane = new HasilPanen();
        HasilPanen hasilPanenX = new HasilPanen();
//        int totalJanjangSubPemanen = 0;
//        int totalBrondolanSubPemanen = 0;
//        int totalJanjangPendapatanSubPemanen = 0;
//        int totalBrondolan = 0;

        if(!etNormal.getText().toString().isEmpty()){
            hasilPanenX.setJanjangNormal(Integer.parseInt(etNormal.getText().toString()));
        }
        if(!etMentah.getText().toString().isEmpty()) {
            hasilPanenX.setBuahMentah(Integer.parseInt(etMentah.getText().toString()));
//            buahMentah = Integer.parseInt(etMentah.getText().toString());
        }
        if(!etBusuk.getText().toString().isEmpty()) {
            hasilPanenX.setBusukNJangkos(Integer.parseInt(etBusuk.getText().toString()));
//            buahMentah = Integer.parseInt(etMentah.getText().toString());
        }
        if(!etLewatMatang.getText().toString().isEmpty()) {
            hasilPanenX.setBuahLewatMatang(Integer.parseInt(etLewatMatang.getText().toString()));
//            buahMentah = Integer.parseInt(etMentah.getText().toString());
        }
        if(!etAbnormal.getText().toString().isEmpty()) {
            hasilPanenX.setBuahAbnormal(Integer.parseInt(etAbnormal.getText().toString()));
//            buahMentah = Integer.parseInt(etMentah.getText().toString());
        }
        if(!etTangkaiPanjang.getText().toString().isEmpty()) {
            hasilPanenX.setTangkaiPanjang(Integer.parseInt(etTangkaiPanjang.getText().toString()));
//            tangkaiPanjang = Integer.parseInt(etTangkaiPanjang.getText().toString());
        }
        if(!etBuahDimakanTikus.getText().toString().isEmpty()) {
            hasilPanenX.setBuahDimakanTikus(Integer.parseInt(etBuahDimakanTikus.getText().toString()));
//            totalJjg = Integer.parseInt(etTotalJanjang.getText().toString());
        }
        if(!etBrondolan.getText().toString().isEmpty()) {
            hasilPanenX.setBrondolan(Integer.parseInt(etBrondolan.getText().toString()));
//            totalBrondolan = Integer.parseInt(etBrondolan.getText().toString());
        }
        if(!etTotalJanjangPendapatan.getText().toString().isEmpty()) {
            hasilPanenX.setTotalJanjangPendapatan(Integer.parseInt(etTotalJanjangPendapatan.getText().toString()));
//            totalJjgPendapatan = Integer.parseInt(etTotalJanjangPendapatan.getText().toString());
        }
        if(!etTotalJanjang.getText().toString().isEmpty()) {
            hasilPanenX.setTotalJanjang(Integer.parseInt(etTotalJanjang.getText().toString()));
//            totalJjg = Integer.parseInt(etTotalJanjang.getText().toString());
        }

        if(idKongsi == Kongsi.idNone){
            if(rvPemanen.getAdapter().getItemCount() > 1){
                Toast.makeText(HarvestApp.getContext(), Kongsi.arrayTipeKongsi[idKongsi]+" Hanya Bisa 1 Pemanen Saja", Toast.LENGTH_LONG).show();
                return false;
            }
        }else if(idKongsi == Kongsi.idKelompok){
            if(rvPemanen.getAdapter().getItemCount() > GlobalHelper.MAX_PEMANEN_KELOMPOK || rvPemanen.getAdapter().getItemCount() == 1){
                Toast.makeText(HarvestApp.getContext(), Kongsi.arrayTipeKongsi[idKongsi]+" Hanya Bisa "+String.valueOf(GlobalHelper.MAX_PEMANEN_KELOMPOK)+" Pemanen Dan Tidak Bisa 1 Pemanen", Toast.LENGTH_LONG).show();
                return false;
            }
        }else if(idKongsi == Kongsi.idMekanis){
            if(rvPemanen.getAdapter().getItemCount() > GlobalHelper.MAX_PEMANEN_MEKANIS || rvPemanen.getAdapter().getItemCount() == 1){
                Toast.makeText(HarvestApp.getContext(), Kongsi.arrayTipeKongsi[idKongsi]+" Hanya Bisa "+String.valueOf(GlobalHelper.MAX_PEMANEN_MEKANIS)+" Pemanen Dan Tidak Bisa 1 Pemanen", Toast.LENGTH_LONG).show();
                return false;
            }
        }

        if(lSisa.getVisibility() == View.VISIBLE && pemanenSisa == null){
            Toast.makeText(HarvestApp.getContext(), "Mohon Pilih Pemanen Yang akan di Berikan Sisa Kalkulasi", Toast.LENGTH_LONG).show();
            etPemanenSisa.requestFocus();
            return false;
        }

        if(tipeKongsi.getPosition() == Kongsi.idMekanis && (pemanenOperator.size() == 0 && pemanenOperatorTonase.size() == 0)){
            Toast.makeText(HarvestApp.getContext(), "Mohon Pilih Operator (O)", Toast.LENGTH_LONG).show();
            return false;
        }

        if(rvPemanen.getAdapter().getItemCount() <= pemanenOperator.size()){
            Toast.makeText(HarvestApp.getContext(), "Tidak Bisa Semua Tipe Operator (OJ)", Toast.LENGTH_LONG).show();
            return false;
        }

        if(rvPemanen.getAdapter().getItemCount() <= pemanenOperatorTonase.size()){
            Toast.makeText(HarvestApp.getContext(), "Tidak Bisa Semua Tipe Operator (OT)", Toast.LENGTH_LONG).show();
            return false;
        }

        if(pemanenOperator.size() >= 1 && pemanenOperatorTonase.size() >= 1){
            Toast.makeText(HarvestApp.getContext(), "Tidak Boleh Ada Operator Janjang Dan Operator Tonase Secara Bersamaan", Toast.LENGTH_LONG).show();
            return false;
        }

        if(pemanenOperatorTonase.size() >= 3 ){
            Toast.makeText(HarvestApp.getContext(), "Operator Tonase Tidak Bisa Lebih Dari 2 Orang ", Toast.LENGTH_LONG).show();
            return false;
        }

        Set<String> kodePemanen = new ArraySet<>();
        AppCompatEditText etPersentase = null;
        int pesenOT = 0;
        for(int i = 0; i < rvPemanen.getAdapter().getItemCount(); i++){
            boolean canRead = false;
            RecyclerView.ViewHolder viewHolder = rvPemanen.findViewHolderForAdapterPosition(i);
            if(viewHolder != null) {
                Gson gson = new Gson();
                canRead = false;
                View view = viewHolder.itemView;
                etPersentase = view.findViewById(R.id.etPersentase);
                AppCompatEditText etNormalSubPemanen = view.findViewById(R.id.etNormalSubPemanen);
                AppCompatEditText etBuahMentahSubPemanen = view.findViewById(R.id.etBuahMentahSubPemanen);
                AppCompatEditText etBusukSubPemanen = view.findViewById(R.id.etBusukSubPemanen);
                AppCompatEditText etLewatMatangSubPemanen = view.findViewById(R.id.etLewatMatangSubPemanen);
                AppCompatEditText etAbnormalSubPemanen = view.findViewById(R.id.etAbnormalSubPemanen);
                AppCompatEditText etTangkaiPanjangSubPemanen = view.findViewById(R.id.etTangkaiPanjangSubPemanen);
                AppCompatEditText etBuahDimakanTikusSubPemanen = view.findViewById(R.id.etBuahDimakanTikusSubPemanen);
                AppCompatEditText etTotalBrondolanSubPemanen = view.findViewById(R.id.etTotalBrondolanSubPemanen);
                AppCompatEditText etTotalJanjangSubPemanen = view.findViewById(R.id.etTotalJanjangSubPemanen);
                AppCompatEditText etTotalPdtSubPemanen = view.findViewById(R.id.etTotalPdtSubPemanen);
                TextView tvValue = view.findViewById(R.id.tvValue);
                CompleteTextViewHelper lsTipe = view.findViewById(R.id.lsTipe);
                CompleteTextViewHelper etPemanen = view.findViewById(R.id.etPemanenSub);

                if (tvValue.getText().toString().isEmpty()) {
                    etPemanen.requestFocus();
                    Toast.makeText(HarvestApp.getContext(), getResources().getString(R.string.please_chose_pemanen), Toast.LENGTH_LONG).show();
                    return false;
                }else{
                    Pemanen pemanen = gson.fromJson(tvValue.getText().toString(),Pemanen.class);
                    kodePemanen.add(pemanen.getKodePemanen());
                }

                if (!etPersentase.getText().toString().isEmpty()) {
                    persentase += Integer.parseInt(etPersentase.getText().toString());
                    if (lsTipe.getText().toString().equals(SubPemanen.arrayCodeTipePemanen[SubPemanen.idOperatorTonase])) {
                        pesenOT += Integer.parseInt(etPersentase.getText().toString());
                    }
                }
                if (!etNormalSubPemanen.getText().toString().isEmpty()) {
                    hasilPanenSubPemane.setJanjangNormal(hasilPanenSubPemane.getJanjangNormal() + Integer.parseInt(etNormalSubPemanen.getText().toString()));
                }
                if (!etBuahMentahSubPemanen.getText().toString().isEmpty()) {
                    hasilPanenSubPemane.setBuahMentah(hasilPanenSubPemane.getBuahMentah() + Integer.parseInt(etBuahMentahSubPemanen.getText().toString()));
                }
                if (!etBusukSubPemanen.getText().toString().isEmpty()) {
                    hasilPanenSubPemane.setBusukNJangkos(hasilPanenSubPemane.getBusukNJangkos() + Integer.parseInt(etBusukSubPemanen.getText().toString()));
                }
                if (!etLewatMatangSubPemanen.getText().toString().isEmpty()) {
                    hasilPanenSubPemane.setBuahLewatMatang(hasilPanenSubPemane.getBuahLewatMatang() + Integer.parseInt(etLewatMatangSubPemanen.getText().toString()));
                }
                if (!etAbnormalSubPemanen.getText().toString().isEmpty()) {
                    hasilPanenSubPemane.setBuahAbnormal(hasilPanenSubPemane.getBuahAbnormal() + Integer.parseInt(etAbnormalSubPemanen.getText().toString()));
                }
                if (!etTangkaiPanjangSubPemanen.getText().toString().isEmpty()) {
                    hasilPanenSubPemane.setTangkaiPanjang(hasilPanenSubPemane.getTangkaiPanjang() + Integer.parseInt(etTangkaiPanjangSubPemanen.getText().toString()));
                }
                if (!etBuahDimakanTikusSubPemanen.getText().toString().isEmpty()) {
                    hasilPanenSubPemane.setBuahDimakanTikus(hasilPanenSubPemane.getBuahDimakanTikus() + Integer.parseInt(etBuahDimakanTikusSubPemanen.getText().toString()));
                }
                if (!etTotalJanjangSubPemanen.getText().toString().isEmpty()) {
                    hasilPanenSubPemane.setTotalJanjang(hasilPanenSubPemane.getTotalJanjang() + Integer.parseInt(etTotalJanjangSubPemanen.getText().toString()));
                }
                if (!etTotalBrondolanSubPemanen.getText().toString().isEmpty()) {
                    hasilPanenSubPemane.setBrondolan(hasilPanenSubPemane.getBrondolan() + Integer.parseInt(etTotalBrondolanSubPemanen.getText().toString()));
                }
                if (!etTotalPdtSubPemanen.getText().toString().isEmpty()) {
                    hasilPanenSubPemane.setTotalJanjangPendapatan(hasilPanenSubPemane.getTotalJanjangPendapatan() + Integer.parseInt(etTotalPdtSubPemanen.getText().toString()));
                }
            }else{
                SubPemanen subPemanen = subPemanens.get(i);
                if (subPemanen.getPemanen().getKodePemanen() == null) {
                    Toast.makeText(HarvestApp.getContext(), getResources().getString(R.string.please_chose_pemanen), Toast.LENGTH_LONG).show();
                    return false;
                }
                kodePemanen.add(subPemanen.getPemanen().getKodePemanen());
                persentase += subPemanen.getPersentase();
                hasilPanenSubPemane = subPemanen.getHasilPanen();
//                hasilPanenSubPemane.setTotalJanjang(subPemanen.getHasilPanen().getTotalJanjang());
//                hasilPanenSubPemane.setTotalJanjangPendapatan(subPemanen.getHasilPanen().getTotalJanjangPendapatan());
//                hasilPanenSubPemane.setBrondolan(subPemanen.getHasilPanen().getBrondolan());
//                totalJanjangSubPemanen += subPemanen.getJanjang();
//                totalBrondolanSubPemanen += subPemanen.getBrondolan();
//                totalJanjangPendapatanSubPemanen += subPemanen.getJanjangPendapatan();
            }

            if(persentase > 100){
                if(canRead) {
                    etPersentase.requestFocus();
                }
                Toast.makeText(HarvestApp.getContext(), "Persentase Lebih Dari 100", Toast.LENGTH_LONG).show();
                return false;
            }

            if(pesenOT >= 1){
                Toast.makeText(HarvestApp.getContext(), "Pastikan Tipe OT Hanya 0%", Toast.LENGTH_LONG).show();
                return false;
            }

            if(hasilPanenSubPemane.getJanjangNormal() > hasilPanenX.getJanjangNormal()){
                if(canRead) {
                    etPersentase.requestFocus();
                }
                Toast.makeText(HarvestApp.getContext(), "Janjang Normal Lebih Besar Dari "+ hasilPanenX.getJanjangNormal(), Toast.LENGTH_LONG).show();
                return false;
            }

            if(hasilPanenSubPemane.getBuahMentah() > hasilPanenX.getBuahMentah()){
                if(canRead) {
                    etPersentase.requestFocus();
                }
                Toast.makeText(HarvestApp.getContext(), "Buah Mentah Lebih Besar Dari "+ hasilPanenX.getBuahMentah(), Toast.LENGTH_LONG).show();
                return false;
            }

            if(hasilPanenSubPemane.getBusukNJangkos() > hasilPanenX.getBusukNJangkos()){
                if(canRead) {
                    etPersentase.requestFocus();
                }
                Toast.makeText(HarvestApp.getContext(), "Busuk dan Jangkos Lebih Besar Dari "+ hasilPanenX.getBusukNJangkos(), Toast.LENGTH_LONG).show();
                return false;
            }

            if(hasilPanenSubPemane.getBuahLewatMatang() > hasilPanenX.getBuahLewatMatang()){
                if(canRead) {
                    etPersentase.requestFocus();
                }
                Toast.makeText(HarvestApp.getContext(), "Buah Lewat Matang Lebih Besar Dari "+ hasilPanenX.getBuahLewatMatang(), Toast.LENGTH_LONG).show();
                return false;
            }

            if(hasilPanenSubPemane.getBuahAbnormal() > hasilPanenX.getBuahAbnormal()){
                if(canRead) {
                    etPersentase.requestFocus();
                }
                Toast.makeText(HarvestApp.getContext(), "Buah Abnormal Lebih Besar Dari "+ hasilPanenX.getBuahAbnormal(), Toast.LENGTH_LONG).show();
                return false;
            }

            if(hasilPanenSubPemane.getTangkaiPanjang() > hasilPanenX.getTangkaiPanjang()){
                if(canRead) {
                    etPersentase.requestFocus();
                }
                Toast.makeText(HarvestApp.getContext(), "Tangkai Panjang Lebih Besar Dari "+ hasilPanenX.getTangkaiPanjang(), Toast.LENGTH_LONG).show();
                return false;
            }

            if(hasilPanenSubPemane.getBuahDimakanTikus() > hasilPanenX.getBuahDimakanTikus()){
                if(canRead) {
                    etPersentase.requestFocus();
                }
                Toast.makeText(HarvestApp.getContext(), "Buah Dimakan Tikus Lebih Besar Dari "+ hasilPanenX.getBuahDimakanTikus(), Toast.LENGTH_LONG).show();
                return false;
            }

            if(hasilPanenSubPemane.getBrondolan() > hasilPanenX.getBrondolan()){
                if(canRead) {
                    etPersentase.requestFocus();
                }
                Toast.makeText(HarvestApp.getContext(), "Total Brondolan Lebih Besar Dari "+ hasilPanenX.getBrondolan(), Toast.LENGTH_LONG).show();
                return false;
            }

            if(hasilPanenSubPemane.getTotalJanjang() > hasilPanenX.getTotalJanjang()){
                if(canRead) {
                    etPersentase.requestFocus();
                }
                Toast.makeText(HarvestApp.getContext(), "Total Janjang Lebih Besar Dari "+ hasilPanenX.getTotalJanjang(), Toast.LENGTH_LONG).show();
                return false;
            }

            if(hasilPanenSubPemane.getTotalJanjangPendapatan() > hasilPanenX.getTotalJanjangPendapatan()){
                if(canRead) {
                    etPersentase.requestFocus();
                }
                Toast.makeText(HarvestApp.getContext(), "Total Janjang Lebih Besar Dari "+ hasilPanenX.getTotalJanjangPendapatan(), Toast.LENGTH_LONG).show();
                return false;
            }
        }

        if(pemanenSisa != null){
            boolean pemanenSisaMasuk = false;
            for (String kPemanen : kodePemanen){
                if(kPemanen.equalsIgnoreCase(pemanenSisa.getKodePemanen())){
                    pemanenSisaMasuk = true;
                    break;
                }
            }
            if(!pemanenSisaMasuk){
                Toast.makeText(HarvestApp.getContext(), "Pemanen Sisa Harus ada Di List Info Pemanen", Toast.LENGTH_LONG).show();
                return false;
            }
        }

        if(kodePemanen.size() != rvPemanen.getAdapter().getItemCount()){
            Toast.makeText(HarvestApp.getContext(), "Ada Pemanen Yang Dipilih 2 kali Mohon Di Cek", Toast.LENGTH_LONG).show();
            return false;
        }

        if(persentase != 100){
            if(etPersentase != null) {
                etPersentase.requestFocus();
            }
            Toast.makeText(HarvestApp.getContext(), "Persentase Tidak Sama Dengan 100", Toast.LENGTH_LONG).show();
            return false;
        }

        if(hasilPanenSubPemane.getTotalJanjang() != hasilPanenX.getTotalJanjang()){
            if(pemanenSisa == null) {
                etPemanenSisa.requestFocus();
                Toast.makeText(HarvestApp.getContext(), "Total Janjang Tidak Sama Dengan " + hasilPanenX.getTotalJanjang(), Toast.LENGTH_LONG).show();
                return false;
            }
        }

        if(hasilPanenSubPemane.getBrondolan() != hasilPanenX.getBrondolan()){
            if(pemanenSisa == null) {
                etPemanenSisa.requestFocus();
                Toast.makeText(HarvestApp.getContext(), "Total Brondolan Tidak Sama Dengan " + hasilPanenX.getBrondolan(), Toast.LENGTH_LONG).show();
                return false;
            }
        }

        return true;
    }

    private void takePicture(){
        if(qcMutuBuahActivity != null) {
            if(tphSelected != null && baseActivity.currentlocation != null) {
                if (GlobalHelper.distance(tphSelected.getLatitude(), baseActivity.currentlocation.getLatitude(),
                        tphSelected.getLongitude(),baseActivity.currentlocation.getLongitude()) > GlobalHelper.RADIUS){
                    Toast.makeText(HarvestApp.getContext(),getResources().getString(R.string.radius_to_long_with_tph),Toast.LENGTH_SHORT).show();
                    return;
                }
            }else{
                Toast.makeText(HarvestApp.getContext(),getResources().getString(R.string.chose_tph),Toast.LENGTH_SHORT).show();
            }
        }
        Gson gson  = new Gson();
        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.STAMP_IMAGES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(HarvestApp.STAMP_IMAGES,gson.toJson(stampImage));
        editor.apply();

        GlobalHelper.TAG_CAMERA = GlobalHelper.TAG_CAMERA_QC_MUTUBUAH;
        EasyImage.openCamera(getActivity(),1);
    }

    private void setMasterSpk(){
        if(allSpk == null || allSpk.size() == 0) {
            allSpk = GlobalHelper.getDataAllSPKPanen();
        }
    }

    private void mainQcMutuBuah(){

        etBlock.setFocusable(true);
        etNotph.setFocusable(true);
        etAncak.setFocusable(true);
        etTglPanen.setText(formatDate.format(System.currentTimeMillis()));

        if(!hidePemanen) {

            lInfoSubPemanen.setVisibility(View.VISIBLE);
            cbBorongan.setVisibility(View.VISIBLE);
            tipeKongsi.setVisibility(View.VISIBLE);
            rvPemanen.setVisibility(View.VISIBLE);
            lKraniInfo.setVisibility(View.VISIBLE);
            cbKraniSubstitute.setVisibility(View.VISIBLE);

            etBlock.setFocusable(false);
            etNotph.setFocusable(false);
            etAncak.setFocusable(false);

            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_BUAH);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("param1", selectedTransaksiPanen.getIdTPanen()));
            if (cursor.size() > 0) {
                if (selectedTransaksiPanen.getStatus() < 2) {
                    showYesNo("Apakah Anda Akan Melakukan Update Qc Buah pada Tph " + selectedTransaksiPanen.getTph().getNamaTph(), YesNo_UpdateQc);
                }else{
                    lsave.setVisibility(View.GONE);
                }
            }
            db.close();

            etTglPanen.setText(formatDate.format(selectedTransaksiPanen.getCreateDate()));

        }else{
            lInfoSubPemanen.setVisibility(View.GONE);
            cbBorongan.setVisibility(View.GONE);
            lBorongan.setVisibility(View.GONE);
            tipeKongsi.setVisibility(View.GONE);
            rvPemanen.setVisibility(View.GONE);
            lSisa.setVisibility(View.GONE);
            lKraniInfo.setVisibility(View.GONE);
            cbKraniSubstitute.setVisibility(View.GONE);
            lSubstitute.setVisibility(View.GONE);

            etBlock.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateNearestBlock();
                    etBlock.showDropDown();
                }
            });

            etNotph.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!etBlock.getText().toString().isEmpty()){
                        if (baseActivity.currentLocationOK()) {
                            latitude = baseActivity.currentlocation.getLatitude();
                            longitude = baseActivity.currentlocation.getLongitude();
//                                showmapTPH();
                            setDropdownTph();
                        } else {
                            if (qcMutuBuahActivity != null) {
//                                tphActivity.searchingGPS = Snackbar.make(tphActivity.myCoordinatorLayout, getResources().getString(R.string.search_gps), Snackbar.LENGTH_INDEFINITE);
//                                tphActivity.searchingGPS.show();
                                WidgetHelper.warningFindGps(qcMutuBuahActivity,qcMutuBuahActivity.myCoordinatorLayout);
                            }
                        }
                        etNotph.setText("");
                        etNotph.showDropDown();
                    }
                }
            });

            etNotph.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    adapterTph = (SelectTphAdapter) parent.getAdapter();
                    TPH tph = adapterTph.getItemAt(position);
                    if (baseActivity.currentLocationOK()) {
                        latitude = baseActivity.currentlocation.getLatitude();
                        longitude = baseActivity.currentlocation.getLongitude();

                        Double distance = GlobalHelper.distance(latitude,tph.getLatitude(),longitude,tph.getLongitude());
                        if(distance <= GlobalHelper.RADIUS) {
                            setTph(tph);
                        }else{
                            setDropdownTph();
                            Toast.makeText(HarvestApp.getContext(), getResources().getString(R.string.radius_to_long_with_tph), Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        if (qcMutuBuahActivity != null) {
                            WidgetHelper.warningFindGps(qcMutuBuahActivity,qcMutuBuahActivity.myCoordinatorLayout);
                        }
                    }
                }
            });

            etNotph.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(!hasFocus){
                        setTph(tphSelected);
                    }
                }
            });

            etBlock.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    etNotph.setText("");
                    blockSelected = etBlock.getText().toString();
                    blockName = etBlock.getText().toString();
                    if (baseActivity.currentLocationOK()) {
                        cariTphTerdekat = false;
                        latitude = baseActivity.currentlocation.getLatitude();
                        longitude = baseActivity.currentlocation.getLongitude();
                        setDropdownTph();
                    } else {
                        if (qcMutuBuahActivity != null) {
                            WidgetHelper.warningFindGps(qcMutuBuahActivity,qcMutuBuahActivity.myCoordinatorLayout);
                        }
                    }

                }
            });
            etBlock.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(!hasFocus){
                        etBlock.setText(blockSelected == "" || blockSelected == null ? "" : blockSelected);
                        blockName = etBlock.getText().toString();
                    }
                }
            });

            cbTphBayangan.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (checkBoxTphBayanganEfect) {
                        if (isChecked) {
                            if (baseActivity.currentLocationOK()) {
                                if (!etBlock.getText().toString().isEmpty()) {
                                    lTph.setVisibility(View.GONE);
                                    lAncak.setVisibility(View.GONE);
                                    tphSelected = tphHelper.getTphBayangan(GlobalHelper.getEstate().getEstCode(), etBlock.getText().toString());
                                    if(tphSelected != null) {
                                        tphSelected.setLatitude(baseActivity.currentlocation.getLatitude());
                                        tphSelected.setLongitude(baseActivity.currentlocation.getLongitude());
                                        etTotalJanjang.requestFocus();
                                        cbTphBayangan.setChecked(isChecked);
                                        if(stampImage != null) {
                                            stampImage.setTph(tphSelected);
                                        }else{
                                            stampImage = new StampImage();
                                            stampImage.setTph(tphSelected);
                                        }
                                    }else{
                                        lTph.setVisibility(View.VISIBLE);
                                        cbTphBayangan.setChecked(false);

                                        etNotph.requestFocus();
                                        Toast.makeText(HarvestApp.getContext(), getResources().getString(R.string.tph_imaginary_not_found), Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    lTph.setVisibility(View.VISIBLE);
                                    cbTphBayangan.setChecked(false);

                                    etBlock.requestFocus();
                                    Toast.makeText(HarvestApp.getContext(), getResources().getString(R.string.please_chose_block), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                lTph.setVisibility(View.VISIBLE);
                                cbTphBayangan.setChecked(false);

                                WidgetHelper.warningFindGps(qcMutuBuahActivity,qcMutuBuahActivity.myCoordinatorLayout);
                            }
                        } else {
                            lTph.setVisibility(View.VISIBLE);
                            setTph(null);
                        }
                    }
                }
            });
        }
    }



    /*
     * list param
     * stat :
     *  1. YesNo_Simpan = 10
     *  2. YesNo_SimpanT2 = 11
     *  3. YEsNo_Hapus = 12
     *  4. YesNo_HapusT2 = 13
     *  5. YesNo_Batal = 14
     *  6. YesNo_SimpanQc = 15
     *  7. YesNo_HapusQc = 16
     *  8. YesNo_UpdateQc = 17
     */
    public void showYesNo(String text,int stat){
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.yes_no, null);
        TextView tvDialogKet = (TextView) dialogView.findViewById(R.id.ket1);
        SlideView btnCancelDialog = (SlideView) dialogView.findViewById(R.id.btnCancel);
        SlideView btnOkDialog = (SlideView) dialogView.findViewById(R.id.btnOK);

        final AlertDialog dialog = new AlertDialog.Builder(getActivity(), R.style.MyAlertDialogStyle)
                .setView(dialogView)
                .setCancelable(false)
                .create();

        final ForegroundColorSpan fcs = new ForegroundColorSpan(getActivity().getResources().getColor(R.color.Red));
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);

        tvDialogKet.setText(text);
        btnOkDialog.setText("Yes");
        btnCancelDialog.setText("No");

        btnCancelDialog.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(SlideView slideView) {
                switch (stat) {
                    case YesNo_UpdateQc:
                        qcMutuBuahActivity.backProses();
                        dialog.dismiss();
                        break;

                    case YesNo_Pilih_TPH:{
                        etBlock.requestFocus();
                        etBlock.setText("");
                        etNotph.setText("");
                        tphSelected = new TPH();
                        dialog.dismiss();
                        break;
                    }
                    case YesNo_Batal:{
                        dialog.dismiss();
                        break;
                    }
                    default:
                        dialog.dismiss();
                        break;
                }
            }
        });
        btnOkDialog.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(SlideView slideView) {
                switch (stat){
                    case YesNo_SimpanQc:{
                        new LongOperation(getView()).execute(String.valueOf(LongOperation_Save_Data_Qc));
                        break;
                    }
                    case YesNo_HapusQc:{
                        new LongOperation(getView()).execute(String.valueOf(LongOperation_Delet_Data_Qc));
                        break;
                    }
                    case YesNo_UpdateQc:{
                        dialog.dismiss();
                        break;
                    }
                    case YesNo_Pilih_TPH:{
                        dialog.dismiss();
                        break;
                    }
                    case YesNo_Batal:{
                        qcMutuBuahActivity.backProses();
                        dialog.dismiss();
                        break;
                    }
                }

                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private boolean validasiSaveTransaksiPanen(){
        //validasi untuk tph aktivity
        if (qcMutuBuahActivity != null){
            if(baseActivity.currentLocationOK()){
                longitude = baseActivity.currentlocation.getLongitude();
                latitude = baseActivity.currentlocation.getLatitude();
                //jika dari nfc biru ada maka cek apa kah jarak lokasi sekarang denggan tph masih sesuai toleransi
                //jika dari nfc hijau ada maka cek apa kah jarak lokasi sekarang denggan tph langsiran masih sesuai toleransi
                if(selectedTransaksiPanen != null){
                    double distance = GlobalHelper.distance(selectedTransaksiPanen.getTph().getLatitude(),latitude,
                            selectedTransaksiPanen.getTph().getLongitude(),longitude);
                    if(distance > GlobalHelper.RADIUS){
                        WidgetHelper.warningRadiusTph(qcMutuBuahActivity,qcMutuBuahActivity.myCoordinatorLayout,distance);
//                        Toast.makeText(HarvestApp.getContext(),getResources().getString(R.string.radius_to_long_with_tph),Toast.LENGTH_SHORT).show();
                        return false;
                    }
                }
            }else{
                WidgetHelper.warningFindGps(qcMutuBuahActivity,qcMutuBuahActivity.myCoordinatorLayout);
//                qcMutuBuahActivity.searchingGPS = Snackbar.make(qcMutuBuahActivity.myCoordinatorLayout, getResources().getString(R.string.search_gps), Snackbar.LENGTH_INDEFINITE);
//                qcMutuBuahActivity.searchingGPS.show();
                return false;
            }
        }

        if(waktuPanen == null){
            Toast.makeText(HarvestApp.getContext(), "Mohon Tentukan Waktu Panen Dahulu", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(etNormal.getText().toString().equals("")){
            Toast.makeText(HarvestApp.getContext(), "Mohon Input Janjang Normal", Toast.LENGTH_SHORT).show();
            return false;
        }else{
            Integer normal = Integer.parseInt(etNormal.getText().toString());
            if(normal < 1 ){
                Toast.makeText(HarvestApp.getContext(), "Janjang Normal "+getActivity().getResources().getString(R.string.canot_nol), Toast.LENGTH_SHORT).show();
                return false;
            }
        }

        return true;
    }

    private void hapusData(TransaksiPanen transaksiPanen){
//        if(tphActivity.tulisUlangNFC) {
//            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_TPH);
//            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
//            Gson gson = new Gson();
//            transaksiPanen.setStatus(TransaksiPanen.DELETED);
//            DataNitrit dataNitrit = new DataNitrit(transaksiPanen.getIdTPanen(),
//                    gson.toJson(transaksiPanen),
//                    transaksiPanen.getTph().getNoTph(),
//                    transaksiPanen.getPemanen().getKodePemanen()
//            );
//            repository.update(dataNitrit);
//            db.close();
//            tphActivity.backProses();
//        }
    }

    private void saveTransaksiPanenQc() {
        HashSet<String> sFoto = new HashSet<>();
        if (ALselectedImage.size() > 0) {
            for (File file : ALselectedImage) {
                sFoto.add(file.getAbsolutePath());
            }
        }

        String idTransaksi = null;
        String idQcMutubuah = null;

        if (!hidePemanen) {
            idTransaksi = selectedTransaksiPanen.getIdTPanen();
            idQcMutubuah = "QC1_" + GlobalHelper.getUser().getUserID() + "_" + idTransaksi;
        } else {
            if (idQcMutubuahSelected == null) {
                SimpleDateFormat sdf = new SimpleDateFormat("ddMMyy");
                idQcMutubuah = "QC1_" + GlobalHelper.getUser().getUserID() + "_" + "QCH" + String.format("%03d", GlobalHelper.getCountNumber(GlobalHelper.LIST_FOLDER_SELECTED_QC_MUTUBUAH))
                        + sdf.format(System.currentTimeMillis()) + GlobalHelper.getUser().getUserID();
            } else {
                idQcMutubuah = idQcMutubuahSelected;
            }
        }

        Boolean alas = true;
        if (lsAlasBrondol.getText().toString().equalsIgnoreCase("no")) {
            alas = false;
        }
        HasilPanen hasilPanen = new HasilPanen(
                Integer.parseInt(etNormal.getText().toString().isEmpty() ? "0" : etNormal.getText().toString()),
                Integer.parseInt(etMentah.getText().toString().isEmpty() ? "0" : etMentah.getText().toString()),
                Integer.parseInt(etBusuk.getText().toString().isEmpty() ? "0" : etBusuk.getText().toString()),
                Integer.parseInt(etLewatMatang.getText().toString().isEmpty() ? "0" : etLewatMatang.getText().toString()),
                Integer.parseInt(etAbnormal.getText().toString().isEmpty() ? "0" : etAbnormal.getText().toString()),
                Integer.parseInt(etTangkaiPanjang.getText().toString().isEmpty() ? "0" : etTangkaiPanjang.getText().toString()),
                Integer.parseInt(etBuahDimakanTikus.getText().toString().isEmpty() ? "0" : etBuahDimakanTikus.getText().toString()),
                Integer.parseInt(etBrondolan.getText().toString().isEmpty() ? "0" : etBrondolan.getText().toString()),
                totalJjgJelek,
                totalJjg,
                totalJjgPendapatan,
                waktuPanen,
                alas
        );

        int Status = TransaksiPanen.TAP;
        Kongsi kongsi = null;

        if (!hidePemanen) {
            ArrayList<SubPemanen> subPemanens = new ArrayList<>();
            for (int i = 0; i < rvPemanen.getAdapter().getItemCount(); i++) {
                RecyclerView.ViewHolder viewHolder = rvPemanen.findViewHolderForAdapterPosition(i);
                View view = viewHolder.itemView;
                TextView tvValue = view.findViewById(R.id.tvValue);
                AppCompatEditText etPersentase = view.findViewById(R.id.etPersentase);
                AppCompatEditText etNormalSubPemanen = view.findViewById(R.id.etNormalSubPemanen);
                AppCompatEditText etBuahMentahSubPemanen = view.findViewById(R.id.etBuahMentahSubPemanen);
                AppCompatEditText etBusukSubPemanen = view.findViewById(R.id.etBusukSubPemanen);
                AppCompatEditText etLewatMatangSubPemanen = view.findViewById(R.id.etLewatMatangSubPemanen);
                AppCompatEditText etAbnormalSubPemanen = view.findViewById(R.id.etAbnormalSubPemanen);
                AppCompatEditText etTangkaiPanjangSubPemanen = view.findViewById(R.id.etTangkaiPanjangSubPemanen);
                AppCompatEditText etBuahDimakanTikusSubPemanen = view.findViewById(R.id.etBuahDimakanTikusSubPemanen);
                AppCompatEditText etTotalJanjangSubPemanen = view.findViewById(R.id.etTotalJanjangSubPemanen);
                AppCompatEditText etTotalBrondolanSubPemanen = view.findViewById(R.id.etTotalBrondolanSubPemanen);
                AppCompatEditText etTotalPdtSubPemanen = view.findViewById(R.id.etTotalPdtSubPemanen);
                CompleteTextViewHelper lsTipe = view.findViewById(R.id.lsTipe);

                Gson gson = new Gson();
//            int jjg = Integer.parseInt(etTotalJanjangSubPemanen.getText().toString());
//            int brondolan = Integer.parseInt(etTotalBrondolanSubPemanen.getText().toString());
//            int jjgPdt = Integer.parseInt(etTotalPdtSubPemanen.getText().toString());
//            int buahMentah = Integer.parseInt(etBuahMentahSubPemanen.getText().toString());
//            int tangkaiPanjang = Integer.parseInt(etTangkaiPanjangSubPemanen.getText().toString());
                HasilPanen hasilPanenSubPemanen = new HasilPanen(
                        Integer.parseInt(etNormalSubPemanen.getText().toString()),
                        Integer.parseInt(etBuahMentahSubPemanen.getText().toString()),
                        Integer.parseInt(etBusukSubPemanen.getText().toString()),
                        Integer.parseInt(etLewatMatangSubPemanen.getText().toString()),
                        Integer.parseInt(etAbnormalSubPemanen.getText().toString()),
                        Integer.parseInt(etTangkaiPanjangSubPemanen.getText().toString()),
                        Integer.parseInt(etBuahDimakanTikusSubPemanen.getText().toString()),
                        Integer.parseInt(etTotalBrondolanSubPemanen.getText().toString()),
                        0,
                        Integer.parseInt(etTotalJanjangSubPemanen.getText().toString()),
                        Integer.parseInt(etTotalPdtSubPemanen.getText().toString())
                );
                Pemanen pemanen = gson.fromJson(tvValue.getText().toString(), Pemanen.class);

                if (pemanenSisa != null) {
                    if (pemanen.getKodePemanen().equalsIgnoreCase(pemanenSisa.getKodePemanen())) {
                        hasilPanenSubPemanen = transaksiPanenHelper.combineHasilPanen(hasilPanenSubPemanen, sisaJjgBrdl);
//                    jjg += sisaJjgBrdl.getTotalJanjang();
//                    brondolan += sisaJjgBrdl.getBrondolan();
//                    jjgPdt += sisaJjgBrdl.getTotalJanjangPendapatan();
//                    buahMentah += sisaJjgBrdl.getBuahMentah();
//                    tangkaiPanjang += sisaJjgBrdl.getTangkaiPanjang();
                    }
                }

                int tipe = SubPemanen.idPemnaen;
                if (lsTipe.getText().toString().equals(SubPemanen.arrayCodeTipePemanen[SubPemanen.idOperator])) {
                    tipe = SubPemanen.idOperator;
                }

                subPemanens.add(new SubPemanen(i,
                        GlobalHelper.getCharForNumber(i),
                        Integer.parseInt(etPersentase.getText().toString()),
                        hasilPanenSubPemanen,
                        tipe,
                        pemanen)
                );

                if (i == 0) {
                    pemanenSelected = gson.fromJson(tvValue.getText().toString(), Pemanen.class);
                }
            }

            kongsi = new Kongsi(idKongsi, subPemanens);
        }

        TransaksiPanen transaksiPanen = new TransaksiPanen(idTransaksi,
                latitude,
                longitude,
                GlobalHelper.getUser().getUserID(),
                System.currentTimeMillis(),
                GlobalHelper.getUser().getUserID(),
                System.currentTimeMillis(),
                tphSelected.getAncak(),
                "",
                pemanenSelected,
                tphSelected,
                hasilPanen,
                sFoto,
                Status,
                0,
                kongsi,
                spkSelected);

        if (selectedTransaksiPanen != null) {
            if (selectedTransaksiPanen.getIdNfc() != null) {
                transaksiPanen.setIdNfc(selectedTransaksiPanen.getIdNfc());
            }
        }else{
            selectedTransaksiPanen =  transaksiPanen;
        }

        QcMutuBuah qcMutuBuah = new QcMutuBuah(
                idQcMutubuah,
                idTransaksi,
                selectedTransaksiPanen,transaksiPanen);

        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_BUAH);

        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", qcMutuBuah.getIdQCMutuBuah()));
        Gson gson = new Gson();
        DataNitrit dataNitrit = new DataNitrit(qcMutuBuah.getIdQCMutuBuah(),gson.toJson(qcMutuBuah),qcMutuBuah.getIdTPanen());
        if(cursor.size() > 0) {
            repository.update(dataNitrit);
        }else{
            repository.insert(dataNitrit);
        }
        db.close();
        selectedTransaksiPanen = transaksiPanen;

        if(hidePemanen && idQcMutubuahSelected == null){
            GlobalHelper.setCountNumberPlusOne(GlobalHelper.LIST_FOLDER_SELECTED_QC_MUTUBUAH);
        }

        qcMutuBuahActivity.backProses();
    }

    private void hapusDataQc(TransaksiPanen transaksiPanen){
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_BUAH);
        String idQcBuah = "QC1_"+ GlobalHelper.getUser().getUserID()+"_"+ transaksiPanen.getIdTPanen();
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", idQcBuah));
        if(cursor.size() > 0){
            for (Iterator iterator = cursor.iterator(); iterator.hasNext();) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                repository.remove(dataNitrit);
                break;
            }
        }
        db.close();
        qcMutuBuahActivity.backProses();
    }

    private void setTotalJjgJelek(boolean withEditText){
        totalJjgJelek = Integer.parseInt(etMentah.getText().toString().isEmpty() ? "0" : etMentah.getText().toString()) +
                Integer.parseInt(etLewatMatang.getText().toString().isEmpty() ? "0" : etLewatMatang.getText().toString()) +
//                Integer.parseInt(etBusuk.getText().toString().isEmpty() ? "0" : etBusuk.getText().toString()) +
                Integer.parseInt(etTangkaiPanjang.getText().toString().isEmpty() ? "0" : etTangkaiPanjang.getText().toString()) +
                Integer.parseInt(etAbnormal.getText().toString().isEmpty() ? "0" : etAbnormal.getText().toString());
        if (withEditText) {
            etTotalJanjangBuruk.setText(String.valueOf(totalJjgJelek));
        }
    }

    private void setTotalJjg(boolean withEditText) {
        totalJjg = GlobalHelper.getTotalJanjang(
                    new HasilPanen(
                            Integer.parseInt(etNormal.getText().toString().isEmpty() ? "0" : etNormal.getText().toString()),
                            Integer.parseInt(etMentah.getText().toString().isEmpty() ? "0" : etMentah.getText().toString()),
                            Integer.parseInt(etBusuk.getText().toString().isEmpty() ? "0" : etBusuk.getText().toString()),
                            Integer.parseInt(etLewatMatang.getText().toString().isEmpty() ? "0" : etLewatMatang.getText().toString()),
                            Integer.parseInt(etAbnormal.getText().toString().isEmpty() ? "0" : etAbnormal.getText().toString()),
                            Integer.parseInt(etTangkaiPanjang.getText().toString().isEmpty() ? "0" : etTangkaiPanjang.getText().toString()),
                            Integer.parseInt(etBuahDimakanTikus.getText().toString().isEmpty() ? "0" : etBuahDimakanTikus.getText().toString()),
                            Integer.parseInt(etTotalJanjang.getText().toString().isEmpty() ? "0" : etTotalJanjang.getText().toString()),
                            Integer.parseInt(etTotalJanjangPendapatan.getText().toString().isEmpty() ? "0" : etTotalJanjangPendapatan.getText().toString()),
                            Integer.parseInt(etBrondolan.getText().toString().isEmpty() ? "0" : etBrondolan.getText().toString()),
                            lsAlasBrondol.getText().toString().equalsIgnoreCase("no") ? false : true
                    )
                );
        if (withEditText) {
            etTotalJanjang.setText(String.valueOf(totalJjg));
        }
    }

    private void setJjgNormal(boolean withEditText) {
         int jjgT = GlobalHelper.getTotalJanjang(
                new HasilPanen(
                        0,
                        Integer.parseInt(etMentah.getText().toString().isEmpty() ? "0" : etMentah.getText().toString()),
                        Integer.parseInt(etBusuk.getText().toString().isEmpty() ? "0" : etBusuk.getText().toString()),
                        Integer.parseInt(etLewatMatang.getText().toString().isEmpty() ? "0" : etLewatMatang.getText().toString()),
                        Integer.parseInt(etAbnormal.getText().toString().isEmpty() ? "0" : etAbnormal.getText().toString()),
                        Integer.parseInt(etTangkaiPanjang.getText().toString().isEmpty() ? "0" : etTangkaiPanjang.getText().toString()),
                        Integer.parseInt(etBuahDimakanTikus.getText().toString().isEmpty() ? "0" : etBuahDimakanTikus.getText().toString()),
                        Integer.parseInt(etTotalJanjang.getText().toString().isEmpty() ? "0" : etTotalJanjang.getText().toString()),
                        Integer.parseInt(etTotalJanjangPendapatan.getText().toString().isEmpty() ? "0" : etTotalJanjangPendapatan.getText().toString()),
                        Integer.parseInt(etBrondolan.getText().toString().isEmpty() ? "0" : etBrondolan.getText().toString()),
                        lsAlasBrondol.getText().toString().equalsIgnoreCase("no") ? false : true
                )
        );
//        int jjgT = GlobalHelper.getTotalJanjangPendapatan(new HasilPanen(
//                        0,
//                        Integer.parseInt(etMentah.getText().toString().isEmpty() ? "0" : etMentah.getText().toString()),
//                        Integer.parseInt(etBusuk.getText().toString().isEmpty() ? "0" : etBusuk.getText().toString()),
//                        Integer.parseInt(etLewatMatang.getText().toString().isEmpty() ? "0" : etLewatMatang.getText().toString()),
//                        Integer.parseInt(etAbnormal.getText().toString().isEmpty() ? "0" : etAbnormal.getText().toString()),
//                        Integer.parseInt(etTangkaiPanjang.getText().toString().isEmpty() ? "0" : etTangkaiPanjang.getText().toString()),
//                        Integer.parseInt(etBuahDimakanTikus.getText().toString().isEmpty() ? "0" : etBuahDimakanTikus.getText().toString()),
//                        Integer.parseInt(etTotalJanjang.getText().toString().isEmpty() ? "0" : etTotalJanjang.getText().toString()),
//                        Integer.parseInt(etTotalJanjangPendapatan.getText().toString().isEmpty() ? "0" : etTotalJanjangPendapatan.getText().toString()),
//                        Integer.parseInt(etBrondolan.getText().toString().isEmpty() ? "0" : etBrondolan.getText().toString()),
//                        lsAlasBrondol.getText().toString().equalsIgnoreCase("no") ? false : true
//                ),dynamicParameterPenghasilan
//        );
        totalJjg = Integer.parseInt(etTotalJanjang.getText().toString().isEmpty() ? "0" : etTotalJanjang.getText().toString());

        if (withEditText) {
//            if(total == 0){
//                etMentah.setText("0");
//                etBusuk.setText("0");
//                etLewatMatang.setText("0");
//                etAbnormal.setText("0");7
//                etTangkaiPanjang.setText("0");
//                etBuahDimakanTikus.setText("0");
//                etTotalJanjang.setText("0");
//                etTotalJanjangPendapatan.setText("0");
//            }
            etNormal.setText(String.valueOf(totalJjg - jjgT));
        }
    }

    private void setTotalJjgPendapatan(boolean withEditText) {

        totalJjgPendapatan = GlobalHelper.getTotalJanjangPendapatan(new HasilPanen(
                Integer.parseInt(etNormal.getText().toString().isEmpty() ? "0" : etNormal.getText().toString()),
                Integer.parseInt(etMentah.getText().toString().isEmpty() ? "0" : etMentah.getText().toString()),
                Integer.parseInt(etBusuk.getText().toString().isEmpty() ? "0" : etBusuk.getText().toString()),
                Integer.parseInt(etLewatMatang.getText().toString().isEmpty() ? "0" : etLewatMatang.getText().toString()),
                Integer.parseInt(etAbnormal.getText().toString().isEmpty() ? "0" : etAbnormal.getText().toString()),
                Integer.parseInt(etTangkaiPanjang.getText().toString().isEmpty() ? "0" : etTangkaiPanjang.getText().toString()),
                Integer.parseInt(etBuahDimakanTikus.getText().toString().isEmpty() ? "0" : etBuahDimakanTikus.getText().toString()),
                Integer.parseInt(etTotalJanjang.getText().toString().isEmpty() ? "0" : etTotalJanjang.getText().toString()),
                Integer.parseInt(etTotalJanjangPendapatan.getText().toString().isEmpty() ? "0" : etTotalJanjangPendapatan.getText().toString()),
                Integer.parseInt(etBrondolan.getText().toString().isEmpty() ? "0" : etBrondolan.getText().toString()),
                lsAlasBrondol.getText().toString().equalsIgnoreCase("no") ? false : true
                ),dynamicParameterPenghasilan
        );
        if (withEditText) {
            etTotalJanjangPendapatan.setText(String.valueOf(totalJjgPendapatan));
        }
    }

    private void infoPanenPopUp(int param){
        String message = "";
        switch (param){
            case infoTotalJanjang:
                message = getActivity().getResources().getString(R.string.info_total_janjang);
                break;
            case infoTotalJanjangPendapatan:
                message = getActivity().getResources().getString(R.string.info_total_janjang_pendapatan);
                break;
            case infoNormal:
                message = getActivity().getResources().getString(R.string.info_tbs_normal);
                break;
            case infoMentah:
                message = getActivity().getResources().getString(R.string.info_tbs_mentah);
                break;
            case infoBusuk:
                message = getActivity().getResources().getString(R.string.info_tbs_busuk);
                break;
            case infoLewatMatang:
                message = getActivity().getResources().getString(R.string.info_tbs_lewat_matang);
                break;
            case infoAbnormal:
                message = getActivity().getResources().getString(R.string.info_tbs_abnormal);
                break;
            case infoTangkaiPanjang:
                message = getActivity().getResources().getString(R.string.info_tbs_tangkai);
                break;
            case infoBuahDimakanTikus:
                message = getActivity().getResources().getString(R.string.info_tbs_bdt);
                break;
        }
        WidgetHelper.showOKDialog(getActivity(),
                "Info",
                message,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }
        );
    }

    private void setUpValueTransaksiPanen(){
        latitude = selectedTransaksiPanen.getLatitude();
        longitude = selectedTransaksiPanen.getLongitude();
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd MMM yyyy");
        String alas;
        waktuPanen = selectedTransaksiPanen.getHasilPanen().getWaktuPanen();
        etTglPanen.setText(formatDate.format(waktuPanen));
        if(selectedTransaksiPanen.getHasilPanen().isAlasanBrondolan()){
            alas = "Yes";
        }else{
            alas = "No";
        }

        ALselectedImage.clear();
        if(selectedTransaksiPanen.getFoto() != null) {
            for (String s : selectedTransaksiPanen.getFoto()) {
                File file = new File(s);
                ALselectedImage.add(file);
            }
        }
        setupimageslide();

//        List<String> lGang = new ArrayList<>();
//        lGang.add(selectedTransaksiPanen.getPemanen().getGank());
//        ArrayAdapter<String> adapterGang = new ArrayAdapter<String>(getActivity(),
//                android.R.layout.simple_dropdown_item_1line, lGang);
//        etGang.setAdapter(adapterGang);
//        etGang.setText(lGang.get(0));

        checkBoxTphBayanganEfect = false;

        if(!hidePemanen) {
            if (selectedTransaksiPanen.getPemanen() != null) {
                setPemanen(selectedTransaksiPanen.getPemanen());
            } else if (selectedTransaksiPanen.getKongsi().getSubPemanens() != null) {
                if (selectedTransaksiPanen.getKongsi().getSubPemanens().size() > 0) {
                    setPemanen(selectedTransaksiPanen.getKongsi().getSubPemanens().get(0).getPemanen());
                }
            }
        }

        setTph(selectedTransaksiPanen.getTph());
        etAncak.setText(selectedTransaksiPanen.getAncak());
        checkBoxTphBayanganEfect = true;
//        etBaris.setText(selectedTransaksiPanen.getBaris());

//        etTotalJanjang.setText(String.valueOf(selectedTransaksiPanen.getHasilPanen().getTotalJanjang()));
//        etTotalJanjangPendapatan.setText(String.valueOf(selectedTransaksiPanen.getHasilPanen().getTotalJanjangPendapatan()));
        etNormal.setText(String.valueOf(selectedTransaksiPanen.getHasilPanen().getJanjangNormal()));
        etMentah.setText(String.valueOf(selectedTransaksiPanen.getHasilPanen().getBuahMentah()));
        etBusuk.setText(String.valueOf(selectedTransaksiPanen.getHasilPanen().getBusukNJangkos()));
        etLewatMatang.setText(String.valueOf(selectedTransaksiPanen.getHasilPanen().getBuahLewatMatang()));
        etAbnormal.setText(String.valueOf(selectedTransaksiPanen.getHasilPanen().getBuahAbnormal()));
        etTangkaiPanjang.setText(String.valueOf(selectedTransaksiPanen.getHasilPanen().getTangkaiPanjang()));
        etBuahDimakanTikus.setText(String.valueOf(selectedTransaksiPanen.getHasilPanen().getBuahDimakanTikus()));
        etBrondolan.setText(String.valueOf(selectedTransaksiPanen.getHasilPanen().getBrondolan()));
        etTotalJanjangBuruk.setText(String.valueOf(selectedTransaksiPanen.getHasilPanen().getTotalJanjangBuruk()));
        etJarak.setText(sdfT.format(waktuPanen));
        lsAlasBrondol.setText(alas);

        if(!hidePemanen) {
            idKongsi = selectedTransaksiPanen.getKongsi().getTipeKongsi();
            tipeKongsi.setPosition(idKongsi);
            sharedPrefHelper.commit(SharedPrefHelper.KEY_TipeKongsi, String.valueOf(idKongsi));
            pemanenOperator = new HashMap<>();
            pemanenOperatorTonase = new HashMap<>();
            lSisa.setVisibility(View.GONE);

            for (int i = 0; i < selectedTransaksiPanen.getKongsi().getSubPemanens().size(); i++) {
                SubPemanen subPemanen = selectedTransaksiPanen.getKongsi().getSubPemanens().get(i);
                if (subPemanen.getTipePemanen() == SubPemanen.idOperator) {
                    pemanenOperator.put(subPemanen.getPemanen().getKodePemanen(), subPemanen.getPemanen());
                } else if (subPemanen.getTipePemanen() == SubPemanen.idOperatorTonase) {
                    pemanenOperatorTonase.put(subPemanen.getPemanen().getKodePemanen(), subPemanen.getPemanen());
                }
            }

            spkSelected = selectedTransaksiPanen.getSpk();
            if (spkSelected != null) {
                cbBorongan.setChecked(true);
                lBorongan.setVisibility(View.VISIBLE);
                etSpk.setText(spkSelected.getIdSPK());
            }
        }

        if(selectedTransaksiPanen.getSubstitute() != null){
//            substituteSelected = selectedTransaksiPanen.getSubstitute();
//            etSubstitute.setText(substituteSelected.getNama());
            cbKraniSubstitute.setChecked(true);
            lSubstitute.setVisibility(View.VISIBLE);
        }else{
//            substituteSelected = null;
            etSubstitute.setText("");
            cbKraniSubstitute.setChecked(false);
            lSubstitute.setVisibility(View.GONE);
        }

        setTotalJjg(true);
        setTotalJjgPendapatan(true);

        if(selectedTransaksiPanen.getStatus() == TransaksiPanen.UPLOAD){
            lsave.setVisibility(View.GONE);
        }
    }

    private void setNearestBlock(){
//        if(tphActivity != null && selectedTransaksiPanen == null ){
//            if (tphActivity.nearestBlock != null) {
////                latitude = tphActivity.nearestBlock.getLocation().getLatitude();
////                longitude = tphActivity.nearestBlock.getLocation().getLongitude();
//
//                ArrayAdapter<String> adapterBlock = new ArrayAdapter<String>
//                        (getActivity(),android.R.layout.select_dialog_item, tphActivity.nearestBlock.getBlocks().toArray(new String[tphActivity.nearestBlock.getBlocks().size()]));
//
//                etBlock.setThreshold(1);
//                etBlock.setAdapter(adapterBlock);
//                etBlock.setText(tphActivity.nearestBlock.getBlock());
//
//                baseActivity.currentlocation = tphActivity.nearestBlock.getLocation();
////                setDropdownTph();
//            } else {
//                if(tphActivity != null) {
//                    WidgetHelper.warningFindGps(tphActivity,tphActivity.myCoordinatorLayout);
//                }
//            }
//        }
    }

    private void updateNearestBlock(){
        if(qcMutuBuahActivity != null && hidePemanen ){
            if (baseActivity.currentLocationOK()) {
                latitude = baseActivity.currentlocation.getLatitude();
                longitude = baseActivity.currentlocation.getLongitude();

                Set<String> allBlock = GlobalHelper.getNearBlock(kml,latitude,longitude);
                Set<String> bloks = new ArraySet<>();
                for(String est : allBlock){
                    try {
                        String [] split = est.split(";");
                        bloks.add(split[2]);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
                ArrayAdapter<String> adapterBlock = new ArrayAdapter<String>
                        (getActivity(),android.R.layout.select_dialog_item, bloks.toArray(new String[bloks.size()]));

                etBlock.setThreshold(1);
                etBlock.setAdapter(adapterBlock);
                try {
                    String [] estAfdBlock = GlobalHelper.getAndAssignBlock(kml,latitude,longitude).split(";");
                    etBlock.setText(estAfdBlock[2]);
//                    setDropdownTph();
                }catch (Exception e){
                    e.printStackTrace();
                }
            } else {
                if(qcMutuBuahActivity != null) {
                    WidgetHelper.warningFindGps(qcMutuBuahActivity,qcMutuBuahActivity.myCoordinatorLayout);
                }
            }
        }
    }

    public void setPemanen(Pemanen pemanen){
//        etPemanen.setText(pemanen.getNama());
        pemanenSelected = pemanen;
        if(stampImage == null){
            stampImage = new StampImage();
        }
        stampImage.setPemanen(pemanen);
    }

//    private void takePicture(){
//        if(qcMutuBuahActivity != null) {
//            if(tphSelected != null && baseActivity.currentlocation != null) {
//                if (GlobalHelper.distance(tphSelected.getLatitude(), baseActivity.currentlocation.getLatitude(),
//                        tphSelected.getLongitude(),baseActivity.currentlocation.getLongitude()) > GlobalHelper.RADIUS){
//                    Toast.makeText(HarvestApp.getContext(),getResources().getString(R.string.radius_to_long_with_tph),Toast.LENGTH_SHORT).show();
//                    return;
//                }
//            }else{
//                Toast.makeText(HarvestApp.getContext(),getResources().getString(R.string.chose_tph),Toast.LENGTH_SHORT).show();
//            }
//        }
//        Gson gson  = new Gson();
//        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.STAMP_IMAGES, Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = preferences.edit();
//        editor.putString(HarvestApp.STAMP_IMAGES,gson.toJson(stampImage));
//        editor.apply();
//
//        GlobalHelper.TAG_CAMERA = GlobalHelper.TAG_CAMERA_TRANSAKSI_TPH;
//        EasyImage.openCamera(getActivity(),1);
//    }

//    private void showmapTPH(){
//        popupTPH = true;
//        new LongOperation(getView()).execute();
//    }

    private void setDropdownTph(){
        popupTPH = true;
        new LongOperation(getView()).execute(String.valueOf(LongOperation_Setup_Data_TPH));
    }

    public void setTph(TPH tph) {
        if (tph == null) {
            etNotph.setText("");
            etAncak.setText("");
            tphSelected = null;
            blockSelected = null;
        } else {
            if(stampImage == null){
                stampImage = new StampImage();
            }
            stampImage.setTph(tph);
            stampImage.setBlock(tph.getBlock());

            if (tph.getCreateBy() == TPH.USER_GIS_SYSTEM) {
                lTph.setVisibility(View.GONE);
                lAncak.setVisibility(View.GONE);
                cbTphBayangan.setChecked(true);
            } else {
                lTph.setVisibility(View.VISIBLE);
                cbTphBayangan.setChecked(false);
            }

//            if(angkutActivity != null) {
//                setDropDownTPH();
//            }else if (tphActivity != null){
//                setDropdownTph();
//            }

            etNotph.setText(tph.getNamaTph());
            etAncak.setText(tph.getAncak());
            etBlock.setText(tph.getBlock());
            tphSelected = tph;
            blockSelected = tph.getBlock();
            etTotalJanjang.requestFocus();

            GlobalHelper.showKeyboard(getActivity());

            if (selectedTransaksiPanen == null) {
                if (tphSelected.getNamaTph() != null) {
                    showYesNo("Apakah Anda Yakin Untuk Pilih Block " + tphSelected.getBlock() + "\n Tph " + tphSelected.getNamaTph(), YesNo_Pilih_TPH);
                }else{
                    etBlock.requestFocus();
                }
            }
        }
    }

    public void setTPH(TPH TPH){
        if(TPH == null){
            etNotph.setText("");
        }else{
            if(stampImage == null){
                stampImage = new StampImage();
            }

//            if(TPH.getTph()!= null) {
//                stampImage.setBlock(TPH.getTph().getBlock());
//                stampImage.setTph(TPH.getTph());
//
//                etNotph.setText(TPH.getTph().getNamaTph());
//                etBlock.setText(TPH.getTph().getBlock());
//            }else if (TPH.getLangsiran() != null){
//                stampImage.setBlock(TPH.getLangsiran().getBlock());
//                stampImage.setLangsiran(TPH.getLangsiran());
//
//                etNotph.setText(TPH.getLangsiran().getNamaTujuan());
//                etBlock.setText(TPH.getLangsiran().getBlock());
//            }
        }
        selectedTPH = TPH;
    }

    @Override
    public void onTimeSet(ViewGroup viewGroup, int hourOfDay, int minute) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
        cal.set(Calendar.MINUTE, minute);
        waktuPanen = cal.getTimeInMillis();
        etJarak.setText(hourOfDay+" : "+minute);
    }

    private DialogFragment createDialogWithSetters() {
        BottomSheetPickerDialog dialog = NumberPadTimePickerDialog.newInstance(this,true);
        ((NumberPadTimePickerDialog) dialog).setHeaderTextColor(0xFFFF4081);
        dialog.setAccentColor(0xFFFF4081);
        dialog.setBackgroundColor(0xFF2196F3);
        dialog.setHeaderColor(0xFF2196F3);
        dialog.setHeaderTextDark(true);
        return dialog;
    }

    public void setImages(File images){
        ALselectedImage.add(images);
        setupimageslide();
    }

    private void setupimageslide(){
        Set<File> sf = new HashSet<>();
        sf.addAll(ALselectedImage);
        ALselectedImage.clear();
        ALselectedImage.addAll(sf);

        if(ALselectedImage.size() > 3){
            ivPhoto.setVisibility(View.GONE);
        }else{
            ivPhoto.setVisibility(View.VISIBLE);
        }
        if(ALselectedImage.size() != 0){
//            tphphoto.setVisibility(View.VISIBLE);
            imagecacnel.setVisibility(View.VISIBLE);
            imagesview.setVisibility(View.VISIBLE);
            sliderLayout.setVisibility(View.VISIBLE);
        }else{
//            tphphoto.setVisibility(View.GONE);
            imagecacnel.setVisibility(View.GONE);
            imagesview.setVisibility(View.GONE);
            sliderLayout.setVisibility(View.GONE);
        }
        sliderLayout.removeAllSliders();
        for(File file : ALselectedImage){
            TextSliderView textSliderView = new TextSliderView(getActivity());
            // initialize a SliderLayout
            if(file.toString().startsWith("http:")){
                String surl = file.toString();
                if(file.toString().startsWith("http://")){

                }else if(file.toString().startsWith("http:/")){
                    surl =surl.replace("http:/","http://");
                }

                textSliderView
                        .image(GlobalHelper.setUrlFoto(surl))
                        .setScaleType(BaseSliderView.ScaleType.Fit);
            }else {
                textSliderView
                        .image(file)
                        .setScaleType(BaseSliderView.ScaleType.Fit);
            }
            sliderLayout.addSlider(textSliderView);
        }
        sliderLayout.stopAutoCycle();
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.FlipHorizontal);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setCustomAnimation(new DescriptionAnimation());

        imagecacnel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(GSApp.getContext(),"slider getCurrentPosition " + sliderLayout.getCurrentPosition(),Toast.LENGTH_SHORT).show();
                try {
                    if (sliderLayout.getCurrentPosition() == 0 && ALselectedImage.size() == 1) {
                        ALselectedImage.clear();
                        sliderLayout.removeAllSliders();
                        sliderLayout.setVisibility(View.GONE);
                        imagecacnel.setVisibility(View.GONE);
                        imagesview.setVisibility(View.GONE);
                    } else {
                        ALselectedImage.remove(sliderLayout.getCurrentPosition());
                        sliderLayout.removeSliderAt(sliderLayout.getCurrentPosition());
                    }
                }catch (Exception e){
                    Log.e("sliderLayout", e.toString());
                }
            }
        });
        imagesview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WidgetHelper.showImagesZoom(getActivity(),ALselectedImage);
            }
        });
    }

    public void startLongOperation(int stat){
//        if(tphActivity != null) {
//            if (tphActivity.tulisUlangNFC && selectedTransaksiPanen != null) {
//                transaksiPanenHelper.hapusDataNFC(selectedTransaksiPanen);
//            }
//        }

        new LongOperation(getView()).execute(String.valueOf(stat));
    }

    private class LongOperation extends AsyncTask<String, String, String> {
        private AlertDialog alertDialogAllpoin ;
        private View view;
        Snackbar snackbar;
        private Boolean validasi;
        //tambahan zendi, untuk check
        private boolean isRusak = false;
        public String messageError = "";

        public LongOperation(View v) {
            view = v;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            alertDialogAllpoin = WidgetHelper.showWaitingDialog(getActivity(), getResources().getString(R.string.wait));
            if (popupTPH) {
                validasi = true;
            } else {
                validasi = validasiSaveTransaksiPanen();
            }
            if(lsStatus.getText().toString().equals("Kartu Rusak")){
                isRusak = true;
            }else{
                etNoNFC.setText("");
                isRusak = false;
            }
        }

        @Override
        protected String doInBackground(String... params) {
            if (LongOperation_Setup_Point == Integer.parseInt(params[0])) {
                setupPoint();
                validasi = true;
                popupTPH = false;
                return String.valueOf(params[0]);
            }else if (LongOperation_UpdateLocation_Setup_Point == Integer.parseInt(params[0])) {
                if (baseActivity.currentlocation != null) {
                    setupPoint();
                    validasi = true;
                    popupTPH = false;
                }
                return String.valueOf(params[0]);
            }else if(validasi) {
                switch (Integer.parseInt(params[0])) {
                    case LongOperation_Setup_SPK:{
                        setMasterSpk();
                        break;
                    }
                    case LongOperation_Save_Data_Qc: {
                        saveTransaksiPanenQc();
                        break;
                    }
                    case LongOperation_Delet_Data_Qc: {
                        hapusDataQc(selectedTransaksiPanen);
                        break;
                    }
                    case LongOperation_Setup_Data_TPH:{

                        Log.d(TAG, "doInBackground: tphTerdekat "+ blockName);
                        allTph = new ArrayList<>();
                        allTph = tphHelper.setDataTph(GlobalHelper.getEstate().getEstCode(),blockName);
                        tphTerdekat = tphHelper.cariTphTerdekat(allTph);
                        //Log.d(TAG, "doInBackground: tphTerdekat "+ tphTerdekat.getBlock());
                        break;
                    }
                }

                return String.valueOf(params[0]);
            }

            return "salah";
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(result.equals("salah")){
                if (alertDialogAllpoin != null) {
                    alertDialogAllpoin.cancel();
                }
            }else if(validasi && !popupTPH) {
                if (alertDialogAllpoin != null) {
                    alertDialogAllpoin.cancel();
                }
                switch (Integer.parseInt(result)) {
                    case LongOperation_Setup_Point:{
                        //Log.d(TAG, "onPostExecute: setuppoint"+ selectedTransaksiPanen);
                        if(selectedTransaksiPanen == null) {
                            setDropdownTph();
                        }
                        break;
                    }
                    case LongOperation_UpdateLocation_Setup_Point:{
                        if(selectedTransaksiPanen == null) {
                            setDropdownTph();
                        }
                        break;
                    }
                    case LongOperation_Setup_SPK:{
                        if(allSpk.size() > 0){
                            adapterSpk = new SelectSpkAdapter(getActivity(), R.layout.row_setting, new ArrayList<SPK>(allSpk.values()));
                            etSpk.setThreshold(1);
                            etSpk.setAdapter(adapterSpk);
                            adapterSpk.notifyDataSetChanged();

                            lBorongan.setVisibility(View.VISIBLE);
                            etSpk.setText("");
                            spkSelected = null;
                        }else{
                            lBorongan.setVisibility(View.GONE);
                            etSpk.setText("");
                            spkSelected = null;
                            Toast.makeText(HarvestApp.getContext(),"Tidak Ada Master SPK",Toast.LENGTH_SHORT).show();
                        }
                        break;
                    }
                }
            }else if(validasi && popupTPH) {
                popupTPH = false;
                switch (Integer.parseInt(result)) {
                    case LongOperation_Setup_Data_TPH: {
                        if (allTph.size() > 0 && allTph != null) {
                            adapterTph = new SelectTphAdapter(getActivity(), R.layout.row_setting, allTph);
                            etNotph.setThreshold(1);
                            etNotph.setAdapter(adapterTph);
                            adapterTph.notifyDataSetChanged();
                            if(cariTphTerdekat) {
                                setTph(tphTerdekat);
                            }else{
                                cariTphTerdekat = true;
                            }
                        } else {
                            setTph(null);
                        }
                        break;
                    }
                }
                if (alertDialogAllpoin != null) {
                    alertDialogAllpoin.cancel();
                }
//                new Handler().post(new Runnable() {
//                    @Override
//                    public void run() {
//                        tphHelper.choseTph(etBlock.getText().toString());
//                        popupTPH = false;
//                        if (alertDialogAllpoin != null) {
//                            alertDialogAllpoin.cancel();
//                        }
//                    }
//                });
            } else if (!validasi){
                if (alertDialogAllpoin != null) {
                    alertDialogAllpoin.cancel();
                }
                switch (Integer.parseInt(result)) {
//                    case LongOperation_NFC_With_Save: {
//                        Toast.makeText(HarvestApp.getContext(),messageError,Toast.LENGTH_SHORT).show();
//                        break;
//                    }
                }
            }
        }

        public void publishProgress(JSONObject objProgres){
            onProgressUpdate(String.valueOf(objProgres));
        }
    }
}
