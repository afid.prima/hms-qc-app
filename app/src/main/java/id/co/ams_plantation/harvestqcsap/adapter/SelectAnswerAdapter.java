package id.co.ams_plantation.harvestqcsap.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;

import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.model.Answer;

/**
 * Created on : 14,October,2021
 * Author     : Afid
 */

public class SelectAnswerAdapter extends ArrayAdapter<Answer> {
    public ArrayList<Answer> items;
    public ArrayList<Answer> itemsAll;
    public ArrayList<Answer> suggestions;
    private int viewResourceId;

    public SelectAnswerAdapter(Context context, int viewResourceId, ArrayList<Answer> data){
        super(context, viewResourceId, data);
        this.items = new ArrayList<>();
        this.items.addAll(data);
        this.itemsAll = (ArrayList<Answer>)this.items.clone();
        this.suggestions = new ArrayList<>();
        this.viewResourceId = viewResourceId;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(viewResourceId, null);
        }
        if(items.size()>position){
            Answer Answer = items.get(position);
            if (Answer != null) {
                TextView item_ket1 = (TextView) v.findViewById(R.id.item_ket1);
                item_ket1.setText(Answer.getQcAnsware());
            }
        }
        return v;
    }

    public Answer getItemAt(int position){
        return items.get(position);
    }

    @Override
    public int getCount(){
        return items!=null?items.size():0;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            String str = ((Answer)(resultValue)).getQcAnsware();
            return str;
        }
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if(constraint != null) {
                suggestions.clear();
                for (Answer Answer : itemsAll) {
                    if(Answer.getQcAnsware().toLowerCase().contains(constraint.toString().toLowerCase())){
                        suggestions.add(Answer);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<Answer> filteredList = (ArrayList<Answer>) results.values;
//            clear();
            items.clear();
//            itemsAll.clear();
            if(results != null && results.count > 0) {
//                addAll(filteredList);
//                itemsAll.addAll(filteredList);
//                for (Anggota anggota : filteredList){
//                    items.add(anggota);
//                }
                items.addAll(filteredList);
            }else{
//                clear();
                items.clear();
//                itemsAll.clear();
            }
            notifyDataSetChanged();
        }
    };
}
