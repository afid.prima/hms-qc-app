package id.co.ams_plantation.harvestqcsap.model;

import java.util.HashSet;

/**
 * Created on : 25,October,2021
 * Author     : Afid
 */

public class QcFotoPanen {
    String idTPanen;
    double latitude;
    double longitude;
    String createBy;
    long createDate;
    String updateBy;
    long updateDate;
    String ancak;
    String baris;
    int status;
    int restan;
    String estCode;
    String block;
    Pemanen pemanen;
    TPH tph;
    HasilPanen hasilPanen;
    HashSet<String> foto;
    String idNfc;
    SupervisionList substitute;
    Kongsi kongsi;
    SPK spk;
    NFCFlagTap nfcFlagTap;
    int tahunTanam;
    String kraniName;
    DataQcFotoPanen dataQcFotoPanen;

    public String getIdTPanen() {
        return idTPanen;
    }

    public void setIdTPanen(String idTPanen) {
        this.idTPanen = idTPanen;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public long getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(long updateDate) {
        this.updateDate = updateDate;
    }

    public String getAncak() {
        return ancak;
    }

    public void setAncak(String ancak) {
        this.ancak = ancak;
    }

    public String getBaris() {
        return baris;
    }

    public void setBaris(String baris) {
        this.baris = baris;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getRestan() {
        return restan;
    }

    public void setRestan(int restan) {
        this.restan = restan;
    }

    public String getEstCode() {
        return estCode;
    }

    public void setEstCode(String estCode) {
        this.estCode = estCode;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public Pemanen getPemanen() {
        return pemanen;
    }

    public void setPemanen(Pemanen pemanen) {
        this.pemanen = pemanen;
    }

    public TPH getTph() {
        return tph;
    }

    public void setTph(TPH tph) {
        this.tph = tph;
    }

    public HasilPanen getHasilPanen() {
        return hasilPanen;
    }

    public void setHasilPanen(HasilPanen hasilPanen) {
        this.hasilPanen = hasilPanen;
    }

    public HashSet<String> getFoto() {
        return foto;
    }

    public void setFoto(HashSet<String> foto) {
        this.foto = foto;
    }

    public String getIdNfc() {
        return idNfc;
    }

    public void setIdNfc(String idNfc) {
        this.idNfc = idNfc;
    }

    public SupervisionList getSubstitute() {
        return substitute;
    }

    public void setSubstitute(SupervisionList substitute) {
        this.substitute = substitute;
    }

    public Kongsi getKongsi() {
        return kongsi;
    }

    public void setKongsi(Kongsi kongsi) {
        this.kongsi = kongsi;
    }

    public SPK getSpk() {
        return spk;
    }

    public void setSpk(SPK spk) {
        this.spk = spk;
    }

    public NFCFlagTap getNfcFlagTap() {
        return nfcFlagTap;
    }

    public void setNfcFlagTap(NFCFlagTap nfcFlagTap) {
        this.nfcFlagTap = nfcFlagTap;
    }

    public int getTahunTanam() {
        return tahunTanam;
    }

    public void setTahunTanam(int tahunTanam) {
        this.tahunTanam = tahunTanam;
    }

    public String getKraniName() {
        return kraniName;
    }

    public void setKraniName(String kraniName) {
        this.kraniName = kraniName;
    }

    public DataQcFotoPanen getDataQcFotoPanen() {
        return dataQcFotoPanen;
    }

    public void setDataQcFotoPanen(DataQcFotoPanen dataQcFotoPanen) {
        this.dataQcFotoPanen = dataQcFotoPanen;
    }
}
