package id.co.ams_plantation.harvestqcsap.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Created on : 07,September,2021
 * Author     : Afid
 */

public class Answer {
    @SerializedName("IdQcAnswer")
    @Expose
    private String idQcAnswer;
    @SerializedName("QcAnsware")
    @Expose
    private String qcAnsware;

    public String getIdQcAnswer() {
        return idQcAnswer;
    }

    public void setIdQcAnswer(String idQcAnswer) {
        this.idQcAnswer = idQcAnswer;
    }

    public String getQcAnsware() {
        return qcAnsware;
    }

    public void setQcAnsware(String qcAnsware) {
        this.qcAnsware = qcAnsware;
    }
}
