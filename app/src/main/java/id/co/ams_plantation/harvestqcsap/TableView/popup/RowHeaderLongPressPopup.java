package id.co.ams_plantation.harvestqcsap.TableView.popup;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.evrencoskun.tableview.TableView;

import id.co.ams_plantation.harvestqcsap.Fragment.QcFotoPanenFragmet;
import id.co.ams_plantation.harvestqcsap.TableView.model.TableViewCell;
import id.co.ams_plantation.harvestqcsap.ui.MainMenuActivity;


public class RowHeaderLongPressPopup extends PopupMenu implements PopupMenu
        .OnMenuItemClickListener {

    // Menu Item constants
    private static final int REMOVE_ROW = 1;
    private static final int DOWNLOAD_REPORT_ROW = 2;
    private static final int VIEW_REPORT_ROW = 3;

    @NonNull
    private TableView mTableView;
    private int mRowPosition;
    private Context context;

    public RowHeaderLongPressPopup(@NonNull RecyclerView.ViewHolder viewHolder, @NonNull TableView tableView, Context context) {
        super(viewHolder.itemView.getContext(), viewHolder.itemView);

        this.mTableView = tableView;
        this.mRowPosition = viewHolder.getAdapterPosition();
        this.context = context;

        initialize();
    }

    private void initialize() {
        createMenuItem();

        this.setOnMenuItemClickListener(this);
    }

    private void createMenuItem() {
        this.getMenu().add(Menu.NONE, REMOVE_ROW, 2, "Hapus");
        this.getMenu().add(Menu.NONE, DOWNLOAD_REPORT_ROW, 2, "Downlaod Summary");
        this.getMenu().add(Menu.NONE, VIEW_REPORT_ROW, 2, "Open Summary");
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        // Note: item id is index of menu item..

        switch (menuItem.getItemId()) {
            case REMOVE_ROW:
                if(context instanceof MainMenuActivity){
                    MainMenuActivity mainMenuActivity = ((MainMenuActivity) context);
                    Fragment fragment = ((MainMenuActivity.TabPagerAdapter) ((MainMenuActivity) mainMenuActivity).viewPager.getAdapter()).getItem(((MainMenuActivity) mainMenuActivity).viewPager.getCurrentItem());
                    if(fragment instanceof QcFotoPanenFragmet){
                        TableViewCell cell = ((TableViewCell) mTableView.getAdapter().getCellItem(0,mRowPosition));
                        String [] id = cell.getId().split("-");
                        ((QcFotoPanenFragmet) fragment).hapusSession(id[1]);
                    }
                }
                break;
            case DOWNLOAD_REPORT_ROW:
                if(context instanceof MainMenuActivity) {
                    MainMenuActivity mainMenuActivity = ((MainMenuActivity) context);
                    Fragment fragment = ((MainMenuActivity.TabPagerAdapter) ((MainMenuActivity) mainMenuActivity).viewPager.getAdapter()).getItem(((MainMenuActivity) mainMenuActivity).viewPager.getCurrentItem());
                    if (fragment instanceof QcFotoPanenFragmet) {
                        TableViewCell cell = ((TableViewCell) mTableView.getAdapter().getCellItem(0,mRowPosition));
                        String [] id = cell.getId().split("-");
                        ((QcFotoPanenFragmet) fragment).downloadReportSession(id[1]);
                    }
                }
            case VIEW_REPORT_ROW:
                if(context instanceof MainMenuActivity) {
                    MainMenuActivity mainMenuActivity = ((MainMenuActivity) context);
                    Fragment fragment = ((MainMenuActivity.TabPagerAdapter) ((MainMenuActivity) mainMenuActivity).viewPager.getAdapter()).getItem(((MainMenuActivity) mainMenuActivity).viewPager.getCurrentItem());
                    if (fragment instanceof QcFotoPanenFragmet) {
                        TableViewCell cell = ((TableViewCell) mTableView.getAdapter().getCellItem(0,mRowPosition));
                        String [] id = cell.getId().split("-");
                        ((QcFotoPanenFragmet) fragment).viewReportSession(id[1]);
                    }
                }
        }
        return true;
    }
}