package id.co.ams_plantation.harvestqcsap.connection;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;



public class ServiceRequest {

    private String host;
    private String path;
    private Method method;
    private Tag tag;
    private HashMap<String, String> headers;
    private HashMap<String, String> bodyParameter;
    private JSONObject bodyRaw;
    private JSONArray bodyRawArray;
    private HashMap<String, String> queryParameter;
    private HashMap<String, String> pathParameter;
    private HashMap<String, File> files;

    public JSONObject getBodyRaw() {
        return bodyRaw;
    }

    public void setBodyRaw(JSONObject bodyRaw) {
        this.bodyRaw = bodyRaw;
    }

    public JSONArray getBodyRawArray() {
        return bodyRawArray;
    }

    public void setBodyRawArray(JSONArray bodyRawArray) {
        this.bodyRawArray = bodyRawArray;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }

    public HashMap<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(HashMap<String, String> headers) {
        this.headers = headers;
    }

    public HashMap<String, String> getBodyParameter() {
        return bodyParameter;
    }

    public void setBodyParameter(HashMap<String, String> bodyParameter) {
        this.bodyParameter = bodyParameter;
    }

    public HashMap<String, String> getQueryParameter() {
        return queryParameter;
    }

    public void setQueryParameter(HashMap<String, String> queryParameter) {
        this.queryParameter = queryParameter;
    }

    public HashMap<String, String> getPathParameter() {
        return pathParameter;
    }

    public void setPathParameter(HashMap<String, String> pathParameter) {
        this.pathParameter = pathParameter;
    }

    public HashMap<String, File> getFiles() {
        return files;
    }

    public void setFiles(HashMap<String, File> files) {
        this.files = files;
    }

}
