package id.co.ams_plantation.harvestqcsap.model;

import java.util.ArrayList;
import java.util.HashSet;

public class QcAncakPohon {
    String idQcAncakPohon;
    int noBaris;
    String idPohon;
    int janjangPanen;
    int buahTinggalSegar;
    int buahTinggalBusuk;
    String penyebabBuahTinggal;
    QuestionAnswer penyebabBuahTinggals;
    int brondolanSegarTinggal;
    int brondolanLamaTinggal;
    String  penyebabBrondolan;
    QuestionAnswer  penyebabBrondolans;
    String lokasiTertinggal;
    QuestionAnswer  lokasiTertinggals;
    boolean overPrun;
    boolean pelepahSengkel;
    boolean susunanPelepah;
    int buahMatahari;
    int buahTinggal;
    int brondolanTinggal;
    double latitude;
    double longitude;
    long waktuQcAncakPohon;
    HashSet<String> foto;

    public QcAncakPohon(String idQcAncakPohon, int noBaris, String idPohon, int janjangPanen, int buahTinggalSegar, int buahTinggalBusuk, String penyebabBuahTinggal,QuestionAnswer penyebabBuahTinggals,int brondolanSegarTinggal, int brondolanLamaTinggal, String penyebabBrondolan,QuestionAnswer penyebabBrondolans,String lokasiTertinggal,QuestionAnswer lokasiTertinggals, boolean overPrun, boolean pelepahSengkel, boolean susunanPelepah, int buahMatahari, double latitude, double longitude, long waktuQcAncakPohon,HashSet<String> foto) {
        this.idQcAncakPohon = idQcAncakPohon;
        this.noBaris = noBaris;
        this.idPohon = idPohon;
        this.janjangPanen = janjangPanen;
        this.buahTinggalSegar = buahTinggalSegar;
        this.buahTinggalBusuk = buahTinggalBusuk;
        this.penyebabBuahTinggal = penyebabBuahTinggal;
        this.penyebabBuahTinggals = penyebabBuahTinggals;
        this.brondolanSegarTinggal = brondolanSegarTinggal;
        this.brondolanLamaTinggal = brondolanLamaTinggal;
        this.penyebabBrondolan = penyebabBrondolan;
        this.penyebabBrondolans = penyebabBrondolans;
        this.lokasiTertinggal = lokasiTertinggal;
        this.lokasiTertinggals = lokasiTertinggals;
        this.overPrun = overPrun;
        this.pelepahSengkel = pelepahSengkel;
        this.susunanPelepah = susunanPelepah;
        this.buahMatahari = buahMatahari;
        this.latitude = latitude;
        this.longitude = longitude;
        this.waktuQcAncakPohon = waktuQcAncakPohon;
        this.foto = foto;
    }

    public String getIdQcAncakPohon() {
        return idQcAncakPohon;
    }

    public void setIdQcAncakPohon(String idQcAncakPohon) {
        this.idQcAncakPohon = idQcAncakPohon;
    }

    public int getNoBaris() {
        return noBaris;
    }

    public void setNoBaris(int noBaris) {
        this.noBaris = noBaris;
    }

    public String getIdPohon() {
        return idPohon;
    }

    public void setIdPohon(String idPohon) {
        this.idPohon = idPohon;
    }

    public int getJanjangPanen() {
        return janjangPanen;
    }

    public void setJanjangPanen(int janjangPanen) {
        this.janjangPanen = janjangPanen;
    }

    public int getBuahTinggalSegar() {
        return buahTinggalSegar;
    }

    public void setBuahTinggalSegar(int buahTinggalSegar) {
        this.buahTinggalSegar = buahTinggalSegar;
    }

    public int getBuahTinggalBusuk() {
        return buahTinggalBusuk;
    }

    public void setBuahTinggalBusuk(int buahTinggalBusuk) {
        this.buahTinggalBusuk = buahTinggalBusuk;
    }

    public String getPenyebabBuahTinggal() {
        return penyebabBuahTinggal;
    }

    public void setPenyebabBuahTinggal(String penyebabBuahTinggal) {
        this.penyebabBuahTinggal = penyebabBuahTinggal;
    }

    public int getBrondolanSegarTinggal() {
        return brondolanSegarTinggal;
    }

    public void setBrondolanSegarTinggal(int brondolanSegarTinggal) {
        this.brondolanSegarTinggal = brondolanSegarTinggal;
    }

    public int getBrondolanLamaTinggal() {
        return brondolanLamaTinggal;
    }

    public void setBrondolanLamaTinggal(int brondolanLamaTinggal) {
        this.brondolanLamaTinggal = brondolanLamaTinggal;
    }

    public String getPenyebabBrondolan() {
        return penyebabBrondolan;
    }

    public void setPenyebabBrondolan(String penyebabBrondolan) {
        this.penyebabBrondolan = penyebabBrondolan;
    }

    public String getLokasiTertinggal() {
        return lokasiTertinggal;
    }

    public void setLokasiTertinggal(String lokasiTertinggal) {
        this.lokasiTertinggal = lokasiTertinggal;
    }

    public boolean isOverPrun() {
        return overPrun;
    }

    public void setOverPrun(boolean overPrun) {
        this.overPrun = overPrun;
    }

    public boolean isPelepahSengkel() {
        return pelepahSengkel;
    }

    public void setPelepahSengkel(boolean pelepahSengkel) {
        this.pelepahSengkel = pelepahSengkel;
    }

    public boolean isSusunanPelepah() {
        return susunanPelepah;
    }

    public void setSusunanPelepah(boolean susunanPelepah) {
        this.susunanPelepah = susunanPelepah;
    }

    public int getBuahMatahari() {
        return buahMatahari;
    }

    public void setBuahMatahari(int buahMatahari) {
        this.buahMatahari = buahMatahari;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public long getWaktuQcAncakPohon() {
        return waktuQcAncakPohon;
    }

    public void setWaktuQcAncakPohon(long waktuQcAncakPohon) {
        this.waktuQcAncakPohon = waktuQcAncakPohon;
    }

    public int getBuahTinggal() {
        return buahTinggal;
    }

    public void setBuahTinggal(int buahTinggal) {
        this.buahTinggal = buahTinggal;
    }

    public int getBrondolanTinggal() {
        return brondolanTinggal;
    }

    public void setBrondolanTinggal(int brondolanTinggal) {
        this.brondolanTinggal = brondolanTinggal;
    }

    public HashSet<String> getFoto() {
        return foto;
    }

    public void setFoto(HashSet<String> foto) {
        this.foto = foto;
    }

    public QuestionAnswer getPenyebabBuahTinggals() {
        return penyebabBuahTinggals;
    }

    public void setPenyebabBuahTinggals(QuestionAnswer penyebabBuahTinggals) {
        this.penyebabBuahTinggals = penyebabBuahTinggals;
    }

    public QuestionAnswer getPenyebabBrondolans() {
        return penyebabBrondolans;
    }

    public void setPenyebabBrondolans(QuestionAnswer penyebabBrondolans) {
        this.penyebabBrondolans = penyebabBrondolans;
    }

    public QuestionAnswer getLokasiTertinggals() {
        return lokasiTertinggals;
    }

    public void setLokasiTertinggals(QuestionAnswer lokasiTertinggals) {
        this.lokasiTertinggals = lokasiTertinggals;
    }
}
