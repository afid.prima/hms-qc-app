package id.co.ams_plantation.harvestqcsap.util;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import id.co.ams_plantation.harvestqcsap.Fragment.TphInputFragment;
import id.co.ams_plantation.harvestqcsap.HarvestApp;
import id.co.ams_plantation.harvestqcsap.ui.BaseActivity;
import id.co.ams_plantation.harvestqcsap.ui.MapActivity;
import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.model.TPH;

/**
 * Created by user on 12/11/2018.
 */

public class InfoWindow {
    static Context context;
    static ViewGroup retView;
    static LinearLayout ll_ipf_takepicture;
    static Button btnChose,btnDetail,btnDelet;
    static TextView tvheader,tvket1_iw,tvket2_iw,tvket3_iw,tvket4_iw;
    static ImageView ivPhoto,ic_eye;
    static SliderLayout sliderLayout;
    static FragmentActivity activity;
    static int Activity_From;
    final static int Activity_From_TphActivity = 0;
    final static int Activity_From_MapActivity = 1;

    private static void setupInfoWindowsP(final Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        retView = (ViewGroup) inflater.inflate(R.layout.info_window, null);
        InfoWindow.context = context;
    }
    public static ViewGroup setupInfoWindows(final Context context, TPH tph){
        setupInfoWindowsP(context);
        initBlockDetail(retView,tph);
        return retView;
    }

    private static void initBlockDetail(final ViewGroup retView, final TPH tph) {

        tvheader = (TextView) retView.findViewById(R.id.tv_header_iw);
        tvket1_iw = (TextView) retView.findViewById(R.id.tvket1_iw);
//        tvket2_iw = (TextView) retView.findViewById(R.id.tvket2_iw);
//        tvket3_iw = (TextView) retView.findViewById(R.id.tvket3_iw);
//        tvket4_iw = (TextView) retView.findViewById(R.id.tvket4_iw);

        ivPhoto = (ImageView) retView.findViewById(R.id.ivPhoto);
        ic_eye = (ImageView) retView.findViewById(R.id.ic_eye);
        sliderLayout = (SliderLayout) retView.findViewById(R.id.sliderLayout);
        btnChose = (Button) retView.findViewById(R.id.btnChose_iw);
        btnDetail = (Button) retView.findViewById(R.id.btnDetail_iw);
        btnDelet = (Button) retView.findViewById(R.id.btnDelet_iw);

        SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd MMM yyyy");


        if(context instanceof MapActivity){
            activity = (MapActivity) context;
            Activity_From = Activity_From_MapActivity;
            btnChose.setVisibility(View.GONE);
            btnDelet.setVisibility(View.VISIBLE);
        }

        tvheader.setText(tph.getNamaTph().trim());
        String ket1 = "Ancak : " + tph.getAncak()+ "\n";
        ket1 = ket1 + activity.getResources().getString(R.string.create_time)+" : "+ sdf.format(new Date(tph.getCreateDate()));
        tvket1_iw.setText(ket1);
//        tvket2_iw.setVisibility(View.GONE);
//        tvket3_iw.setVisibility(View.GONE);
//        tvket4_iw.setVisibility(View.GONE);

        ArrayList<File> files = new ArrayList<>();
        if(tph.getFoto() != null) {
            if (tph.getFoto().size() > 0) {
                for (String s : tph.getFoto()) {
                    File file = new File(s);
                    files.add(file);
                }
            }
        }
        setupSlideshow(files);

        btnChose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = (Fragment) activity.getSupportFragmentManager().findFragmentById(R.id.content_container);
                if (fragment instanceof TphInputFragment) {
                    double distance = GlobalHelper.distance(((TphInputFragment) fragment).tphHelper.latawal, tph.getLatitude(), ((TphInputFragment) fragment).tphHelper.longawal, tph.getLongitude());
                    if (distance > GlobalHelper.MAX_RADIUS_MAPMENU) {
                        Toast.makeText(HarvestApp.getContext(), activity.getResources().getString(R.string.radius_to_long), Toast.LENGTH_SHORT).show();
                    } else {
                        ((TphInputFragment) fragment).setTph(tph);
                        ((TphInputFragment) fragment).tphHelper.listrefrence.dismiss();
                    }
                }
            }
        });

        btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (Activity_From) {
                    case Activity_From_TphActivity:
                        Fragment fragment = (Fragment) activity.getSupportFragmentManager().findFragmentById(R.id.content_container);
                        if(fragment instanceof TphInputFragment){
                            ((TphInputFragment) fragment).tphHelper.callout.hide();
                            ((TphInputFragment) fragment).tphHelper.showValueForm(tph);
                        }
                        break;
                    case Activity_From_MapActivity:
                        ((MapActivity) context).callout.hide();
//                        ((MapActivity) context).startLongOperation(MapActivity.LongOperation_Setup_FormTPH,new TPHnLangsiran(tph.getNoTph(),tph));

                        break;
                }
            }
        });

    }

    private static void setupSlideshow(final List<File> ALselectedImage){
        try {
            final Activity activity = (Activity) context;
            Set<File> sf = new HashSet<>();
            sf.addAll(ALselectedImage);
            ALselectedImage.clear();
            ALselectedImage.addAll(sf);

            if (ALselectedImage.size() != 0) {
                sliderLayout.setVisibility(View.VISIBLE);
                ic_eye.setVisibility(View.VISIBLE);
                ivPhoto.setVisibility(View.GONE);
            } else {
                sliderLayout.setVisibility(View.GONE);
                ic_eye.setVisibility(View.GONE);
                ivPhoto.setVisibility(View.VISIBLE);
            }
            sliderLayout.removeAllSliders();
            for (File file : ALselectedImage) {
                TextSliderView textSliderView = new TextSliderView(activity);
                // initialize a SliderLayout
                if (file.toString().startsWith("http:")) {
                    String surl = file.toString();
                    if (file.toString().startsWith("http://")) {

                    } else if (file.toString().startsWith("http:/")) {
                        surl = surl.replace("http:/", "http://");
                    }

                    textSliderView
                            .image(GlobalHelper.setUrlFoto(surl))
                            .setScaleType(BaseSliderView.ScaleType.Fit);
                } else {
                    textSliderView
                            .image(file)
                            .setScaleType(BaseSliderView.ScaleType.Fit);
                }
                sliderLayout.addSlider(textSliderView);
            }
            sliderLayout.stopAutoCycle();
            sliderLayout.setPresetTransformer(SliderLayout.Transformer.FlipHorizontal);
            sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
            sliderLayout.setCustomAnimation(new DescriptionAnimation());
            ic_eye.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    WidgetHelper.showImagesZoom(activity,ALselectedImage);
                    //MainActivity.showZoom(ALselectedImage.get(sliderLayout.getCurrentPosition()).toString(),v,activity);
                }
            });
        }catch (Exception e) {
            Log.e("sliderLayout", e.toString());
        }
    }
}

