package id.co.ams_plantation.harvestqcsap.util;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.unnamed.b.atv.model.TreeNode;
import com.unnamed.b.atv.view.AndroidTreeView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import id.co.ams_plantation.harvestqcsap.ui.MapActivity;
import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.adapter.IconTreeItemHolder;
import id.co.ams_plantation.harvestqcsap.adapter.SelectableHeaderHolder;
import id.co.ams_plantation.harvestqcsap.adapter.SelectableItemHolder;
import id.co.ams_plantation.harvestqcsap.model.Block;

public class AfdelingBlockHelper {
    Context context;
    AlertDialog listrefrence;
    ViewGroup treeView;
    AndroidTreeView tView;
    TreeNode root;
    ArrayList<String> stringBlock;
    MapActivity mapActivity;
    CompleteTextViewHelper etSearch;

    public AfdelingBlockHelper(Context context) {
        this.context = context;
    }

    public void setUpTreeeView(){
        View view = LayoutInflater.from(context).inflate(R.layout.select_afdeling_block_layout,null);
        treeView = (ViewGroup) view.findViewById(R.id.treeView);
        TextView idHeader = (TextView) view.findViewById(R.id.idHeader);
        etSearch = (CompleteTextViewHelper) view.findViewById(R.id.etSearch);
        Button btnClear = (Button) view.findViewById(R.id.btnClear);
//        Button btnSave = (Button) view.findViewById(R.id.btnSave);
        Button btnClose = (Button) view.findViewById(R.id.btnClose);

        idHeader.setText("Pilih Afdeling & Block");
        stringBlock = new ArrayList<>();
        mapActivity = (MapActivity) context;
        generateTreeView();

        ArrayAdapter<String> adapterBlock = new ArrayAdapter<String>
                (context,android.R.layout.select_dialog_item, stringBlock.toArray(new String[stringBlock.size()]));
        etSearch.setThreshold(1);
        etSearch.setAdapter(adapterBlock);
        etSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etSearch.showDropDown();
            }
        });
        etSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                updateValueAfdelingBlock();
                generateTreeView();
            }
        });

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateValueAfdelingBlock();
                etSearch.setText("");
                etSearch.setFocusable(false);
                generateTreeView();
            }
        });

//        btnSave.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String cek = "";
////                if(listrefrence!= null) {
////                    listrefrence.dismiss();
////                }
//            }
//        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mapActivity.alertDialog = WidgetHelper.showWaitingDialog(context, context.getResources().getString(R.string.wait));
                mapActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateValueAfdelingBlock();
                        mapActivity.setAfdelingBlocksValue();
                        if(listrefrence!= null) {
                            listrefrence.dismiss();
                        }
                    }
                });
            }
        });

        listrefrence = WidgetHelper.showFormDialog(listrefrence,view,context);
    }

    private void generateTreeView(){
        treeView.removeAllViews();
        root = TreeNode.root();
        boolean ada = false;
        for ( String key : mapActivity.afdelingBlocks.keySet() ) {
            TreeNode afdelingTree = new TreeNode(new IconTreeItemHolder.IconTreeItem(R.string.ic_folder, key)).setViewHolder(
                    new SelectableHeaderHolder(context));

            if(etSearch.getText().toString().isEmpty()) {
                //jika tidak sedang search munuculkan semua afdeling dan block
                Boolean selectedAll = true;
                for (int i = 0; i < mapActivity.afdelingBlocks.get(key).size(); i++) {
                    stringBlock.add(mapActivity.afdelingBlocks.get(key).get(i).getBlock());

                    TreeNode blockTree = new TreeNode(mapActivity.afdelingBlocks.get(key).get(i).getBlock())
                            .setViewHolder(new SelectableItemHolder(context));
                    blockTree.setSelected(mapActivity.afdelingBlocks.get(key).get(i).getSelected());
                    blockTree.setSelectable(true);
                    if (!mapActivity.afdelingBlocks.get(key).get(i).getSelected()) {
                        selectedAll = false;
                    }
                    afdelingTree.addChildren(blockTree);
                }
                afdelingTree.setSelectable(true);
                afdelingTree.setSelected(selectedAll);
                root.addChild(afdelingTree);
            }else{
                //jika tidak sedang search maka munuclkan blok tsb aja
                for (int i = 0; i < mapActivity.afdelingBlocks.get(key).size(); i++) {
                    if(mapActivity.afdelingBlocks.get(key).get(i).getBlock().equals(etSearch.getText().toString())) {
                        ada = true;

                        TreeNode blockTree = new TreeNode(mapActivity.afdelingBlocks.get(key).get(i).getBlock())
                                .setViewHolder(new SelectableItemHolder(context));

                        blockTree.setSelected(mapActivity.afdelingBlocks.get(key).get(i).getSelected());
                        blockTree.setSelectable(true);
                        afdelingTree.addChildren(blockTree);
                        break;
                    }
                }
                if(ada) {
                    afdelingTree.setSelectable(false);
                    afdelingTree.setExpanded(true);
                    root.addChild(afdelingTree);
                    break;
                }
            }
        }

        tView = new AndroidTreeView(context, root);
        tView.setDefaultAnimation(true);
        tView.setSelectionModeEnabled(true);
        treeView.addView(tView.getView());
    }

    private void updateValueAfdelingBlock(){
        if(tView.getSelected().size() > 0) {
            for (String key : mapActivity.afdelingBlocks.keySet()) {
                ArrayList<Block> blocks = new ArrayList<>();
                for (int i = 0; i < mapActivity.afdelingBlocks.get(key).size(); i++) {
                    boolean ada = false;
                    for (int j = 0; j < tView.getSelected().size(); j++) {
                        if ((mapActivity.afdelingBlocks.get(key).get(i).getBlock().equals(tView.getSelected().get(j).getValue()))) {
                            ada = true;
                            break;
                        }
                    }
                    Block block = new Block(mapActivity.afdelingBlocks.get(key).get(i).getBlock(), ada,mapActivity.afdelingBlocks.get(key).get(i).getPolygons());
                    blocks.add(block);
                }

                Collections.sort(blocks, new Comparator<Block>() {
                    @Override
                    public int compare(Block o1, Block o2) {
                        return o1.getBlock().compareToIgnoreCase(o2.getBlock());
                    }
                });

                mapActivity.afdelingBlocks.put(key, blocks);
            }
        }
    }
}
