package id.co.ams_plantation.harvestqcsap.TableView.view_model;

import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import id.co.ams_plantation.harvestqcsap.TableView.model.TableViewCell;
import id.co.ams_plantation.harvestqcsap.TableView.model.TableViewColumnHeader;
import id.co.ams_plantation.harvestqcsap.TableView.model.TableViewRowHeader;
import id.co.ams_plantation.harvestqcsap.model.QcAncak;
import id.co.ams_plantation.harvestqcsap.model.QcAncakPohon;

/**
 * Created by user on 12/24/2018.
 */

public class AncakTableViewModel {
    private Context mContext;
    private ArrayList<QcAncak> qcAncaks;
    String [] HeaderColumn = {
            "Block",
            "Tph",
            "Kondisi Ancak",
            "Pokok",
            "Pokok Panen",
            "Janjang Panen",
            "Buah Tinggal",
            "Buah Tinggal Segar",
            "Buah Tinggal Busuk",
            "Brdl Tinggal",
            "Brdl Segar Tinggal",
            "Brdl Lama Tinggal",
            "Over Prun",
            "Pelepah Sengkel",
            "Susunan Pelepah",
            "Buah Matahari",
            "Brdl Di TPH"
    };

    public AncakTableViewModel(Context context, ArrayList<QcAncak> hQcAncak) {
        mContext = context;
        qcAncaks = new ArrayList<>();
        qcAncaks = hQcAncak;
    }

    private List<TableViewRowHeader> getSimpleRowHeaderList() {
        List<TableViewRowHeader> list = new ArrayList<>();
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
        for (int i = 0; i < qcAncaks.size(); i++) {
            String id = i + "-" + qcAncaks.get(i).getIdQcAncak() + "-" + qcAncaks.get(i).getStatus();
            TableViewRowHeader header = new TableViewRowHeader(id, timeFormat.format(qcAncaks.get(i).getFinishTime()));
            list.add(header);
        }

        return list;
    }

    /**
     * This is a dummy model list test some cases.
     */
    private List<TableViewColumnHeader> getRandomColumnHeaderList() {
        List<TableViewColumnHeader> list = new ArrayList<>();

        for (int i = 0; i < HeaderColumn.length; i++) {
            TableViewColumnHeader header = new TableViewColumnHeader(String.valueOf(i), HeaderColumn[i]);
            list.add(header);
        }

        return list;
    }

    /**
     * This is a dummy model list test some cases.
     */
    private List<List<TableViewCell>> getCellListForSortingTest() {
        List<List<TableViewCell>> list = new ArrayList<>();
        for (int i = 0; i < qcAncaks.size(); i++) {

            int pokokPanen = 0;
            int totalBuahTinggalSegar = 0;
            int totalBuahTinggalBusuk  = 0;
            int totalBrondolanSegarTinggal  = 0;
            int totalBrondolanLamaTinggal  = 0;

            for(int x = 0 ; x < qcAncaks.get(i).getQcAncakPohons().size(); x++){
                QcAncakPohon qcAncakPohon = qcAncaks.get(i).getQcAncakPohons().get(x);
                if(qcAncakPohon.getJanjangPanen() > 0){
                    pokokPanen++;
                }
                totalBuahTinggalSegar += qcAncakPohon.getBuahTinggalSegar();
                totalBuahTinggalBusuk += qcAncakPohon.getBuahTinggalBusuk();
                totalBrondolanSegarTinggal += qcAncakPohon.getBrondolanSegarTinggal();
                totalBrondolanLamaTinggal += qcAncakPohon.getBrondolanLamaTinggal();
            }

            List<TableViewCell> cellList = new ArrayList<>();
            for (int j = 0; j < HeaderColumn.length; j++) {
                // Create dummy id.
                String id = j + "-" + qcAncaks.get(i).getIdQcAncak() + "-" + qcAncaks.get(i).getStatus() ;
                TableViewCell cell = null;
                switch (j){
                    case 0:
                        cell = new TableViewCell(id, qcAncaks.get(i).getTph().getBlock());
                        break;
                    case 1:
                        cell = new TableViewCell(id, qcAncaks.get(i).getTph().getNamaTph());
                        break;
                    case 2:
                        cell = new TableViewCell(id, qcAncaks.get(i).getKondisiAncakText() == null ? "" : qcAncaks.get(i).getKondisiAncakText());
                        break;
                    case 3:
                        cell = new TableViewCell(id, qcAncaks.get(i).getQcAncakPohons().size());
                        break;
                    case 4:
                        cell = new TableViewCell(id, pokokPanen);
                        break;
                    case 5:
                        cell = new TableViewCell(id, qcAncaks.get(i).getTotalJanjangPanen());
                        break;
                    case 6:
                        cell = new TableViewCell(id, qcAncaks.get(i).getTotalBuahTinggal());
                        break;
                    case 7:
                        cell = new TableViewCell(id, totalBuahTinggalSegar);
                        break;
                    case 8:
                        cell = new TableViewCell(id, totalBuahTinggalBusuk);
                        break;
                    case 9:
                        cell = new TableViewCell(id, qcAncaks.get(i).getTotalBrondolan());
                        break;
                    case 10:
                        cell = new TableViewCell(id, totalBrondolanSegarTinggal);
                        break;
                    case 11:
                        cell = new TableViewCell(id, totalBrondolanLamaTinggal);
                        break;
                    case 12:
                        cell = new TableViewCell(id, qcAncaks.get(i).getTotalOverPrun());
                        break;
                    case 13:
                        cell = new TableViewCell(id, qcAncaks.get(i).getTotalPelepahSengkelan());
                        break;
                    case 14:
                        cell = new TableViewCell(id, qcAncaks.get(i).getTotalSusunanPelepah());
                        break;
                    case 15:
                        cell = new TableViewCell(id, qcAncaks.get(i).getTotalBuahMatahari());
                        break;
                    case 16:
                        cell = new TableViewCell(id, qcAncaks.get(i).getBrondolanDiTph());
                        break;
                }
                cellList.add(cell);
            }
            list.add(cellList);
        }

        return list;
    }

//    /**
//     * This is a dummy model list test some cases.
//     */
//    private List<List<TableViewCell>> getRandomCellList() {
//        List<List<TableViewCell>> list = new ArrayList<>();
//        for (int i = 0; i < ROW_SIZE; i++) {
//            List<TableViewCell> cellList = new ArrayList<>();
//            list.add(cellList);
//            for (int j = 0; j < COLUMN_SIZE; j++) {
//                String text = "cell " + j + " " + i;
//                int random = new Random().nextInt();
//                if (random % 2 == 0 || random % 5 == 0 || random == j) {
//                    text = "large cell  " + j + " " + i + getRandomString() + ".";
//                }
//
//                // Create dummy id.
//                String id = j + "-" + i;
//
//                TableViewCell cell = new TableViewCell(id, text);
//                cellList.add(cell);
//            }
//        }
//
//        return list;
//    }
//
//    /**
//     * This is a dummy model list test some cases.
//     */
//    private List<List<TableViewCell>> getEmptyCellList() {
//        List<List<TableViewCell>> list = new ArrayList<>();
//        for (int i = 0; i < ROW_SIZE; i++) {
//            List<TableViewCell> cellList = new ArrayList<>();
//            list.add(cellList);
//            for (int j = 0; j < COLUMN_SIZE; j++) {
//
//                // Create dummy id.
//                String id = j + "-" + i;
//
//                TableViewCell cell = new TableViewCell(id, "");
//                cellList.add(cell);
//            }
//        }
//
//        return list;
//    }
//
//    private List<TableViewRowHeader> getEmptyRowHeaderList() {
//        List<TableViewRowHeader> list = new ArrayList<>();
//        for (int i = 0; i < ROW_SIZE; i++) {
//            TableViewRowHeader header = new TableViewRowHeader(String.valueOf(i), "");
//            list.add(header);
//        }
//
//        return list;
//    }
//
//    /**
//     * This is a dummy model list test some cases.
//     */
//    public static List<List<TableViewCell>> getRandomCellList(int startIndex) {
//        List<List<TableViewCell>> list = new ArrayList<>();
//        for (int i = 0; i < ROW_SIZE; i++) {
//            List<Cell> cellList = new ArrayList<>();
//            list.add(cellList);
//            for (int j = 0; j < COLUMN_SIZE; j++) {
//                String text = "cell " + j + " " + (i + startIndex);
//                int random = new Random().nextInt();
//                if (random % 2 == 0 || random % 5 == 0 || random == j) {
//                    text = "large cell  " + j + " " + (i + startIndex) + getRandomString() + ".";
//                }
//
//                String id = j + "-" + (i + startIndex);
//
//                Cell cell = new Cell(id, text);
//                cellList.add(cell);
//            }
//        }
//
//        return list;
//    }


    private static String getRandomString() {
        Random r = new Random();
        String str = " a ";
        for (int i = 0; i < r.nextInt(); i++) {
            str = str + " a ";
        }

        return str;
    }

//    public Drawable getDrawable(int value, boolean isGender) {
//        if (isGender) {
//            return value == BOY ? mBoyDrawable : mGirlDrawable;
//        } else {
//            return value == SAD ? mSadDrawable : mHappyDrawable;
//        }
//    }

    public List<List<TableViewCell>> getCellList() {
        return getCellListForSortingTest();
    }

    public List<TableViewRowHeader> getRowHeaderList() {
        return getSimpleRowHeaderList();
    }

    public List<TableViewColumnHeader> getColumnHeaderList() {
        return getRandomColumnHeaderList();
    }

}