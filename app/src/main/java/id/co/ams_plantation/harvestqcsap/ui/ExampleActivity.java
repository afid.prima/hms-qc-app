package id.co.ams_plantation.harvestqcsap.ui;

import id.co.ams_plantation.harvestqcsap.model.ExampleModel;
import id.co.ams_plantation.harvestqcsap.presenter.ExamplePresenter;
import id.co.ams_plantation.harvestqcsap.view.ExampleView;

//1. Declare your presenter on this class
//2. Extend BaseActivity
//3. Implements Class View on this class
//
public class ExampleActivity extends BaseActivity implements ExampleView {
    ExamplePresenter presenter;
    @Override
    protected void initPresenter() {
        //init your presenter here
        presenter = new ExamplePresenter(this);
    }

    @Override
    public void setExampleScreen(ExampleModel exampleModel) {
        //Override from ExampleView
    }
}
