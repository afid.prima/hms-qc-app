package id.co.ams_plantation.harvestqcsap.TableView.view_model;

import android.content.Context;

import java.net.NetworkInterface;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import id.co.ams_plantation.harvestqcsap.TableView.model.TableViewCell;
import id.co.ams_plantation.harvestqcsap.TableView.model.TableViewColumnHeader;
import id.co.ams_plantation.harvestqcsap.TableView.model.TableViewRowHeader;
import id.co.ams_plantation.harvestqcsap.model.QcAncakPohon;

/**
 * Created by user on 12/24/2018.
 */

public class AncakPohonTableViewModel {
    private Context mContext;
    private ArrayList<QcAncakPohon> qcPohon;
    String [] HeaderColumn = {
            "Baris","Pohon","Janjang Panen",
            "Buah Tinggal","Buah Tinggal Segar","Buah Tinggal Busuk",
            "Brondolan Tinggal","Brondolan Segar Tinggal","Brondolan Lama Tinggal","Over Prun",
            "Pelepah Sengkel","Susunan Pelepah","Buah Matahari"
    };

    public AncakPohonTableViewModel(Context context, ArrayList<QcAncakPohon> hQcPohon) {
        mContext = context;
        qcPohon = new ArrayList<>();
        qcPohon = hQcPohon;
    }

    private List<TableViewRowHeader> getSimpleRowHeaderList() {
        List<TableViewRowHeader> list = new ArrayList<>();
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
        for (int i = 0; i < qcPohon.size(); i++) {
            String id = i + "-" + qcPohon.get(i).getIdQcAncakPohon() ;
            TableViewRowHeader header = new TableViewRowHeader(id, timeFormat.format(qcPohon.get(i).getWaktuQcAncakPohon()));
            list.add(header);
        }

        return list;
    }

    /**
     * This is a dummy model list test some cases.
     */
    private List<TableViewColumnHeader> getRandomColumnHeaderList() {
        List<TableViewColumnHeader> list = new ArrayList<>();

        for (int i = 0; i < HeaderColumn.length; i++) {
            TableViewColumnHeader header = new TableViewColumnHeader(String.valueOf(i), HeaderColumn[i]);
            list.add(header);
        }

        return list;
    }

    /**
     * This is a dummy model list test some cases.
     */
    private List<List<TableViewCell>> getCellListForSortingTest() {
        List<List<TableViewCell>> list = new ArrayList<>();
        for (int i = 0; i < qcPohon.size(); i++) {
            List<TableViewCell> cellList = new ArrayList<>();
            for (int j = 0; j < HeaderColumn.length; j++) {
                // Create dummy id.
                String id = j + "-" + qcPohon.get(i).getIdQcAncakPohon() ;
                TableViewCell cell = null;
                int nilai = 0;
                switch (j){
                    case 0:
                        cell = new TableViewCell(id, qcPohon.get(i).getNoBaris());
                        break;
                    case 1:
                        cell = new TableViewCell(id, qcPohon.get(i).getIdPohon());
                        break;
                    case 2:
                        cell = new TableViewCell(id, qcPohon.get(i).getJanjangPanen());
                        break;
                    case 3:
                        cell = new TableViewCell(id, qcPohon.get(i).getBuahTinggal());
                        break;
                    case 4:
                        cell = new TableViewCell(id, qcPohon.get(i).getBuahTinggalSegar());
                        break;
                    case 5:
                        cell = new TableViewCell(id, qcPohon.get(i).getBuahTinggalBusuk());
                        break;
                    case 6:
                        cell = new TableViewCell(id, qcPohon.get(i).getBrondolanTinggal());
                        break;
                    case 7:
                        cell = new TableViewCell(id, qcPohon.get(i).getBrondolanSegarTinggal());
                        break;
                    case 8:
                        cell = new TableViewCell(id, qcPohon.get(i).getBrondolanLamaTinggal());
                        break;
                    case 9:
                        nilai = 0;
                        if(qcPohon.get(i).isOverPrun()){
                            nilai = 1;
                        }
                        cell = new TableViewCell(id, nilai);
                        break;
                    case 10:
                        nilai = 0;
                        if(qcPohon.get(i).isPelepahSengkel()){
                            nilai = 1;
                        }
                        cell = new TableViewCell(id, nilai);
                        break;
                    case 11:
                        nilai = 0;
                        if(qcPohon.get(i).isSusunanPelepah()){
                            nilai = 1;
                        }
                        cell = new TableViewCell(id, nilai);
                        break;
                    case 12:
                        cell = new TableViewCell(id, qcPohon.get(i).getBuahMatahari());
                        break;
                }
                cellList.add(cell);
            }
            list.add(cellList);
        }

        return list;
    }

//    /**
//     * This is a dummy model list test some cases.
//     */
//    private List<List<TableViewCell>> getRandomCellList() {
//        List<List<TableViewCell>> list = new ArrayList<>();
//        for (int i = 0; i < ROW_SIZE; i++) {
//            List<TableViewCell> cellList = new ArrayList<>();
//            list.add(cellList);
//            for (int j = 0; j < COLUMN_SIZE; j++) {
//                String text = "cell " + j + " " + i;
//                int random = new Random().nextInt();
//                if (random % 2 == 0 || random % 5 == 0 || random == j) {
//                    text = "large cell  " + j + " " + i + getRandomString() + ".";
//                }
//
//                // Create dummy id.
//                String id = j + "-" + i;
//
//                TableViewCell cell = new TableViewCell(id, text);
//                cellList.add(cell);
//            }
//        }
//
//        return list;
//    }
//
//    /**
//     * This is a dummy model list test some cases.
//     */
//    private List<List<TableViewCell>> getEmptyCellList() {
//        List<List<TableViewCell>> list = new ArrayList<>();
//        for (int i = 0; i < ROW_SIZE; i++) {
//            List<TableViewCell> cellList = new ArrayList<>();
//            list.add(cellList);
//            for (int j = 0; j < COLUMN_SIZE; j++) {
//
//                // Create dummy id.
//                String id = j + "-" + i;
//
//                TableViewCell cell = new TableViewCell(id, "");
//                cellList.add(cell);
//            }
//        }
//
//        return list;
//    }
//
//    private List<TableViewRowHeader> getEmptyRowHeaderList() {
//        List<TableViewRowHeader> list = new ArrayList<>();
//        for (int i = 0; i < ROW_SIZE; i++) {
//            TableViewRowHeader header = new TableViewRowHeader(String.valueOf(i), "");
//            list.add(header);
//        }
//
//        return list;
//    }
//
//    /**
//     * This is a dummy model list test some cases.
//     */
//    public static List<List<TableViewCell>> getRandomCellList(int startIndex) {
//        List<List<TableViewCell>> list = new ArrayList<>();
//        for (int i = 0; i < ROW_SIZE; i++) {
//            List<Cell> cellList = new ArrayList<>();
//            list.add(cellList);
//            for (int j = 0; j < COLUMN_SIZE; j++) {
//                String text = "cell " + j + " " + (i + startIndex);
//                int random = new Random().nextInt();
//                if (random % 2 == 0 || random % 5 == 0 || random == j) {
//                    text = "large cell  " + j + " " + (i + startIndex) + getRandomString() + ".";
//                }
//
//                String id = j + "-" + (i + startIndex);
//
//                Cell cell = new Cell(id, text);
//                cellList.add(cell);
//            }
//        }
//
//        return list;
//    }


    private static String getRandomString() {
        Random r = new Random();
        String str = " a ";
        for (int i = 0; i < r.nextInt(); i++) {
            str = str + " a ";
        }

        return str;
    }

//    public Drawable getDrawable(int value, boolean isGender) {
//        if (isGender) {
//            return value == BOY ? mBoyDrawable : mGirlDrawable;
//        } else {
//            return value == SAD ? mSadDrawable : mHappyDrawable;
//        }
//    }

    public List<List<TableViewCell>> getCellList() {
        return getCellListForSortingTest();
    }

    public List<TableViewRowHeader> getRowHeaderList() {
        return getSimpleRowHeaderList();
    }

    public List<TableViewColumnHeader> getColumnHeaderList() {
        return getRandomColumnHeaderList();
    }

}