package id.co.ams_plantation.harvestqcsap.model;

import java.util.ArrayList;

import id.co.ams_plantation.harvestqcsap.HarvestApp;
import id.co.ams_plantation.harvestqcsap.R;

/**
 * Created on : 25,October,2021
 * Author     : Afid
 */

public class SessionQcFotoPanen {

    public static final int Status_Deleted = -1;
    public static final int Status_OutStanding_Qc = 0;
    public static final int Status_Done_Qc = 1;
    public static final int Status_Upload_Qc = 2;

    public static final int COLOR_OutStanding = HarvestApp.getContext().getResources().getColor(R.color.OrangeRed);
    public static final int COLOR_Done = HarvestApp.getContext().getResources().getColor(R.color.Yellow);
    public static final int COLOR_Upload = HarvestApp.getContext().getResources().getColor(R.color.SkyBlue);

    String idSession;
    String createBy;
    String requestDate;
    String estCode;
    String afdeling;
    Answer methode;
    Answer selection;
    ChoiceSelection sectionValue;
    int sample;
    long createDate;
    long updateDate;
    int status;
    ArrayList<QcFotoPanen> qcFotoPanen;
    String urlDownload;
    String filePDF;

    public SessionQcFotoPanen() {
    }

    public SessionQcFotoPanen(String idSession, String createBy, String requestDate, String estCode, String afdeling, Answer methode, Answer selection, ChoiceSelection sectionValue, int sample, long createDate, long updateDate, int status) {
        this.idSession = idSession;
        this.createBy = createBy;
        this.requestDate = requestDate;
        this.estCode = estCode;
        this.afdeling = afdeling;
        this.methode = methode;
        this.selection = selection;
        this.sectionValue = sectionValue;
        this.sample = sample;
        this.createDate = createDate;
        this.updateDate = updateDate;
        this.status = status;
    }

    public String getIdSession() {
        return idSession;
    }

    public void setIdSession(String idSession) {
        this.idSession = idSession;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(String requestDate) {
        this.requestDate = requestDate;
    }

    public String getEstCode() {
        return estCode;
    }

    public void setEstCode(String estCode) {
        this.estCode = estCode;
    }

    public String getAfdeling() {
        return afdeling;
    }

    public void setAfdeling(String afdeling) {
        this.afdeling = afdeling;
    }

    public Answer getMethode() {
        return methode;
    }

    public void setMethode(Answer methode) {
        this.methode = methode;
    }

    public Answer getSelection() {
        return selection;
    }

    public void setSelection(Answer selection) {
        this.selection = selection;
    }

    public ChoiceSelection getSectionValue() {
        return sectionValue;
    }

    public void setSectionValue(ChoiceSelection sectionValue) {
        this.sectionValue = sectionValue;
    }

    public int getSample() {
        return sample;
    }

    public void setSample(int sample) {
        this.sample = sample;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(long updateDate) {
        this.updateDate = updateDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<QcFotoPanen> getQcFotoPanen() {
        return qcFotoPanen;
    }

    public void setQcFotoPanen(ArrayList<QcFotoPanen> qcFotoPanen) {
        this.qcFotoPanen = qcFotoPanen;
    }

    public String getUrlDownload() {
        return urlDownload;
    }

    public void setUrlDownload(String urlDownload) {
        this.urlDownload = urlDownload;
    }

    public String getFilePDF() {
        return filePDF;
    }

    public void setFilePDF(String filePDF) {
        this.filePDF = filePDF;
    }
}
