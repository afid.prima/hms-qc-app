package id.co.ams_plantation.harvestqcsap.model;

import java.util.HashMap;

public class LastSyncTime {

    Estate estate;
    HashMap<String,SyncTime> syncTimeHashMap;

    public LastSyncTime() {
    }

    public LastSyncTime(Estate estate, HashMap<String, SyncTime> syncTimeHashMap) {
        this.estate = estate;
        this.syncTimeHashMap = syncTimeHashMap;
    }

    public HashMap<String, SyncTime> getSyncTimeHashMap() {
        return syncTimeHashMap;
    }

    public void setSyncTimeHashMap(HashMap<String, SyncTime> syncTimeHashMap) {
        this.syncTimeHashMap = syncTimeHashMap;
    }

    public Estate getEstate() {
        return estate;
    }

    public void setEstate(Estate estate) {
        this.estate = estate;
    }
}
