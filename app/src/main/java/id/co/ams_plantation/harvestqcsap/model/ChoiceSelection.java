package id.co.ams_plantation.harvestqcsap.model;

/**
 * Created on : 15,October,2021
 * Author     : Afid
 */

public class ChoiceSelection {

    private String type;
    private String value;
    private String text;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}