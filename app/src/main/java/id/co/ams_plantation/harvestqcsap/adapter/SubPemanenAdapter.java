package id.co.ams_plantation.harvestqcsap.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import id.co.ams_plantation.harvestqcsap.Fragment.TphInputFragment;
import id.co.ams_plantation.harvestqcsap.HarvestApp;
import id.co.ams_plantation.harvestqcsap.SwipeItemRecycleView.SwipeControllerInterface;
import id.co.ams_plantation.harvestqcsap.model.Pemanen;
import id.co.ams_plantation.harvestqcsap.model.SubPemanen;
import id.co.ams_plantation.harvestqcsap.ui.BaseActivity;
import id.co.ams_plantation.harvestqcsap.util.CompleteTextViewHelper;
import id.co.ams_plantation.harvestqcsap.util.GlobalHelper;
import id.co.ams_plantation.harvestqcsap.util.PemanenHelper;
import id.co.ams_plantation.harvestqcsap.util.SharedPrefHelper;
import id.co.ams_plantation.harvestqcsap.R;

public class SubPemanenAdapter extends RecyclerView.Adapter<SubPemanenAdapter.Holder> implements SwipeControllerInterface {
    Context context;
    public boolean updateValue;
    public ArrayList<SubPemanen> subPemanens;
    PemanenHelper pemanenHelper;
    String gangSelected;
    SharedPrefHelper sharedPrefHelper;
    public ArrayList<String> pemanenHasBeenSelected;
    ArrayList<Integer> persentase;
    List<String> bTipe;
    ArrayAdapter<String> adapterTipe;

    String TAG = this.getClass().getName().toString();

    public SubPemanenAdapter(Context context, ArrayList<SubPemanen> subPemanens) {
        this.context = context;
        this.subPemanens = subPemanens;
        updateValue = false;
        pemanenHelper = new PemanenHelper((BaseActivity)context);
        persentase = new ArrayList<>();
        pemanenHasBeenSelected = new ArrayList<>();
        sharedPrefHelper = new SharedPrefHelper(this.context);
        gangSelected = sharedPrefHelper.getName(SharedPrefHelper.KEY_GENGSELECTED);

        bTipe = new ArrayList<>();
        for(int i = 0 ; i < SubPemanen.arrayCodeTipePemanen.length;i++){
            bTipe.add(SubPemanen.arrayCodeTipePemanen[i]);
        }

        adapterTipe = new ArrayAdapter<String>(context,android.R.layout.simple_dropdown_item_1line, bTipe);
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.pemanen_transasi_item, parent, false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.setValue(subPemanens.get(position));
    }

    @Override
    public int getItemCount() {
        return subPemanens != null ? subPemanens.size() : 0;
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {

    }

    public class Holder extends RecyclerView.ViewHolder {
        CompleteTextViewHelper etGang;
        CompleteTextViewHelper etPemanen;
        AppCompatEditText etPersentase;
        AppCompatEditText etTotalJanjangSubPemanen;
        AppCompatEditText etTotalBrondolanSubPemanen;
        AppCompatEditText etTotalPdtSubPemanen;
        AppCompatEditText etNormalSubPemanen;
        AppCompatEditText etBuahMentahSubPemanen;
        AppCompatEditText etBusukSubPemanen;
        AppCompatEditText etLewatMatangSubPemanen;
        AppCompatEditText etAbnormalSubPemanen;
        AppCompatEditText etTangkaiPanjangSubPemanen;
        AppCompatEditText etBuahDimakanTikusSubPemanen;
        CompleteTextViewHelper lsTipe;
        TextView tvValue;
        TextView tvPemanen;

        SelectGangAdapter adapterGang;
        SelectPemanenAdapter adapterPemanen;

        public Holder(View view) {
            super(view);
            etGang = view.findViewById(R.id.etGangSub);
            etPemanen = view.findViewById(R.id.etPemanenSub);
            etPersentase = view.findViewById(R.id.etPersentase);
            etTotalJanjangSubPemanen = view.findViewById(R.id.etTotalJanjangSubPemanen);
            etTotalPdtSubPemanen = view.findViewById(R.id.etTotalPdtSubPemanen);
            etTotalBrondolanSubPemanen = view.findViewById(R.id.etTotalBrondolanSubPemanen);
            etNormalSubPemanen = view.findViewById(R.id.etNormalSubPemanen);
            etBuahMentahSubPemanen = view.findViewById(R.id.etBuahMentahSubPemanen);
            etBusukSubPemanen = view.findViewById(R.id.etBusukSubPemanen);
            etLewatMatangSubPemanen = view.findViewById(R.id.etLewatMatangSubPemanen);
            etAbnormalSubPemanen = view.findViewById(R.id.etAbnormalSubPemanen);
            etTangkaiPanjangSubPemanen = view.findViewById(R.id.etTangkaiPanjangSubPemanen);
            etBuahDimakanTikusSubPemanen = view.findViewById(R.id.etBuahDimakanTikusSubPemanen);
            lsTipe = view.findViewById(R.id.lsTipe);
            tvValue = view.findViewById(R.id.tvValue);
            tvPemanen = view.findViewById(R.id.tvPemanen);

            lsTipe.setAdapter(adapterTipe);
            lsTipe.setText(bTipe.get(SubPemanen.idPemnaen));

            etPersentase.post(new Runnable() {
                @Override
                public void run() {
                    etPersentase.setSelection(etPersentase.getText().length());
                }
            });
        }

        public void setValue(final SubPemanen value) {

            Log.d(this.getClass() + " setValue 1",String.valueOf(value.getNoUrutKongsi()));
            Gson gson = new Gson();
            if (value.getPemanen() != null) {

                if(value.getPemanen().getGank() != null) {
                    Log.d(this.getClass() + " setValue 1",value.getPemanen().getGank());
                    etGang.setText(value.getPemanen().getGank());
                }else {
                    if (gangSelected != null) {
                        etGang.setText(gangSelected);
                    }else {
                        etGang.setText("");
                    }
                }

                if(value.getPemanen().getNama() != null){
                    Log.d(this.getClass() + " setValue 1",value.getPemanen().getNama());
                    etPemanen.setText(value.getPemanen().getNama());
                    tvValue.setText(gson.toJson(value.getPemanen()));
                    if(value.getPemanen().getGank() != null && value.getPemanen().getNik() != null && value.getPemanen().getNama() != null){
                        tvPemanen.setText(value.getPemanen().getGank() + " - " + value.getPemanen().getNik() +" - "+ value.getPemanen().getNama());
                    }
                    pemanenHasBeenSelected.add(value.getPemanen().getKodePemanen());
                }else{
                    etPemanen.setText("");
                }
            }

            etPersentase.setText(String.valueOf(value.getPersentase()));
            etTotalJanjangSubPemanen.setText(String.valueOf(value.getHasilPanen().getTotalJanjang()));
            etNormalSubPemanen.setText(String.valueOf(value.getHasilPanen().getJanjangNormal()));
            etBuahMentahSubPemanen.setText(String.valueOf(value.getHasilPanen().getBuahMentah()));
            etBusukSubPemanen.setText(String.valueOf(value.getHasilPanen().getBusukNJangkos()));
            etLewatMatangSubPemanen.setText(String.valueOf(value.getHasilPanen().getBuahLewatMatang()));
            etAbnormalSubPemanen.setText(String.valueOf(value.getHasilPanen().getBuahAbnormal()));
            etTangkaiPanjangSubPemanen.setText(String.valueOf(value.getHasilPanen().getTangkaiPanjang()));
            etBuahDimakanTikusSubPemanen.setText(String.valueOf(value.getHasilPanen().getBuahDimakanTikus()));
            etTotalBrondolanSubPemanen.setText(String.valueOf(value.getHasilPanen().getBrondolan()));
            etTotalPdtSubPemanen.setText(String.valueOf(value.getHasilPanen().getTotalJanjangPendapatan()));

            HashMap<String, Pemanen> sGang = new HashMap<>();
            if(GlobalHelper.dataPemanen.size() > 0) {
                for (HashMap.Entry<String, Pemanen> entry : GlobalHelper.dataPemanen.entrySet()) {
                    sGang.put(entry.getValue().getGank(), entry.getValue());
                }
                adapterGang = new SelectGangAdapter(context, R.layout.row_setting, new ArrayList<Pemanen>(sGang.values()));
                etGang.setThreshold(1);
                etGang.setAdapter(adapterGang);
                adapterGang.notifyDataSetChanged();
            }

            if(gangSelected != null){
                Pemanen adaPemanen = sGang.get(gangSelected);
                if (adaPemanen != null) {
                    if(etGang.getText().toString().isEmpty()) {
                        etGang.setText(gangSelected);
                    }
                    adapterPemanen = new SelectPemanenAdapter(context, R.layout.row_record,
                            pemanenHelper.getListPemanenByGangWithCompare(GlobalHelper.dataPemanen,
                            pemanenHasBeenSelected,
                            etGang.getText().toString())
                    );
                    etPemanen.setThreshold(1);
                    etPemanen.setAdapter(adapterPemanen);
                    adapterPemanen.notifyDataSetChanged();
                }
            }

            lsTipe.setText(bTipe.get(value.getTipePemanen()));

            etGang.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    adapterGang = (SelectGangAdapter) adapterView.getAdapter();
                    gangSelected =  adapterGang.getItemAt(i).getGank();

                    if(!gangSelected.contains("PN")){
                        AlertDialog alertDialog = new AlertDialog.Builder(context, R.style.MyAlertDialogStyle)
                                .setTitle("Perhatian")
                                .setMessage("Apakah Anda Yakin Untuk Simpan Bukan Gang Panen")
                                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        etGang.setText("");
                                        etPemanen.setText("");
                                        tvValue.setText("");
                                    }
                                })
                                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();

                                        setGang(gangSelected);
                                    }
                                })
                                .create();
                        alertDialog.show();
                    }else{
                        setGang(gangSelected);
                    }
                }
            });

            etGang.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    etGang.showDropDown();
                }
            });

            etGang.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(!hasFocus){
                        if(gangSelected == null){
                            etGang.setText("");
                        }else{
                            etGang.setText(gangSelected);
                        }
                    }
                }
            });

            etPemanen.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                    if(!tvValue.getText().toString().isEmpty()) {
                        Pemanen pemanenX = gson.fromJson(tvValue.getText().toString(),Pemanen.class);
                        for (int i = 0; i < pemanenHasBeenSelected.size(); i++) {
                            if (pemanenX.getKodePemanen().equalsIgnoreCase(pemanenHasBeenSelected.get(i))){
                                pemanenHasBeenSelected.remove(i);
                                break;
                            }
                        }
                    }

                    adapterPemanen = (SelectPemanenAdapter) adapterView.getAdapter();
                    Pemanen pemanen = adapterPemanen.getItemAt(position);
                    if(pemanen.getGank() != null){
                        if(!pemanen.getGank().contains("PN")){
                            AlertDialog alertDialog = new AlertDialog.Builder(context, R.style.MyAlertDialogStyle)
                                    .setTitle("Perhatian")
                                    .setMessage("Apakah Anda Yakin Untuk Simpan Pemanen Bukan Gang Panen")
                                    .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            etPemanen.setText("");
                                            tvValue.setText("");
                                        }
                                    })
                                    .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            setPemanen(adapterPemanen.getItemAt(position));
                                        }
                                    })
                                    .create();
                            alertDialog.show();
                        }else{
                            setPemanen(adapterPemanen.getItemAt(position));
                        }
                    }else {
                        setPemanen(adapterPemanen.getItemAt(position));
                    }
                }
            });

            etPersentase.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if(!tvValue.getText().toString().isEmpty() && updateValue) {
                        if (s.toString().matches(".*\\d.*")) {
                            try {
                                if(Integer.parseInt(etPersentase.getText().toString()) > 100){
                                    etPersentase.setText("100");
                                }else {
//                                    if (context instanceof TphActivity) {
//                                        if (((TphActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof TphInputFragment) {
//                                            TphInputFragment fragment = (TphInputFragment) ((TphActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container);
////                                    int cekPersen = fragment.cekPersentase();
////                                    if (cekPersen == 0) {
//                                            fragment.distribusiJanjangSubPemanen(false);
////                                    } else {
////                                        int ubahPersen = persen - cekPersen;
////                                        etPersentase.setText(String.valueOf(ubahPersen));
////                                    }
//                                        }
//                                    }
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                                etPersentase.setText("0");
                            }
                        }
                    }else{
                        etPemanen.requestFocus();
                    }
                }
            });

            etPersentase.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus) {
                        updateValue = true;
                    }else{
                        updateValue = false;
                    }
                    Log.d(TAG, "onFocusChange: "+ updateValue);
                }
            });

            lsTipe.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lsTipe.setText("");
                    lsTipe.showDropDown();
                }
            });
            lsTipe.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Gson gson = new Gson();
                    Pemanen pemanen = gson.fromJson(tvValue.getText().toString(), Pemanen.class);
                    if(pemanen != null) {
//                        if (context instanceof TphActivity) {
//                            if (((TphActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof TphInputFragment) {
//                                TphInputFragment fragment = (TphInputFragment) ((TphActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container);
//                                fragment.distribusiJanjangSubPemanen(true);
//                            }
//                        }
                    }else{
                        etPemanen.requestFocus();
                        Toast.makeText(HarvestApp.getContext(),"Mohon Pilih Pemanen Dahulu",Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        private void setGang(String Gang){
            etGang.setText(Gang);
            sharedPrefHelper.commit(SharedPrefHelper.KEY_GENGSELECTED,Gang);
//                        File fileGangSelected = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_PANEN] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_PANEN_GANG] );
//                        GlobalHelper.writeFileContent(fileGangSelected.getAbsolutePath(),etGang.getText().toString());

            adapterPemanen = new SelectPemanenAdapter(context, R.layout.row_record,
                    pemanenHelper.getListPemanenByGangWithCompare(GlobalHelper.dataPemanen,
                            pemanenHasBeenSelected,
                            etGang.getText().toString())
            );
            etPemanen.setThreshold(1);
            etPemanen.setAdapter(adapterPemanen);
            adapterPemanen.notifyDataSetChanged();
            etPemanen.requestFocus();
            etPemanen.callOnClick();
        }

        private void setPemanen(Pemanen pemanen){
            Gson gson = new Gson();
            etPemanen.setText(pemanen.getNama());
            tvValue.setText(gson.toJson(pemanen));
            pemanenHasBeenSelected.add(pemanen.getKodePemanen());
            tvPemanen.setText(pemanen.getGank() + " - " + pemanen.getNik() +" - "+ pemanen.getNama());

//            if (context instanceof TphActivity) {
//                if (((TphActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof TphInputFragment) {
//                    TphInputFragment fragment = (TphInputFragment) ((TphActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container);
//                    if (getAdapterPosition() == 0) {
//                        fragment.setPemanen(pemanen);
//                    }
//                    fragment.distribusiJanjangSubPemanen(true);
//                }
//
//            }
        }
    }
}
