package id.co.ams_plantation.harvestqcsap.presenter.base;

import id.co.ams_plantation.harvestqcsap.view.BaseView;


public class BasePresenter<V extends BaseView> {
    public V view;

    protected void attachView(V view) {
        this.view = view;
    }

    public void detachView() {
        view = null;
    }
}
