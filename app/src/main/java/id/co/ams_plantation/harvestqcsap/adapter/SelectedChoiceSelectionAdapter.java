package id.co.ams_plantation.harvestqcsap.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;

import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.model.ChoiceSelection;

/**
 * Created on : 15,October,2021
 * Author     : Afid
 */

public class SelectedChoiceSelectionAdapter extends ArrayAdapter<ChoiceSelection> {
    public ArrayList<ChoiceSelection> items;
    public ArrayList<ChoiceSelection> itemsAll;
    public ArrayList<ChoiceSelection> suggestions;
    private int viewResourceId;

    public SelectedChoiceSelectionAdapter(Context context, int viewResourceId, ArrayList<ChoiceSelection> data){
        super(context, viewResourceId, data);
        this.items = new ArrayList<>();
        this.items.addAll(data);
        this.itemsAll = (ArrayList<ChoiceSelection>)this.items.clone();
        this.suggestions = new ArrayList<>();
        this.viewResourceId = viewResourceId;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(viewResourceId, null);
        }
        if(items.size()>position){
            ChoiceSelection choiceSelection = items.get(position);
            if (choiceSelection != null) {
                TextView item_ket1 = (TextView) v.findViewById(R.id.item_ket1);
                item_ket1.setText(choiceSelection.getText());
            }
        }
        return v;
    }

    public ChoiceSelection getItemAt(int position){
        return items.get(position);
    }

    @Override
    public int getCount(){
        return items!=null?items.size():0;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            String str = ((ChoiceSelection)(resultValue)).getText();
            return str;
        }
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if(constraint != null) {
                suggestions.clear();
                for (ChoiceSelection choiceSelection : itemsAll) {
                    if(choiceSelection.getText().toLowerCase().contains(constraint.toString().toLowerCase())){
                        suggestions.add(choiceSelection);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<ChoiceSelection> filteredList = (ArrayList<ChoiceSelection>) results.values;
//            clear();
            items.clear();
//            itemsAll.clear();
            if(results != null && results.count > 0) {
//                addAll(filteredList);
//                itemsAll.addAll(filteredList);
//                for (Anggota anggota : filteredList){
//                    items.add(anggota);
//                }
                items.addAll(filteredList);
            }else{
//                clear();
                items.clear();
//                itemsAll.clear();
            }
            notifyDataSetChanged();
        }
    };
}