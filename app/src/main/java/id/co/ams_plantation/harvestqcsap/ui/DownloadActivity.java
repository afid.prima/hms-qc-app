package id.co.ams_plantation.harvestqcsap.ui;

import static id.co.ams_plantation.harvestqcsap.util.GlobalHelper.encryptString;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;
import com.downloader.Status;
import com.google.gson.Gson;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.TreeMap;

import co.ceryle.segmentedbutton.SegmentedButtonGroup;
import id.co.ams_plantation.harvestqcsap.HarvestApp;
import id.co.ams_plantation.harvestqcsap.R;
import id.co.ams_plantation.harvestqcsap.adapter.MapRVListAdapter;
import id.co.ams_plantation.harvestqcsap.adapter.RefreshHeaderRecycleView;
import id.co.ams_plantation.harvestqcsap.connection.ServiceResponse;
import id.co.ams_plantation.harvestqcsap.encryptor.Encrypts;
import id.co.ams_plantation.harvestqcsap.model.AppInfo;
import id.co.ams_plantation.harvestqcsap.model.Block;
import id.co.ams_plantation.harvestqcsap.presenter.ConnectionPresenter;
import id.co.ams_plantation.harvestqcsap.util.DownloadMapHelper;
import id.co.ams_plantation.harvestqcsap.util.GlobalHelper;
import id.co.ams_plantation.harvestqcsap.util.SetUpDataSyncHelper;
import id.co.ams_plantation.harvestqcsap.util.WidgetHelper;
import id.co.ams_plantation.harvestqcsap.view.DownloadView;
import pl.droidsonroids.gif.GifImageView;

/**
 * Created on : 20,December,2021
 * Author     : Afid
 */

public class DownloadActivity extends BaseActivity implements DownloadView {

    RecyclerView rv;
    SegmentedButtonGroup segmentedButtonGroup;
    LinearLayout lnoData;
    RefreshLayout refreshRv;
    ImageView ivUserEntry;
    ImageView ivCompanyEntry;
    TextView tvUserEntry;
    TextView tvCompanyEntry;
    TextView keterangan_entry;
    GifImageView giv_loader;
    CoordinatorLayout myCoordinatorLayout;
    ArrayList<AppInfo> mapFiles;

    ServiceResponse serviceResponse;
    ConnectionPresenter connectionPresenter;
    SetUpDataSyncHelper setUpDataSyncHelper;
    DownloadMapHelper downloadMapHelper;

    boolean disableRefresh;

    final int LongOperation_GetAllMapFilesByEstate = 0;
    final int LongOperation_SetUpAllMapFilesByEstate = 1;

    public static final int ButtonGroup_Lokal = 0;
    public static final int ButtonGroup_Public = 1;

    @Override
    protected void initPresenter() {
        connectionPresenter = new ConnectionPresenter(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.download_layout);
        if(getSupportActionBar()!=null)
            getSupportActionBar().hide();

        setUpDataSyncHelper = new SetUpDataSyncHelper(this);

        segmentedButtonGroup = findViewById(R.id.segmentedButtonGroup);
        rv = findViewById(R.id.rv);
        lnoData = findViewById(R.id.lnoData);
        refreshRv = findViewById(R.id.refreshRv);
        ivUserEntry =  findViewById(R.id.iv_user_entry);
        ivCompanyEntry = findViewById(R.id.iv_company_entry);
        tvUserEntry = findViewById(R.id.user_entry);
        tvCompanyEntry = findViewById(R.id.company_entry);
        keterangan_entry = findViewById(R.id.keterangan_entry);
        giv_loader = findViewById(R.id.giv_loader);
        myCoordinatorLayout = findViewById(R.id.myCoordinatorLayout);

        mapFiles = new ArrayList<>();
        disableRefresh = false;

        ivUserEntry.setImageDrawable(
                new IconicsDrawable(this)
                        .icon(MaterialDesignIconic.Icon.gmi_account)
                        .sizeDp(24)
                        .colorRes(R.color.Gray)
        );

        ivCompanyEntry.setImageDrawable(
                new IconicsDrawable(this)
                        .icon(MaterialDesignIconic.Icon.gmi_home)
                        .sizeDp(24)
                        .colorRes(R.color.Gray)
        );

        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
        if(preferences.getBoolean(HarvestApp.CONNECTION_PREF,false)){
            segmentedButtonGroup.setPosition(ButtonGroup_Public);
        }else{
            segmentedButtonGroup.setPosition(ButtonGroup_Lokal);
        }

        tvUserEntry.setText(GlobalHelper.getUser().getUserFullName());
        tvCompanyEntry.setText(GlobalHelper.getEstate().getCompanyShortName() + " - "
                + GlobalHelper.getEstate().getEstCode() + " : "
                + GlobalHelper.getEstate().getEstName());
        keterangan_entry.setText(this.getString(R.string.app_name));

        rv.setLayoutManager(new LinearLayoutManager(this));
        refreshRv.setEnableAutoLoadMore(true);
        refreshRv.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                refresh();
            }
        });
        refreshRv.setRefreshHeader(new RefreshHeaderRecycleView(this));
        refreshRv.setHeaderHeight(60);
        refreshRv.autoRefresh();

    }

    public void rowMapItemAction(AppInfo appInfo,int position){
        disableRefresh = true;
        if (Status.RUNNING == PRDownloader.getStatus(appInfo.getId())) {
            PRDownloader.pause(appInfo.getId());
            return;
        }

        if (Status.PAUSED == PRDownloader.getStatus(appInfo.getId())) {
            PRDownloader.resume(appInfo.getId());
            return;
        }
        appInfo.setId(
                PRDownloader.download(appInfo.getUrl(),
                        appInfo.getFilePath(),
                    encryptString(appInfo.getMaps().getFilename())
                )
                .build()
                .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                    @Override
                    public void onStartOrResume() {
                        if(isCurrentListViewItemVisible(position)){
                            final MapRVListAdapter.Holder holderMap = getViewHolder(position);
                            holderMap.btnDownload.setText("PAUSE");
                        }
                    }
                })
                .setOnPauseListener(new OnPauseListener() {
                    @Override
                    public void onPause() {
                        if(isCurrentListViewItemVisible(position)){
                            final MapRVListAdapter.Holder holderMap = getViewHolder(position);
                            holderMap.btnDownload.setText("RESUME");
                        }
                    }
                })
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel() {
                        if(isCurrentListViewItemVisible(position)){
                            final MapRVListAdapter.Holder holderMap = getViewHolder(position);
                            holderMap.btnDownload.setText("DOWNLOAD");
                            appInfo.setId(0);
                        }
                    }
                })
                .setOnProgressListener(new OnProgressListener() {
                    @Override
                    public void onProgress(Progress progress) {
                        if(isCurrentListViewItemVisible(position)){
                            final MapRVListAdapter.Holder holderMap = getViewHolder(position);
                            holderMap.map_tv_filesize.setText(
                                    DownloadMapHelper.fileSizeShorter(String.valueOf(progress.currentBytes),true)
                                    +"/"+
                                    DownloadMapHelper.fileSizeShorter(String.valueOf(progress.totalBytes),true)
                            );
                        }
                    }
                })
                .start(new OnDownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        if(isCurrentListViewItemVisible(position)){
                            final MapRVListAdapter.Holder holderMap = getViewHolder(position);
                            holderMap.btnDownload.setText("COMPLATE");
                            cekFile();
                        }
                    }

                    @Override
                    public void onError(Error error) {
                        if(isCurrentListViewItemVisible(position)){
                            final MapRVListAdapter.Holder holderMap = getViewHolder(position);
                            holderMap.btnDownload.setText("DOWNLOAD");
                            appInfo.setId(0);
                            holderMap.map_tv_filesize.setText(DownloadMapHelper.fileSizeShorter(String.valueOf(0),true));
                        }
                    }
                })
        );
    }

    private void cekFile(){
        String locKmlMap = GlobalHelper.decryptFiles(GlobalHelper.TYPE_KMLBLOCK);
        if(new File(locKmlMap).exists()) {
            String locVkmMap = GlobalHelper.decryptFiles(GlobalHelper.TYPE_VKM);
            if(new File(locVkmMap).exists()) {
                nextActivity();
            }else{
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Mohon Downlaod VKM Dahulu Sampai COMPLATE");
            }
        }else{
            WidgetHelper.showSnackBar(myCoordinatorLayout,"Mohon Downlaod Block Dahulu Sampai COMPLATE");
        }
    }

    private void nextActivity(){
        try {
            File cekFileLastSync = new File(GlobalHelper.getDatabasePathHMS(), Encrypts.encrypt(GlobalHelper.LAST_SYNC));
            String locKmlMap = GlobalHelper.decryptFiles(GlobalHelper.TYPE_KMLBLOCK);
            TreeMap<String, ArrayList<Block>> afdelingBlocks = GlobalHelper.getAllAfdelingBlock(new File(locKmlMap));
            JSONObject object = new JSONObject(afdelingBlocks);

            SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.AFDELING_BLOCK, MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(HarvestApp.AFDELING_BLOCK, object.toString());
            editor.apply();

            if (cekFileLastSync.exists()) {
                JSONObject jModule = GlobalHelper.getModule();
                if (jModule == null) {

                    WidgetHelper.showSnackBar(myCoordinatorLayout,getResources().getString(R.string.cannot_acses));
                    Intent intent = getPackageManager().getLaunchIntentForPackage("id.co.ams_plantation.amsadminapps");
//                        if(BuildConfig.BUILD_VARIANT.equals("dev")){
//                            intent = getPackageManager().getLaunchIntentForPackage("id.co.ams_plantation.amsadminapps.dev");
//                        }
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                    return;
                }
                if ((jModule.getString("mdlAccCode").equals("QHS3")) && (jModule.getString("subMdlAccCode").equals("P5"))) {
                    nextIntentAfterDownload("MapActivity");
                } else {
                    if (GlobalHelper.aktifKembali(jModule)) {
                        nextIntentAfterDownload("ActiveActivity");
                    } else {
                        GlobalHelper.setUpAllData();
                        nextIntentAfterDownload("MainMenuActivity");
                    }
                }
            } else {
                nextIntentAfterDownload("ActiveActivity");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void nextIntentAfterDownload(String nextActivity){
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        try {
            Long exptDate = sdf.parse(GlobalHelper.getUser().getExpirationDate()).getTime();
            long sisaHari = exptDate - System.currentTimeMillis();
            sisaHari = sisaHari / (24 * 60 * 60 * 1000);
            if(sisaHari <= 0 ){
                WidgetHelper.showSnackBar(myCoordinatorLayout,getResources().getString(R.string.token_expired));
                Intent intent = getPackageManager().getLaunchIntentForPackage("id.co.ams_plantation.amsadminapps");
//                if(BuildConfig.BUILD_VARIANT.equals("dev")){
//                    intent = getPackageManager().getLaunchIntentForPackage("id.co.ams_plantation.amsadminapps.dev");
//                }
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }else if(sisaHari <= GlobalHelper.NOTIF_TOKEN_DAYS){
                String redWord = sisaHari + " Hari Lagi";
                String pesan = String.format("Token Anda Akan Habis Dalam Waktu %,d Hari Lagi\n" +
                        "Harap Extand Token Di Admin Apps.\n"+
                        "Buka Admin Apps ?",sisaHari);
                SpannableString spanPesan = new SpannableString(pesan);
                spanPesan.setSpan(new ForegroundColorSpan(Color.RED),
                        spanPesan.toString().indexOf(redWord),
                        spanPesan.toString().indexOf(redWord) + redWord.length(),
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                AlertDialog alertDialog = new AlertDialog.Builder(this, R.style.MyAlertDialogStyle)
                        .setTitle("Perhatian")
                        .setMessage(spanPesan)
                        .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                switch (nextActivity){
                                    case "MapActivity":
                                        setIntent(DownloadActivity.this, MapActivity.class);
                                        break;
                                    case "ActiveActivity":
                                        setIntent(DownloadActivity.this,ActiveActivity.class);
                                        break;
                                    case "MainMenuActivity":
                                        setIntent(DownloadActivity.this, MainMenuActivity.class);
                                        break;
                                }
                            }
                        })
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Intent intent = getPackageManager().getLaunchIntentForPackage("id.co.ams_plantation.amsadminapps");
//                                if(BuildConfig.BUILD_VARIANT.equals("dev")){
//                                    intent = getPackageManager().getLaunchIntentForPackage("id.co.ams_plantation.amsadminapps.dev");
//                                }
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                finish();
                            }
                        })
                        .create();
                alertDialog.show();
            }else{
                Log.d("nextActivity",nextActivity);
                switch (nextActivity){
                    case "MapActivity":
                        setIntent(DownloadActivity.this, MapActivity.class);
                        break;
                    case "ActiveActivity":
                        setIntent(DownloadActivity.this,ActiveActivity.class);
                        break;
                    case "MainMenuActivity":
                        setIntent(DownloadActivity.this, MainMenuActivity.class);
                        break;
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
            WidgetHelper.showSnackBar(myCoordinatorLayout,"Get User Gagal");
            Intent intent = getPackageManager().getLaunchIntentForPackage("id.co.ams_plantation.amsadminapps");
//            if(BuildConfig.BUILD_VARIANT.equals("dev")){
//                intent = getPackageManager().getLaunchIntentForPackage("id.co.ams_plantation.amsadminapps.dev");
//            }
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
    }

    private boolean isCurrentListViewItemVisible(int position){
        LinearLayoutManager layoutManager = (LinearLayoutManager) rv.getLayoutManager();
        int first = layoutManager.findFirstVisibleItemPosition();
        int last = layoutManager.findLastVisibleItemPosition();
        return first <= position && position <=last;
    }

    private MapRVListAdapter.Holder getViewHolder(int position){
        return (MapRVListAdapter.Holder) rv.findViewHolderForLayoutPosition(position);
    }

    private void refresh() {
        if(!disableRefresh && mapFiles.size() == 0) {
            lnoData.setVisibility(View.GONE);
            refreshRv.getLayout().postDelayed(() -> {
                new LongOperation().execute(String.valueOf(LongOperation_GetAllMapFilesByEstate));
            }, 2000);
        }else{
            WidgetHelper.showSnackBar(myCoordinatorLayout,"List downlaod peta sudah muncul tidak perlu refresh list");
        }
    }

    private void setupAdapter(){
        MapRVListAdapter adapter = new MapRVListAdapter(this,mapFiles);
        rv.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        if(mapFiles.size() > 0){
            lnoData.setVisibility(View.GONE);
        }else{
            lnoData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void successResponse(ServiceResponse serviceResponse) {
        try {
            switch (serviceResponse.getTag()){
                case GetAllMapFilesByEstate:
                    this.serviceResponse = serviceResponse;
                    new LongOperation().execute(String.valueOf(LongOperation_SetUpAllMapFilesByEstate));
                    break;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void badResponse(ServiceResponse serviceResponse) {
        WidgetHelper.showSnackBar(myCoordinatorLayout,"Gagal GetAllMapFilesByEstate");
    }

    public class LongOperation extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            try {
                switch (Integer.parseInt(strings[0])) {
                    case LongOperation_GetAllMapFilesByEstate:
                        connectionPresenter.GetAllMapFilesByEstate();
                        break;
                    case LongOperation_SetUpAllMapFilesByEstate:
                        mapFiles = setUpDataSyncHelper.setUpAllMapFilesByEstate(serviceResponse,this);
                        break;
                }
                return strings[0];
            } catch (Exception e) {
                return "Exception";
            }
        }


        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject objProgres = new JSONObject(String.valueOf(values[0]));
                        WidgetHelper.showSnackBar(myCoordinatorLayout,(String.format(objProgres.getString("ket") + " %d %s [%d/%d] ",
                                objProgres.getInt("persen"), "%", objProgres.getInt("count"), objProgres.getInt("total"))));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(result.equals("Exception")){
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Gagal Prepare Data GetAllMapFilesByEstate");
                refreshRv.finishRefresh();
            }else {
                switch (Integer.parseInt(result)) {
                    case LongOperation_SetUpAllMapFilesByEstate:
                        setupAdapter();
                        refreshRv.finishRefresh();
                        break;
                }
            }
        }
    }
}
