package id.co.ams_plantation.harvestqcsap.model;

import java.util.HashMap;
import java.util.HashSet;

import id.co.ams_plantation.harvestqcsap.HarvestApp;
import id.co.ams_plantation.harvestqcsap.R;

public class SensusBjr {
    public static final int SAVE = 1;
    public static final int UPLOAD = 2;


    public static final int COLOR_SAVE = HarvestApp.getContext().getResources().getColor(R.color.Yellow);
    public static final int COLOR_UPLOAD = HarvestApp.getContext().getResources().getColor(R.color.SkyBlue);

    String idQCSensusBJR;
    String idTPanen;
    double latitude;
    double longitude;
    Integer beratBrondolan;
    double rataRataBjr;
    String createBy;
    long createDate;
    int status;
    TPH tph;
    HashMap<Integer,JanjangSensus> beratJanjang;
    HashSet<String> foto;
    TransaksiPanen transaksiPanen;

    public SensusBjr(String idQCSensusBJR,String idTPanen, double latitude, double longitude, Integer beratBrondolan, double rataRataBjr, String createBy, long createDate, int status, TPH tph, HashMap<Integer, JanjangSensus> beratJanjang, HashSet<String> foto,TransaksiPanen transaksiPanen) {
        this.idQCSensusBJR = idQCSensusBJR;
        this.idTPanen = idTPanen;
        this.latitude = latitude;
        this.longitude = longitude;
        this.beratBrondolan = beratBrondolan;
        this.rataRataBjr = rataRataBjr;
        this.createBy = createBy;
        this.createDate = createDate;
        this.status = status;
        this.tph = tph;
        this.beratJanjang = beratJanjang;
        this.foto = foto;
        this.transaksiPanen = transaksiPanen;
    }

    public String getIdQCSensusBJR() {
        return idQCSensusBJR;
    }

    public void setIdQCSensusBJR(String idQCSensusBJR) {
        this.idQCSensusBJR = idQCSensusBJR;
    }

    public String getIdTPanen() {
        return idTPanen;
    }

    public void setIdTPanen(String idTPanen) {
        this.idTPanen = idTPanen;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public Integer getBeratBrondolan() {
        return beratBrondolan;
    }

    public void setBeratBrondolan(Integer beratBrondolan) {
        this.beratBrondolan = beratBrondolan;
    }

    public double getRataRataBjr() {
        return rataRataBjr;
    }

    public void setRataRataBjr(double rataRataBjr) {
        this.rataRataBjr = rataRataBjr;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public TPH getTph() {
        return tph;
    }

    public void setTph(TPH tph) {
        this.tph = tph;
    }

    public HashMap<Integer, JanjangSensus> getBeratJanjang() {
        return beratJanjang;
    }

    public void setBeratJanjang(HashMap<Integer, JanjangSensus> beratJanjang) {
        this.beratJanjang = beratJanjang;
    }

    public HashSet<String> getFoto() {
        return foto;
    }

    public void setFoto(HashSet<String> foto) {
        this.foto = foto;
    }

    public TransaksiPanen getTransaksiPanen() {
        return transaksiPanen;
    }

    public void setTransaksiPanen(TransaksiPanen transaksiPanen) {
        this.transaksiPanen = transaksiPanen;
    }
}
